package ma.atos.integration.monitoring;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.cxf.interceptor.Fault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeOutAOPInterceptor implements MethodInterceptor {
	Logger logger = LoggerFactory.getLogger(TimeOutAOPInterceptor.class
			.getName());
	private long timeout;

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	@Override
	public Object invoke(final MethodInvocation method) throws Throwable {
		try {
			ExecutorService executor = Executors.newCachedThreadPool();
			Callable<Object> task = new Callable<Object>() {
				@Override
				public Object call() {
					try {
						return method.proceed();
					} catch (Throwable e) {
						return null;
					}
				}
			};

			Future<Object> future = executor.submit(task);
			Object result = future.get(timeout, TimeUnit.MILLISECONDS);
			future.cancel(true); // may or may not desire this
			return result;
		} catch (Throwable th) {
			logger.warn(th.toString());
			throw new Fault(th);
		}

	}

}
