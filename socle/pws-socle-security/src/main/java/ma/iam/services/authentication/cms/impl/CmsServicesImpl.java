package ma.iam.services.authentication.cms.impl;

import java.util.Properties;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.services.authentication.Constants;
import ma.iam.services.authentication.cms.CmsServices;

import com.alcatel.ccl.client.CMSClient;
import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.client.CMSFactory;

/**
 * 
 * This class represent . <br/>
 * <br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class CmsServicesImpl implements CmsServices {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * CMS properties Commented by Ftat duplicate with cms
	 */
	// public static Properties cmsProps = null;

	private Properties cms;

	/**
	 * The setter method for the field cms.
	 * 
	 * @param cms
	 *            the cms to set.
	 */
	public void setCms(Properties cms) {
		this.cms = cms;
	}

	/**
	 * ftat comment the properties is loaded by context
	 * 
	 */
	// public void init() {
	// if (cmsProps == null) {
	// Class thisClass = getClass();
	// ClassLoader theClassLoader = thisClass.getClassLoader();
	// InputStream inputStream = theClassLoader
	// .getResourceAsStream(resource);
	//
	// Properties properties = new Properties();
	// try {
	// properties.load(inputStream);
	// } catch (IOException e) {
	// LOGGER.error(getClass(), "init", e.getMessage(), e);
	// throw new IllegalStateException(e.getMessage(), e);
	// }
	// cmsProps = properties;
	// }
	// }

	/**
	 * (non javadoc) See @see
	 * ma.iam.socle.security.cms.ICmsServices#loginCms(java.lang.String,
	 * java.lang.String).
	 * 
	 * @param login
	 * @param motDePasse
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	public void loginCms(String login, String motDePasse)
			throws FunctionalException, TechnicalException {

		LOGGER.info(getClass(), "login", "Tentative de connexion : " + login);

		CMSClient cmsClient = null;

		try {
			/* new a cms client */
			// cmsClient = (CMSClient) CMSFactory.getInstance().createCMSClient(
			// cmsProps);
			cmsClient = (CMSClient) CMSFactory.getInstance().createCMSClient(
					cms);
			cmsClient.connect(login, motDePasse);
		} catch (CMSException ex) {
			String msg = ex.getMessage();
			if (msg.contains("ORA-28000")) {
				throw new FunctionalException(Constants.CMS_COMPTE_VERROUILLE);
			} else {
				throw new FunctionalException(Constants.CMS_ECHEC_AUTH);
			}
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		} finally {
			if (cmsClient != null) {
				/* disconnect */
				try {
					cmsClient.disconnect();
					cmsClient = null;
				} catch (Exception e) {
					LOGGER.error(getClass(), "loginCms",
							"Error while trying to disconnect", e);
				}
			}
		}
	}

}
