package ma.iam.services.authentication;

public class Constants {

	public static final String CMS_COMPTE_VERROUILLE = "cms.compte.verrouille";
	public static final String CMS_ECHEC_AUTH = "cms.echec.authentification";
	public static final String BEAN_USER_CACHE = "userCache";
	public static final String BEAN_AUTHENTICATION_MANAGER = "authenticationManager";

}
