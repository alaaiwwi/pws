package ma.iam.services.authentication;

import java.util.ArrayList;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
;

public class UserDetails extends User implements org.springframework.security.core.userdetails.UserDetails {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -2164670970132627784L;
	
	private Integer costId;
	private Integer locationId;;
	
	/**
	 * Full Constructor
	 * @param username
	 * @param password
	 * @param costId
	 * @param locationId
	 * @throws IllegalArgumentException
	 */
	public UserDetails(String username, String password, Integer costId, Integer locationId)
			throws IllegalArgumentException {
		
		super(username, password, true, true, true, true,  new ArrayList<GrantedAuthority>());
		this.costId = costId;
		this.locationId = locationId;
	}

	/**
	 * The getter method for the field costId.
	 * @return the costId.
	 */
	public Integer getCostId() {
		return costId;
	}

	/**
	 * The setter method for the field costId.
	 * @param costId the costId to set.
	 */
	public void setCostId(Integer costId) {
		this.costId = costId;
	}

	/**
	 * The getter method for the field locationId.
	 * @return the locationId.
	 */
	public Integer getLocationId() {
		return locationId;
	}

	/**
	 * The setter method for the field locationId.
	 * @param locationId the locationId to set.
	 */
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

}
