package ma.iam.services.authentication.cms;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;


/**
 * The Interface Connecteur.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public interface CmsServices {

	/**
	 * Login cms.
	 * 
	 * @param login the login
	 * @param motDePasse the mot de passe
	 * @return the hash map
	 * @throws FunctionalException 
	 * @throws TechnicalException 
	 */
	void loginCms(String login, String motDePasse) throws FunctionalException, TechnicalException;

}
