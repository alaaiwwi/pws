package ma.iam.services.authentication;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.services.authentication.cms.CmsServices;

import org.springframework.dao.DataAccessException;
import org.springframework.util.Assert;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class AuthenticationManager {
	
	private boolean cmsAuthentication;
	
	/** The cmsServices. */
	private CmsServices cmsServices = null;
	
	/**
	 * The setter method for the field cmsAuthentication.
	 * @param cmsAuthentication the cmsAuthentication to set.
	 */
	public void setCmsAuthentication(boolean cmsAuthentication) {
		this.cmsAuthentication = cmsAuthentication;
	}
	
	/**
	 * Sets the cmsServices.
	 *
	 * @param cmsServices the new cmsServices
	 */
	public void setCmsServices(CmsServices cmsServices) {
		this.cmsServices = cmsServices;
	}


	/**
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @throws Exception
	 */
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(cmsServices, "cmsServices is required");
	}

	/**
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @param username
	 * @return
	 * @throws TechnicalException 
	 * @throws DataAccessException
	 */
	public org.springframework.security.core.userdetails.UserDetails authenticate(String username, String password)
			throws FunctionalException, TechnicalException {
		if (cmsAuthentication) {
			cmsServices.loginCms(username.toUpperCase(), password);
		}
		return new UserDetails(username, password, null, null);
	}
}
