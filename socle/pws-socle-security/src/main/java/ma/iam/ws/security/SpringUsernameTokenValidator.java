package ma.iam.ws.security;

import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.dom.handler.RequestData;
import org.apache.wss4j.dom.message.token.UsernameToken;
import org.apache.wss4j.dom.validate.UsernameTokenValidator;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SpringUsernameTokenValidator extends UsernameTokenValidator {

	private final AuthenticationManager authenticationManager;

	public SpringUsernameTokenValidator(
			AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@Override
	protected void verifyPlaintextPassword(UsernameToken usernameToken,
			RequestData data) throws WSSecurityException {
		String user = usernameToken.getName();
		String password = usernameToken.getPassword();
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user, password);
		authentication = authenticationManager.authenticate(authentication);
		if (!authentication.isAuthenticated()) {
			throw new WSSecurityException(
					WSSecurityException.ErrorCode.FAILED_AUTHENTICATION);
		}
		 
	}
}
