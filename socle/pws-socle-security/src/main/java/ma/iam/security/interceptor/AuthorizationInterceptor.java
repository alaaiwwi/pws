package ma.iam.security.interceptor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import ma.iam.security.SecuredObject;

import org.apache.cxf.common.classloader.ClassLoaderUtils;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.common.util.ClassHelper;
import org.apache.cxf.interceptor.security.SimpleAuthorizingInterceptor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

public class AuthorizationInterceptor extends SimpleAuthorizingInterceptor {

	private static final Logger LOG = LogUtils
			.getL7dLogger(AuthorizationInterceptor.class);
	private static final String DEFAULT_ANNOTATION_CLASS_NAME = "javax.annotation.security.RolesAllowed";

	private static final Set<String> SKIP_METHODS;
	static {
		SKIP_METHODS = new HashSet<String>();
		SKIP_METHODS.addAll(Arrays.asList(new String[] { "wait", "notify",
				"notifyAll", "equals", "toString", "hashCode" }));
	}

	private String annotationClassName = DEFAULT_ANNOTATION_CLASS_NAME;
	private String basePackage;

	public void setAnnotationClassName(String name) {
		try {
			ClassLoaderUtils.loadClass(name, AuthorizationInterceptor.class);
			annotationClassName = name;
		} catch (Throwable ex) {
			LOG.warning(ex.getMessage());
			throw new IllegalArgumentException("Annotation class " + name
					+ " is not available");
		}
	}

	public void setSecuredObject(List<Object> listObject) {
		Map<String, String> rolesMapAll = new HashMap<String, String>();

		for (Object object : listObject) {
			Map<String, String> rolesMap = new HashMap<String, String>();
			Class<?> cls = ClassHelper.getRealClass(object);

			findRoles(cls, rolesMap);
			if (rolesMap.isEmpty()) {
				LOG.warning("The roles map is empty, the service object is not protected");
			} else if (LOG.isLoggable(Level.FINE)) {
				for (Map.Entry<String, String> entry : rolesMap.entrySet()) {
					LOG.fine("Method: " + entry.getKey() + ", roles: "
							+ entry.getValue());
				}
			}

			rolesMapAll.putAll(rolesMap);
		}
		super.setMethodRolesMap(rolesMapAll);
	}

	protected void findRoles(Class<?> cls, Map<String, String> rolesMap) {
		if (cls == null || cls == Object.class) {
			return;
		}
		String classRolesAllowed = getRoles(cls.getAnnotations(),
				annotationClassName);
		for (Method m : cls.getMethods()) {
			if (SKIP_METHODS.contains(m.getName())) {
				continue;
			}
			String methodRolesAllowed = getRoles(m.getAnnotations(),
					annotationClassName);
			String theRoles = methodRolesAllowed != null ? methodRolesAllowed
					: classRolesAllowed;
			if (theRoles != null) {
				rolesMap.put(m.getName(), theRoles);
				rolesMap.put(createMethodSig(m), theRoles);
			}
		}
		if (!rolesMap.isEmpty()) {
			return;
		}

		findRoles(cls.getSuperclass(), rolesMap);

		if (!rolesMap.isEmpty()) {
			return;
		}

		for (Class<?> interfaceCls : cls.getInterfaces()) {
			findRoles(interfaceCls, rolesMap);
		}
	}

	private String getRoles(Annotation[] anns, String annName) {
		for (Annotation ann : anns) {
			if (ann.annotationType().getName().equals(annName)) {
				try {
					Method valueMethod = ann.annotationType().getMethod(
							"value", new Class[] {});
					String[] roles = (String[]) valueMethod.invoke(ann,
							new Object[] {});
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < roles.length; i++) {
						sb.append(roles[i]);
						if (i + 1 < roles.length) {
							sb.append(" ");
						}
					}
					return sb.toString();
				} catch (Exception ex) {
					LOG.warning(ex.getMessage());
				}
				break;
			}
		}
		return null;
	}

	public void setBasePackage(String basePackage)
			throws ClassNotFoundException {
		this.basePackage = basePackage;
		Map<String, String> rolesMapAll = new HashMap<String, String>();
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
				false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(SecuredObject.class));
		for (BeanDefinition object : scanner
				.findCandidateComponents(basePackage)) {
			LOG.info("Scanned " + object.getBeanClassName());
			Map<String, String> rolesMap = new HashMap<String, String>();
			Class<?> cls = Class.forName(object.getBeanClassName());

			findRoles(cls, rolesMap);
			if (rolesMap.isEmpty()) {
				LOG.warning("The roles map is empty, the service object is not protected");
			} else if (LOG.isLoggable(Level.FINE)) {
				for (Map.Entry<String, String> entry : rolesMap.entrySet()) {
					LOG.fine("Method: " + entry.getKey() + ", roles: "
							+ entry.getValue());
				}
			}

			rolesMapAll.putAll(rolesMap);
		}
		super.setMethodRolesMap(rolesMapAll);
	}

}
