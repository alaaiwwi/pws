package ma.iam.ws.juddi.publish;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.juddi.api_v3.AccessPointType;
import org.apache.juddi.v3.client.config.UDDIClient;
import org.apache.juddi.v3.client.transport.Transport;
import org.uddi.api_v3.AccessPoint;
import org.uddi.api_v3.AuthToken;
import org.uddi.api_v3.BindingTemplate;
import org.uddi.api_v3.BindingTemplates;
import org.uddi.api_v3.BusinessDetail;
import org.uddi.api_v3.BusinessEntity;
import org.uddi.api_v3.BusinessInfo;
import org.uddi.api_v3.BusinessList;
import org.uddi.api_v3.BusinessService;
import org.uddi.api_v3.CategoryBag;
import org.uddi.api_v3.DeleteService;
import org.uddi.api_v3.Description;
import org.uddi.api_v3.DiscardAuthToken;
import org.uddi.api_v3.FindBusiness;
import org.uddi.api_v3.FindService;
import org.uddi.api_v3.GetAuthToken;
import org.uddi.api_v3.KeyedReference;
import org.uddi.api_v3.Name;
import org.uddi.api_v3.SaveBusiness;
import org.uddi.api_v3.SaveService;
import org.uddi.api_v3.ServiceDetail;
import org.uddi.api_v3.ServiceInfo;
import org.uddi.api_v3.ServiceList;
import org.uddi.v3_service.DispositionReportFaultMessage;
import org.uddi.v3_service.UDDIInquiryPortType;
import org.uddi.v3_service.UDDIPublicationPortType;
import org.uddi.v3_service.UDDISecurityPortType;

public class PublishClient {
	private static Logger logger = Logger.getLogger(PublishClient.class
			.getCanonicalName());
	private static UDDISecurityPortType security = null;
	private static UDDIPublicationPortType publish = null;
	private static UDDIInquiryPortType inquiry = null;

	public PublishClient(String conf) {
		init(conf);
	}

	public PublishClient() {
		init("META-INF/uddi.xml");
	}

	private void init(String conf) {
		try {

			UDDIClient uddiClient = new UDDIClient(conf);
			Transport transport = uddiClient.getTransport("default");
			security = transport.getUDDISecurityService();
			publish = transport.getUDDIPublishService();
			inquiry = transport.getUDDIInquiryService();
		} catch (Exception e) {
			logger.warning(e.getMessage());
		}
	}

	public String findBusinessByName(String authInfi, String businessName) {
		try {
			FindBusiness findBusiness = new FindBusiness();
			findBusiness.setAuthInfo(authInfi);
			findBusiness.getName().add(new Name(businessName, null));
			BusinessList businessList = inquiry.findBusiness(findBusiness);
			if (businessList == null || businessList.getBusinessInfos() == null)
				return null;
			List<BusinessInfo> businessInfoList = businessList
					.getBusinessInfos().getBusinessInfo();
			if (businessInfoList == null || businessInfoList.size() != 1)
				return null;
			for (BusinessInfo businessInfo : businessInfoList) {
				return businessInfo.getBusinessKey();
			}
		} catch (DispositionReportFaultMessage e) {

			logger.warning(e.getMessage());
		} catch (RemoteException e) {

			logger.warning(e.getMessage());
		}
		return null;
	}

	public String findServiceByName(String authInfi, String serviceName) {
		try {
			FindService findService = new FindService();

			findService.setAuthInfo(authInfi);
			findService.getName().add(new Name(serviceName, null));
			ServiceList serviceList = inquiry.findService(findService);
			if (serviceList == null || serviceList.getServiceInfos() == null)
				return null;
			List<ServiceInfo> serviceInfoList = serviceList.getServiceInfos()
					.getServiceInfo();
			if (serviceInfoList == null || serviceInfoList.size() != 1)
				return null;
			for (ServiceInfo serviceInfo : serviceInfoList) {
				return serviceInfo.getServiceKey();
			}
		} catch (DispositionReportFaultMessage e) {

			logger.warning(e.getMessage());
		} catch (RemoteException e) {

			logger.warning(e.getMessage());
		}
		return null;
	}

	public String getAuthInfo(String login, String password) {
		try {
			GetAuthToken getAuthTokenMyPub = new GetAuthToken();
			getAuthTokenMyPub.setUserID(login);
			getAuthTokenMyPub.setCred(password);
			AuthToken myPubAuthToken;

			myPubAuthToken = security.getAuthToken(getAuthTokenMyPub);

			return myPubAuthToken.getAuthInfo();
		} catch (DispositionReportFaultMessage e) {

			logger.warning(e.getMessage());
		} catch (RemoteException e) {

			logger.warning(e.getMessage());
		}
		return null;
	}

	public String addBusiness(String authInfo, String businessName)
			throws DispositionReportFaultMessage, RemoteException {

		BusinessEntity myBusEntity = new BusinessEntity();
		Name myBusName = new Name();
		myBusName.setValue(businessName);
		myBusEntity.getName().add(myBusName);

		SaveBusiness sb = new SaveBusiness();
		sb.getBusinessEntity().add(myBusEntity);
		sb.setAuthInfo(authInfo);
		BusinessDetail bd = publish.saveBusiness(sb);
		String myBusKey = bd.getBusinessEntity().get(0).getBusinessKey();
		return myBusKey;
	}

	public String addService(String authInfo, String businessKey,
			String serviceName, String wsdl, String description,
			Map<String, String> categories)
			throws DispositionReportFaultMessage, RemoteException {
		BusinessService myService = new BusinessService();
		myService.setBusinessKey(businessKey);
		Name myServName = new Name();
		myServName.setValue(serviceName);
		myService.getName().add(myServName);
		myService.getDescription().add(new Description(description, null));
		addCategories(myService, categories);
		BindingTemplate myBindingTemplate = new BindingTemplate();
		AccessPoint accessPoint = new AccessPoint();
		accessPoint.setUseType(AccessPointType.WSDL_DEPLOYMENT.toString());
		accessPoint.setValue(wsdl);
		myBindingTemplate.setAccessPoint(accessPoint);
		BindingTemplates myBindingTemplates = new BindingTemplates();
		myBindingTemplate = UDDIClient.addSOAPtModels(myBindingTemplate);
		myBindingTemplates.getBindingTemplate().add(myBindingTemplate);

		myService.setBindingTemplates(myBindingTemplates);

		SaveService ss = new SaveService();
		ss.getBusinessService().add(myService);
		ss.setAuthInfo(authInfo);
		ServiceDetail sd = publish.saveService(ss);
		String myServKey = sd.getBusinessService().get(0).getServiceKey();
		return myServKey;
	}

	private void addCategories(BusinessService myService,
			Map<String, String> categories) {

		CategoryBag categoryBag = new CategoryBag();
		// List<KeyedReference> list=new ArrayList<KeyedReference>();
		// categoryBag.
		myService.setCategoryBag(categoryBag);
		for (String key : categories.keySet()) {
			KeyedReference keyedReference = new KeyedReference();
			keyedReference.setKeyName(key);
			keyedReference.setKeyValue(categories.get(key));
			keyedReference.setTModelKey("tModel");
			
			myService.getCategoryBag().getKeyedReference().add(keyedReference);
		}
	}

	public void deleteService(String authInfo, String serviceKey)
			throws DispositionReportFaultMessage, RemoteException {
		DeleteService deleteService = new DeleteService();
		deleteService.setAuthInfo(authInfo);
		deleteService.getServiceKey().add(serviceKey);

		publish.deleteService(deleteService);

	}

	public void publish(String login, String password, String business,
			String serviceName, String wsdl, String description,
			Map<String, String> categories) {
		try {

			String authInfo = getAuthInfo(login, password);
			String myBusKey = findBusinessByName(authInfo, business);
			if (myBusKey == null)
				myBusKey = addBusiness(authInfo, business);
			logger.info("myBusiness key:  " + myBusKey);

			String myServKey = findServiceByName(authInfo, serviceName);

			if (myServKey != null)
				deleteService(authInfo, myServKey);
			myServKey = addService(authInfo, myBusKey, serviceName, wsdl,
					description, categories);

			logger.info("myService key:  " + myServKey);

			security.discardAuthToken(new DiscardAuthToken(authInfo));
			logger.info("Success!");

		} catch (Exception e) {
			logger.warning(e.getMessage());
			
		}
	}

}
