package ma.iam.ws.juddi.publish;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;

/**
 * Servlet implementation class PublishServlet
 */
public class PublishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager
			.getLogger(PublishServlet.class.getName());
	private static final String ENABLED = "juddi.enabled";

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init();
		// Context envEntryContext;
		String base_config = "";
		try {
			// envEntryContext = (Context) new InitialContext()
			// .lookup("java:comp/env");
			// base_config = (String) envEntryContext.lookup("BASE_CONFIG");
			base_config = System.getenv("BASE_CONFIG");
			base_config += "/";
		} catch (Exception e1) {
			logger.error(e1.getMessage());
		}
		
		String publishParamsLocation = config.getServletContext()
				.getInitParameter("publish-params-location");
		String uddiConfigLocation = config.getServletContext()
				.getInitParameter("uddi-config-location");

		String publishParamsLocationEboutique = config.getServletContext()
				.getInitParameter("publish-params-location-eboutique");

		Properties propertiesGlobale = new Properties();
		Properties propertiesEboutique = new Properties();
		try {
			File fileParams = new File(base_config + publishParamsLocation);
			File fileParamsEboutique = new File(base_config
					+ publishParamsLocationEboutique);
			if (fileParams.exists() && fileParamsEboutique.exists()) {
				propertiesGlobale.load(new FileInputStream(fileParams));
				propertiesEboutique.load(new FileInputStream(
						fileParamsEboutique));

				if (Boolean.valueOf((String) propertiesGlobale.get(ENABLED))) {
					PublishClient publishClient = new PublishClient(base_config
							+ uddiConfigLocation);
					String servicelist = (String) propertiesEboutique
							.get("servicelist");
					String[] servicelistArray = servicelist.split(",");
					for (String ser : servicelistArray) {
						if (Boolean.valueOf((String) propertiesEboutique
								.getProperty(ser + ".publish.enabled"))) {
							String login = (String) propertiesGlobale
									.get("login");
							String password = (String) propertiesGlobale
									.get("password");
							String business = (String) propertiesEboutique
									.get(ser + ".business");
							String serviceName = (String) propertiesEboutique
									.get(ser + ".serviceName");
							String wsdl = (String) propertiesEboutique.get(ser
									+ ".wsdl");
							String description = (String) propertiesEboutique
									.get(ser + ".description")
									+ " ["
									+ new SimpleDateFormat().format(new Date())
									+ "]";
							Integer cat = Integer
									.parseInt((String) propertiesGlobale
											.get("categorie"));
							Map<String, String> hashMap = new Hashtable<String, String>();

							for (int i = 1; i <= cat; i++) {
								hashMap.put(
										(String) propertiesEboutique.get(ser
												+ ".cat" + i + ".key"),
										(String) propertiesEboutique.get(ser
												+ ".cat" + i + ".value"));
							}

							publishClient.publish(login, password, business,
									serviceName, wsdl, description, hashMap);
						}
					}
				}
			}
		} catch (FileNotFoundException e) {

			logger.error(e.getMessage());
		} catch (IOException e) {

			logger.error(e.getMessage());
		}
		super.init(config);
	}

}
