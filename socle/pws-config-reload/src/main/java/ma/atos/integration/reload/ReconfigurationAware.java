package ma.atos.integration.reload;

/**
 */
public interface ReconfigurationAware {
  public void beforeReconfiguration() throws Exception;;
  public void afterReconfiguration() throws Exception;;
}
