package ma.atos.integration.reload;

/**
 *
 */
public interface ReloadablePropertiesListener {
  void propertiesReloaded(PropertiesReloadedEvent event);
}
