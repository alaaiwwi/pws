package ma.atos.integration.reload;

public interface ReconfigurableBean {
  void reloadConfiguration() throws Exception;
}
