package ma.iam.mutualisation.fwk.transaction;

import ma.iam.mutualisation.fwk.cms.service.SOIService;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionStatus;
import org.springframework.transaction.support.ResourceTransactionManager;

import com.alcatel.ccl.client.CMSException;

public class CMSTransactionManager extends AbstractPlatformTransactionManager
		implements ResourceTransactionManager, InitializingBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2823749970173512305L;
	private SOIService soiService;

	public void afterPropertiesSet() throws Exception {
		if (getSoiService() == null) {
			throw new IllegalArgumentException(
					"Property 'soiService' is required");
		}
	}

	@Override
	protected void doCommit(DefaultTransactionStatus arg0)
			throws TransactionException {
		try {
			if (getSoiService() == null
					|| getSoiService().getCmsClient() == null) {
				throw new IllegalArgumentException(
						"Property 'soiService' is required");
			}
			soiService.getCmsClient().commit();
		} catch (CMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void doRollback(DefaultTransactionStatus arg0)
			throws TransactionException {
		try {
			if (getSoiService() == null
					|| getSoiService().getCmsClient() == null) {
				throw new IllegalArgumentException(
						"Property 'soiService' is required");
			}
			getSoiService().getCmsClient().rollback();
		} catch (CMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public SOIService getSoiService() {
		return soiService;
	}

	public void setSoiService(SOIService soiService) {
		this.soiService = soiService;
	}

	public Object getResourceFactory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object doGetTransaction() throws TransactionException {
		return null;
	}

	@Override
	protected void doBegin(Object transaction, TransactionDefinition definition)
			throws TransactionException {
		// TODO Auto-generated method stub

	}

}
