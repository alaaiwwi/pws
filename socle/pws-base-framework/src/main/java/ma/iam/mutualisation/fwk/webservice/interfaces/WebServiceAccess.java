package ma.iam.mutualisation.fwk.webservice.interfaces;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

/**
 * The Interface WebServiceAccess.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public interface WebServiceAccess {

	/**
	 * Invoke ws.
	 * 
	 * @param urlName
	 *            the url name
	 * @param serviceName
	 *            the service name
	 * @param agr
	 *            the agr
	 * @return the string
	 * @throws TechnicalException
	 *             the technical exception
	 */
	 String invokeWs(String urlName, String serviceName, Object[] arg) throws TechnicalException;
}
