package ma.iam.mutualisation.fwk.beans;

import ma.iam.mutualisation.fwk.common.Constants;


/**
 * The Class ResultatBean.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class ResultatBean implements java.io.Serializable {

	private static final long serialVersionUID = -2446717644187305012L;

	/** The Constant ALIAS. */
	public static final String ALIAS = "resultat";

	/** The code resultat. */
	private int codeResultat = Constants.ERROR_CODE;

	/** The message resultat. */
	private String messageResultat;

	/**
	 * Instantiates a new b resultat.
	 */
	public ResultatBean(String messageResultat) {
		super();
		this.messageResultat = messageResultat;
	}

	/**
	 * Instantiates a new b resultat.
	 * 
	 * @param codeResultat
	 *            the code resultat
	 * @param messageResultat
	 *            the message resultat
	 */
	public ResultatBean(int codeResultat, String messageResultat) {
		super();
		this.codeResultat = codeResultat;
		this.messageResultat = messageResultat;
	}

	/**
	 * Gets the code resultat.
	 * 
	 * @return the code resultat
	 */
	public int getCodeResultat() {
		return codeResultat;
	}

	/**
	 * Gets the message resultat.
	 * 
	 * @return the message resultat
	 */
	public String getMessageResultat() {
		return messageResultat;
	}
}
