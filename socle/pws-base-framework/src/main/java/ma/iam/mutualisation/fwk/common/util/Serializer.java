package ma.iam.mutualisation.fwk.common.util;

import ma.iam.mutualisation.fwk.beans.ResultatBean;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * The Class Serializer.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class Serializer {

	/** The xstream. */
	static XStream xstream = null;

	/**
	 * Serialize.
	 * 
	 * @param obj
	 *            the obj
	 * @return the string
	 * @throws TechnicalException 
	 */
	public static String serialize(Object obj) throws TechnicalException {
		try {
			if (null == xstream) {
				init();
			}
			return xstream.toXML(obj);
		} catch (Exception ex) {
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	/**
	 * Inits the.
	 */
	public static void init() {
		xstream = new XStream(new DomDriver());
		
		xstream.alias(ResultatBean.ALIAS, ResultatBean.class);
	}

	/**
	 * Deserialize.
	 * 
	 * @param xml
	 *            the xml
	 * @return the object
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public static Object deserialize(String xml) throws TechnicalException {
		try {
			if (null == xstream) {
				init();
			}
			return xstream.fromXML(xml);
		} catch (Exception ex) {
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

}
