package ma.iam.mutualisation.fwk.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import ma.iam.mutualisation.fwk.common.logging.LogManager;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.util.Assert;

/**
 * RetrievePasswordCallbackHandler. <br/>
 * <br/>
 * Projet : Mutualisation
 * Créé le 3 juin 2011
 * 
 * @author mei
 */
public class RetrievePasswordCallbackHandler implements CallbackHandler {

	private UserCache userCache = new NullUserCache();

	private UserDetailsService userDetailsService;

	/** Sets the users cache. Not required, but can benefit performance. */
	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	/** Sets the Spring Security user details service. Required. */
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(userDetailsService, "userDetailsService is required");
	}

	private UserDetails loadUserDetails(String username)
			throws DataAccessException {
		UserDetails user = userCache.getUserFromCache(username);

		if (user == null) {
			SecurityContextHolder.clearContext();
			try {
				user = userDetailsService.loadUserByUsername(username);
			} catch (UsernameNotFoundException notFound) {
				LogManager.getLogger().error(getClass(), "loadUserDetails", "Username '" + username + "' not found");
				return null;
			}
			userCache.putUserInCache(user);
		}
		return user;
	}

	/**
	 * See @see
	 * javax.security.auth.callback.CallbackHandler#handle(javax.security
	 * .auth.callback.Callback[]).
	 * 
	 * @param callbacks
	 * @throws IOException
	 * @throws UnsupportedCallbackException
	 */
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		WSPasswordCallback callback = (WSPasswordCallback) callbacks[0];

		String identifier = callback.getIdentifier();
		UserDetails user = loadUserDetails(identifier);
		if (user != null) {
			callback.setPassword(user.getPassword());
			SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, user.getPassword(), null));
		}
	}

}
