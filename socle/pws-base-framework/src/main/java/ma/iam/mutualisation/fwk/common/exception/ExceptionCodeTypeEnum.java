package ma.iam.mutualisation.fwk.common.exception;

/*
 * @author AtoS
 */

/**
 * The Enum ExceptionCodeTypeEnum.
 */
public enum ExceptionCodeTypeEnum {

	/** The EMPT y_ resul t_ ws. */
	EMPTY_RESULT_WS_FR("ERR_001", "Aucun résultat trouvé"),

	/** The PROBLE m_ criteri a_ ws. */
	PROBLEM_CRITERIA_WS_FR("ERR_002", "Probléme dans les arguments d'entrés"),

	
	/** The ERRW s1000. */
	SYSTEM_ERROR_WS("WS-ERR", "Erreur systéme au niveau du webservice"),

	/** The ERRW s1000. */
	SYSTEM_ERROR_CMS("WS-CMS-ERR", "Erreur systéme au niveau CMS"),

	/** The QUER y_ error. */
	WS_SYNTAX_ERROR("ERR_ERR", "Erreur de format de la requéte utilisée");

	/** The error code. */
	private final String errorCode;

	/** The error context. */
	private final String errorContext;

	/**
	 * Instantiates a new exception code type enum.
	 * 
	 * @param errorCode
	 *            the error code
	 * @param errorContext
	 *            the error context
	 */
	ExceptionCodeTypeEnum(final String errorCode, final String errorContext) {
		this.errorCode = errorCode;
		this.errorContext = errorContext;
	}

	/**
	 * Gets the error code.
	 * 
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error name.
	 * 
	 * @return the error name
	 */
	public String getErrorContext() {
		return errorContext;
	}

}
