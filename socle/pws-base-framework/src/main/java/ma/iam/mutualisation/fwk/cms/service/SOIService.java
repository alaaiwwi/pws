package ma.iam.mutualisation.fwk.cms.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.util.PropertiesUtil;

import com.alcatel.ccl.client.CMSClient;
import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.client.CMSFactory;
import com.alcatel.ccl.client.ConnectedCMSClientPool;

@SuppressWarnings("unchecked")
public class SOIService implements ISoiService {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager.getLogger(SOIService.class
			.getName());

	private CMSClient cmsClient;
	private ConnectedCMSClientPool cmsClientPool = null;

	/**
	 * info for connection
	 */
	private String login;
	private String passWord;

	private String serversoiname;
	private String serverversion;

	private PropertiesUtil propertiesUtil;

	public String getUserName() {
		return login;
	}

	public SOIService init() {
		return new SOIService();
	}

	private SOIService() {

	}

	private Properties mapToProperties(Map<String, String> map) {
		Properties p = new Properties();
		Set<Map.Entry<String, String>> set = map.entrySet();
		for (Map.Entry<String, String> entry : set) {
			p.put(entry.getKey(), entry.getValue());
		}
		return p;
	}

	public void login(String login, String motDePasse) throws CMSException {

		cmsClient.connect(login, motDePasse);
		this.login = login;

	}

	public Map executeCommand(String command, Map inputParameters)
			throws CMSException {
		LOGGER.info("Call :" + command + " Command With following parameters "
				+ inputParameters);

		if (inputParameters instanceof HashMap<?, ?>) {
			return cmsClient.executeCommand(command,
					(HashMap<String, Object>) inputParameters);
		} else {
			throw new CMSException("error in input parameter");
		}

	}

	/**
	 * 
	 * @param cmsClient
	 * @param login
	 * @param motDePasse
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Boolean checkConnection(CMSClient cmsClient, String login,
			String motDePasse) {

		try {
			cmsClient.checkConnection(login, motDePasse);
			return Boolean.TRUE;
		} catch (CMSException ex) {
			LOGGER.error("checkConnection Error when trying to close pool : ",
					ex);
			return Boolean.FALSE;
		}

	}

	public void initialiseConnection() throws CMSException {
		if (cmsClient == null) {

			Properties cmsPropsVar = mapToProperties(propertiesUtil
					.getPropertiesMap());

			try {

				cmsPropsVar.put("cms.soiname", serversoiname);
				cmsPropsVar.put("cms.soiversion", serverversion);

				cmsClient = (CMSClient) CMSFactory.getInstance()
						.createCMSClient(cmsPropsVar);
				cmsClient.connect(this.login, this.passWord);

			} catch (CMSException ex) {
				// TODO
				throw new CMSException("Erreur in Getting Soi Service client",
						ex);
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
	}

	public void cleanUp() {
		if (cmsClient != null) {
			cmsClient.disconnect();
		}
	}

	public boolean isConnectionOk() {
		LOGGER.info("=> isConnectionOk()");

		boolean isOk = true;

		try {
			executeCommand("WELCOMEPROCS.READ", new HashMap<String, Object>());
		} catch (CMSException exception) {
			isOk = false;
			cmsClient.disconnect();

		}

		LOGGER.info("<= isConnectionOk() : " + isOk);
		return isOk;
	}

	public CMSClient getCmsClient() {
		return cmsClient;
	}

	public void setCmsClient(CMSClient cmsClient) {
		this.cmsClient = cmsClient;
	}

	public ConnectedCMSClientPool getCmsClientPool() {
		return cmsClientPool;
	}

	public void setCmsClientPool(ConnectedCMSClientPool cmsClientPool) {
		this.cmsClientPool = cmsClientPool;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public PropertiesUtil getPropertiesUtil() {
		return propertiesUtil;
	}

	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getServersoiname() {
		return serversoiname;
	}

	public void setServersoiname(String serversoiname) {
		this.serversoiname = serversoiname;
	}

	public String getServerversion() {
		return serverversion;
	}

	public void setServerversion(String serverversion) {
		this.serverversion = serverversion;
	}

}
