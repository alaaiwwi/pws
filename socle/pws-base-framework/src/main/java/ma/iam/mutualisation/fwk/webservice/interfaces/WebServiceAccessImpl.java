package ma.iam.mutualisation.fwk.webservice.interfaces;

import java.util.HashMap;
import java.util.Map;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

/**
 * The Class WebServiceAccessImpl.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class WebServiceAccessImpl implements WebServiceAccess {
	
	private Map<String, Client> services = new HashMap<String, Client>();
	
	public WebServiceAccessImpl(Map<String, Object> properties, long connectionTimeout, long receiveTimeout) {
		super();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(connectionTimeout);
		httpClientPolicy.setReceiveTimeout(receiveTimeout);
		HTTPConduit http = null;
		for (Map.Entry<String, Object> entry : properties.entrySet()) {
			Client client = ClientProxy.getClient(entry.getValue());
			http = (HTTPConduit) client.getConduit();
			http.setClient(httpClientPolicy);
			services.put(entry.getKey(), client);
		}
	}
	
	/**
	 * Invoke ws.
	 * 
	 * @param urlName
	 *            the url name
	 * @param serviceName
	 *            the service name
	 * @param arg
	 *            the arg
	 * @return the string
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public String invokeWs(String urlName, String serviceName, Object[] arg) throws TechnicalException {
		Object[] results = null;
		try {
			results = services.get(urlName).invoke(serviceName, (arg != null ? arg : new Object[] {}));
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
		if (results != null && results.length > 0) {
			return results[0].toString();
		}
		return null;
	}
}
