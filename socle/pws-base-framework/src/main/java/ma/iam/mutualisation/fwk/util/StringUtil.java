package ma.iam.mutualisation.fwk.util;

public class StringUtil {
	public static final boolean isNullOrEmpty(String value) {
		return (value == null || "".equals(value)) ? Boolean.TRUE
				: Boolean.FALSE;
	}

	public static final boolean isNumeric(String value) {

		try {
			Long.parseLong(value);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
}
