package ma.iam.mutualisation.fwk.common.logging;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe assurant la trace des messages de log.
 * <br/><br/>
 * Projet : GE
 * Créé le 5 oct. 2010
 * 
 * @author mei
 */
public final class LogManager {

    /**
     * Attribut stockant l'instance unique de LogManager.
     */
     private static LogManager instance;

     /**
      * Attribut pour stocker les loggers par modules.
      */
     private static Map<String, Logger> loggers = null;

    /**
     * Constructeur redéfini comme étant privé pour forcer
     * l'appel à la méthode getInstance().
     */
    private LogManager() {
        if (null == loggers) {
        	loggers = new HashMap<String, Logger>();
        }
    }

    /**
     * l'instance unique de la class LogManager.
     * @since 4.0
     * @return LogManager
     */
    private static void init() {
        if (null == instance) {
            instance = new LogManager();
        }
    }
    
	/**
     * Permet de récupérer (ou créer s’il n’existe pas déjà)le Logger associé
     * au module dont le nom est rootLogger.
     * @param name de type String
     * @return Logger
     */
    public static Logger getLogger() {
    	init();
    	Logger logger = loggers.get("root");
        if (logger == null) {
        	logger = new Logger();
            loggers.put("root", logger);
        }
        return logger;
    }
    
	/**
     * Permet de récupérer (ou créer s’il n’existe pas déjà)le Logger associé
     * au module dont le nom est fourni parmi les paramètres.
     * @param name de type String
     * @return Logger
     */
    public static Logger getLogger(String name) {
    	init();
    	Logger logger = loggers.get(name);
        if (logger == null) {
        	logger = new Logger(name);
            loggers.put(name, logger);
        }
        return logger;
    }
}
