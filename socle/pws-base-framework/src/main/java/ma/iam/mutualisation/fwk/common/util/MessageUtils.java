package ma.iam.mutualisation.fwk.common.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import ma.iam.mutualisation.fwk.beans.ResultatBean;
import ma.iam.mutualisation.fwk.common.Constants;
import ma.iam.mutualisation.fwk.common.logging.LogManager;

/**
 * The Class MessageUtils : Permet de lire les paramètres du fichier
 * message.properties
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class MessageUtils {

	/** The Constant BUNDLE_NAME. */
	private static final String BUNDLE_NAME = Constants.MESSAGE_FILE_NAME;

	/** The Constant RESOURCE_BUNDLE. */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	/**
	 * Instantiates a new message.
	 */
	private MessageUtils() {
	}

	/**
	 * Gets the message.
	 * 
	 * @param key
	 *            the key
	 * 
	 * @return the message
	 */
	public static String getMessage(String key) {
		try {
			return new String(RESOURCE_BUNDLE.getString(key).getBytes("ISO-8859-1"),"UTF-8") ;
		} catch (Exception e) {
			LogManager.getLogger().error(MessageUtils.class, "getMessage", e.getMessage(), e);
			return key;
		}
	}
	
 	/**
 	 * Retourne la chaine de caractères associée à la clé.
 	 * @param key la clef
 	 * @param args tableau de paramètres Object[] 
 	 * @return String
 	 */
 	public static String getMessage(String key, Object[] args) {
 		String message = null;
 		String value = getMessage(key);
 		message = MessageFormat.format(value, args);
 		return message;
 	}

	public static ResultatBean getErrorMessage() {
		return new ResultatBean(Constants.ERROR_CODE, getMessage(Constants.MESSAGE_ERREUR_TECHNIQUE));
	}
	
	public static ResultatBean getErrorMessage(String key) {
		return new ResultatBean(Constants.ERROR_CODE, getMessage(key));
	}
	
	/**
	 * Construit un message d'erreur et le retourner sous forme d'un résultat.
	 * @param key the key
	 * @param args tableau de paramètres
	 * @return BResultat contenant un message d'information
	 */
	public static ResultatBean getErrorMessage(String key, Object[] args) {
		return new ResultatBean(Constants.ERROR_CODE, getMessage(key, args));
	}
	
	public static ResultatBean getConfirmMessage(String key) {
		return new ResultatBean(Constants.CONFIRM_CODE, getMessage(key));
	}
	
	public static ResultatBean getConfirmMessage(String key, Object[] args) {
		return new ResultatBean(Constants.CONFIRM_CODE, getMessage(key, args));
	}
	
	public static ResultatBean getInfoMessage(String key) {
		return new ResultatBean(Constants.INFO_CODE, getMessage(key));
	}
	
	public static ResultatBean getInfoMessage(String key, Object[] args) {
		return new ResultatBean(Constants.INFO_CODE, getMessage(key, args));
	}
	
	public static ResultatBean getHabilitaionMessage(String key) {
		return new ResultatBean(Constants.HABLITATION_CODE, getMessage(key));
	}
}
