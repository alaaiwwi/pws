package ma.iam.mutualisation.fwk.dao.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.dao.PersistenceManager;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.SessionImpl;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.springframework.transaction.annotation.Transactional;

/**
 * La classe d'implémentation su service de persistance.
 * 
 * @author Atos Origin
 * @version 1.0
 */
@Transactional
public class PersistenceManagerImpl implements PersistenceManager {

	/**
	 * Find by id.
	 * 
	 * @param classe
	 *            the classe
	 * @param id
	 *            the id
	 * @param lock
	 *            the lock
	 * @return the object
	 * @throws TechnicalException
	 *             the technical exception
	 * @see ma.iam.selfcare.dao.PersistenceManager#findById(java.lang.Class,
	 *      java.io.Serializable, boolean)
	 */

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Object findById(Class classe, Serializable id, boolean lock)
			throws TechnicalException {
		Object entity = null;
		try {
			if (lock) {
				entity = getCurrentSessionFactory().get(classe, id,
						LockMode.UPGRADE);
			} else {
				entity = getCurrentSessionFactory().get(classe, id);
			}
		} catch (Exception e) {
			throw new TechnicalException("dao.probleme.recherche.par.id", e);
		}

		return entity;
	}

	/**
	 * Find all. See @see
	 * ma.iam.geve.dao.PersistenceManager#findAll(java.lang.Class, boolean).
	 * 
	 * @param classe
	 * @param cacheable
	 * @return
	 * @throws TechnicalException
	 */
	public List findAll(Class classe, boolean cacheable)
			throws TechnicalException {
		try {
			Criteria crit = getCurrentSessionFactory().createCriteria(classe);
			if (cacheable) {
				crit.setCacheable(cacheable);
			}
			return crit.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * Persiste un objet en base.
	 * 
	 * @param entity
	 *            the entity
	 * @throws TechnicalException
	 *             the technical exception
	 * @see ma.iam.selfcare.dao.PersistenceManager#save(java.lang.Object)
	 */
	public void save(Object entity) throws TechnicalException {
		try {
			Session session = getCurrentSessionFactory();
			try {
				session.saveOrUpdate(entity);
			} catch (NonUniqueObjectException e) {
				session.merge(entity);
			}
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * Supprimer.
	 * 
	 * @param entity
	 *            the entity
	 * @throws TechnicalException
	 *             the technical exception
	 * @see ma.iam.selfcare.dao.PersistenceManager#delete(java.lang.Object)
	 */
	public void delete(Object entity) throws TechnicalException {
		try {
			getCurrentSessionFactory().delete(entity);
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}

	}

	/**
	 * Supprimer.
	 * 
	 * @param classe
	 *            the classe
	 * @param id
	 *            the id
	 * @throws TechnicalException
	 *             the technical exception
	 * @see ma.iam.selfcare.dao.PersistenceManager#delete(java.lang.Class,
	 *      java.io.Serializable)
	 */
	public void delete(Class classe, Serializable id) throws TechnicalException {
		Object obj = findById(classe, id, false);
		if (obj != null) {
			delete(obj);
		}

	}

	/**
	 * Use this inside subclasses as a convenience method.
	 * 
	 * @param classe
	 *            the classe
	 * @param criterion
	 *            the criterion
	 * @return the list
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public List findByCriteria(Class classe, List criteriasList,
			boolean cacheable) throws TechnicalException {
		try {
			Criteria crit = getCurrentSessionFactory().createCriteria(classe);
			if (cacheable) {
				crit.setCacheable(cacheable);
			}
			if (criteriasList != null) {
				for (Iterator next = criteriasList.iterator(); next.hasNext();) {
					crit.add((Criterion) next.next());
				}
			}
			return crit.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @param requete
	 * @param alias
	 * @param classe
	 * @return
	 * @throws TechnicalException
	 */
	public List findBySQLQuery(String requete, String alias, Class classe)
			throws TechnicalException {
		try {
			Query query = getCurrentSessionFactory().createSQLQuery(requete)
					.addEntity(alias, classe);
			return query.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.geve.dao.PersistenceManager#findByQuery(java.lang.String).
	 * 
	 * @param hqlQuery
	 * @return
	 * @throws TechnicalException
	 */
	public List findByQuery(String hqlQuery) throws TechnicalException {
		try {
			Query query = getCurrentSessionFactory().createQuery(hqlQuery);
			return query.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	public List findByQuery(String hqlQuery, Map<String, Object> parameters)
			throws TechnicalException {
		try {
			Query query = getCurrentSessionFactory().createQuery(hqlQuery);
			for (String cle : parameters.keySet()) {
				query.setParameter(cle, parameters.get(cle));
			}
			return query.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * Find by criteria.
	 * 
	 * @param classe
	 *            the classe
	 * @param criteria
	 *            the criteria
	 * @param orders
	 *            the orders
	 * @return the list
	 * @throws TechnicalException
	 *             the technical exception
	 * @see ma.iam.selfcare.dao.PersistenceManager#findByCriteria(java.lang.Class,
	 *      java.util.List, java.util.List, boolean)
	 */
	public List findByCriteria(Class classe, List criteria, List orders,
			boolean cacheable) throws TechnicalException {
		try {
			Session session = getCurrentSessionFactory();
			Criteria crit = session.createCriteria(classe);
			if (cacheable) {
				crit.setCacheable(true);
			}
			if (criteria != null) {
				for (int i = 0; i < criteria.size(); i++) {
					crit.add((Criterion) criteria.get(i));
				}
			}
			if (orders != null) {
				for (int i = 0; i < orders.size(); i++) {
					crit.addOrder((Order) orders.get(i));
				}
			}
			return crit.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/*
	 * (non javadoc) See @see
	 * ma.iam.geve.dao.PersistenceManager#getNamedQuery(java.lang.String,
	 * java.util.Map).
	 * 
	 * @param queryName
	 * 
	 * @param params
	 * 
	 * @return List
	 * 
	 * @throws TechnicalException
	 */
	public List getNamedQuery(String queryName, Map<String, Object> params)
			throws TechnicalException {
		try {
			Session session = getCurrentSessionFactory();
			Query query = session.getNamedQuery(queryName);
			if (params != null) {
				Set<Map.Entry<String, Object>> entries = params.entrySet();
				for (Map.Entry<String, Object> entry : entries) {
					if (entry.getValue() instanceof Collection) {
						query.setParameterList(entry.getKey(),
								(Collection) entry.getValue());
					} else {
						query.setString(entry.getKey(),
								String.valueOf(entry.getValue()));
					}
				}
			}
			return query.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * Créer un objet Criteria.
	 * 
	 * @param classe
	 *            the classe
	 * @return Criteria
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public Criteria createCriteria(Class classe) throws TechnicalException {
		try {
			return getCurrentSessionFactory().createCriteria(classe);
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * execute une requête update
	 * 
	 * @param sqlQuery
	 * @param params
	 * @throws TechnicalException
	 */
	public int executeUpdateQuery(String sqlQuery, List params)
			throws TechnicalException {
		try {
			Query query = getCurrentSessionFactory().createSQLQuery(sqlQuery);
			if (params != null) {
				for (int i = 0; i < params.size(); i++) {
					query.setParameter(i, params.get(i));
				}
			}
			return query.executeUpdate();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ma.iam.selfcare.dao.PersistenceManager#findByExample(java.lang.Class,
	 * java.lang.Object, java.lang.String[])
	 */
	public List findByExample(Class classe, Object exampleInstance,
			String[] excludeProperty) throws TechnicalException {

		Criteria crit = getCurrentSessionFactory().createCriteria(classe);
		try {
			Example example = Example.create(exampleInstance);
			if (excludeProperty != null) {
				for (int i = 0; i < excludeProperty.length; i++) {
					example.excludeProperty(excludeProperty[i]);
				}
			}
			crit.add(example);
			return crit.list();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ma.iam.selfcare.dao.PersistenceManager#getQueryResult(java.lang.String,
	 * java.util.Hashtable)
	 */
	public ScrollableResults getQueryResult(String queryName, Hashtable params)
			throws TechnicalException {

		Enumeration listKeys = params.keys();
		Session session = getCurrentSessionFactory();
		try {
			Query query = session.getNamedQuery(queryName);
			while (listKeys.hasMoreElements()) {
				String key = (String) listKeys.nextElement();
				query.setString(key, String.valueOf(params.get(key)));
			}
			return query.scroll();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	public int getSize(String queryName, Map params) throws TechnicalException {

		try {

			// Query query =
			// getCurrentSessionFactory().createQuery(hqlQuery);
			Query query = getCurrentSessionFactory().getNamedQuery(queryName);
			Set keys = params.keySet();
			Iterator it = keys.iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				query.setParameter(key, params.get(key));
			}

			return ((Integer) query.uniqueResult()).intValue();

			// return ( (Integer) query.iterate().next() ).intValue();

		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

	/**
	 * (non javadoc) See @see ma.iam.geve.dao.PersistenceManager#getConnection()
	 * 
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("deprecation")
	public Connection getConnection() throws TechnicalException {

		try {
			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) ((SessionImpl) getCurrentSessionFactory())
					.getSessionFactory();
			
			ConnectionProvider connectionProvider = sessionFactoryImplementation
					.getConnectionProvider();
			return connectionProvider.getConnection();
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		}

	}

	/*
	 * f.tatbi fonction ajouter pour contourner PB de {No Session found for
	 * current thread}
	 */
	private Session getCurrentSessionFactory() {

		try {
			return getSessionFactory().getCurrentSession();
		} catch (Exception e) {
			return getSessionFactory().openSession();
		}
	}
}
