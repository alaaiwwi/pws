package ma.iam.mutualisation.fwk.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.jaxws.context.WrappedMessageContext;
import org.apache.cxf.message.Message;
import org.apache.wss4j.dom.WSConstants;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SecurityUtils {

	public static Element getUsernameTokenHeader(WebServiceContext context) {
		MessageContext messageContext = context.getMessageContext();
		List<Header> headers = null;
		if (messageContext != null
				&& (messageContext instanceof WrappedMessageContext)) {
			Message message = ((WrappedMessageContext) messageContext)
					.getWrappedMessage();
			headers = CastUtils.cast((List<?>) message.get(Header.HEADER_LIST));
		}
		if (headers != null) {
			for (Header h : headers) {
				QName n = h.getName();
				if (n.getLocalPart().equals("Security")
						&& (n.getNamespaceURI().equals(WSConstants.WSSE_NS) || n
								.getNamespaceURI()
								.equals(WSConstants.WSSE11_NS))) {
					Element elm = (Element) h.getObject();
					NodeList nodeList = elm
							.getElementsByTagName("wsse:UsernameToken");
					return (Element) nodeList.item(0);
				}
			}

		}
		return null;
	}

	public static Element getTimestampHeader(WebServiceContext context) {
		MessageContext messageContext = context.getMessageContext();
		List<Header> headers = null;
		if (messageContext != null
				&& (messageContext instanceof WrappedMessageContext)) {
			Message message = ((WrappedMessageContext) messageContext)
					.getWrappedMessage();
			headers = CastUtils.cast((List<?>) message.get(Header.HEADER_LIST));
		}
		if (headers != null) {
			for (Header h : headers) {
				QName n = h.getName();
				if (n.getLocalPart().equals("Security")
						&& (n.getNamespaceURI().equals(WSConstants.WSSE_NS) || n
								.getNamespaceURI()
								.equals(WSConstants.WSSE11_NS))) {
					Element elm = (Element) h.getObject();
					NodeList nodeList = elm
							.getElementsByTagName("wsu:Timestamp");
					return (Element) nodeList.item(0);
				}
			}

		}
		return null;
	}

	public static String getUsername(Element element) {
		NodeList nodeList = element.getElementsByTagName("wsse:Username");
		return nodeList.item(0).getNodeValue();
	}

	public static String getPassword(Element element) {
		NodeList nodeList = element.getElementsByTagName("wsse:Password");
		return nodeList.item(0).getNodeValue();
	}

	public static String getNonce(Element element) {
		NodeList nodeList = element.getElementsByTagName("wsse:Nonce");
		return nodeList.item(0).getNodeValue();
	}

	public static String getCreated(Element element) {
		NodeList nodeList = element.getElementsByTagName("wsu:Created");
		return nodeList.item(0).getNodeValue();
	}

	public static String getCreatedTimestamp(WebServiceContext wsContext) {
		return getCreated(getTimestampHeader(wsContext));
	}

	public static String encodePasswordDigest(String nonce, String timestamp,
			String password) throws TechnicalException {
		try {
			String toHash = new String(Base64.decode(nonce), "UTF-8")
					+ timestamp + password;
			byte[] hash = MessageDigest.getInstance("SHA1").digest(
					toHash.getBytes("UTF-8"));
			return Base64.encode(hash);
		} catch (NoSuchAlgorithmException e) {
			throw new TechnicalException(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			throw new TechnicalException(e.getMessage(), e);
		} catch (Base64DecodingException e) {
			throw new TechnicalException(e.getMessage(), e);
		}
	}

}
