package ma.iam.mutualisation.fwk.common.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ma.iam.mutualisation.fwk.common.Constants;

/**
* The Class Utils : Classe Utilitaire.
*
* @author Atos Origin
*/
public final class Utils {

	/**
	 * Instantiates a new utils.
	 */
	private Utils() {

	}
	
	/**
	 * Null to blanc.
	 * 
	 * @param str
	 *            the str
	 * @return the string
	 */
	public static String nullToBlanc(String str) {
		return str == null ? "" : str.trim();
	}

	/**
	 * check if a string is empty.
	 * 
	 * @param s the s
	 * @return true, if checks if is empty
	 */
	public static boolean isEmpty(String s) {
		return s == null || s.equals("");
	} 

	/**
	 * Checks if is numeric.
	 * 
	 * @param s the s
	 * @return true, if is numeric
	 */
	public static boolean isNumeric(String s) {
		try {
			Long.parseLong(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Date to string.
	 * 
	 * @param date the date
	 * @return the string
	 */
	public static String dateToString(Date date) {
		return dateToString(date, Constants.FORMAT_DATE);
	}

	/**
	 * Date to string.
	 * 
	 * @param date the date
	 * @param dateFormat the date format
	 * @return the string
	 */
	public static String dateToString(Date date, String dateFormat) {

		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
			return simpleDateFormat.format(date);
		} catch (Exception e) {
			return null;
		}

	}
	
	/**
	 * Method that convert a String to Date Object according to default format.
	 * 
	 * @param sDate the s date
	 * @return Date
	 */
	public static Date stringToDate(String sDate) {
		return stringToDate(sDate, Constants.FORMAT_DATE);
	}

	/**
	 * Method that convert a String to Date Object according to default format.
	 * 
	 * @param sDate the s date
	 * @param format the format
	 * @return Date
	 */
	public static Date stringToDate(String sDate, String format) {

		Date date = null;
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		try {
			date = formatter.parse(sDate);
		} catch (Exception e) {
			return null;
		}
		return date;
	}
	
	/**
	 * Arrondir un nombre.
	 * 
	 * @param d : nombre à arrondir
	 * @param n : nombre de digits après la virgule
	 * @return the double
	 */
	public static Double round(double d, int n) {
		double result = d;
		int p = (int) Math.pow(10, n);
		result *= p;
		result = Math.round(result);
		result /= p;
		return result;
	}

	/**
	 * Round.
	 * 
	 * @param d the d
	 * @param n the n
	 * @return the float
	 */
	public static Float round(float d, int n) {
		float result = d;
		int p = (int) Math.pow(10, n);
		result *= p;
		result = Math.round(result);
		result /= p;
		return result;
	}
	
	/**
	 * Cette méthode permet d'arrondir un double.</br></br>
	 * @param d
	 * @return
	 */
	public static Double roundDown(Double d) {
		Double tmp = d*100;
		if (tmp.longValue() < tmp) {
			BigDecimal b = new BigDecimal(d);
			return b.setScale(2,BigDecimal.ROUND_DOWN).doubleValue();
		}
		return d;
	}
}
