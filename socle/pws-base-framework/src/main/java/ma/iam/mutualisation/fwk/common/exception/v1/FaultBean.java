/*
 * @author AtoS
 */
package ma.iam.mutualisation.fwk.common.exception.v1;

import java.io.Serializable;

/**
 * The Class FaultBean.
 */
public class FaultBean implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5083404358647336446L;

    /** The context. */
    private String context;

    /** The code. */
    private String code;

    /** The message. */
    private Object message;

    /** The text. */
    private String text;

    /**
     * Instantiates a new fault bean.
     */
    public FaultBean() {
    }

    /**
     * Instantiates a new fault bean.
     * 
     * @param context
     *            the context
     * @param code
     *            the code
     * @param message
     *            the message
     * @param text
     *            the text
     */
    public FaultBean(final String context, final String code, final Object message, final String text) {
        this.context = context;
        this.code = code;
        this.message = message;
        this.text = text;
    }

    /**
     * Instantiates a new fault bean.
     * 
     * @param context
     *            the context
     * @param code
     *            the code
     * @param message
     *            the message
     */
    public FaultBean(final String context, final String code, final Object message) {
        this.context = context;
        this.code = code;
        this.message = message;
    }

    /**
     * Instantiates a new fault bean.
     * 
     * @param context
     *            the context
     * @param code
     *            the code
     */
    public FaultBean(final String context, final String code) {
        this.context = context;
        this.code = code;
    }

    /**
     * Gets the context.
     * 
     * @return the context
     */
    public String getContext() {
        return context;
    }

    /**
     * Sets the context.
     * 
     * @param context
     *            the new context
     */
    public void setContext(final String context) {
        this.context = context;
    }

    /**
     * Gets the code.
     * 
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     * 
     * @param code
     *            the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the text.
     * 
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text.
     * 
     * @param text
     *            the new text
     */
    public void setText(final String text) {
        this.text = text;
    }

    /**
     * Sets the message.
     * 
     * @param message
     *            the new message
     */
    public void setMessage(final Object message) {
        this.message = message;
    }

    /**
     * Gets the message.
     * 
     * @return the message
     */
    public Object getMessage() {
        return message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff = buff.append(" Context [").append(context).append("]");
        buff = buff.append(" Code [").append(code).append("]");
        buff = buff.append(" Message [").append(message).append("]");
        buff = buff.append(" Text [").append(text).append("]");
        return buff.toString();
    }

}
