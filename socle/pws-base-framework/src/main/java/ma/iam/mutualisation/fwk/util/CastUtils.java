package ma.iam.mutualisation.fwk.util;

import java.text.ParseException;
import java.util.Date;

import com.lhs.ccb.soi.types.MoneyI;

public class CastUtils {

	public static final Integer getInteger(Object obj) {
		Integer ret;
		if (obj == null)
			return null;
		if (obj instanceof Integer)
			return (Integer) obj;
		String value = String.valueOf(obj);
		ret = Integer.valueOf(value);
		return ret;

	}

	public static final String getString(Object obj) {
		String ret;
		if (obj == null)
			return null;
		if (obj instanceof String)
			return (String) obj;
		ret = String.valueOf(obj);
		return ret;

	}

	public static final Long getLong(Object obj) {
		Long ret;
		if (obj == null)
			return null;
		if (obj instanceof Long)
			return (Long) obj;
		String value = String.valueOf(obj);
		ret = Long.valueOf(value);
		return ret;

	}

	public static Date getDate(Object obj) {
		String ret;
		if (obj == null)
			return null;
		try {
			if (obj instanceof String)
				return DateUtil.parse((String) obj);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return null;

	}

	public static final Double getDouble(Object obj) {
		Double ret;
		if (obj == null)
			return null;
		if (obj instanceof Double)
			return (Double) obj;
		if (obj instanceof MoneyI) {
			MoneyI i = (MoneyI) obj;
			return i.amount;
		}
		String value = String.valueOf(obj);
		ret = Double.valueOf(value);
		return ret;

	}

	public static final Boolean getBoolean(Object obj) {
		Boolean ret = Boolean.FALSE;
		if (obj == null)
			return null;
		if (obj instanceof Boolean)
			return (Boolean) obj;
		String value = String.valueOf(obj);

		if (value != null && "TRUE".equals(value.toUpperCase()))
			return Boolean.TRUE;
		return ret;

	}
}
