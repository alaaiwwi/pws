/*
 * @author AtoS
 */
package ma.iam.mutualisation.fwk.common.exception.v1;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.ws.WebFault;

import ma.iam.mutualisation.fwk.common.exception.ExceptionCodeTypeEnum;

/**
 * The Class FunctionnalException.
 */
@WebFault(name = "FunctionnalException", faultBean = "ma.iam.mob.exceptions.FaultBean")
public class FunctionnalException extends Exception {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -11270732659714459L;

	/** The fault bean. */
	private final FaultBean faultBean;

	/**
	 * .
	 * 
	 * @param exceptionCodeTypeEnum
	 *            the exception code type enum
	 * @param cause
	 *            the cause
	 */
	public FunctionnalException(
			final ExceptionCodeTypeEnum exceptionCodeTypeEnum,
			final Throwable cause) {
		super(cause);
		final StringBuilder result = new StringBuilder();
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw);
		cause.printStackTrace(pw);
		result.append(sw.toString());
		this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(),
				exceptionCodeTypeEnum.getErrorCode());
		this.faultBean.setText(result.toString());
	}

	/**
	 * 
	 * 
	 * @param exceptionCodeTypeEnum
	 *            the exception code type enum
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public FunctionnalException(
			final ExceptionCodeTypeEnum exceptionCodeTypeEnum,
			final Object message, final Throwable cause) {
		super(cause);
		final StringBuilder result = new StringBuilder();
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw);
		cause.printStackTrace(pw);
		result.append(sw.toString());
		this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(),
				exceptionCodeTypeEnum.getErrorCode(), message);
		this.faultBean.setText(result.toString());
	}

	/**
	 * .
	 * 
	 * @param exceptionCodeTypeEnum
	 *            the exception code type enum
	 */
	public FunctionnalException(
			final ExceptionCodeTypeEnum exceptionCodeTypeEnum) {
		this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(),
				exceptionCodeTypeEnum.getErrorCode());
	}

	/**
	 * 
	 * @param exceptionCodeTypeEnum
	 *            the exception code type enum
	 * @param message
	 *            the message
	 */
	public FunctionnalException(
			final ExceptionCodeTypeEnum exceptionCodeTypeEnum,
			final Object message) {
		this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(),
				exceptionCodeTypeEnum.getErrorCode(), message);
	}

	/**
	 * 
	 * @param exceptionCodeTypeEnum
	 *            the exception code type enum
	 * @param message
	 *            the message
	 */
	public FunctionnalException(final String errorCode,
			final Object errorContext, final Object message) {
		this.faultBean = new FaultBean((String) errorContext, errorCode, message);
	}



	/**
	 * Gets the fault info.
	 * 
	 * @return the fault info
	 */
	public FaultBean getFaultInfo() {
		return faultBean;
	}

}
