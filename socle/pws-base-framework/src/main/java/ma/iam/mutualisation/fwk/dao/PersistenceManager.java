package ma.iam.mutualisation.fwk.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;

/**
 * The Interface PersistenceManager : Services de Persistance
 * 
 * @author Atos Origin
 * @version 1.0
 */
public interface PersistenceManager {

	/**
	 * Find by id.
	 * 
	 * @param classe the classe
	 * @param id the id
	 * @param lock the lock
	 * @return the object
	 * @throws TechnicalException the technical exception
	 */
	Object findById(Class classe, Serializable id, boolean lock) throws TechnicalException;
	/**
	 * Find all.
	 * 
	 * @param classe the classe
	 * @param cacheable TODO
	 * @return the list
	 * @throws TechnicalException the technical exception
	 */
	List findAll(Class classe, boolean cacheable) throws TechnicalException;

	/**
	 * sauvegarder ou mettre a jours.
	 * 
	 * @param entity the entity
	 * @throws TechnicalException the technical exception
	 */
	void save(Object entity) throws TechnicalException;

	/**
	 * Supprimer.
	 * 
	 * @param entity the entity
	 * @throws TechnicalException the technical exception
	 */
	void delete(Object entity) throws TechnicalException;

	/**
	 * Supprimer.
	 * 
	 * @param classe the classe
	 * @param id the id
	 * @throws TechnicalException the technical exception
	 */
	void delete(Class classe, Serializable id) throws TechnicalException;

	/**
	 * 
	 * @param requete
	 * @param alias
	 * @param classe
	 * @return
	 * @throws TechnicalException
	 */
	List findBySQLQuery(String requete, String alias, Class classe) throws TechnicalException;
	
	/**
	 * Cette méthode permet d'executer une requête HQL.</br></br>
	 * @param hqlQuery requête HQL
	 * @return
	 * @throws TechnicalException
	 */
	List findByQuery(String hqlQuery) throws TechnicalException;
	
	/**
	 * Cette méthode permet d'executer une requête HQL prenant en compte les paramètres.</br></br>
	 * @param hqlQuery requête HQL
	 * @param parameters paramètres
	 * @return
	 * @throws TechnicalException
	 */
	List findByQuery(String hqlQuery, Map<String, Object> parameters) throws TechnicalException;

	/**
	 * Find by criteria.
	 * 
	 * @param classe the classe
	 * @param criteriasList the criterias list
	 * @param ordersList the orders list
	 * @param cacheable TODO
	 * @return the list
	 * @throws TechnicalException the technical exception
	 */
	List findByCriteria(Class classe, List criteriasList, List ordersList, boolean cacheable) throws TechnicalException;
	
	/**
	 * Find by criteria.
	 * 
	 * @param classe the classe
	 * @param criteriasList the criterias list
	 * @param cacheable
	 * @return the list
	 * @throws TechnicalException the technical exception
	 */
	List findByCriteria(Class classe, List criteriasList, boolean cacheable) throws TechnicalException;

	/**
	 * Gets the named query.
	 * 
	 * @param queryName the query name
	 * @param params the params
	 * @return the named query
	 * @throws TechnicalException the technical exception
	 */
	List getNamedQuery(String queryName, Map<String, Object> params) throws TechnicalException;
	
	/**
	 * Créer un objet Criteria.
	 * 
	 * @param classe the classe
	 * @return Criteria
	 * @throws TechnicalException the technical exception
	 */
	Criteria createCriteria(Class classe) throws TechnicalException;
	
	/**
	 * execute une requ�te update
	 * @param sqlQuery
	 * @param params
	 * @throws TechnicalException
	 */
	int executeUpdateQuery(String sqlQuery, List params) throws TechnicalException;
	
	
	public List findByExample(Class classe,Object exampleInstance, String[] excludeProperty) throws TechnicalException;
	
	
	public ScrollableResults getQueryResult(String queryName, Hashtable params) throws TechnicalException;
	
	public int  getSize(String queryName, Map params) throws TechnicalException;
	
	/**
	 * Récuperer une connexion JDBC liée à la transaction en cours .</br></br>
	 * Created on Sprint #
	 * @return Connection
	 * @throws TechnicalException
	 */
	public Connection getConnection() throws TechnicalException;

}
