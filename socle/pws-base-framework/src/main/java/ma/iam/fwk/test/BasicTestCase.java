package ma.iam.fwk.test;


import java.util.List;
import java.util.Properties;



import junit.framework.TestCase;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public abstract class BasicTestCase extends TestCase {


	protected ApplicationContext context;

	protected Properties properties = new Properties();

	/**
	 * Charge le contexte Spring à partir du fichier passé en paramètre
	 * 
	 * @param springContextFiles
	 *            la liste des noms des fichiers de contexte Spring
	 */
	protected void loadSpringContext(List<String> springContextFiles) {
		String[] contexts = springContextFiles
				.toArray(new String[springContextFiles.size()]);
		this.context = new ClassPathXmlApplicationContext(contexts, true);
	}

	/**
	 * Raccourci de récupération d'un bean du contexte Spring
	 * 
	 * @param beanName
	 *            identifiant du bean Spring
	 * @return le bean Spring si trouvé
	 */
	protected Object getBean(String beanName) {
		return this.context.getBean(beanName);
	}

	/**
	 * Initialisation des données nécessaires au test
	 * 
	 * @throws Exception
	 *             si l'initialisation échoue
	 */
	protected void setUp() throws Exception {
		this.loadSpringContext(this.getSpringContextUri());

		if (context == null) {
			throw new RuntimeException(
					"Le contexte Spring doit être initialisé via loadSpringContext");
		}
	}

	/**
	 * Permet de définir les fichiers de configuration Spring au niveau du
	 * ClassPath
	 * 
	 * @return la liste des noms des fichiers de contexte Spring
	 */
	protected abstract List<String> getSpringContextUri();

	public Properties getProperties() {
		return properties;
	}
}