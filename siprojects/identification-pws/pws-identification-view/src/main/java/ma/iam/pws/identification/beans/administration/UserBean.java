 package ma.iam.pws.identification.beans.administration;
 
 import java.util.Date;
 import java.util.List;
 import javax.xml.bind.annotation.XmlAccessType;
 import javax.xml.bind.annotation.XmlAccessorType;
 import javax.xml.bind.annotation.XmlElement;
 
 
 @XmlAccessorType(XmlAccessType.NONE)
 public class UserBean
 {
   public static final String ALIAS = "User";
   @XmlElement
   private long costcenter_id;
   private String description;
   private String group;
   @XmlElement
   private boolean is_batch;
   @XmlElement
   private long lng_code;
   private String lng_shdes;
   @XmlElement
   private String name;
   @XmlElement
   private String password;
   private List<String> rights;
   @XmlElement
   private Date password_expires;
   @XmlElement
   private long pwd_remind_days;
   private String user_type;
   
   public List<String> getRights()
   {
/*  36 */     return this.rights;
   }
   
/*  39 */   public void setRights(List<String> rights) { this.rights = rights; }
   
   public String getPassword() {
/*  42 */     return this.password;
   }
   
/*  45 */   public void setPassword(String password) { this.password = password; }
   
   public long getCostcenter_id() {
/*  48 */     return this.costcenter_id;
   }
   
/*  51 */   public void setCostcenter_id(long costcenter_id) { this.costcenter_id = costcenter_id; }
   
   public String getDescription() {
/*  54 */     return this.description;
   }
   
/*  57 */   public void setDescription(String description) { this.description = description; }
   
   public String getGroup() {
/*  60 */     return this.group;
   }
   
/*  63 */   public void setGroup(String group) { this.group = group; }
   
   public boolean isIs_batch() {
/*  66 */     return this.is_batch;
   }
   
   public void setIs_batch(boolean is_batch) { this.is_batch = is_batch; }
   
   public long getLng_code() {
    return this.lng_code;
   }
   
/*  75 */   public void setLng_code(long lng_code) { this.lng_code = lng_code; }
   
   public String getLng_shdes() {
/*  78 */     return this.lng_shdes;
   }
   
/*  81 */   public void setLng_shdes(String lng_shdes) { this.lng_shdes = lng_shdes; }
   
   public String getName() {
/*  84 */     return this.name;
   }
   
/*  87 */   public void setName(String name) { this.name = name; }
   
   public Date getPassword_expires() {
/*  90 */     return this.password_expires;
   }
   
/*  93 */   public void setPassword_expires(Date password_expires) { this.password_expires = password_expires; }
   
   public long getPwd_remind_days() {
/*  96 */     return this.pwd_remind_days;
   }
   
/*  99 */   public void setPwd_remind_days(long pwd_remind_days) { this.pwd_remind_days = pwd_remind_days; }
   
   public String getUser_type() {
/* 102 */     return this.user_type;
   }
   
/* 105 */   public void setUser_type(String user_type) { this.user_type = user_type; }
 }

