package ma.iam.pws.identification.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import ma.iam.pws.identification.beans.contract.ContractBean;
import ma.iam.pws.identification.beans.services.ServiceBean;
import ma.iam.pws.identification.dao.InfoContratsDAO;
import ma.iam.pws.identification.exception.FaultWS;
import ma.iam.pws.identification.exception.TechnicalException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class InfoContractsDAOImpl implements InfoContratsDAO {
	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("queries")
	private Properties queries;
	@Autowired
	@Qualifier("msgpropertiesBean")
	private Properties msgProperties;
	/* 53 */private static Log log = LogFactory
			.getLog(InfoContractsDAOImpl.class);

	public ContractBean getContractByMsisdn(String msisdn) throws FaultWS {
		/* 61 */ContractBean contract = null;
		try {
			/* 64 */log.info("Operation : - getContractByMsisdn checking : "
					+ msisdn);

			/* 66 */int check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkMsisdn"),
					new Object[] { msisdn });

			/* 69 */if (check > 0) {
				/* 71 */log
						.info("Operation : - getContractByMsisdn getting contract for : "
								+ msisdn);

				/* 73 */contract = (ContractBean) this.jdbcTemplate
						.queryForObject(this.queries
								.getProperty("getActifcontractsByMSISDN"),
								new Object[] { msisdn }, new RowMapper() {
									public ContractBean mapRow(
											final ResultSet rs, final int rowNum)
											throws SQLException {
										ContractBean ctrBean = new ContractBean();
										ctrBean.setCo_Id(rs.getLong(1));
										ctrBean.setMsisdn(rs.getString(2));
										ctrBean.setCustCode(rs.getString(3));
										ctrBean.setStatus(rs.getLong(4));
										ctrBean.setCustomerId(rs.getLong(5));
										ctrBean.setCustnum(rs.getString(6));
										ctrBean.setTmCode(rs.getLong(7));
										ctrBean.setCo_Code(rs.getString(8));
										ctrBean.setPrgcode(rs.getLong(9));
										ctrBean.setRpShdes(rs.getString(10));
										return ctrBean;
									}
								});

			} else {

				/* 94 */log
						.info("Operation : - getContractByMsisdn Exception : MSISDN_NOT_FOUND "
								+ msisdn);

				/* 96 */throw new FaultWS("MT0043", this.msgProperties);
			}
		} catch (EmptyResultDataAccessException dex) {
			/* 99 */log
					.info("Operation : - getContractByMsisdn Exception : MSISDN_INACTIFCONTRACT "
							+ msisdn);

			/* 101 */throw new FaultWS("MT0044", this.msgProperties);
		} catch (Exception e) {
			/* 103 */if ((e instanceof FaultWS)) {
				/* 104 */throw ((FaultWS) e);
			}
			/* 106 */log.info(e.getLocalizedMessage());
			/* 107 */throw new FaultWS("MT9999", this.msgProperties);
		}

		/* 110 */return contract;
	}

	public ServiceBean getServiceBean(long coId, String shdes) throws FaultWS {
		/* 114 */ServiceBean svcBean = null;
		try {
			/* 116 */log.info("Operation : - getServiceBean received : " + coId
					+ " shdes " + shdes);
			/* 117 */svcBean = (ServiceBean) this.jdbcTemplate.queryForObject(
					this.queries.getProperty("getServiceStatusInContract"),
					new Object[] { Long.valueOf(coId), shdes },
					new RowMapper() {
						public ServiceBean mapRow(final ResultSet rs,
								final int rowNum) throws SQLException {
							ServiceBean svcBean = new ServiceBean();
							svcBean.setCoId(rs.getLong(1));
							svcBean.setShdes(rs.getString(2));
							svcBean.setStatus(rs.getLong(3));
							return svcBean;
						}
					});

		} catch (EmptyResultDataAccessException dex) {

			/* 132 */log
					.info("Operation : - getServiceBean Exception : SERVICE_NOT_FOUND"
							+ coId + " shdes " + shdes);
			/* 133 */throw new FaultWS("MT0047", this.msgProperties);
		} catch (Exception e) {
			/* 135 */if ((e instanceof FaultWS)) {
				/* 136 */throw ((FaultWS) e);
			}
			/* 138 */log.info(e.getLocalizedMessage());
			/* 139 */throw new FaultWS("MT9999", this.msgProperties);
		}

		/* 142 */return svcBean;
	}

	public boolean checkInternetDebit(long coId, String shdes) throws FaultWS {
		try {
			/* 148 */log.info("Operation : - checkInternetDebit checking : "
					+ coId + " shdes " + shdes);
			/* 149 */int check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkInternetdebit"),
					new Object[] { Long.valueOf(coId), shdes });

			/* 152 */if (check != 0)
				return true;
			/* 153 */return false;
		} catch (EmptyResultDataAccessException dex) {
			/* 155 */log
					.info("Operation : - getServiceBean Exception : SERVICE_NOT_FOUND"
							+ coId + " shdes " + shdes);
			/* 156 */throw new FaultWS("MT0047", this.msgProperties);
		} catch (Exception e) {
			/* 158 */if ((e instanceof FaultWS)) {
				/* 159 */throw ((FaultWS) e);
			}
			/* 161 */log.info(e.getLocalizedMessage());
			/* 162 */throw new FaultWS("MT9999", this.msgProperties);
		}
	}

	public ContractBean getContractByMsisdnAndStatus(String msisdn,
			String status) throws FaultWS {
		/* 169 */ContractBean contract = null;
		try {
			log.info("Operation : - getContractByMsisdnAndStatus checking : "
					+ msisdn);

			int check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkMsisdn"),
					new Object[] { msisdn });

			if (check > 0) {
				log.info("Operation : - getContractByMsisdnAndStatus getting contract for : "
						+ msisdn);

				contract = (ContractBean) this.jdbcTemplate.queryForObject(
						this.queries
								.getProperty("getContractsByMSISDNAndStatus"),
						new Object[] { msisdn, status }, new RowMapper() {
							public ContractBean mapRow(final ResultSet rs,
									final int rowNum) throws SQLException {
								ContractBean ctrBean = new ContractBean();
								ctrBean.setCo_Id(rs.getLong(1));
								ctrBean.setMsisdn(rs.getString(2));
								ctrBean.setCustCode(rs.getString(3));
								ctrBean.setStatus(rs.getLong(4));
								ctrBean.setCustomerId(rs.getLong(5));
								ctrBean.setCustnum(rs.getString(6));
								ctrBean.setTmCode(rs.getLong(7));
								ctrBean.setCo_Code(rs.getString(8));
								ctrBean.setPrgcode(rs.getLong(9));
								ctrBean.setRpShdes(rs.getString(10));
								return ctrBean;
							}
						});

			} else {

				/* 202 */log
						.info("Operation : - getContractByMsisdnAndStatus Exception : MSISDN_NOT_FOUND "
								+ msisdn);

				/* 204 */throw new FaultWS("MT0043", this.msgProperties);
			}
		} catch (EmptyResultDataAccessException dex) {
			/* 207 */log
					.info("Operation : - getContractByMsisdnAndStatus Exception : NO FOUND Contracts "
							+ msisdn);

			/* 209 */return null;
		} catch (Exception e) {
			/* 211 */if ((e instanceof FaultWS)) {
				/* 212 */throw ((FaultWS) e);
			}
			/* 214 */log.info(e.getLocalizedMessage());
			/* 215 */throw new FaultWS("MT9999", this.msgProperties);
		}

		/* 218 */return contract;
	}

	public int checkIdentity(String msisdn) throws FaultWS, TechnicalException {
		try {
			/* 232 */log.info("Operation : - checkIdentity for msisdn : "
					+ msisdn);

			/* 234 */long check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkMsisdn"),
					new Object[] { msisdn });

			/* 237 */if (check > 0L) {
				/* 238 */check = this.jdbcTemplate.queryForLong(
						this.queries.getProperty("checkprepaidNd"),
						new Object[] { msisdn });

				/* 240 */if (check > 0L) {
					/* 241 */check = this.jdbcTemplate.queryForInt(
							this.queries.getProperty("checkIdentity"),
							new Object[] { Long.valueOf(check) });

					/* 243 */if (check > 0L) {
						/* 244 */return 0;
					}
					/* 246 */return 1;
				}

				/* 249 */log
						.info("Operation : - checkIdentity : MSISDN_NOT_PREPAID "
								+ msisdn);
				/* 250 */return 2;
			}

			/* 253 */log.info("Operation : - checkIdentity : MSISDN_NOT_FOUND "
					+ msisdn);
			/* 254 */return 3;
		} catch (EmptyResultDataAccessException dx) {
			/* 257 */return 2;
		} catch (Exception e) {
			/* 259 */if ((e instanceof FaultWS)) {
				/* 260 */throw ((FaultWS) e);
			}
			/* 262 */log.info(e.getLocalizedMessage());
			/* 263 */throw new ma.iam.pws.identification.exception.TechnicalException("MT9999", this.msgProperties);
		}
	}
}
