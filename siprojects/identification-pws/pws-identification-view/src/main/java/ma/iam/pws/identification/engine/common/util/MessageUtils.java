 package ma.iam.pws.identification.engine.common.util;
 
 import java.text.MessageFormat;
import java.util.ResourceBundle;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.pws.identification.engine.beans.ResultatBean;
import ma.iam.pws.identification.engine.common.Constants;
 
 
 
 
 
 
 
 
 
 public class MessageUtils
 {
/* 20 */   private static final String BUNDLE_NAME = Constants.MESSAGE_FILE_NAME;
   
 
/* 23 */   private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
   
 
 
 
 
 
 
 
 
 
 
 
 
   public static String getMessage(String key)
   {
     try
     {
/* 41 */       return new String(RESOURCE_BUNDLE.getString(key).getBytes("ISO-8859-1"), "UTF-8");
     } catch (Exception e) {
/* 43 */       LogManager.getLogger().error(MessageUtils.class, "getMessage", e.getMessage(), e); }
/* 44 */     return key;
   }
   
 
 
 
 
 
 
   public static String getMessage(String key, Object[] args)
   {
/* 55 */     String message = null;
/* 56 */     String value = getMessage(key);
/* 57 */     message = MessageFormat.format(value, args);
/* 58 */     return message;
   }
   
   public static ResultatBean getErrorMessage() {
/* 62 */     return new ResultatBean(4, getMessage("message.erreur.technique"));
   }
   
   public static ResultatBean getErrorMessage(String key) {
/* 66 */     return new ResultatBean(4, getMessage(key));
   }
   
 
 
 
 
 
   public static ResultatBean getErrorMessage(String key, Object[] args)
   {
/* 76 */     return new ResultatBean(4, getMessage(key, args));
   }
   
   public static ResultatBean getConfirmMessage(String key) {
/* 80 */     return new ResultatBean(1, getMessage(key));
   }
   
   public static ResultatBean getConfirmMessage(String key, Object[] args) {
/* 84 */     return new ResultatBean(1, getMessage(key, args));
   }
   
   public static ResultatBean getInfoMessage(String key) {
/* 88 */     return new ResultatBean(2, getMessage(key));
   }
   
   public static ResultatBean getInfoMessage(String key, Object[] args) {
/* 92 */     return new ResultatBean(2, getMessage(key, args));
   }
   
   public static ResultatBean getHabilitaionMessage(String key) {
/* 96 */     return new ResultatBean(3, getMessage(key));
   }
 }

