package ma.iam.pws.identification.domaine;

import ma.iam.pws.identification.beans.administration.ProfileEligibilityBean;
import ma.iam.pws.identification.exception.FaultWS;

public abstract interface GetProfileEligibilityDomain
{
  public abstract ProfileEligibilityBean getProfileEligibility(String paramString)
    throws FaultWS;
}


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\domaine\GetProfileEligibilityDomain.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */