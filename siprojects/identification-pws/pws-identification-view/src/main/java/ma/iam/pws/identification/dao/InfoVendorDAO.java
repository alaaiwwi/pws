package ma.iam.pws.identification.dao;

import ma.iam.pws.identification.exception.FaultWS;

public abstract interface InfoVendorDAO
{
  public abstract String AuthorizedVendor(String paramString)
    throws FaultWS;
}


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\dao\InfoVendorDAO.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */