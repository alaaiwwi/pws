package ma.iam.pws.identification.dao;

import ma.iam.pws.identification.beans.contract.ContractBean;
import ma.iam.pws.identification.beans.services.ServiceBean;
import ma.iam.pws.identification.exception.FaultWS;
import ma.iam.pws.identification.exception.TechnicalException;

public abstract interface InfoContratsDAO
{
  public abstract ContractBean getContractByMsisdn(String paramString)
    throws FaultWS;
  
  public abstract ServiceBean getServiceBean(long paramLong, String paramString)
    throws FaultWS;
  
  public abstract boolean checkInternetDebit(long paramLong, String paramString)
    throws FaultWS;
  
  public abstract ContractBean getContractByMsisdnAndStatus(String paramString1, String paramString2)
    throws FaultWS;
  
  public abstract int checkIdentity(String paramString)
    throws FaultWS, TechnicalException;
}


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\dao\InfoContratsDAO.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */