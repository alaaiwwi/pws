package ma.iam.pws.identification.webservice.identity;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.pws.identification.exception.FaultWS;
import ma.iam.pws.identification.exception.TechnicalException;

@WebService(targetNamespace = "http://webservice.iam.ma/Identityws")
public  interface IdentityWS {
	@WebMethod(operationName = "checkIdentity")
	@WebResult(partName = "result")
	public  int checkIdentity(
			@WebParam(name = "msisdn", mode = WebParam.Mode.IN) @XmlElement(required = true) String paramString)
			throws FaultWS, TechnicalException;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public Integer byPass();
}
