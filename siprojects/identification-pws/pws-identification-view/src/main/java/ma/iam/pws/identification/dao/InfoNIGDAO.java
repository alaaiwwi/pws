package ma.iam.pws.identification.dao;

import java.util.ArrayList;

import ma.iam.pws.identification.beans.nig.NIG;
import ma.iam.pws.identification.beans.nig.NIG_ELIGIBILITY;
import ma.iam.pws.identification.beans.nig.NIG_STATUS;
import ma.iam.pws.identification.exception.FaultWS;

public abstract interface InfoNIGDAO
{
  public abstract NIG_ELIGIBILITY getNigEligibility(String paramString)
    throws FaultWS;
  
  public abstract NIG_STATUS getNigStatus(String paramString)
    throws FaultWS;
  
  public abstract ArrayList<NIG> getNigMicrocell(String paramString)
    throws FaultWS;
}


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\dao\InfoNIGDAO.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */