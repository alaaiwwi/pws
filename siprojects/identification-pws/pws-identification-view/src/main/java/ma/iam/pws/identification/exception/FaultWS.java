package ma.iam.pws.identification.exception;

import java.io.Serializable;
import java.util.Properties;

import ma.iam.pws.identification.common.Message;


public class FaultWS extends Exception implements Serializable {

	private static final long serialVersionUID = 1L;


	private String codefault;


	private String descfault;

	
	public String getCodefault() {
		return codefault;
	}


	public void setCodefault(String codefault) {
		this.codefault = codefault;
	}

	
	public String getDescfault() {
		return descfault;
	}


	public void setDescfault(String descfault) {
		this.descfault = descfault;
	}

	    
	    public FaultWS(String message, String codefault, String descfault,
	            Throwable cause) {
	        this.codefault = codefault;
	        this.descfault=descfault;
	    }

	    public FaultWS(String message, String codefault, String descfault) {
	        super(message);
	        this.codefault = codefault;
	        this.descfault=descfault;
	    }
	    public FaultWS( String codefault, String descfault) {
	        super(descfault);
	        this.codefault = codefault;
	        this.descfault=descfault;
	    }
	    public FaultWS( String codefault , Properties msgPr) {
	    	 super(Message.getMessage(codefault,msgPr));
	    	 this.descfault=Message.getMessage(codefault,msgPr);
	    	 this.codefault = codefault;
	       
	    }
	    
	    public FaultWS(  Object obj, String codefault ,Properties msgPr) {
	    	 super(Message.getMessage(obj, codefault , msgPr));
	    	 this.descfault=Message.getMessage(obj ,codefault, msgPr);
	    	 this.codefault = codefault;
	       
	    }
	    
	    
//	    public FaultWS getFaultInfo() {
//	        return  super(this.codefault, this.descfault) ;
//	    }

}
