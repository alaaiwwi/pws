package ma.iam.pws.identification.webservice.identity.impl;

import java.util.Properties;

import javax.jws.WebService;

import ma.iam.pws.identification.dao.InfoContratsDAO;
import ma.iam.pws.identification.exception.FaultWS;
import ma.iam.pws.identification.exception.TechnicalException;
import ma.iam.pws.identification.webservice.identity.IdentityWS;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@WebService(portName = "IdentityWSPort", serviceName = "IdentityWSService", targetNamespace = "http://webservice.iam.ma/Identityws", endpointInterface = "ma.iam.pws.identification.webservice.identity.IdentityWS")
public class IdentityWSServiceImpl implements IdentityWS {
	private static final String msisdnformatRegEx = "212[0-9]{1,9}$";
	@Autowired
	private InfoContratsDAO infoContratsDAO;
	@Autowired
	@Qualifier("msgpropertiesBean")
	Properties msgProperties;

	/**
	 * objet pour le ping sur la base bscs
	 */
	private JDBCPing pingBscs;

	public void setPingBscs(JDBCPing pingBscs) {
		this.pingBscs = pingBscs;
	}

	public int checkIdentity(String msisdn) throws FaultWS, TechnicalException {
		/* 38 */if (msisdn.matches(msisdnformatRegEx)) {
			/* 39 */return this.infoContratsDAO.checkIdentity(msisdn);
		}
		/* 41 */throw new FaultWS("MT0042", this.msgProperties);
	}
	
	@Override
	public Integer byPass() {
		boolean ret1 = pingBscs.check();

		if (ret1 )
			return 1;
		return -1;

	}
}
