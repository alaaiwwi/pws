 package ma.iam.pws.identification.util;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
 
 
 
 
 
 
 public class Constants
 {
   public static final ShaPasswordEncoder ENCODER = new ShaPasswordEncoder();
   
   public static final String VENDOR_EXIST = "ND_VENDOR";
   
   public static final String VENDOR_UNKNOWN_OR_NOT_EXIST = "ND_NON_VENDOR";
   
   public static final String INTERNAL_ERROR = "MT9999";
   public static final String VENDOR_MSISDNFAILED = "MT0034";
   public static final String IMSI_FAILEDFORMAT = "MT0035";
   public static final String IMSI_HOLDCHECK = "MT0036";
   public static final String IMSI_PREPAIDCHECK = "MT0037";
   public static final String IMSI_IDENTITYCHECK = "MT0038";
   public static final String IMSI_TRACECHECK = "MT0039";
   public static final String CIN_FAILEDFORMAT = "MT0040";
   public static final String IMSI_NOTTRACED = "MT0041";
   public static final String MSISDN_FAILEDFORMAT = "MT0042";
   public static final String MSISDN_NOT_FOUND = "MT0043";
   public static final String MSISDN_INACTIFCONTRACT = "MT0044";
   public static final String MSISDN_NOT_ELEGIBLE_TMCODE = "MT0045";
   public static final String MSISDN_NOT_ELEGIBLE_PRGCODE = "MT0046";
   public static final String SERVICE_NOT_FOUND = "MT0047";
   public static final Boolean IDENTITY_INSERTED = new Boolean(true);
   public static final Boolean IDENTITY_NOT_INSERTED = new Boolean(false);
 }
