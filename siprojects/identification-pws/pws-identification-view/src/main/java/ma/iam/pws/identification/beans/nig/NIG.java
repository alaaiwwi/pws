 package ma.iam.pws.identification.beans.nig;
 
 import javax.xml.bind.annotation.XmlRootElement;
 
 
 @XmlRootElement(name="NIG")
 public class NIG
 {
   private String msisdn;
   
   public String getMsisdn()
   {
/* 13 */     return this.msisdn;
   }
   
 
 
   public void setMsisdn(String msisdn)
   {
/* 20 */     this.msisdn = msisdn;
   }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\nig\NIG.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */