 package ma.iam.pws.identification.beans.contract;
 
 import java.util.Date;
 import javax.xml.bind.annotation.XmlType;
 
 
 
 
 
 
 
 @XmlType(name="Contract", propOrder={"CO_ID", "CO_ID_PUB", "CO_STATUS", "CO_PENDING_STATUS", "CO_STATUS_DATE", "CO_PENDING_DATE"})
 public class ContractStatBean
 {
   private long CO_ID;
   private String CO_ID_PUB;
   private long CO_STATUS;
   private Date CO_STATUS_DATE;
   private long CO_PENDING_STATUS;
   private Date CO_PENDING_DATE;
   
   public long getCO_ID()
   {
/* 24 */     return this.CO_ID;
   }
   
/* 27 */   public void setCO_ID(long cO_ID) { this.CO_ID = cO_ID; }
   
   public String getCO_ID_PUB() {
/* 30 */     return this.CO_ID_PUB;
   }
   
/* 33 */   public void setCO_ID_PUB(String cO_ID_PUB) { this.CO_ID_PUB = cO_ID_PUB; }
   
   public long getCO_STATUS() {
/* 36 */     return this.CO_STATUS;
   }
   
/* 39 */   public void setCO_STATUS(long cO_STATUS) { this.CO_STATUS = cO_STATUS; }
   
   public Date getCO_STATUS_DATE() {
/* 42 */     return this.CO_STATUS_DATE;
   }
   
/* 45 */   public void setCO_STATUS_DATE(Date cO_STATUS_DATE) { this.CO_STATUS_DATE = cO_STATUS_DATE; }
   
   public long getCO_PENDING_STATUS() {
/* 48 */     return this.CO_PENDING_STATUS;
   }
   
/* 51 */   public void setCO_PENDING_STATUS(long cO_PENDING_STATUS) { this.CO_PENDING_STATUS = cO_PENDING_STATUS; }
   
   public Date getCO_PENDING_DATE() {
/* 54 */     return this.CO_PENDING_DATE;
   }
   
/* 57 */   public void setCO_PENDING_DATE(Date cO_PENDING_DATE) { this.CO_PENDING_DATE = cO_PENDING_DATE; }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\contract\ContractStatBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */