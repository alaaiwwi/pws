package ma.iam.pws.identification.dao.nip;

import java.util.Date;

import ma.iam.pws.identification.beans.nip.NIPBean;
import ma.iam.pws.identification.exception.FaultWS;

public abstract interface InfoNIPDAO
{
  public abstract NIPBean canNipBeModified(String paramString)
    throws FaultWS;
  
  public abstract NIPBean canNipBeDeleted(String paramString)
    throws FaultWS;
  
  public abstract NIPBean getNipModificationFee(String paramString, int paramInt);
  
  public abstract NIPBean getMaxNipToChange(String paramString);
  
  public abstract NIPBean saveNipChanges(String paramString1, Date paramDate, String paramString2, String paramString3);
}


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\dao\nip\InfoNIPDAO.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */