/*     */ package ma.iam.pws.identification.beans.nip;
/*     */ 
/*     */ 
/*     */ public class MicroCellBean
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   
/*     */   private int maxFixend;
/*     */   
/*     */   private int maxMobiled;
/*     */   
/*     */   private String ffSHDES;
/*     */   
/*     */   private String ffcode;
/*     */   
/*     */   private String ff_pcode;
/*     */   
/*     */   private String ff_des;
/*     */   
/*     */   private int scale_factor;
/*     */   
/*     */   private String imc_scalefactor_type;
/*     */   
/*     */   private String origin_default;
/*     */   
/*     */   private String origin_npcode;
/*     */   private String dest_npcode;
/*     */   private long penalty;
/*     */   private String occ_spshdes;
/*     */   private String occ_modif;
/*     */   private String sn_shdes;
/*     */   private String parameter_value;
/*     */   private String parameter_value2;
/*     */   private int FREE_CHANGES;
/*     */   
/*     */   public int getMaxMobiled()
/*     */   {
/*  38 */     return this.maxMobiled;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMaxMobiled(int maxMobiled)
/*     */   {
/*  44 */     this.maxMobiled = maxMobiled;
/*     */   }
/*     */   
/*     */ 
/*     */   public int getMaxFixend()
/*     */   {
/*  50 */     return this.maxFixend;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMaxFixend(int maxFixend)
/*     */   {
/*  56 */     this.maxFixend = maxFixend;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFfSHDES()
/*     */   {
/*  62 */     return this.ffSHDES;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFfSHDES(String ffSHDES)
/*     */   {
/*  68 */     this.ffSHDES = ffSHDES;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFfcode()
/*     */   {
/*  74 */     return this.ffcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFfcode(String ffcode)
/*     */   {
/*  80 */     this.ffcode = ffcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFf_pcode()
/*     */   {
/*  86 */     return this.ff_pcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFf_pcode(String ff_pcode)
/*     */   {
/*  92 */     this.ff_pcode = ff_pcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFf_des()
/*     */   {
/*  98 */     return this.ff_des;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFf_des(String ff_des)
/*     */   {
/* 104 */     this.ff_des = ff_des;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getImc_scalefactor_type()
/*     */   {
/* 110 */     return this.imc_scalefactor_type;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setImc_scalefactor_type(String imc_scalefactor_type)
/*     */   {
/* 116 */     this.imc_scalefactor_type = imc_scalefactor_type;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOrigin_default()
/*     */   {
/* 122 */     return this.origin_default;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOrigin_default(String origin_default)
/*     */   {
/* 128 */     this.origin_default = origin_default;
/*     */   }
/*     */   
/*     */ 
/*     */   public int getScale_factor()
/*     */   {
/* 134 */     return this.scale_factor;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setScale_factor(int scale_factor)
/*     */   {
/* 140 */     this.scale_factor = scale_factor;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOrigin_npcode()
/*     */   {
/* 146 */     return this.origin_npcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOrigin_npcode(String origin_npcode)
/*     */   {
/* 152 */     this.origin_npcode = origin_npcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getDest_npcode()
/*     */   {
/* 158 */     return this.dest_npcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setDest_npcode(String dest_npcode)
/*     */   {
/* 164 */     this.dest_npcode = dest_npcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public long getPenalty()
/*     */   {
/* 170 */     return this.penalty;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setPenalty(long penalty)
/*     */   {
/* 176 */     this.penalty = penalty;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOcc_spshdes()
/*     */   {
/* 182 */     return this.occ_spshdes;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOcc_spshdes(String occ_spshdes)
/*     */   {
/* 188 */     this.occ_spshdes = occ_spshdes;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOcc_modif()
/*     */   {
/* 194 */     return this.occ_modif;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOcc_modif(String occ_modif)
/*     */   {
/* 200 */     this.occ_modif = occ_modif;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSn_shdes()
/*     */   {
/* 206 */     return this.sn_shdes;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSn_shdes(String sn_shdes)
/*     */   {
/* 212 */     this.sn_shdes = sn_shdes;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getParameter_value()
/*     */   {
/* 218 */     return this.parameter_value;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setParameter_value(String parameter_value)
/*     */   {
/* 224 */     this.parameter_value = parameter_value;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getParameter_value2()
/*     */   {
/* 230 */     return this.parameter_value2;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setParameter_value2(String parameter_value2)
/*     */   {
/* 236 */     this.parameter_value2 = parameter_value2;
/*     */   }
/*     */   
/*     */ 
/*     */   public int getFREE_CHANGES()
/*     */   {
/* 242 */     return this.FREE_CHANGES;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFREE_CHANGES(int fREE_CHANGES)
/*     */   {
/* 248 */     this.FREE_CHANGES = fREE_CHANGES;
/*     */   }
/*     */ }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\nip\MicroCellBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */