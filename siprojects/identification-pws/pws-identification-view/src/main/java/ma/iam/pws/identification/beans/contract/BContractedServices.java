/*     */ package ma.iam.pws.identification.beans.contract;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Date;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BContractedServices
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   public static final String ALIAS = "SERVICES";
/*     */   private double CS_SUBSCRIPTION;
/*     */   private double CS_ACCESS;
/*     */   private String CS_PROIND;
/*     */   private String CS_ADVIND;
/*     */   private long SPCODE;
/*     */   private String SPCODE_PUB;
/*     */   private long SNCODE;
/*     */   private Integer COS_STATUS;
/*     */   private Date COS_STATUS_DATE;
/*     */   private Integer COS_PENDING_STATUS;
/*     */   private Date COS_PENDING_STATUS_DATE;
/*     */   private Date COS_LAST_ACT_DATE;
/*     */   private long CO_REQ_PENDING;
/*     */   private String SNCODE_PUB;
/*     */   private String SERVICE_DES;
/*     */   private String SERVICE_PKG_DES;
/*     */   
/*     */   public String getSERVICE_DES()
/*     */   {
/*  76 */     return this.SERVICE_DES;
/*     */   }
/*     */   
/*     */   public void setSERVICE_DES(String sERVICE_DES) {
/*  80 */     this.SERVICE_DES = sERVICE_DES;
/*     */   }
/*     */   
/*     */   public String getSERVICE_PKG_DES() {
/*  84 */     return this.SERVICE_PKG_DES;
/*     */   }
/*     */   
/*     */   public void setSERVICE_PKG_DES(String sERVICE_PKG_DES) {
/*  88 */     this.SERVICE_PKG_DES = sERVICE_PKG_DES;
/*     */   }
/*     */   
/*     */   public String getCS_ADVIND() {
/*  92 */     return this.CS_ADVIND;
/*     */   }
/*     */   
/*     */   public void setCS_ADVIND(String cS_ADVIND) {
/*  96 */     this.CS_ADVIND = cS_ADVIND;
/*     */   }
/*     */   
/*     */   public double getCS_ACCESS() {
/* 100 */     return this.CS_ACCESS;
/*     */   }
/*     */   
/*     */   public double getCS_SUBSCRIPTION() {
/* 104 */     return this.CS_SUBSCRIPTION;
/*     */   }
/*     */   
/*     */   public void setCS_SUBSCRIPTION(double cS_SUBSCRIPTION) {
/* 108 */     this.CS_SUBSCRIPTION = cS_SUBSCRIPTION;
/*     */   }
/*     */   
/*     */   public void setCS_ACCESS(double cS_ACCESS) {
/* 112 */     this.CS_ACCESS = cS_ACCESS;
/*     */   }
/*     */   
/*     */   public String getCS_PROIND() {
/* 116 */     return this.CS_PROIND;
/*     */   }
/*     */   
/*     */   public void setCS_PROIND(String cS_PROIND) {
/* 120 */     this.CS_PROIND = cS_PROIND;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Date getCOS_LAST_ACT_DATE()
/*     */   {
/* 351 */     return this.COS_LAST_ACT_DATE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCOS_LAST_ACT_DATE(Date cOS_LAST_ACT_DATE)
/*     */   {
/* 359 */     this.COS_LAST_ACT_DATE = cOS_LAST_ACT_DATE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getCOS_PENDING_STATUS()
/*     */   {
/* 376 */     return this.COS_PENDING_STATUS;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCOS_PENDING_STATUS(Integer cOS_PENDING_STATUS)
/*     */   {
/* 395 */     this.COS_PENDING_STATUS = cOS_PENDING_STATUS;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Date getCOS_PENDING_STATUS_DATE()
/*     */   {
/* 403 */     return this.COS_PENDING_STATUS_DATE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCOS_PENDING_STATUS_DATE(Date cOS_PENDING_STATUS_DATE)
/*     */   {
/* 411 */     this.COS_PENDING_STATUS_DATE = cOS_PENDING_STATUS_DATE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getCOS_STATUS()
/*     */   {
/* 445 */     return this.COS_STATUS;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCOS_STATUS(Integer cOS_STATUS)
/*     */   {
/* 462 */     this.COS_STATUS = cOS_STATUS;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Date getCOS_STATUS_DATE()
/*     */   {
/* 470 */     return this.COS_STATUS_DATE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCOS_STATUS_DATE(Date cOS_STATUS_DATE)
/*     */   {
/* 478 */     this.COS_STATUS_DATE = cOS_STATUS_DATE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getCO_REQ_PENDING()
/*     */   {
/* 546 */     return this.CO_REQ_PENDING;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCO_REQ_PENDING(long cO_REQ_PENDING)
/*     */   {
/* 553 */     this.CO_REQ_PENDING = cO_REQ_PENDING;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getSNCODE()
/*     */   {
/* 692 */     return this.SNCODE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setSNCODE(long sNCODE)
/*     */   {
/* 699 */     this.SNCODE = sNCODE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSNCODE_PUB()
/*     */   {
/* 720 */     return this.SNCODE_PUB;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setSNCODE_PUB(String sNCODE_PUB)
/*     */   {
/* 727 */     this.SNCODE_PUB = sNCODE_PUB;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public long getSPCODE()
/*     */   {
/* 734 */     return this.SPCODE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setSPCODE(long sPCODE)
/*     */   {
/* 741 */     this.SPCODE = sPCODE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getSPCODE_PUB()
/*     */   {
/* 748 */     return this.SPCODE_PUB;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setSPCODE_PUB(String sPCODE_PUB)
/*     */   {
/* 755 */     this.SPCODE_PUB = sPCODE_PUB;
/*     */   }
/*     */ }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\contract\BContractedServices.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */