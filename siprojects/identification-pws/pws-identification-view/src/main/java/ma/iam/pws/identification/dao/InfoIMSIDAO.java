package ma.iam.pws.identification.dao;

import ma.iam.pws.identification.beans.prepaid.IMSIbean;
import ma.iam.pws.identification.exception.FaultWS;

public abstract interface InfoIMSIDAO
{
  public abstract IMSIbean checkIMSI(String paramString)
    throws FaultWS;
  
  public abstract int getCINUsesCount(String paramString)
    throws FaultWS;
  
  public abstract IMSIbean getIMSITraceStat(String paramString)
    throws FaultWS;
  
  public abstract Boolean writeIMSIIdentity(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    throws FaultWS;
}


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\dao\InfoIMSIDAO.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */