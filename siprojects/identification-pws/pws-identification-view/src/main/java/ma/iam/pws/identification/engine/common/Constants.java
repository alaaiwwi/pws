 package ma.iam.pws.identification.engine.common;
 
 
 
 public class Constants
 {
   public static final int CONFIRM_CODE = 1;
   
 
   public static final int INFO_CODE = 2;
   
 
   public static final int ERROR_CODE = 4;
   
 
   public static final int HABLITATION_CODE = 3;
   
 
   public static final int RESULT_CODE = 0;
   
 
   public static final int NO_ALERTE_CODE = 5;
   
/* 24 */   public static String MESSAGE_FILE_NAME = "ressource.message";
   public static final String MESSAGE_ERREUR_TECHNIQUE = "message.erreur.technique";
   public static final String AUTHORIZATION_FAILURE_FLG = "authorizationFailureFlg";
   public static final String AUTHENTICATION_NOT_FOUND_FLG = "authenticationNotFound";
   public static final String AUTHENTICATION_FAILED_MSG = "authenticationFailed";
   public static final String FORMAT_DATE = "dd/MM/yyyy";
   public static final String CODE_REVENDEUR = "15";
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\engine\common\Constants.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */