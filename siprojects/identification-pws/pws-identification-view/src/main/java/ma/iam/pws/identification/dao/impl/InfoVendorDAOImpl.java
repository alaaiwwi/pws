 package ma.iam.pws.identification.dao.impl;
 
 import java.util.Properties;

import ma.iam.pws.identification.dao.InfoVendorDAO;
import ma.iam.pws.identification.exception.FaultWS;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
 
 
 
 
 
 
 
 
 @Service
 public class InfoVendorDAOImpl
   implements InfoVendorDAO
 {
   @Autowired
   @Qualifier("jdbcTemplate")
   JdbcTemplate jdbcTemplate;
   @Autowired
   @Qualifier("queries")
   Properties queries;
   @Autowired
   @Qualifier("msgpropertiesBean")
   Properties msgProperties;
/* 33 */   private static Log log = LogFactory.getLog(InfoVendorDAOImpl.class);
   
   public String AuthorizedVendor(String msisdn) throws FaultWS {
/* 36 */     int exist = 0;
     try {
/* 38 */       exist = this.jdbcTemplate.queryForInt(this.queries.getProperty("checkVendeur"), new Object[] { msisdn });
     }
     catch (Exception e)
     {
/* 42 */       log.info(e.getLocalizedMessage());
/* 43 */       throw new FaultWS("MT9999", this.msgProperties.getProperty("MT9999"));
     }
     
/* 46 */     if (exist > 0) {
/* 47 */       return "ND_VENDOR";
     }
/* 49 */     return "ND_NON_VENDOR";
   }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\dao\impl\InfoVendorDAOImpl.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */