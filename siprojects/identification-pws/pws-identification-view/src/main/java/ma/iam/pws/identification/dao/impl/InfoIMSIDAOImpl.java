package ma.iam.pws.identification.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import ma.iam.pws.identification.beans.prepaid.IMSIbean;
import ma.iam.pws.identification.dao.InfoIMSIDAO;
import ma.iam.pws.identification.exception.FaultWS;
import ma.iam.pws.identification.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class InfoIMSIDAOImpl implements InfoIMSIDAO {
	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("queries")
	Properties queries;
	@Autowired
	@Qualifier("msgpropertiesBean")
	Properties msgProperties;
	/* 42 */private static Log log = LogFactory.getLog(InfoIMSIDAOImpl.class);

	public IMSIbean checkIMSI(String imsi) throws FaultWS {
		/* 45 */int exist = 0;
		IMSIbean imsiBean;
		try {
			/* 49 */exist = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("getIMSIWithHoldContract"),
					new Object[] { imsi });

			/* 52 */if (exist > 0) {
				try {
					/* 55 */imsiBean = (IMSIbean) this.jdbcTemplate
							.queryForObject(this.queries
									.getProperty("getIMSIInfoContract"),
									new Object[] { imsi }, new RowMapper() {
										public IMSIbean mapRow(ResultSet rs,
												int rowNum) throws SQLException {
											IMSIbean ismiBean = new IMSIbean();
											ismiBean.setCo_id(rs.getString(1));
											ismiBean.setCustomerId(rs
													.getString(2));
											ismiBean.setIMSI_ref(rs
													.getString(3));
											ismiBean.setIMSI_stat(rs
													.getString(4));
											return ismiBean;
										}
									});

					/* 68 */if (imsiBean != null) {
						/* 70 */exist = this.jdbcTemplate.queryForInt(
								this.queries.getProperty("checkInfoCustomer"),
								new Object[] { imsiBean.getCustomerId() });

						/* 73 */if (exist > 0) {
							/* 75 */exist = this.jdbcTemplate.queryForInt(
									this.queries.getProperty("checkTraceIMSI"),
									new Object[] { imsi });

							/* 78 */if (exist <= 0) {
								/* 79 */return imsiBean;
							}
							/* 81 */throw new FaultWS("MT0039",
									this.msgProperties);
						}

						/* 85 */throw new FaultWS("MT0038", this.msgProperties);
					}

				} catch (EmptyResultDataAccessException dex) {
					/* 91 */throw new FaultWS("MT0037", this.msgProperties);
				} catch (Exception e) {
					/* 94 */if ((e instanceof FaultWS)) {
						/* 95 */throw ((FaultWS) e);
					}
					/* 97 */log.info(e.getLocalizedMessage());
					/* 98 */throw new FaultWS("MT9999", this.msgProperties);
				}
			}

			/* 103 */throw new FaultWS("MT0036", this.msgProperties);
		} catch (Exception e) {
			/* 106 */if (!(e instanceof FaultWS))
				
			/* 107 */throw ((FaultWS) e);
			/* 109 */log.info(e.getLocalizedMessage());
			/* 110 */throw new FaultWS("MT9999", this.msgProperties);
		} finally {
			/* 113 */log.info("handle imsi : " + imsi);
		}
	}

	public int getCINUsesCount(String cin) throws FaultWS {
		/* 119 */int count = 0;
		try {
			/* 122 */count = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("getCinCount"),
					new Object[] { cin });
		} catch (Exception e) {
			/* 125 */log.info(e.getLocalizedMessage());
			/* 126 */throw new FaultWS("MT9999", this.msgProperties);
		}
		/* 128 */return count;
	}

	public IMSIbean getIMSITraceStat(String imsi) throws FaultWS {
		try {
			/* 134 */return (IMSIbean) this.jdbcTemplate.queryForObject(
					this.queries.getProperty("getStatusTraceIMSI"),
					new Object[] { imsi }, new RowMapper() {
						public IMSIbean mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							IMSIbean ismiBean = new IMSIbean();
							ismiBean.setCo_id(rs.getString(1));
							ismiBean.setIMSI_ref(rs.getString(2));
							ismiBean.setIMSI_stat(rs.getString(3));
							ismiBean.setRejectReason(rs.getString(4));
							return ismiBean;
						}
					});

		} catch (EmptyResultDataAccessException ex) {

			/* 149 */throw new FaultWS("MT0041", this.msgProperties);
		} catch (Exception ex) {
			/* 151 */log.info(ex.getLocalizedMessage());
			/* 152 */throw new FaultWS("MT9999", this.msgProperties);
		}
	}

	public Boolean writeIMSIIdentity(String msisdn, String imsi, String cin,
			String firstname, String lastname) throws FaultWS {
		try {
			/* 159 */this.jdbcTemplate.update(
					this.queries.getProperty("writeISMIIdentity"),
					new Object[] { cin, msisdn, lastname, imsi, firstname });
		} catch (DataAccessException dex) {
			/* 162 */log.info(dex.getLocalizedMessage());
			/* 163 */return Constants.IDENTITY_NOT_INSERTED;
		} catch (Exception x) {
			/* 165 */log.info(x.getLocalizedMessage());
			/* 166 */throw new FaultWS("MT9999", this.msgProperties);
		}
		/* 168 */return Constants.IDENTITY_INSERTED;
	}

}

/*
 * Location: C:\Users\A596968\Desktop\En
 * cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT
 * .war!\WEB-INF\classes\ma\iam\ws\dao\impl\InfoIMSIDAOImpl.class Java compiler
 * version: 5 (49.0) JD-Core Version: 0.7.1
 */