 package ma.iam.pws.identification.beans.administration;
 
 import javax.xml.bind.annotation.XmlAccessType;
 import javax.xml.bind.annotation.XmlAccessorType;
 import javax.xml.bind.annotation.XmlElement;
 
 
 
 
 
 
 @XmlAccessorType(XmlAccessType.NONE)
 public class ProfileEligibilityBean
 {
   public static final String ALIAS = "ProfileEligibility";
   @XmlElement
   private long eligibility;
   @XmlElement
   private long profile;
   
   public ProfileEligibilityBean()
   {
    this.eligibility = -1L;
     this.profile = -1L;
   }
   
 
 
 
 
 
 
 
 
 
 
 
 
   public long getEligibility()
   {
     return this.eligibility;
   }
   
   public void setEligibility(long eligibility) {
    this.eligibility = eligibility;
   }
   
   public long getProfile() {
   return this.profile;
   }
   
   public void setProfile(long profile) {
     this.profile = profile;
   }
   
   public static String getAlias() {
    return "ProfileEligibility";
   }
 }

