 package ma.iam.pws.identification.beans.nip;
 
 import com.thoughtworks.xstream.annotations.XStreamAlias;
 
 @XStreamAlias("parameter")
 public class Parameter
 {
   private String PARAMETER_ID;
   private String PRM_NO;
   private String PRM_DES;
   private String mobile;
   private String fixe;
   
   public String getPARAMETER_ID()
   {
/* 16 */     return this.PARAMETER_ID;
   }
   
   public void setPARAMETER_ID(String parameter_id) {
/* 20 */     this.PARAMETER_ID = parameter_id;
   }
   
   public String getPRM_NO() {
/* 24 */     return this.PRM_NO;
   }
   
   public void setPRM_NO(String prm_no) {
/* 28 */     this.PRM_NO = prm_no;
   }
   
   public String getPRM_DES() {
/* 32 */     return this.PRM_DES;
   }
   
   public void setPRM_DES(String prm_des) {
/* 36 */     this.PRM_DES = prm_des;
   }
   
   public String getMobile() {
/* 40 */     return this.mobile;
   }
   
   public void setMobile(String mobile) {
/* 44 */     this.mobile = mobile;
   }
   
   public String getFixe() {
/* 48 */     return this.fixe;
   }
   
   public void setFixe(String fixe) {
/* 52 */     this.fixe = fixe;
   }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\nip\Parameter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */