 package ma.iam.pws.identification.beans.nip;
 
 import com.thoughtworks.xstream.annotations.XStreamAlias;
 import com.thoughtworks.xstream.annotations.XStreamImplicit;
 import java.util.Arrays;
 import java.util.List;
 
 
 
 
 
 
 @XStreamAlias("NIPBean")
 public class NIPBean
 {
   public static final String ALIAS = "retour";
   private String result;
   private String message;
   @XStreamImplicit(itemFieldName="mobile")
   private List<String> mobile;
   @XStreamImplicit(itemFieldName="fixe")
   private List<String> fixe;
   @XStreamAlias("maxNDs")
   private List<Parameter> parameter;
   
   public NIPBean() {}
   
   public NIPBean(String result, String message)
   {
/* 30 */     this.result = result;
/* 31 */     this.message = message;
   }
   
   public String getResult() {
/* 35 */     return this.result;
   }
   
   public void setResult(String result) {
/* 39 */     this.result = result;
   }
   
   public String getMessage() {
/* 43 */     return this.message;
   }
   
   public void setMessage(String message) {
/* 47 */     this.message = message;
   }
   
   public List<String> getMobile() {
/* 51 */     return this.mobile;
   }
   
   public void setMobile(String[] mobile) {
/* 55 */     this.mobile = Arrays.asList(mobile);
   }
   
   public List<String> getFixe() {
/* 59 */     return this.fixe;
   }
   
   public void setFixe(String[] fixe) {
/* 63 */     this.fixe = Arrays.asList(fixe);
   }
   
   public List<Parameter> getParameter() {
/* 67 */     return this.parameter;
   }
   
   public void setParameter(List<Parameter> parameter) {
/* 71 */     this.parameter = parameter;
   }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\nip\NIPBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */