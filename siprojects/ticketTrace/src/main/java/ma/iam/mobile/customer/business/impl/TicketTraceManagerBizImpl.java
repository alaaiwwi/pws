package ma.iam.mobile.customer.business.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.bean.TicketRequest;
import ma.iam.mobile.customer.bean.TicketResponse;
import ma.iam.mobile.customer.business.TicketTraceManagerBiz;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.controller.TicketTraceController;
import ma.iam.mobile.customer.dao.TicketTraceDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "ticketTraceManagerBiz")
public class TicketTraceManagerBizImpl implements TicketTraceManagerBiz {
	@Autowired
	TicketTraceController ticketTraceController;

	@Autowired
	private Properties config;

	@Autowired
	TicketTraceDao ticketTraceDao;

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory.getLogger(TicketTraceManagerBizImpl.class);
		

	
	@Override
	public List<TicketResponse> updateTicketStatus( List<TicketRequest> request) 
			throws FunctionnalException, TechnicalException {
		
		return ticketTraceDao.updateTicketStatus(request);
		
	}	
	
	
}
