/*
 * @author AtoS
 */
package ma.iam.mobile.customer.constants;

/**
 * The Interface GRCProperties.
 */
public interface ProjectProperties {

    /** La constante TIMEOUT. */
    int TIMEOUT = 120;
}
