package ma.iam.mobile.customer.bean;

public class TicketResponse {
	
	private String coId;
	private String ticketErevNumber;
	private String ticketSINumber;
	private boolean updated;
	
	/**
	 * @return the coId
	 */
	public String getCoId() {
		return coId;
	}
	/**
	 * @param coId the coId to set
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}
	/**
	 * @return the ticketErevNumber
	 */
	public String getTicketErevNumber() {
		return ticketErevNumber;
	}
	/**
	 * @param ticketErevNumber the ticketErevNumber to set
	 */
	public void setTicketErevNumber(String ticketErevNumber) {
		this.ticketErevNumber = ticketErevNumber;
	}
	/**
	 * @return the ticketSINumber
	 */
	public String getTicketSINumber() {
		return ticketSINumber;
	}
	/**
	 * @param ticketSINumber the ticketSINumber to set
	 */
	public void setTicketSINumber(String ticketSINumber) {
		this.ticketSINumber = ticketSINumber;
	}
	/**
	 * @return the updated
	 */
	public boolean isUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(boolean updated) {
		this.updated = updated;
	}
}
