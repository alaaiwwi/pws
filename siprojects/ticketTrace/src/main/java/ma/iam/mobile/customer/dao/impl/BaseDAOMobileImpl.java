/*
 * @author AtoS
 */
package ma.iam.mobile.customer.dao.impl;

import javax.sql.DataSource;

import ma.iam.mobile.customer.dao.BaseDAOMobile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * The Class BaseDAOMobile.
 */
@Repository
public class BaseDAOMobileImpl implements BaseDAOMobile {

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private DataSource dataSource;

	/**
	 * Sets the mobile bscs data source.
	 * 
	 * @param mobileBSCSDataSource
	 *            the new mobile bscs data source
	 */
	@Autowired
	@Qualifier("mobileBscsDataSource")
	public void setMobileBscsDataSource(final DataSource mobileBscsDataSource) {

		this.setNamedParameterJdbcTemplate(new NamedParameterJdbcTemplate(
				mobileBscsDataSource));

		this.dataSource = mobileBscsDataSource;
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplate(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
