package ma.iam.mobile.customer.service;

import java.util.HashMap;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.bean.TicketRequest;
import ma.iam.mobile.customer.bean.TicketResponse;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;

/**
 * The Class ticketTraceService.
 */
@WebService(serviceName = "ticketTraceService")
public interface TicketTraceManagerService {

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public Integer modeByPass();	
	


	@WebMethod(operationName = "updateTicketStatus")
	@WebResult(name = "ticketResponse")
	public  List<TicketResponse> updateTicketStatus(@WebParam(name = "ticketRequest") List<TicketRequest> request) throws FunctionnalException, TechnicalException;
	

	

}
