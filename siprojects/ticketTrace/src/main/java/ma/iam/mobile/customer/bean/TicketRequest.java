package ma.iam.mobile.customer.bean;

public class TicketRequest {
	
	private String coId;
	private String cuId;
	private String custCode;
	private String nd;
	private String status;
	private String ticketErevNumber;
	private String ticketSINumber;
	/**
	 * @return the coId
	 */
	public String getCoId() {
		return coId;
	}
	/**
	 * @param coId the coId to set
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}
	/**
	 * @return the cuId
	 */
	public String getCuId() {
		return cuId;
	}
	/**
	 * @param cuId the cuId to set
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}
	/**
	 * @return the custCode
	 */
	public String getCustCode() {
		return custCode;
	}
	/**
	 * @param custCode the custCode to set
	 */
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	/**
	 * @return the nd
	 */
	public String getNd() {
		return nd;
	}
	/**
	 * @param nd the nd to set
	 */
	public void setNd(String nd) {
		this.nd = nd;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the ticketErevNumber
	 */
	public String getTicketErevNumber() {
		return ticketErevNumber;
	}
	/**
	 * @param ticketErevNumber the ticketErevNumber to set
	 */
	public void setTicketErevNumber(String ticketErevNumber) {
		this.ticketErevNumber = ticketErevNumber;
	}
	/**
	 * @return the ticketSINumber
	 */
	public String getTicketSINumber() {
		return ticketSINumber;
	}
	/**
	 * @param ticketSINumber the ticketSINumber to set
	 */
	public void setTicketSINumber(String ticketSINumber) {
		this.ticketSINumber = ticketSINumber;
	}
	
}
