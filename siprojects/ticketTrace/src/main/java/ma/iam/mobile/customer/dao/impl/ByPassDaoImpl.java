/*
 * @author AtoS
 */

package ma.iam.mobile.customer.dao.impl;

import ma.iam.mobile.customer.dao.ByPassDao;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ByPassDaoImpl extends BaseDAOMobileImpl implements ByPassDao {

	@Autowired
	private JDBCPing pingBSCSMobile;

	public boolean modeByPass() {
		return pingBSCSMobile.check();
	}

}
