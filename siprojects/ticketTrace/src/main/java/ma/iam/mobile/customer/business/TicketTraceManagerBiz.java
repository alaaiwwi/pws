package ma.iam.mobile.customer.business;

import java.util.HashMap;
import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.bean.TicketRequest;
import ma.iam.mobile.customer.bean.TicketResponse;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;

public interface TicketTraceManagerBiz {
	

	
	List<TicketResponse> updateTicketStatus( List<TicketRequest> request) 
			throws FunctionnalException, TechnicalException;
	

}
