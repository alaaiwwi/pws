/*
 * @author AtoS
 */
package ma.iam.mobile.customer.util;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class IntervalWSValidator.
 */
public final class WSValidator {

	/**
	 * Instantiates a new interval ws validator.
	 */
	private WSValidator() {

	}

	/** The FORMAT. */

	public static final String MAIL_MATCHER = "([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})";
	public static final String NON_NUMERIC_MATCHER = "^[A-Za-z \'-.]{0,40}$";
	public static final String FIXE_ND_MATCHER = "(0|00212|\\+212|212)5[0-9]{8}";
	public static final String FIXE_ND_MATCHER2 = "(0)5[0-9]{8}";
	public static final String MOBILE_ND_MATCHER = "(212)6|7[0-9]{8}";
	public static final String STRING_MATCHER="^[a-zA-Z0-9]*$";
	public static final String FIDELIO_MATCHER="[0-9]{4}";

	/**
	 * Validate date.
	 * 
	 * @param date
	 *            the date
	 * @return the boolean
	 */
	public static Boolean validateDate(final String date) {
		return validateDate(date, DateUtil.FRENCH_FORMAT);
	}

	/**
	 * Validate date.
	 * 
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return the boolean
	 */
	public static Boolean validateDate(final String date, final String format) {
		Boolean result = Boolean.TRUE;
		try {
			Date res = DateUtil.stringToDate(date, format);
			if (res == null)
				result = Boolean.FALSE;
		} catch (final IllegalArgumentException e) {
			result = Boolean.FALSE;
		}
		return result;
	}

	/**
	 * Validate Long.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateLongNumber(final String value) {
		Boolean result = Boolean.TRUE;
		if (StringUtil.isEmpty(value)) {
			result = Boolean.FALSE;
		} else {
			try {
				Long.parseLong(value);
			} catch (final NumberFormatException e) {
				result = Boolean.FALSE;
			}
		}
		return result;
	}

	/**
	 * Validate Double.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateDoubleNumber(final String value) {
		Boolean result = Boolean.TRUE;
		if (StringUtil.isEmpty(value)) {
			result = Boolean.FALSE;
		} else {
			try {
				Double.parseDouble(value);
			} catch (final NumberFormatException e) {
				result = Boolean.FALSE;
			}
		}
		return result;
	}

	/**
	 * Validate Mobile Number.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateMobileNumber(final String value) {

		Pattern pattern = Pattern.compile(MOBILE_ND_MATCHER);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();

	}

	/**
	 * Validate Fix Number.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateFixNumber(final String value) {

		Pattern pattern = Pattern.compile(FIXE_ND_MATCHER);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();

	}

	public static Boolean validateFixNumber2(final String value) {

		Pattern pattern = Pattern.compile(FIXE_ND_MATCHER2);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();

	}

	public static Boolean validateNonNumeric(final String value) {

		Pattern pattern = Pattern.compile(NON_NUMERIC_MATCHER);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();
	}

	public static Boolean validateMail(final String value) {

		Pattern pattern = Pattern.compile(MAIL_MATCHER);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();
	}

	public static Boolean validateNumberLength(final String value,
			final int length) {
		return value.length() == length;
	}

	public static Boolean isLengthLessThan(final String value, final int length) {
		return value.length() <= length;
	}
	
	public static Boolean hasWhiteSpace(final String value) {
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(value);
		return matcher.find();
	}
	
	public static Boolean validateAlphanumericString(final String value) {

		Pattern pattern = Pattern.compile(STRING_MATCHER);
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	public static Boolean validateCodeFidelio(final String value) {

		Pattern pattern = Pattern.compile(FIDELIO_MATCHER);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();
	}
}
