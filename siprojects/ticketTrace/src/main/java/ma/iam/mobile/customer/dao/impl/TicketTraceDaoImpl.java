/*
 * @author AtoS
 */

package ma.iam.mobile.customer.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import ma.iam.mobile.customer.bean.TicketRequest;
import ma.iam.mobile.customer.bean.TicketResponse;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.dao.TicketTraceDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.exceptions.WsException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mobile.customer.util.StringUtil;
import ma.iam.mobile.customer.util.WSValidator;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

@Repository
public class TicketTraceDaoImpl extends BaseDAOMobileImpl implements TicketTraceDao {
	private static final Logger LOG = LoggerFactory.getLogger(TicketTraceDaoImpl.class);
	
	private static final String TICKET_TRACE_UPDATE = TicketTraceDaoImpl.class.getName()
			+ ".TICKET_TRACE_UPDATE";

	
	
	
	public List<TicketResponse> updateTicketStatus( List<TicketRequest> request) 
			throws FunctionnalException, TechnicalException{
		LOG.debug("--> Start updateTicketStatus");
		List<TicketResponse> ops = new ArrayList();
		
		if(request!=null && request.size() >0 ){
			
			for (int i=0;i<request.size();i++){
				try {
					TicketRequest temp = request.get(i);
					TicketResponse tempRes = new TicketResponse();
					final String sql = CustomSQLUtil.get(TICKET_TRACE_UPDATE);
					Map<String, String> namedParameters = new HashMap();
					
					namedParameters.put("coId", temp.getCoId());
					namedParameters.put("cuId", temp.getCuId());
					namedParameters.put("custCode", temp.getCustCode());
					namedParameters.put("nd", temp.getNd());
					namedParameters.put("status", temp.getStatus());
					namedParameters.put("ticketErevNumber", temp.getTicketErevNumber());
					namedParameters.put("ticketSiNumber", temp.getTicketSINumber());
					

					int res = super.getNamedParameterJdbcTemplate().update(sql, namedParameters);
					
					if(res>0){
						tempRes.setCoId(temp.getCoId());
						tempRes.setTicketErevNumber(temp.getTicketErevNumber());
						tempRes.setTicketSINumber(temp.getTicketSINumber());
						tempRes.setUpdated(true);
					}
					else{
						tempRes.setCoId(temp.getCoId());
						tempRes.setTicketErevNumber(temp.getTicketErevNumber());
						tempRes.setTicketSINumber(temp.getTicketSINumber());
						tempRes.setUpdated(false);						
					}
					ops.add(tempRes);
					LOG.debug("--> End updateTicketStatus");
				} catch (EmptyResultDataAccessException e) {
					LOG.error("Erreur d'insertion updateTicketStatus");
				}
			}
		}
		
		
		return ops;
		
	}
	
}
