package ma.iam.mobile.mobicash.service.impl;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.business.ByPassManagerBiz;
import ma.iam.mobile.mobicash.business.MobicashManagerBiz;
import ma.iam.mobile.mobicash.constants.Constants;
import ma.iam.mobile.mobicash.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;
import ma.iam.mobile.mobicash.exceptions.SyntaxiqueException;
import ma.iam.mobile.mobicash.service.MobicashManagerService;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

@Component(value = "mobicashManagerService")
public class MobicashManagerServiceImpl extends SpringBeanAutowiringSupport implements MobicashManagerService {

	@Autowired
	private ByPassManagerBiz byPassManagerBiz;
	@Autowired
	private JDBCPing pingFixeBscs;

	@Autowired
	private MobicashManagerBiz mobicashManagerBiz;

	@Override
	public CustomerPosition getOpenInvoiceList(int domain, String uniqueId, String pin) throws FunctionnalException {
		try {
			if (domain == Constants.MOBICASH_DOMAIN_INTERNET) {
				return mobicashManagerBiz.getOpenInvoiceListInternet(Constants.MOBICASH_PREFIX_FIXE, uniqueId, pin);
			} else {
				return mobicashManagerBiz.getOpenInvoiceListFixe(Constants.MOBICASH_PREFIX_FIXE, uniqueId, pin);
			}
		} catch (FunctionnalException e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.ERRO_DOMAIN_CODE, Constants.ERRO_DOMAIN_CODE);
		} catch (SyntaxiqueException e) {
			e.printStackTrace();
		} catch (TechnicalException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String payInvoiceList(int domain, int customerId, String uniqueId, String pin, String authNum,
			ArrayList invoiceList) throws FunctionnalException {
		// TODO Auto-generated method stub
		try {
			if (domain == Constants.MOBICASH_DOMAIN_INTERNET) {
				return mobicashManagerBiz.payInvoiceListInternet(Constants.MOBICASH_PREFIX_FIXE, customerId, uniqueId,
						pin, authNum, invoiceList);
			} else {
				return mobicashManagerBiz.payInvoiceListFixe(Constants.MOBICASH_PREFIX_FIXE, customerId, uniqueId, pin,
						authNum, invoiceList);
			}
		} catch (FunctionnalException e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.ERRO_DOMAIN_CODE, Constants.ERRO_DOMAIN_CODE);
		} catch (SyntaxiqueException e) {
			e.printStackTrace();
		} catch (TechnicalException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String reconcile(int domain, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException {
		// TODO Auto-generated method stub
		try {
			if (domain == Constants.MOBICASH_DOMAIN_INTERNET) {
				return mobicashManagerBiz.reconcileInternet(Constants.MOBICASH_PREFIX_FIXE, date, operationCount,
						totalPaidAmount);
			} else {
				return mobicashManagerBiz.reconcileFixe(Constants.MOBICASH_PREFIX_FIXE, date, operationCount,
						totalPaidAmount);
			}
		} catch (FunctionnalException e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.ERRO_DOMAIN_CODE, Constants.ERRO_DOMAIN_CODE);
		} catch (SyntaxiqueException e) {
			e.printStackTrace();
		} catch (TechnicalException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Integer byPass() {
		// TODO Auto-generated method stub
		return byPassManagerBiz.modeByPass();
	}
}
