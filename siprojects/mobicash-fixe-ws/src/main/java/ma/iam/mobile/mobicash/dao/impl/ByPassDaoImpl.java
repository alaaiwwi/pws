/*
 * @author AtoS
 */

package ma.iam.mobile.mobicash.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.mobile.mobicash.dao.ByPassDao;
import net.atos.ma.integration.jee.bypass.JDBCPing;

@Repository
public class ByPassDaoImpl extends BaseDAOMobileImpl implements ByPassDao {

	@Autowired
	private JDBCPing pingBSCSFixe;

	public boolean modeByPass() {
		return pingBSCSFixe.check();
	}

}
