package ma.iam.mobile.mobicash.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DateUtil {

	public static final Log logger = LogFactory.getLog(DateUtil.class);

	public static final String FORMAT_DEFAUT = "yyyy-MM-dd";

	public static final String FRENCH_FORMAT = "dd/MM/yyyy";

	public static final String YEAR_FORMAT = "yyyy";
	public static final String MONTH_FORMAT = "MM";
	public static final String DAY_FORMAT = "dd";

	public static final String FORMAT_DDMMYY = "dd-MM-yy";

	public static String[] mois = { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aoét",
			"Septembre", "Octobre", "Novembre", "Décembre" };

	public static final Map<String, Integer> moisByNumList = new HashMap<String, Integer>();

	static {
		moisByNumList.put("janvier", 0);
		moisByNumList.put("février", 1);
		moisByNumList.put("mars", 2);
		moisByNumList.put("avril", 3);
		moisByNumList.put("mai", 4);
		moisByNumList.put("juin", 5);
		moisByNumList.put("juillet", 6);
		moisByNumList.put("aoét", 7);
		moisByNumList.put("septembre", 8);
		moisByNumList.put("octobre", 9);
		moisByNumList.put("novembre", 10);
		moisByNumList.put("décembre", 11);

	}

	public static final Map<Integer, String> moisByKey = new HashMap<Integer, String>();

	static {
		moisByKey.put(0, "janvier");
		moisByKey.put(1, "février");
		moisByKey.put(2, "mars");
		moisByKey.put(3, "avril");
		moisByKey.put(4, "mai");
		moisByKey.put(5, "juin");
		moisByKey.put(6, "juillet");
		moisByKey.put(7, "aoét");
		moisByKey.put(8, "septembre");
		moisByKey.put(9, "octobre");
		moisByKey.put(10, "novembre");
		moisByKey.put(11, "décembre");

	}

	public static final long MS_PAR_JOUR = 1000 * 60 * 60 * 24;

	public final static String FRENCH_DATE_SEPARATOR = "/";

	/**
	 * Méthode permettant de récupérer la date courante pattern french
	 * 
	 * @return Date
	 */
	public static Date currentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat(FRENCH_FORMAT);
		String strDate = formatter.format(new Date());
		return stringToDate(strDate);
	}

	/**
	 * Méthode permettant de récupérer l'année courant
	 * 
	 * @return int
	 */

	public static int currentYear() {
		SimpleDateFormat formatter = new SimpleDateFormat(YEAR_FORMAT);
		return Integer.parseInt(formatter.format(currentDate()));
	}

	/**
	 * Méthode permettant de récupérer le mois courant
	 * 
	 * @return String
	 */
	public static String currentMonth() {
		return donneMoisLiteral(getMonth(currentDate()));

	}

	/**
	 * Méthode permettant de retourner une date du type "dd/mm/yyyy" avec comme
	 * paramétre le jour, le mois et l'année
	 * 
	 * @return Date
	 * @throws ParseException
	 */
	public static Date format(String day, String month, String year) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(FRENCH_FORMAT);
		return formatter.parse(day + FRENCH_DATE_SEPARATOR + month + FRENCH_DATE_SEPARATOR + year);
	}

	/**
	 * Méthode permettant de récuperer une date <br>
	 * par exemple : pour récuperer le 1er jour du mois modifieDate(date,0,1)
	 * pour récuperer le 8eme jour du mois suivant modifieDate(date,1,8)
	 * 
	 * rmq : pour obtenir le dernier jour du mois de date modifieDate(date,1,0)
	 * 
	 * @param dateOrigine
	 * @param mois
	 *            : nombre de mois é ajouter ou retrancher par rapport
	 * @param jour
	 *            : le jour voulu
	 * @return
	 */
	public static Date modifieDate(Date dateOrigine, int mois, int jour) {
		Calendar calendrier = Calendar.getInstance();
		calendrier.setTime(dateOrigine);
		calendrier.set(calendrier.get(Calendar.YEAR), calendrier.get(Calendar.MONTH) + mois, jour);
		return calendrier.getTime();
	}

	/**
	 * Méthode retourne vrai si les deux dates correspondent au méme jour<br>
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		String jour1 = dateToString(date1, "ddMMyyyy");
		String jour2 = dateToString(date2, "ddMMyyyy");
		if (jour1.equals(jour2)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Méthode retourne vrai si la premiere date est un jour avant le date
	 * suivante <br>
	 * 
	 * @param date1
	 * @param date2
	 * @return boolean
	 */
	public static boolean isNextDay(Date dateBefore, Date date2) {
		if (isSameDay(dateBefore, date2))
			return false;
		if (dateBefore.before(date2)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Méthode verifie si la date dateATester appartient é l'intervalle
	 * [dateLimite-jourInf j ,dateLimite+jourSup j]
	 * 
	 * @param dateATester
	 * @param dateLimite
	 * @param jourInf
	 * @param jourSup
	 * @return boolean
	 */
	public static boolean isBetween(Date dateATester, Date dateLimite, int jourInf, int jourSup) {
		long timeATester = dateATester.getTime();
		long timeReference = dateLimite.getTime();

		// dateATester + jourInf * jours => dateLimite
		// et dateATester - jourSup * jours - dateLimite < 24 heures
		return (timeATester - timeReference + (jourInf + 1) * MS_PAR_JOUR) > 0
				&& (timeATester - (jourSup + 1) * MS_PAR_JOUR - timeReference) < 0;

	}

	/**
	 * Méthode vérifiant si une date est compris entre des deux dates<br>
	 * 
	 * @param dateATester
	 * @param dateInf
	 * @param dateSup
	 * @return
	 */
	public static boolean isBetween(Date dateATester, Date dateInf, Date dateSup) {
		if (DateUtil.isSameDay(dateATester, dateInf) || DateUtil.isSameDay(dateATester, dateSup)) {
			return true;
		} else {
			if (dateATester.before(dateSup) && dateATester.after(dateInf)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Méthode permettant comparer une date par rapport é la date du jour
	 * 
	 * @param date
	 * @return boolean
	 */
	public static boolean isCurrentDay(Date date) {
		return isSameDay(date, new Date());
	}

	/**
	 * Méthode permettant de vérifier si un champ de type String est une date
	 * 
	 * @param data
	 * @param pattern
	 * @return boolean
	 */
	public static boolean isDate(String data, String pattern) {

		if (data == null || data.equals(""))
			return false;

		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = formatter.parse(data);
		} catch (ParseException e) {
			return false;
		}

		String dateReverse = formatter.format(date);

		return data.equals(dateReverse);
	}

	/**
	 * Méthode permettant de rendre le mois littéral en fct de son indice
	 * 
	 * @param pos
	 * @return
	 */
	public static String donneMoisLiteral(int pos) {
		if (pos > 0 && pos < 13) {
			return mois[pos - 1];
		}
		return "";
	}

	/**
	 * Méthode permettant convertir une java.util.Date en java.sql.Date
	 * 
	 * @param date
	 * @return java.sql.Date
	 */
	public static java.sql.Date toSqlDate(Date date) {
		return new java.sql.Date(date.getTime());
	}

	/**
	 * Méthode permettant d'ajouter un nombre de jours é une date
	 * 
	 * @param date
	 *            de départ
	 * @param nbJours
	 *            nombre de jours é ajouter
	 * @return date de départ + nbJours
	 */
	public static java.util.Date addJours(Date date, int nbJours) {
		Calendar now = Calendar.getInstance();

		now.setTime(date);
		now.add(Calendar.DAY_OF_YEAR, nbJours);
		return now.getTime();
	}

	/**
	 * Méthode permettant d'ajouter un nombre de mois é une date
	 * 
	 * @param date
	 *            de départ
	 * @param nbMois
	 *            nombre de mois é ajouter
	 * @return date de départ + nbMois
	 */
	public static java.util.Date addMois(Date date, int nbMois) {
		Calendar now = Calendar.getInstance();

		now.setTime(date);
		now.add(Calendar.MONTH, nbMois);
		return now.getTime();
	}

	/**
	 * Méthode permettant d'obtenir le jour d'une date
	 * 
	 * @param date
	 *            de départ
	 * @return jour
	 */
	public static int getDay(Date date) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Méthode permettant d'obtenir le mois d'une date (janvier = 1, ...)
	 * 
	 * @param date
	 *            de départ
	 * @return mois
	 */
	public static int getMonth(Date date) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * Méthode permettant d'obtenir l'annee d'une date
	 * 
	 * @param date
	 *            de départ
	 * @return annee
	 */
	public static int getYear(Date date) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * Méthode permettant de modifier le jour d'une date
	 * 
	 * @param date
	 *            de départ
	 * @param
	 * @return la date modifiée
	 */
	public static Date setDay(Date date, int day) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		return calendar.getTime();
	}

	/**
	 * Méthode permettant de modifier le mois d'une date
	 * 
	 * @param date
	 *            de départ
	 * @param month
	 *            nouvelle valeur pour le mois (janvier=1, décembre=12)
	 * @return la date modifiée
	 */
	public static Date setMonth(Date date, int month) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, month - 1);
		return calendar.getTime();
	}

	/**
	 * Méthode permettant de modifier l'année d'une date
	 * 
	 * @param date
	 *            de départ
	 * @param year
	 *            nouvelle valeur pour l'année
	 * @return la date modifiée
	 */
	public static Date setYear(Date date, int year) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.YEAR, year);
		return calendar.getTime();
	}

	/**
	 * Méthode mettant é 0 les champs heure, minutes, secondes, millisecondes
	 * 
	 * @param date
	 *            de départ
	 * @param year
	 *            nouvelle valeur pour l'année
	 * @return la date modifiée
	 */
	public static Date clean(Date date) {
		Calendar calendar;

		calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * @return date
	 */
	public static Date getDateWithoutHour() {
		return stringToDate(dateToString(new Date()));
	}

	/**
	 * @param date
	 * @return date
	 */
	public static Date getDateWithoutHour(Date date) {
		return stringToDate(dateToString(date));
	}

	/**
	 * Conversion d'un objet java.util.Date dans une chaéne de caractére selon
	 * le format fourni
	 * 
	 * @param DateUtil
	 * @return String
	 */
	public static String to_String(java.util.Date dt, String format) {
		String st = null;
		st = new String();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			st = sdf.format(dt);
		} catch (Exception e) {

		}
		return st;
	}

	/**
	 * Obtention de la date de la date au format franéais
	 * 
	 * @param DateUtil
	 * @return String
	 */
	public static String formatFrancais(java.util.Date dt) {

		return DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE).format(dt);
	}

	/**
	 * Obtention de la date de la date au format par défaut (yyyy-MM-dd)
	 * 
	 * @param DateUtil
	 * @return String
	 */
	public static String to_String(java.util.Date dt) {
		return DateUtil.to_String(dt, FORMAT_DEFAUT);
	}

	/**
	 * Obtention de la date de la date au format par défaut (yyyy-MM-dd)
	 * 
	 * @param DateUtil
	 * @return String
	 */
	public static String to_FrenchString(java.util.Date dt) {
		if (dt == null) {
			return "";
		}
		return DateUtil.to_String(dt, FRENCH_FORMAT);

	}

	/**
	 * Transformation d'une date de chaéne de caractére é un objet java.sql.Date
	 * 
	 * @param DateUtil
	 * @return java.sql.Date
	 */
	public static java.sql.Date to_Date(String sdt, String format) {
		java.sql.Date dt = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			ParsePosition pp = new ParsePosition(0);
			dt = new java.sql.Date(sdf.parse(sdt, pp).getTime());
		} catch (Exception e) {
		}
		return dt;
	}

	/**
	 * Obtention de la date de la date au format par défaut (yyyy-MM-dd)
	 * 
	 * @param DateUtil
	 * @return java.sql.Date
	 */
	public static java.sql.Date to_Date(String sdt) {
		return DateUtil.to_Date(sdt, FORMAT_DEFAUT);
	}

	/**
	 * Conversion d'un objet java.util.Date en java.sql.Date
	 * 
	 * @param DateUtil
	 * @return java.sql.Date
	 */
	public static java.sql.Date to_Sql(Date date) {
		return DateUtil.to_Date(DateUtil.to_String(date));
	}

	/**
	 * Obtention de la date courante en chaéne de caractére suivant le format
	 * fourni
	 * 
	 * @param DateUtil
	 * @return java.lang.String
	 */
	public static String sysdate(String format) {
		java.util.GregorianCalendar unGregorianCalendar = null;
		unGregorianCalendar = new java.util.GregorianCalendar();
		return DateUtil.to_String(unGregorianCalendar.getTime(), format);
	}

	/**
	 * Obtention de la Date du jour au format en chaéne de caractére au format
	 * par défaut (yyyy-MM-dd)
	 * 
	 * @param DateUtil
	 * @return String
	 */
	public static String sysdate() {
		return DateUtil.sysdate(FORMAT_DEFAUT);
	}

	/**
	 * Méthode de test pour une date en chaéne de caractére avec le format par
	 * défaut
	 * 
	 * @param DateUtil
	 * @return boolean
	 */
	public static boolean isDate(String sdt) {
		return DateUtil.isDate(sdt, FORMAT_DEFAUT);
	}

	/**
	 * @param date1
	 * @param date2
	 * @return la différence entre deux dates comptée en semaines
	 */
	public static double ecartDate(String date1, String date2) {

		double nbSemaine = 0;

		DateFormat dateFormat = new SimpleDateFormat(FRENCH_FORMAT);
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();

		try {
			Date dat1 = dateFormat.parse(date1);
			Date dat2 = dateFormat.parse(date2);
			cal1.setTime(dat1);
			cal2.setTime(dat2);
			nbSemaine = ((cal1.getTime().getTime() - cal2.getTime().getTime()) / (24 * 60 * 60 * 1000));
			nbSemaine = nbSemaine / 7.0;
			nbSemaine = Math.abs(nbSemaine);

		} catch (ParseException ex) {
			logger.error(ex.getMessage(), ex);
		}
		return nbSemaine;
	}

	/**
	 * Retourne la date passée en paramétre sous forme de String au format
	 * indiqué.
	 * 
	 * @param date
	 *            une date.
	 * @param pattern
	 *            un pattern de formattage pour la date. Exemple : dd/MM/yyyy,
	 *            dd/MM/yy,
	 * @param locale
	 *            la locale d'affichage de la date dd-MM-yyyy, dd-MM-yy ...
	 * @return une chaéne de caractéres représentant la date passée en
	 *         paramétres au format indiqué.
	 */
	public static String getStringFromDate(Date date, String pattern) {
		String stringDate = "";
		DateFormat dateFormat = null;

		if (date == null)
			return "";

		dateFormat = new SimpleDateFormat(pattern);
		stringDate = dateFormat.format(date);

		return stringDate;
	}

	/**
	 * Extrait le mois de la date
	 * 
	 * @param date
	 *            une date
	 * @param locale
	 *            la locale d'affichage de la date
	 * @return une chaine representant le mois sur 2 chiffres
	 */
	public static String getMoisSurDeuxChiffresFromDate(Date date) {
		return getStringFromDate(date, MONTH_FORMAT);
	}

	/**
	 * Extrait le jour de la date
	 * 
	 * @param date
	 *            une date
	 * @param locale
	 *            la locale d'affichage de la date
	 * @return une chaine representant le mois sur 2 chiffres
	 */
	public static String getJourSurDeuxChiffresFromDate(Date date) {
		return getStringFromDate(date, DAY_FORMAT);
	}

	/**
	 * Extrait l annee de la date
	 * 
	 * @param date
	 *            une date
	 * @param locale
	 *            la locale d'affichage de la date
	 * @return une chaine representant l annee sur 4 chiffres
	 */
	public static String getAnneeSurQuatreChiffresFromDate(Date date) {
		return getStringFromDate(date, YEAR_FORMAT);
	}

	/**
	 * Gets the locale medium date in String format
	 * 
	 * @param Date
	 *            Medium date is dd/MM/yyyy
	 */
	public static String getLocaleDateMedium(Date date) {
		if (date == null)
			return "";
		SimpleDateFormat dateFormatter = new SimpleDateFormat(FRENCH_FORMAT);
		return dateFormatter.format(date);
	}

	/**
	 * Gets the locale medium date from a Date in String format Medium date is
	 * dd/MM/yyyy
	 */
	public static Date parseLocaleDateMedium(String date) throws ParseException {
		if ((date == null) || (date.length() == 0))
			return null;
		SimpleDateFormat dateFormatter = new SimpleDateFormat(FRENCH_FORMAT);
		dateFormatter.setLenient(false);
		return (Date) dateFormatter.parse(date);
	}

	public static boolean isBetween(Date dateToTest, Date beginDate, Date endDate, boolean sameDaysEnabled) {
		boolean between = false;

		if (beginDate != null) {
			if (endDate != null) {
				if (beginDate.before(endDate)) {
					if (beginDate.before(dateToTest) && endDate.after(dateToTest))
						between = true;
					if (sameDaysEnabled) {
						if (beginDate.equals(dateToTest) || endDate.equals(dateToTest))
							between = true;
					}
				} else {
					if (beginDate.equals(endDate) && beginDate.equals(dateToTest) && sameDaysEnabled)
						between = true;

				}
			} else {
				if ((sameDaysEnabled && beginDate.equals(dateToTest)) || dateToTest.after(beginDate))
					between = true;
			}
		}
		return between;
	}

	/**
	 * Returns <code>int value >0</code> if the year of <code>date1</code> is
	 * strictly before the year of <code>date2</code>, <code>int value =0</code>
	 * if the year of <code>date1</code> equals the year of <code>date2</code>
	 * and , <code>int value < 0</code> if the year of <code>date1</code> is
	 * strictly after the year of <code>date2</code>
	 * 
	 * @param date1
	 *            - the Date which represents the date1
	 * @param date2
	 *            - the Date which represents the date2
	 * @return <code> value > 0 </code> if and only if the year of
	 *         <code>date1</code> is strictly before the year of
	 *         <code>date2</code>. <code>0</code> if and only if the year of
	 *         <code>date1</code> equals the year of <code>date2</code>.
	 *         <code>value < 0 </code> if the year of <code>date1</code> is
	 *         after the year of <code>date2</code> OR if <code>date1</code> or
	 *         <code>date2</code> is <code>null</code>.
	 */
	public static int compareYearDate1BeforeYearDate2(Date date1, Date date2) {

		int resultat = -1;
		if (date1 != null && date2 != null) {
			String anneeDate1 = DateUtil.getAnneeSurQuatreChiffresFromDate(date1);
			String anneeDate2 = DateUtil.getAnneeSurQuatreChiffresFromDate(date2);
			resultat = -anneeDate1.compareTo(anneeDate2);
		}
		return resultat;
	}

	/**
	 * Returns <code>true</code> if the <code>date1</code> is strictly before
	 * the <code>date2</code>
	 * 
	 * @param date1
	 *            - the string which represents the date1
	 * @param date2
	 *            - the string which represents the date2
	 * @return <code>true</code> if and only if the <code>date1</code> is
	 *         strictly before the <code>date2</code>. If <code>date1</code> or
	 *         <code>date2</code> is <code>null</code> or equals <code>""</code>
	 *         , returns <code>false</code>
	 */
	public static boolean isDate1BeforeDate2(String date1, String date2) throws ParseException {
		if (StringUtil.isEmpty(date1) || StringUtil.isEmpty(date2))
			return false;
		return (DateUtil.parseLocaleDateMedium(date1).before(DateUtil.parseLocaleDateMedium(date2)));
	}

	/**
	 * Methode permettant de recuperer le min de 2 dates
	 * 
	 * @param date1
	 *            : une date
	 * @param date2
	 *            : une date
	 * @return la date la + petite
	 * @author alsa
	 */
	public static Date getMinDate(Date date1, Date date2) {
		if (date1.before(date2))
			return date1;
		return date2;
	}

	/**
	 * Methode permettant de recuperer le max de 2 dates
	 * 
	 * @param date1
	 *            : une date
	 * @param date2
	 *            : une date
	 * @return la date la + grande
	 * @author alsa
	 */
	public static Date getMaxDate(Date date1, Date date2) {
		if (date2 == null)
			return date1;
		if (date1 == null)
			return date2;
		if (date1.after(date2))
			return date1;
		return date2;
	}

	/**
	 * Methode permettant de recuperer le dernier jours du mois
	 * 
	 * 
	 * @return la date du dernier jours du mois
	 * @author
	 * @throws ParseException
	 */
	public static Date getEndCurrentMonth() {

		return modifieDate(currentDate(), 1, 0);
	}

	/**
	 * Methode permettant de recuperer le premeier jours du mois
	 * 
	 * 
	 * @return la date du dernier jours du mois
	 * @author
	 * @throws ParseException
	 */
	public static Date getStartCurrentMonth() {

		return modifieDate(currentDate(), 0, 1);
	}

	/**
	 * Methode permettant de recuperer le dernier jours du mois
	 * 
	 * 
	 * @return la date du dernier jours du mois
	 * @author
	 * @throws ParseException
	 */
	public static Date getEndDate(Date date) {

		return modifieDate(date, 1, 0);
	}

	/**
	 * Methode permettant de recuperer le premeier jours du mois
	 * 
	 * 
	 * @return la date du dernier jours du mois
	 * @author
	 * @throws ParseException
	 */
	public static Date getStartDate(Date date) {

		return modifieDate(date, 0, 1);
	}

	public static Date stringToDate(String data) {
		return stringToDate(data, FRENCH_FORMAT);
	}

	/**
	 * Méthode permettant de convertir un String en Date
	 * 
	 * @param data
	 * @param pattern
	 * @return Date
	 * @throws TechnicalException
	 */
	public static Date stringToDate(String data, String pattern) {

		if (StringUtil.isEmpty(data) || StringUtil.isEmpty(pattern))
			return null;

		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Date res = null;
		try {
			res = formatter.parse(data);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			res = null;
		}
		return res;
	}

	/**
	 * methode convertissant une date en string
	 * 
	 * @param date
	 * @return
	 */
	public static String dateToString(Date date) {
		return dateToString(date, FRENCH_FORMAT);
	}

	/**
	 * methode convertissant une date en string
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String dateToString(Date date, String pattern) {

		if (date == null)
			return null;

		String st = new String();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		st = sdf.format(date);
		return st;
	}

}