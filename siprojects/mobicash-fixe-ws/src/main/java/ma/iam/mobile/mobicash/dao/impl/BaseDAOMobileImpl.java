/*
 * @author AtoS
 */
package ma.iam.mobile.mobicash.dao.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ma.iam.mobile.mobicash.dao.BaseDAOMobile;

/**
 * The Class BaseDAOMobile.
 */
@Repository
public class BaseDAOMobileImpl implements BaseDAOMobile {

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private DataSource dataSource;

	/**
	 * Sets the mobile bscs data source.
	 * 
	 * @param fixeBscsDataSource
	 *            the new mobile bscs data source
	 */
	@Autowired
	@Qualifier("fixeBscsDataSource")
	public void setFixeBscsDataSource(final DataSource fixeBscsDataSource) {

		this.setNamedParameterJdbcTemplate(new NamedParameterJdbcTemplate(
				fixeBscsDataSource));

		this.dataSource = fixeBscsDataSource;
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplate(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
