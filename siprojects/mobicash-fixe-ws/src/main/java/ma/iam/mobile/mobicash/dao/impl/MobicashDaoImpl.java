package ma.iam.mobile.mobicash.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.bean.MobicashOpenInvoice;
import ma.iam.mobile.mobicash.constants.Constants;
import ma.iam.mobile.mobicash.dao.MobicashDao;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;
import ma.iam.mobile.mobicash.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

@Repository
public class MobicashDaoImpl extends BaseDAOMobileImpl implements MobicashDao {
	private static final Logger LOG = LoggerFactory.getLogger(MobicashDaoImpl.class);

	@Override
	public CustomerPosition getOpenInvoiceListInternet(String mobicashPrefixFixe, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {

		CustomerPosition customerPosition = new CustomerPosition();
		String sql = "{? = call INTERFACE.MTCASH_PAYMENT_IN_EDP.get_phone_invoices_by_client (?,?,?,?,?,?,?,?,?)}";
		Connection connection = null;
		CallableStatement cstmt = null;
		String codeErreur = null;
		try {
			connection = super.getDataSource().getConnection();
			cstmt = connection.prepareCall(sql);
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.setString(2, uniqueId);
			cstmt.setString(3, pin);
			cstmt.setInt(4, Constants.MOBICASH_FX_PARTNER_ID);
			cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(8, java.sql.Types.FLOAT);
			cstmt.registerOutParameter(9, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(10, java.sql.Types.VARCHAR);
			cstmt.execute();

			codeErreur = cstmt.getString(1);
			if (codeErreur.equals(Constants.MOBICASH_CODE_SUCCESS)) {
				customerPosition.setQueryOutcome(codeErreur);
				int customerId = cstmt.getInt(5);
				customerPosition.setCustomerId(customerId);
				String customerName = cstmt.getString(6);
				customerPosition.setCustomerName(customerName);
				int invcnt = cstmt.getInt(7);
				float totOpenAmount = cstmt.getFloat(8);
				customerPosition.setTotOpenAmount(totOpenAmount);

				String invlist = cstmt.getString(9);
				ArrayList<MobicashOpenInvoice> openInvoiceList = new ArrayList<MobicashOpenInvoice>();
				if (invcnt != 0 && invlist != null) {
					for (int i = 0; i < invcnt; i++) {
						MobicashOpenInvoice openInvoice = new MobicashOpenInvoice();
						String strOpenInvoice = invlist.substring(0, 77);
						invlist = invlist.substring(77);
						String ohrefnum = strOpenInvoice.substring(0, 16);
						openInvoice.setOhrefnum(ohrefnum);
						String month = strOpenInvoice.substring(16, 24);
						openInvoice.setMonth(month);
						float openAmount = Float.parseFloat(strOpenInvoice.substring(24, 39).trim());
						openInvoice.setOpenAmount(openAmount);
						int ohxact = Integer.parseInt(strOpenInvoice.substring(39, 77).trim());
						openInvoice.setOhxact(ohxact);
						openInvoiceList.add(openInvoice);
					}
				}
				customerPosition.setOpenInvoiceList(openInvoiceList);
			} else {
				customerPosition.setQueryOutcome(codeErreur);
			}
		} catch (Exception e) {
			LOG.error("error in getOpenInvoiceListInternet : " + e.getMessage());
			codeErreur = "502";
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
					LOG.error("error in getOpenInvoiceListInternet : " + e.getMessage());
					cstmt = null;
				}
			}
		}

		return customerPosition;

	}

	@Override
	public CustomerPosition getOpenInvoiceListFixe(String mobicashPrefixFixe, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {

		CustomerPosition customerPosition = new CustomerPosition();
		String sql = "{? = call INTERFACE.MTCASH_PAYMENT_EDP.get_phone_invoices_by_client (?,?,?,?,?,?,?,?,?)}";
		Connection connection = null;
		CallableStatement cstmt = null;
		String codeErreur = null;
		try {
			connection = super.getDataSource().getConnection();
			cstmt = connection.prepareCall(sql);
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.setString(2, uniqueId);
			cstmt.setString(3, pin);
			cstmt.setInt(4, Constants.MOBICASH_FX_PARTNER_ID);
			cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(8, java.sql.Types.FLOAT);
			cstmt.registerOutParameter(9, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(10, java.sql.Types.VARCHAR);
			cstmt.execute();

			codeErreur = cstmt.getString(1);
			if (codeErreur.equals(Constants.MOBICASH_CODE_SUCCESS)) {
				customerPosition.setQueryOutcome(codeErreur);
				int customerId = cstmt.getInt(5);
				customerPosition.setCustomerId(customerId);
				String customerName = cstmt.getString(6);
				customerPosition.setCustomerName(customerName);
				int invcnt = cstmt.getInt(7);
				float totOpenAmount = cstmt.getFloat(8);
				customerPosition.setTotOpenAmount(totOpenAmount);

				String invlist = cstmt.getString(9);
				ArrayList<MobicashOpenInvoice> openInvoiceList = new ArrayList<MobicashOpenInvoice>();
				if (invcnt != 0 && invlist != null) {
					for (int i = 0; i < invcnt; i++) {
						MobicashOpenInvoice openInvoice = new MobicashOpenInvoice();
						String strOpenInvoice = invlist.substring(0, 77);
						invlist = invlist.substring(77);
						String ohrefnum = strOpenInvoice.substring(0, 16);
						openInvoice.setOhrefnum(ohrefnum);
						String month = strOpenInvoice.substring(16, 24);
						openInvoice.setMonth(month);
						float openAmount = Float.parseFloat(strOpenInvoice.substring(24, 39).trim());
						openInvoice.setOpenAmount(openAmount);
						int ohxact = Integer.parseInt(strOpenInvoice.substring(39, 77).trim());
						openInvoice.setOhxact(ohxact);
						openInvoiceList.add(openInvoice);
					}
				}
				customerPosition.setOpenInvoiceList(openInvoiceList);
			} else {
				customerPosition.setQueryOutcome(codeErreur);
			}
		} catch (Exception e) {
			LOG.error("error in getOpenInvoiceListFixe : " + e.getMessage());
			codeErreur = "502";
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
					LOG.error("error in getOpenInvoiceListFixe : " + e.getMessage());
					cstmt = null;
				}
			}
		}

		return customerPosition;

	}

	@Override
	public String payInvoiceListInternet(String mobicashPrefixFixe, int customerId, String uniqueId, String pin,
			String authNum, ArrayList invoiceList)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {

		String codeErreur = null;
		String sql = "{? = call INTERFACE.MTCASH_PAYMENT_IN_EDP.bill_payment_confirmation (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		Connection connection = null;
		CallableStatement cstmt = null;
		try {
			connection = super.getDataSource().getConnection();
			connection.setAutoCommit(false);
			cstmt = connection.prepareCall(sql);
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.setString(2, "MOBICASH");
			cstmt.setInt(3, customerId);
			cstmt.setString(4, uniqueId);
			cstmt.setString(5, pin);
			cstmt.setDate(6, new java.sql.Date(System.currentTimeMillis()));
			cstmt.setString(7, authNum);
			cstmt.setString(8, "E");
			cstmt.setString(9, null);
			cstmt.setInt(10, Constants.MOBICASH_FX_PARTNER_ID);
			cstmt.setString(11, null);
			if (invoiceList != null) {
				int invoiceNbre = invoiceList.size();
				cstmt.setInt(12, invoiceNbre);
				StringBuffer buff = new StringBuffer();
				float totOpenAmount = 0;
				for (int i = 0; i < invoiceList.size(); i++) {
					MobicashOpenInvoice openInvoice = (MobicashOpenInvoice) invoiceList.get(i);
					String ohrefnum = StringUtils.rightPad(openInvoice.getOhrefnum(), 16);
					String month = openInvoice.getMonth();
					String openAmount = StringUtils.rightPad("" + openInvoice.getOpenAmount(), 15);
					totOpenAmount = totOpenAmount + openInvoice.getOpenAmount();
					String ohxact = StringUtils.rightPad("" + openInvoice.getOhxact(), 38);
					String strOpenInvoice = ohrefnum + month + openAmount + ohxact;
					buff.append(strOpenInvoice);
				}
				cstmt.setFloat(13, totOpenAmount);
				cstmt.setString(14, buff.toString());
				cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);
			} else {
				codeErreur = "107";
				return codeErreur;
			}
			cstmt.execute();
			connection.commit();
		} catch (Exception e) {
			LOG.error("error in payInvoiceListInternet : " + e.getMessage());
			codeErreur = "502";
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
					LOG.error("error in payInvoiceListInternet : " + e.getMessage());
					cstmt = null;
				}
			}
		}

		return codeErreur;

	}

	@Override
	public String payInvoiceListFixe(String mobicashPrefixFixe, int customerId, String uniqueId, String pin,
			String authNum, ArrayList invoiceList)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {

		String codeErreur = null;
		String sql = "{? = call INTERFACE.MTCASH_PAYMENT_EDP.bill_payment_confirmation (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		Connection connection = null;
		CallableStatement cstmt = null;
		try {
			connection = super.getDataSource().getConnection();
			connection.setAutoCommit(false);
			cstmt = connection.prepareCall(sql);
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.setString(2, "MOBICASH");
			cstmt.setInt(3, customerId);
			cstmt.setString(4, uniqueId);
			cstmt.setString(5, pin);
			cstmt.setDate(6, new java.sql.Date(System.currentTimeMillis()));
			cstmt.setString(7, authNum);
			cstmt.setString(8, "E");
			cstmt.setString(9, null);
			cstmt.setInt(10, Constants.MOBICASH_FX_PARTNER_ID);
			cstmt.setString(11, null);
			if (invoiceList != null) {
				int invoiceNbre = invoiceList.size();
				cstmt.setInt(12, invoiceNbre);
				StringBuffer buff = new StringBuffer();
				float totOpenAmount = 0;
				for (int i = 0; i < invoiceList.size(); i++) {
					MobicashOpenInvoice openInvoice = (MobicashOpenInvoice) invoiceList.get(i);
					String ohrefnum = StringUtils.rightPad(openInvoice.getOhrefnum(), 16);
					String month = openInvoice.getMonth();
					String openAmount = StringUtils.rightPad("" + openInvoice.getOpenAmount(), 15);
					totOpenAmount = totOpenAmount + openInvoice.getOpenAmount();
					String ohxact = StringUtils.rightPad("" + openInvoice.getOhxact(), 38);
					String strOpenInvoice = ohrefnum + month + openAmount + ohxact;
					buff.append(strOpenInvoice);
				}
				cstmt.setFloat(13, totOpenAmount);
				cstmt.setString(14, buff.toString());
				cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);
			} else {
				codeErreur = "107";
				return codeErreur;
			}
			cstmt.execute();
			connection.commit();
		} catch (Exception e) {
			LOG.error("error in payInvoiceListFixe : " + e.getMessage());
			codeErreur = "502";
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
					LOG.error("error in payInvoiceListFixe : " + e.getMessage());
					cstmt = null;
				}
			}
		}

		return codeErreur;

	}

	@Override
	public String reconcileInternet(String mobicashPrefixFixe, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {

		String codeErreur = null;
		String sql = "{? = call INTERFACE.MTCASH_PAYMENT_IN_EDP.BILL_PAYMENT_RECONCILIATION (?,?,?,?)}";
		Connection connection = null;
		CallableStatement cstmt = null;
		try {
			connection = super.getDataSource().getConnection();
			cstmt = connection.prepareCall(sql);
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.setDate(2, new java.sql.Date(date.getTime()));
			cstmt.setInt(3, operationCount);
			cstmt.setFloat(4, totalPaidAmount);
			cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
			cstmt.execute();
			codeErreur = cstmt.getString(1);
		} catch (Exception e) {
			LOG.error("error in reconcileInternet : " + e.getMessage());
			codeErreur = "502";
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
					LOG.error("error in reconcileInternet : " + e.getMessage());
					cstmt = null;
				}
			}
		}
		return codeErreur;

	}

	@Override
	public String reconcileFixe(String mobicashPrefixFixe, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {

		String codeErreur = null;
		String sql = "{? = call INTERFACE.MTCASH_PAYMENT_EDP.BILL_PAYMENT_RECONCILIATION (?,?,?,?)}";
		Connection connection = null;
		CallableStatement cstmt = null;
		try {
			connection = super.getDataSource().getConnection();
			cstmt = connection.prepareCall(sql);
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.setDate(2, new java.sql.Date(date.getTime()));
			cstmt.setInt(3, operationCount);
			cstmt.setFloat(4, totalPaidAmount);
			cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
			cstmt.execute();
			codeErreur = cstmt.getString(1);
		} catch (Exception e) {
			LOG.error("error in reconcileFixe : " + e.getMessage());
			codeErreur = "502";
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
					LOG.error("error in reconcileFixe : " + e.getMessage());
					cstmt = null;
				}
			}
		}
		return codeErreur;

	}
}
