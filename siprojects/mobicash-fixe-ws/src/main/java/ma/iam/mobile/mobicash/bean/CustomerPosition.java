/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.iam.mobile.mobicash.bean;

import java.util.ArrayList;

/**
 *
 * @author R00TiamdG
 */
public class CustomerPosition {
	private int customerId;
	private String customerName;
	private float totOpenAmount;
	private ArrayList<MobicashOpenInvoice> openInvoiceList;
	private String queryOutcome;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public float getTotOpenAmount() {
		return totOpenAmount;
	}

	public void setTotOpenAmount(float totOpenAmount) {
		this.totOpenAmount = totOpenAmount;
	}

	public ArrayList<MobicashOpenInvoice> getOpenInvoiceList() {
		return openInvoiceList;
	}

	public void setOpenInvoiceList(ArrayList<MobicashOpenInvoice> openInvoiceList) {
		this.openInvoiceList = openInvoiceList;
	}

	public String getQueryOutcome() {
		return queryOutcome;
	}

	public void setQueryOutcome(String queryOutcome) {
		this.queryOutcome = queryOutcome;
	}

	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("\n - " + "customerId :" + customerId);
		str.append("\n - " + "customerName :" + customerName);
		str.append("\n - " + "totOpenAmount :" + totOpenAmount);
		if (openInvoiceList != null && openInvoiceList.size() != 0) {
			for (int i = 0; i < openInvoiceList.size(); i++) {
				MobicashOpenInvoice mobicashOpenInvoice = openInvoiceList.get(i);
				str.append("\n - " + "MobicashOpenInvoice " + i + " : " + mobicashOpenInvoice.toString());
			}
		}
		str.append("\n - " + "ErreurCode :" + queryOutcome);
		return str.toString();
	}

}
