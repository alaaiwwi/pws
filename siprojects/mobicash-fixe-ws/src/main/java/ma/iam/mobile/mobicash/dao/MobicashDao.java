package ma.iam.mobile.mobicash.dao;

import java.util.ArrayList;
import java.util.Date;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;
import ma.iam.mobile.mobicash.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface MobicashDao extends BaseDAOMobile {

	public CustomerPosition getOpenInvoiceListInternet(String mobicashPrefixFixe, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public CustomerPosition getOpenInvoiceListFixe(String mobicashPrefixFixe, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public String payInvoiceListInternet(String mobicashPrefixFixe, int customerId, String uniqueId, String pin,
			String authNum, ArrayList invoiceList) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public String payInvoiceListFixe(String mobicashPrefixFixe, int customerId, String uniqueId, String pin,
			String authNum, ArrayList invoiceList) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public String reconcileInternet(String mobicashPrefixFixe, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public String reconcileFixe(String mobicashPrefixFixe, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException;
	
}
