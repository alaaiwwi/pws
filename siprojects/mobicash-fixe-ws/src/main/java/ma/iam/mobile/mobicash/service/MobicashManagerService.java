package ma.iam.mobile.mobicash.service;

import java.util.ArrayList;
import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;

/**
 * This class was generated by Apache CXF 2.7.0
 * 2018-11-23T12:25:02.087+01:00
 * Generated source version: 2.7.0
 */
@WebService(targetNamespace = "http://service.mobicash.mobile.iam.ma/", name = "MobicashManagerService")
public interface MobicashManagerService {
    /**
     * Web service operation
     * @throws FunctionnalException 
     */
    @WebMethod(operationName = "getOpenInvoiceList")
    public CustomerPosition getOpenInvoiceList(@WebParam(name = "domain")
    int domain, @WebParam(name = "uniqueId")
    String uniqueId, @WebParam(name = "pin")
    String pin) throws FunctionnalException;

    /**
     * Web service operation
     * @throws FunctionnalException 
     */
    @WebMethod(operationName = "payInvoiceList")
    public String payInvoiceList(@WebParam(name = "domain")
    int domain, @WebParam(name = "customerId")
    int customerId, @WebParam(name = "uniqueId")
    String uniqueId, @WebParam(name = "pin")
    String pin, @WebParam(name = "authNum")
    String authNum, @WebParam(name = "invoiceList")
    ArrayList invoiceList) throws FunctionnalException;

    /**
     * Web service operation
     * @throws FunctionnalException 
     */
    @WebMethod(operationName = "reconcile")
    public String reconcile(@WebParam(name = "domain")
    int domain, @WebParam(name = "date")
    Date date, @WebParam(name = "operationCount")
    int operationCount, @WebParam(name = "totalPaidAmount")
    float totalPaidAmount) throws FunctionnalException;
    
    @WebMethod
    @RequestWrapper(localName = "byPass", targetNamespace = "http://service.mobicash.mobile.iam.ma/", className = "ma.iam.mobile.mobicash.service.ByPass")
    @ResponseWrapper(localName = "byPassResponse", targetNamespace = "http://service.mobicash.mobile.iam.ma/", className = "ma.iam.mobile.mobicash.service.ByPassResponse")
    @WebResult(name = "statut", targetNamespace = "")
    public Integer byPass();    
}
