package ma.iam.mobile.customer.util;

import org.xml.sax.SAXException;
import javax.xml.transform.Source;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class XMLUtil {
    public static final Map<String,Validator> xsdMap = new HashMap<String, Validator>();
    public static boolean validate(String xmlFile, String schemaFile) throws  IOException,SAXException  {
        Validator validator = getXSD(schemaFile);
        validator.validate(new StreamSource(new StringReader(xmlFile)));
        return true;
    }



    public static Validator getXSD(String filename) throws FileNotFoundException, SAXException {
        if(xsdMap.containsKey(filename)){
            return xsdMap.get(filename);
        }else {
            InputStream resource = XMLUtil.class.getClassLoader().getResourceAsStream(filename);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource xsdSource = new StreamSource(resource);
            Schema schema = schemaFactory.newSchema(xsdSource);
            Validator validator = schema.newValidator();
            xsdMap.put(filename,validator);
            return validator;
        }
    }
}
