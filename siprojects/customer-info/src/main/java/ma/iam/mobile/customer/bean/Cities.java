package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Cities implements Serializable {
	
	

	private List <City> city;

	/**
	 * @return the city
	 */
	public List<City> getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(List<City> city) {
		this.city = city;
	}


}
