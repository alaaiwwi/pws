package ma.iam.mobile.customer.service.impl;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.bean.cmsint.OccType;
import ma.iam.mobile.customer.business.CheckDependecyNABiz;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.business.ByPassManagerBiz;
import ma.iam.mobile.customer.business.CustomerEditManagerBiz;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.service.CustomerEditManagerService;
import ma.iam.ws.tree.CheckDependencyBIZ;
import ma.iam.ws.tree.CheckDependencyBean;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.jws.WebParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component(value = "customerEditManagerService")
public class CustomerEditManagerServiceImpl extends SpringBeanAutowiringSupport implements CustomerEditManagerService {
    @Autowired
    private Properties git;
    /**
     * The customer Edits manager biz.
     */
    @Autowired
    private ByPassManagerBiz byPassManagerBiz;
    @Autowired
    private CheckDependecyNABiz checkDependecyNABiz;
    @Autowired
    private CustomerEditManagerBiz customerEditManagerBiz;
    @Autowired
    private CheckDependencyBIZ checkDependencyBIZ;
    @Autowired
    private JDBCPing pingMobileBscs;

    public Integer modeByPass() {
        return byPassManagerBiz.modeByPass();
    }

    @Override
    public String infos() {
        return "branch: "+ git.getProperty("git.branch") +
                ", closest.tag.name : "+git.getProperty("git.closest.tag.name") +
                ", build.time : "+git.getProperty("git.build.time") +
                ", commit.id : "+ git.getProperty("git.commit.id");
    }


    @Override
    public Long createCustomer(CreateCustomerRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException {
        // TODO Auto-generated method stub
        if (request != null && request.getCustomer() != null && request.getCustomer().getContracts() != null && request.getCustomer().getContracts().size() > 0) {
            List<ContractCreate> contracts = request.getCustomer().getContracts();
            String message="";
            try {
                for (ContractCreate contractCreate : contracts
                ) {
                    if (contractCreate.getServices() != null) {
                        Set<Long> longSet = new HashSet<Long>();
                        for (Service service : contractCreate.getServices()
                        ) {
                            longSet.add(Long.valueOf(service.getSncode()));
                        }
                        CheckDependencyBean checkDependencyBean = checkDependencyBIZ.getCheckListDependency(longSet);
                        if (!checkDependencyBean.isStatus())
                            message+="tmcode : "+contractCreate.getRate_plan_code()+" : "+checkDependencyBean.toString()+"\n";

                    }
                }
                if(!"".equals(message.trim())){
                    throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_DEPENDECY,message);
                }
            }catch (Exception e){
                if (e instanceof SyntaxiqueException)
                    throw (SyntaxiqueException) e;
                else
                    throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_DEPENDECY);
            }
        }
        if(request.isCheckOnly())
            return new Long(-1);
        else
            return customerEditManagerBiz.createCustomer(request.getCustomer(), request.getUserName());
    }

    @Override
    public Long identifieClient(IdentifyCustomerRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException {
        // TODO Auto-generated method stub
        return customerEditManagerBiz.identifieClient(request.getCustomer(), request.getUserName());
    }

    @Override
    public SMSResponse genererSMS(SMSRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException {
        // TODO Auto-generated method stub
        return customerEditManagerBiz.genererSMS(request.getNd(), request.getUserName());
    }

    @Override
    public Long changementSIM(SIMRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException {
        // TODO Auto-generated method stub
        return customerEditManagerBiz.changementSIM(request.getSim(), request.getUserName());
    }


    @Override
    public String savePreOrderPoP(PreOrderPoPRequest request)
            throws FunctionnalException, TechnicalException, SyntaxiqueException {
        // TODO Auto-generated method stub
        return customerEditManagerBiz.savePreOrderPoP(request);
    }

    @Override
    public Long addOcc(OCCRequest occRequest) throws FunctionnalException, TechnicalException, SyntaxiqueException {
        return customerEditManagerBiz.addOcc(occRequest);
    }

}
