package ma.iam.mobile.customer.bean;

import javax.jws.WebParam;
import java.io.Serializable;

public class VerifierEligibiliteRequest implements Serializable {

	private String coId;
	private String cuId;

	public String getCoId() {
		return coId;
	}
	/**
	 * @param coId the coId to set
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}
	/**
	 * @return the cuId
	 */
	public String getCuId() {
		return cuId;
	}
	/**
	 * @param cuId the cuId to set
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}
}
