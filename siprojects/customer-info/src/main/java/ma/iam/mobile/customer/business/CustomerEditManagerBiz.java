package ma.iam.mobile.customer.business;

import java.util.List;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.bean.cmsint.OccType;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;

public interface CustomerEditManagerBiz {
	
	Long createCustomer(Customer customer, String userName) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	Long identifieClient(CustomerUpdate customer, String userName) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	SMSResponse genererSMS(String nd, String username) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	Long changementSIM(SIMChange sim, String username) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
	String savePreOrderPoP(PreOrderPoPRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException;


    Long addOcc(OCCRequest occRequest)throws FunctionnalException, TechnicalException, SyntaxiqueException ;
}
