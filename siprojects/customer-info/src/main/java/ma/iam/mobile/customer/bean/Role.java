package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Role implements Serializable {
	
	
	private String role;

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

}
