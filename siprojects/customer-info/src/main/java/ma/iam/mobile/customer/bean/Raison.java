package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Raison implements Serializable {

	private String id;
	private String status;
	private String rsShdes;
	private String rsDes;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the rsShdes
	 */
	public String getRsShdes() {
		return rsShdes;
	}
	/**
	 * @param rsShdes the rsShdes to set
	 */
	public void setRsShdes(String rsShdes) {
		this.rsShdes = rsShdes;
	}
	/**
	 * @return the rsDes
	 */
	public String getRsDes() {
		return rsDes;
	}
	/**
	 * @param rsDes the rsDes to set
	 */
	public void setRsDes(String rsDes) {
		this.rsDes = rsDes;
	}
	
}
