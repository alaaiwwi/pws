package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class CustomerRequest implements Serializable {
	
	private String ID_TYPE;
	private String PASSPORTNO;
	private String MSISDN;
	private String CUSTCODE;
	private String SM_SERIALNUM;
	private String FIRST_NAME;
	private String LASTNAME;
	private String CCU;
	private String NOM;
	private String PRENOM;
	


	/**
	 * @return the nOM
	 */
	public String getNOM() {
		return NOM;
	}


	/**
	 * @param nOM the nOM to set
	 */
	public void setNOM(String nOM) {
		NOM = nOM;
	}


	/**
	 * @return the pRENOM
	 */
	public String getPRENOM() {
		return PRENOM;
	}


	/**
	 * @param pRENOM the pRENOM to set
	 */
	public void setPRENOM(String pRENOM) {
		PRENOM = pRENOM;
	}


	/**
	 * @return the iD_TYPE
	 */
	public String getID_TYPE() {
		return ID_TYPE;
	}


	/**
	 * @param iD_TYPE the iD_TYPE to set
	 */
	public void setID_TYPE(String iD_TYPE) {
		ID_TYPE = iD_TYPE;
	}


	/**
	 * @return the pASSPORTNO
	 */
	public String getPASSPORTNO() {
		return PASSPORTNO;
	}


	/**
	 * @param pASSPORTNO the pASSPORTNO to set
	 */
	public void setPASSPORTNO(String pASSPORTNO) {
		PASSPORTNO = pASSPORTNO;
	}


	/**
	 * @return the mSISDN
	 */
	public String getMSISDN() {
		return MSISDN;
	}


	/**
	 * @param mSISDN the mSISDN to set
	 */
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}


	/**
	 * @return the cUSTCODE
	 */
	public String getCUSTCODE() {
		return CUSTCODE;
	}


	/**
	 * @param cUSTCODE the cUSTCODE to set
	 */
	public void setCUSTCODE(String cUSTCODE) {
		CUSTCODE = cUSTCODE;
	}


	/**
	 * @return the sM_SERIALNUM
	 */
	public String getSM_SERIALNUM() {
		return SM_SERIALNUM;
	}


	/**
	 * @param sM_SERIALNUM the sM_SERIALNUM to set
	 */
	public void setSM_SERIALNUM(String sM_SERIALNUM) {
		SM_SERIALNUM = sM_SERIALNUM;
	}


	/**
	 * @return the fIRST_NAME
	 */
	public String getFIRST_NAME() {
		return FIRST_NAME;
	}


	/**
	 * @param fIRST_NAME the fIRST_NAME to set
	 */
	public void setFIRST_NAME(String fIRST_NAME) {
		FIRST_NAME = fIRST_NAME;
	}


	/**
	 * @return the lASTNAME
	 */
	public String getLASTNAME() {
		return LASTNAME;
	}


	/**
	 * @param lASTNAME the lASTNAME to set
	 */
	public void setLASTNAME(String lASTNAME) {
		LASTNAME = lASTNAME;
	}


	/**
	 * @return the cCU
	 */
	public String getCCU() {
		return CCU;
	}


	/**
	 * @param cCU the cCU to set
	 */
	public void setCCU(String cCU) {
		CCU = cCU;
	}


	public CustomerRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

}
