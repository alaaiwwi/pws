package ma.iam.mobile.customer.business.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.business.CustomerInfoManagerBiz;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.controller.CustomerInfoController;
import ma.iam.mobile.customer.dao.CustomerInfoDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

@Component(value = "customerInfoManagerBiz")
public class CustomerInfoManagerBizImpl implements CustomerInfoManagerBiz {
	public static final String NUMERIQUE_REGEX = "\\d+";
	public static final String ALPHANUM_REGEX = "^[a-zA-Z0-9]+$";
	public static final String ALPHA_REGEX = "^[a-zA-ZéÉàÀèÈùâÂêÊîÎôÔûÛëËïÏüÜç ’\\_]+$";
	public static final String CUSTCODE_REGEX = "^\\d+(\\.\\d+)+$";
	@Autowired
	CustomerInfoController customerInfoController;

	@Autowired
	private Properties config;

	@Autowired
	CustomerInfoDao customerInfoDao;

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory.getLogger(CustomerInfoManagerBizImpl.class);

	@Override
	public List<Invoice> getListUnpaidInvoices(ListUnpaidInvoicesRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		// check syntaxique
		if (StringUtil.isEmpty(request.getCustcode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_UI_INVALIDE_CUST_CODE);
		}
		if (StringUtil.isEmpty(request.getOhstatus()) || !request.getOhstatus().matches(ALPHA_REGEX) || request.getOhstatus().length()!=2) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_OHSTATUS);
		}
		if (!StringUtil.isEmpty(request.getCustcode())){ 
			if(!request.getCustcode().matches(CUSTCODE_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_CUSTCODE_STRUCTURE);
			}
			else
				if (!customerInfoDao.isCustomerExist(request.getCustcode())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_CI_CUSTOMER_NOT_FOUND);
				}
			
	}
		return customerInfoDao.getUnpaidInvoices(request.getCustcode(), request.getOhstatus());

	}
	
	@Override
	public List<Operation> verifierEligibilite(String nd, String coId, String cuId)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {


		if (!StringUtil.isEmpty(nd) && !StringUtil.isEmpty(coId)) {
			
		}
		Integer coIdFromNd = customerInfoDao.getCoIDFromDnNum(nd);
		if(coIdFromNd != null && !coIdFromNd.toString().equals(coId)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2);
		}
		
		return customerInfoDao.verifierEligibilite(nd, coId, cuId);

	}
	
	@Override
	@Cacheable(value="refrential", key="{'listeRateplanServicesParameters', #tmcode, #foId }")
	public List<ServiceDTO> listeRateplanServicesParameters(String tmcode, String foId)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {
		
		return customerInfoDao.listeRateplanServicesParameters(tmcode, foId);

	}

	@Override
	@Cacheable(value="refrential", key="{'listeCategoryPlanTarifaire',#prgcode}")
	public List<OutPlanTarifaire> listeCategoryPlanTarifaire(String prgcode) 
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoDao.listeCategoryPlanTarifaire(prgcode);

	}

	@Override
	@Cacheable(value="refrential", key="{'listePlanTarifaire',#areaID}")
	public List<OutCategorie> listePlanTarifaire(String areaID)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoDao.listePlanTarifaire(areaID);

	}
	
	@Override
	@Cacheable(value="refrential", key="{'listeForfaits',#tmcode,#prgcode}")
	public List<Forfait> listeForfaits(String tmcode,String prgcode)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoDao.listeForfaits(tmcode,prgcode);

	}

	@Override
	public List<ParameterValues> listePack(String coId, String tmcode)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoDao.listePack(coId, tmcode);

	}

	@Override
	public DureeEngagementResponse listeDureeEngagement(String coId, String tmcode, String sncode,String gammeForfait, String identifiantForfait)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoDao.listeDureeEngagement(coId, tmcode,sncode,gammeForfait,identifiantForfait);

	}

	@Override
	public List<SimValidate> valideSIM(String sim, List<String> bo)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoDao.valideSIM(sim, bo);

	}	
	
	@Override
	public boolean verifierExistanceRC(String rc) throws FunctionnalException, TechnicalException {
		
		return customerInfoDao.verifierExistanceRC(rc);
		
	}
	
	@Override
	public boolean verifierCodeGenere(VerifierCodeGenereRequest request) throws FunctionnalException, TechnicalException {
		
		return customerInfoDao.verifierCodeGenere(request);
		
	}

	@Override
	public boolean isBlackList(CustomerRequest customer) throws FunctionnalException, TechnicalException {
		// check syntaxique
		if (!StringUtil.isEmpty(customer.getCUSTCODE())) {
			//throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_BL_INVALIDE_CUST_CODE);
			//}
			// check client

			if (!customerInfoDao.isCustomerExist(customer.getCUSTCODE())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_BL_CUSTOMER_NOT_FOUND);
			}
		}
		//if (!customerInfoDao.isBlackList(customer.getCustCode())) {
			return customerInfoDao.isBlackList(customer.getCUSTCODE(),customer.getPASSPORTNO());
		//}

		//return false;
	}

	@Override
	public List<CustomerResponse> getCustomerInfo(CustomerRequest customer) throws FunctionnalException, TechnicalException, SyntaxiqueException {

			if (!StringUtil.isEmpty(customer.getID_TYPE())){
				if(!customer.getID_TYPE().matches(NUMERIQUE_REGEX)) {
					throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_ID_TYPE_NUMERIQUE);
				}
				else
					if (!customerInfoDao.isDnType(customer)) {
						throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_ID_TYPE_INEXISTANT);
					}
			}
				
				
			
			/*if (!StringUtil.isEmpty(customer.getPASSPORTNO()) && !customer.getPASSPORTNO().matches(ALPHANUM_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_PASSPORTNO_ALPHANUMERIQUE);
			}*/
			
			if (!StringUtil.isEmpty(customer.getCUSTCODE())){ 
					if(!customer.getCUSTCODE().matches(CUSTCODE_REGEX)) {
						throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_CUSTCODE_STRUCTURE);
					}
					else
						if (!customerInfoDao.isCustomerExist(customer.getCUSTCODE())) {
							throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_CI_CUSTOMER_NOT_FOUND);
						}
					
			}
			
			
			if (!StringUtil.isEmpty(customer.getMSISDN())){
				String nd = validateND(customer.getMSISDN());
				if(nd == null) {
					throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_MSISDN_NUMEROTATION);
				}
//				else{
//					customer.setMSISDN(nd);
//
//					if (!customerInfoDao.isCustomerND(customer)) {
//						throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE);
//					}
//					
//				}
			}
					
					
			if (!StringUtil.isEmpty(customer.getSM_SERIALNUM())){
				if(!customer.getSM_SERIALNUM().matches(NUMERIQUE_REGEX)) {
					throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_SIM_ALPHANUMERIQUE);
				}
//				else
//					if (!customerInfoDao.isCustomerSim(customer)) {
//						throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2);
//					}
			}
				
			
			if (!StringUtil.isEmpty(customer.getFIRST_NAME()) && !customer.getFIRST_NAME().matches(ALPHA_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_NOM_PRENOM_ALPHA);
			}
			
			if (!StringUtil.isEmpty(customer.getLASTNAME()) && !customer.getLASTNAME().matches(ALPHA_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_NOM_PRENOM_ALPHA);
			}
			
			if (!StringUtil.isEmpty(customer.getCCU()) && !customer.getCCU().matches(ALPHANUM_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_CCU_ALPHANUMERIQUE);
			}
			
			if(StringUtil.isEmpty(customer.getMSISDN()) && StringUtil.isEmpty(customer.getCUSTCODE()) 
					&& StringUtil.isEmpty(customer.getSM_SERIALNUM())  && StringUtil.isEmpty(customer.getCCU()) ){
				
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_RECHEARCH_INVALID);
				
			}
		
		
		return customerInfoDao.getCustomerInfo(customer);
	}@Override
	public List<CustFindContractWS> getAllContracts(AllContractsRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException {

		LOG.info("-> findContract: custcode , customer_id ", request.getCustcode() + " "
				+ request.getCustomerId());
		List<CustFindContractWS> list = null;
		if (GRCStringUtil.isNullOrEmpty(request.getCustomerId())
				&& GRCStringUtil.isNullOrEmpty(request.getCustcode())) {
			throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_RECHEARCH_INVALID);

		}
		String customerId = request.getCustomerId();
		if (request.isHierarchique()) {
			if (GRCStringUtil.isNullOrEmpty(request.getCustomerId())) {
				customerId = customerInfoDao.getCustomerByCustCode(request.getCustcode());
			}
			if (!customerId.matches(NUMERIQUE_REGEX))
				return list;
			List<String> listCustomer = new ArrayList<String>();
			listCustomer.add(customerId);
			List<String> listCustomerChild = new ArrayList<String>();
			listCustomerChild.add(customerId);
			do {

				List<Map<String, Object>> listMap = customerInfoDao
						.findChildCustomer(listCustomerChild);
				listCustomerChild = new ArrayList<String>();
				for (Map<String, Object> map : listMap) {
					for (Object obj : map.values()) {
						listCustomerChild.add(String.valueOf(obj));
					}
				}
				listCustomer.addAll(listCustomerChild);
			} while (listCustomerChild != null && !listCustomerChild.isEmpty());
			list = customerInfoDao.findContract(listCustomer, request.getNd(), request.getCoId());
		} else {
			list = customerInfoDao.findContract(request.getCustcode(), customerId, request.getNd(), request.getCoId());
		}
		return list;	
}
	
	
	public List<CustSearchWS> searchCustomer(SearchCustomerWS searchCustomerWS,
			Integer product) throws FunctionnalException {

		LOG.info("-> searchCustomer: searchCustomerWS {}",
				searchCustomerWS.toString());
		if (GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCo_id())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS
						.getCodeCategorie())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNd())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNom())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS
						.getNumPieceIdentite())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getPrenom())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getVille())
				&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCustCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_RECHEARCH_INVALID);

		}
		try {
			List<CustSearchWS> listCustomers = customerInfoDao
					.searchCustomers(searchCustomerWS);
//			if (product != 2) {
//				List<Long> list = new ArrayList<Long>();
//				for (CustSearchWS custSearchWS : listCustomers) {
//					if (custSearchWS.getCustomerId().matches(NUMERIQUE_REGEX))
//						list.add(new Long(custSearchWS.getCustomerId()));
//				}
//
//				List<CustSearchWS> listAgenceCustomers = customerInfoDao
//						.searchAgenceCustomers(list);
//
//				for (CustSearchWS custSearchWS : listCustomers) {
//					for (CustSearchWS cust : listAgenceCustomers) {
//						if (cust.getCustomerId().equals(
//								custSearchWS.getCustomerId())) {
//							custSearchWS.setCodeAgence(cust.getCodeAgence());
//							custSearchWS.setLibelleAgence(cust
//									.getLibelleAgence());
//							break;
//						}
//					}
//				}
//			}
			return listCustomers;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"veuillez contactez l'administrateur");
		}
	}
	


	@Override
	@Cacheable(value="refrential" ,key = "'getConfigAll'")
	public ConfigAll getConfigAll() throws FunctionnalException, TechnicalException {

		return customerInfoDao.getConfigAll();
	}
	

	@Override
	public List<IdentifierType> getAllIdentifierType() throws FunctionnalException, TechnicalException {

		return customerInfoDao.getAllIdentifierType();
	}

	@Override
	public Sim getSIMActuel(String nd) throws FunctionnalException, TechnicalException {

		return customerInfoDao.getSIMActuel(nd);
	}

	@Override
	public Eng engagementEnCours(String nd) throws FunctionnalException, TechnicalException {

		return customerInfoDao.engagementEnCours(nd);
	}

	@Override
	@Cacheable(value="refrential" , key = "'listeRaisonsModification'")
	public List<Raison> listeRaisonsModification() throws FunctionnalException, TechnicalException {

		return customerInfoDao.listeRaisonsModification();
	}

	@Override
	public List<Raison> getRaisonmodificationSIM() throws FunctionnalException, TechnicalException {

		return customerInfoDao.getRaisonmodificationSIM();
	}

	@Override
	public List<Raison> statutRaisonClient() throws FunctionnalException, TechnicalException {

		return customerInfoDao.statutRaisonClient();
	}

	@Override
	public List<Language> getAllLanguages() throws FunctionnalException, TechnicalException {

		return customerInfoDao.getAllLanguages();
	}

	@Override
	public List<DirectoryNumber> rechercheNumeroAppel(DirectoryNumber dnSearch) throws FunctionnalException, TechnicalException {

		return customerInfoDao.rechercheNumeroAppel(dnSearch);
	}

    @Override
    public DirectoryNumber prolongerNumeroAppel(String nd, String minutesToExtend) throws FunctionnalException, TechnicalException {
        return customerInfoDao.prolongerNumeroAppel(nd, minutesToExtend);
    }

    @Override
	public List<City> getAllCities() throws FunctionnalException, TechnicalException {

		return customerInfoDao.getAllCities();
	}

	@Override
	public List<Segment> getAllSegments() throws FunctionnalException, TechnicalException {

		return customerInfoDao.getAllSegments();
	}

	@Override
	public List<Category> getListCategoryBySegment(String segmentCode) throws FunctionnalException, TechnicalException {
		return customerInfoDao.getListCategoryBySegment(segmentCode);
	}
	
	public List<PaymentType> getAllPaymentType() throws FunctionnalException, TechnicalException {
		return customerInfoDao.getAllPaymentType();
	}
	
	public String validateND(String nd) {
		
		String ndTemp = nd;
		if (ndTemp.startsWith("+")){
			ndTemp = ndTemp.substring(1);
			if (!ndTemp.startsWith("2126") && !ndTemp.startsWith("2127"))
				return null;
		}
		else
			if (!ndTemp.startsWith("06") && !ndTemp.startsWith("07")){
				if(!ndTemp.startsWith("2126") && !ndTemp.startsWith("2127")){
					return null;
				}
			}
			else
				ndTemp = "212".concat(ndTemp.substring(1));
		
		if(!ndTemp.matches(NUMERIQUE_REGEX)){
			return null;		
		}
		if (ndTemp.length() != 12)
			return null;
		
	return ndTemp;
}

	@Override
	public List<Country> getAllCountries() throws FunctionnalException,
			TechnicalException {
		return customerInfoDao.getAllCountries();
	}

	@Override
	public List<Title> getAllTitles() throws FunctionnalException,
			TechnicalException {
		return customerInfoDao.getAllTitles();
	}

}
