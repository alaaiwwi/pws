package ma.iam.mobile.customer.dao;

import java.util.List;
import java.util.Map;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface CustomerInfoDao extends BaseDAOMobile {

	ContractPersist getContractByDnNum(String nd);

	boolean isServiceInternetActif(String coId);

	void insertCMS(String coID, String custID, String ND, String cmdData2);

	List<ServiceDTO> listeRateplanServicesParameters(String tmcode, String foId)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	List<Forfait> listeForfaits(String tmcode,String prgcode) throws FunctionnalException;
	
	Integer getCoIDFromDnNum(String nd) throws FunctionnalException;
	
	List<Operation> verifierEligibilite(String nd, String coId, String cuId) throws FunctionnalException;

	List<OutCategorie> listePlanTarifaire(String areaID) throws FunctionnalException;

	List<OutPlanTarifaire> listeCategoryPlanTarifaire(String prgcode) throws FunctionnalException;

	boolean verifierExistanceRC(String rc);

	boolean isCustomerExist(String custCode);

	DureeEngagementResponse listeDureeEngagement(String coId, String tmcode ,String sncode, String gammeForfait, String identifiantForfait) throws FunctionnalException;

	List<ParameterValues> listePack(String coId, String tmcode) throws FunctionnalException;

	List<SimValidate> valideSIM(String sim, List<String> bo) throws FunctionnalException;

	List<Invoice> getUnpaidInvoices(String custcode, String ohstatus);

	boolean verifierCodeGenere(VerifierCodeGenereRequest request);

	Eng engagementEnCours(String nd);

	Sim getSIMActuel(String nd);

	ConfigAll getConfigAll();

	boolean isBlackList(String custCode,String cin) throws FunctionnalException;

	List<CustomerResponse> getCustomerInfo(CustomerRequest cust) throws FunctionnalException;
	List<Contract> getAllContracts(ContractRequest cust) throws FunctionnalException;

	List<IdentifierType> getAllIdentifierType();

	List<Language> getAllLanguages();

	List<Raison> statutRaisonClient();

	List<Raison> listeRaisonsModification();

	List<Raison> getRaisonmodificationSIM();

	List<City> getAllCities();

	List<DirectoryNumber> rechercheNumeroAppel(DirectoryNumber dnSearch) throws FunctionnalException;

	DirectoryNumber prolongerNumeroAppel (String nd, String minutesToExtend) throws FunctionnalException;

	List<Segment> getAllSegments();
	
	List<Category> getListCategoryBySegment(String segmentCode);
	
	List<PaymentType> getAllPaymentType();	
	
	List<Country> getAllCountries();	
	
	List<Title> getAllTitles();	


	boolean isCustomerND(CustomerRequest cust);

	boolean isCustomerSim(CustomerRequest cust);

	boolean isDnType(CustomerRequest cust);
	
	
	
	
	String getCustomerByCustCode(String custcode);
	
	public List<Map<String, Object>> findChildCustomer(
			List<String> list);

	List<CustFindContractWS> findContract(String custcode, String customerId,
			String nd, String coId);

	List<CustFindContractWS> findContract(List<String> listCustomer, String nd,
			String coId);
	List<CustSearchWS> searchCustomers(SearchCustomerWS searchCustomerWS);

	List<Map<String, Object>> loadDependecies ();


}
