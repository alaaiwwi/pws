package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Raisons implements Serializable {
	
	

	private List <Raison> raison;

	/**
	 * @return the raison
	 */
	public List<Raison> getRaison() {
		return raison;
	}

	/**
	 * @param raison the raison to set
	 */
	public void setRaison(List<Raison> raison) {
		this.raison = raison;
	}

	
}
