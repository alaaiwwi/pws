package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class PaymentTypes implements Serializable {
	
	

	private List <PaymentType> paymentType;

	/**
	 * @return the paymentType
	 */
	public List<PaymentType> getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(List<PaymentType> paymentType) {
		this.paymentType = paymentType;
	}

	
}
