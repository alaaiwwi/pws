package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class ParametreServiceDTO implements Serializable {

	  private String code;
      private String id;
      private String label;
      private String valeurParDefaut;
      private String typeValeur;
      private List<String> listValeur;
      private Boolean obligatoire;
      private String no;
      private String sncode;
      private String tmcode;
      private String dataType;
      private String parameterType;
      
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the valeurParDefaut
	 */
	public String getValeurParDefaut() {
		return valeurParDefaut;
	}
	/**
	 * @param valeurParDefaut the valeurParDefaut to set
	 */
	public void setValeurParDefaut(String valeurParDefaut) {
		this.valeurParDefaut = valeurParDefaut;
	}
	/**
	 * @return the typeValeur
	 */
	public String getTypeValeur() {
		return typeValeur;
	}
	/**
	 * @param typeValeur the typeValeur to set
	 */
	public void setTypeValeur(String typeValeur) {
		this.typeValeur = typeValeur;
	}
	/**
	 * @return the listValeur
	 */
	public List<String> getListValeur() {
		return listValeur;
	}
	/**
	 * @param listValeur the listValeur to set
	 */
	public void setListValeur(List<String> listValeur) {
		this.listValeur = listValeur;
	}
	/**
	 * @return the obligatoire
	 */
	public Boolean getObligatoire() {
		return obligatoire;
	}
	/**
	 * @param obligatoire the obligatoire to set
	 */
	public void setObligatoire(Boolean obligatoire) {
		this.obligatoire = obligatoire;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}

	public String getSncode() {
		return sncode;
	}

	public void setSncode(String sncode) {
		this.sncode = sncode;
	}

	public String getTmcode() {
		return tmcode;
	}

	public void setTmcode(String tmcode) {
		this.tmcode = tmcode;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getParameterType() {
		return parameterType;
	}

	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}
}
