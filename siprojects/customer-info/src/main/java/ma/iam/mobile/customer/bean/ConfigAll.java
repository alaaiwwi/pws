package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class ConfigAll implements Serializable {

	private IdTypes idTypes;
	private Languages languages;
	private Raisons reasons;
	private Segments segments;
	private PaymentTypes paymentType;
	private Cities cities;
	private Countries countries;
	private Titles titles;
	private DnTypes dnTypes;
	/**
	 * @return the idTypes
	 */
	public IdTypes getIdTypes() {
		return idTypes;
	}
	/**
	 * @param idTypes the idTypes to set
	 */
	public void setIdTypes(IdTypes idTypes) {
		this.idTypes = idTypes;
	}
	/**
	 * @return the languages
	 */
	public Languages getLanguages() {
		return languages;
	}
	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(Languages languages) {
		this.languages = languages;
	}
	/**
	 * @return the reasons
	 */
	public Raisons getReasons() {
		return reasons;
	}
	/**
	 * @param reasons the reasons to set
	 */
	public void setReasons(Raisons reasons) {
		this.reasons = reasons;
	}
	/**
	 * @return the segments
	 */
	public Segments getSegments() {
		return segments;
	}
	/**
	 * @param segments the segments to set
	 */
	public void setSegments(Segments segments) {
		this.segments = segments;
	}
	/**
	 * @return the paymentType
	 */
	public PaymentTypes getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(PaymentTypes paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the cities
	 */
	public Cities getCities() {
		return cities;
	}
	/**
	 * @param cities the cities to set
	 */
	public void setCities(Cities cities) {
		this.cities = cities;
	}
	/**
	 * @return the countries
	 */
	public Countries getCountries() {
		return countries;
	}
	/**
	 * @param countries the countries to set
	 */
	public void setCountries(Countries countries) {
		this.countries = countries;
	}
	/**
	 * @return the titles
	 */
	public Titles getTitles() {
		return titles;
	}
	/**
	 * @param titles the titles to set
	 */
	public void setTitles(Titles titles) {
		this.titles = titles;
	}
	/**
	 * @return the dnTypes
	 */
	public DnTypes getDnTypes() {
		return dnTypes;
	}
	/**
	 * @param dnTypes the dnTypes to set
	 */
	public void setDnTypes(DnTypes dnTypes) {
		this.dnTypes = dnTypes;
	}
	
	
}
