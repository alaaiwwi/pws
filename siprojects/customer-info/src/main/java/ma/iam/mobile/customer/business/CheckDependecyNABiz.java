package ma.iam.mobile.customer.business;

import ma.iam.mobile.customer.bean.ContractCreate;
import ma.iam.mobile.customer.bean.Service;
import ma.iam.mobile.customer.bean.ServiceDependency;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CheckDependecyNABiz {

    boolean checkDependecy ( String tmcode,List<Service> services) throws SyntaxiqueException;;
    Map<String, ServiceDependency> loadDependecies(List<Map<String, Object>>rowDependecies);

}
