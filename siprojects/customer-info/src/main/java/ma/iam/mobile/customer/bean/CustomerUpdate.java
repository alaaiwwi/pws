package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class CustomerUpdate implements Serializable {
	
	
	private String ticketNo;
    private String coID;
    
    private String titre;
    private String id;
	private String nationality;
    private String nom;
    private String prenom;
    private String rue;

    private String quartier;
    private String ville;
    private String pays;
    private String zipCode;
    private String addrComp;
    private String typeIdentif;
    private String identif;
    private String sex;

	/**
	 * @return the ticketNo
	 */
	public String getTicketNo() {
		return ticketNo;
	}
	/**
	 * @param ticketNo the ticketNo to set
	 */
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	/**
	 * @return the coID
	 */
	public String getCoID() {
		return coID;
	}
	/**
	 * @param coID the coID to set
	 */
	public void setCoID(String coID) {
		this.coID = coID;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}
	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * @return the rue
	 */
	public String getRue() {
		return rue;
	}
	/**
	 * @param rue the rue to set
	 */
	public void setRue(String rue) {
		this.rue = rue;
	}
	/**
	 * @return the quartier
	 */
	public String getQuartier() {
		return quartier;
	}
	/**
	 * @param quartier the quartier to set
	 */
	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}
	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	public String getPays() {
		return pays;
	}
	/**
	 * @param pays the pays to set
	 */
	public void setPays(String pays) {
		this.pays = pays;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the addrComp
	 */
	public String getAddrComp() {
		return addrComp;
	}
	/**
	 * @param addrComp the addrComp to set
	 */
	public void setAddrComp(String addrComp) {
		this.addrComp = addrComp;
	}
	/**
	 * @return the typeIdentif
	 */
	public String getTypeIdentif() {
		return typeIdentif;
	}
	/**
	 * @param typeIdentif the typeIdentif to set
	 */
	public void setTypeIdentif(String typeIdentif) {
		this.typeIdentif = typeIdentif;
	}
	/**
	 * @return the identif
	 */
	public String getIdentif() {
		return identif;
	}
	/**
	 * @param identif the identif to set
	 */
	public void setIdentif(String identif) {
		this.identif = identif;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
