package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class OutService implements Serializable {

	 private String sncode;
	 private String core;
	 private String service_package_code;    
     private List<OutParameter> parameters;
     

     /**
	 * @return the core
	 */
	public String getCore() {
		return core;
	}
	/**
	 * @param core the core to set
	 */
	public void setCore(String core) {
		this.core = core;
	}
	
	/**
	 * @return the sncode
	 */
	public String getSncode() {
		return sncode;
	}
	/**
	 * @param sncode the sncode to set
	 */
	public void setSncode(String sncode) {
		this.sncode = sncode;
	}
	/**
	 * @return the service_package_code
	 */
	public String getService_package_code() {
		return service_package_code;
	}
	/**
	 * @param service_package_code the service_package_code to set
	 */
	public void setService_package_code(String service_package_code) {
		this.service_package_code = service_package_code;
	}
	/**
	 * @return the parameters
	 */
	public List<OutParameter> getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<OutParameter> parameters) {
		this.parameters = parameters;
	}
	
}
