package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Contract implements Serializable {

	private String msisdn;
	private String status;
	private String tmShdes;
	private String tmDes;

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the tmShdes
	 */
	public String getTmShdes() {
		return tmShdes;
	}

	/**
	 * @param tmShdes the tmShdes to set
	 */
	public void setTmShdes(String tmShdes) {
		this.tmShdes = tmShdes;
	}

	/**
	 * @return the tmDes
	 */
	public String getTmDes() {
		return tmDes;
	}

	/**
	 * @param tmDes the tmDes to set
	 */
	public void setTmDes(String tmDes) {
		this.tmDes = tmDes;
	}

	public Contract() {
		super();
		// TODO Auto-generated constructor stub
	}

}
