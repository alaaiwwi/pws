package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Languages implements Serializable {
	
	

	private List <Language> language;

	/**
	 * @return the language
	 */
	public List<Language> getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(List<Language> language) {
		this.language = language;
	}

	
}
