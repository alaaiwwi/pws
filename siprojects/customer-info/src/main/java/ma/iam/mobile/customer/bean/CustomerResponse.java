package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class CustomerResponse implements Serializable {

	private String customerID;
	private String IsPaymentResponsable;

	private String custCode;
	private String agencyCode;
	private String ccu;
	private String categorieClient;
	private String nom;
	private String prenom;
	private String typeIdentifiant;
	private String identifiant;
	private String agenceClient;
	private String adresse;
	private String ville;
	private String pays;
	private String email;
	private String prgcode;
	private boolean flagNomPrenom;
	private String dnStatus;
	private String isIdentified;

	private String isBalckListed ;
	private ContactLegal contactLegal;
	private String billingAccountId;
	private String ccseq;
	private String ccsex;


	  
	private Category category;
	private Agency agency;
	


	/**
	 * @return the dnStatus
	 */
	public String getDnStatus() {
		return dnStatus;
	}

	/**
	 * @param dnStatus the dnStatus to set
	 */
	public void setDnStatus(String dnStatus) {
		this.dnStatus = dnStatus;
	}

	/**
	 * @return the isIdentified
	 */
	public String getIsIdentified() {
		return isIdentified;
	}

	/**
	 * @param isIdentified the isIdentified to set
	 */
	public void setIsIdentified(String isIdentified) {
		this.isIdentified = isIdentified;
	}


	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @return the isBalckListed
	 */
	public String getIsBalckListed() {
		return isBalckListed;
	}

	/**
	 * @param isBalckListed the isBalckListed to set
	 */
	public void setIsBalckListed(String isBalckListed) {
		this.isBalckListed = isBalckListed;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @return the agency
	 */
	public Agency getAgency() {
		return agency;
	}

	/**
	 * @param agency the agency to set
	 */
	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	private List<Address> addresses;
	

	/**
	 * @return the isPaymentResponsable
	 */
	public String getIsPaymentResponsable() {
		return IsPaymentResponsable;
	}

	/**
	 * @param isPaymentResponsable the isPaymentResponsable to set
	 */
	public void setIsPaymentResponsable(String isPaymentResponsable) {
		IsPaymentResponsable = isPaymentResponsable;
	}
    /**
	 * @return the contactLegal
	 */
	public ContactLegal getContactLegal() {
		return contactLegal;
	}

	/**
	 * @param contactLegal the contactLegal to set
	 */
	public void setContactLegal(ContactLegal contactLegal) {
		this.contactLegal = contactLegal;
	}


	/**
	 * @return the customerID
	 */
	public String getCustomerID() {
		return customerID;
	}

	/**
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	
	/**
	 * @return the addresses
	 */
	public List<Address> getAddresses() {
		return addresses;
	}

	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	/**
	 * @return the flagNomPrenom
	 */
	public boolean isFlagNomPrenom() {
		return flagNomPrenom;
	}

	/**
	 * @param flagNomPrenom the flagNomPrenom to set
	 */
	public void setFlagNomPrenom(boolean flagNomPrenom) {
		this.flagNomPrenom = flagNomPrenom;
	}

	/**
	 * @return the costId
	 */
	public String getAgencyCode() {
		return agencyCode;
	}

	/**
	 * @param costId the costId to set
	 */
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	/**
	 * @return the prgcode
	 */
	public String getPrgcode() {
		return prgcode;
	}

	/**
	 * @param prgcode the prgcode to set
	 */
	public void setPrgcode(String prgcode) {
		this.prgcode = prgcode;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCcu() {
		return ccu;
	}

	public void setCcu(String ccu) {
		this.ccu = ccu;
	}

	public String getCategorieClient() {
		return categorieClient;
	}

	public void setCategorieClient(String categorieClient) {
		this.categorieClient = categorieClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTypeIdentifiant() {
		return typeIdentifiant;
	}

	public void setTypeIdentifiant(String typeIdentifiant) {
		this.typeIdentifiant = typeIdentifiant;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public String getAgenceClient() {
		return agenceClient;
	}

	public void setAgenceClient(String agenceClient) {
		this.agenceClient = agenceClient;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public CustomerResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getBillingAccountId() {
		return billingAccountId;
	}

	public void setBillingAccountId(String billingAccountId) {
		this.billingAccountId = billingAccountId;
	}

	public String getCcseq() {
		return ccseq;
	}

	public void setCcseq(String ccseq) {
		this.ccseq = ccseq;
	}

	public String getCcsex() {
		return ccsex;
	}

	public void setCcsex(String ccsex) {
		this.ccsex = ccsex;
	}
}
