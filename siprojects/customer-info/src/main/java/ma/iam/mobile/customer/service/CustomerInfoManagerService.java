package ma.iam.mobile.customer.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import org.apache.cxf.annotations.WSDLDocumentation;
import org.apache.cxf.annotations.WSDLDocumentationCollection;

/**
 * The Class customerInfoService.
 */

@WebService(serviceName = "customerInfoService")
public interface CustomerInfoManagerService {

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public Integer modeByPass();
	@WebMethod
	public String infos();
	@WebMethod
	public void forceRefresh();
	

	@WebMethod(operationName = "searchCustomer")
	@WebResult(name = "ListClientsWs")
	public List<CustSearchWS> searchCustomer(
			@WebParam(name = "request") SearchCustomerRequest request)
			throws FunctionnalException;

	@WebMethod(operationName = "verifierEligibilite")
	@WebResult(name = "Operations")
	public List<Operation> verifierEligibilite(@WebParam(name = "request") VerifierEligibiliteRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "listePlanTarifaire")
	@WebResult(name = "Forfaits")
	public List<OutCategorie> listePlanTarifaire(@WebParam(name = "SEGMENT") String areaID)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "listeRateplanServicesParameters")
	@WebResult(name = "Services")
	public List<ServiceDTO> listeRateplanServicesParameters(@WebParam(name = "RATEPLAN") String tmcode, @WebParam(name = "FORFAIT") String foId)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "listeCategoryPlanTarifaire")
	@WebResult(name = "PlanTarifaire")
	public List<OutPlanTarifaire> listeCategoryPlanTarifaire(@WebParam(name = "PRGCODE") String prgcode)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "listeForfaits")
	@WebResult(name = "Forfaits")
	public List<Forfait> listeForfaits(@WebParam(name = "TMCODE") String tmcode,@WebParam(name = "PRGCODE") String prgcode)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "listeDureeEngagement")
	@WebResult(name = "result")
	public DureeEngagementResponse listeDureeEngagement(@WebParam(name = "requset") ContactRequest requset)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "listePack")
	@WebResult(name = "result")
	public List<ParameterValues> listePack(@WebParam(name = "requset") ContactRequest requset)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "valideSIM")
	@WebResult(name = "result")
	public SimValid valideSIM(@WebParam(name = "request") ValideSIMRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "getListUnpaidInvoices")
	@WebResult(name = "result")
	public List<Invoice> getListUnpaidInvoices(@WebParam(name = "request") ListUnpaidInvoicesRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "rechercheNumeroAppel")
	@WebResult(name = "result")
	public List<DirectoryNumber> rechercheNumeroAppel(@WebParam(name = "request") DirectoryNumber dnSearch)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "prolongerNumeroAppel")
	@WebResult(name = "result")
	public DirectoryNumber prolongerNumeroAppel(@WebParam(name = "nd") String nd, @WebParam(name = "minutesToExtend")String minutesToExtend)	throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "verifierExistanceRC")
	@WebResult(name = "result")
	public boolean verifierExistanceRC(@WebParam(name = "rc") String rc)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "verifierCodeGenere")
	@WebResult(name = "result")
	public boolean verifierCodeGenere(@WebParam(name = "request") VerifierCodeGenereRequest request)
			throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "engagementEnCours")
	@WebResult(name = "result")
	public Eng engagementEnCours(@WebParam(name = "ND") String nd)
			throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "getRaisonmodificationSIM")
	@WebResult(name = "result")
	public List<Raison> getRaisonmodificationSIM()
			throws FunctionnalException, TechnicalException;
	

	
	@WebMethod(operationName = "getConfigAll")
	@WebResult(name = "ConfigAll")
	public ConfigAll getConfigAll()
			throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "getSIMActuel")
	@WebResult(name = "result")
	public Sim getSIMActuel(@WebParam(name = "ND") String nd)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "isBlackListed")
	@WebResult(name = "result")
	public boolean isBlackListed(@WebParam(name = "request") CustomerRequest customer)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getAllContracts")
	@WebResult(name = "result")
	public List<CustFindContractWS> getAllContracts(
			@WebParam(name = "request") final AllContractsRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "getCustomerInfo")
	@WebResult(name = "result")
	public List<CustomerResponse> getCustomerInfo(@WebParam(name = "request") CustomerRequest customer)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

//	@WebMethod(operationName = "getAllIdentifierType")
//	@WebResult(name = "result")
//	public List<IdentifierType> getAllIdentifierType() throws FunctionnalException, TechnicalException;
//	
//	@WebMethod(operationName = "getAllLanguages")
//	@WebResult(name = "result")
//	public List<Language> getAllLanguages() throws FunctionnalException, TechnicalException;
//	
	@WebMethod(operationName = "statutRaisonClient")
	@WebResult(name = "result")
	public List<Raison> statutRaisonClient() throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "listeRaisonsModification")
	@WebResult(name = "result")
	public List<Raison> listeRaisonsModification() throws FunctionnalException, TechnicalException;
	
//	@WebMethod(operationName = "getAllSegments")
//	@WebResult(name = "result")
//	public List<Segment> getAllSegments() throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "getListCategoryBySegment")
	@WebResult(name = "result")
	public List<Category> getListCategoryBySegment(@WebParam(name = "request") String segmentCode) throws FunctionnalException, TechnicalException;
	
//	@WebMethod(operationName = "getAllPaymentType")
//	@WebResult(name = "result")
//	public List<PaymentType> getAllPaymentType() throws FunctionnalException, TechnicalException;
//
//	public List<City> getAllCities() throws FunctionnalException, TechnicalException;
//
//	public List<Country> getAllCountries() throws FunctionnalException, TechnicalException;
//
//	public List<Title> getAllTitles() throws FunctionnalException, TechnicalException;
	
	


}
