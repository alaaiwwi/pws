/*
 * @author Capgemini
 */
package ma.iam.mobile.customer.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.ws.WebFault;

import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;

/**
 * The Class WsException.
 */
@WebFault(name = "WsException", faultBean = "fr.capgemini.iam.grc.exceptions.FaultBean")
public class WsException extends Exception {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -1127071073519514459L;

    /** The fault bean. */
    private final FaultBean faultBean;

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param cause
     *            the cause
     */
    public WsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final Throwable cause) {
        super(cause);
        final StringBuilder result = new StringBuilder();
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        result.append(sw.toString());
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode());
        this.faultBean.setText(result.toString());
    }

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public WsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final String message, final Throwable cause) {
        super(cause);
        final StringBuilder result = new StringBuilder();
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        result.append(sw.toString());
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode(), message);
        this.faultBean.setText(result.toString());
    }

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     */
    public WsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum) {
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode());
    }

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param message
     *            the message
     */
    public WsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final String message) {
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode(), message);
    }

    /**
     * Gets the fault info.
     * 
     * @return the fault info
     */
    public FaultBean getFaultInfo() {
        return faultBean;
    }

}
