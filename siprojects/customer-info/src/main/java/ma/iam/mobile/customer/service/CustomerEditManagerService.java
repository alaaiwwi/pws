package ma.iam.mobile.customer.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.bean.cmsint.OccType;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;

/**
 * The Class customerInfoService.
 */
@WebService(serviceName = "customerEditService")
public interface CustomerEditManagerService {

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public Integer modeByPass();
	@WebMethod
	public String infos();
	@WebMethod(operationName = "createCustomer")
	@WebResult(name = "result")
	public Long createCustomer(@WebParam(name = "request") CreateCustomerRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "identifieClient")
	@WebResult(name = "result")
	public Long identifieClient(@WebParam(name = "request") IdentifyCustomerRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;
	

	@WebMethod(operationName = "genererSMS")
	@WebResult(name = "result")
	public SMSResponse genererSMS(@WebParam(name = "request") SMSRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;
	

	@WebMethod(operationName = "changementSIM")
	@WebResult(name = "result")
	public Long changementSIM(@WebParam(name = "request") SIMRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
	@WebMethod(operationName = "savePreOrderPoP")
	@WebResult(name = "result")
	public String savePreOrderPoP(@WebParam(name = "request") PreOrderPoPRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	@WebMethod(operationName = "addOcc")
	@WebResult(name = "result")
	public Long addOcc(@WebParam(name = "occ") OCCRequest occRequest)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
}
