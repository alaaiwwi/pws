package ma.iam.mobile.customer.business;

import java.util.List;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;

public interface CustomerInfoManagerBiz {


	List<Invoice> getListUnpaidInvoices(ListUnpaidInvoicesRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException;

	List<Forfait> listeForfaits(String tmcode,String prgcode) throws FunctionnalException, TechnicalException, SyntaxiqueException;

	List<ServiceDTO> listeRateplanServicesParameters(String tmcode, String foId)
			throws FunctionnalException, TechnicalException, SyntaxiqueException;

	List<Operation> verifierEligibilite(String nd, String coId, String cuId) throws FunctionnalException, TechnicalException, SyntaxiqueException;

	List<OutCategorie> listePlanTarifaire(String areaID) throws FunctionnalException, TechnicalException, SyntaxiqueException;

	List<OutPlanTarifaire> listeCategoryPlanTarifaire(String prgcode) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
	List<SimValidate> valideSIM(String sim, List<String> bo) throws FunctionnalException, TechnicalException, SyntaxiqueException;

	DureeEngagementResponse listeDureeEngagement(String coId, String tmcode,String sncode, String gammeForfait, String identifiantForfait) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
	List<ParameterValues> listePack(String coId, String tmcode) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
	boolean verifierExistanceRC(String rc) throws FunctionnalException, TechnicalException;
	
	boolean verifierCodeGenere(VerifierCodeGenereRequest request) throws FunctionnalException, TechnicalException;
	
	Eng engagementEnCours(String nd) throws FunctionnalException, TechnicalException;
	
	Sim getSIMActuel(String nd) throws FunctionnalException, TechnicalException;
	
	ConfigAll getConfigAll() throws FunctionnalException, TechnicalException;
	
	boolean isBlackList(CustomerRequest customer) throws FunctionnalException, TechnicalException;
	
	List<CustomerResponse> getCustomerInfo(CustomerRequest customer) throws FunctionnalException, TechnicalException, SyntaxiqueException;
	
	List<IdentifierType> getAllIdentifierType() throws FunctionnalException, TechnicalException;
	
	List<Language> getAllLanguages() throws FunctionnalException, TechnicalException;
	
	List<Raison> listeRaisonsModification() throws FunctionnalException, TechnicalException;
	
	List<Raison> getRaisonmodificationSIM() throws FunctionnalException, TechnicalException;
	
	List<Raison> statutRaisonClient() throws FunctionnalException, TechnicalException;
	
	List<City> getAllCities() throws FunctionnalException, TechnicalException;
	
	List<Country> getAllCountries() throws FunctionnalException, TechnicalException;
	
	List<Title> getAllTitles() throws FunctionnalException, TechnicalException;
	
	List<DirectoryNumber> rechercheNumeroAppel(DirectoryNumber dnSearch) throws FunctionnalException, TechnicalException;

	public DirectoryNumber prolongerNumeroAppel(String nd, String minutesToExtend)	throws FunctionnalException, TechnicalException;

	List<Segment> getAllSegments() throws FunctionnalException, TechnicalException;
	
	public List<Category> getListCategoryBySegment(String segmentCode) throws FunctionnalException, TechnicalException;
	
	public List<PaymentType> getAllPaymentType() throws FunctionnalException, TechnicalException;

	List<CustFindContractWS> getAllContracts(AllContractsRequest request)throws FunctionnalException, TechnicalException,SyntaxiqueException;
	
	public List<CustSearchWS> searchCustomer(SearchCustomerWS searchCustomerWS,
			Integer product) throws FunctionnalException;

}
