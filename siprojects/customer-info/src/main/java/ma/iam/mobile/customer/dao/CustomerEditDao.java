package ma.iam.mobile.customer.dao;

import java.util.List;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.bean.cmsint.OccType;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface CustomerEditDao extends BaseDAOMobile {

	Long insertCMSCreateCustomer(Customer cust, String userName) throws FunctionnalException;

	Long insertIdentifieClient(CustomerUpdate cust, String userName) throws FunctionnalException;

	SMSResponse genererSMS (String nd, String username, String coID) throws FunctionnalException;

	Long changementSIM(SIMChange sim, String username) throws FunctionnalException;
	
	Integer getCoIDFromDnNum(String nd) throws FunctionnalException;

	String savePreOrderPoP (PreOrderPoPRequest request) throws FunctionnalException;

	public Long addOcc(OCCRequest occRequest)throws FunctionnalException;
}
