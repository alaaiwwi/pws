package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Eng implements Serializable {

	private String engagementEnCours;
	private String penaliteAPayer;
	private String dateDebut;
	private String dateFin;
	/**
	 * @return the engagementEnCours
	 */
	public String getEngagementEnCours() {
		return engagementEnCours;
	}
	/**
	 * @param engagementEnCours the engagementEnCours to set
	 */
	public void setEngagementEnCours(String engagementEnCours) {
		this.engagementEnCours = engagementEnCours;
	}
	/**
	 * @return the penaliteAPayer
	 */
	public String getPenaliteAPayer() {
		return penaliteAPayer;
	}
	/**
	 * @param penaliteAPayer the penaliteAPayer to set
	 */
	public void setPenaliteAPayer(String penaliteAPayer) {
		this.penaliteAPayer = penaliteAPayer;
	}
	/**
	 * @return the dateDebut
	 */
	public String getDateDebut() {
		return dateDebut;
	}
	/**
	 * @param dateDebut the dateDebut to set
	 */
	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}
	/**
	 * @return the dateFin
	 */
	public String getDateFin() {
		return dateFin;
	}
	/**
	 * @param dateFin the dateFin to set
	 */
	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}
	
	
}
