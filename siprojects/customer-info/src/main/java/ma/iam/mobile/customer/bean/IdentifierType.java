package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class IdentifierType implements Serializable {

	private String code;
	private String label;



	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}



	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}



	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}



	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}



	public IdentifierType() {
		super();
	}

}
