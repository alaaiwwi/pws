package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class PaymentArrangement implements Serializable {

	private String payment_method_id;
    private String account_no;
    private String account_owner;
    private String bank_name;
    private String bank_zip;
    private String bank_city;
    private String swift;
    private String orderNumber;
	/**
	 * @return the payment_method_id
	 */
	public String getPayment_method_id() {
		return payment_method_id;
	}
	/**
	 * @param payment_method_id the payment_method_id to set
	 */
	public void setPayment_method_id(String payment_method_id) {
		this.payment_method_id = payment_method_id;
	}
	/**
	 * @return the account_no
	 */
	public String getAccount_no() {
		return account_no;
	}
	/**
	 * @param account_no the account_no to set
	 */
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	/**
	 * @return the account_owner
	 */
	public String getAccount_owner() {
		return account_owner;
	}
	/**
	 * @param account_owner the account_owner to set
	 */
	public void setAccount_owner(String account_owner) {
		this.account_owner = account_owner;
	}
	/**
	 * @return the bank_name
	 */
	public String getBank_name() {
		return bank_name;
	}
	/**
	 * @param bank_name the bank_name to set
	 */
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	/**
	 * @return the bank_zip
	 */
	public String getBank_zip() {
		return bank_zip;
	}
	/**
	 * @param bank_zip the bank_zip to set
	 */
	public void setBank_zip(String bank_zip) {
		this.bank_zip = bank_zip;
	}
	/**
	 * @return the bank_city
	 */
	public String getBank_city() {
		return bank_city;
	}
	/**
	 * @param bank_city the bank_city to set
	 */
	public void setBank_city(String bank_city) {
		this.bank_city = bank_city;
	}
	/**
	 * @return the swift
	 */
	public String getSwift() {
		return swift;
	}
	/**
	 * @param swift the swift to set
	 */
	public void setSwift(String swift) {
		this.swift = swift;
	}

	public String getOrderNumber() {		return orderNumber;	}

	public void setOrderNumber(String orderNumber) {		this.orderNumber = orderNumber;	}
}
