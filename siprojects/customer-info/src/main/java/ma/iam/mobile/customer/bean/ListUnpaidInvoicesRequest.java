package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebParam;

public class ListUnpaidInvoicesRequest implements Serializable {
	
	

	private String custcode;
	private String ohstatus;
	/**
	 * @return the custcode
	 */
	public String getCustcode() {
		return custcode;
	}
	/**
	 * @param custcode the custcode to set
	 */
	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}
	/**
	 * @return the ohstatus
	 */
	public String getOhstatus() {
		return ohstatus;
	}
	/**
	 * @param ohstatus the ohstatus to set
	 */
	public void setOhstatus(String ohstatus) {
		this.ohstatus = ohstatus;
	}
}
	
