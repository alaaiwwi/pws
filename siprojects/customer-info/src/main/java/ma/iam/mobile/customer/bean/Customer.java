package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Customer implements Serializable {
	
	private String customerID;
	private String is_business_partner;
    private String occ_rate_plan_code;
	private String categorie;
	private String family_id;
    private String billing_cycle;
    private String dealer_id;
    private String cost_center;

	/**
	 * @return the cost_center
	 */
	public String getCost_center() {
		return cost_center;
	}
	/**
	 * @param cost_center the cost_center to set
	 */
	public void setCost_center(String cost_center) {
		this.cost_center = cost_center;
	}
	private String segment ;
    private List<Address> addresses;
    private PaymentArrangement paymentArrangement;
    private List<ContractCreate> contracts;

	/**
	 * @return the customerID
	 */
	public String getCustomerID() {
		return customerID;
	}
	/**
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	/**
	 * @return the segment
	 */
	public String getSegment() {
		return segment;
	}
	/**
	 * @param segment the segment to set
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}
	/**
	 * @return the categorie
	 */
	public String getCategorie() {
		return categorie;
	}
	/**
	 * @param categorie the categorie to set
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	/**
	 * @return the is_business_partner
	 */
	public String getIs_business_partner() {
		return is_business_partner;
	}
	/**
	 * @param is_business_partner the is_business_partner to set
	 */
	public void setIs_business_partner(String is_business_partner) {
		this.is_business_partner = is_business_partner;
	}
	/**
	 * @return the occ_rate_plan_code
	 */
	public String getOcc_rate_plan_code() {
		return occ_rate_plan_code;
	}
	/**
	 * @param occ_rate_plan_code the occ_rate_plan_code to set
	 */
	public void setOcc_rate_plan_code(String occ_rate_plan_code) {
		this.occ_rate_plan_code = occ_rate_plan_code;
	}
	/**
	 * @return the family_id
	 */
	public String getFamily_id() {
		return family_id;
	}
	/**
	 * @param family_id the family_id to set
	 */
	public void setFamily_id(String family_id) {
		this.family_id = family_id;
	}
	/**
	 * @return the billing_cycle
	 */
	public String getBilling_cycle() {
		return billing_cycle;
	}
	/**
	 * @param billing_cycle the billing_cycle to set
	 */
	public void setBilling_cycle(String billing_cycle) {
		this.billing_cycle = billing_cycle;
	}
	/**
	 * @return the dealer_id
	 */
	public String getDealer_id() {
		return dealer_id;
	}
	/**
	 * @param dealer_id the dealer_id to set
	 */
	public void setDealer_id(String dealer_id) {
		this.dealer_id = dealer_id;
	}
	/**
	 * @return the addresses
	 */
	public List<Address> getAddresses() {
		return addresses;
	}
	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	/**
	 * @return the paymentArrangement
	 */
	public PaymentArrangement getPaymentArrangement() {
		return paymentArrangement;
	}
	/**
	 * @param paymentArrangement the paymentArrangement to set
	 */
	public void setPaymentArrangement(PaymentArrangement paymentArrangement) {
		this.paymentArrangement = paymentArrangement;
	}
	/**
	 * @return the contracts
	 */
	public List<ContractCreate> getContracts() {
		return contracts;
	}
	/**
	 * @param contracts the contracts to set
	 */
	public void setContracts(List<ContractCreate> contracts) {
		this.contracts = contracts;
	}

}
