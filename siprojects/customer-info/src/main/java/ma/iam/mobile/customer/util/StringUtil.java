package ma.iam.mobile.customer.util;

import org.apache.commons.lang.StringUtils;

public class StringUtil {
	
	
    /**
     * test si une chaine de caract�re est vide
     * @param s
     * @return
     */
    public static boolean isEmpty(String s){
    		
    	return (s == null || "".equals(s.trim())) ? Boolean.TRUE
				: Boolean.FALSE;
  
    }
    public static String getValueAsStringOrNull(Object o){

        return (o == null ? null : String.valueOf(o));

    }
    public static boolean isNumeric(String str){
        if (str == null) {
            return false;
        } else {
            int sz = str.length();

            for(int i = 0; i < sz; ++i) {
                if (!Character.isDigit(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        }
    }

}
