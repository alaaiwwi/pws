package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebParam;

public class ContactRequest implements Serializable {

	private String coId;
	private String tmcode;
	private String gammeForfait;
	private String identifiantForfait;
	private String sncode;
	/**
	 * @return the coId
	 */
	public String getCoId() {
		return coId;
	}
	/**
	 * @param coId the coId to set
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}
	/**
	 * @return the tmcode
	 */
	public String getTmcode() {
		return tmcode;
	}
	/**
	 * @param tmcode the tmcode to set
	 */
	public void setTmcode(String tmcode) {
		this.tmcode = tmcode;
	}

	public String getGammeForfait() {
		return gammeForfait;
	}

	public void setGammeForfait(String gammeForfait) {
		this.gammeForfait = gammeForfait;
	}

	public String getIdentifiantForfait() {
		return identifiantForfait;
	}

	public void setIdentifiantForfait(String identifiantForfait) {
		this.identifiantForfait = identifiantForfait;
	}

	public String getSncode() {		return sncode;	}

	public void setSncode(String sncode) {		this.sncode = sncode;	}
}
