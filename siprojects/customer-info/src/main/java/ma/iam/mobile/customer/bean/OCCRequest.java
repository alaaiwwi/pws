package ma.iam.mobile.customer.bean;

import ma.iam.mobile.customer.bean.cmsint.OccType;

import java.io.Serializable;

public class OCCRequest implements Serializable {

	private OccType occType;
	private String userName;
	private String ticket;
	private String parentID;

	public OccType getOccType() {
		return occType;
	}

	public void setOccType(OccType occType) {
		this.occType = occType;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}
}
