package ma.iam.mobile.customer.util;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLQueryBuilder<T> {
    private static final Logger LOG = LoggerFactory.getLogger(SQLQueryBuilder.class);
    private String sqlQuery;
    private String sqlQueryId;
    private Class<T> mappedClass;

    public static SQLQueryBuilder getInstance(String sqlQueryId, NamedParameterJdbcTemplate namedParameterJdbcTemplate){
        return new SQLQueryBuilder(null,sqlQueryId,namedParameterJdbcTemplate);
    }
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private Map<String, String> namedParameters = new HashMap();
    private final List<String> tags = new ArrayList<String>(0);

    public SQLQueryBuilder(Class<T> mappedClass, String sqlQueryId, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.mappedClass = mappedClass;
        this.sqlQueryId = sqlQueryId;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public SQLQueryBuilder addNamedParameter(String key, String value) {
        this.namedParameters.put(key, value);
        return this;
    }

    public SQLQueryBuilder addTag(String tag) {
        if (!GRCStringUtil.isNullOrEmpty(tag))
            tags.add(tag);
        return this;
    }

    public SQLQueryBuilder addNamedParameterWithTag(T t, String key, String value) {
        if (!GRCStringUtil.isNullOrEmpty(key)) {
            this.namedParameters.put(key, value);
            tags.add(key.toUpperCase());
        }
        return this;
    }

    public List<Map<String, Object>> executeAsSelectUsingMap() {
        sqlQuery = CustomSQLUtil.get(sqlQueryId);
        return this.namedParameterJdbcTemplate.queryForList(sqlQuery, namedParameters);
    }

    public List<Map<String, Object>> executeAsSelectUsingMapAndTags() throws Exception {
        sqlQuery = GRCStringUtil.formatQuery(sqlQueryId, tags);
        return this.namedParameterJdbcTemplate.queryForList(sqlQuery, namedParameters);
    }

    public List<T> executeAsSelectUsingRowMapper() {
        sqlQuery = CustomSQLUtil.get(sqlQueryId);
        final CustomBeanPropertyRowMapper<T> custRowMapper = new CustomBeanPropertyRowMapper<T>(
                mappedClass);
        return this.namedParameterJdbcTemplate.query(sqlQuery, namedParameters, custRowMapper);
    }

    public List<T> executeAsSelectUsingRowMapperAndTags() throws Exception {
        sqlQuery = GRCStringUtil.formatQuery(sqlQueryId, tags);
        final CustomBeanPropertyRowMapper<T> custRowMapper = new CustomBeanPropertyRowMapper<T>(
                mappedClass);
        return this.namedParameterJdbcTemplate.query(sqlQuery, namedParameters, custRowMapper);
    }

    public int executeAsUpdate() {
        sqlQuery = CustomSQLUtil.get(sqlQueryId);
        return this.namedParameterJdbcTemplate.update(sqlQuery, namedParameters);
    }

    public int executeAsSelectUsingTags() throws Exception {
        sqlQuery = GRCStringUtil.formatQuery(sqlQueryId, tags);
        return this.namedParameterJdbcTemplate.update(sqlQuery, namedParameters);
    }
}
