package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class OutParameter implements Serializable {

	private String id;
    private String no;
    private String shdes;
    private String des;
    private List<OutValue> values;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the no
	 */
	public String getNo() {
		return no;
	}
	/**
	 * @param no the no to set
	 */
	public void setNo(String no) {
		this.no = no;
	}
	/**
	 * @return the shdes
	 */
	public String getShdes() {
		return shdes;
	}
	/**
	 * @param shdes the shdes to set
	 */
	public void setShdes(String shdes) {
		this.shdes = shdes;
	}
	/**
	 * @return the des
	 */
	public String getDes() {
		return des;
	}
	/**
	 * @param des the des to set
	 */
	public void setDes(String des) {
		this.des = des;
	}
	/**
	 * @return the values
	 */
	public List<OutValue> getValues() {
		return values;
	}
	/**
	 * @param values the values to set
	 */
	public void setValues(List<OutValue> values) {
		this.values = values;
	}
    
	
}
