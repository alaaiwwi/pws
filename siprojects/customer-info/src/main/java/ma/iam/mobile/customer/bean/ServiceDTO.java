package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class ServiceDTO implements Serializable {

	private String id;
	private String code;
	private String label;

	private String chargeUnique;
    private String chargeRecurente;
    private Boolean obligatoire;
    private Boolean configurable;
    private Boolean forfait;
    private String spcode;
    private String sncode;
    private String operations;
    
    private List<ParametreServiceDTO> listParametreService ;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the chargeUnique
	 */
	public String getChargeUnique() {
		return chargeUnique;
	}

	/**
	 * @param chargeUnique the chargeUnique to set
	 */
	public void setChargeUnique(String chargeUnique) {
		this.chargeUnique = chargeUnique;
	}

	/**
	 * @return the chargeRecurente
	 */
	public String getChargeRecurente() {
		return chargeRecurente;
	}

	/**
	 * @param chargeRecurente the chargeRecurente to set
	 */
	public void setChargeRecurente(String chargeRecurente) {
		this.chargeRecurente = chargeRecurente;
	}

	/**
	 * @return the obligatoire
	 */
	public Boolean getObligatoire() {
		return obligatoire;
	}

	/**
	 * @param obligatoire the obligatoire to set
	 */
	public void setObligatoire(Boolean obligatoire) {
		this.obligatoire = obligatoire;
	}

	/**
	 * @return the configurable
	 */
	public Boolean getConfigurable() {
		return configurable;
	}

	/**
	 * @param configurable the configurable to set
	 */
	public void setConfigurable(Boolean configurable) {
		this.configurable = configurable;
	}

	/**
	 * @return the listParametreService
	 */
	public List<ParametreServiceDTO> getListParametreService() {
		return listParametreService;
	}

	/**
	 * @param listParametreService the listParametreService to set
	 */
	public void setListParametreService(
			List<ParametreServiceDTO> listParametreService) {
		this.listParametreService = listParametreService;
	}

	public Boolean getForfait() {
		return forfait;
	}

	public void setForfait(Boolean forfait) {
		this.forfait = forfait;
	}

	public String getSpcode() {
		return spcode;
	}

	public void setSpcode(String spcode) {
		this.spcode = spcode;
	}

	public String getSncode() {
		return sncode;
	}

	public void setSncode(String sncode) {
		this.sncode = sncode;
	}

	public String getOperations() {
		return operations;
	}

	public void setOperations(String operations) {
		this.operations = operations;
	}
}
