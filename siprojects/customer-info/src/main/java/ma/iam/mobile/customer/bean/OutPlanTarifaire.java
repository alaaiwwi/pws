package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class OutPlanTarifaire implements Serializable {

	 public OutPlanTarifaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String tmcode;
     private String des;
     private String eligOperat;
     private String vscode;
     private List<OutService> services;
	/**
	 * @return the tmcode
	 */
	public String getTmcode() {
		return tmcode;
	}
	/**
	 * @param tmcode the tmcode to set
	 */
	public void setTmcode(String tmcode) {
		this.tmcode = tmcode;
	}
	/**
	 * @return the des
	 */
	public String getDes() {
		return des;
	}
	/**
	 * @param des the des to set
	 */
	public void setDes(String des) {
		this.des = des;
	}

	public String getEligOperat() {
		return eligOperat;
	}

	public void setEligOperat(String eligOperat) {
		this.eligOperat = eligOperat;
	}

	public String getVscode() {
		return vscode;
	}

	public void setVscode(String vscode) {
		this.vscode = vscode;
	}

	/**
	 * @return the services
	 */
	public List<OutService> getServices() {
		return services;
	}
	/**
	 * @param services the services to set
	 */
	public void setServices(List<OutService> services) {
		this.services = services;
	}
	
}
