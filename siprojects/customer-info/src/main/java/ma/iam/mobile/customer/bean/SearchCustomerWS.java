package ma.iam.mobile.customer.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "co_id", "codeCategorie", "nd", "nom",
		"numPieceIdentite", "prenom", "ville", "custCode","idType" })
public class SearchCustomerWS implements Serializable {

	private String nd;
	private String nom;
	private String prenom;
	private String ville;
	private String codeCategorie;
	private String numPieceIdentite;
	private String co_id;
	private String custCode;
	private String idType;

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodeCategorie() {
		return codeCategorie;
	}

	public void setCodeCategorie(String codeCategorie) {
		this.codeCategorie = codeCategorie;
	}

	public String getNumPieceIdentite() {
		return numPieceIdentite;
	}

	public void setNumPieceIdentite(String numPieceIdentite) {
		this.numPieceIdentite = numPieceIdentite;
	}

	public String getCo_id() {
		return co_id;
	}

	public void setCo_id(String co_id) {
		this.co_id = co_id;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    @Override
	public String toString() {
		return "SearchCustomerWS [nd=" + nd + ", nom=" + nom + ", prenom="
				+ prenom + ", ville=" + ville + ", codeCategorie="
				+ codeCategorie + ", numPieceIdentite=" + numPieceIdentite
				+ ", co_id=" + co_id + ", custCode=" + custCode + "]";
	}

}
