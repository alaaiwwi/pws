package ma.iam.mobile.customer.service.impl;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.util.StringUtil;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.business.ByPassManagerBiz;
import ma.iam.mobile.customer.business.CustomerInfoManagerBiz;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.service.CustomerInfoManagerService;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.cxf.annotations.WSDLDocumentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component(value = "customerInfoManagerService")
public class CustomerInfoManagerServiceImpl extends SpringBeanAutowiringSupport implements CustomerInfoManagerService {
	@Autowired
	private Properties git;
	/** The customer infos manager biz. */
	@Autowired
	private ByPassManagerBiz byPassManagerBiz;

	@Autowired
	private CustomerInfoManagerBiz customerInfoManagerBiz;

	@Autowired
	private JDBCPing pingMobileBscs;

	public Integer modeByPass() {
		return byPassManagerBiz.modeByPass();
	}

	@Override
	public String infos() {
		return "branch: "+ git.getProperty("git.branch") +
				", closest.tag.name : "+git.getProperty("git.closest.tag.name") +
				", build.time : "+git.getProperty("git.build.time") +
				", commit.id : "+ git.getProperty("git.commit.id");
	}

	@Override
	@CacheEvict( value = "refrential" ,allEntries = true)
	public void forceRefresh() {

	}


	public List<CustSearchWS> searchCustomer(
			SearchCustomerRequest request)
			throws FunctionnalException {
		return customerInfoManagerBiz.searchCustomer(request.getSearchCustomerWS(), request.getProduct());
	}
	

	public List<ServiceDTO> listeRateplanServicesParameters(String tmcode, String foId)
			throws FunctionnalException, TechnicalException, SyntaxiqueException{
		return customerInfoManagerBiz.listeRateplanServicesParameters(tmcode, foId);
	}
	
	@Override
	public List<Operation> verifierEligibilite(VerifierEligibiliteRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.verifierEligibilite(null, request.getCoId(), request.getCuId());
	}



	@Override
	public ConfigAll getConfigAll() throws FunctionnalException,
			TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.getConfigAll();
	}

	@Override
	public List<OutCategorie> listePlanTarifaire(String areaID)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.listePlanTarifaire(areaID);
	}

	@Override
	public List<OutPlanTarifaire> listeCategoryPlanTarifaire(String prgcode)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.listeCategoryPlanTarifaire(prgcode);
	}

	@Override
	public List<Forfait> listeForfaits(String tmcode,String prgcode)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.listeForfaits(tmcode,prgcode);
	}

	@Override
	public List<Invoice> getListUnpaidInvoices(ListUnpaidInvoicesRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.getListUnpaidInvoices(request);
	}

	@Override
	public List<ParameterValues> listePack(ContactRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.listePack(request.getCoId(), request.getTmcode());
	}

	@Override
	public DureeEngagementResponse listeDureeEngagement(ContactRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.listeDureeEngagement(request.getCoId(), request.getTmcode(),request.getSncode(), request.getGammeForfait(),request.getIdentifiantForfait());
	}

	@Override
	public SimValid valideSIM(ValideSIMRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		List<SimValidate> res = customerInfoManagerBiz.valideSIM(request.getSim(), null);
		if(res!=null && res.size()>0){
			SimValid sim = new SimValid();
			sim.setSim(res.get(0).getSim());
			sim.setStatus(res.get(0).getStatus());
			sim.setType(res.get(0).getType());			
			sim.setHlcode(res.get(0).getHlcode());
			sim.setHlrgroup(res.get(0).getHlrgroup());
			List<String> bos = new ArrayList<String>();
			for (int i=0;i<res.size();i++){
				bos.add(res.get(i).getBo());
			}
			
			sim.setBo(bos);
			
			return sim;
			
		}
		return null;
	}

	@Override
	public List<DirectoryNumber> rechercheNumeroAppel(DirectoryNumber dnSearch)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerInfoManagerBiz.rechercheNumeroAppel(dnSearch);
	}

    @Override
    public DirectoryNumber prolongerNumeroAppel(String nd, String minutesToExtend) throws FunctionnalException, TechnicalException, SyntaxiqueException {
		if (StringUtil.isEmpty(nd) || StringUtil.isEmpty(minutesToExtend)){
			throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_RECHEARCH_INVALID);
		}
        return customerInfoManagerBiz.prolongerNumeroAppel(nd,minutesToExtend);
    }

//	@Override
//	public List<Country> getAllCountries() throws FunctionnalException,
//			TechnicalException {
//		// TODO Auto-generated method stub
//		return customerInfoManagerBiz.getAllCountries();
//	}

	@Override
	public Sim getSIMActuel(String nd) throws FunctionnalException,
			TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.getSIMActuel(nd);
	}

	@Override
	public Eng engagementEnCours(String nd) throws FunctionnalException,
			TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.engagementEnCours(nd);
	}

	@Override
	public List<Raison> getRaisonmodificationSIM() throws FunctionnalException,
			TechnicalException {
		return customerInfoManagerBiz.getRaisonmodificationSIM();
	}

	@Override
	public List<Raison> listeRaisonsModification() throws FunctionnalException,
			TechnicalException {
		return customerInfoManagerBiz.listeRaisonsModification();
	}

	@Override
	public List<Raison> statutRaisonClient() throws FunctionnalException,
			TechnicalException {
		return customerInfoManagerBiz.statutRaisonClient();
	}

//	@Override
//	public List<Title> getAllTitles() throws FunctionnalException,
//			TechnicalException {
//		return customerInfoManagerBiz.getAllTitles();
//	}

	@Override
	public boolean verifierExistanceRC(String rc) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.verifierExistanceRC(rc);
	}

	@Override
	public boolean verifierCodeGenere(VerifierCodeGenereRequest request) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.verifierCodeGenere(request);
	}

	@Override
	public boolean isBlackListed(CustomerRequest customer) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.isBlackList(customer);
	}

	@Override
	public List<CustomerResponse> getCustomerInfo(CustomerRequest customer) throws FunctionnalException, TechnicalException, SyntaxiqueException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.getCustomerInfo(customer);
	}

	@Override
	public List<CustFindContractWS> getAllContracts(AllContractsRequest request) throws FunctionnalException, TechnicalException, SyntaxiqueException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.getAllContracts(request);
	}

//	@Override
//	public List<IdentifierType> getAllIdentifierType() throws FunctionnalException, TechnicalException {
//		// TODO Auto-generated method stub
//		return customerInfoManagerBiz.getAllIdentifierType();
//	}
//
//	@Override
//	public List<City> getAllCities() throws FunctionnalException, TechnicalException {
//		// TODO Auto-generated method stub
//		return customerInfoManagerBiz.getAllCities();
//	}
//
//
//	@Override
//	public List<Language> getAllLanguages() throws FunctionnalException, TechnicalException {
//		// TODO Auto-generated method stub
//		return customerInfoManagerBiz.getAllLanguages();
//	}
//
//	@Override
//	public List<Segment> getAllSegments() throws FunctionnalException, TechnicalException {
//		// TODO Auto-generated method stub
//		return customerInfoManagerBiz.getAllSegments();
//	}

	@Override
	public List<Category> getListCategoryBySegment(String segmentCode) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return customerInfoManagerBiz.getListCategoryBySegment(segmentCode);
	}

//	@Override
//	public List<PaymentType> getAllPaymentType() throws FunctionnalException, TechnicalException {
//		// TODO Auto-generated method stub
//		return customerInfoManagerBiz.getAllPaymentType();
//	}
	
	

}
