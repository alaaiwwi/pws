package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class DureeEngagementResponse implements Serializable {

    private Long parameterId;
    private List<ParameterValues> parameterValuesList ;

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public List<ParameterValues> getParameterValuesList() {
        return parameterValuesList;
    }

    public void setParameterValuesList(List<ParameterValues> parameterValuesList) {
        this.parameterValuesList = parameterValuesList;
    }
}
