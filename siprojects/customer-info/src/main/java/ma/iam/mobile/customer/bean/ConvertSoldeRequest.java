package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class ConvertSoldeRequest implements Serializable {

	private String nd;
	private String oriAccountType;
	private String desAccountType;
	private String amount;

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getOriAccountType() {
		return oriAccountType;
	}

	public void setOriAccountType(String oriAccountType) {
		this.oriAccountType = oriAccountType;
	}

	public String getDesAccountType() {
		return desAccountType;
	}

	public void setDesAccountType(String desAccountType) {
		this.desAccountType = desAccountType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public ConvertSoldeRequest() {
		super();
	}

}
