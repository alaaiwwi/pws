package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class DnTypes implements Serializable {
	
	

	private List <DnType> dnType;

	/**
	 * @return the dnType
	 */
	public List<DnType> getDnType() {
		return dnType;
	}

	/**
	 * @param dnType the dnType to set
	 */
	public void setDnType(List<DnType> dnType) {
		this.dnType = dnType;
	}


}
