package ma.iam.mobile.customer.business.impl;

import java.util.List;
import java.util.Properties;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.bean.cmsint.OccType;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.business.CustomerEditManagerBiz;
import ma.iam.mobile.customer.business.CustomerInfoManagerBiz;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.controller.CustomerInfoController;
import ma.iam.mobile.customer.dao.CustomerEditDao;
import ma.iam.mobile.customer.dao.CustomerInfoDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mobile.customer.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "customerEditManagerBiz")
public class CustomerEditManagerBizImpl implements CustomerEditManagerBiz {
	public static final String NUMERIQUE_REGEX = "\\d+";
	public static final String ALPHANUM_REGEX = "^[a-zA-Z0-9]+$";
	public static final String ALPHA_REGEX = "^[a-zA-ZéÉàÀèÈùâÂêÊîÎôÔûÛëËïÏüÜç’\\_]+$";
	public static final String CUSTCODE_REGEX = "^\\d+(\\.\\d+)+$";
	
	@Autowired
	private Properties config;

	@Autowired
	CustomerEditDao customerEditDao;

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory.getLogger(CustomerEditManagerBizImpl.class);

	@Override
	public Long createCustomer(Customer customer, String userName) throws FunctionnalException, TechnicalException, SyntaxiqueException {

			/*
			if (!StringUtil.isEmpty(customer.getPASSPORTNO) && !customer.getPASSPORTNO().matches(ALPHANUM_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_PASSPORTNO_ALPHANUMERIQUE);
			}
			
			if (!StringUtil.isEmpty(customer.getCUSTCODE())){ 
					if(!customer.getCUSTCODE().matches(CUSTCODE_REGEX)) {
						throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_CUSTCODE_STRUCTURE);
					}
					else
						if (!customerInfoDao.isCustomerExist(customer.getCUSTCODE())) {
							throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_CI_CUSTOMER_NOT_FOUND);
						}
					
			}
			
			
			if (!StringUtil.isEmpty(customer.getMSISDN())){
				String nd = validateND(customer.getMSISDN());
				if(nd == null) {
					throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_MSISDN_NUMEROTATION);
				}
				else{
					customer.setMSISDN(nd);
				}
			}
					
					
			if (!StringUtil.isEmpty(customer.getSM_SERIALNUM())){
				if(!customer.getSM_SERIALNUM().matches(NUMERIQUE_REGEX)) {
					throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_SIM_ALPHANUMERIQUE);
				}
			}
				
			
			if (!StringUtil.isEmpty(customer.getFIRST_NAME()) && !customer.getFIRST_NAME().matches(ALPHA_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_NOM_PRENOM_ALPHA);
			}
			
			if (!StringUtil.isEmpty(customer.getLASTNAME()) && !customer.getLASTNAME().matches(ALPHA_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_NOM_PRENOM_ALPHA);
			}
			
			if (!StringUtil.isEmpty(customer.getCCU()) && !customer.getCCU().matches(ALPHANUM_REGEX)) {
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_CCU_ALPHANUMERIQUE);
			}
			
			if(StringUtil.isEmpty(customer.getMSISDN()) && StringUtil.isEmpty(customer.getCUSTCODE()) 
					&& StringUtil.isEmpty(customer.getSM_SERIALNUM())  && StringUtil.isEmpty(customer.getCCU()) ){
				
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_RECHEARCH_INVALID);
				
			}
		
		*/
		return customerEditDao.insertCMSCreateCustomer(customer, userName);
	}
	
	@Override
	public Long identifieClient(CustomerUpdate customer, String userName) throws FunctionnalException, TechnicalException, SyntaxiqueException {

		return customerEditDao.insertIdentifieClient(customer, userName);
	}
	
	
	@Override
	public SMSResponse genererSMS(String nd, String username) throws FunctionnalException, TechnicalException, SyntaxiqueException {

		String tempnd = null;
			if (!StringUtil.isEmpty(nd)){
				tempnd = validateND(nd);
				if(nd == null) {
					throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_MSISDN_NUMEROTATION);
				}
			}
			else{
				throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_MSISDN_NUMEROTATION);				
			}
		Integer contract =  customerEditDao.getCoIDFromDnNum(tempnd);
		if(contract!=null)
			return customerEditDao.genererSMS(tempnd, username, contract.toString());
		else
			return null;
	}
	
	@Override
	public Long changementSIM(SIMChange sim, String username) throws FunctionnalException, TechnicalException, SyntaxiqueException {
	
		return customerEditDao.changementSIM(sim, username);
	}
	
	public String validateND(String nd) {
		
		String ndTemp = nd;
		if (ndTemp.startsWith("+")){
			ndTemp = ndTemp.substring(1);
			if (!ndTemp.startsWith("2126") && !ndTemp.startsWith("2127"))
				return null;
		}
		else
			if (!ndTemp.startsWith("06") && !ndTemp.startsWith("07")){
				if(!ndTemp.startsWith("2126") && !ndTemp.startsWith("2127")){
					return null;
				}
			}
			else
				ndTemp = "212".concat(ndTemp.substring(1));
		
		if(!ndTemp.matches(NUMERIQUE_REGEX)){
			return null;		
		}
		if (ndTemp.length() != 12)
			return null;
		
	return ndTemp;
}

	@Override
	public String savePreOrderPoP(PreOrderPoPRequest request)
			throws FunctionnalException, TechnicalException, SyntaxiqueException {
		return customerEditDao.savePreOrderPoP(request);
	}

	@Override
	public Long addOcc(OCCRequest occRequest) throws FunctionnalException, TechnicalException, SyntaxiqueException {
		return customerEditDao.addOcc(occRequest);
	}
}
