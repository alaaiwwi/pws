package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Sim implements Serializable {

	private String sim;
	private String type;
	private String label;
	private String hlcode;
	private String hlrgroup;

	
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the sim
	 */
	public String getSim() {
		return sim;
	}
	/**
	 * @param sim the sim to set
	 */
	public void setSim(String sim) {
		this.sim = sim;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public String getHlcode() {
		return hlcode;
	}

	public void setHlcode(String hlcode) {
		this.hlcode = hlcode;
	}

	public String getHlrgroup() {
		return hlrgroup;
	}

	public void setHlrgroup(String hlrgroup) {
		this.hlrgroup = hlrgroup;
	}
}
