package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Category implements Serializable {

	private String id;
	private String code;
	private String label;

	private String segmentLabel;
	private String segmentCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSegmentLabel() {
		return segmentLabel;
	}

	public void setSegmentLabel(String segmentLabel) {
		this.segmentLabel = segmentLabel;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Category() {
		super();
	}

}
