package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Segments implements Serializable {
	
	

	private List <Segment> segment;

	/**
	 * @return the segment
	 */
	public List<Segment> getSegment() {
		return segment;
	}

	/**
	 * @param segment the segment to set
	 */
	public void setSegment(List<Segment> segment) {
		this.segment = segment;
	}

	


}
