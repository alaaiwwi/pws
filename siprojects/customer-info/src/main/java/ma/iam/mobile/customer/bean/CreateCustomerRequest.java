package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class CreateCustomerRequest implements Serializable {

	private Customer customer;
	private String userName;
	private boolean checkOnly;
	private String ticket;
	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isCheckOnly() {
		return checkOnly;
	}

	public void setCheckOnly(boolean checkOnly) {
		this.checkOnly = checkOnly;
	}

	public String getTicket() {		return ticket;	}

	public void setTicket(String ticket) {		this.ticket = ticket;	}
}
