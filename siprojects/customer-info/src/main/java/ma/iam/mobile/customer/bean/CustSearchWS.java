package ma.iam.mobile.customer.bean;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType(propOrder = { "adresseFacturation", "codeAgence", "codeCategorie",
		"custCode", "customerId", "email", "libelleAgence", "libelleCategorie",
		"nom", "numPieceIdentite", "prenom", "ville", "clientTitle",
		"modePayment", "statutClient", "typeIdentite" })
public class CustSearchWS implements Serializable {
	private String customerId;
	private String custCode;
	private String codeCategorie;
	private String libelleCategorie;
	private String email;
	private String adresseFacturation;
	private String codeAgence;
	private String libelleAgence;
	private String nom;
	private String prenom;
	private String numPieceIdentite;
	private String ville;

	private String typeIdentite;
	private String statutClient;
	private String modePayment;
	private String clientTitle;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCodeCategorie() {
		return codeCategorie;
	}

	public void setCodeCategorie(String codeCategorie) {
		this.codeCategorie = codeCategorie;
	}

	public String getLibelleCategorie() {
		return libelleCategorie;
	}

	public void setLibelleCategorie(String libelleCategorie) {
		this.libelleCategorie = libelleCategorie;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresseFacturation() {
		return adresseFacturation;
	}

	public void setAdresseFacturation(String adresseFacturation) {
		this.adresseFacturation = adresseFacturation;
	}

	public String getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}

	public String getLibelleAgence() {
		return libelleAgence;
	}

	public void setLibelleAgence(String libelleAgence) {
		this.libelleAgence = libelleAgence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumPieceIdentite() {
		return numPieceIdentite;
	}

	public void setNumPieceIdentite(String numPieceIdentite) {
		this.numPieceIdentite = numPieceIdentite;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getTypeIdentite() {
		return typeIdentite;
	}

	public void setTypeIdentite(String typeIdentite) {
		this.typeIdentite = typeIdentite;
	}

	public String getStatutClient() {
		return statutClient;
	}

	public void setStatutClient(String statutClient) {
		this.statutClient = statutClient;
	}

	public String getModePayment() {
		return modePayment;
	}

	public void setModePayment(String modePayment) {
		this.modePayment = modePayment;
	}

	public String getClientTitle() {
		return clientTitle;
	}

	public void setClientTitle(String clientTitle) {
		this.clientTitle = clientTitle;
	}

}
