package ma.iam.mobile.customer.bean;


import java.io.Serializable;

public class SimValidate implements Serializable {

	private String sim;
	private String status;
	private String bo;
	private String type;
	private String hlcode;
	private String hlrgroup;

	

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the sim
	 */
	public String getSim() {
		return sim;
	}


	/**
	 * @param sim the sim to set
	 */
	public void setSim(String sim) {
		this.sim = sim;
	}


	/**
	 * @return the bo
	 */
	public String getBo() {
		return bo;
	}


	/**
	 * @param bo the bo to set
	 */
	public void setBo(String bo) {
		this.bo = bo;
	}


	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}


	public SimValidate() {
		super();
	}

	public String getHlcode() {
		return hlcode;
	}

	public void setHlcode(String hlcode) {
		this.hlcode = hlcode;
	}

	public String getHlrgroup() {
		return hlrgroup;
	}

	public void setHlrgroup(String hlrgroup) {
		this.hlrgroup = hlrgroup;
	}
}
