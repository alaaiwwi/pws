package ma.iam.mobile.customer.interceptor;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public interface BaseDAO {
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate();
}
