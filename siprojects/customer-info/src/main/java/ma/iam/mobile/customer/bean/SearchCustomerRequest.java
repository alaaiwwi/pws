package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebParam;

public class SearchCustomerRequest implements Serializable {

	private SearchCustomerWS searchCustomerWS;
	private Integer product;
	/**
	 * @return the searchCustomerWS
	 */
	public SearchCustomerWS getSearchCustomerWS() {
		return searchCustomerWS;
	}
	/**
	 * @param searchCustomerWS the searchCustomerWS to set
	 */
	public void setSearchCustomerWS(SearchCustomerWS searchCustomerWS) {
		this.searchCustomerWS = searchCustomerWS;
	}
	/**
	 * @return the product
	 */
	public Integer getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Integer product) {
		this.product = product;
	}
}
