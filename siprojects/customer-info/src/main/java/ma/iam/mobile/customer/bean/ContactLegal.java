package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class ContactLegal implements Serializable {

	private String nomContactLegal;
	private String cinContactLegal;
	private String tellContactLegal;
	/**
	 * @return the nomContactLegal
	 */
	public String getNomContactLegal() {
		return nomContactLegal;
	}
	/**
	 * @param nomContactLegal the nomContactLegal to set
	 */
	public void setNomContactLegal(String nomContactLegal) {
		this.nomContactLegal = nomContactLegal;
	}
	/**
	 * @return the cinContactLegal
	 */
	public String getCinContactLegal() {
		return cinContactLegal;
	}
	/**
	 * @param cinContactLegal the cinContactLegal to set
	 */
	public void setCinContactLegal(String cinContactLegal) {
		this.cinContactLegal = cinContactLegal;
	}
	/**
	 * @return the tellContactLegal
	 */
	public String getTellContactLegal() {
		return tellContactLegal;
	}
	/**
	 * @param tellContactLegal the tellContactLegal to set
	 */
	public void setTellContactLegal(String tellContactLegal) {
		this.tellContactLegal = tellContactLegal;
	}
	
	
}
