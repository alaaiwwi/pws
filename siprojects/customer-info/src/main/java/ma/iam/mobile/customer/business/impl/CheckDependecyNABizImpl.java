package ma.iam.mobile.customer.business.impl;

import ma.iam.mobile.customer.bean.ContractCreate;
import ma.iam.mobile.customer.bean.Service;
import ma.iam.mobile.customer.bean.ServiceDependency;
import ma.iam.mobile.customer.business.CheckDependecyNABiz;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.dao.CustomerInfoDao;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import org.apache.cxf.common.i18n.Exception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(value = "checkDependecyNABiz")
public class CheckDependecyNABizImpl implements CheckDependecyNABiz {
    private final static int REFRESH_PERIODE = 24;
    private Date refreshTime = null;
    @Autowired
    CustomerInfoDao customerInfoDao;
    private static  Map<String, ServiceDependency> dependeciesInstance =null;

    public Map<String, ServiceDependency> getDependecies() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR,REFRESH_PERIODE);
        if (refreshTime == null || now.getTime().after( refreshTime)) {
            List<Map<String, Object>> loadedDependecies = customerInfoDao.loadDependecies();
            dependeciesInstance = loadDependecies(loadedDependecies);
            refreshTime = new Date();
        }
        return dependeciesInstance;
    }

    @Override
    public boolean checkDependecy(String tmcode,List<Service> services) throws SyntaxiqueException {
        Map<String, ServiceDependency> dependecies = getDependecies();
        Vector<String> contractServices = new Vector<String>();
        Vector<String> dependeningServices = new Vector<String>();
        Vector<String> icompatibleServices = new Vector<String>();

        for (Service service : services) {
            contractServices.add(service.getSncode());
        }
        Collections.sort(contractServices);
        for (String sncodeToCompare : contractServices
        ) {

            // case tmcode:x, service:y
            ServiceDependency serviceDependency = dependecies.get(sncodeToCompare);
            if (serviceDependency != null) {
                dependeningServices= serviceDependency.getDependeningServices();
                icompatibleServices= serviceDependency.getIcompatibleServices();
                //check depending
                for (String d : dependeningServices) {
                    if (!contractServices.contains(d))
                        throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_DEPENDECY, "service dependant : " + d + " avec " + sncodeToCompare + " pour le plan tarrifaire : "+tmcode);
                }
                for (String d : icompatibleServices) {
                    if (contractServices.contains(d))
                        throw new SyntaxiqueException(ExceptionCodeTypeEnum.PROBLEM_DEPENDECY, "service non compatible : " + d + " avec " + sncodeToCompare + " pour le plan tarrifaire : "+tmcode);
                }
            }
        }

        return true;
    }

    @Override
    public Map<String, ServiceDependency> loadDependecies(List<Map<String, Object>> rowDependecies) {
        Map<String, ServiceDependency> dependencyHashMap = new HashMap<String, ServiceDependency>();
        for (Map<String, Object> map : rowDependecies
        ) {
            String sncode1 = map.get("sncode1").toString();
            String sncode2 = map.get("sncode2").toString();
            String tpDependency = map.get("tpDependency").toString();

            ServiceDependency serviceDependency = dependencyHashMap.get(sncode1);
            if (serviceDependency == null) {
                serviceDependency = new ServiceDependency();
                serviceDependency.setDependeningServices(new Vector<String>());
                serviceDependency.setIcompatibleServices(new Vector<String>());
                dependencyHashMap.put(sncode1, serviceDependency);
            }

            if ("P".equals(tpDependency.trim())) {
                serviceDependency.getIcompatibleServices().add(sncode2);
            } else {
                serviceDependency.getDependeningServices().add(sncode2);
            }

        }
        return dependencyHashMap;
    }

}
