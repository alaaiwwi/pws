package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Address implements Serializable {
	
	

	private String custtype;
	private String titre;
	private String nationalite;
	private String nom;
	private String prenom;
	private String rue;
	private String quartier;
	private String codePostal;
	private String complementAdresse;
	private String Telephone1Part1;
	private String Telephone1Part2;
	private String Telephone2Part1;
	private String Telephone2Part2;
	private String fax;
	private String StreetNumber;
	private String email;
	private String langue;
	private String dateNaissance;
	private String typeIdentifiant;
	private String identifiant;
	private String valable;
	private String statut;
	private String raisonStatut;
	private String ccu;
	private String ACICGC;
	private String contactLegal; 
	private String CIN;
	private String telContact;
	private String emailContact;
	private String modePaiement;
	private String compte;
	private String banque;	

	private Country country;	 
	private City city;	
	
	
	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	private List<Role> roles;
	/**
	 * @return the custtype
	 */
	public String getCusttype() {
		return custtype;
	}
	/**
	 * @param custtype the custtype to set
	 */
	public void setCusttype(String custtype) {
		this.custtype = custtype;
	}
	/**
	 * @return the streetNumber
	 */
	public String getStreetNumber() {
		return StreetNumber;
	}
	/**
	 * @param streetNumber the streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		StreetNumber = streetNumber;
	}
	/**
	 * @return the roles
	 */
	public List<Role> getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the nationalite
	 */
	public String getNationalite() {
		return nationalite;
	}
	/**
	 * @param nationalite the nationalite to set
	 */
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the rue
	 */
	public String getRue() {
		return rue;
	}
	/**
	 * @param rue the rue to set
	 */
	public void setRue(String rue) {
		this.rue = rue;
	}
	/**
	 * @return the quartier
	 */
	public String getQuartier() {
		return quartier;
	}
	/**
	 * @param quartier the quartier to set
	 */
	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}
	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}
	/**
	 * @param codePostal the codePostal to set
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	/**
	 * @return the complementAdresse
	 */
	public String getComplementAdresse() {
		return complementAdresse;
	}
	/**
	 * @param complementAdresse the complementAdresse to set
	 */
	public void setComplementAdresse(String complementAdresse) {
		this.complementAdresse = complementAdresse;
	}
	/**
	 * @return the telephone1Part1
	 */
	public String getTelephone1Part1() {
		return Telephone1Part1;
	}
	/**
	 * @param telephone1Part1 the telephone1Part1 to set
	 */
	public void setTelephone1Part1(String telephone1Part1) {
		Telephone1Part1 = telephone1Part1;
	}
	/**
	 * @return the telephone1Part2
	 */
	public String getTelephone1Part2() {
		return Telephone1Part2;
	}
	/**
	 * @param telephone1Part2 the telephone1Part2 to set
	 */
	public void setTelephone1Part2(String telephone1Part2) {
		Telephone1Part2 = telephone1Part2;
	}
	/**
	 * @return the telephone2Part1
	 */
	public String getTelephone2Part1() {
		return Telephone2Part1;
	}
	/**
	 * @param telephone2Part1 the telephone2Part1 to set
	 */
	public void setTelephone2Part1(String telephone2Part1) {
		Telephone2Part1 = telephone2Part1;
	}
	/**
	 * @return the telephone2Part2
	 */
	public String getTelephone2Part2() {
		return Telephone2Part2;
	}
	/**
	 * @param telephone2Part2 the telephone2Part2 to set
	 */
	public void setTelephone2Part2(String telephone2Part2) {
		Telephone2Part2 = telephone2Part2;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the langue
	 */
	public String getLangue() {
		return langue;
	}
	/**
	 * @param langue the langue to set
	 */
	public void setLangue(String langue) {
		this.langue = langue;
	}
	/**
	 * @return the dateNaissance
	 */
	public String getDateNaissance() {
		return dateNaissance;
	}
	/**
	 * @param dateNaissance the dateNaissance to set
	 */
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	/**
	 * @return the typeIdentifiant
	 */
	public String getTypeIdentifiant() {
		return typeIdentifiant;
	}
	/**
	 * @param typeIdentifiant the typeIdentifiant to set
	 */
	public void setTypeIdentifiant(String typeIdentifiant) {
		this.typeIdentifiant = typeIdentifiant;
	}
	/**
	 * @return the identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}
	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	/**
	 * @return the valable
	 */
	public String getValable() {
		return valable;
	}
	/**
	 * @param valable the valable to set
	 */
	public void setValable(String valable) {
		this.valable = valable;
	}
	/**
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}
	/**
	 * @param statut the statut to set
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}
	/**
	 * @return the raisonStatut
	 */
	public String getRaisonStatut() {
		return raisonStatut;
	}
	/**
	 * @param raisonStatut the raisonStatut to set
	 */
	public void setRaisonStatut(String raisonStatut) {
		this.raisonStatut = raisonStatut;
	}
	/**
	 * @return the ccu
	 */
	public String getCcu() {
		return ccu;
	}
	/**
	 * @param ccu the ccu to set
	 */
	public void setCcu(String ccu) {
		this.ccu = ccu;
	}
	/**
	 * @return the aCICGC
	 */
	public String getACICGC() {
		return ACICGC;
	}
	/**
	 * @param aCICGC the aCICGC to set
	 */
	public void setACICGC(String aCICGC) {
		ACICGC = aCICGC;
	}
	/**
	 * @return the contactLegal
	 */
	public String getContactLegal() {
		return contactLegal;
	}
	/**
	 * @param contactLegal the contactLegal to set
	 */
	public void setContactLegal(String contactLegal) {
		this.contactLegal = contactLegal;
	}
	/**
	 * @return the cIN
	 */
	public String getCIN() {
		return CIN;
	}
	/**
	 * @param cIN the cIN to set
	 */
	public void setCIN(String cIN) {
		CIN = cIN;
	}
	/**
	 * @return the telContact
	 */
	public String getTelContact() {
		return telContact;
	}
	/**
	 * @param telContact the telContact to set
	 */
	public void setTelContact(String telContact) {
		this.telContact = telContact;
	}
	/**
	 * @return the emailContact
	 */
	public String getEmailContact() {
		return emailContact;
	}
	/**
	 * @param emailContact the emailContact to set
	 */
	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}
	/**
	 * @return the modePaiement
	 */
	public String getModePaiement() {
		return modePaiement;
	}
	/**
	 * @param modePaiement the modePaiement to set
	 */
	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}
	/**
	 * @return the compte
	 */
	public String getCompte() {
		return compte;
	}
	/**
	 * @param compte the compte to set
	 */
	public void setCompte(String compte) {
		this.compte = compte;
	}
	/**
	 * @return the banque
	 */
	public String getBanque() {
		return banque;
	}
	/**
	 * @param banque the banque to set
	 */
	public void setBanque(String banque) {
		this.banque = banque;
	}
	
}
