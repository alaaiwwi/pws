/*
 * @author AtoS
 */

package ma.iam.mobile.customer.dao.impl;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.dao.CustomerEditDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.util.StringUtil;
import ma.iam.mobile.customer.util.XMLUtil;
import ma.iam.ws.tree.CheckDependencyDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CheckDependencyNADaoImpl extends BaseDAOMobileImpl implements CheckDependencyDAO {
	private static final Logger LOG = LoggerFactory.getLogger(CheckDependencyNADaoImpl.class);

	private static final String LIST_DEPENDECY = CheckDependencyNADaoImpl.class.getName()+ ".LIST_DEPENDECY";
	private static final String LIST_PRIORITY = CheckDependencyNADaoImpl.class.getName()+ ".LIST_PRIORITY";

	@Override
	public List<Map<String, Object>> loadDependencies() {
		LOG.debug("--> loadDependencies");
		final List<Map<String, Object>> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_DEPENDECY);
			Map<String, String> namedParameters = new HashMap();
			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
			LOG.debug("<-- loadDependencies");
		} catch (EmptyResultDataAccessException e) {

			LOG.debug("loadDependencies is  null for");
			return null;
		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getPriority() {
		LOG.debug("--> getPriority");
		final List<Map<String, Object>> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_DEPENDECY);
			Map<String, String> namedParameters = new HashMap();
			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
			LOG.debug("<-- getPriority");
		} catch (EmptyResultDataAccessException e) {

			LOG.debug("getPriority is  null");
			return null;
		}
		return list;
	}
}
