package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class IdTypes implements Serializable {
	
	

	private List <IdentifierType> identifierType;

	/**
	 * @return the identifierType
	 */
	public List<IdentifierType> getIdentifierType() {
		return identifierType;
	}

	/**
	 * @param identifierType the identifierType to set
	 */
	public void setIdentifierType(List<IdentifierType> identifierType) {
		this.identifierType = identifierType;
	}

	
}
