package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebParam;

public class VerifierCodeGenereRequest implements Serializable {



	private String nd;
	private String code;

	/**
	 * @return the nd
	 */
	public String getNd() {
		return nd;
	}
	/**
	 * @param nd the nd to set
	 */
	public void setNd(String nd) {
		this.nd = nd;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
}
