package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class SIMChange implements Serializable {

	private String coID;
	private String type;
	private String oldSim;
	private String newSim;
	private String reason;
	private String retention;
	private String comment;
	
	/**
	 * @return the coID
	 */
	public String getCoID() {
		return coID;
	}
	/**
	 * @param coID the coID to set
	 */
	public void setCoID(String coID) {
		this.coID = coID;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the oldSim
	 */
	public String getOldSim() {
		return oldSim;
	}
	/**
	 * @param oldSim the oldSim to set
	 */
	public void setOldSim(String oldSim) {
		this.oldSim = oldSim;
	}
	/**
	 * @return the newSim
	 */
	public String getNewSim() {
		return newSim;
	}
	/**
	 * @param newSim the newSim to set
	 */
	public void setNewSim(String newSim) {
		this.newSim = newSim;
	}
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	/**
	 * @return the retention
	 */
	public String getRetention() {
		return retention;
	}
	/**
	 * @param retention the retention to set
	 */
	public void setRetention(String retention) {
		this.retention = retention;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
