package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Countries implements Serializable {
	
	

	private List <Country> country;

	/**
	 * @return the country
	 */
	public List<Country> getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(List<Country> country) {
		this.country = country;
	}

	


}
