package ma.iam.mobile.customer.bean;


import java.io.Serializable;

public class Forfait implements Serializable {
	

	private String id;
	private String tmcode;
	private String spcode;
	private Double sncode;
	private Double prm_id;
	private String value;
	private String valueDes;
	private String fuFlag;
	private String cmdFlag;
	private String depSncode;
	private String seqNo;
	private String sdp;



	
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}




	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}




	/**
	 * @return the tmcode
	 */
	public String getTmcode() {
		return tmcode;
	}




	/**
	 * @param tmcode the tmcode to set
	 */
	public void setTmcode(String tmcode) {
		this.tmcode = tmcode;
	}




	/**
	 * @return the spcode
	 */
	public String getSpcode() {
		return spcode;
	}




	/**
	 * @param spcode the spcode to set
	 */
	public void setSpcode(String spcode) {
		this.spcode = spcode;
	}




	/**
	 * @return the sncode
	 */
	public Double getSncode() {
		return sncode;
	}




	/**
	 * @param sncode the sncode to set
	 */
	public void setSncode(Double sncode) {
		this.sncode = sncode;
	}




	/**
	 * @return the prm_id
	 */
	public Double getPrm_id() {
		return prm_id;
	}




	/**
	 * @param prm_id the prm_id to set
	 */
	public void setPrm_id(Double prm_id) {
		this.prm_id = prm_id;
	}




	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}




	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}




	/**
	 * @return the valueDes
	 */
	public String getValueDes() {
		return valueDes;
	}




	/**
	 * @param valueDes the valueDes to set
	 */
	public void setValueDes(String valueDes) {
		this.valueDes = valueDes;
	}




	/**
	 * @return the fuFlag
	 */
	public String getFuFlag() {
		return fuFlag;
	}




	/**
	 * @param fuFlag the fuFlag to set
	 */
	public void setFuFlag(String fuFlag) {
		this.fuFlag = fuFlag;
	}




	/**
	 * @return the cmdFlag
	 */
	public String getCmdFlag() {
		return cmdFlag;
	}




	/**
	 * @param cmdFlag the cmdFlag to set
	 */
	public void setCmdFlag(String cmdFlag) {
		this.cmdFlag = cmdFlag;
	}




	/**
	 * @return the depSncode
	 */
	public String getDepSncode() {
		return depSncode;
	}




	/**
	 * @param depSncode the depSncode to set
	 */
	public void setDepSncode(String depSncode) {
		this.depSncode = depSncode;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public Forfait() {
		super();
	}

	public String getSdp() {
		return sdp;
	}

	public void setSdp(String sdp) {
		this.sdp = sdp;
	}
}
