/*
 * @author AtoS
 */
package ma.iam.mobile.customer.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

/**
 * The Class BaseDAOMobile.
 */
@Repository
public class BaseDAOImpl implements BaseDAO {

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private DataSource dataSource;

	/**
	 * Sets the mobile bscs data source.
	 * 
	 * @param traceDataSource
	 *            the new mobile bscs data source
	 */
	@Autowired
	@Qualifier("mobileBscsDataSource")
	public void setTraceDataSource(final DataSource traceDataSource) {

		this.setNamedParameterJdbcTemplate(new NamedParameterJdbcTemplate(traceDataSource));

		this.dataSource = traceDataSource;
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplate(final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
