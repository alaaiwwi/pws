package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebParam;

public class SIMRequest implements Serializable {

	private SIMChange sim;
	private String userName;
	private String ticket;
	/**
	 * @return the sim
	 */
	public SIMChange getSim() {
		return sim;
	}
	/**
	 * @param sim the sim to set
	 */
	public void setSim(SIMChange sim) {
		this.sim = sim;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
