package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class OutValue implements Serializable {

	private String value;
    private String valueDes;
    
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the valueDes
	 */
	public String getValueDes() {
		return valueDes;
	}
	/**
	 * @param valueDes the valueDes to set
	 */
	public void setValueDes(String valueDes) {
		this.valueDes = valueDes;
	}
    
	
}
