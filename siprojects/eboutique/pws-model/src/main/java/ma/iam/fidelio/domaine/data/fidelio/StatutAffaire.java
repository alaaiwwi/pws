package ma.iam.fidelio.domaine.data.fidelio;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class StatutAffaire extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -767794101983990176L;

	/** The Constant CODE. */
	public static final String CODE = "code";

	/** The libelle. */
	private String libelle;

	/** The code. */
	private String code;

	/**
	 * Gets the libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the code.
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}
}
