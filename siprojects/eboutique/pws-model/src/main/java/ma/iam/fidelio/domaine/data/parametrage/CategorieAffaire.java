package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class CategorieAffaire extends BaseObject {

	/** The Constant serialVersionUID.*/
	private static final long serialVersionUID = 6005378238515742455L;

	/** The Constant CODE. */
	public static final String CODE = "code";

	/** The Constant LIBELLE. */
	public static final String LIBELLE = "libelle";

	/** The code. */
	private String code;

	/** The libelle. */
	private String libelle;

	/** The utilisateur. */
	private String utilisateur;

	/** The en service. */
	private Boolean enService;

	/**
	 * Gets the libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the utilisateur.
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * @param utilisateur the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Gets the en service.
	 * @return the en service
	 */
	public Boolean getEnService() {
		return enService;
	}

	/**
	 * Sets the en service.
	 * @param enService the new en service
	 */
	public void setEnService(Boolean enService) {
		this.enService = enService;
	}

	/**
	 * Gets the code.
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}
}
