package ma.iam.fidelio.domaine.data.parametrage;



public class ConversionMonsuelle extends Parametre {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 18987454643214654L;

	/** The nbr conv mois. */
	private Integer nbrConvMois;

	/**
	 * Gets the nbr conv mois.
	 * @return the nbr conv mois
	 */
	public Integer getNbrConvMois() {
		return nbrConvMois;
	}

	/**
	 * Sets the nbr conv mois.
	 * @param nbrConvMois the new nbr conv mois
	 */
	public void setNbrConvMois(Integer nbrConvMois) {
		this.nbrConvMois = nbrConvMois;
	}
}
