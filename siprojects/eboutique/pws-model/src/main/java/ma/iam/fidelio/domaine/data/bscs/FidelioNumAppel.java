package ma.iam.fidelio.domaine.data.bscs;

import java.util.Collection;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class FidelioNumAppel extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3360255756707269436L;

	/** The num tel. */
	private String numTel;

	/** The ref contrats. */
	private Collection<FidelioContrat> refContrats;

	/**
	 * Gets the num tel.
	 * @return the num tel
	 */
	public String getNumTel() {
		return numTel;
	}

	/**
	 * Sets the num tel.
	 * @param numTel the new num tel
	 */
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	/**
	 * Gets the ref contrats.
	 * @return the ref contrats
	 */
	public Collection<FidelioContrat> getRefContrats() {
		return refContrats;
	}

	/**
	 * Sets the ref contrats.
	 * @param refContrats the new ref contrats
	 */
	public void setRefContrats(Collection<FidelioContrat> refContrats) {
		this.refContrats = refContrats;
	}

}
