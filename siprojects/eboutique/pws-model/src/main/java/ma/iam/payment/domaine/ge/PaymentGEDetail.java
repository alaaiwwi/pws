package ma.iam.payment.domaine.ge;

import java.io.Serializable;

/**
 * The Class PaymentDetailsGE. This is an object that contains data related to the SEMA_OP_PAY_DETAILS table. Do not modify this class because it will
 * be overwritten if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_PAY_DETAILS"
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentGEDetail implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1424930177483269197L;

	/** The REF. */
	public static final String REF = "PaymentDetailsGE";

	/** The LABE l_ chkamt. */
	public static final String LABEL_CHKAMT = "Chkamt";

	/** The LABE l_ ohxact. */
	public static final String LABEL_OHXACT = "Ohxact";

	/** The LABE l_ p d_ id. */
	public static final String LABEL_PD_ID = "PdId";

	/** The LABE l_ caxact. */
	public static final String LABEL_CAXACT = "Caxact";

	/** The LABE l_ paymen t_ id. */
	public static final String LABEL_PAYMENT_ID = "PaymentId";

	/** The LABE l_ system. */
	public static final String LABEL_SYSTEM = "System";

	/** The LABE l_ id. */
	public static final String LABEL_ID = "Id";

	// constructors
	/**
	 * Instantiates a new payment details ge.
	 */
	public PaymentGEDetail() {
	}

	/**
	 * Constructor for required fields.
	 * 
	 * @param id the id
	 */
	public PaymentGEDetail(java.lang.Long id) {

		this.id = id;
	}

	// fields
	/** The caxact. */
	private java.lang.Long caxact;

	/** The ohxact. */
	private java.lang.Long ohxact;

	/** The chkamt. */
	private java.lang.Double chkamt;

	/** The id. */
	private java.lang.Long id;

	/** The system. */
	private java.lang.String system;

	/** The payment id. */
	private java.lang.Long paymentId;

	/**
	 * Return the value associated with the column: CAXACT.
	 * 
	 * @return the caxact
	 */
	public java.lang.Long getCaxact() {
		return caxact;
	}

	/**
	 * Set the value related to the column: CAXACT.
	 * 
	 * @param caxact the CAXACT value
	 */
	public void setCaxact(java.lang.Long caxact) {
		this.caxact = caxact;
	}

	/**
	 * Return the value associated with the column: OHXACT.
	 * 
	 * @return the ohxact
	 */
	public java.lang.Long getOhxact() {
		return ohxact;
	}

	/**
	 * Set the value related to the column: OHXACT.
	 * 
	 * @param ohxact the OHXACT value
	 */
	public void setOhxact(java.lang.Long ohxact) {
		this.ohxact = ohxact;
	}

	/**
	 * Return the value associated with the column: CHKAMT.
	 * 
	 * @return the chkamt
	 */
	public java.lang.Double getChkamt() {
		return chkamt;
	}

	/**
	 * Set the value related to the column: CHKAMT.
	 * 
	 * @param chkamt the CHKAMT value
	 */
	public void setChkamt(java.lang.Double chkamt) {
		this.chkamt = chkamt;
	}

	/**
	 * Return the value associated with the column: ID.
	 * 
	 * @return the id
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * Set the value related to the column: ID.
	 * 
	 * @param id the ID value
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
	}

	/**
	 * Return the value associated with the column: SYSTEM.
	 * 
	 * @return the system
	 */
	public java.lang.String getSystem() {
		return system;
	}

	/**
	 * Set the value related to the column: SYSTEM.
	 * 
	 * @param system the SYSTEM value
	 */
	public void setSystem(java.lang.String system) {
		this.system = system;
	}

	/**
	 * Return the value associated with the column: PAYMENT_ID.
	 * 
	 * @return the payment id
	 */
	public java.lang.Long getPaymentId() {
		return paymentId;
	}

	/**
	 * Set the value related to the column: PAYMENT_ID.
	 * 
	 * @param paymentId the PAYMENT_ID value
	 */
	public void setPaymentId(java.lang.Long paymentId) {
		this.paymentId = paymentId;
	}

}