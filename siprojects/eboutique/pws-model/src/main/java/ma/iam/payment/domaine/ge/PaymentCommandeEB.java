package ma.iam.payment.domaine.ge;

import java.io.Serializable;

/**
 * The Class PaymentCommandeEB.
 * @hibernate.class table="SEMA_OP_PAY_CAMMANDE_EB"
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentCommandeEB implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -1182866421511426838L;
	
	/** The hash code. */
	private int hashCode = Integer.MIN_VALUE;

	/** primary key */
	private java.lang.Long id;
	
	// fields
	private PaymentGE paymentGE;
	/** N° commande EB. */
	private java.lang.String numCmdEB;

	/**
	 * Constructor for primary key.
	 * 
	 * @param id the id
	 */
	public PaymentCommandeEB(java.lang.Long id) {
		this.id=id;
	}

	/**
	 * Constructor for required fields.
	 * 
	 * @param id the id
	 * @param status the status
	 */
	public PaymentCommandeEB(PaymentGE paymentGE, java.lang.String numCmdEB) {
		this.id = paymentGE.getId();
		this.paymentGE = paymentGE;
		this.numCmdEB = numCmdEB;
	}

	/**
	 * The getter method for the field id.
	 * @return the id.
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * The setter method for the field id.
	 * @param id the id to set.
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
	}

	/**
	 * The getter method for the field numCmdEB.
	 * @return the numCmdEB.
	 */
	public java.lang.String getNumCmdEB() {
		return numCmdEB;
	}

	/**
	 * The setter method for the field numCmdEB.
	 * @param numCmdEB the numCmdEB to set.
	 */
	public void setNumCmdEB(java.lang.String numCmdEB) {
		this.numCmdEB = numCmdEB;
	}


	/**
	 * Equals.
	 * 
	 * @param obj the obj
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!(obj instanceof PaymentCommandeEB)) {
			return false;
		} else {
			PaymentCommandeEB semaOpPayStatus = (PaymentCommandeEB) obj;
			if (null == this.getId() || null == semaOpPayStatus.getId()) {
				return false;
			} else {
				return this.getId().equals(semaOpPayStatus.getId());
			}
		}
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) {
				return super.hashCode();
			} else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}

	/**
	 * The getter method for the field paymentGE.
	 * @return the paymentGE.
	 */
	public PaymentGE getPaymentGE() {
		return paymentGE;
	}

	/**
	 * The setter method for the field paymentGE.
	 * @param paymentGE the paymentGE to set.
	 */
	public void setPaymentGE(PaymentGE paymentGE) {
		this.paymentGE = paymentGE;
	}


}