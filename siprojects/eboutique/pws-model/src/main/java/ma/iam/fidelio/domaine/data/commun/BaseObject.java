package ma.iam.fidelio.domaine.data.commun;


public class BaseObject {
	/** The Constant ID. */
	public static final String ID = "id";

	// primary key
	/** The id. */
	private Integer id;

	/**
	 * Hash code.
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		return (prime * 19 + ((id == null) ? 0 : id.hashCode()));
	}

	/**
	 * Equals.
	 * @param obj the obj
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final BaseObject other = (BaseObject) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the id.
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}
