package ma.iam.payment.domaine.bscs;

import java.io.Serializable;

/**
 * <p>
 * <ul>
 * <li>Schema : BSCS</li>
 * <li>Table : CCONTACT_ALL</li>
 * </ul>
 * </p>
 * 
 * @author Atos Origin
 * @version 1.0
 */
class ContactId implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4569183324139501231L;

	/** The Constant LABEL_ID_CLIENT. */
	public static final String LABEL_ID_CLIENT = "idClient";

	/** The Constant LABEL_NUM_ORDRE. */
	public static final String LABEL_NUM_ORDRE = "numeroOrdre";

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : Cutomer_id</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long idClient;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CCSEQ</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long numeroOrdre;

	/**
	 * Gets the id client.
	 * 
	 * @return the id client
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * Sets the id client.
	 * 
	 * @param idClient the new id client
	 */
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	/**
	 * Gets the numero ordre.
	 * 
	 * @return the numero ordre
	 */
	public Long getNumeroOrdre() {
		return numeroOrdre;
	}

	/**
	 * Sets the numero ordre.
	 * 
	 * @param numeroOrdre the new numero ordre
	 */
	public void setNumeroOrdre(Long numeroOrdre) {
		this.numeroOrdre = numeroOrdre;
	}

	/**
	 * Equals.
	 * 
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ContactId)) {
			return false;
		}
		final ContactId obj = (ContactId) other;
		return (obj.idClient.equals(idClient) && obj.getNumeroOrdre().equals(numeroOrdre));
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return idClient.intValue() + numeroOrdre.intValue();
	}

}
