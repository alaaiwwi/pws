package ma.iam.fidelio.dto;

import java.io.Serializable;

public class PrixStandardDto implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -6849252913483088979L;

	/** Prix Effectif En Point du Poste */ 
	private Integer pointsNu;
	
	/** Prix Effectif du Poste */
	private Float prixNu;
	
//	private String categorieClient;
//	
//	private Integer idCategorieClient;

	/** Constructeur */
	public PrixStandardDto() {
	}

	/**
	 * The getter method for the field pointsNu.
	 * @return the pointsNu.
	 */
	public Integer getPointsNu() {
		return pointsNu;
	}

	/**
	 * The setter method for the field pointsNu.
	 * @param pointsNu the pointsNu to set.
	 */
	public void setPointsNu(Integer prixPointNu) {
		this.pointsNu = prixPointNu;
	}

	/**
	 * The getter method for the field prixNu.
	 * @return the prixNu.
	 */
	public Float getPrixNu() {
		return prixNu;
	}

	/**
	 * The setter method for the field prixNu.
	 * @param prixNu the prixNu to set.
	 */
	public void setPrixNu(Float prixNu) {
		this.prixNu = prixNu;
	}

	
}