package ma.iam.fidelio.domaine.data.parametrage;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

/**
 * The Class Exclusion.
 * @version 1.0.0
 * @author Atos Origin
 */
public class Exclusion extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8979408932603847959L;

	/** The Constant REF_ARTICLE. */
	public static final String REF_ARTICLE = "refArticle";

	/** The Constant SOUS_CATEGORIE. */
	public static final String SOUS_CATEGORIE = "refSousCategorie";

	/** The Constant CATEGORIE_CLIENT. */
	public static final String CATEGORIE_CLIENT = "refCategorieClient";

	/** The Constant QUALITE_CLIENT. */
	public static final String QUALITE_CLIENT = "refQualite";

	/** The Constant PLANT_TARIFAIRE. */
	public static final String PLANT_TARIFAIRE = "planTarifaireId";

	/** The Constant DATE_DEBUT. */
	public static final String DATE_DEBUT = "dateDebut";

	/** The Constant DATE_FIN. */
	public static final String DATE_FIN = "dateFin";

	/** The date debut. */
	private Date dateDebut;

	/** The date fin. */
	private Date dateFin;

	/** The date contrat de. */
	private Date dateContratDe;

	/** The date contrat a. */
	private Date dateContratA;

	/** The utilisateur. */
	private String utilisateur;

	/** The ref qualite. */
	private Qualite refQualite;

	/** The price group id. */
	private Integer priceGroupId;

	/** The plan tarifaire id. */
	private Integer planTarifaireId;

	/** The ref article. */
	private Article refArticle;

	/** The ref sous categorie. */
	private SousCategorie refSousCategorie;

	/** The ref categorie client. */
	private CategorieClient refCategorieClient;

	/**
	 * Gets the ref categorie client.
	 * @return the ref categorie client
	 */
	public CategorieClient getRefCategorieClient() {
		return refCategorieClient;
	}

	/**
	 * Sets the ref categorie client.
	 * @param refCategorieClient the new ref categorie client
	 */
	public void setRefCategorieClient(CategorieClient refCategorieClient) {
		this.refCategorieClient = refCategorieClient;
	}

	/**
	 * Gets the date debut.
	 * @return the date debut
	 */
	public Date getDateDebut() {
		return dateDebut;
	}

	/**
	 * Sets the date debut.
	 * @param dateDebut the new date debut
	 */
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * Gets the date fin.
	 * @return the date fin
	 */
	public Date getDateFin() {
		return dateFin;
	}

	/**
	 * Sets the date fin.
	 * @param dateFin the new date fin
	 */
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * Gets the date contrat de.
	 * @return the date contrat de
	 */
	public Date getDateContratDe() {
		return dateContratDe;
	}

	/**
	 * Sets the date contrat de.
	 * @param dateContratDe the new date contrat de
	 */
	public void setDateContratDe(Date dateContratDe) {
		this.dateContratDe = dateContratDe;
	}

	/**
	 * Gets the date contrat a.
	 * @return the date contrat a
	 */
	public Date getDateContratA() {
		return dateContratA;
	}

	/**
	 * Sets the date contrat a.
	 * @param dateContratA the new date contrat a
	 */
	public void setDateContratA(Date dateContratA) {
		this.dateContratA = dateContratA;
	}

	/**
	 * Gets the utilisateur.
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * @param utilisateur the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Gets the ref qualite.
	 * @return the ref qualite
	 */
	public Qualite getRefQualite() {
		return refQualite;
	}

	/**
	 * Sets the ref qualite.
	 * @param refQualite the new ref qualite
	 */
	public void setRefQualite(Qualite refQualite) {
		this.refQualite = refQualite;
	}

	/**
	 * Gets the plan tarifaire id.
	 * @return the plan tarifaire id
	 */
	public Integer getPlanTarifaireId() {
		return planTarifaireId;
	}

	/**
	 * Sets the plan tarifaire id.
	 * @param planTarifaireId the new plan tarifaire id
	 */
	public void setPlanTarifaireId(Integer planTarifaireId) {
		this.planTarifaireId = planTarifaireId;
	}

	/**
	 * Gets the price group id.
	 * @return the price group id
	 */
	public Integer getPriceGroupId() {
		return priceGroupId;
	}

	/**
	 * Sets the price group id.
	 * @param priceGroupId the new price group id
	 */
	public void setPriceGroupId(Integer priceGroupId) {
		this.priceGroupId = priceGroupId;
	}

	/**
	 * Gets the ref article.
	 * @return the ref article
	 */
	public Article getRefArticle() {
		return refArticle;
	}

	/**
	 * Sets the ref article.
	 * @param refArticle the new ref article
	 */
	public void setRefArticle(Article refArticle) {
		this.refArticle = refArticle;
	}

	/**
	 * Gets the ref sous categorie.
	 * @return the ref sous categorie
	 */
	public SousCategorie getRefSousCategorie() {
		return refSousCategorie;
	}

	/**
	 * Sets the ref sous categorie.
	 * @param refSousCategorie the new ref sous categorie
	 */
	public void setRefSousCategorie(SousCategorie refSousCategorie) {
		this.refSousCategorie = refSousCategorie;
	}

}
