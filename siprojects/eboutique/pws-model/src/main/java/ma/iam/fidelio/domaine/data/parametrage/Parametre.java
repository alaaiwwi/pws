package ma.iam.fidelio.domaine.data.parametrage;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class Parametre extends BaseObject {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant DATE_DEBUT. */
	public static final String DATE_DEBUT = "dateDebut";

	/** The Constant REF_CAT_CLIENT. */
	public static final String REF_CAT_CLIENT = "refCategorieClient";

	/** The date debut. */
	private Date dateDebut;

	/** The utilisateur. */
	private String utilisateur;

	/** The ref categorie client. */
	private CategorieClient refCategorieClient;

	/**
	 * Gets the date debut.
	 * @return the date debut
	 */
	public Date getDateDebut() {
		return dateDebut;
	}

	/**
	 * Sets the date debut.
	 * @param dateDebut the new date debut
	 */
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * Gets the utilisateur.
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * @param utilisateur the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @param refCategorieClient the refCategorieClient to set
	 */
	public void setRefCategorieClient(CategorieClient refCategorieClient) {
		this.refCategorieClient = refCategorieClient;
	}

	/**
	 * @return the refCategorieClient
	 */
	public CategorieClient getRefCategorieClient() {
		return refCategorieClient;
	}

}
