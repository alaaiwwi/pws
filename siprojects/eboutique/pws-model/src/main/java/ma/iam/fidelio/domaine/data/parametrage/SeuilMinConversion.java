package ma.iam.fidelio.domaine.data.parametrage;

/**
 * The Class SeuilMinConversion.
 * @version 1.0.0
 * @author Atos Origin
 */
public class SeuilMinConversion extends Parametre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3613920643509802637L;

	/** The Constant MODE_ENG. */
	public static final String MODE_ENG = "refModeEngagement";
	
	/** The seuil min conv. */
	private Double seuilMinConv;

	/** The ref mode engagement. */
	private ModeEngagement refModeEngagement;

	/**
	 * Gets the seuil min conv.
	 * @return the seuil min conv
	 */
	public Double getSeuilMinConv() {
		return seuilMinConv;
	}

	/**
	 * Sets the seuil min conv.
	 * @param seuilMinConv the new seuil min conv
	 */
	public void setSeuilMinConv(Double seuilMinConv) {
		this.seuilMinConv = seuilMinConv;
	}

	/**
	 * Gets the ref mode engagement.
	 * @return the ref mode engagement
	 */
	public ModeEngagement getRefModeEngagement() {
		return refModeEngagement;
	}

	/**
	 * Sets the ref mode engagement.
	 * @param refModeEngagement the new ref mode engagement
	 */
	public void setRefModeEngagement(ModeEngagement refModeEngagement) {
		this.refModeEngagement = refModeEngagement;
	}

}
