package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Collection;
import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.ModeEngagement;

public class Commande extends BaseObject {

	/** La Constante serialVersionUID **/
	private static final long serialVersionUID = 152070108661952361L;

	/** La Constante REF_AFFAIRE. **/
	public static final String REF_AFFAIRE = "refAffaire";

	/** La Constante CO_ID. **/
	public static final String CO_ID = "coId";

	/** La Constante DATE_COMMANDE. **/
	public static final String DATE_COMMANDE = "dateCommande";
	
	/** La Constante DATE_LIVRAISON. **/
	public static final String DATE_LIVRAISON = "dateLivraison";

	/** La Constante REF_MODE_ENGAGEMENT. **/
	public static final String REF_MODE_ENGAGEMENT = "refModeEngagement";

	/** La date Commande. **/
	private Date dateCommande;

	/** L'id Contrat **/
	private Long coId;

	/** La référence Article. **/
	private String referenceArticle;

	/** La date Livraison. **/
	private Date dateLivraison;

	/** L'utilisateur Livraison. **/
	private String utilsateurLivraison;

	/** Le Commenatire. **/
	private String commentaire;

	/** Le solde utilise. **/
	private Long soldeUtilise;

	/** Le Complement d'argent. **/
	private Double complementArgent;

	/** Le Montant Rachat. **/
	private Double montantRachat;

	/** Le Point Rachat. **/
	private Double pointRachat;

	/** La Date d'Engagement. **/
	private Date dateEngagement;

	/** Le Solde En Cours Utilise. **/
	private Long soldeEnCoursUtilise;

	/** Le Solde Acquis Utilise. **/
	private Long soldeAcquisUtilise;

	/** L'id Engagement BSCS. **/
	private Integer engagementBscsId;

	/** La référence ModeEngagement. **/
	private ModeEngagement refModeEngagement;

	/** La référence Compensation. **/
	private Compensation refCompensation;

	/** La référence Article. **/
	private Article refArticle;

	/** La référence Affaire. **/
	private Affaire refAffaire;

	/** La référence Collection AffectationCommande. **/
	private Collection<AffectationCommande> refAffectationCommandes;
	
	/** Numéro Commande E-Boutique */
	private String numCommandeEB;

	/**
	 * Retourner l'attribut dateCommande.
	 * @return : dateCommande
	 */
	public Date getDateCommande() {
		return dateCommande;
	}

	/**
	 * Modifier l'attribut dateCommande.
	 * @param : dateCommande
	 */
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	/**
	 * Retourner l'attribut coId.
	 * @return : coId
	 */
	public Long getCoId() {
		return coId;
	}

	/**
	 * Modifier l'attribut coId.
	 * @param : coId
	 */
	public void setCoId(Long coId) {
		this.coId = coId;
	}

	/**
	 * Retourner l'attribut referenceArticle.
	 * @return : referenceArticle
	 */
	public String getReferenceArticle() {
		return referenceArticle;
	}

	/**
	 * Modifier l'attribut referenceArticle.
	 * @param : referenceArticle
	 */
	public void setReferenceArticle(String referenceArticle) {
		this.referenceArticle = referenceArticle;
	}

	/**
	 * Retourner l'attribut dateLivraison.
	 * @return : dateLivraison
	 */
	public Date getDateLivraison() {
		return dateLivraison;
	}

	/**
	 * Modifier l'attribut dateLivraison.
	 * @param : dateLivraison
	 */
	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	/**
	 * Retourner l'attribut utilsateurLivraison.
	 * @return : utilsateurLivraison
	 */
	public String getUtilsateurLivraison() {
		return utilsateurLivraison;
	}

	/**
	 * Modifier l'attribut utilsateurLivraison.
	 * @param : utilsateurLivraison
	 */
	public void setUtilsateurLivraison(String utilsateurLivraison) {
		this.utilsateurLivraison = utilsateurLivraison;
	}

	/**
	 * Retourner l'attribut commentaire.
	 * @return : commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * Modifier l'attribut commentaire.
	 * @param : commentaire
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * Retourner l'attribut soldeUtilise.
	 * @return : soldeUtilise
	 */
	public Long getSoldeUtilise() {
		return soldeUtilise;
	}

	/**
	 * Modifier l'attribut soldeUtilise.
	 * @param : soldeUtilise
	 */
	public void setSoldeUtilise(Long soldeUtilise) {
		this.soldeUtilise = soldeUtilise;
	}

	/**
	 * Retourner l'attribut complementArgent.
	 * @return : complementArgent
	 */
	public Double getComplementArgent() {
		return complementArgent;
	}

	/**
	 * Modifier l'attribut complementArgent.
	 * @param : complementArgent
	 */
	public void setComplementArgent(Double complementArgent) {
		this.complementArgent = complementArgent;
	}

	/**
	 * Retourner l'attribut montantRachat.
	 * @return : montantRachat
	 */
	public Double getMontantRachat() {
		return montantRachat;
	}

	/**
	 * Modifier l'attribut montantRachat.
	 * @param : montantRachat
	 */
	public void setMontantRachat(Double montantRachat) {
		this.montantRachat = montantRachat;
	}

	/**
	 * Retourner la référence ModeEngagement.
	 * @return : refModeEngagement
	 */
	public ModeEngagement getRefModeEngagement() {
		return refModeEngagement;
	}

	/**
	 * Modifier la référence ModeEngagement.
	 * @param : refModeEngagement
	 */
	public void setRefModeEngagement(ModeEngagement refModeEngagement) {
		this.refModeEngagement = refModeEngagement;
	}

	/**
	 * Retourner la référence Compensation.
	 * @return : refCompensation
	 */
	public Compensation getRefCompensation() {
		return refCompensation;
	}

	/**
	 * Modifier la référence Compensation.
	 * @param : refCompensation
	 */
	public void setRefCompensation(Compensation refCompensation) {
		this.refCompensation = refCompensation;
	}

	/**
	 * Retourner la référence Article.
	 * @return : refArticle
	 */
	public Article getRefArticle() {
		return refArticle;
	}

	/**
	 * Modifier la référence Article.
	 * @param : refArticle
	 */
	public void setRefArticle(Article refArticle) {
		this.refArticle = refArticle;
	}

	/**
	 * Retourner la référence Affaire.
	 * @return : refAffaire
	 */
	public Affaire getRefAffaire() {
		return refAffaire;
	}

	/**
	 * Modifier la référence Affaire.
	 * @param : refAffaire
	 */
	public void setRefAffaire(Affaire refAffaire) {
		this.refAffaire = refAffaire;
	}

	/**
	 * Retourner la référence AffectationCommandes.
	 * @return : refAffectationCommandes
	 */
	public Collection<AffectationCommande> getRefAffectationCommandes() {
		return refAffectationCommandes;
	}

	/**
	 * Modifier la référence AffectationCommandes.
	 * @param : refAffectationCommandes
	 */
	public void setRefAffectationCommandes(Collection<AffectationCommande> refAffectationCommandes) {
		this.refAffectationCommandes = refAffectationCommandes;
	}

	/**
	 * Retourner l'attribut dateEngagement.
	 * @return : dateEngagement
	 */
	public Date getDateEngagement() {
		return dateEngagement;
	}

	/**
	 * Modifier l'attribut dateEngagement.
	 * @param : dateEngagement
	 */
	public void setDateEngagement(Date dateEngagement) {
		this.dateEngagement = dateEngagement;
	}

	/**
	 * Retourner l'attribut soldeEnCoursUtilise.
	 * @return : soldeEnCoursUtilise
	 */
	public Long getSoldeEnCoursUtilise() {
		return soldeEnCoursUtilise;
	}

	/**
	 * Modifier l'attribut soldeEnCoursUtilise.
	 * @param : soldeEnCoursUtilise
	 */
	public void setSoldeEnCoursUtilise(Long soldeEnCoursUtilise) {
		this.soldeEnCoursUtilise = soldeEnCoursUtilise;
	}

	/**
	 * Retourner l'attribut engagementBscsId.
	 * @return : engagementBscsId
	 */
	public Integer getEngagementBscsId() {
		return engagementBscsId;
	}

	/**
	 * Modifier l'attribut engagementBscsId.
	 * @param : engagementBscsId
	 */
	public void setEngagementBscsId(Integer engagementBscsId) {
		this.engagementBscsId = engagementBscsId;
	}

	/**
	 * Retourner l'attribut soldeAcquisUtilise.
	 * @return : soldeAcquisUtilise
	 */
	public Long getSoldeAcquisUtilise() {
		return soldeAcquisUtilise;
	}

	/**
	 * Modifier l'attribut soldeAcquisUtilise.
	 * @param : soldeAcquisUtilise
	 */
	public void setSoldeAcquisUtilise(Long soldeAcquisUtilise) {
		this.soldeAcquisUtilise = soldeAcquisUtilise;
	}

	/**
	 * Modifier l'attribut pointRachat.
	 * @param : pointRachat
	 */
	public void setPointRachat(Double pointRachat) {
		this.pointRachat = pointRachat;
	}

	/**
	 * Retourner l'attribut pointRachat.
	 * @return : pointRachat
	 */
	public Double getPointRachat() {
		return pointRachat;
	}

	/**
	 * Modifier l'attribut numCommandeEB.
	 * @param : numCommandeEB
	 */
	public void setNumCommandeEB(String numCommandeEB) {
		this.numCommandeEB = numCommandeEB;
	}

	/**
	 * Retourner l'attribut numCommandeEB.
	 * @return : numCommandeEB
	 */
	public String getNumCommandeEB() {
		return numCommandeEB;
	}

}
