package ma.iam.payment.domaine.bscs;

import java.io.Serializable;

/**
 * The Class DetailPaiementId.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentBSCSDetailId implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8671832282765306553L;

	/**
	 * <p>
	 * Champs : CADOXACT
	 * </p>
	 * .
	 */
	private Long idPaiement;

	/**
	 * <p>
	 * Champs : CADXACT
	 * </p>
	 * .
	 */
	private Long idOrdre;

	/**
	 * Instantiates a new detail paiement id.
	 */
	public PaymentBSCSDetailId() {
	}

	/**
	 * Constructeur.
	 * 
	 * @param idPaiement Identifiant paiement
	 * @param idOrdre Identifiant de la facture
	 */
	public PaymentBSCSDetailId(Long idPaiement, Long idOrdre) {
		this.idPaiement = idPaiement;
		this.idOrdre = idOrdre;
	}

	/**
	 * Equals.
	 * 
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PaymentBSCSDetailId)) {
			return false;
		}
		final PaymentBSCSDetailId obj = (PaymentBSCSDetailId) other;
		return (obj.idPaiement.equals(idPaiement) && obj.idOrdre.equals(idOrdre));
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return 29 * super.hashCode() + idPaiement.intValue() + idOrdre.intValue();
	}

	/**
	 * Gets the id ordre.
	 * 
	 * @return the id ordre
	 */
	public Long getIdOrdre() {
		return idOrdre;
	}

	/**
	 * Sets the id ordre.
	 * 
	 * @param idOrdre the new id ordre
	 */
	public void setIdOrdre(Long idOrdre) {
		this.idOrdre = idOrdre;
	}

	/**
	 * Gets the id paiement.
	 * 
	 * @return the id paiement
	 */
	public Long getIdPaiement() {
		return idPaiement;
	}

	/**
	 * Sets the id paiement.
	 * 
	 * @param idPaiement the new id paiement
	 */
	public void setIdPaiement(Long idPaiement) {
		this.idPaiement = idPaiement;
	}

}
