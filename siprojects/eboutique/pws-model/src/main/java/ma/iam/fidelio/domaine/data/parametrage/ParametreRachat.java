package ma.iam.fidelio.domaine.data.parametrage;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class ParametreRachat extends BaseObject {
	
	/** The Constant MODE_ENG. */
	public static final String MODE_ENG = "refModeEngagement";

	/** The Constant DATE_DEBUT. */
	public static final String DATE_DEBUT = "dateDebut";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4103029714690084857L;

	/** The date debut. */
	private Date dateDebut;

	/** The periode residuaire. */
	private Integer periodeResiduaire;

	/** The prix rachat. */
	private Double prixRachat;

	/** The point rachat. */
	private Double pointRachat;

	/** The ref mode engagement. */
	private ModeEngagement refModeEngagement;

	/**
	 * Gets the date debut.
	 * @return the date debut
	 */
	public Date getDateDebut() {
		return dateDebut;
	}

	/**
	 * Sets the date debut.
	 * @param dateDebut the new date debut
	 */
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * Gets the periode residuaire.
	 * @return the periode residuaire
	 */
	public Integer getPeriodeResiduaire() {
		return periodeResiduaire;
	}

	/**
	 * Sets the periode residuaire.
	 * @param periodeResiduaire the new periode residuaire
	 */
	public void setPeriodeResiduaire(Integer periodeResiduaire) {
		this.periodeResiduaire = periodeResiduaire;
	}

	/**
	 * Gets the prix rachat.
	 * @return the prix rachat
	 */
	public Double getPrixRachat() {
		return prixRachat;
	}

	/**
	 * Sets the prix rachat.
	 * @param prixRachat the new prix rachat
	 */
	public void setPrixRachat(Double prixRachat) {
		this.prixRachat = prixRachat;
	}

	/**
	 * Sets the point rachat.
	 * @param pointRachat the pointRachat to set
	 */
	public void setPointRachat(Double pointRachat) {
		this.pointRachat = pointRachat;
	}

	/**
	 * Gets the point rachat.
	 * @return the pointRachat
	 */
	public Double getPointRachat() {
		return pointRachat;
	}

	/**
	 * @param refModeEngagement the refModeEngagement to set
	 */
	public void setRefModeEngagement(ModeEngagement refModeEngagement) {
		this.refModeEngagement = refModeEngagement;
	}

	/**
	 * @return the refModeEngagement
	 */
	public ModeEngagement getRefModeEngagement() {
		return refModeEngagement;
	}

}
