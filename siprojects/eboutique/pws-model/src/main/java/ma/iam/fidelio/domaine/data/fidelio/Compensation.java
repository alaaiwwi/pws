package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Collection;
import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;
import ma.iam.fidelio.domaine.data.parametrage.Article;

public class Compensation extends BaseObject {
	
	/** La Constante SOLDE_ACQUIS. **/
	public static final String SOLDE_ACQUIS = "soldeAcquis";

	/** La Constante SOLDE_EN_COURS. **/
	public static final String SOLDE_EN_COURS = "soldeEnCours";

	/** La Constante SOLDE_CONVERTIS. **/
	public static final String SOLDE_CONVERTIS = "soldeConvertis";

	/** La Constante SOLDE_ENCOURS_CONVERTIS. **/
	public static final String SOLDE_ENCOURS_CONVERTIS = "soldeEnCoursConvertis";

	/** La Constante SOLDE_RESIDUE. **/
	public static final String SOLDE_RESIDUE = "soldeResidue";

	/** La Constante REF_COMPTE_CLIENT. **/
	public static final String REF_COMPTE_CLIENT = "refCompteClient";

	/** La Constante REF_ARTICLE. **/
	public static final String REF_ARTICLE = "refArticle";

	/** La Constante REF_AFFAIRE. **/
	public static final String REF_AFFAIRE = "refAffaire";

	/** La Constante DATE_TRANSACTION. **/
	public static final String DATE_TRANSACTION = "dateTransaction";

	/** La Constante DATE_PURGE. **/
	public static final String DATE_PURGE = "datePurge";

	/** L'id Facture. **/
	private Long idFacture;

	/** Le solde Acquis. **/
	private Long soldeAcquis;

	/** Le solde En Cours. **/
	private Long soldeEnCours;

	/** Le montant convertible. **/
	private Float montantConvertible;

	/** La date transaction. **/
	private Date dateTransaction;

	/** La date application. **/
	private Date dateApplication;

	/** Le solde convertis. **/
	private Long soldeConvertis;

	/** Le solde En Cours Convertis. **/
	private Long soldeEnCoursConvertis;

	/** Le Solde Residue. **/
	private Long soldeResidue;

	/** Le Solde Purge. **/
	private Long soldePurge;

	/** La Date Purge. **/
	private Date datePurge;

	/** La destination Compensation. **/
	private Integer destCompensation;

	/** La référence Affaire. **/
	private Affaire refAffaire;

	/** La référence Article. **/
	private Article refArticle;

	/** La référence CompteClient. **/
	private CompteClient refCompteClient;

	/** La référence Collection DetailCompensation. **/
	private Collection<DetailCompensation> refDetailCompensations;

	/** Les Acquis non convertis. **/
	private Long acquisNonConvertis;
	
	/** En Cours Non Convertis. **/
	private Long enCoursNonConvertis;
	
	/**
	 * Retourner l'attribut acquisNonConvertis.
	 * @return : acquisNonConvertis
	 */
	public Long getAcquisNonConvertis() {
		return acquisNonConvertis;
	}

	/**
	 * Modifier l'attribut acquisNonConvertis.
	 * @param : acquisNonConvertis
	 */
	public void setAcquisNonConvertis(Long acquisNonConvertis) {
		this.acquisNonConvertis = acquisNonConvertis;
	}

	/**
	 * Retourner l'attribut enCoursNonConvertis.
	 * @return : enCoursNonConvertis
	 */
	public Long getEnCoursNonConvertis() {
		return enCoursNonConvertis;
	}

	/**
	 * Modifier l'attribut enCoursNonConvertis.
	 * @param : enCoursNonConvertis
	 */
	public void setEnCoursNonConvertis(Long enCoursNonConvertis) {
		this.enCoursNonConvertis = enCoursNonConvertis;
	}

	/**
	 * Retourner l'attribut idFacture.
	 * @return : idFacture
	 */
	public Long getIdFacture() {
		return idFacture;
	}

	/**
	 * Modifier l'attribut idFacture.
	 * @param : idFacture
	 */
	public void setIdFacture(Long idFacture) {
		this.idFacture = idFacture;
	}

	/**
	 * Retourner l'attribut soldeAcquis.
	 * @return : soldeAcquis
	 */
	public Long getSoldeAcquis() {
		return soldeAcquis;
	}

	/**
	 * Modifier l'attribut soldeAcquis.
	 * @param : soldeAcquis
	 */
	public void setSoldeAcquis(Long soldeAcquis) {
		this.soldeAcquis = soldeAcquis;
	}

	/**
	 * Retourner l'attribut soldeEnCours.
	 * @return : soldeEnCours
	 */
	public Long getSoldeEnCours() {
		return soldeEnCours;
	}

	/**
	 * Modifier l'attribut soldeEnCours.
	 * @param : soldeEnCours
	 */
	public void setSoldeEnCours(Long soldeEncours) {
		this.soldeEnCours = soldeEncours;
	}

	/**
	 * Retourner l'attribut montantConvertible.
	 * @return : montantConvertible
	 */
	public Float getMontantConvertible() {
		return montantConvertible;
	}

	/**
	 * Modifier l'attribut montantConvertible.
	 * @param : montantConvertible
	 */
	public void setMontantConvertible(Float montantConvertible) {
		this.montantConvertible = montantConvertible;
	}

	/**
	 * Retourner l'attribut dateTransaction.
	 * @return : dateTransaction
	 */
	public Date getDateTransaction() {
		return dateTransaction;
	}

	/**
	 * Modifier l'attribut dateTransaction.
	 * @param : dateTransaction
	 */
	public void setDateTransaction(Date dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	/**
	 * Retourner l'attribut dateApplication.
	 * @return : dateApplication
	 */
	public Date getDateApplication() {
		return dateApplication;
	}

	/**
	 * Modifier l'attribut dateApplication.
	 * @param : dateApplication
	 */
	public void setDateApplication(Date dateApplication) {
		this.dateApplication = dateApplication;
	}

	/**
	 * Retourner l'attribut soldeConvertis.
	 * @return : soldeConvertis
	 */
	public Long getSoldeConvertis() {
		return soldeConvertis;
	}

	/**
	 * Modifier l'attribut soldeConvertis.
	 * @param : soldeConvertis
	 */
	public void setSoldeConvertis(Long soldeConvertis) {
		this.soldeConvertis = soldeConvertis;
	}

	/**
	 * Retourner l'attribut soldePurge.
	 * @return : soldePurge
	 */
	public Long getSoldePurge() {
		return soldePurge;
	}

	/**
	 * Modifier l'attribut soldePurge.
	 * @param : soldePurge
	 */
	public void setSoldePurge(Long soldePurge) {
		this.soldePurge = soldePurge;
	}

	/**
	 * Retourner l'attribut datePurge.
	 * @return : datePurge
	 */
	public Date getDatePurge() {
		return datePurge;
	}

	/**
	 * Modifier l'attribut datePurge.
	 * @param : datePurge
	 */
	public void setDatePurge(Date datePurge) {
		this.datePurge = datePurge;
	}

	/**
	 * Retourner la référence Affaire .
	 * @return : refAffaire
	 */
	public Affaire getRefAffaire() {
		return refAffaire;
	}

	/**
	 * Modifier la référence Affaire.
	 * @param : refAffaire
	 */
	public void setRefAffaire(Affaire refAffaire) {
		this.refAffaire = refAffaire;
	}

	/**
	 * Retourner la référence Article.
	 * @return : refArticle
	 */
	public Article getRefArticle() {
		return refArticle;
	}

	/**
	 * Modifier la référence Article.
	 * @param : refArticle
	 */
	public void setRefArticle(Article refArticle) {
		this.refArticle = refArticle;
	}

	/**
	 * Retourner la référence CompteClient.
	 * @return : refCompteClient
	 */
	public CompteClient getRefCompteClient() {
		return refCompteClient;
	}

	/**
	 * Modifier l'attribut refCompteClient.
	 * @param : refCompteClient
	 */
	public void setRefCompteClient(CompteClient refCompteClient) {
		this.refCompteClient = refCompteClient;
	}

	/**
	 * Retourner la référence DetailCompensations.
	 * @return : refDetailCompensations
	 */
	public Collection<DetailCompensation> getRefDetailCompensations() {
		return refDetailCompensations;
	}

	/**
	 * Modifier la référence DetailCompensations.
	 * @param : refDetailCompensations
	 */
	public void setRefDetailCompensations(Collection<DetailCompensation> refDetailCompensations) {
		this.refDetailCompensations = refDetailCompensations;
	}

	/**
	 * Retourner l'attribut soldeResidue.
	 * @return : soldeResidue
	 */
	public Long getSoldeResidue() {
		return soldeResidue;
	}

	/**
	 * Modifier l'attribut soldeResidue.
	 * @param : soldeResidue
	 */
	public void setSoldeResidue(Long soldeResidue) {
		this.soldeResidue = soldeResidue;
	}

	/**
	 * Retourner l'attribut destCompensation.
	 * @return : destCompensation
	 */
	public Integer getDestCompensation() {
		return destCompensation;
	}

	/**
	 * Modifier l'attribut destCompensation.
	 * @param : destCompensation
	 */
	public void setDestCompensation(Integer destCompensation) {
		this.destCompensation = destCompensation;
	}

	/**
	 * Retourner l'attribut soldeEnCoursConvertis.
	 * @return : soldeEnCoursConvertis
	 */
	public Long getSoldeEnCoursConvertis() {
		return soldeEnCoursConvertis;
	}

	/**
	 * Modifier l'attribut soldeEnCoursConvertis.
	 * @param : soldeEnCoursConvertis
	 */
	public void setSoldeEnCoursConvertis(Long soldeEnCoursConvertis) {
		this.soldeEnCoursConvertis = soldeEnCoursConvertis;
	}
}