package ma.iam.fidelio.domaine.data.parametrage;

/**
 * The Class SeuilMinPrixPack.
 * @version 1.0.0
 * @author Atos Origin
 */
public class SeuilMinPrixPack extends Parametre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -454987231256349687L;

	/** The seuil prix. */
	private Float seuilPrix;

	/**
	 * Gets the seuil prix.
	 * @return the seuil prix
	 */
	public Float getSeuilPrix() {
		return seuilPrix;
	}

	/**
	 * Sets the seuil prix.
	 * @param seuilPrix the new seuil prix
	 */
	public void setSeuilPrix(Float seuilPrix) {
		this.seuilPrix = seuilPrix;
	}

}
