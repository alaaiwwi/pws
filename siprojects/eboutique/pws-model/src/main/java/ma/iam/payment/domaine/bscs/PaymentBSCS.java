package ma.iam.payment.domaine.bscs;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * <p>
 * Table : CASHRECEIPTS_ALL
 * </p>
 * .
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentBSCS implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -431887613053394519L;

	/**
	 * <p>
	 * Champ : CAXACT
	 * </p>
	 * .
	 */
	private Long id;

	/**
	 * <p>
	 * Champ : CACHKNUM
	 * </p>
	 * .
	 */
	private String numReference;

	/**
	 * <p>
	 * Champ: CATYPE
	 * </p>
	 * .
	 */
	private Long type;

	/**
	 * <p>
	 * Champ : CAENTDATE
	 * </p>
	 * .
	 */
	private Date datePaiement;

	/**
	 * <p>
	 * Champ : CACHKDATE
	 * </p>
	 * .
	 */
	private Date dateCheque;

	/**
	 * <p>
	 * Champ : CACHKAMT_GL
	 * </p>
	 * .
	 */
	private Double montantRegle;

	/**
	 * <p>
	 * Champ : CACURAMT_GL
	 * </p>
	 * .
	 */
	private Double montantCourant;

	/**
	 * <p>
	 * Champs : C.CAREM ||' - '|| PM.PT_DESC
	 * </p>
	 */
	private Date dateReception;

	/** The commentaire. */
	private String commentaire;

	/** The ref client. */
	private Client refClient;

	/** The code gl. */
	private String codeGL;

	/** The num gl. */
	private String numGL;

	/** The ca batch. */
	private Long caBatch;

	/** The posting period. */
	private String postingPeriod;

	/** The nom banque. */
	private String nomBanque;

	/** The compte banque. */
	private String compteBanque;

	/** The compte banque2. */
	private String compteBanque2;

	/** The causer name. */
	private String causerName;

	/** The debit info. */
	private String debitInfo;

	/** The si modifie. */
	private String siModifie;

	/** The pay mode. */
	private String payMode;

	/** The cost center. */
	private Integer costCenter;

	/** The raison transaction. */
	private Long raisonTransaction;

	/** The rec version. */
	private Long recVersion;

	/** The convrate type gl. */
	private Long convrateTypeGL;

	/** The convrate type doc. */
	private Long convrateTypeDoc;

	/** The cadisamt gl. */
	private Double cadisamtGL;

	/** The montant paye. */
	private Double montantPaye;

	/** The cadis amt pay. */
	private Double cadisAmtPay;

	/** The cacur amt pay. */
	private Double cacurAmtPay;

	/** The cabalance home. */
	private Double cabalanceHome;

	// 91 est l'identifiant du devise (DH)
	/** The id devise. */
	private Long idDevise = Long.valueOf(91);

	// 91 est l'identifiant du devise (DH)
	/** The gl currency. */
	private Long glCurrency = Long.valueOf(91);

	// 91 est l'identifiant du devise (DH)
	/** The devise. */
	private Long devise = Long.valueOf(91);

	/** The lignes paiement. */
	private Collection lignesPaiement;

	/**
	 * <p>
	 * </p>
	 * .
	 */
	private Collection refDetailsPaiement;

	/**
	 * Equals.
	 * 
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PaymentBSCS)) {
			return false;
		}
		final PaymentBSCS obj = (PaymentBSCS) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}

	/**
	 * Gets the commentaire.
	 * 
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * Sets the commentaire.
	 * 
	 * @param commentaire the new commentaire
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * Gets the date cheque.
	 * 
	 * @return the date cheque
	 */
	public Date getDateCheque() {
		return dateCheque != null ? (Date) dateCheque.clone() : null;
	}

	/**
	 * Sets the date cheque.
	 * 
	 * @param dateCheque the new date cheque
	 */
	public void setDateCheque(Date dateCheque) {
		this.dateCheque = dateCheque != null ? (Date) dateCheque.clone() : null;
	}

	/**
	 * Gets the date paiement.
	 * 
	 * @return the date paiement
	 */
	public Date getDatePaiement() {
		return datePaiement != null ? (Date) datePaiement.clone() : null;
	}

	/**
	 * Sets the date paiement.
	 * 
	 * @param datePaiement the new date paiement
	 */
	public void setDatePaiement(Date datePaiement) {
		this.datePaiement = datePaiement != null ? (Date) datePaiement.clone() : null;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the montant courant.
	 * 
	 * @return the montant courant
	 */
	public Double getMontantCourant() {
		return montantCourant;
	}

	/**
	 * Sets the montant courant.
	 * 
	 * @param montantCourant the new montant courant
	 */
	public void setMontantCourant(Double montantCourant) {
		this.montantCourant = montantCourant;
	}

	/**
	 * Gets the montant regle.
	 * 
	 * @return the montant regle
	 */
	public Double getMontantRegle() {
		return montantRegle;
	}

	/**
	 * Sets the montant regle.
	 * 
	 * @param montantRegle the new montant regle
	 */
	public void setMontantRegle(Double montantRegle) {
		this.montantRegle = montantRegle;
	}

	/**
	 * Gets the num reference.
	 * 
	 * @return the num reference
	 */
	public String getNumReference() {
		return numReference;
	}

	/**
	 * Sets the num reference.
	 * 
	 * @param numReference the new num reference
	 */
	public void setNumReference(String numReference) {
		this.numReference = numReference;
	}

	/**
	 * Gets the ref client.
	 * 
	 * @return the ref client
	 */
	public Client getRefClient() {
		return refClient;
	}

	/**
	 * Sets the ref client.
	 * 
	 * @param refClient the new ref client
	 */
	public void setRefClient(Client refClient) {
		this.refClient = refClient;
	}

	/**
	 * Gets the ref details paiement.
	 * 
	 * @return the ref details paiement
	 */
	public Collection getRefDetailsPaiement() {
		return refDetailsPaiement;
	}

	/**
	 * Sets the ref details paiement.
	 * 
	 * @param refDetailsPaiement the new ref details paiement
	 */
	public void setRefDetailsPaiement(Collection refDetailsPaiement) {
		this.refDetailsPaiement = refDetailsPaiement;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public Long getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type the new type
	 */
	public void setType(Long type) {
		this.type = type;
	}

	/**
	 * Gets the code gl.
	 * 
	 * @return the code gl
	 */
	public String getCodeGL() {
		return codeGL;
	}

	/**
	 * Sets the code gl.
	 * 
	 * @param codeGL the new code gl
	 */
	public void setCodeGL(String codeGL) {
		this.codeGL = codeGL;
	}

	/**
	 * Gets the num gl.
	 * 
	 * @return the num gl
	 */
	public String getNumGL() {
		return numGL;
	}

	/**
	 * Sets the num gl.
	 * 
	 * @param numGL the new num gl
	 */
	public void setNumGL(String numGL) {
		this.numGL = numGL;
	}

	/**
	 * Gets the ca batch.
	 * 
	 * @return the ca batch
	 */
	public Long getCaBatch() {
		return caBatch;
	}

	/**
	 * Sets the ca batch.
	 * 
	 * @param caBatch the new ca batch
	 */
	public void setCaBatch(Long caBatch) {
		this.caBatch = caBatch;
	}

	/**
	 * Gets the posting period.
	 * 
	 * @return the posting period
	 */
	public String getPostingPeriod() {
		return postingPeriod;
	}

	/**
	 * Sets the posting period.
	 * 
	 * @param postingPeriod the new posting period
	 */
	public void setPostingPeriod(String postingPeriod) {
		this.postingPeriod = postingPeriod;
	}

	/**
	 * Gets the nom banque.
	 * 
	 * @return the nom banque
	 */
	public String getNomBanque() {
		return nomBanque;
	}

	/**
	 * Sets the nom banque.
	 * 
	 * @param nomBanque the new nom banque
	 */
	public void setNomBanque(String nomBanque) {
		this.nomBanque = nomBanque;
	}

	/**
	 * Gets the compte banque.
	 * 
	 * @return the compte banque
	 */
	public String getCompteBanque() {
		return compteBanque;
	}

	/**
	 * Sets the compte banque.
	 * 
	 * @param compteBanque the new compte banque
	 */
	public void setCompteBanque(String compteBanque) {
		this.compteBanque = compteBanque;
	}

	/**
	 * Gets the compte banque2.
	 * 
	 * @return the compte banque2
	 */
	public String getCompteBanque2() {
		return compteBanque2;
	}

	/**
	 * Sets the compte banque2.
	 * 
	 * @param compteBanque2 the new compte banque2
	 */
	public void setCompteBanque2(String compteBanque2) {
		this.compteBanque2 = compteBanque2;
	}

	/**
	 * Gets the causer name.
	 * 
	 * @return the causer name
	 */
	public String getCauserName() {
		return causerName;
	}

	/**
	 * Sets the causer name.
	 * 
	 * @param causerName the new causer name
	 */
	public void setCauserName(String causerName) {
		this.causerName = causerName;
	}

	/**
	 * Gets the debit info.
	 * 
	 * @return the debit info
	 */
	public String getDebitInfo() {
		return debitInfo;
	}

	/**
	 * Sets the debit info.
	 * 
	 * @param debitInfo the new debit info
	 */
	public void setDebitInfo(String debitInfo) {
		this.debitInfo = debitInfo;
	}

	/**
	 * Checks if is si modifie.
	 * 
	 * @return the string
	 */
	public String isSiModifie() {
		return siModifie;
	}

	/**
	 * Sets the si modifie.
	 * 
	 * @param siModifie the new si modifie
	 */
	public void setSiModifie(String siModifie) {
		this.siModifie = siModifie;
	}

	/**
	 * Gets the cost center.
	 * 
	 * @return the cost center
	 */
	public Integer getCostCenter() {
		return costCenter;
	}

	/**
	 * Sets the cost center.
	 * 
	 * @param costCenter the new cost center
	 */
	public void setCostCenter(Integer costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * Gets the raison transaction.
	 * 
	 * @return the raison transaction
	 */
	public Long getRaisonTransaction() {
		return raisonTransaction;
	}

	/**
	 * Sets the raison transaction.
	 * 
	 * @param raisonTransaction the new raison transaction
	 */
	public void setRaisonTransaction(Long raisonTransaction) {
		this.raisonTransaction = raisonTransaction;
	}

	/**
	 * Gets the rec version.
	 * 
	 * @return the rec version
	 */
	public Long getRecVersion() {
		return recVersion;
	}

	/**
	 * Sets the rec version.
	 * 
	 * @param recVersion the new rec version
	 */
	public void setRecVersion(Long recVersion) {
		this.recVersion = recVersion;
	}

	/**
	 * Gets the convrate type gl.
	 * 
	 * @return the convrate type gl
	 */
	public Long getConvrateTypeGL() {
		return convrateTypeGL;
	}

	/**
	 * Sets the convrate type gl.
	 * 
	 * @param convrateTypeGL the new convrate type gl
	 */
	public void setConvrateTypeGL(Long convrateTypeGL) {
		this.convrateTypeGL = convrateTypeGL;
	}

	/**
	 * Gets the convrate type doc.
	 * 
	 * @return the convrate type doc
	 */
	public Long getConvrateTypeDoc() {
		return convrateTypeDoc;
	}

	/**
	 * Sets the convrate type doc.
	 * 
	 * @param convrateTypeDoc the new convrate type doc
	 */
	public void setConvrateTypeDoc(Long convrateTypeDoc) {
		this.convrateTypeDoc = convrateTypeDoc;
	}

	/**
	 * Gets the cadisamt gl.
	 * 
	 * @return the cadisamt gl
	 */
	public Double getCadisamtGL() {
		return cadisamtGL;
	}

	/**
	 * Sets the cadisamt gl.
	 * 
	 * @param cadisamtGL the new cadisamt gl
	 */
	public void setCadisamtGL(Double cadisamtGL) {
		this.cadisamtGL = cadisamtGL;
	}

	/**
	 * Gets the montant paye.
	 * 
	 * @return the montant paye
	 */
	public Double getMontantPaye() {
		return montantPaye;
	}

	/**
	 * Sets the montant paye.
	 * 
	 * @param montantPaye the new montant paye
	 */
	public void setMontantPaye(Double montantPaye) {
		this.montantPaye = montantPaye;
	}

	/**
	 * Gets the cadis amt pay.
	 * 
	 * @return the cadis amt pay
	 */
	public Double getCadisAmtPay() {
		return cadisAmtPay;
	}

	/**
	 * Sets the cadis amt pay.
	 * 
	 * @param cadisAmtPay the new cadis amt pay
	 */
	public void setCadisAmtPay(Double cadisAmtPay) {
		this.cadisAmtPay = cadisAmtPay;
	}

	/**
	 * Gets the cacur amt pay.
	 * 
	 * @return the cacur amt pay
	 */
	public Double getCacurAmtPay() {
		return cacurAmtPay;
	}

	/**
	 * Sets the cacur amt pay.
	 * 
	 * @param cacurAmtPay the new cacur amt pay
	 */
	public void setCacurAmtPay(Double cacurAmtPay) {
		this.cacurAmtPay = cacurAmtPay;
	}

	/**
	 * Gets the cabalance home.
	 * 
	 * @return the cabalance home
	 */
	public Double getCabalanceHome() {
		return cabalanceHome;
	}

	/**
	 * Sets the cabalance home.
	 * 
	 * @param cabalanceHome the new cabalance home
	 */
	public void setCabalanceHome(Double cabalanceHome) {
		this.cabalanceHome = cabalanceHome;
	}

	/**
	 * Gets the id devise.
	 * 
	 * @return the id devise
	 */
	public Long getIdDevise() {
		return idDevise;
	}

	/**
	 * Sets the id devise.
	 * 
	 * @param idDevise the new id devise
	 */
	public void setIdDevise(Long idDevise) {
		this.idDevise = idDevise;
	}

	/**
	 * Gets the gl currency.
	 * 
	 * @return the gl currency
	 */
	public Long getGlCurrency() {
		return glCurrency;
	}

	/**
	 * Sets the gl currency.
	 * 
	 * @param glCurrency the new gl currency
	 */
	public void setGlCurrency(Long glCurrency) {
		this.glCurrency = glCurrency;
	}

	/**
	 * Gets the devise.
	 * 
	 * @return the devise
	 */
	public Long getDevise() {
		return devise;
	}

	/**
	 * Sets the devise.
	 * 
	 * @param devise the new devise
	 */
	public void setDevise(Long devise) {
		this.devise = devise;
	}

	/**
	 * Gets the date reception.
	 * 
	 * @return the date reception
	 */
	public Date getDateReception() {
		return dateReception != null ? (Date) dateReception.clone() : null;
	}

	/**
	 * Sets the date reception.
	 * 
	 * @param dateReception the new date reception
	 */
	public void setDateReception(Date dateReception) {
		this.dateReception = dateReception != null ? (Date) dateReception.clone() : null;
	}

	/**
	 * Gets the lignes paiement.
	 * 
	 * @return the lignes paiement
	 */
	public Collection getLignesPaiement() {
		if (lignesPaiement == null) {
			return new HashSet();
		}
		return lignesPaiement;
	}

	/**
	 * Sets the lignes paiement.
	 * 
	 * @param lignesPaiement the new lignes paiement
	 */
	public void setLignesPaiement(Collection lignesPaiement) {
		this.lignesPaiement = lignesPaiement;
	}

	/**
	 * Gets the si modifie.
	 * 
	 * @return the si modifie
	 */
	public String getSiModifie() {
		return siModifie;
	}

	/**
	 * Gets the pay mode.
	 * 
	 * @return the pay mode
	 */
	public String getPayMode() {
		return payMode;
	}

	/**
	 * Sets the pay mode.
	 * 
	 * @param payMode the new pay mode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

}
