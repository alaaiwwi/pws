package ma.iam.fidelio.domaine.data.parametrage;

/**
 * The Class TarifAchatPoint.
 * @version 1.0.0
 * @author Atos Origin
 */
public class TarifAchatPoint extends Parametre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9181701207869282485L;
	
	
	/** The Constant REF_MODE_ENGAGEMENT. */
	public static final String REF_MODE_ENGAGEMENT = "refModeEngagement";


	/** The tarif achat pt. */
	private Double tarifAchatPt;

	/** The ref mode engagement. */
	private ModeEngagement refModeEngagement;

	/**
	 * Gets the tarif achat pt.
	 * @return the tarif achat pt
	 */
	public Double getTarifAchatPt() {
		return tarifAchatPt;
	}

	/**
	 * Sets the tarif achat pt.
	 * @param tarifAchatPt the new tarif achat pt
	 */
	public void setTarifAchatPt(Double tarifAchatPt) {
		this.tarifAchatPt = tarifAchatPt;
	}

	/**
	 * Gets the ref mode engagement.
	 * @return the ref mode engagement
	 */
	public ModeEngagement getRefModeEngagement() {
		return refModeEngagement;
	}

	/**
	 * Sets the ref mode engagement.
	 * @param refModeEngagement the new ref mode engagement
	 */
	public void setRefModeEngagement(ModeEngagement refModeEngagement) {
		this.refModeEngagement = refModeEngagement;
	}

}
