package ma.iam.fidelio.dto;

import java.io.Serializable;

public class PrixEngagementDto implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -2034546372082352231L;

	/** Prix En Point Avec Engagement */
	private Integer prixPointEngagement;
	
	/** Prix Avec Engagement */
	private Float prixEngagement;
	
	private Integer dureeEnMois;
	

	/** Constructeur */
	public PrixEngagementDto() {
	}


	/**
	 * The getter method for the field prixPointEngagement.
	 * @return the prixPointEngagement.
	 */
	public Integer getPrixPointEngagement() {
		return prixPointEngagement;
	}


	/**
	 * The setter method for the field prixPointEngagement.
	 * @param prixPointEngagement the prixPointEngagement to set.
	 */
	public void setPrixPointEngagement(Integer prixPointEngagement) {
		this.prixPointEngagement = prixPointEngagement;
	}


	/**
	 * The getter method for the field prixEngagement.
	 * @return the prixEngagement.
	 */
	public Float getPrixEngagement() {
		return prixEngagement;
	}


	/**
	 * The setter method for the field prixEngagement.
	 * @param prixEngagement the prixEngagement to set.
	 */
	public void setPrixEngagement(Float prixEngagement) {
		this.prixEngagement = prixEngagement;
	}


	/**
	 * The getter method for the field dureeEnMois.
	 * @return the dureeEnMois.
	 */
	public Integer getDureeEnMois() {
		return dureeEnMois;
	}


	/**
	 * The setter method for the field dureeEnMois.
	 * @param dureeEnMois the dureeEnMois to set.
	 */
	public void setDureeEnMois(Integer dureeEnMois) {
		this.dureeEnMois = dureeEnMois;
	}


	
}