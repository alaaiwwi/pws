package ma.iam.fidelio.domaine.data.parametrage;

import java.util.Collection;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

/**
 * The Class ModelePoste.
 * @version 1.0.0
 * @author Atos Origin
 */
public class ModelePoste extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8887309651058545714L;

	/** The Constant LIBELLE. */
	public static final String LIBELLE = "libelle";

	/** The libelle. */
	private String libelle;

	/** The ref articles. */
	private Collection<Article> refArticles;

	/**
	 * Gets the libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the ref articles.
	 * @return the ref articles
	 */
	public Collection<Article> getRefArticles() {
		return refArticles;
	}

	/**
	 * Sets the ref articles.
	 * @param refArticles the new ref articles
	 */
	public void setRefArticles(Collection<Article> refArticles) {
		this.refArticles = refArticles;
	}

}
