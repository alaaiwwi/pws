package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class SousCategorie extends BaseObject {

	/** Libelle. **/
	private String libelle;

	/** L'utilisateur. **/
	private String utilisateur;

	/** Le Price Group Id. **/
	private Integer priceGroupId;

	/** En Service. */
	private Boolean enService;
	

	/** The ref categorie client. */
	private CategorieClient refCategorieClient;

	/**
	 * Modifier L'attribut libelle.
	 * @param : libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	/**
	 * Modifier L'attribut utilisateur.
	 * @param : utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	/**
	 * Modifier L'attribut priceGroupId.
	 * @param : priceGroupId
	 */
	public void setPriceGroupId(Integer priceGroupId) {
		this.priceGroupId = priceGroupId;
	}
	
	/**
	 * Modifier L'attribut enService.
	 * @param : enService
	 */
	public void setEnService(Boolean enService) {
		this.enService = enService;
	}
	
	/**
	 * Retourner la valeur de l'atrribut libelle.
	 * @return : libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Retourner la valeur de l'atrribut utilisateur.
	 * @return : utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Retourner la valeur de l'atrribut priceGroupId.
	 * @return : priceGroupId
	 */
	public Integer getPriceGroupId() {
		return priceGroupId;
	}

	/**
	 * Retourner la valeur de l'atrribut EnService.
	 * @return : EnService
	 */
	public Boolean getEnService() {
		return enService;
	}

	/**
	 * @param refCategorieClient the refCategorieClient to set
	 */
	public void setRefCategorieClient(CategorieClient refCategorieClient) {
		this.refCategorieClient = refCategorieClient;
	}

	/**
	 * @return the refCategorieClient
	 */
	public CategorieClient getRefCategorieClient() {
		return refCategorieClient;
	}
}
