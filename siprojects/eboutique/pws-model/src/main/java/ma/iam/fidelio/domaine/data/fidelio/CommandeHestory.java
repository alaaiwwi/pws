package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class CommandeHestory extends BaseObject {

	/** La Constante serialVersionUID. **/
	private static final long serialVersionUID = 1L;

	/** L'id commande. **/
	private Integer idCommande;

	/** La date creation. **/
	private Date dateCreation;

	/** La date de livraison. **/
	private Date dateLivraison;

	/** L'id article. **/
	private Integer idArticle;

	/** L'id Contrat. **/
	private long coId;

	/** Les Points Acquis. **/
	private Integer pointsAcquis;

	/** Les Point En Cours. **/
	private Integer pointsEnCours;

	/** Le Complement d'argent. **/
	private Double complementArgent;

	/** Le Montant Rachat. **/
	private Double montantRachat;

	/** Les Points Rachat. **/
	private Integer pointsRachat;

	/** Le Prix GSM. **/
	private float prixGsm;

	/** Le Bonus. **/
	private Integer bonus;

	/** L'entreprise. **/
	private String entreprise;

	/** Le Privilége. **/
	private String privilege;

	/** L'id Client. **/
	private Integer idClient;

	/** Le numéro d'appel. **/
	private String nd;

	/** Le prix en points. **/
	private Integer prixEnPoints;

	/** Le prix effectif. **/
	private Long prixEffectif;

	/** Le solde acquis client. **/
	private Integer soldeAcquisClient;

	/** Le solde En Cours Client. **/
	private Integer soldeEncoursClient;

	/** Le type Engagement. **/
	private String typeCatalogue;

	/**
	 * Retourner L'attribut idCommande.
	 * @return : idCommande
	 */
	public Integer getIdCommande() {
		return idCommande;
	}

	/**
	 * Modifier l'attribut idCommenade.
	 * @param : idCommande
	 */
	public void setIdCommande(Integer idCommande) {
		this.idCommande = idCommande;
	}

	/**
	 * Retourner L'attribut dateCreation.
	 * @return : dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * Modifier l'attribut dateCreation.
	 * @param : dateCreation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * Retourner L'attribut dateLivraison.
	 * @return : dateLivraison
	 */
	public Date getDateLivraison() {
		return dateLivraison;
	}

	/**
	 * Modifier l'attribut dateLivraison.
	 * @param : dateLivraison
	 */
	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	/**
	 * Retourner L'attribut idArticle.
	 * @return : idArticle
	 */
	public Integer getIdArticle() {
		return idArticle;
	}

	/**
	 * Modifier l'attribut idArticle.
	 * @param : idArticle
	 */
	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}

	/**
	 * Retourner L'attribut coId.
	 * @return : coId
	 */
	public long getCoId() {
		return coId;
	}

	/**
	 * Modifier l'attribut coId.
	 * @param : coId
	 */
	public void setCoId(long coId) {
		this.coId = coId;
	}

	/**
	 * Retourner L'attribut pointsAcquis.
	 * @return : pointsAcquis
	 */
	public Integer getPointsAcquis() {
		return pointsAcquis;
	}

	/**
	 * Modifier l'attribut pointsAcquis.
	 * @param : pointsAcquis
	 */
	public void setPointsAcquis(Integer pointsAcquis) {
		this.pointsAcquis = pointsAcquis;
	}

	/**
	 * Retourner L'attribut pointsEnCours.
	 * @return : pointsEnCours
	 */
	public Integer getPointsEnCours() {
		return pointsEnCours;
	}

	/**
	 * Modifier l'attribut pointsEnCours.
	 * @param : pointsEnCours
	 */
	public void setPointsEnCours(Integer pointsEnCours) {
		this.pointsEnCours = pointsEnCours;
	}

	/**
	 * Retourner L'attribut complementArgent.
	 * @return : complementArgent
	 */
	public Double getComplementArgent() {
		return complementArgent;
	}

	/**
	 * Modifier l'attribut complementArgent.
	 * @param : complementArgent
	 */
	public void setComplementArgent(Double complementArgent) {
		this.complementArgent = complementArgent;
	}

	/**
	 * Retourner L'attribut montantRachat.
	 * @return : montantRachat
	 */
	public Double getMontantRachat() {
		return montantRachat;
	}

	/**
	 * Modifier l'attribut montantRachat.
	 * @param : montantRachat
	 */
	public void setMontantRachat(Double montantRachat) {
		this.montantRachat = montantRachat;
	}

	/**
	 * Retourner L'attribut pointsRachat.
	 * @return : pointsRachat
	 */
	public Integer getPointsRachat() {
		return pointsRachat;
	}

	/**
	 * Modifier l'attribut pointsRachat.
	 * @param : pointsRachat
	 */
	public void setPointsRachat(Integer pointsRachat) {
		this.pointsRachat = pointsRachat;
	}

	/**
	 * Retourner L'attribut prixGsm.
	 * @return : prixGsm
	 */
	public float getPrixGsm() {
		return prixGsm;
	}

	/**
	 * Modifier l'attribut prixGsm.
	 * @param : prixGsm
	 */
	public void setPrixGsm(float prixGsm) {
		this.prixGsm = prixGsm;
	}

	/**
	 * Retourner L'attribut bonus.
	 * @return : bonus
	 */
	public Integer getBonus() {
		return bonus;
	}

	/**
	 * Modifier l'attribut bonus.
	 * @param : bonus
	 */
	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	/**
	 * Retourner L'attribut entreprise.
	 * @return : entreprise
	 */
	public String getEntreprise() {
		return entreprise;
	}

	/**
	 * Modifier l'attribut entreprise.
	 * @param : entreprise
	 */
	public void setEntreprise(String entreprise) {
		this.entreprise = entreprise;
	}

	/**
	 * Retourner L'attribut privilege.
	 * @return : privilege
	 */
	public String getPrivilege() {
		return privilege;
	}

	/**
	 * Modifier l'attribut privilege.
	 * @param : privilege
	 */
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}

	/**
	 * Retourner L'attribut idClient.
	 * @return : idClient
	 */
	public Integer getIdClient() {
		return idClient;
	}

	/**
	 * Modifier l'attribut idClient.
	 * @param : idClient
	 */
	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	/**
	 * Retourner L'attribut nd.
	 * @return : nd
	 */
	public String getNd() {
		return nd;
	}

	/**
	 * Modifier l'attribut nd.
	 * @param : nd
	 */
	public void setNd(String nd) {
		this.nd = nd;
	}

	/**
	 * Modifier l'attribut prixEffectif.
	 * @param : prixEffectif
	 */
	public void setPrixEffectif(Long prixEffectif) {
		this.prixEffectif = prixEffectif;
	}

	/**
	 * Retourner L'attribut prixEffectif.
	 * @return : prixEffectif
	 */
	public Long getPrixEffectif() {
		return prixEffectif;
	}

	/**
	 * Modifier l'attribut prixEnPoints.
	 * @param : prixEnPoints
	 */
	public void setPrixEnPoints(Integer prixEnPoints) {
		this.prixEnPoints = prixEnPoints;
	}

	/**
	 * Retourner L'attribut prixEnPoints.
	 * @return : prixEnPoints
	 */
	public Integer getPrixEnPoints() {
		return prixEnPoints;
	}

	/**
	 * Modifier l'attribut soldeEncoursClient.
	 * @param : soldeEncoursClient
	 */
	public void setSoldeEncoursClient(Integer soldeEncoursClient) {
		this.soldeEncoursClient = soldeEncoursClient;
	}

	/**
	 * Retourner L'attribut soldeEncoursClient.
	 * @return : soldeEncoursClient
	 */
	public Integer getSoldeEncoursClient() {
		return soldeEncoursClient;
	}

	/**
	 * Modifier l'attribut soldeAcquisClient.
	 * @param : soldeAcquisClient
	 */
	public void setSoldeAcquisClient(Integer soldeAcquisClient) {
		this.soldeAcquisClient = soldeAcquisClient;
	}

	/**
	 * Retourner L'attribut soldeAcquisClient.
	 * @return : soldeAcquisClient
	 */
	public Integer getSoldeAcquisClient() {
		return soldeAcquisClient;
	}

	/**
	 * Modifier l'attribut typeCatalogue.
	 * @param : typeCatalogue
	 */
	public void setTypeCatalogue(String typeCatalogue) {
		this.typeCatalogue = typeCatalogue;
	}

	/**
	 * Retourner L'attribut typeCatalogue.
	 * @return : typeCatalogue
	 */
	public String getTypeCatalogue() {
		return typeCatalogue;
	}
}
