package ma.iam.payment.domaine.ge;

import java.io.Serializable;

/**
 * The Class Location This is an object that contains data related to the SEMA_OP_LOCATION table. Do not modify this class because it will be
 * overwritten if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_LOCATION"
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class Location implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5651368684315473812L;

	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_LOC_DESC. */
	public static final String LABEL_LOC_DESC = "locDesc";

	/** The Constant LABEL_OP_CODE. */
	public static final String LABEL_OP_CODE = "opCode";

	// primary key
	/** The id. */
	private Integer id;

	// fields
	/** The loc desc. */
	private String locDesc;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the loc desc.
	 * 
	 * @return the loc desc
	 */
	public String getLocDesc() {
		return locDesc;
	}

	/**
	 * Sets the loc desc.
	 * 
	 * @param locDesc the new loc desc
	 */
	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

}