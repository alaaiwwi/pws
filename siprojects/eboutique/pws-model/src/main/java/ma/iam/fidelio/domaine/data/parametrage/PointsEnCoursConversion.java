package ma.iam.fidelio.domaine.data.parametrage;

/**
 * The Class PointsEnCoursConversion.
 * @version 1.0.0
 * @author Atos Origin
 */
public class PointsEnCoursConversion extends Parametre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2328550884925489144L;

	/** The pct conversion. */
	private Integer pctConversion;

	/**
	 * Gets the pct conversion.
	 * @return the pct conversion
	 */
	public Integer getPctConversion() {
		return pctConversion;
	}

	/**
	 * Sets the pct conversion.
	 * @param pctConversion the new pct conversion
	 */
	public void setPctConversion(Integer pctConversion) {
		this.pctConversion = pctConversion;
	}
}
