package ma.iam.payment.domaine.ge;

import java.io.Serializable;

/**
 * The Class TransactionType. This is an object that contains data related to the SEMA_OP_TRX_TYPE table. Do not modify this class because it will be
 * overwritten if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_TRX_TYPE"
 * @author Atos Origin
 * @version 1.0
 */
public class TransactionType implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7603491376432553677L;

	/** The REF. */
	public static final String REF = "TransactionType";

	/** The LABE l_ restricted. */
	public static final String LABEL_RESTRICTED = "Restricted";

	/** The LABE l_ addtickler. */
	public static final String LABEL_ADDTICKLER = "Addtickler";

	/** The LABE l_ c o_ based. */
	public static final String LABEL_CO_BASED = "CoBased";

	/** The LABE l_ canceltickler. */
	public static final String LABEL_CANCELTICKLER = "Canceltickler";

	/** The LABE l_ tr x_ id. */
	public static final String LABEL_TRX_ID = "id";

	/** The LABE l_ tr x_ type. */
	public static final String LABEL_TRX_TYPE = "TrxType";

	/** The LABE l_ t r_ desc. */
	public static final String LABEL_TR_DESC = "TrDesc";

	/** The LABE l_ defaul t_ amt. */
	public static final String LABEL_DEFAULT_AMT = "DefaultAmt";

	// fields
	/** The id. */
	private Integer id;

	/** The tr desc. */
	private String trDesc;

	/** The trx type. */
	private String trxType;

	/** The default amt. */
	private Float defaultAmt;

	/** The restricted. */
	private String restricted;

	/** The co based. */
	private String coBased;

	/** The addtickler. */
	private String addtickler;

	/** The canceltickler. */
	private String canceltickler;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the tr desc.
	 * 
	 * @return the tr desc
	 */
	public java.lang.String getTrDesc() {
		return trDesc;
	}

	/**
	 * Sets the tr desc.
	 * 
	 * @param trDesc the new tr desc
	 */
	public void setTrDesc(java.lang.String trDesc) {
		this.trDesc = trDesc;
	}

	/**
	 * Gets the trx type.
	 * 
	 * @return the trx type
	 */
	public java.lang.String getTrxType() {
		return trxType;
	}

	/**
	 * Sets the trx type.
	 * 
	 * @param trxType the new trx type
	 */
	public void setTrxType(java.lang.String trxType) {
		this.trxType = trxType;
	}

	/**
	 * Gets the default amt.
	 * 
	 * @return the default amt
	 */
	public java.lang.Float getDefaultAmt() {
		return defaultAmt;
	}

	/**
	 * Sets the default amt.
	 * 
	 * @param defaultAmt the new default amt
	 */
	public void setDefaultAmt(java.lang.Float defaultAmt) {
		this.defaultAmt = defaultAmt;
	}

	/**
	 * Gets the restricted.
	 * 
	 * @return the restricted
	 */
	public java.lang.String getRestricted() {
		return restricted;
	}

	/**
	 * Sets the restricted.
	 * 
	 * @param restricted the new restricted
	 */
	public void setRestricted(java.lang.String restricted) {
		this.restricted = restricted;
	}

	/**
	 * Gets the co based.
	 * 
	 * @return the co based
	 */
	public java.lang.String getCoBased() {
		return coBased;
	}

	/**
	 * Sets the co based.
	 * 
	 * @param coBased the new co based
	 */
	public void setCoBased(java.lang.String coBased) {
		this.coBased = coBased;
	}

	/**
	 * Gets the addtickler.
	 * 
	 * @return the addtickler
	 */
	public java.lang.String getAddtickler() {
		return addtickler;
	}

	/**
	 * Sets the addtickler.
	 * 
	 * @param addtickler the new addtickler
	 */
	public void setAddtickler(java.lang.String addtickler) {
		this.addtickler = addtickler;
	}

	/**
	 * Gets the canceltickler.
	 * 
	 * @return the canceltickler
	 */
	public java.lang.String getCanceltickler() {
		return canceltickler;
	}

	/**
	 * Sets the canceltickler.
	 * 
	 * @param canceltickler the new canceltickler
	 */
	public void setCanceltickler(java.lang.String canceltickler) {
		this.canceltickler = canceltickler;
	}


}