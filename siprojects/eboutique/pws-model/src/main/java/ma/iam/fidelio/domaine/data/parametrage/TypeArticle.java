package ma.iam.fidelio.domaine.data.parametrage;

import java.util.Collection;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class TypeArticle extends BaseObject {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1941598753527339470L;

	/** The Constant REF_CATEGORIE_ARTICLE. */
	public static final String REF_CATEGORIE_ARTICLE = "refCategorieArticle";

	/** The Constant ID. */
	public static final String ID = "id";

	/** The libelle. */
	private String libelle;

	/** The ref categorie article. */
	private CategorieArticle refCategorieArticle;
	
	/** The ref articles. */
	private Collection<Article> refArticles;

	/**
	 * Gets the libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the ref categorie article.
	 * @return the ref categorie article
	 */
	public CategorieArticle getRefCategorieArticle() {
		return refCategorieArticle;
	}

	/**
	 * Sets the ref categorie article.
	 * @param refCategorieArticle the new ref categorie article
	 */
	public void setRefCategorieArticle(CategorieArticle refCategorieArticle) {
		this.refCategorieArticle = refCategorieArticle;
	}

	/**
	 * @param refArticles the refArticles to set
	 */
	public void setRefArticles(Collection<Article> refArticles) {
		this.refArticles = refArticles;
	}

	/**
	 * @return the refArticles
	 */
	public Collection<Article> getRefArticles() {
		return refArticles;
	}

}
