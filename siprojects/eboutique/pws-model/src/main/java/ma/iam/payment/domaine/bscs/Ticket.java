package ma.iam.payment.domaine.bscs;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class Ticket implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -947844022191966827L;

	/** The id client. */
	private Long idClient;

	/** The id. */
	private Long id;

	/** The code. */
	private String code;

	/** The status. */
	private String status;

	/** The priorite. */
	private Long priorite;

	/** The follow up date. */
	private Date followUpDate;

	/** The date creation. */
	private Date dateCreation;

	/** The description. */
	private String description;

	/** The description2. */
	private String description2;

	/** The rec version. */
	private Long recVersion;

	/** The follow up status. */
	private String followUpStatus;

	/** The id contrat. */
	private Long idContrat;

	/** The x cordinate. */
	private String coordinateX;

	/** The user. */
	private String user;

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the coordinate x.
	 * 
	 * @return the coordinate x
	 */
	public String getCoordinateX() {
		return coordinateX;
	}

	/**
	 * Sets the coordinate x.
	 * 
	 * @param coordinateX the new coordinate x
	 */
	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	/**
	 * Gets the id contrat.
	 * 
	 * @return the id contrat
	 */
	public Long getIdContrat() {
		return idContrat;
	}

	/**
	 * Sets the id contrat.
	 * 
	 * @param idContrat the new id contrat
	 */
	public void setIdContrat(Long idContrat) {
		this.idContrat = idContrat;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the priorite.
	 * 
	 * @return the priorite
	 */
	public Long getPriorite() {
		return priorite;
	}

	/**
	 * Sets the priorite.
	 * 
	 * @param priorite the new priorite
	 */
	public void setPriorite(Long priorite) {
		this.priorite = priorite;
	}

	/**
	 * Gets the follow up date.
	 * 
	 * @return the follow up date
	 */
	public Date getFollowUpDate() {
		return followUpDate != null ? (Date) followUpDate.clone() : null;
	}

	/**
	 * Sets the follow up date.
	 * 
	 * @param followUpDate the new follow up date
	 */
	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate != null ? (Date) followUpDate.clone() : null;
	}

	/**
	 * Gets the date creation.
	 * 
	 * @return the date creation
	 */
	public Date getDateCreation() {
		return dateCreation != null ? (Date) dateCreation.clone() : null;
	}

	/**
	 * Sets the date creation.
	 * 
	 * @param dateCreation the new date creation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation != null ? (Date) dateCreation.clone() : null;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the description2.
	 * 
	 * @return the description2
	 */
	public String getDescription2() {
		return description2;
	}

	/**
	 * Sets the description2.
	 * 
	 * @param description2 the new description2
	 */
	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	/**
	 * Gets the rec version.
	 * 
	 * @return the rec version
	 */
	public Long getRecVersion() {
		return recVersion;
	}

	/**
	 * Sets the rec version.
	 * 
	 * @param recVersion the new rec version
	 */
	public void setRecVersion(Long recVersion) {
		this.recVersion = recVersion;
	}

	/**
	 * Gets the follow up status.
	 * 
	 * @return the follow up status
	 */
	public String getFollowUpStatus() {
		return followUpStatus;
	}

	/**
	 * Sets the follow up status.
	 * 
	 * @param followUpStatus the new follow up status
	 */
	public void setFollowUpStatus(String followUpStatus) {
		this.followUpStatus = followUpStatus;
	}

	/**
	 * Gets the id client.
	 * 
	 * @return the id client
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * Sets the id client.
	 * 
	 * @param idClient the new id client
	 */
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param numTicket the new id
	 */
	public void setId(Long numTicket) {
		this.id = numTicket;
	}

}
