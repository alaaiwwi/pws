package ma.iam.payment.domaine.bscs;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class BscsUser.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class User implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4320793086178990478L;
	
	/** The user name. */
	private String userName;

	/** The description. */
	private String description;

	/** The cost center. */
	private Integer costCenter;

	/** The cost center desc. */
	private String costCenterDesc;
	
	/** The password user expiration date*/
	private Date expirationDate;

	/**
	 * Gets the cost center desc.
	 * 
	 * @return the cost center desc
	 */
	public String getCostCenterDesc() {
		return costCenterDesc;
	}

	/**
	 * Sets the cost center desc.
	 * 
	 * @param costCenterDesc the new cost center desc
	 */
	public void setCostCenterDesc(String costCenterDesc) {
		this.costCenterDesc = costCenterDesc;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the cost center.
	 * 
	 * @return the cost center
	 */
	public Integer getCostCenter() {
		return costCenter;
	}

	/**
	 * Sets the cost center.
	 * 
	 * @param costCenter the new cost center
	 */
	public void setCostCenter(Integer costCenter) {
		this.costCenter = costCenter;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

}
