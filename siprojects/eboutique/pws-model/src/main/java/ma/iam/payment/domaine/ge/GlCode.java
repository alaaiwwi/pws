package ma.iam.payment.domaine.ge;

import java.io.Serializable;


/**
 * The Class GlCode. This is an object that contains data related to the SEMA_OP_GLCODE table. Do not modify this class because it will be overwritten
 * if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_GLCODE"
 * @author Atos Origin
 * @version 1.0
 */
public class GlCode implements Serializable {

	/** The Constant serialVersionUID. */ 
	private static final long serialVersionUID = -7835917697019498859L;
	// Constante
	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_DEBGLCODE. */
	public static final String LABEL_DEBGLCODE = "debGlcode";

	/** The Constant LABEL_CRDGLCODE. */
	public static final String LABEL_CRDGLCODE = "crdGlcode";

	/** The Constant LABEL_ACCOUNT. */
	public static final String LABEL_ACCOUNT = "account";

	// primary key
	/** The id. */
	private Long id;

	// fields
	/** The deb glcode. */
	private String debGlcode;

	/** The crd glcode. */
	private String crdGlcode;

	/** The account. */
	private String account;

	// many to one
	/** The trx. */
	private TransactionType trx;

	/** The op location. */
	private Location opLocation;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the deb glcode.
	 * 
	 * @return the deb glcode
	 */
	public java.lang.String getDebGlcode() {
		return debGlcode;
	}

	/**
	 * Sets the deb glcode.
	 * 
	 * @param debGlcode the new deb glcode
	 */
	public void setDebGlcode(java.lang.String debGlcode) {
		this.debGlcode = debGlcode;
	}

	/**
	 * Gets the crd glcode.
	 * 
	 * @return the crd glcode
	 */
	public java.lang.String getCrdGlcode() {
		return crdGlcode;
	}

	/**
	 * Sets the crd glcode.
	 * 
	 * @param crdGlcode the new crd glcode
	 */
	public void setCrdGlcode(java.lang.String crdGlcode) {
		this.crdGlcode = crdGlcode;
	}

	/**
	 * Gets the account.
	 * 
	 * @return the account
	 */
	public java.lang.String getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 * 
	 * @param account the new account
	 */
	public void setAccount(java.lang.String account) {
		this.account = account;
	}

	/**
	 * Gets the trx.
	 * 
	 * @return the trx
	 */
	public TransactionType getTrx() {
		return trx;
	}

	/**
	 * Sets the trx.
	 * 
	 * @param trx the new trx
	 */
	public void setTrx(TransactionType trx) {
		this.trx = trx;
	}

	/**
	 * Gets the op location.
	 * 
	 * @return the op location
	 */
	public Location getOpLocation() {
		return opLocation;
	}

	/**
	 * Sets the op location.
	 * 
	 * @param opLocation the new op location
	 */
	public void setOpLocation(Location opLocation) {
		this.opLocation = opLocation;
	}

	/**
	 * Equals.
	 * 
	 * @param obj the obj
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!(obj instanceof GlCode)) {
			return false;
		} else {
			GlCode semaOpGlcode = (GlCode) obj;
			if (null == this.getId() || null == semaOpGlcode.getId()) {
				return false;
			} else {
				return (this.getId().equals(semaOpGlcode.getId()));
			}
		}
	}

	/*
	 * public int hashCode () { if (Integer.MIN_VALUE == this.hashCode) { if (null == this.getId()) return super.hashCode(); else { String hashStr =
	 * this.getClass().getName() + ":" + this.getId().hashCode(); this.hashCode = hashStr.hashCode(); } } return this.hashCode; }
	 */

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}


}