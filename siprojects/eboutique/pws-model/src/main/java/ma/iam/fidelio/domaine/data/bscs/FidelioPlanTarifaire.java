package ma.iam.fidelio.domaine.data.bscs;

import java.io.Serializable;


public class FidelioPlanTarifaire implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1156886478247539815L;

	/** The id. */
	private Long id;

	/** The description. */
	private String description;

	/** The code. */
	private String code;

	/**
	 * Equals.
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FidelioPlanTarifaire)) {
			return false;
		}
		final FidelioPlanTarifaire obj = (FidelioPlanTarifaire) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}

	/**
	 * Gets the description.
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the id.
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the code.
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
