package ma.iam.payment.domaine.ge;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class PaymentStatus. This is an object that contains data related to the SEMA_OP_PAY_STATUS table. Do not modify this class because it will be
 * overwritten if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_PAY_STATUS"
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentStatus implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4594302822298221070L;

	/** The REF. */
	public static final String REF = "PaymentStatus";

	/** The LABE l_ status. */
	public static final String LABEL_STATUS = "Status";

	/** The LABE l_ rej. */
	public static final String LABEL_REJ = "Rej";

	/** The LABE l_ ann. */
	public static final String LABEL_ANN = "Ann";

	/** The LABE l_ histno. */
	public static final String LABEL_HISTNO = "Histno";

	/** The LABE l_ statu s_ date. */
	public static final String LABEL_STATUS_DATE = "StatusDate";

	/** The LABE l_ caxact. */
	public static final String LABEL_CAXACT = "Caxact";

	/** The LABE l_ id. */
	public static final String LABEL_ID = "Id";

	/** The LABE l_ s t_ pyid. */
	public static final String LABEL_ST_PYID = "StPyid";

	/** The LABE l_ username. */
	public static final String LABEL_USERNAME = "Username";

	// constructors
	/**
	 * Instantiates a new payment status. 
	 */
	public PaymentStatus() {
	}

	/**
	 * Constructor for primary key.
	 * 
	 * @param id the id
	 */
	public PaymentStatus(java.lang.Long id) {
		this.id=id;
	}

	/**
	 * Constructor for required fields.
	 * 
	 * @param id the id
	 * @param status the status
	 */
	public PaymentStatus(java.lang.Long id, java.lang.String status) {

		this.id=id;
		this.status = status;
	}

	/** The hash code. */
	private int hashCode = Integer.MIN_VALUE;

	// primary key
	/** The id. */
	private java.lang.Long id;

	// fields
	/** The caxact. */
	private java.lang.Integer caxact;

	/** The histno. */
	private java.lang.Integer histno;

	/** The status. */
	private java.lang.String status;

	/** The status date. */
	private java.util.Date statusDate;

	/** The username. */
	private java.lang.String username;

	// many to one
	/** The st pyid. */
	private PaymentGE stPyid;

	/**
	 * Return the unique identifier of this class.
	 * 
	 * @return the id
	 * @hibernate.id generator-class="sequence" column="ST_ID"
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * Set the unique identifier of this class.
	 * 
	 * @param id the new ID
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}

	/**
	 * Return the value associated with the column: CAXACT.
	 * 
	 * @return the caxact
	 */
	public java.lang.Integer getCaxact() {
		return caxact;
	}

	/**
	 * Set the value related to the column: CAXACT.
	 * 
	 * @param caxact the CAXACT value
	 */
	public void setCaxact(java.lang.Integer caxact) {
		this.caxact = caxact;
	}

	/**
	 * Return the value associated with the column: HISTNO.
	 * 
	 * @return the histno
	 */
	public java.lang.Integer getHistno() {
		return histno;
	}

	/**
	 * Set the value related to the column: HISTNO.
	 * 
	 * @param histno the HISTNO value
	 */
	public void setHistno(java.lang.Integer histno) {
		this.histno = histno;
	}

	/**
	 * Return the value associated with the column: STATUS.
	 * 
	 * @return the status
	 */
	public java.lang.String getStatus() {
		return status;
	}

	/**
	 * Set the value related to the column: STATUS.
	 * 
	 * @param status the STATUS value
	 */
	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	/**
	 * Return the value associated with the column: STATUS_DATE.
	 * 
	 * @return the status date
	 */
	public java.util.Date getStatusDate() {
		return statusDate != null ? (Date) statusDate.clone() : null;
	}

	/**
	 * Set the value related to the column: STATUS_DATE.
	 * 
	 * @param statusDate the STATUS_DATE value
	 */
	public void setStatusDate(java.util.Date statusDate) {
		this.statusDate = statusDate != null ? (Date) statusDate.clone() : null;
	}

	/**
	 * Return the value associated with the column: USERNAME.
	 * 
	 * @return the username
	 */
	public java.lang.String getUsername() {
		return username;
	}

	/**
	 * Set the value related to the column: USERNAME.
	 * 
	 * @param username the USERNAME value
	 */
	public void setUsername(java.lang.String username) {
		this.username = username;
	}

	/**
	 * Return the value associated with the column: ST_PYID.
	 * 
	 * @return the st pyid
	 */
	public PaymentGE getStPyid() {
		return stPyid;
	}

	/**
	 * Set the value related to the column: ST_PYID.
	 * 
	 * @param stPyid the ST_PYID value
	 */
	public void setStPyid(PaymentGE stPyid) {
		this.stPyid = stPyid;
	}

	/**
	 * Equals.
	 * 
	 * @param obj the obj
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!(obj instanceof PaymentStatus)) {
			return false;
		} else {
			PaymentStatus semaOpPayStatus = (PaymentStatus) obj;
			if (null == this.getId() || null == semaOpPayStatus.getId()) {
				return false;
			} else {
				return this.getId().equals(semaOpPayStatus.getId());
			}
		}
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) {
				return super.hashCode();
			} else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


}