package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;



public class HistStatutAffaire extends BaseObject { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5230994993651801918L;

	/** The Constant AFFAIRE. */
	public static final String AFFAIRE = "refAffaire";

	/** The Constant DATE_STATUS. */
	public static final String DATE_STATUS = "dateStatut";

	/** The Constant STATUS_AFFAIRE. */
	public static final String STATUS_AFFAIRE = "refStatutAffaire";

	/** The Constant UTILISATEUR. */
	public static final String UTILISATEUR = "utilisateur";

	/** The date statut. */
	private Date dateStatut;

	/** The utilisateur. */
	private String utilisateur;

	/** The commentaire. */
	private String commentaire;

	/** The date saisie. */
	private Date dateSaisie;

	/** The ref affaire. */
	private Affaire refAffaire;

	/** The ref statut affaire. */
	private StatutAffaire refStatutAffaire;

	/**
	 * Gets the date statut.
	 * @return the date statut
	 */
	public Date getDateStatut() {
		return dateStatut;
	}

	/**
	 * Sets the date statut.
	 * @param dateStatut the new date statut
	 */
	public void setDateStatut(Date dateStatut) {
		this.dateStatut = dateStatut;
	}

	/**
	 * Gets the utilisateur.
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * @param utilisateur the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Gets the commentaire.
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * Sets the commentaire.
	 * @param commentaire the new commentaire
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * Gets the ref affaire.
	 * @return the ref affaire
	 */
	public Affaire getRefAffaire() {
		return refAffaire;
	}

	/**
	 * Sets the ref affaire.
	 * @param refAffaire the new ref affaire
	 */
	public void setRefAffaire(Affaire refAffaire) {
		this.refAffaire = refAffaire;
	}

	/**
	 * Gets the ref statut affaire.
	 * @return the ref statut affaire
	 */
	public StatutAffaire getRefStatutAffaire() {
		return refStatutAffaire;
	}

	/**
	 * Sets the ref statut affaire.
	 * @param refStatutAffaire the new ref statut affaire
	 */
	public void setRefStatutAffaire(StatutAffaire refStatutAffaire) {
		this.refStatutAffaire = refStatutAffaire;
	}

	/**
	 * Gets the date saisie.
	 * @return the date saisie
	 */
	public Date getDateSaisie() {
		return dateSaisie;
	}

	/**
	 * Sets the date saisie.
	 * @param dateSaisie the new date saisie
	 */
	public void setDateSaisie(Date dateSaisie) {
		this.dateSaisie = dateSaisie;
	}
}
