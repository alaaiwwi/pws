package ma.iam.fidelio.dto;

import java.io.Serializable;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class MessageDto implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = 5665841347259960322L;
	private String code;
	private String message;
	
	/**
	 * @param code
	 * @param message
	 */
	public MessageDto(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	/**
	 * The getter method for the field code.
	 * @return the code.
	 */
	public String getCode() {
		return code;
	}
	/**
	 * The setter method for the field code.
	 * @param code the code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * The getter method for the field message.
	 * @return the message.
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * The setter method for the field message.
	 * @param message the message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
