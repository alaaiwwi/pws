package ma.iam.fidelio.domaine.data.parametrage;

import java.util.Collection;
import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;
import ma.iam.fidelio.domaine.data.fidelio.Affaire;

public class TypeAffaire extends BaseObject {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2105894360281818412L;

	/** The Constant CODE. */
	public static final String CODE = "code";

	/** The Constant CATEGORIE_AFFAIRE. */
	public static final String CATEGORIE_AFFAIRE = "refCategorieAffaire";

	/** The Constant LIBELLE. */
	public static final String LIBELLE = "libelle";

	/** The code. */
	private String code;

	/** The libelle. */
	private String libelle;

	/** The coefficient. */
	private Integer coefficient;

	/** The utilisateur. */
	private String utilisateur;

	/** The date debut. */
	private Date dateDebut;

	/** The ref article. */
	private Article refArticle;
	
	/** The ref categorie affaire. */
	private CategorieAffaire refCategorieAffaire;

	/** The ref affaires. */
	private Collection<Affaire> refAffaires;

	/**
	 * Gets the libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the coefficient.
	 * @return the coefficient
	 */
	public Integer getCoefficient() {
		return coefficient;
	}

	/**
	 * Sets the coefficient.
	 * @param coefficient the new coefficient
	 */
	public void setCoefficient(Integer coefficient) {
		this.coefficient = coefficient;
	}

	/**
	 * Gets the utilisateur.
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * @param utilisateur the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Gets the ref article.
	 * @return the ref article
	 */
	public Article getRefArticle() {
		return refArticle;
	}

	/**
	 * Sets the ref article.
	 * @param article the new ref article
	 */
	public void setRefArticle(Article article) {
		this.refArticle = article;
	}

	/**
	 * Gets the date debut.
	 * @return the date debut
	 */
	public Date getDateDebut() {
		return dateDebut;
	}

	/**
	 * Sets the date debut.
	 * @param dateDebut the new date debut
	 */
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * Gets the code.
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param refCategorieAffaire the refCategorieAffaire to set
	 */
	public void setRefCategorieAffaire(CategorieAffaire refCategorieAffaire) {
		this.refCategorieAffaire = refCategorieAffaire;
	}

	/**
	 * @return the refCategorieAffaire
	 */
	public CategorieAffaire getRefCategorieAffaire() {
		return refCategorieAffaire;
	}

	/**
	 * @param refAffaires the refAffaires to set
	 */
	public void setRefAffaires(Collection<Affaire> refAffaires) {
		this.refAffaires = refAffaires;
	}

	/**
	 * @return the refAffaires
	 */
	public Collection<Affaire> getRefAffaires() {
		return refAffaires;
	}

}
