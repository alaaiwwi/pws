package ma.iam.payment.domaine.bscs;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * <ul>
 * <li>Schema : SYSADM</li>
 * <li>Table : ORDERHDR</li>
 * <li>Descrition : Facture</li>
 * </ul>
 * </p>
 * .
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class Facture implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9156240224218105779L;

	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_NUM_REFERENCE. */
	public static final String LABEL_NUM_REFERENCE = "numReference";

	/** The Constant LABEL_STATUT. */
	public static final String LABEL_STATUT = "statut";

	/** The Constant LABEL_TYPE. */
	public static final String LABEL_TYPE = "type";

	/** The Constant LABEL_DATE_FACTURE. */
	public static final String LABEL_DATE_FACTURE = "dateFacture";

	/** The Constant LABEL_MONTANT_FACTURE. */
	public static final String LABEL_MONTANT_FACTURE = "motantFacture";

	/** The Constant LABEL_DATE_ECHEANCE. */
	public static final String LABEL_DATE_ECHEANCE = "dateEcheance";

	/** The Constant LABEL_MONTANT_OUVERT. */
	public static final String LABEL_MONTANT_OUVERT = "montantOuvert";

	/** The Constant LABEL_FLAG_RECLMAE. */
	public static final String LABEL_FLAG_RECLMAE = "flagReclame";

	/** The Constant LABEL_REF_CLIENT. */
	public static final String LABEL_REF_CLIENT = "refClient";

	/** The Constant LABEL_BALANCE_PAGE_EVENT_DATE */
	public static final String LABEL_BALANCE_PAGE_EVENT_DATE = "balancePageEventDate";

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : OHXACT</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long id;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ : OHREFNUM</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String numReference;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ : OHSTATUS</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String statut;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ : OHINVTYPE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String type;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ : OHREFDATE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date dateFacture;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ :&nbsp; OHINVAMT_GL</li>
	 * </ul>
	 * </p>
	 * .
	 */

	/**
	 * <p>
	 * <ul>
	 * <li>Champ :&nbsp; BALANCE_PAGE_EVENT_DATE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date balancePageEventDate;

	private Double montantFacture;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ : OHDUEDATE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date dateEcheance;

	/** Champ OHOPNAMT_GL. */
	private Double montantOuvert;

	/**
	 * <p>
	 * <ul>
	 * <li>Champ : COMPLAINT</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String flagReclame;

	/** The ent date. */
	private Date entDate;

	/** The name. */
	private String name;

	/** The adresse. */
	private String adresse;

	/** The adresse. */
	private String adresse1;

	/**
	 * Gets the adresse1.
	 * 
	 * @return the adresse1
	 */
	public String getAdresse1() {
		return adresse1;
	}

	/**
	 * Sets the adresse1.
	 * 
	 * @param adresse1
	 *            the new adresse1
	 */
	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	/** The ville. */
	private String ville;

	/** The code postal. */
	private String codePostal;

	/** The asso cx act. */
	private Long assoCxAct;

	/** The ipp. */
	private String ipp;

	/** The glar. */
	private String glar;

	/** The ar flag. */
	private String arFlag;

	/** The mode. */
	private String mode = "X";

	/** The conv date exchange. */
	private Date convDateExchange;

	/** The cost center. */
	private Integer costCenter;

	/** The doc convrate type id. */
	private Long docConvrateTypeId;

	/** The inv amt doc. */
	private Double invAmtDoc;

	/** The opn amt doc. */
	private Double opnAmtDoc;

	/** The gl convrate type id. */
	private Long glConvrateTypeId;

	/** The version. */
	private Long version;

	/** The ref client. */
	private Client refClient;

	/** The currency. */
	private Long currency;

	/** The gl currency. */
	private Long glCurrency;

	/** The doc currency. */
	private Long docCurrency;

	/** The sec currency. */
	private Long secCurrency;

	/** The system type. */
	private String typeSystem = "Mobile";

	/**
	 * Gets the type system.
	 * 
	 * @return the type system
	 */
	public String getTypeSystem() {
		return typeSystem;
	}

	/**
	 * Sets the type system.
	 * 
	 * @param typeSystem
	 *            the new type system
	 */
	public void setTypeSystem(String typeSystem) {
		this.typeSystem = typeSystem;
	}

	/**
	 * Equals.
	 * 
	 * @param other
	 *            the other
	 * 
	 * @return true, if equals
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Facture)) {
			return false;
		}
		final Facture obj = (Facture) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}

	/**
	 * Gets the date echeance.
	 * 
	 * @return the date echeance
	 */
	public Date getDateEcheance() {
		return dateEcheance != null ? (Date) dateEcheance.clone() : null;
	}

	/**
	 * Sets the date echeance.
	 * 
	 * @param dateEcheance
	 *            the new date echeance
	 */
	public void setDateEcheance(Date dateEcheance) {
		this.dateEcheance = dateEcheance != null ? (Date) dateEcheance.clone()
				: null;
	}

	/**
	 * Gets the date facture.
	 * 
	 * @return the date facture
	 */
	public Date getDateFacture() {
		return dateFacture != null ? (Date) dateFacture.clone() : null;
	}

	/**
	 * Sets the date facture.
	 * 
	 * @param dateFacture
	 *            the new date facture
	 */
	public void setDateFacture(Date dateFacture) {
		this.dateFacture = dateFacture != null ? (Date) dateFacture.clone()
				: null;
	}

	public void setBalancePageEventDate(Date balancePageEventDate) {
		this.balancePageEventDate = balancePageEventDate;
	}

	public Date getBalancePageEventDate() {
		return balancePageEventDate;
	}

	/**
	 * Gets the flag reclame.
	 * 
	 * @return the flag reclame
	 */
	public String getFlagReclame() {
		return flagReclame;
	}

	/**
	 * Sets the flag reclame.
	 * 
	 * @param flagReclame
	 *            the new flag reclame
	 */
	public void setFlagReclame(String flagReclame) {
		this.flagReclame = flagReclame;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the montant ouvert.
	 * 
	 * @return the montant ouvert
	 */
	public Double getMontantOuvert() {
		return montantOuvert;
	}

	/**
	 * Sets the montant ouvert.
	 * 
	 * @param montantOuvert
	 *            the new montant ouvert
	 */
	public void setMontantOuvert(Double montantOuvert) {
		this.montantOuvert = 0.0;
		if (montantOuvert != null) {
			this.montantOuvert = montantOuvert;
		}
	}

	/**
	 * Gets the montant facture.
	 * 
	 * @return the montant facture
	 */
	public Double getMontantFacture() {
		return montantFacture;
	}

	/**
	 * Sets the montant facture.
	 * 
	 * @param montantFacture
	 *            the new montant facture
	 */
	public void setMontantFacture(Double montantFacture) {
		this.montantFacture = 0.0;
		if (montantFacture != null) {
			this.montantFacture = montantFacture;
		}
	}

	/**
	 * Gets the num reference.
	 * 
	 * @return the num reference
	 */
	public String getNumReference() {
		return numReference;
	}

	/**
	 * Sets the num reference.
	 * 
	 * @param numReference
	 *            the new num reference
	 */
	public void setNumReference(String numReference) {
		this.numReference = numReference;
	}

	/**
	 * Gets the statut.
	 * 
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * Sets the statut.
	 * 
	 * @param statut
	 *            the new statut
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the ref client.
	 * 
	 * @return the ref client
	 */
	public Client getRefClient() {
		return refClient;
	}

	/**
	 * Sets the ref client.
	 * 
	 * @param refClient
	 *            the new ref client
	 */
	public void setRefClient(Client refClient) {
		this.refClient = refClient;
	}

	/**
	 * Gets the ent date.
	 * 
	 * @return the ent date
	 */
	public Date getEntDate() {
		return entDate != null ? (Date) entDate.clone() : null;
	}

	/**
	 * Sets the ent date.
	 * 
	 * @param entDate
	 *            the new ent date
	 */
	public void setEntDate(Date entDate) {
		this.entDate = entDate != null ? (Date) entDate.clone() : null;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the adresse.
	 * 
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * Sets the adresse.
	 * 
	 * @param adresse
	 *            the new adresse
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * Gets the ville.
	 * 
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * Sets the ville.
	 * 
	 * @param ville
	 *            the new ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * Gets the code postal.
	 * 
	 * @return the code postal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * Sets the code postal.
	 * 
	 * @param codePostal
	 *            the new code postal
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Gets the asso cx act.
	 * 
	 * @return the asso cx act
	 */
	public Long getAssoCxAct() {
		return assoCxAct;
	}

	/**
	 * Sets the asso cx act.
	 * 
	 * @param assoCxAct
	 *            the new asso cx act
	 */
	public void setAssoCxAct(Long assoCxAct) {
		this.assoCxAct = assoCxAct;
	}

	/**
	 * Gets the ipp.
	 * 
	 * @return the ipp
	 */
	public String getIpp() {
		return ipp;
	}

	/**
	 * Sets the ipp.
	 * 
	 * @param ipp
	 *            the new ipp
	 */
	public void setIpp(String ipp) {
		this.ipp = ipp;
	}

	/**
	 * Gets the glar.
	 * 
	 * @return the glar
	 */
	public String getGlar() {
		return glar;
	}

	/**
	 * Sets the glar.
	 * 
	 * @param glar
	 *            the new glar
	 */
	public void setGlar(String glar) {
		this.glar = glar;
	}

	/**
	 * Gets the ar flag.
	 * 
	 * @return the ar flag
	 */
	public String getArFlag() {
		return arFlag;
	}

	/**
	 * Sets the ar flag.
	 * 
	 * @param arFlag
	 *            the new ar flag
	 */
	public void setArFlag(String arFlag) {
		this.arFlag = arFlag;
	}

	/**
	 * Gets the mode.
	 * 
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Sets the mode.
	 * 
	 * @param mode
	 *            the new mode
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * Gets the conv date exchange.
	 * 
	 * @return the conv date exchange
	 */
	public Date getConvDateExchange() {
		return convDateExchange != null ? (Date) convDateExchange.clone()
				: null;
	}

	/**
	 * Sets the conv date exchange.
	 * 
	 * @param convDateExchange
	 *            the new conv date exchange
	 */
	public void setConvDateExchange(Date convDateExchange) {
		this.convDateExchange = convDateExchange != null ? (Date) convDateExchange
				.clone() : null;
	}

	/**
	 * Gets the cost center.
	 * 
	 * @return the cost center
	 */
	public Integer getCostCenter() {
		return costCenter;
	}

	/**
	 * Sets the cost center.
	 * 
	 * @param costCenter
	 *            the new cost center
	 */
	public void setCostCenter(Integer costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * Gets the doc convrate type id.
	 * 
	 * @return the doc convrate type id
	 */
	public Long getDocConvrateTypeId() {
		return docConvrateTypeId;
	}

	/**
	 * Sets the doc convrate type id.
	 * 
	 * @param docConvrateTypeId
	 *            the new doc convrate type id
	 */
	public void setDocConvrateTypeId(Long docConvrateTypeId) {
		this.docConvrateTypeId = docConvrateTypeId;
	}

	/**
	 * Gets the inv amt doc.
	 * 
	 * @return the inv amt doc
	 */
	public Double getInvAmtDoc() {
		return invAmtDoc;
	}

	/**
	 * Sets the inv amt doc.
	 * 
	 * @param invAmtDoc
	 *            the new inv amt doc
	 */
	public void setInvAmtDoc(Double invAmtDoc) {
		this.invAmtDoc = 0.0;
		if (invAmtDoc != null) {
			this.invAmtDoc = invAmtDoc;
		}
	}

	/**
	 * Gets the opn amt doc.
	 * 
	 * @return the opn amt doc
	 */
	public Double getOpnAmtDoc() {
		return opnAmtDoc;
	}

	/**
	 * Sets the opn amt doc.
	 * 
	 * @param opnAmtDoc
	 *            the new opn amt doc
	 */
	public void setOpnAmtDoc(Double opnAmtDoc) {
		this.opnAmtDoc = 0.0;
		if (opnAmtDoc != null) {
			this.opnAmtDoc = opnAmtDoc;
		}
	}

	/**
	 * Gets the gl convrate type id.
	 * 
	 * @return the gl convrate type id
	 */
	public Long getGlConvrateTypeId() {
		return glConvrateTypeId;
	}

	/**
	 * Sets the gl convrate type id.
	 * 
	 * @param glConvrateTypeId
	 *            the new gl convrate type id
	 */
	public void setGlConvrateTypeId(Long glConvrateTypeId) {
		this.glConvrateTypeId = glConvrateTypeId;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the currency.
	 * 
	 * @return the currency
	 */
	public Long getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 * 
	 * @param currency
	 *            the new currency
	 */
	public void setCurrency(Long currency) {
		this.currency = currency;
	}

	/**
	 * Gets the doc currency.
	 * 
	 * @return the doc currency
	 */
	public Long getDocCurrency() {
		return docCurrency;
	}

	/**
	 * Sets the doc currency.
	 * 
	 * @param docCurrency
	 *            the new doc currency
	 */
	public void setDocCurrency(Long docCurrency) {
		this.docCurrency = docCurrency;
	}

	/**
	 * Gets the gl currency.
	 * 
	 * @return the gl currency
	 */
	public Long getGlCurrency() {
		return glCurrency;
	}

	/**
	 * Sets the gl currency.
	 * 
	 * @param glCurrency
	 *            the new gl currency
	 */
	public void setGlCurrency(Long glCurrency) {
		this.glCurrency = glCurrency;
	}

	/**
	 * Gets the sec currency.
	 * 
	 * @return the sec currency
	 */
	public Long getSecCurrency() {
		return secCurrency;
	}

	/**
	 * Sets the sec currency.
	 * 
	 * @param secCurrency
	 *            the new sec currency
	 */
	public void setSecCurrency(Long secCurrency) {
		this.secCurrency = secCurrency;
	}

}
