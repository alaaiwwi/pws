package ma.iam.fidelio.domaine.data.bscs;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class FidelioProductSAP extends BaseObject{
	
	/** La Constante CODE_PRODUCT. */
	public static final String CODE_PRODUCT = "codeProduct";
	
	/** Code Produit. */
	private String codeProduct;
	
	/** Code Produit SAP. */
	private String codeSAPProduct;
	
	/**
	 * Retourner l'attribut codeProduct.
	 * @return the codeProduct
	 */
	public String getCodeProduct() {
		return this.codeProduct;
	}
	
	/**
	 * Modifier l'attribut codeProduct.
	 * @param codeProduct the codeProduct to set
	 */
	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}
	
	/**
	 * Retourner l'attribut codeSAPProduct.
	 * @return the codeSAPProduct
	 */
	public String getCodeSAPProduct() {
		return this.codeSAPProduct;
	}
	
	/**
	 * Modifier l'attribut codeSAPProduct.
	 * @param codeSAPProduct the codeSAPProduct to set
	 */
	public void setCodeSAPProduct(String codeSAPProduct) {
		this.codeSAPProduct = codeSAPProduct;
	}
}
