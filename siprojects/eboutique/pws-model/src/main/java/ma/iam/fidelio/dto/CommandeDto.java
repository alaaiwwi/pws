package ma.iam.fidelio.dto;

public class CommandeDto {
	/** The Constant serialVersionID. */ 
	private static final long serialVersionUID = -1549909651068522006L;
	
	/** Numéro de la commande sur E-Boutique. */ 
	private String numCommande;
	
	/** ID Du Poste Dans BSCS. **/
	private Integer idArticle;
	
	/** Utilisateur E-Boutique. **/ 
	private String utilisateur;
	
	/** Code de l'agence de livraison. **/ 
	private Integer codeAgence;
	
	/** Numéro D'appel. **/
	private String numeroAppel;
	
	/** Nombre De Point Acquis. **/
	private Long soldeAcquis;
	
	/** Nombre De Point Encours. **/
	private Long soldeEncours;
	
	/** Le complement d'argent. **/
	private Double complement;
	
	/** L'id Engagament **/
	private Integer engagementId;
	
	/**
	 * Retourner la valeur de l'attribut engagementId.
	 * @return : engagementId
	 */
	public Integer getCodeEngagement() {
		return engagementId;
	}

	/**
	 * Modifier L'attribut engagementId.
	 * @param : engagementId
	 */
	public void setEngagementId(Integer engagementId) {
		this.engagementId = engagementId;
	}

	/** Constructeur */
	public CommandeDto() {
	}

	/**
	 * Modifier L'attribut numCommande.
	 * @param : numCommande
	 */
	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}
	
	/**
	 * Modifier L'attribut idArticle.
	 * @param : idArticle
	 */
	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}
	
	/**
	 * Modifier L'attribut utilisateur.
	 * @param : utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}
		
	/**
	 * Modifier L'attribut codeAgence.
	 * @param : codeAgence
	 */
	public void setCodeAgence(Integer codeAgence) {
		this.codeAgence = codeAgence;
	}
	
	/**
	 * Modifier L'attribut numeroAppel.
	 * @param : numeroAppel
	 */
	public void setNumeroAppel(String numeroAppel) {
		this.numeroAppel = numeroAppel;
	}
	
	/**
	 * Modifier L'attribut pointsAcquis.
	 * @param : pointsAcquis
	 */
	public void setSoldeAcquis(Long soldeAcquis) {
		this.soldeAcquis = soldeAcquis;
	}
	
	/**
	 * Modifier L'attribut pointsEncours.
	 * @param : pointsEncours
	 */
	public void setSoldeEncours(Long soldeEncours) {
		this.soldeEncours = soldeEncours;
	}
	
	/**
	 * Modifier L'attribut complement.
	 * @param : complement
	 */
	public void setComplement(Double complement) {
		this.complement = complement;
	}
	
	/**
	 * Retourner la valeur de l'attribut numCommande.
	 * @return : numCommande
	 */
	public String getNumCommande() {
		return numCommande;
	}

	/**
	 * Retourner la valeur de l'attribut idArticle.
	 * @return : idArticle
	 */
	public Integer getIdArticle() {
		return idArticle;
	}
	
	/**
	 * Retourner la valeur de l'attribut utilisateur.
	 * @return : utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Retourner la valeur de l'attribut codeAgence.
	 * @return : codeAgence 
	 */
	public Integer getCodeAgence() {
		return codeAgence;
	}

	/**
	 * Retourner la valeur de l'attribut numeroAppel.
	 * @return : numeroAppel
	 */
	public String getNumeroAppel() {
		return numeroAppel;
	}

	/**
	 * Retourner la valeur de l'attribut pointsAcquis.
	 * @return : pointsAcquis
	 */
	public Long getSoldeAcquis() {
		return soldeAcquis;
	}

	/**
	 * Retourner la valeur de l'attribut pointsEncours.
	 * @return : pointsEncours
	 */
	public Long getSoldeEncours() {
		return soldeEncours;
	}

	/**
	 * Retourner la valeur de l'attribut complement.
	 * @return : complement
	 */
	public Double getComplement() {
		return complement;
	}

}
