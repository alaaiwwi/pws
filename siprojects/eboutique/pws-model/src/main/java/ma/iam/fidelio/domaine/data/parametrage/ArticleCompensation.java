package ma.iam.fidelio.domaine.data.parametrage;


public class ArticleCompensation extends Parametre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2510406807927918088L;

	/** The id article. */
	private Integer idArticle;

	/**
	 * Sets the id article.
	 * @param idArticle the idArticle to set
	 */
	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}

	/**
	 * Gets the id article.
	 * @return the idArticle
	 */
	public Integer getIdArticle() {
		return idArticle;
	}

}
