package ma.iam.fidelio.domaine.data.parametrage;


public class SeuilImpaye extends Parametre {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -68106796869702232L;

	/** The seuil impayes. */
	private Double seuilImpayes;

	/**
	 * Gets the seuil impayes.
	 * @return the seuil impayes
	 */
	public Double getSeuilImpayes() {
		return seuilImpayes;
	}

	/**
	 * Sets the seuil impayes.
	 * @param seuilImpayes the new seuil impayes
	 */
	public void setSeuilImpayes(Double seuilImpayes) {
		this.seuilImpayes = seuilImpayes;
	}
}
