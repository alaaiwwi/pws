package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;


public class Article extends BaseObject {
	
	/** The Constant REF_TYPE_ARTICLE. */
	public static final String REF_TYPE_ARTICLE = "refTypeArticle";
	
	public static final String CODE_PRODUIT = "codeProduit";

	/** Libelle. **/
	private String libelle;
	
	/** Le code produit. **/
	private String codeProduit;
	
	/** La quantité.  **/
	private Integer quantite;
	
	/** L'utilisateur. **/
	private String utilisateur;
	
	/** En service. **/
	private Boolean enService;
	
	/** AvecEngagement. **/
	private Boolean avecEngagement;
	
	/** SansEngagement. **/
	private Boolean sansEngagement;
	
	/** Le Référence TypeArticle. **/
	private TypeArticle refTypeArticle;
	
	/** The ref modele poste. */
	private ModelePoste refModelePoste;
	
	/**
	 * Modifier L'attribut libelle.
	 * @param : libelle
	 */
	public void setLibelle(String libelle) { 
		this.libelle = libelle; 
	}
	
	/**
	 * Modifier L'attribut codeProduit.
	 * @param : codeProduit
	 */
	public void setCodeProduit(String codeProduit) { 
		this.codeProduit = codeProduit; 
	}
	
	/**
	 * Modifier L'attribut quantite.
	 * @param : quantite
	 */
	public void setQuantite(Integer quantite) { 
		this.quantite = quantite; 
	}
	
	/**
	 * Modifier L'attribut utilisateur.
	 * @param : utilisateur
	 */
	public void setUtilisateur(String utilisateur) { 
		this.utilisateur = utilisateur; 
	}
	
	/**
	 * Modifier L'attribut enService.
	 * @param : enService
	 */
	public void setEnService(Boolean enService) { 
		this.enService = enService; 
	}
	
	/**
	 * Modifier L'attribut avecEngagement.
	 * @param : avecEngagement
	 */
	public void setAvecEngagement(Boolean avecEngagement) { 
		this.avecEngagement = avecEngagement; 
	}
	
	/**
	 * Modifier L'attribut sansEngagement.
	 * @param : sansEngagement
	 */
	public void setSansEngagement(Boolean sansEngagement) { 
		this.sansEngagement = sansEngagement; 
	}
	
	/**
	 * Modifier L'attribut refTypeArticle.
	 * @param : refTypeArticle
	 */
	public void setRefTypeArticle(TypeArticle refTypeArticle) {
		this.refTypeArticle = refTypeArticle;
	}

	/**
	 * Retourner la valeur de l'attribut libelle.
	 * @return : libelle
	 */
	public String getLibelle() {
		return libelle;
	}
	
	/**
	 * Retourner la valeur de l'attribut codeProduit.
	 * @return : codeProduit
	 */
	public String getCodeProduit() {
		return codeProduit;
	}
	
	/**
	 * Retourner la valeur de l'attribut quantite.
	 * @return : quantite
	 */
	public Integer getQuantite() {
		return quantite;
	}
	
	/**
	 * Retourner la valeur de l'attribut utilisateur.	
	 * @return : utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}
	
	/**
	 * Retourner la valeur de l'attribut enService.	
	 * @return : enService
	 */
	public Boolean isEnService() {
		return enService;
	}
	
	/**
	 * Retourner la valeur de l'attribut avecEngagement.	
	 * @return : avecEngagement
	 */
	public Boolean isAvecEngagement() {
		return avecEngagement;
	}
	
	/**
	 * Retourner la valeur de l'attribut sansEngagement.	
	 * @return : sansEngagement
	 */
	public Boolean isSansEngagement() {
		return sansEngagement;
	}
	
	/**
	 * Retourner la valeur de l'attribut refTypeArticle.	
	 * @return : refTypeArticle
	 */
	public TypeArticle getRefTypeArticle() {
		return refTypeArticle;
	}

	/**
	 * The getter method for the field refModelePoste.
	 * @return the refModelePoste.
	 */
	public ModelePoste getRefModelePoste() {
		return refModelePoste;
	}

	/**
	 * The setter method for the field refModelePoste.
	 * @param refModelePoste the refModelePoste to set.
	 */
	public void setRefModelePoste(ModelePoste refModelePoste) {
		this.refModelePoste = refModelePoste;
	}
}
