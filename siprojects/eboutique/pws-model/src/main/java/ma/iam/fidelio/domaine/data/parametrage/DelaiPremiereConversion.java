package ma.iam.fidelio.domaine.data.parametrage;

public class DelaiPremiereConversion extends Parametre {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5118204879103280476L;

	public static final String DATE_DEBUT = "dateDebut";

	public static final String REF_CAT_CLIENT = "refCategorieClient";

	/** The delai1ere conv. */
	private Integer delai1ereConv;

	/**
	 * Gets the delai1ere conv.
	 * @return the delai1ere conv
	 */
	public Integer getDelai1ereConv() {
		return delai1ereConv;
	}

	/**
	 * Sets the delai1ere conv.
	 * @param delai1ereConv the new delai1ere conv
	 */
	public void setDelai1ereConv(Integer delai1ereConv) {
		this.delai1ereConv = delai1ereConv;
	}
}
