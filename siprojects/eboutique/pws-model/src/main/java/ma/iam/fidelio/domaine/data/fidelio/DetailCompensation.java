package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;


public class DetailCompensation extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2841907579533355129L;

	/** The Constant SOLDE_ACQUIS. */
	public static final String SOLDE_ACQUIS = "soldeAcquis";

	/** The Constant SOLDE_EN_COURS. */
	public static final String SOLDE_EN_COURS = "soldeEnCours";

	/** The Constant REF_COMPENSATION. */
	public static final String REF_COMPENSATION = "refCompensation";

	/** The Constant DATE_TRANSACTION. */
	public static final String DATE_TRANSACTION = "dateTransaction";

	/** The solde acquis. */
	private Long soldeAcquis;

	/** The solde en cours. */
	private Long soldeEnCours;

	/** The montant convertible. */
	private Long montantConvertible;

	/** The solde converti. */
	private Long soldeConverti;

	/** The solde purge. */
	private Integer soldePurge;

	/** The date transaction. */
	private Date dateTransaction;

	/** The utilisateur. */
	private String utilisateur;

	/** The ref compensation. */
	private Compensation refCompensation;

	/**
	 * Gets the solde acquis.
	 * @return the solde acquis
	 */
	public Long getSoldeAcquis() {
		return soldeAcquis;
	}

	/**
	 * Sets the solde acquis.
	 * @param soldeAcquis the new solde acquis
	 */
	public void setSoldeAcquis(Long soldeAcquis) {
		this.soldeAcquis = soldeAcquis;
	}

	/**
	 * Gets the solde en cours.
	 * @return the solde en cours
	 */
	public Long getSoldeEnCours() {
		return soldeEnCours;
	}

	/**
	 * Sets the solde en cours.
	 * @param soldeEnCours the new solde en cours
	 */
	public void setSoldeEnCours(Long soldeEnCours) {
		this.soldeEnCours = soldeEnCours;
	}

	/**
	 * Gets the montant convertible.
	 * @return the montant convertible
	 */
	public Long getMontantConvertible() {
		return montantConvertible;
	}

	/**
	 * Sets the montant convertible.
	 * @param montantConvertible the new montant convertible
	 */
	public void setMontantConvertible(Long montantConvertible) {
		this.montantConvertible = montantConvertible;
	}

	/**
	 * Gets the solde converti.
	 * @return the solde converti
	 */
	public Long getSoldeConverti() {
		return soldeConverti;
	}

	/**
	 * Sets the solde converti.
	 * @param soldeConverti the new solde converti
	 */
	public void setSoldeConverti(Long soldeConverti) {
		this.soldeConverti = soldeConverti;
	}

	/**
	 * Gets the solde purge.
	 * @return the solde purge
	 */
	public Integer getSoldePurge() {
		return soldePurge;
	}

	/**
	 * Sets the solde purge.
	 * @param soldePurge the new solde purge
	 */
	public void setSoldePurge(Integer soldePurge) {
		this.soldePurge = soldePurge;
	}

	/**
	 * Gets the date transaction.
	 * @return the date transaction
	 */
	public Date getDateTransaction() {
		return dateTransaction;
	}

	/**
	 * Sets the date transaction.
	 * @param dateTransaction the new date transaction
	 */
	public void setDateTransaction(Date dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	/**
	 * Gets the utilisateur.
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * @param utilisateur the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Gets the ref compensation.
	 * @return the ref compensation
	 */
	public Compensation getRefCompensation() {
		return refCompensation;
	}

	/**
	 * Sets the ref compensation.
	 * @param refCompensation the new ref compensation
	 */
	public void setRefCompensation(Compensation refCompensation) {
		this.refCompensation = refCompensation;
	}


}
