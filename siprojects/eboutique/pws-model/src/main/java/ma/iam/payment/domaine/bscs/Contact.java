package ma.iam.payment.domaine.bscs;

import java.io.Serializable;

/**
 * <p>
 * <ul>
 * <li>Schema : BSCS</li>
 * <li>Table : CCONTACT_ALL</li>
 * </ul>
 * </p>
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class Contact implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 334268549713061422L;

	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_ID_TITILE. */
	public static final String LABEL_ID_TITILE = "idTitle";

	/** The Constant LABEL_NOM. */
	public static final String LABEL_NOM = "nom";

	/** The Constant LABEL_NOMS. */
	public static final String LABEL_NOMS = "nomS";

	/** The Constant LABEL_PRENOM. */
	public static final String LABEL_PRENOM = "prenom";

	/** The Constant LABEL_ADRESSE1. */
	public static final String LABEL_ADRESSE1 = "adresse1";

	/** The Constant LABEL_ADRESSE2. */
	public static final String LABEL_ADRESSE2 = "adresse2";

	/**
	 * <p>
	 * <ul>
	 * <li>Clé Concatainée : CUSTOMER_ID && CCSEQ</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private ContactId id;

	/** The numero ordre. */
	private Long numeroOrdre;

	/** The id client. */
	private Long idClient;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CCTITLE</li>
	 * <li>Description : Identifiant du titre</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long idTitle;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CCLNAME</li>
	 * <li>Description : Nom du client</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String nom;

	/** The nom s. */
	private String nomS;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CCFNAME</li>
	 * <li>Description : Prénom du client</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String prenom;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : le libelle du titre dans la table title</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String titre;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CCLINE2 ||' '|| CCLINE3</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String adresse1;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CCLINE4 ||' '|| CCLINE5</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String adresse2;


	/** The ville. */
	private String ville;

	/** The code postal. */
	private String codePostal;

	/** The adresse3. */
	private String adresse3;

	private String ccbill;

	/**
	 * The getter method for the field ccbill.
	 * @return the ccbill.
	 */
	public String getCcbill() {
		return ccbill;
	}

	/**
	 * The setter method for the field ccbill.
	 * @param ccbill the ccbill to set.
	 */
	public void setCcbill(String ccbill) {
		this.ccbill = ccbill;
	}

	/**
	 * Gets the ville.
	 * 
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * Sets the ville.
	 * 
	 * @param ville the new ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * Gets the code postal.
	 * 
	 * @return the code postal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * Sets the code postal.
	 * 
	 * @param codePostal the new code postal
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Gets the adresse3.
	 * 
	 * @return the adresse3
	 */
	public String getAdresse3() {
		return adresse3;
	}

	/**
	 * Sets the adresse3.
	 * 
	 * @param adresse3 the new adresse3
	 */
	public void setAdresse3(String adresse3) {
		this.adresse3 = adresse3;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public ContactId getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(ContactId id) {
		this.id = id;
	}

	/**
	 * Gets the adresse1.
	 * 
	 * @return the adresse1
	 */
	public String getAdresse1() {
		return adresse1;
	}

	/**
	 * Sets the adresse1.
	 * 
	 * @param adresse1 the new adresse1
	 */
	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	/**
	 * Gets the adresse2.
	 * 
	 * @return the adresse2
	 */
	public String getAdresse2() {
		return adresse2;
	}

	/**
	 * Sets the adresse2.
	 * 
	 * @param adresse2 the new adresse2
	 */
	public void setAdresse2(String adresse2) {
		this.adresse2 = adresse2;
	}

	/**
	 * Gets the nom.
	 * 
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 * 
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets the prenom.
	 * 
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Sets the prenom.
	 * 
	 * @param prenom the new prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Gets the titre.
	 * 
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Sets the titre.
	 * 
	 * @param titre the new titre
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * Gets the id title.
	 * 
	 * @return the id title
	 */
	public Long getIdTitle() {
		return idTitle;
	}

	/**
	 * Sets the id title.
	 * 
	 * @param idTitle the new id title
	 */
	public void setIdTitle(Long idTitle) {
		this.idTitle = idTitle;
	}

	/**
	 * Gets the id client.
	 * 
	 * @return the id client
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * Sets the id client.
	 * 
	 * @param idClient the new id client
	 */
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	/**
	 * Gets the numero ordre.
	 * 
	 * @return the numero ordre
	 */
	public Long getNumeroOrdre() {
		return numeroOrdre;
	}

	/**
	 * Sets the numero ordre.
	 * 
	 * @param numeroOrdre the new numero ordre
	 */
	public void setNumeroOrdre(Long numeroOrdre) {
		this.numeroOrdre = numeroOrdre;
	}

	/**
	 * Gets the nom s.
	 * 
	 * @return the nom s
	 */
	public String getNomS() {
		return nomS;
	}

	/**
	 * Sets the nom s.
	 * 
	 * @param nomS the new nom s
	 */
	public void setNomS(String nomS) {
		this.nomS = nomS;
	}

}
