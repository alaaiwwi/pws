package ma.iam.fidelio.domaine.data.bscs;

import ma.iam.fidelio.domaine.data.commun.BaseObject;


public class FidelioContrat extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8784012975762290969L;

	/** The ref client. */
	private FidelioClient refClient;
	
	/** The ref plan tarifaire. */
	private FidelioPlanTarifaire refPlanTarifaire;

	/**
	 * Gets the ref client.
	 * @return the ref client
	 */
	public FidelioClient getRefClient() {
		return refClient;
	}

	/**
	 * The getter method for the field refPlanTarifaire.
	 * @return the refPlanTarifaire.
	 */
	public FidelioPlanTarifaire getRefPlanTarifaire() {
		return refPlanTarifaire;
	}

	/**
	 * The setter method for the field refPlanTarifaire.
	 * @param refPlanTarifaire the refPlanTarifaire to set.
	 */
	public void setRefPlanTarifaire(FidelioPlanTarifaire refPlanTarifaire) {
		this.refPlanTarifaire = refPlanTarifaire;
	}

	/**
	 * Sets the ref client.
	 * @param refClient the new ref client
	 */
	public void setRefClient(FidelioClient refClient) {
		this.refClient = refClient;
	}
}
