package ma.iam.fidelio.domaine.data.bscs;

import java.util.Collection;
import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class FidelioClient extends BaseObject {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 146587465467489765L;

	/** The Constant CODE. */
	public static final String CODE = "code";

	public static final String COST_CENTER_ID = "costCenter";

	/** The Constant REF_CONTRAT. */
	public static final String REF_CONTRAT = "refContact";

	/** The code. */
	private String code;

	/** The cost center. */
	private Integer costCenter;

	/** The glod. */
	private Integer gold;

	/** The date activation. */
	private Date dateActivation;

	/** The statut. */
	private String statut;

	/** The nombre fils. */
	private Integer nombreFils;

	/** The ref categorie client. */
	private FidelioPriceGroup priceGroup;
	
	/** The ref contrats. */
	private Collection<FidelioContrat> refContrats;

	/**
	 * Gets the code.
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the cost center.
	 * @return the cost center
	 */
	public Integer getCostCenter() {
		return costCenter;
	}

	/**
	 * Sets the cost center.
	 * @param costCenter the new cost center
	 */
	public void setCostCenter(Integer costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * Gets the date activation.
	 * @return the date activation
	 */
	public Date getDateActivation() {
		return dateActivation;
	}

	/**
	 * Sets the date activation.
	 * @param dateActivation the new date activation
	 */
	public void setDateActivation(Date dateActivation) {
		this.dateActivation = dateActivation;
	}

	/**
	 * Gets the statut.
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * Sets the statut.
	 * @param statut the new statut
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * Gets the PriceGroup.
	 * @return the ref categorie client
	 */
	public FidelioPriceGroup getPriceGroup() {
		return priceGroup;
	}

	/**
	 * Sets the PriceGroup.
	 * @param priceGroup the new ref categorie client
	 */
	public void setPriceGroup(FidelioPriceGroup priceGroup) {
		this.priceGroup = priceGroup;
	}

	/**
	 * @param refContrats the refContrats to set
	 */
	public void setRefContrats(Collection<FidelioContrat> refContrats) {
		this.refContrats = refContrats;
	}

	/**
	 * @return the refContrats
	 */
	public Collection<FidelioContrat> getRefContrats() {
		return refContrats;
	}


}
