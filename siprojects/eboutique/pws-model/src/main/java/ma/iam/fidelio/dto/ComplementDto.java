package ma.iam.fidelio.dto;

import java.io.Serializable;

/**
 * 
 * @author mei
 *
 */
public class ComplementDto implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -545986067438781423L;

	/** Nombre De Point Acquis. */
	private Long pointsAcquis;

	/** Nombre De Point Encours. */
	private Long pointsEncours;

	/** Le complement d'argent. */
	private Double complement;
	
	private int duree;
	
	private Long prixPtEffectif;
	private Integer bonus;
	private Long prixEnPoints;

	/** Constructeur */
	public ComplementDto() {
	}

	/**
	 * @return Retourner Le Nombre De Points Acquis.
	 */
	public Long getPointsAcquis() {
		return pointsAcquis;
	}

	/**
	 * @param pointsAcquis
	 *            Modifier Le Nombre De Points Acquis.
	 */
	public void setPointsAcquis(Long pointsAcquis) {
		this.pointsAcquis = pointsAcquis;
	}

	/**
	 * @return Retourner Le Nombre De Points Encours.
	 */
	public Long getPointsEncours() {
		return pointsEncours;
	}

	/**
	 * @param pointsEncours
	 *            Modifier Le Nombre De Points Encours.
	 */
	public void setPointsEncours(Long pointsEncours) {
		this.pointsEncours = pointsEncours;
	}

	/**
	 * @return Retourner Le Complement d'argent.
	 */
	public Double getComplement() {
		return complement;
	}

	/**
	 * @param complement
	 *            Modifier Le Complement d'argent.
	 */
	public void setComplement(Double complement) {
		this.complement = complement;
	}

	/**
	 * The getter method for the field prixPtEffectif.
	 * @return the prixPtEffectif.
	 */
	public Long getPrixPtEffectif() {
		return prixPtEffectif;
	}

	/**
	 * The setter method for the field prixPtEffectif.
	 * @param prixPtEffectif the prixPtEffectif to set.
	 */
	public void setPrixPtEffectif(Long prixPtEffectif) {
		this.prixPtEffectif = prixPtEffectif;
	}

	/**
	 * The getter method for the field bonus.
	 * @return the bonus.
	 */
	public Integer getBonus() {
		return bonus;
	}

	/**
	 * The setter method for the field bonus.
	 * @param bonus the bonus to set.
	 */
	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	/**
	 * The getter method for the field prixEnPoints.
	 * @return the prixEnPoints.
	 */
	public Long getPrixEnPoints() {
		return prixEnPoints;
	}

	/**
	 * The setter method for the field prixEnPoints.
	 * @param prixEnPoints the prixEnPoints to set.
	 */
	public void setPrixEnPoints(Long prixEnPoints) {
		this.prixEnPoints = prixEnPoints;
	}

	/**
	 * The getter method for the field duree.
	 * @return the duree.
	 */
	public int getDuree() {
		return duree;
	}

	/**
	 * The setter method for the field duree.
	 * @param duree the duree to set.
	 */
	public void setDuree(int duree) {
		this.duree = duree;
	}

	
}
