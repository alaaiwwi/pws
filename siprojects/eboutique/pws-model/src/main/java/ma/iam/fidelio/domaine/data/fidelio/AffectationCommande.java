package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;
import ma.iam.fidelio.domaine.data.parametrage.Agence;

public class AffectationCommande extends BaseObject {

	/** La date d'affectation. **/
	private Date dateAffectation;

	/** Le commentaire. **/
	private String commentaire;

	/** La référence Commande. **/
	private Commande refCommande;

	/** La référence Agence. **/
	private Agence refAgence;

	/**
	 * Retourner l'attribut dateAffectation.
	 * @return : dateAffectation
	 */
	public Date getDateAffectation() {
		return dateAffectation;
	}

	/**
	 * Modifier l'attribut dateAffectation.
	 * @param : dateAffectation
	 */
	public void setDateAffectation(Date dateAffectation) {
		this.dateAffectation = dateAffectation;
	}

	/**
	 * Retourner l'attribut commentaire.
	 * @return : commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * Modifier l'attribut commentaire.
	 * @param : commentaire
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * Retourner la référence Commande.
	 * @return : refCommande
	 */
	public Commande getRefCommande() {
		return refCommande;
	}

	/**
	 * Modifier la référence Commande.
	 * @param : refCommande
	 */
	public void setRefCommande(Commande refCcommande) {
		this.refCommande = refCcommande;
	}

	/**
	 * Retourner la référence Agence.
	 * @return : refAgence
	 */
	public Agence getRefAgence() {
		return refAgence;
	}

	/**
	 * Modifier la référence Agence.
	 * @param : refAgence
	 */
	public void setRefAgence(Agence refAgence) {
		this.refAgence = refAgence;
	}

}
