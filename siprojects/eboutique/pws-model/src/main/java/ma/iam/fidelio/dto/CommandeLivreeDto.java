package ma.iam.fidelio.dto;

import java.util.Date;

/**
 * @author A175029
 *
 */
public class CommandeLivreeDto {

	private String numCommandeEB;
	private Date dateLivraison;
	
	public String getNumCommandeEB() {
		return numCommandeEB;
	}
	
	public void setNumCommandeEB(String numCommandeEB) {
		this.numCommandeEB = numCommandeEB;
	}
	
	public Date getDateLivraison() {
		return dateLivraison;
	}
	
	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	public CommandeLivreeDto(String numCommandeEB, Date dateLivraison) {
		super();
		this.numCommandeEB = numCommandeEB;
		this.dateLivraison = dateLivraison;
	}
	
	public CommandeLivreeDto() {
		super();
	}
	
}
