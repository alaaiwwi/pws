package ma.iam.fidelio.domaine.data.bscs;

import ma.iam.fidelio.domaine.data.commun.BaseObject;


public class FidelioPriceGroup extends BaseObject {

	/** The prg name. */
	private String prgName;

	/**
	 * Gets the prg name.
	 * @return the prg name
	 */
	public String getPrgName() {
		return prgName;
	}

	/**
	 * Sets the prg name.
	 * @param prgName the new prg name
	 */
	public void setPrgName(String prgName) {
		this.prgName = prgName;
	}

}
