package ma.iam.fidelio.services.catalogue;

import java.util.Collection;
import java.util.List;

import ma.iam.fidelio.dto.ArticleDto;
import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

/**
 * Service de recherche du catalogue
 * <br/><br/>
 * 
 * @author mei
 *
 */
public interface CatalogueService {
	
	/**
	 * Récupération du catalogue des produits disponible sur le SI Fidelio 
	 * @return
	 * @throws TechnicalException
	 */
	Collection<ArticleDto> getCatalogue() throws TechnicalException;

	/**
	 * @param codeClient
	 * @param numAppel
	 * @param idArticle
	 * @return
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	List<ComplementDto> detailComplement(String numAppel,
			Integer idArticle) throws FunctionalException, TechnicalException;
}
