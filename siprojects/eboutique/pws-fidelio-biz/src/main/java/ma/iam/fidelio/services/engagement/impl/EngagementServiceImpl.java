package ma.iam.fidelio.services.engagement.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.client.ClientDao;
import ma.iam.fidelio.dao.compte.CompteClientDao;
import ma.iam.fidelio.dao.engagement.EngagementDao;
import ma.iam.fidelio.dao.parametrage.RechercheParametreDao;
import ma.iam.fidelio.domaine.data.bscs.FidelioClient;
import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.fidelio.domaine.data.fidelio.CompteClient;
import ma.iam.fidelio.domaine.data.fidelio.Engagement;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.ConversionMonsuelle;
import ma.iam.fidelio.domaine.data.parametrage.DelaiPremiereConversion;
import ma.iam.fidelio.domaine.data.parametrage.ModeEngagement;
import ma.iam.fidelio.domaine.data.parametrage.SeuilImpaye;
import ma.iam.fidelio.domaine.data.parametrage.SousCategorie;
import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.fidelio.services.engagement.EngagementService;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.util.Utils;
import ma.iam.security.SecuredObject;

/**
 * 
 * @author A177417
 *
 */

public class EngagementServiceImpl implements EngagementService {

	private CompteClientDao compteClientDao;

	private RechercheParametreDao rechercheParametreDao;

	private ClientDao clientDao;

	private EngagementDao engagementDao;

	/**
	 * The setter method for the field engagementDao.
	 * 
	 * @param engagementDao
	 *            the engagementDao to set.
	 */
	public void setEngagementDao(EngagementDao engagementDao) {
		this.engagementDao = engagementDao;
	}

	/**
	 * The setter method for the field clientDao.
	 * 
	 * @param clientDao
	 *            the clientDao to set.
	 */
	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

	/**
	 * @param compteClientDao
	 */
	public void setCompteClientDao(CompteClientDao compteClientDao) {
		this.compteClientDao = compteClientDao;
	}

	/**
	 * @param rechercheParametreDao
	 */
	public void setRechercheParametreDao(
			RechercheParametreDao rechercheParametreDao) {
		this.rechercheParametreDao = rechercheParametreDao;
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.fidelio.dao.service.engagement.EngagementService
	 * #verifierDisponiblite(java.lang.Integer, java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer).
	 * 
	 * @param articleId
	 * @param qualiteId
	 * @param sousCategorieId
	 * @param planTarifaireId
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public void verifierDisponiblite(Integer articleId, Integer qualiteId,
			Integer sousCategorieId, Integer planTarifaireId)
			throws TechnicalException, FunctionalException {
		// Vérifier si l'article est disponible pour le client
		boolean isExclu = rechercheParametreDao.isExclusion(articleId,
				qualiteId, sousCategorieId, planTarifaireId);
		if (isExclu) {
			throw new FunctionalException(Constantes.ERREUR_CODE_09,
					Constantes.ERREUR_ARTICLE_NON_DISPO_CLIENT);
		}
	}

	/**
	 * 
	 */
	public void verifierCompteClient(Integer clientId, Long contratId,
			Date dateCretaionCompte, Integer priceGroupId,
			Integer categorieClientId) throws TechnicalException,
			FunctionalException {

		// Vérifier si le contrat à des commandes en instance
		Integer nombreCommandeEnInstance = compteClientDao
				.getNombreCommandeEnInstance(contratId);
		if (nombreCommandeEnInstance < 0) {
			throw new FunctionalException(Constantes.ERREUR_CODE_02,
					Constantes.CONTRAT_INCOHERENT_DEMANDES_CONVERSION);
		}
		if (nombreCommandeEnInstance >= 1) {
			throw new FunctionalException(Constantes.ERREUR_CODE_27,
					Constantes.CONTRAT_COMMANDE_EN_INSTANCE);
		}

		/* Récupérer le nombre de conversion autorisé par mois */
		ConversionMonsuelle conversion = rechercheParametreDao
				.getConversionMonsuelle(categorieClientId);
		if (conversion == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_12,
					Constantes.NOMBRE_CONVERSION_MOIS_NON_PARAMETRE);
		}

		// Verifier le nombre de commandes livrees dans le mois en cours /
		int nombreCommandesLivrees = compteClientDao
				.getNombreCommandesLivrees(contratId);
		if (nombreCommandesLivrees >= conversion.getNbrConvMois()) {
			throw new FunctionalException(
					Constantes.ERREUR_CODE_14,
					Constantes.NB_COMMANDE_LIVRE_SUPERIEUR_NB_CONVERSION_AUTORISE);
		}

		/* Vérifier le délai de la premiere conversion */
		DelaiPremiereConversion premiereConversion = rechercheParametreDao
				.getDelaiPremiereConversion(categorieClientId);
		if (premiereConversion == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_01,
					Constantes.ERREUR_PARAM_DELAI_ADHESION_INEXISTANT);
		}
		Calendar date1ereConversion = Calendar.getInstance();
		date1ereConversion.add(Calendar.MONTH,
				(-premiereConversion.getDelai1ereConv()));
		if (!date1ereConversion.getTime().after(dateCretaionCompte)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_13,
					Constantes.DELAI_CONVERSION_NON_ECOULE);
		}

		/* Vérifier si le Seuil Maximal Impaye est atteint */
		SeuilImpaye impaye = rechercheParametreDao
				.getSeuilImpaye(categorieClientId);
		if (impaye == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_22,
					Constantes.PARAMETRE_SEUIL_MAX_IMPAYES_PAS_CONFIGURE);
		}
		if (!Arrays.asList(Constantes.CLIENTS_EXCLUS_IMPAYES).contains(
				priceGroupId)) {
			float totalImpaye = clientDao.getTotalImpaye(clientId);
			if (totalImpaye > impaye.getSeuilImpayes()) {
				throw new FunctionalException(Constantes.ERREUR_CODE_10,
						Constantes.SEUIL_MAX_IMPAYES_ATTEINT);
			}
		}
	}

	/**
	 * 
	 */
	public void verifierEligibiliteClient(String numAppel)
			throws TechnicalException, FunctionalException {

		if (Utils.isEmpty(numAppel)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_21,
					Constantes.ERREUR_NUM_APPEL_OBLIGATOIRE);
		}

		FidelioContrat contrat = this.verifierContrat(numAppel);

		Long contratId = contrat.getId().longValue();

		/* Récuperation du client */
		FidelioClient client = contrat.getRefClient();

		Integer priceGroupId = client.getPriceGroup().getId();

		/* Recuperer : categorieClientId */
		SousCategorie sousCategorie = compteClientDao
				.getSousCategorieByPriceGroupe(priceGroupId);
		Integer categorieClientId = sousCategorie.getRefCategorieClient()
				.getId();

		/* Récupération du compte client */
		CompteClient compteClient = compteClientDao
				.getCompteClientByCustomerId(client.getId());
		if (compteClient == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_34,
					Constantes.ERREUR_COMPTE_FIDELIO_INEXISTANT);
		}

		this.verifierCompteClient(client.getId(), contratId,
				compteClient.getDateCreation(), priceGroupId, categorieClientId);
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.fidelio.dao.service.engagement.EngagementService
	 * #verifierContrat(java.lang.String).
	 * 
	 * @param numAppel
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public FidelioContrat verifierContrat(String numAppel)
			throws TechnicalException, FunctionalException {

		/* Récuperation du contratId */
		FidelioContrat contrat = clientDao.getContratByND(numAppel);

		/* Vérifier le numéro d'appel */
		if (contrat == null) {
			List<Long> list = clientDao.getListContratByNumAppel(numAppel);
			if (list == null || list.isEmpty()) {
				// Numéro d'appel inexistant
				throw new FunctionalException(Constantes.ERREUR_CODE_04,
						Constantes.ERREUR_NUM_APPEL_INEXISTANT);
			} else {
				// La ligne n'est pas active sur le compte à points du client
				throw new FunctionalException(Constantes.ERREUR_CODE_06,
						Constantes.ERREUR_LIGNE_INACTIVE);
			}
		}
		/* Récuperation du client */
		FidelioClient client = contrat.getRefClient();

		// Vérifier si le compte à points est exclu
		if (!client.getStatut().toUpperCase()
				.equals(Constantes.CLIENT_STATUT_ACTIF)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_05,
					Constantes.ERREUR_COMPTE_POINTS_EXCLU);
		}

		// Vérifier si le plan tarifaire du client est compatible avec l’offre
		// avec engagement
		if (Arrays.asList(Constantes.PLAN_TARIFAIRE_EXCLUS).contains(
				contrat.getRefPlanTarifaire().getCode())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_07,
					Constantes.ERREUR_PLAN_TARIFAIRE_INCOMPATIBLE_ENGAGEMENT);
		}

		// Vérifier si il existe un engagement echu
		this.verifierEngagementEchu(contrat.getId().longValue());

		return contrat;
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.fidelio.dao.service.engagement.EngagementService
	 * #calculPoints(java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.String, ma.iam.fidelio.domaine.data.bscs.Contrat,
	 * ma.iam.fidelio.domaine.data.fidelio.CompteClient).
	 * 
	 * @param modeEngagementId
	 * @param articleId
	 * @param categorieClientId
	 * @param numAppel
	 * @param contrat
	 * @param compteClient
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public ComplementDto calculPoints(Integer modeEngagementId,
			Integer articleId, Integer categorieClientId, String numAppel,
			FidelioContrat contrat, CompteClient compteClient)
			throws TechnicalException, FunctionalException {
		try {
			/* Récuperation du client */
			FidelioClient client = contrat.getRefClient();

			/* Récupérer le plan tarifaire */
			Integer planTarifaireId = contrat.getRefPlanTarifaire().getId()
					.intValue();

			/* Récupérer le catalogue d'engagement */
			Engagement engagement = engagementDao.getLigneCatalogue(articleId,
					planTarifaireId, categorieClientId, modeEngagementId,
					compteClient.getRefQualite().getId());

			if (engagement == null) {
				return null;
			}

			/* Récupérer le bonus d'engagement */
			Integer bonus = rechercheParametreDao.getBonusEngagement(
					categorieClientId, modeEngagementId);

			/* Récupérer le prixEnPoints */
			Long prixEnPoints = (engagement.getPrixPoints() != null ? engagement
					.getPrixPoints() : 0);

			/* Calcul du prix effectif */
			Long prixPtEffectif = prixEnPoints - bonus;

			// calcul du seuil minimal pour l'engagement souhaité
			double seuilMinEngagement = rechercheParametreDao
					.getSeuilMinimalEngagement(
							engagement.getModeEngagementId(), categorieClientId);
			if (compteClient.getSoldeAcquis() < (seuilMinEngagement * prixPtEffectif)) {
				throw new FunctionalException(Constantes.ERREUR_CODE_08,
						Constantes.ERREUR_SEUIL_MIN_ENGAGEMENT_NON_ATTEINT,
						new Object[] { engagement.getDureeEnMois() });
			}

			double taux = rechercheParametreDao
					.getTauxConversion(categorieClientId);

			Long pointsAcquisUtilise = 0L;
			Long pointsEnCoursUtilise = 0L;
			Double complement = this.calculComplement(planTarifaireId, client,
					numAppel, engagement, compteClient.getSoldeAcquis(),
					compteClient.getSoldeEnCours(), prixPtEffectif, taux);
			if (compteClient.getSoldeAcquis() < prixPtEffectif) {
				pointsAcquisUtilise = compteClient.getSoldeAcquis();
				pointsEnCoursUtilise = this.getEncoursUtilise(
						compteClient.getSoldeAcquis(),
						compteClient.getSoldeEnCours(), prixPtEffectif, taux);
			} else {
				pointsAcquisUtilise = prixPtEffectif;
			}
			ComplementDto complementDto = new ComplementDto();
			complementDto.setBonus(bonus);
			complementDto.setPrixEnPoints(prixEnPoints);
			complementDto.setPrixPtEffectif(prixPtEffectif);
			complementDto.setDuree(engagement.getDureeEnMois());
			complementDto.setComplement(Utils.round(complement, 2));
			complementDto.setPointsAcquis(pointsAcquisUtilise);
			complementDto.setPointsEncours(pointsEnCoursUtilise);
			return complementDto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Vérifier si le contrat a un engagement échu
	 * 
	 * @param contratId
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	public void verifierEngagementEchu(Long contratId)
			throws FunctionalException, TechnicalException {

		/* Création de la date */
		Date dateDuJour = Calendar.getInstance().getTime();
		Date dateDebut = null;
		/* Traitement de récupération d'engagement et la conversion */
		ModeEngagement modeEngagementEnCours = null;
		String[] resultats = compteClientDao.getEngagementEnCours(contratId
				.longValue());

		String engagementBscs = null;
		String debutEngagementBscs = null;

		if (resultats != null && resultats.length > 0) {
			engagementBscs = resultats[0];
			debutEngagementBscs = resultats[1];
		}
		if (Utils.isEmpty(engagementBscs) || Utils.isEmpty(debutEngagementBscs)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_23,
					Constantes.ERREUR_ND_SANS_ENGAGEMENT_ECHU);
		}
		modeEngagementEnCours = rechercheParametreDao
				.getModeEngagementByBscsId(Integer.valueOf(engagementBscs));
		if (modeEngagementEnCours == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_16,
					Constantes.MODE_ENGAGEMENT_NON_PARAMETRE);
		}

		Calendar engCalendar = Calendar.getInstance();
		dateDebut = Utils.stringToDate(debutEngagementBscs,
				Constantes.ENGLISH_DATE_FORMAT);
		if (dateDebut == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_23,
					Constantes.ERREUR_ND_SANS_ENGAGEMENT_ECHU);
		}
		engCalendar.setTime(dateDebut);
		engCalendar.add(Calendar.MONTH, modeEngagementEnCours.getDureeEnMois());
		Date dateFin = new Date(engCalendar.getTimeInMillis());

		if (!dateDuJour.after(dateFin)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_23,
					Constantes.ERREUR_ND_SANS_ENGAGEMENT_ECHU);
		}

	}

	/**
	 * Calcul du complement
	 * 
	 * @param planTarifaireId
	 * @param client
	 * @param numAppel
	 * @param engagement
	 * @param soldeAquis
	 * @param soldeEnCours
	 * @param prixCatalogue
	 * @param taux
	 * @return
	 * @throws TechnicalException
	 */
	private Double calculComplement(Integer planTarifaireId,
			FidelioClient client, String numAppel, Engagement engagement,
			Long soldeAquis, Long soldeEnCours, Long prixCatalogue, double taux)
			throws TechnicalException {

		if (soldeAquis >= prixCatalogue) {
			return 0.0;
		}

		// si Regle de 75 est respectée alors return 0
		if (((soldeAquis + soldeEnCours) >= prixCatalogue)
				&& ((1 - taux) * prixCatalogue <= soldeAquis)) {
			return 0.0;
		}

		// CA = (PPC – B – SC) * PVP – RCA
		// (Si SC > PPC – B alors CA=0)
		// MRE : Montant de Rachat de l’Engagement
		// PPC : Prix en Point du Cadeau
		// SC : Solde Convertible
		// CA : Complément d’argent
		// PVP : Prix de Vente d’un Point
		// RCA : Remise sur complément d’argent
		// B : Bonus réengagement

		Double soldeConvertible = getSoldeConvertible(soldeAquis, soldeEnCours,
				prixCatalogue, taux);
		Double chiffreAffaire = (prixCatalogue - soldeConvertible);

		// Convertir le prix en point au prix en DH
		Double prixRachatPoint = rechercheParametreDao
				.getPrixRachatPointByModeEngagement(engagement
						.getModeEngagementId());
		chiffreAffaire = chiffreAffaire * prixRachatPoint;

		// CA = prix conquete + frais de mise en service
		Double prixConquete = getPrixConquete(planTarifaireId, engagement,
				client, numAppel);

		return Math.min(chiffreAffaire, prixConquete);

	}

	/**
	 * This method allows you to .</br></br> Created on Sprint #
	 * 
	 * @param soldeAcquis
	 * @param soldeEnCours
	 * @param prixPtEffectif
	 * @param taux
	 * @return
	 */
	private Long getEncoursUtilise(Long soldeAcquis, Long soldeEnCours,
			Long prixPtEffectif, double taux) {

		Long result = 0L;
		Long plafandEnCours = Double.valueOf(Math.floor(taux * prixPtEffectif))
				.longValue();

		if (soldeAcquis + soldeEnCours <= prixPtEffectif) {
			result = (soldeEnCours <= plafandEnCours) ? soldeEnCours : Math
					.min(plafandEnCours, (prixPtEffectif - soldeAcquis));
		} else {
			result = plafandEnCours + soldeAcquis <= prixPtEffectif ? plafandEnCours
					: prixPtEffectif - soldeAcquis;
		}

		return result;
	}

	/**
	 * @param soldeAquis
	 * @param soldeEncours
	 * @param prixTotal
	 * @param taux
	 * @return
	 */
	private Double getSoldeConvertible(Long soldeAquis, Long soldeEncours,
			Long prixTotal, Double taux) {
		Double result = 0d;
		if (prixTotal <= soldeAquis) {
			result = (prixTotal).doubleValue();
		}

		Double reste = (Double) (prixTotal * taux);
		if (soldeEncours >= reste) {
			result = (soldeAquis).doubleValue() + (reste).doubleValue();
		} else {
			result = (soldeAquis).doubleValue() + soldeEncours.doubleValue();
		}
		if (result > prixTotal) {
			return (prixTotal).doubleValue();
		}
		return result;
	}

	/**
	 * Created on Sprint #
	 * 
	 * @param planTarifaireId
	 * @param engagement
	 * @param client
	 * @param numAppel
	 * @return
	 * @throws TechnicalException
	 */
	private double getPrixConquete(Integer planTarifaireId,
			Engagement engagement, FidelioClient client, String numAppel)
			throws TechnicalException {

		Float result = 0f;

		Article produit = (Article) rechercheParametreDao
				.getArticleByCode(engagement.getArticleCode());

		// si la famille du produit est "SGE"
		if (produit.getRefModelePoste() != null
				&& produit.getRefModelePoste().getLibelle().trim()
						.equals(Constantes.MODEL_POST_SGE)) {
			List<String> listNumAplClient = clientDao.getAllNumAppels(client
					.getId());

			if (listNumAplClient.size() > 10) {
				result = 0f;
			} else {
				/* Récupération du mode d'engagement */
				ModeEngagement modeEngagement = rechercheParametreDao
						.getModeEngagementById(engagement.getModeEngagementId());
				result = rechercheParametreDao.getPrixPackXMois(
						planTarifaireId, engagement.getArticleCode(),
						modeEngagement.getDureeEnMois());
			}
		}
		// sinon
		else {
			/* Récupération du mode d'engagement */
			ModeEngagement modeEngagement = rechercheParametreDao
					.getModeEngagementById(engagement.getModeEngagementId());
			Float prixPack = rechercheParametreDao.getPrixPackXMois(
					planTarifaireId, engagement.getArticleCode(),
					modeEngagement.getDureeEnMois());

			// FC 4185 La remise privilege n est pas accordee dans le cas d un
			// engagement de 12 mois Fidelio
			if (modeEngagement.getDureeEnMois().equals(
					Constantes.MODE_ENGAGEMENT_FIDELIO_12M_DUREE)) {
				result = prixPack;
			} else {

				float seuilRemise = rechercheParametreDao
						.getPrixPackMinimalPourRemise(engagement
								.getCategorieClientId());

				result = prixPack
						- rechercheParametreDao.getRemisePackPrivilege(
								client.getCode(), numAppel, prixPack,
								seuilRemise, Constantes.REMISE_PRIVILEGE);
			}
		}

		return result;
	}

}