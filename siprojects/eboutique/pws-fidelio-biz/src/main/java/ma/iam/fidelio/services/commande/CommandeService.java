package ma.iam.fidelio.services.commande;

import java.util.Date;
import java.util.List;

import ma.iam.fidelio.dto.CommandeDto;
import ma.iam.fidelio.dto.CommandeLivreeDto;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface CommandeService {
	
	/**
	 * 
	 * @param commandeDto
	 * @return
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	public Date passerCommande(CommandeDto commandeDto) throws FunctionalException, TechnicalException;
	/**
	 * 
	 * @param numCommandeEB
	 * @param codeClient
	 * @param utilisateur
	 * @param motif
	 * @throws TechnicalException
	 * @throws FunctionalException 
	 */
	public String annulerCommande(String numCommandeEB, String codeClient, String utilisateur, String motif) throws TechnicalException, FunctionalException;
	
	
	/**
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public List<CommandeLivreeDto> rechercheCommandesLivrees(Date dateDebut, Date dateFin) throws TechnicalException, FunctionalException;
}
