package ma.iam.fidelio.dto.converter;

import ma.iam.fidelio.dto.CommandeDto;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.services.commande.rq.CommandeRQ;

public class CommandeDtoConverter {
	
	/**
	 * Fonction creerCommandeDto : Permet de Créer une CommandeDto 
	 * é partir d'un objet de type CommandeRQ.
	 * @param : commandeRq
	 * @return : CommandeDto 
	 */
	public static CommandeDto creerCommandeDto(CommandeRQ commandeRq) throws FunctionalException {
		
		
		CommandeDto commandeDto = new CommandeDto();
		commandeDto.setCodeAgence(commandeRq.getCodeAgence());
		commandeDto.setComplement(commandeRq.getComplement());
		
		commandeDto.setEngagementId(commandeRq.getIdEngagement());
		
		commandeDto.setIdArticle(commandeRq.getIdArticle());
		commandeDto.setNumCommande(commandeRq.getNumCommande());
		commandeDto.setNumeroAppel(commandeRq.getNumAppel());
		commandeDto.setSoldeAcquis(commandeRq.getPointsAcquis());
		commandeDto.setSoldeEncours(commandeRq.getPointsEncours());
		commandeDto.setUtilisateur(commandeRq.getUtilisateur());
		return commandeDto;
	}
}
