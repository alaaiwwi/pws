package ma.iam.fidelio.ws.engagement.impl;

import net.atos.ma.integration.jee.bypass.JDBCPing;
import ma.iam.common.Constantes;
import ma.iam.fidelio.services.engagement.EngagementService;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;
import ma.iam.security.SecuredObject;
import ma.iam.services.common.fault.TechnicalFault;
import ma.iam.services.engagement.rs.StatutClientRS;

/**
 * Web Service : Engagement.
 */
@SecuredObject
@javax.jws.WebService(serviceName = "EngagementService", portName = "EngagementServicePort", targetNamespace = "http://services.iam.ma/engagement/engagementService", endpointInterface = "ma.iam.services.engagement.EngagementService")
public class EngagementWSImpl implements
		ma.iam.services.engagement.EngagementService {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager
			.getLogger(EngagementWSImpl.class.getName());
	

	/** L'attribut engagementService. */
	private EngagementService engagementService;

	/**
	 * Modifier l'objet engagementService.
	 * 
	 * @param : engagementService
	 */
	public void setEngagementService(EngagementService engagementService) {
		this.engagementService = engagementService;
	}

	/**
	 * objet pour le ping sur la base bscs
	 */
	private JDBCPing pingBscs;
	/**
	 * objet pour le ping sur la base fidelio
	 */
	private JDBCPing pingFidelio;

	public void setPingBscs(JDBCPing pingBscs) {
		this.pingBscs = pingBscs;
	}

	public void setPingFidelio(JDBCPing pingFidelio) {
		this.pingFidelio = pingFidelio;
	}

	/**
	 * 
	 */
	public StatutClientRS verifierEligibiliteClient(String numeroAppel)
			throws TechnicalFault {
		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append("Vérifier l'eligibilité du client : ");
		loggingBuffer.append("\t Numero Appel : ").append(numeroAppel);
		LOGGER.info(loggingBuffer.toString());

		StatutClientRS response = new StatutClientRS();
		try {
			engagementService.verifierEligibiliteClient(numeroAppel);
			response.setResultat(1);
			LOGGER.info("Resultat = 1 : Client eligibile");
		} catch (FunctionalException e) {
			LOGGER.error("Resultat = 0 : " + e.getMessage());
			response.setResultat(0);
			response.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (TechnicalException e) {
			LOGGER.error(getClass(), "verifierEligibiliteClient", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}

		catch (Exception e) {
			LOGGER.error(getClass(), "verifierEligibiliteClient", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}
		return response;
	}

	@Override
	public String modeByPass() {
		boolean ret1 = pingBscs.check();
		boolean ret2 = pingFidelio.check();

		if (ret1 && ret2)
			return Constantes.BY_PASS_OK;
		return Constantes.BY_PASS_NOK;

	}

}