package ma.iam.fidelio.services.catalogue.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.common.Constantes;
import ma.iam.fidelio.common.ServiceMessageManager;
import ma.iam.fidelio.dao.compte.CompteClientDao;
import ma.iam.fidelio.dao.engagement.EngagementDao;
import ma.iam.fidelio.dao.parametrage.RechercheParametreDao;
import ma.iam.fidelio.domaine.data.bscs.FidelioClient;
import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.fidelio.domaine.data.bscs.FidelioProductSAP;
import ma.iam.fidelio.domaine.data.fidelio.CompteClient;
import ma.iam.fidelio.domaine.data.fidelio.Engagement;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.ModeEngagement;
import ma.iam.fidelio.domaine.data.parametrage.SousCategorie;
import ma.iam.fidelio.dto.ArticleDto;
import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.fidelio.dto.PrixEngagementDto;
import ma.iam.fidelio.dto.PrixStandardDto;
import ma.iam.fidelio.services.catalogue.CatalogueService;
import ma.iam.fidelio.services.engagement.EngagementService;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;
import ma.iam.mutualisation.fwk.common.util.Utils;

/**
 * Service de recherche du catalogue
 * <br/><br/>
 * 
 * @author Atos
 *
 */
public class CatalogueServiceImpl implements CatalogueService {
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.fidelio.dao.service.catalogue.CatalogueService#detailComplement(java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer).
	 * @param codeClient
	 * @param numAppel
	 * @param idArticle
	 * @param idModeEngagement
	 * @return
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	public List<ComplementDto> detailComplement(String numAppel,
			Integer idArticle) throws FunctionalException, TechnicalException {
		
		if (Utils.isEmpty(numAppel)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_21, Constantes.ERREUR_NUM_APPEL_OBLIGATOIRE);
		}
		if (idArticle == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_26, Constantes.ERREUR_ID_POSTE_OBLIGATOIRE);
		}
		
		Article article = rechercheParametreDao.getArticleById(idArticle);
		if (article == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_03, Constantes.ERREUR_ID_ARTICLE_INEXISTANT);
		}
		
		/* Vérification de l'état de suspension de l'article commandé */
		if(!article.isEnService()) {
			throw new FunctionalException(Constantes.ERREUR_CODE_11, Constantes.ERREUR_ARTICLE_SUSPENDU);
		}
		// L’offre sans terminale ne doit pas être traitée dans EB
		if (Constantes.CODE_POSTE_SANS_TERMINAL.equals(article.getCodeProduit())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_31, Constantes.ERREUR_OFFRE_SANS_TERMINAL);
		}
		
		// vérification du contrat client
		FidelioContrat contrat = engagementService.verifierContrat(numAppel);
		
		/* Récuperation du client */
		FidelioClient client = contrat.getRefClient();
		
		/* Récupération du compte client */
		CompteClient compteClient = compteClientDao.getCompteClientByCustomerId(client.getId());
		if (compteClient == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_34, Constantes.ERREUR_COMPTE_FIDELIO_INEXISTANT);
		}
		
		/* Recuperer : categorieClientId */
		SousCategorie sousCategorie = compteClientDao.getSousCategorieByPriceGroupe(client.getPriceGroup().getId());
		
		// Vérifier si l'article est disponible pour le client
		engagementService.verifierDisponiblite(idArticle, compteClient.getRefQualite().getId(),
				sousCategorie.getId(), contrat.getRefPlanTarifaire().getId().intValue());
		
		List<ComplementDto> listComplementDto = new ArrayList<ComplementDto>();
		
		try {
			// Engagement 24 Mois
			ModeEngagement modeEngagement = rechercheParametreDao.getModeEngagementByBscsId(Constantes.ID_ENGAGEMENT_BSCS_24_MOIS);
			if (modeEngagement == null) {
				throw new FunctionalException(Constantes.ERREUR_CODE_20, Constantes.ERREUR_MODE_ENGAGEMENT_INEXISTANT);
			}			
			ComplementDto complement24Dto =  engagementService.calculPoints(modeEngagement.getId(),
					idArticle, sousCategorie.getRefCategorieClient().getId(),
					numAppel, contrat, compteClient);
			if (complement24Dto == null) {
				throw new FunctionalException(Constantes.ERREUR_CODE_25,
						Constantes.ERREUR_ARTICLE_INDISPONIBLE_MODE_ENGAGEMENT,
						new Object[] {Constantes.MODE_ENGAGEMENT_FIDELIO_24M_DUREE});
			}
			listComplementDto.add(complement24Dto);
		} catch (FunctionalException e) {
			ServiceMessageManager.addErrorMessage(e.getMessageCode(), e.getMessage());
		}
		try {
			// Engagement 12 Mois
			ModeEngagement modeEngagement = rechercheParametreDao.getModeEngagementByBscsId(Constantes.ID_ENGAGEMENT_BSCS_12_MOIS);
			if (modeEngagement == null) {
				throw new FunctionalException(Constantes.ERREUR_CODE_20, Constantes.ERREUR_MODE_ENGAGEMENT_INEXISTANT);
			}
			ComplementDto complement12Dto =  engagementService.calculPoints(modeEngagement.getId(),
					idArticle, sousCategorie.getRefCategorieClient().getId(),
					numAppel, contrat, compteClient);
			if (complement12Dto == null) {
				throw new FunctionalException(Constantes.ERREUR_CODE_25,
						Constantes.ERREUR_ARTICLE_INDISPONIBLE_MODE_ENGAGEMENT,
						new Object[] {Constantes.MODE_ENGAGEMENT_FIDELIO_12M_DUREE});
			}
			listComplementDto.add(complement12Dto);
		} catch (FunctionalException e) {
			ServiceMessageManager.addErrorMessage(e.getMessageCode(), e.getMessage());
		}
		return listComplementDto;
	}
	
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.fidelio.dao.service.CatalogueService#getCatalogue().
	 * @return
	 * @throws TechnicalException
	 */
	public Collection<ArticleDto> getCatalogue() throws TechnicalException {
		
		Integer categorieClientId = compteClientDao.getCategorieClientByCode(Constantes.CATEGORIE_CLIENT_GRAND_PUBLIC).getId();
		
		List<Engagement> catalogues = engagementDao.getListCatalogue(categorieClientId);
		
		ArticleDto articleDto = null;
		PrixEngagementDto prixEngagementDto = null;
		PrixStandardDto prixStandardDto = null;
		Map<Integer, ArticleDto> mapArticles = new HashMap<Integer, ArticleDto>();
		
		Map<String, Float> mapPrixPoste = new HashMap<String, Float>();
		for (Engagement engagement : catalogues) {
			if (!mapArticles.containsKey(engagement.getArticleId())) {
				articleDto = new ArticleDto();
				articleDto.setIdPoste(engagement.getArticleId());
				if (Utils.isEmpty(engagement.getArticleCode())) {
					ServiceMessageManager.addErrorMessage(Constantes.ERREUR_CODE_05,
							MessageUtils.getMessage(Constantes.ERREUR_CODE_PRODUIT_INEXISTANT, new Object[] {engagement.getArticleId()}));
				}
				if (Utils.isEmpty(engagement.getArticleLibelle())) {
					ServiceMessageManager.addErrorMessage(Constantes.ERREUR_CODE_04,
							MessageUtils.getMessage(Constantes.ERREUR_LIBELLE_POSTE_INEXISTANT, new Object[] {engagement.getArticleId()}));
				}
				if (engagement.getArticleEnService() == null) {
					ServiceMessageManager.addErrorMessage(Constantes.ERREUR_CODE_11,
							MessageUtils.getMessage(Constantes.ERREUR_ETAT_SUPENSION_INEXISTANT, new Object[] {engagement.getArticleId()}));
				}
				articleDto.setCodeArticle(engagement.getArticleCode());
				
				/* Récupération du code SAP. */
				FidelioProductSAP productSAP = engagementDao.getProductSAPByCodeArticle(engagement.getArticleCode());
				
				if(productSAP != null) {
					articleDto.setCodeArticleSAP(productSAP.getCodeSAPProduct());
				} else {
					articleDto.setCodeArticleSAP("");
				}
				
				articleDto.setLibelle(engagement.getArticleLibelle());
				articleDto.setEnService(engagement.getArticleEnService());
				articleDto.setPrixStandardDto(new PrixStandardDto());
				mapArticles.put(engagement.getArticleId(), articleDto);
				mapPrixPoste.putAll(rechercheParametreDao.getPrixPoste(engagement.getArticleCode()));
			}
			articleDto = mapArticles.get(engagement.getArticleId());
			
			if (Constantes.TYPE_CATALOGUE_ENGAGEMENT.equals(engagement.getType())) {
				prixEngagementDto = new PrixEngagementDto();
				if (engagement.getPrixPoints() == null) {
					ServiceMessageManager.addErrorMessage(Constantes.ERREUR_CODE_08,
							MessageUtils.getMessage(Constantes.ERREUR_POINTS_ENGAGEMENT_INEXISTANT,
									new Object[] {engagement.getDureeEnMois(), engagement.getArticleId()}));
				} else {
					prixEngagementDto.setPrixPointEngagement(engagement.getPrixPoints().intValue());
				}
				
				prixEngagementDto.setPrixEngagement(mapPrixPoste.get(engagement.getDureeEnMois()+"MPA-"+engagement.getArticleCode()));
				prixEngagementDto.setDureeEnMois(engagement.getDureeEnMois());
				articleDto.getListPrixEngagementDto().add(prixEngagementDto);
			} else {
				if (engagement.getPrixPoints() == null) {
					ServiceMessageManager.addErrorMessage(Constantes.ERREUR_CODE_07,
							MessageUtils.getMessage(Constantes.ERREUR_POINTS_POSTE_NU_INEXISTANT, new Object[] {engagement.getArticleId()}));
				} else {
					prixStandardDto = articleDto.getPrixStandardDto();
					if (prixStandardDto.getPointsNu() == null 
							|| prixStandardDto.getPointsNu() < engagement.getPrixPoints().intValue()) {
						prixStandardDto.setPointsNu(engagement.getPrixPoints().intValue());
						prixStandardDto.setPrixNu(mapPrixPoste.get(engagement.getArticleCode()));
					}
				}
			}
		}
		return mapArticles.values();
	}
	
	private RechercheParametreDao rechercheParametreDao = null;
	
	private CompteClientDao compteClientDao;
	
	private EngagementService engagementService;
	
	private EngagementDao engagementDao;
	
	/**
	 * The setter method for the field engagementDao.
	 * @param engagementDao the engagementDao to set.
	 */
	public void setEngagementDao(EngagementDao engagementDao) {
		this.engagementDao = engagementDao;
	}
	
	/**
	 * The setter method for the field compteClientDao.
	 * @param compteClientDao the compteClientDao to set.
	 */
	public void setCompteClientDao(CompteClientDao compteClientDao) {
		this.compteClientDao = compteClientDao;
	}
	
	/**
	 * @param engagementService
	 */
	public void setEngagementService(EngagementService engagementService) {
		this.engagementService = engagementService;
	}
	
	/**
	 * The setter method for the field rechercheParametreDao.
	 * @param rechercheParametreDao the rechercheParametreDao to set.
	 */
	public void setRechercheParametreDao(RechercheParametreDao rechercheParametreDao) {
		this.rechercheParametreDao = rechercheParametreDao;
	}
}
