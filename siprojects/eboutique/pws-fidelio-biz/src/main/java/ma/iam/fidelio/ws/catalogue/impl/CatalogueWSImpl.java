package ma.iam.fidelio.ws.catalogue.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import ma.iam.common.Constantes;
import ma.iam.fidelio.common.ServiceMessageManager;
import ma.iam.fidelio.dto.ArticleDto;
import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.fidelio.dto.MessageDto;
import ma.iam.fidelio.dto.converter.CatalogueDtoConverter;
import ma.iam.fidelio.dto.converter.ComplementDtoConverter;
import ma.iam.fidelio.services.catalogue.CatalogueService;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;
import ma.iam.security.SecuredObject;
import ma.iam.services.catalogue.rs.CatalogueRS;
import ma.iam.services.catalogue.rs.ComplementRS;
import ma.iam.services.common.fault.TechnicalFault;
import net.atos.ma.integration.jee.bypass.JDBCPing;

/**
 * Web service de recherche du catalogue
 */
@SecuredObject
@javax.jws.WebService(serviceName = "CatalogueService", portName = "CatalogueServicePort", targetNamespace = "http://services.iam.ma/catalogue/catalogueService", endpointInterface = "ma.iam.services.catalogue.CatalogueService")
public class CatalogueWSImpl implements
		ma.iam.services.catalogue.CatalogueService {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager
			.getLogger(CatalogueWSImpl.class.getName());


	/** L'attribut catalogueService. */
	CatalogueService catalogueService = null;

	/**
	 * Retourner l'objet catalogueService.
	 * 
	 * @return : CatalogueService
	 */
	public CatalogueService getCatalogueService() {
		return catalogueService;
	}

	/**
	 * Modifier l'objet catalogueService.
	 * 
	 * @param : catalogueService
	 */
	public void setCatalogueService(CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	/**
	 * objet pour le ping sur la base bscs
	 */
	private JDBCPing pingBscs;
	/**
	 * objet pour le ping sur la base fidelio
	 */
	private JDBCPing pingFidelio;

	public void setPingBscs(JDBCPing pingBscs) {
		this.pingBscs = pingBscs;
	}

	public void setPingFidelio(JDBCPing pingFidelio) {
		this.pingFidelio = pingFidelio;
	}
	
	

	/**
	 * Méthode consulterCatalogue : Permet de récupérer un objet de type
	 * CatalogueRS à partir d'un priceGroup.
	 * 
	 * @param : priceGroup
	 * @return : CatalogueRS
	 * @throws : TechnicalFault
	 */
	
	public CatalogueRS consulterCatalogue() throws TechnicalFault {

		/* Création de l'objet catalogueRS à retourner. */
		CatalogueRS catalogueRS = new CatalogueRS();
		catalogueRS.setDateConsultation(Calendar.getInstance().getTime());

		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append("Consultation catalogue : ");
		loggingBuffer.append("Date catalogue : ").append(
				catalogueRS.getDateConsultation());
		LOGGER.info(loggingBuffer.toString());

		try {
			ServiceMessageManager.reset();
			/* Récupérer la collection des articles. */
			Collection<ArticleDto> articles = catalogueService.getCatalogue();

			/*
			 * Convertir ArticleDto to ArticleRS puis l'ajouter dans l'objet
			 * catalogueRS.
			 */
			catalogueRS.setArticle(CatalogueDtoConverter
					.creeListArticleRS(articles));
			if (!ServiceMessageManager.isSuccess()) {
				for (MessageDto messageDto : ServiceMessageManager
						.getMessages()) {
					LOGGER.error(messageDto.getCode() + " - "
							+ messageDto.getMessage());
					catalogueRS.addErrorMessage(messageDto.getCode(),
							messageDto.getMessage());
				}
			}
		} catch (TechnicalException e) {
			LOGGER.error(getClass(), "consulterCatalogue", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		} catch (Exception e) {
			LOGGER.error(getClass(), "consulterCatalogue", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}
		return catalogueRS;

	}

	/**
	 * Méthode detailComplement : Permet de récupérer un objet de type
	 * DetailComplement à partir d'un numéro d'apple, d'id article et d'un code
	 * d'engagement.
	 * 
	 * @param : numeroAppel
	 * @param : idArticle
	 * @param : codeEngagement
	 * @return : DetailComplementRS
	 * @throws : TechnicalFault
	 */
	public ComplementRS detailComplement(String numeroAppel, Integer idArticle)
			throws TechnicalFault {

		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append("Détail complement :");
		loggingBuffer.append("\t N° Appel : ").append(numeroAppel);
		loggingBuffer.append("\t ID poste : ").append(
				idArticle != null ? idArticle : "");
		LOGGER.info(loggingBuffer.toString());

		/* Création de l'objet detailComplementRS à retourner. */
		ComplementRS complementRS = new ComplementRS();
		try {
			//
			ServiceMessageManager.reset();
			/* Récupérer les détails produits. */
			List<ComplementDto> listComplementDto = catalogueService
					.detailComplement(numeroAppel, idArticle);
			if (listComplementDto != null) {
				for (ComplementDto complementDto : listComplementDto) {
					if (complementDto.getDuree() == 12) {
						complementRS.setEngagement12Mois(ComplementDtoConverter
								.creerDetailComplementRS(complementDto));
					} else {
						complementRS.setEngagement24Mois(ComplementDtoConverter
								.creerDetailComplementRS(complementDto));
					}
				}
			}
			if (!ServiceMessageManager.isSuccess()) {
				for (MessageDto messageDto : ServiceMessageManager
						.getMessages()) {
					LOGGER.error(messageDto.getCode() + " - "
							+ messageDto.getMessage());
					complementRS.addErrorMessage(messageDto.getCode(),
							messageDto.getMessage());
				}
			}

		} catch (FunctionalException e) {
			LOGGER.error(e.getMessage());
			complementRS.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (TechnicalException e) {
			LOGGER.error(getClass(), "detailComplement", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		} catch (Exception e) {
			LOGGER.error(getClass(), "detailComplement", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}
		return complementRS;

	}

	@Override
	public String modeByPass() {
		boolean ret1 = pingBscs.check();
		boolean ret2 = pingFidelio.check();

		if (ret1 && ret2)
			return Constantes.BY_PASS_OK;
		return Constantes.BY_PASS_NOK;

	}
}