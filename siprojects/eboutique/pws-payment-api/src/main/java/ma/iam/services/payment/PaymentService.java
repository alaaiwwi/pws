package ma.iam.services.payment;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mutualisation.fwk.webservice.interfaces.WSModeByPasse;
import ma.iam.services.common.fault.TechnicalFault;
import ma.iam.services.payment.rq.PaymentRQ;
import ma.iam.services.payment.rs.PaymentRS;
import ma.iam.services.payment.rs.ResultRS;

@WebService(name = "PaymentPortType" , targetNamespace = "http://services.iam.ma/payment/paymentService")
public interface PaymentService extends WSModeByPasse{
	
	/**
	 * La création d'une avance sur le SI GE et BSCS.
	 * @param paymentRQ Le Détails du Paiement d'avance.
	 * @return Retourne Le numéro du paiement Sur le SI GE.
	 */
	@WebResult(name = "resultat", targetNamespace = "http://services.iam.ma/payment/paymentService")
    @WebMethod
	public PaymentRS creerAvance(
			@WebParam(name = "payment", targetNamespace = "http://services.iam.ma/payment/paymentService")
			PaymentRQ paymentRQ
			) throws TechnicalFault;
	
	/**
	 * L'annulation d'une avances sur Le SI GE.
	 * @param numCommande Le numéro de la commande E-Boutique.
	 * @param motif Le Motif d'annulation.
	 * @return Retourne Une Valeur Boolean. 
	 */
	@WebResult(name = "resultat", targetNamespace = "http://services.iam.ma/payment/paymentService")
    @WebMethod
	public ResultRS annulerAvance(
			@WebParam(name = "utilisateur", targetNamespace = "http://services.iam.ma/payment/paymentService")
			String utilisateur,
			@WebParam(name = "numCommande", targetNamespace = "http://services.iam.ma/payment/paymentService")
			String numCommande,
			@WebParam(name = "motif", targetNamespace = "http://services.iam.ma/payment/paymentService")
			String motif
			) throws TechnicalFault;
}
