package ma.iam.services.payment.rs;

import java.io.Serializable;

import ma.iam.services.common.rs.BaseRS;

public class PaymentRS extends BaseRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -202006406713471577L;
	
	private Long numPayment;

	/**
	 * @param numPayment the numPayment to set
	 */
	public void setNumPayment(Long numPayment) {
		this.numPayment = numPayment;
	}

	/**
	 * @return the numPayment
	 */
	public Long getNumPayment() {
		return numPayment;
	}
	

}
