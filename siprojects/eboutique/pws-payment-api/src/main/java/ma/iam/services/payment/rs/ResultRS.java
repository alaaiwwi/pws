package ma.iam.services.payment.rs;

import ma.iam.services.common.rs.BaseRS;

public class ResultRS extends BaseRS {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7656113399478385872L;
	
	private boolean resultat;

	/**
	 * @param resultat the resultat to set
	 */
	public void setResultat(boolean resultat) {
		this.resultat = resultat;
	}

	/**
	 * @return the resultat
	 */
	public boolean isResultat() {
		return resultat;
	}

}
