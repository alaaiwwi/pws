package ma.iam.services.common.rs;

import java.io.Serializable;

public class BaseRS implements Serializable {
	
	/** La Constante serialVersionUID */
	private static final long serialVersionUID = -6949932616120253015L;
	
	/** L'attribut messageErreur. */
	private MessageRS messageErreur;

	/**
	 * @param code
	 * @param message
	 */
	public void addErrorMessage(String code, String message) {
		this.messageErreur = new MessageRS(code, message);
	}

	/**
	 * Retourner L'attribut messageErreur.
	 * @return : messageErreur
	 */
	public MessageRS getMessageErreur() {
		return messageErreur;
	}

	/**
	 * The setter method for the field messageErreur.
	 * @param messageErreur the messageErreur to set.
	 */
	public void setMessageErreur(MessageRS messageErreur) {
		this.messageErreur = messageErreur;
	}
	
}
