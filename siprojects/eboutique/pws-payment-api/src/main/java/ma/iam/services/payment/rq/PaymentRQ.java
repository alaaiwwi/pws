package ma.iam.services.payment.rq;

import java.io.Serializable;

public class PaymentRQ implements Serializable {
	
	/** The Constant serialVersionID. */
	private static final long serialVersionUID = 4296162070721131491L;

	/** Code Client. */
	private String codeClient;
	
	/** Numéro d'appel. */
	private String numAppel;
	
	/** Montant De L'avance. */
	private Double montant;
	
	/** Utilisateur E-Boutique. */
	private String utilisateur;
	
	/** ID Mode du Paiement Sur Le SI GE. */
	private Integer modePaiement;
	
	/** Référence du paiement En Ligne. */
	private String refPaiement;
	
	/** Type d'avance Sur Le SI GE. */
	private Integer typeAvance;
	
	/** Numéro De La Commande De La E-Boutique. */ 
	private String numCommande;

	/** Constructeur */
	public PaymentRQ() {
	}

	/**
	 * @return Retourner Le Code Client.
	 */
	public String getCodeClient() {
		return codeClient;
	}

	/**
	 * @param codeClient Modifier Le Code Client.
	 */
	public void setCodeClient(String codeClient) {
		this.codeClient = codeClient;
	}

	/**
	 * @return Retourner Le Numéro d'appel.
	 */
	public String getNumAppel() {
		return numAppel;
	}

	/**
	 * @param numAppel Modifier Le Numéro d'appel.
	 */
	public void setNumAppel(String numAppel) {
		this.numAppel = numAppel;
	}

	/**
	 * @return Retourner Le Montant De L'avance.
	 */
	public Double getMontant() {
		return montant;
	}

	/**
	 * @param montant Modifier Le Montant De L'avance.
	 */
	public void setMontant(Double montant) {
		this.montant = montant;
	}

	/**
	 * @return Retourner L'utilisateur E-Boutique.
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param utilisateur Modifier L'utilisateur E-Boutique.
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @return Retourner L'ID du Mode du Paiement Sur Le SI GE.
	 */
	public Integer getModePaiement() {
		return modePaiement;
	}

	/**
	 * @param modePaiement Modifier L'ID Mode du Paiement Sur Le SI GE.
	 */
	public void setModePaiement(Integer modePaiement) {
		this.modePaiement = modePaiement;
	}

	/**
	 * @return Retourner Le Référence du Paiement En Ligne.
	 */
	public String getRefPaiement() {
		return refPaiement;
	}

	/**
	 * @param refPaiement Modifier Le Référence De Paiement En Ligne.
	 */
	public void setRefPaiement(String refPaiement) {
		this.refPaiement = refPaiement;
	}

	/**
	 * @return Retourner Le Type d'avance Sur Le SI GE.
	 */
	public Integer getTypeAvance() {
		return typeAvance;
	}

	/**
	 * @param typeAvance Modifier Le Type d'avance Sur Le SI GE.
	 */
	public void setTypeAvance(Integer typeAvance) {
		this.typeAvance = typeAvance;
	}

	/**
	 * @return Retourner Le Numéro De La Commande De La E-Boutique. 
	 */
	public String getNumCommande() {
		return numCommande;
	}

	/**
	 * @param numCommande Modifier Le Numéro De La Commande De La E-Boutique. 
	 */
	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}
}
