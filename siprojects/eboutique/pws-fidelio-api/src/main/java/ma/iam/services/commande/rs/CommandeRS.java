package ma.iam.services.commande.rs;

import java.util.Date;

import ma.iam.services.common.rs.BaseRS;

/**
 * <br/><br/>
 * 
 * @author mei
 *
 */
public class CommandeRS extends BaseRS {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6891322085694838773L;
	
	private Date date;
	private String message;


	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
