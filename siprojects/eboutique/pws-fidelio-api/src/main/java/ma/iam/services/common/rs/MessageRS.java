package ma.iam.services.common.rs;

import java.io.Serializable;

public class MessageRS implements Serializable {


	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = 2086735342344381370L;
	
	private String code;
	private String detail;
	
	
	public MessageRS() {
	}
	
	/**
	 * @param code
	 * @param detail
	 */
	protected MessageRS(String code, String detail) {
		this.code = code;
		this.detail = detail;
	}
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}
	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
}
