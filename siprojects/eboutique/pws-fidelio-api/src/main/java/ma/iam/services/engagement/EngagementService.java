package ma.iam.services.engagement;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mutualisation.fwk.webservice.interfaces.WSModeByPasse;
import ma.iam.services.common.fault.TechnicalFault;
import ma.iam.services.engagement.rs.StatutClientRS;

/**
 * This class represent .
 * <br/><br/>
 * 
 * @author mei
 *
 */
@WebService(
		name = "EngagementPortType",
        targetNamespace = "http://services.iam.ma/engagement/engagementService")
public interface EngagementService extends WSModeByPasse{

	/**
	 * Vérifier l'eligibilité du client à passer des commandes de réengagement.
	 * @param numeroAppel Numéro d'appel.
	 * @return StatutClientRS.
	 */
	@WebResult(name = "statutClient", targetNamespace = "http://services.iam.ma/engagement/engagementService")
    @WebMethod
	public StatutClientRS verifierEligibiliteClient(
			@WebParam(name = "numeroAppel", targetNamespace = "http://services.iam.ma/engagement/engagementService")
			String numeroAppel
			) throws TechnicalFault;
}