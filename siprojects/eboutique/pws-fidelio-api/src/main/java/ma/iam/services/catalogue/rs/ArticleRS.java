package ma.iam.services.catalogue.rs;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class ArticleRS implements Serializable {

	/** The Constant serialVersionID. */
	private static final long serialVersionUID = 2768833057668117273L;
	
	/** Id Article */
	private Integer idPoste;
	
	/** Code Article Sur BSCS */
	private String codeArticle;
	
	/** Nom Article */
	private String libelle;
	
	/** Etat De Supenseion Dans Fidelio*/
	private Integer suspendu;
	
	/** Code Article SAP. */
	private String codeArticleSAP;
	
	LignePrixRS prix;
	
	LignePointRS points;
	
	/** Constructeur */
	public ArticleRS() {
	}

	/**
	 * @return Retourner l'ID du Poste
	 */
	public Integer getIdPoste() {
		return idPoste;
	}

	/**
	 * @param idPoste Modifier l'ID du Poste
	 */
	public void setIdPoste(Integer idPoste) {
		this.idPoste = idPoste;
	}

	/**
	 * @return Retourner Le Code Article Sur BSCS
	 */
	public String getCodeArticle() {
		return codeArticle;
	}

	/**
	 * @param codeBSCS Modifier Le Code Article Sur BSCS
	 */
	public void setCodeArticle(String codeBSCS) {
		this.codeArticle = codeBSCS;
	}

	/**
	 * @return Retourner Le Nom d'article
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle Modifier Le Nom d'article
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * @return Retourner L'Etat De Supenseion Dans Fidelio
	 */
	public Integer isSuspendu() {
		return suspendu;
	}

	/**
	 * @param etat Modifier L'Etat De Supenseion Dans Fidelio
	 */
	public void setSuspendu(Integer etat) {
		this.suspendu = etat;
	}
	
	/**
	 * The getter methode for the field codeArticleSAP.
	 * @return the codeArticleSAP
	 */
	@XmlElement (name="codeArticleSAP", nillable=true)
	public String getCodeArticleSAP() {
		return this.codeArticleSAP;
	}
	
	/**
	 * The setter method for the field codeArticleSAP.
	 * @param codeArticleSAP
	 */
	public void setCodeArticleSAP(String codeArticleSAP) {
		this.codeArticleSAP = codeArticleSAP;
	}

	/**
	 * The getter method for the field lignesPrix.
	 * @return the lignesPrix.
	 */
	public LignePrixRS getPrix() {
		return prix;
	}

	/**
	 * The setter method for the field lignesPrix.
	 * @param lignesPrix the lignesPrix to set.
	 */
	public void setPrix(LignePrixRS prix) {
		this.prix = prix;
	}


	/**
	 * @return the points
	 */
	public LignePointRS getPoints() {
		return points;
	}

	/**
	 * @param points the points to set
	 */
	public void setPoints(LignePointRS points) {
		this.points = points;
	}

}