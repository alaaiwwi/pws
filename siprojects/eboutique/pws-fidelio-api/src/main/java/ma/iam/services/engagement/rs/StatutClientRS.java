package ma.iam.services.engagement.rs;

import ma.iam.services.common.rs.BaseRS;

/**
 * <br/><br/>
 * 
 * @author mei
 *
 */
public class StatutClientRS extends BaseRS {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 898765073402733291L;
	
	private Integer resultat;
	
	public Integer getResultat() {
		return resultat;
	}
	
	public void setResultat(Integer resultat) {
		this.resultat = resultat;
	}

}
