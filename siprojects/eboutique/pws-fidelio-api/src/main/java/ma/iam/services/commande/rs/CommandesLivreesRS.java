package ma.iam.services.commande.rs;

import javax.xml.bind.annotation.XmlElement;

import ma.iam.services.common.rs.BaseRS;

/**
 * @author A175029
 *
 */
public class CommandesLivreesRS extends BaseRS {

	/**
	 * 
	 */
	private static final long serialVersionUID = 444529977849310237L;
	
	/**
	 * Liste des commandes
	 */
	private InfoCommandeRS[] commandes;

	@XmlElement (name="commande")
	public InfoCommandeRS[] getCommandes() {
		return commandes;
	}

	public void setCommandes(InfoCommandeRS[] commandes) {
		this.commandes = commandes;
	}
	
	
	
}
