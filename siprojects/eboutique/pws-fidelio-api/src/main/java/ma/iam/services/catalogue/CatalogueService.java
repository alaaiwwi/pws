package ma.iam.services.catalogue;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mutualisation.fwk.webservice.interfaces.WSModeByPasse;
import ma.iam.services.catalogue.rs.CatalogueRS;
import ma.iam.services.catalogue.rs.ComplementRS;
import ma.iam.services.common.fault.TechnicalFault;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
@WebService(
		name = "CataloguePortType",
        targetNamespace = "http://services.iam.ma/catalogue/catalogueService")
public interface CatalogueService extends WSModeByPasse{

	/**
	 * Le Catalogue des produits disponibles sur le SI Fidelio.
	 * @return Retourner Le Catalogue Des Articles.
	 */
	@WebResult(name = "Catalogue", targetNamespace = "http://services.iam.ma/catalogue/catalogueService")
    @WebMethod
	public CatalogueRS consulterCatalogue() throws TechnicalFault;
	
	/**
	 * Calculer le nombre de points acquis et Encours ainsi que le complement.
	 * @param codeClient Numéro du Client.
	 * @param numeroAppel Numéro d'appel.
	 * @param idArticle ID du Poste Sur BSCS.
	 * @param idModeEngagement ID du Mode d'engagement.
	 * @return Retourner L'objet ComplementArticleVO.
	 */
	@WebResult(name = "InfosComplement", targetNamespace = "http://services.iam.ma/catalogue/catalogueService")
    @WebMethod
	public ComplementRS detailComplement(
			@WebParam(name = "numeroAppel", targetNamespace = "http://services.iam.ma/catalogue/catalogueService")
			String numeroAppel,
			@WebParam(name = "idArticle", targetNamespace = "http://services.iam.ma/catalogue/catalogueService")
			Integer idArticle
			) throws TechnicalFault;
}