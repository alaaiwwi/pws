package ma.iam.services.catalogue.rs;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class LignePrixRS implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -6336021823085880561L;

	
	/** Prix Effectif du Poste */
	private Float prixNu;
	
	private Float prix12Mois;
	
	private Float prix24Mois;
	

	/** Constructeur */
	public LignePrixRS() {
	}



	/**
	 * The getter method for the field prixNu.
	 * @return the prixNu.
	 */
	@XmlElement(nillable=true)
	public Float getPrixNu() {
		return prixNu;
	}

	/**
	 * The setter method for the field prixNu.
	 * @param prixNu the prixNu to set.
	 */
	public void setPrixNu(Float prixNu) {
		this.prixNu = prixNu;
	}



	/**
	 * @return the prix12Mois
	 */
	@XmlElement(nillable=true)
	public Float getPrix12Mois() {
		return prix12Mois;
	}



	/**
	 * @param prix12Mois the prix12Mois to set
	 */
	public void setPrix12Mois(Float prix12Mois) {
		this.prix12Mois = prix12Mois;
	}



	/**
	 * @return the prix24Mois
	 */
	@XmlElement(nillable=true)
	public Float getPrix24Mois() {
		return prix24Mois;
	}



	/**
	 * @param prix24Mois the prix24Mois to set
	 */
	public void setPrix24Mois(Float prix24Mois) {
		this.prix24Mois = prix24Mois;
	}
}