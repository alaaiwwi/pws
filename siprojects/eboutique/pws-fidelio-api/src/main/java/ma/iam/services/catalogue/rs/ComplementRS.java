package ma.iam.services.catalogue.rs;

import ma.iam.services.common.rs.BaseRS;

public class ComplementRS extends BaseRS {
	
	private static final long serialVersionUID = -3878537564802280242L;
	private DetailComplementRS engagement12mois;
	private DetailComplementRS engagement24mois;
	
	public DetailComplementRS getEngagement12Mois() {
		return engagement12mois;
	}
	public void setEngagement12Mois(DetailComplementRS complement12) {
		this.engagement12mois = complement12;
	}
	public DetailComplementRS getEngagement24Mois() {
		return engagement24mois;
	}
	public void setEngagement24Mois(DetailComplementRS complement24) {
		this.engagement24mois = complement24;
	}
}
