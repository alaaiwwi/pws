package ma.iam.services.common.fault;

import javax.xml.ws.WebFault;

@WebFault(name = "technicalFault", targetNamespace = "http://services.iam.ma/common/technicalFault")
public class TechnicalFault extends Exception{

	private static final long serialVersionUID = -4011929467790787502L;
	
	private String codeErreur;
	
	public TechnicalFault(){
		super();
	}
	
	public TechnicalFault(String message){
		super(message);
	}

	public TechnicalFault(String message, String code){
		super(message);
		setCodeErreur(code);
	}
	
	public TechnicalFault(String message, String code, Throwable throwable){
		super(message, throwable);
		setCodeErreur(code);
	}

	public void setCodeErreur(String codeErreur) {
		this.codeErreur = codeErreur;
	}

	public String getCodeErreur() {
		return codeErreur;
	}
}
