package ma.iam.services.commande;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mutualisation.fwk.webservice.interfaces.WSModeByPasse;
import ma.iam.services.commande.rq.CommandeRQ;
import ma.iam.services.commande.rs.CommandeRS;
import ma.iam.services.commande.rs.CommandesLivreesRS;
import ma.iam.services.common.fault.TechnicalFault;

@WebService(name = "CommandePortType" , targetNamespace = "http://services.iam.ma/commande/commandeService")
public interface CommandeService extends WSModeByPasse{

	/**
	 * la création d'une commande de réservation 
	 * d'un poste avec engagement sur le SI Fidelio.
	 * @param commandeRQ La commande de réservation d'article.
	 * @return Retourne la date de création de la commande sur Fidelio.
	 */
	@WebResult(name = "InfoCommande", targetNamespace = "http://services.iam.ma/commande/commandeService")
    @WebMethod
	public CommandeRS creerCommande(
			@WebParam(name = "commande", targetNamespace = "http://services.iam.ma/commande/commandeService")
			CommandeRQ commandeRQ
			) throws TechnicalFault;
	
	/**
	 * L'annulation d'une commande de réservation d'un poste.
	 * @param numeroCommande Le Numéro de la Commande.
	 * @param codeClient Le Code Client.
	 * @param motif Motif d'annulation.
	 * @return Retourne Une Valeur Boolean. 
	 */
	@WebResult(name = "resultat", targetNamespace = "http://services.iam.ma/commande/commandeService")
    @WebMethod
	public CommandeRS annulerCommande(
			@WebParam(name = "numeroCommande", targetNamespace = "http://services.iam.ma/commande/commandeService")
			String numeroCommande,
			@WebParam(name = "codeClient", targetNamespace = "http://services.iam.ma/commande/commandeService")
			String codeClient, 
			@WebParam(name = "motif", targetNamespace = "http://services.iam.ma/commande/commandeService")
			String motif
			) throws TechnicalFault;
	
	
	/**
	 * Lister les commandes E-Boutique livrées répondant aux critères en entrée
	 * @param numeroCommande Le Numéro de la Commande.
	 * @param codeClient Le Code Client.
	 * @param motif Motif d'annulation.
	 * @return Retourne Une Valeur Boolean. 
	 */
	@WebResult(name = "resultat", targetNamespace = "http://services.iam.ma/commande/commandeService")
    @WebMethod
	public CommandesLivreesRS listerCommandesLivrees(
			@WebParam(name = "dateDebut", targetNamespace = "http://services.iam.ma/commande/commandeService")
			Date dateDebut,
			@WebParam(name = "dateFin", targetNamespace = "http://services.iam.ma/commande/commandeService")
			Date dateFin
			) throws TechnicalFault;
	
}
