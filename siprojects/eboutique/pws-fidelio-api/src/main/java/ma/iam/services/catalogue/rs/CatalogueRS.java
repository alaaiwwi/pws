package ma.iam.services.catalogue.rs;

import java.util.Date;

import ma.iam.services.common.rs.BaseRS;


public class CatalogueRS extends BaseRS {
	
	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -4979997335833126994L;

	/** Date Consultaion du Catalogue */
	private Date dateConsultation;
	
	/** La Liste des articles du catalogue */
	private ArticleRS[] article;
	
	/** Constructeur */
	public CatalogueRS() {
	}
	
	/**
	 * @return retourner la date d'appel du catalogue
	 */
	public Date getDateConsultation() {
		return dateConsultation;
	}

	/**
	 * @param dateConsultation modifier la date d'appel du catalogue
	 */
	public void setDateConsultation(Date dateConsultation) {
		this.dateConsultation = dateConsultation;
	}

	/**
	 * @return retourner la liste des articles
	 */
	public ArticleRS[] getArticle() {
		return article;
	}

	/**
	 * @param articles modifier la liste des articles
	 */
	public void setArticle(ArticleRS[] article) {
		this.article = article;
	}
}