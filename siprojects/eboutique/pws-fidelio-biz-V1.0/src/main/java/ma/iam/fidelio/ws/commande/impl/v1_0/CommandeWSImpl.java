package ma.iam.fidelio.ws.commande.impl.v1_0;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.xml.ws.WebServiceContext;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dto.CommandeDto;
import ma.iam.fidelio.dto.CommandeLivreeDto;
import ma.iam.fidelio.dto.converter.CommandeDtoConverter;
import ma.iam.fidelio.dto.converter.CommandeLivreeDtoConverter;
import ma.iam.fidelio.services.commande.CommandeService;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;
import ma.iam.mutualisation.fwk.common.util.Utils;
import ma.iam.mutualisation.fwk.security.SecurityUtils;
//import ma.iam.services.authentication.Constants;
//import ma.iam.services.authentication.UserDetails;
import ma.iam.services.commande.rq.CommandeRQ;
import ma.iam.services.commande.rs.CommandeRS;
import ma.iam.services.commande.rs.CommandesLivreesRS;
import ma.iam.services.common.fault.TechnicalFault;
import net.atos.ma.integration.jee.bypass.JDBCPing;

//import org.springframework.security.providers.dao.UserCache;
import org.w3c.dom.Element;

/**
 * Web Service : Création Ou Annulation d'une Commande.
 */
@javax.jws.WebService(serviceName = "CommandeService", portName = "CommandeServicePort", targetNamespace = "http://services.iam.ma/commande/commandeService", endpointInterface = "ma.iam.services.commande.CommandeService")
public class CommandeWSImpl implements ma.iam.services.commande.CommandeService {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager
			.getLogger(CommandeWSImpl.class.getName());

	private WebServiceContext wsContext;

	/**
	 * The setter method for the field wsContext.
	 * 
	 * @param wsContext
	 *            the wsContext to set.
	 */
	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	/** L'attribut commandeService. */
	private CommandeService commandeService;

	/**
	 * Modifier l'objet commandeService.
	 * 
	 * @param : commandeService
	 */
	public void setCommandeService(CommandeService commandeService) {
		this.commandeService = commandeService;
	}

	/**
	 * objet pour le ping sur la base bscs
	 */
	private JDBCPing pingBscs;
	/**
	 * objet pour le ping sur la base fidelio
	 */
	private JDBCPing pingFidelio;

	public void setPingBscs(JDBCPing pingBscs) {
		this.pingBscs = pingBscs;
	}

	public void setPingFidelio(JDBCPing pingFidelio) {
		this.pingFidelio = pingFidelio;
	}

	/**
	 * Méthode annulerCommande : Permet d'annuler une commande à partir d'un
	 * numCommande, codeClient, et motif.
	 * 
	 * @param : numCommande
	 * @param : codeClient
	 * @param : motif
	 * @return : CommandeRS
	 * @throws : TechnicalFault
	 */
	@RolesAllowed("ROLE_WRITE")
	public CommandeRS annulerCommande(String numCommande, String codeClient,
			String motif) throws TechnicalFault {

		Element usernameToken = SecurityUtils.getUsernameTokenHeader(wsContext);
		String login = SecurityUtils.getUsername(usernameToken);

		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append("Annulation de la commande N° : ").append(
				numCommande);
		loggingBuffer.append("\n\t Code client : ").append(codeClient);
		loggingBuffer.append("\n\t Motif :").append(motif);
		loggingBuffer.append("\n\t Utilisateur :").append(login);
		LOGGER.info(loggingBuffer.toString());

		/* Création d'un objet de type CommandeRS. */
		CommandeRS response = new CommandeRS();
		try {
			String retour = commandeService.annulerCommande(numCommande,
					codeClient, login, motif);
			response.setMessage(retour);
			LOGGER.info("Commande annulée");
		} catch (FunctionalException e) {
			LOGGER.error(e.getMessage());
			response.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (TechnicalException e) {
			LOGGER.error(e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		} catch (Exception e) {
			LOGGER.error(getClass(), "annulerCommande", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}

		return response;
	}

	/**
	 * Méthode creerCommande : Permet de créer une commande à partir d'un objet
	 * de type CommandeRQ.
	 * 
	 * @param : commande
	 * @return : CommandeRS
	 * @throws : TechnicalFault
	 */
	@RolesAllowed("ROLE_WRITE")
	public CommandeRS creerCommande(CommandeRQ commande) throws TechnicalFault {

		/* Création d'un objet de type CommandeRS. */
		CommandeRS response = new CommandeRS();
		try {
			if (commande == null) {
				throw new FunctionalException(Constantes.ERREUR_CODE_33,
						Constantes.NUMERO_COMMANDE_OBLIGATOIRE);
			}
			StringBuffer loggingBuffer = new StringBuffer();
			loggingBuffer.append("Création de la commande N° : ").append(
					commande.getNumCommande());
			loggingBuffer.append("\n\t Utilisateur :").append(
					commande.getUtilisateur());
			loggingBuffer.append("\n\t Agence :").append(
					commande.getCodeAgence() != null ? commande.getCodeAgence()
							: "");
			loggingBuffer.append("\n\t Engagement code :").append(
					commande.getIdEngagement() != null ? commande
							.getIdEngagement() : "");
			loggingBuffer.append("\n\t ND : ").append(commande.getNumAppel());
			loggingBuffer.append("\n\t ID Poste :").append(
					commande.getIdArticle() != null ? commande.getIdArticle()
							: "");
			loggingBuffer.append("\n\t Complement :").append(
					commande.getComplement() != null ? commande.getComplement()
							: "");
			loggingBuffer.append("\n\t Points Acquis :").append(
					commande.getPointsAcquis() != null ? commande
							.getPointsAcquis() : "");
			loggingBuffer.append("\n\t Points Encours :").append(
					commande.getPointsEncours() != null ? commande
							.getPointsEncours() : "");
			LOGGER.info(loggingBuffer.toString());

			// TODO FOR SEcurity

			// UserCache userCache = (UserCache)
			// ContextManager.getInstance().getBean(Constants.BEAN_USER_CACHE);
			// UserDetails userDetails = (UserDetails)
			// userCache.getUserFromCache(commande.getUtilisateur());
			// if (userDetails == null) {
			// throw new FunctionalException(Constantes.ERR_AUTH_04);
			// }

			/* Conversion d'un objet de type CommandeRQ to CommandeDto. */
			CommandeDto commandeDto = CommandeDtoConverter
					.creerCommandeDto(commande);
			Date dateCommande = commandeService.passerCommande(commandeDto);
			response.setDate(dateCommande);
			LOGGER.info("Commande créée");
		} catch (FunctionalException e) {
			LOGGER.error(e.getMessage());
			response.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (Exception e) {
			LOGGER.error(getClass(), "creerCommande", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}
		return response;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see ma.iam.services.commande.CommandeService#listerCommandesLivrees(java.util.Date,
	 *      java.util.Date)
	 */
	public CommandesLivreesRS listerCommandesLivrees(Date dateDebut,
			Date dateFin) throws TechnicalFault {

		// LOGGING
		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append("Recherche des commandes E-Boutique livrées  : ");
		loggingBuffer.append("\n\t Date début : ").append(
				dateDebut != null ? Utils.dateToString(dateDebut,
						Constantes.ENGLISH_DATE_FORMAT) : "null");
		loggingBuffer.append("\n\t Date fin : ").append(
				dateFin != null ? Utils.dateToString(dateFin,
						Constantes.ENGLISH_DATE_FORMAT) : "null");

		LOGGER.info(loggingBuffer.toString());

		// Création d'un objet de type CommandesLivreesRS.
		CommandesLivreesRS response = new CommandesLivreesRS();
		try {
			List<CommandeLivreeDto> lCommande = commandeService
					.rechercheCommandesLivrees(dateDebut, dateFin);
			response.setCommandes(CommandeLivreeDtoConverter
					.creerListeCommandesEBLivrees(lCommande));

		} catch (FunctionalException e) {
			LOGGER.error(e.getMessage());
			response.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (TechnicalException e) {
			LOGGER.error(e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		} catch (Exception e) {
			LOGGER.error(getClass(), "listerCommandesLivrees", e.getMessage(),
					e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERREUR_TECHNIQUE));
		}
		return response;
	}

	@Override
	public String modeByPass() {
		boolean ret1 = pingBscs.check();
		boolean ret2 = pingFidelio.check();

		if (ret1 && ret2)
			return Constantes.BY_PASS_OK;
		return Constantes.BY_PASS_NOK;

	}

}