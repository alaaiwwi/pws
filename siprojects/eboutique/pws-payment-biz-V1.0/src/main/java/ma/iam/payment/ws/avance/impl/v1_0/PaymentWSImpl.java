package ma.iam.payment.ws.avance.impl.v1_0;

import javax.annotation.security.RolesAllowed;

import ma.iam.common.Constantes;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;
import ma.iam.mutualisation.fwk.web.context.ContextManager;
import ma.iam.payment.dto.PaiementDto;
import ma.iam.payment.dto.UserDto;
import ma.iam.payment.dto.converter.PayerAvanceDtoConv;
import ma.iam.payment.service.PaiementService;
import ma.iam.payment.service.UserService;
import ma.iam.services.authentication.Constants;
import ma.iam.services.authentication.UserDetails;
import ma.iam.services.common.fault.TechnicalFault;
import ma.iam.services.payment.rq.PaymentRQ;
import ma.iam.services.payment.rs.PaymentRS;
import ma.iam.services.payment.rs.ResultRS;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.security.core.userdetails.UserCache;

@javax.jws.WebService(serviceName = "PaymentService", portName = "PaymentServicePort", targetNamespace = "http://services.iam.ma/payment/paymentService", endpointInterface = "ma.iam.services.payment.PaymentService")
public class PaymentWSImpl implements ma.iam.services.payment.PaymentService {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager
			.getLogger(PaymentWSImpl.class.getName());

	private PaiementService paiementService;

	/**
	 * The setter method for the field paiementService.
	 * 
	 * @param paiementService
	 *            the paiementService to set.
	 */
	public void setPaiementService(PaiementService paiementService) {
		this.paiementService = paiementService;
	}

	/**
	 * objet pour le ping sur la base bscs
	 */
	private JDBCPing pingBscs;
	/**
	 * objet pour le ping sur la base fidelio
	 */
	private JDBCPing pingFidelio;

	public void setPingBscs(JDBCPing pingBscs) {
		this.pingBscs = pingBscs;
	}

	public void setPingFidelio(JDBCPing pingFidelio) {
		this.pingFidelio = pingFidelio;
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.services.paiement.PaiementService#annulerAvance(java.lang.String,
	 * java.lang.Integer, java.lang.String).
	 * 
	 * @param numCommande
	 * @param motif
	 * @return
	 * @throws TechnicalFault
	 */
	@RolesAllowed("ROLE_WRITE")
	public ResultRS annulerAvance(String username, String numCommande,
			String motif) throws TechnicalFault {
		ResultRS response = new ResultRS();

		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append(
				"Annulation de l'avance liée à la commande EB N° : ").append(
				numCommande);
		loggingBuffer.append("\n\t Motif :").append(motif);
		loggingBuffer.append("\n\t Utilisateur : ").append(username);
		LOGGER.info(loggingBuffer.toString());
		try {
			UserCache userCache = (UserCache) ContextManager.getInstance()
					.getBean(Constants.BEAN_USER_CACHE);
			UserDetails userDetails = (UserDetails) userCache
					.getUserFromCache(username);
			if (userDetails == null) {
				throw new FunctionalException(Constantes.ERR_CODE_04,
						Constantes.ERR_AUTHORISATION);
			}
			response.setResultat(paiementService.annulerAvance(username,
					numCommande, motif));
			LOGGER.info("Avance annulée");
		} catch (FunctionalException e) {
			LOGGER.error(e.getMessage());
			response.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (Exception e) {
			LOGGER.error(getClass(), "annulerAvance", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERR_TECHNIQUE));
		}

		return response;
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.services.paiement.PaiementService#creerAvance
	 * (ma.iam.services.paiement.rq.PaymentRQ).
	 * 
	 * @param paymentRQ
	 * @return
	 * @throws TechnicalFault
	 */
	@RolesAllowed("ROLE_WRITE")
	public PaymentRS creerAvance(PaymentRQ paymentRQ) throws TechnicalFault {

		PaymentRS response = new PaymentRS();

		StringBuffer loggingBuffer = new StringBuffer();

		loggingBuffer.append("Paiement d'avance pour la commande EB N° : ")
				.append(paymentRQ.getNumCommande());
		loggingBuffer.append("\n\t code Client : ").append(
				paymentRQ.getCodeClient());
		loggingBuffer.append("\n\t Mode de paiement : ").append(
				paymentRQ.getModePaiement() != null ? paymentRQ
						.getModePaiement() : "");
		loggingBuffer.append("\n\t Montant : ").append(
				paymentRQ.getMontant() != null ? paymentRQ.getMontant() : "");
		loggingBuffer.append("\n\t ND : ").append(paymentRQ.getNumAppel());
		loggingBuffer.append("\n\t Référence du Paiement : ").append(
				paymentRQ.getRefPaiement());
		loggingBuffer.append("\n\t Type de transaction :").append(
				paymentRQ.getTypeAvance() != null ? paymentRQ.getTypeAvance()
						: "");
		loggingBuffer.append("\n\t Utilisateur : ").append(
				paymentRQ.getUtilisateur());
		LOGGER.info(loggingBuffer.toString());

		try {
			UserService userService = (UserService) ContextManager
					.getInstance().getBean("userService");

			UserDto userDto = userService.getUserByLogin(paymentRQ
					.getUtilisateur());
			PaiementDto paiementDto = PayerAvanceDtoConv.creerPaiementDto(
					paymentRQ, userDto.getCostId(), userDto.getLocationId());
			response.setNumPayment(paiementService.payerAvance(paiementDto));
			LOGGER.info("Avance créée N° Paiement :" + response.getNumPayment());
		} catch (FunctionalException e) {
			LOGGER.error(e.getMessage());
			response.addErrorMessage(e.getMessageCode(), e.getMessage());
		} catch (Exception e) {
			LOGGER.error(getClass(), "creerAvance", e.getMessage(), e);
			throw new TechnicalFault(
					MessageUtils.getMessage(Constantes.ERR_TECHNIQUE));
		}
		return response;
	}

	@Override
	public String modeByPass() {
		boolean ret1 = pingBscs.check();
		boolean ret2 = pingFidelio.check();

		if (ret1 && ret2)
			return Constantes.BY_PASS_OK;
		return Constantes.BY_PASS_NOK;

	}
}
