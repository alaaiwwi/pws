package ma.iam.payment.service.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.util.Utils;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.bscs.ClientDao;
import ma.iam.payment.dao.bscs.FactureDao;
import ma.iam.payment.dao.bscs.PaymentBSCSDao;
import ma.iam.payment.dao.ge.PaymentGEDao;
import ma.iam.payment.dao.ge.PaymentModeDao;
import ma.iam.payment.dao.ge.TransactionTypeDao;
import ma.iam.payment.domaine.bscs.Client;
import ma.iam.payment.domaine.bscs.Facture;
import ma.iam.payment.domaine.bscs.PaymentBSCS;
import ma.iam.payment.domaine.bscs.PaymentBSCSDetail;
import ma.iam.payment.domaine.bscs.PaymentBSCSDetailId;
import ma.iam.payment.domaine.bscs.Ticket;
import ma.iam.payment.domaine.ge.GlCode;
import ma.iam.payment.domaine.ge.PaymentCommandeEB;
import ma.iam.payment.domaine.ge.PaymentGE;
import ma.iam.payment.domaine.ge.PaymentGEDetail;
import ma.iam.payment.domaine.ge.PaymentMode;
import ma.iam.payment.domaine.ge.TransactionType;
import ma.iam.payment.dto.PaiementDto;
import ma.iam.payment.service.PaiementService;

/**
 * Service de paiement . <br/>
 * <br/>
 * 
 * @author mei
 *
 */
public class PaiementServiceImpl implements PaiementService {

	/** The client DAO. */
	private ClientDao clientDao = null;

	private FactureDao factureDao = null;

	private PaymentBSCSDao paymentBSCSDao;

	private PaymentGEDao paymentGEDao;

	private TransactionTypeDao transactionTypeDao = null;

	private PaymentModeDao paymentModeDao;

	/**
	 * The setter method for the field factureDao.
	 * 
	 * @param factureDao
	 *            the factureDao to set.
	 */
	public void setFactureDao(FactureDao factureDao) {
		this.factureDao = factureDao;
	}

	/**
	 * The setter method for the field paymentBSCSDao.
	 * 
	 * @param paymentBSCSDao
	 *            the paymentBSCSDao to set.
	 */
	public void setPaymentBSCSDao(PaymentBSCSDao paymentBSCSDao) {
		this.paymentBSCSDao = paymentBSCSDao;
	}

	/**
	 * The setter method for the field paymentGEDao.
	 * 
	 * @param paymentGEDao
	 *            the paymentGEDao to set.
	 */
	public void setPaymentGEDao(PaymentGEDao paymentGEDao) {
		this.paymentGEDao = paymentGEDao;
	}

	/**
	 * The setter method for the field transactionTypeDao.
	 * 
	 * @param transactionTypeDao
	 *            the transactionTypeDao to set.
	 */
	public void setTransactionTypeDao(TransactionTypeDao transactionTypeDao) {
		this.transactionTypeDao = transactionTypeDao;
	}

	/**
	 * The setter method for the field paymentModeDao.
	 * 
	 * @param paymentModeDao
	 *            the paymentModeDao to set.
	 */
	public void setPaymentModeDao(PaymentModeDao paymentModeDao) {
		this.paymentModeDao = paymentModeDao;
	}

	/**
	 * Sets the client DAO.
	 *
	 * @param clientDao
	 *            the new client DAO
	 */
	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

	/**
	 * Crée un paiement de type avance dans BSCS.</br></br>
	 * 
	 * @param utilisateur
	 * @param debGlcode
	 * @param costId
	 * @param client
	 * @param chknum
	 * @param carem
	 * @param montant
	 * @param factureId
	 * @return
	 * @throws TechnicalException
	 */
	private PaymentBSCS creerAvanceBSCS(String utilisateur, String debGlcode,
			Integer costId, Client client, String chknum,
			TransactionType typeTrx, Double montant, Long factureId)
			throws TechnicalException {

		// mis à jour de la balance client
		clientDao.updateBalanceClient(client.getId(), montant);

		PaymentBSCS payment = paymentBSCSDao.savePaiement(client, new Date(),
				chknum, debGlcode, Constantes.GL_DIS_6386000000,
				Long.valueOf(Constantes.CA_TYPE_3), typeTrx.getTrDesc(),
				Constantes.VAL_VIDE, utilisateur,
				Constantes.PARAM_PAIEMENT_DEBIT_INFO, costId,
				Constantes.VALEUR_1, Constantes.VALEUR_1, montant,
				Constantes.VAL_DOUBLE_0, montant, montant,
				Constantes.VAL_DOUBLE_0, montant, montant,
				Constantes.CA_REASON_CODE_19);

		// Création de la facture
		Facture newFacture = new Facture();
		newFacture.setId(factureId);
		newFacture.setStatut(Constantes.FACTURE_STATUS_CO);
		newFacture.setEntDate(new Date());
		newFacture.setNumReference(chknum);
		newFacture.setRefClient(client);
		if (!Utils.isEmpty(client.getRefContact().getNomS())) {
			newFacture.setName(client.getRefContact().getNomS());
		} else {
			newFacture.setName(client.getRefContact().getNom());
		}
		if (newFacture.getName() != null && newFacture.getName().length() > 40) {
			newFacture.setName(newFacture.getName().substring(0, 40));
		}
		newFacture.setVille(client.getRefContact().getVille());
		newFacture.setCodePostal(client.getRefContact().getCodePostal());
		newFacture.setDateFacture(new Date());
		newFacture.setDateEcheance(new Date()); // à vérifier
		newFacture.setGlar(Constantes.GLAR_44211001);
		newFacture.setArFlag(Constantes.AR_FLAG_B);
		newFacture.setConvDateExchange(new Date());
		newFacture.setMode(Constantes.FACTURE_MODE_X);
		newFacture.setVersion(Constantes.VALEUR_1);
		newFacture.setCostCenter(costId);
		newFacture.setDocConvrateTypeId(Constantes.VALEUR_1);
		newFacture.setDocCurrency(Constantes.DEVISE_MAROC_DIRHAM);
		newFacture.setGlCurrency(Constantes.DEVISE_MAROC_DIRHAM);

		newFacture.setMontantFacture(0 - montant);
		newFacture.setMontantOuvert(0 - montant);
		newFacture.setInvAmtDoc(0 - montant);
		newFacture.setOpnAmtDoc(0 - montant);
		newFacture.setGlConvrateTypeId(Constantes.VALEUR_1);

		newFacture.setIpp(factureDao.getPeriodeFacture());
		/* pour résoudre le problème de facturation : que pour la BSCS Mobile */
		newFacture.setBalancePageEventDate(new Date());
		factureDao.saveFacture(newFacture);

		// Création détail paiement
		PaymentBSCSDetail payBSCSDetail = new PaymentBSCSDetail();

		PaymentBSCSDetailId payBSCSDetailId = new PaymentBSCSDetailId();
		payBSCSDetailId.setIdOrdre(newFacture.getId());
		payBSCSDetailId.setIdPaiement(payment.getId());
		payBSCSDetail.setId(payBSCSDetailId);
		payBSCSDetail.setCadaglar(Constantes.GLAR_44211001);
		payBSCSDetail.setCadassocxact(payment.getId());
		payBSCSDetail.setCadexpconvdateExchange(new Date());
		payBSCSDetail.setRecVersion(payment.getRecVersion());
		payBSCSDetail.setPaymentDevise(Constantes.DEVISE_MAROC_DIRHAM);
		payBSCSDetail.setDocumentDevise(Constantes.DEVISE_MAROC_DIRHAM);
		payBSCSDetail.setGlDevise(Constantes.DEVISE_MAROC_DIRHAM);
		payBSCSDetail.setCadAmtDoc(montant);
		payBSCSDetail.setCadAmtPay(montant);
		payBSCSDetail.setCadCurAmtDoc(montant);
		payBSCSDetail.setCadCurAmtGl(montant);
		payBSCSDetail.setCadCurAmtPay(montant);
		payBSCSDetail.setMontant(montant);
		payBSCSDetail.setCadConvDateExchangeDoc(new Date());
		payBSCSDetail.setCadConvDateExchangeGl(new Date());
		paymentBSCSDao.savePaiementDetailBSCS(payBSCSDetail);

		// Création Mémo BSCS
		Ticket ticket = new Ticket();
		ticket.setDescription2(Constantes.AR_DEPOSIT_OUT);
		ticket.setDescription(newFacture.getId().toString());
		ticket.setCode(typeTrx.getAddtickler());
		ticket.setFollowUpDate(new Date());
		ticket.setUser(utilisateur);
		ticket.setStatus(Constantes.TICKET_STATUS_NOTE);
		ticket.setIdClient(client.getId());
		ticket.setFollowUpStatus(Constantes.TICKET_FOLLOW_UP_STATUS_N);
		ticket.setRecVersion(Constantes.VALEUR_1);
		ticket.setPriorite(Constantes.TICKET_PRIORITY_5);
		ticket.setDateCreation(new Date());
		paymentBSCSDao.saveMemoBSCS(ticket);

		return payment;
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.paiement.service.PaiementService#payerAvance
	 * (ma.iam.paiement.dto.PaiementDto).
	 * 
	 * @param paiementDto
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public Long payerAvance(PaiementDto paiementDto) throws TechnicalException,
			FunctionalException {

		if (!Utils.isEmpty(paiementDto.getNumCommande())) {
			// Vérifier que aucun paiement n'a été fait pour la même commande EB
			PaymentGE paymentGE = paymentGEDao
					.getPaymentByNumCommande(paiementDto.getNumCommande());

			if (paymentGE != null) {
				throw new FunctionalException(Constantes.ERR_CODE_06,
						Constantes.ERR_AVANCE_DEJA_SAISIE);
			}
		}
		// Recuperer le client
		Client client = null;
		String codeClient = paiementDto.getCodeClient();
		if (Utils.isEmpty(codeClient)) {
			client = clientDao.getClientByDN(paiementDto.getNumAppel());
			// ND Client inconnu
			if (client == null) {
				throw new FunctionalException(Constantes.ERR_CODE_03,
						Constantes.ERR_ND_CLIENT_INCONNU);
			}
			codeClient = client.getCode();
		}
		client = clientDao.getResponsablePaiement(codeClient);

		// Code ou ND Client inconnu
		if (client == null) {
			throw new FunctionalException(Constantes.ERR_CODE_03,
					Constantes.ERR_ND_CLIENT_INCONNU);
		}
		TransactionType typeTrx = transactionTypeDao
				.getTransactionTypeById(paiementDto.getTypeTrx());
		// TYPE AVANCE inconnu
		if (typeTrx == null) {
			throw new FunctionalException(Constantes.ERR_CODE_04,
					Constantes.ERR_TYPE_AVANCE_INCONNU);
		}

		// Recuperer le mode de paiement
		PaymentMode pMode = null;
		if (paiementDto.getModePaiement() != null) {
			pMode = paymentModeDao.getPaymentModeById(paiementDto
					.getModePaiement());
		}
		// Mode de paiement inconnu
		if (pMode == null) {
			throw new FunctionalException(Constantes.ERR_CODE_05,
					Constantes.ERR_MODE_PAIEMENT_INCONNU);
		}

		GlCode glCode = paymentGEDao.getGlcodeByTrxAndLocation(
				paiementDto.getTypeTrx(), paiementDto.getUserLocationId());

		String debGlcode = Constantes.VAL_VIDE;
		if (glCode != null) {
			debGlcode = glCode.getDebGlcode();
		}

		Long factureId = factureDao.getFactureSeq();

		// Crée une avance dans BSCS
		PaymentBSCS payment = this.creerAvanceBSCS(
				paiementDto.getUtilisateur(), debGlcode,
				paiementDto.getCostCenterId(), client,
				paiementDto.getRefPaiement(), typeTrx,
				paiementDto.getMontant(), factureId);

		// construction d'un paiement au niveau GE
		PaymentGE payGE = new PaymentGE();
		payGE.setCostId(paiementDto.getCostCenterId());
		payGE.setCustomerId(client.getId());
		payGE.setGlcode(debGlcode);
		String custName = "";
		if (client.getRefContact() != null) {
			if (!Utils.isEmpty(client.getRefContact().getNomS())) {
				custName = client.getRefContact().getNomS();
			} else {
				if (client.getRefContact().getNom() != null) {
					custName = client.getRefContact().getNom();
				}
				if (client.getRefContact().getPrenom() != null) {
					custName = custName + " "
							+ client.getRefContact().getPrenom();
				}
			}
		}
		payGE.setCustName(custName);
		payGE.setUsername(paiementDto.getUtilisateur());
		payGE.setPriorsystem(Constantes.SYSTEME_MOBILE);

		payGE.setChknum(paiementDto.getRefPaiement());
		payGE.setChkdate(new Date());

		// Lier le paiement au reponsable de paiement au niveau de GE
		payGE.setCustomerId(client.getId());
		payGE.setNewSub(Constantes.VAL_VIDE); // à verifier

		payGE.setSemaOpTrxType(typeTrx);

		payGE.setSemaOpPayMode(pMode);
		// Le paiement sur GE sera créé avec le statut Déposé
		// et un numéro de bordereau lui sera attribué
		payGE.setStatus(Constantes.STATUS_PAIEMENT_DEPOSE);
		String bordnum = new DecimalFormat(Constantes.PATTERN_NUM_BORDEREAU)
				.format(paymentGEDao.getNextNumBordereau());
		payGE.setBorduNum(bordnum);
		payGE.setTotalAmt(paiementDto.getMontant()); // a verifier
		payGE.setPostAmt(paiementDto.getMontant());
		payGE.setStampAmt(Constantes.VAL_DOUBLE_0);
		payGE.setTrxDate(new Date());

		payGE.setCaxact(payment.getId());
		paymentGEDao.savePaymentGE(payGE);

		// Ajout du détail paiement GE
		PaymentGEDetail payGEDetail = new PaymentGEDetail();
		payGEDetail.setCaxact(payment.getId());
		payGEDetail.setChkamt(paiementDto.getMontant());
		payGEDetail.setSystem(Constantes.SYSTEME_MOBILE);
		payGEDetail.setOhxact(factureId);
		payGEDetail.setPaymentId(payGE.getId());
		paymentGEDao.savePaiementDetailGE(payGEDetail);

		// création d'un nouveau statut lié a ce paiement
		paymentGEDao.updatePaymentStatus(payGE,
				Constantes.STATUS_PAIEMENT_DEPOSE,
				paiementDto.getUtilisateur(), 1);

		if (!Utils.isEmpty(paiementDto.getNumCommande())) {
			// Création de la liaison entre la commande de la boutique en ligne
			// et le paiement GE
			PaymentCommandeEB paymentCommandeEB = new PaymentCommandeEB(payGE,
					paiementDto.getNumCommande());
			paymentGEDao.savePaymentCommandeEB(paymentCommandeEB);
		}
		return payGE.getId();

	}

	/**
	 * This method allows you to .</br></br> Created on Sprint #
	 * 
	 * @param username
	 * @param paiement
	 * @param motif
	 * @param isPayOrginal
	 * @throws TechnicalException
	 */
	private void annulerPaiementBSCS(String username, PaymentBSCS paiement,
			String motif, boolean isPayOrginal) throws TechnicalException {

		PaymentBSCS paiementAnnule = null;
		PaymentBSCSDetail detailPaiement = null;
		PaymentBSCSDetail detailPaiementAnnule = null;
		Facture facture = null;
		PaymentBSCSDetailId detailPaiementId = null;
		List<PaymentBSCSDetail> listPaymentBSCSDetail = null;

		// BALANCER le paiement ORIGINAL
		if (isPayOrginal) {
			paiement.setMontantCourant(Constantes.VAL_DOUBLE_0);
			paiement.setMontantPaye(Constantes.VAL_DOUBLE_0);
			paiement.setRecVersion(paiement.getRecVersion() + 1);
			paymentBSCSDao.savePaiement(paiement);

			// créer mémo
			Ticket ticket = new Ticket();
			ticket.setFollowUpStatus(Constantes.TICKET_FOLLOW_UP_STATUS_N);
			ticket.setRecVersion(Constantes.VALEUR_1);
			ticket.setDescription(Constantes.TICKET_DESC_AR_BOUNCE_CHECK);
			ticket.setDateCreation(new Date());
			ticket.setFollowUpDate(new Date());
			ticket.setPriorite(Constantes.TICKET_PRIORITY_8);
			ticket.setStatus(Constantes.TICKET_STATUS_NOTE);
			ticket.setCode(Constantes.TICKET_CODE_SYSTEM);
			ticket.setIdClient(paiement.getRefClient().getId());
			paymentBSCSDao.saveMemoBSCS(ticket);
		}
		// inserer un paiement negatif dans CASHRECEIPTS avec CATYPE, REASONCODE
		// 3, 18
		Double lMontantRegle = 0d;
		if (paiement.getMontantRegle() != null) {
			lMontantRegle = 0 - Math.abs(paiement.getMontantRegle());
		}
		paiementAnnule = paymentBSCSDao.savePaiement(paiement.getRefClient(),
				new Date(), paiement.getNumReference(), paiement.getCodeGL(),
				paiement.getNumGL(), paiement.getType(), motif,
				paiement.getPostingPeriod(), username,
				Constantes.PARAM_PAIEMENT_DEBIT_INFO, paiement.getCostCenter(),
				paiement.getConvrateTypeGL(), paiement.getConvrateTypeDoc(),
				lMontantRegle, Constantes.VAL_DOUBLE_0,
				Constantes.VAL_DOUBLE_0, lMontantRegle, null,
				Constantes.VAL_DOUBLE_0, lMontantRegle,
				Constantes.CA_REASON_CODE_20);

		// Ouvrir toutes les factures/Avances cloturées par le
		// paiement original
		if (paiement.getLignesPaiement() != null) {
			for (Iterator itFactures = paiement.getLignesPaiement().iterator(); itFactures
					.hasNext();) {
				detailPaiement = (PaymentBSCSDetail) itFactures.next();
				facture = detailPaiement.getRefFacture();

				// annuler le detail paiement correpondant au paiement courant
				detailPaiementId = new PaymentBSCSDetailId();
				detailPaiementId.setIdOrdre(facture.getId());
				detailPaiementId.setIdPaiement(paiementAnnule.getId());
				detailPaiementAnnule = new PaymentBSCSDetail();
				detailPaiementAnnule.setId(detailPaiementId);
				detailPaiementAnnule.setNumTransaction(paiementAnnule.getId());
				detailPaiementAnnule.setRefPaiement(paiementAnnule);
				detailPaiementAnnule.setCadaglar(detailPaiement.getCadaglar());
				detailPaiementAnnule.setCadassocxact(detailPaiement
						.getCadassocxact());
				detailPaiementAnnule.setCadexpconvdateExchange(detailPaiement
						.getCadexpconvdateExchange());
				detailPaiementAnnule.setRecVersion(Constantes.VALEUR_1);
				detailPaiementAnnule
						.setPaymentDevise(Constantes.DEVISE_MAROC_DIRHAM);
				detailPaiementAnnule
						.setDocumentDevise(Constantes.DEVISE_MAROC_DIRHAM);
				detailPaiementAnnule
						.setGlDevise(Constantes.DEVISE_MAROC_DIRHAM);
				detailPaiementAnnule.setCadAmtDoc(0 - detailPaiement
						.getCadAmtDoc());
				detailPaiementAnnule.setMontant(0 - detailPaiement
						.getCadAmtDoc());
				detailPaiementAnnule.setCadCurAmtGl(Constantes.VAL_DOUBLE_0);
				detailPaiementAnnule.setCadAmtPay(0 - detailPaiement
						.getCadAmtDoc());
				detailPaiementAnnule.setCadCurAmtPay(Constantes.VAL_DOUBLE_0);
				detailPaiementAnnule.setCadConvDateExchangeGl(new Date());
				detailPaiementAnnule.setCadConvDateExchangeDoc(new Date());
				detailPaiementAnnule.setCadCurAmtDoc(Constantes.VAL_DOUBLE_0);
				paymentBSCSDao.savePaiementDetailBSCS(detailPaiementAnnule);

				// mise à jour du detail paiement original
				detailPaiement.setCadCurAmtPay(Constantes.VAL_DOUBLE_0);
				detailPaiement.setCadCurAmtGl(Constantes.VAL_DOUBLE_0);
				detailPaiement.setCadCurAmtDoc(Constantes.VAL_DOUBLE_0);
				detailPaiement
						.setRecVersion(detailPaiement.getRecVersion() + 1);
				paymentBSCSDao.savePaiementDetailBSCS(detailPaiement);

				// mise a jour du montant ouvert de la facture en plus
				facture.setVersion(facture.getVersion() + 1);
				facture.setMontantOuvert(facture.getMontantOuvert()
						+ detailPaiement.getCadAmtDoc());
				facture.setOpnAmtDoc(facture.getOpnAmtDoc()
						+ detailPaiement.getCadAmtDoc());

				factureDao.saveFacture(facture);
				if (isPayOrginal) {
					double dpMontant = 0;
					if (detailPaiement.getMontant() != null) {
						dpMontant = detailPaiement.getMontant();
					}

					// Mise a jour de la balance Client en cas de payement de
					// facture de type "IN"
					if (facture.getStatut().equalsIgnoreCase(
							Constantes.FACTURE_STATUS_IN)) {
						clientDao.updateBalanceClient(paiement.getRefClient()
								.getId(), 0 - Math.abs(dpMontant));
					}

					if (facture.getStatut().equalsIgnoreCase(
							Constantes.FACTURE_STATUS_CO)) {
						// Mise a jour de la balance Client en cas de payement
						// de facture de type "CO"
						clientDao.updateBalanceClient(paiement.getRefClient()
								.getId(), 0 - Math.abs(dpMontant));
						/*
						 * Pour tous les paiements liés à l'avoir autre que le
						 * paiement à annuler 1) Annuler les factures
						 * correspondantes au paiement 2) Inserer un paiement
						 * fictif (pour annuler) 3) Pour toutes les factures
						 * allouées du paiement courant 4) Allouer ce paiement
						 * en négatif sur les factures 5) Mettre à jour le
						 * montant courant de l'ancienne allocation 6) Mettre à
						 * jour le montant ouvert des factures
						 */
						listPaymentBSCSDetail = paymentBSCSDao
								.getDetailPaymentByFacture(facture.getId());
						if (listPaymentBSCSDetail != null) {
							for (PaymentBSCSDetail pDetail : listPaymentBSCSDetail) {
								paiementAnnule = pDetail.getRefPaiement();
								if (!paiementAnnule.getId().equals(
										paiement.getId())) {
									annulerPaiementBSCS(
											username,
											paiementAnnule,
											motif
													+ Constantes.CAREM_ANNULATION_SUFFIX,
											false);
								}
							}
						}
					}
				}
			}
		}

	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.paiement.service.PaiementService#annulerAvance(java.lang.String,
	 * java.lang.Integer, java.lang.String).
	 * 
	 * @param username
	 * @param numCommande
	 * @param motif
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public Boolean annulerAvance(String username, String numCommande,
			String motif) throws TechnicalException, FunctionalException {

		// Recuperer le paiement au niveau de GE
		PaymentGE paymentGE = paymentGEDao.getPaymentByNumCommande(numCommande);

		if (paymentGE == null) {
			throw new FunctionalException(Constantes.ERR_CODE_03,
					Constantes.ERR_AUCUNE_AVANCE_TROUVE);
		}
		if (Constantes.STATUS_PAIEMENT_REJETE.equals(paymentGE.getStatus())
				|| Constantes.STATUS_PAIEMENT_ANNULE.equals(paymentGE
						.getStatus())) {
			throw new FunctionalException(Constantes.ERR_CODE_02,
					Constantes.ERR_AVANCE_ANNULE);
		}

		// Modifier le status du paiement au niveau de GE
		paymentGE.setStatus(Constantes.STATUS_PAIEMENT_REJETE);
		paymentGEDao.savePaymentGE(paymentGE);
		// Mise à jour du status de paiement avec le nouveau status
		paymentGEDao.updatePaymentStatus(paymentGE,
				Constantes.STATUS_PAIEMENT_REJETE, username, 2);

		for (PaymentGEDetail pDetailsGE : paymentGE.getSemaOpPayDetails()) {
			// Recuperer le paiement à annuler
			PaymentBSCS paiement = paymentBSCSDao.getPaiementByID(pDetailsGE
					.getCaxact());
			if (paiement.getLignesPaiement() == null
					|| paiement.getLignesPaiement().isEmpty()) {
				throw new FunctionalException(Constantes.ERR_CODE_03,
						Constantes.ERR_AUCUNE_AVANCE_TROUVE);
			}

			// Annuler le paiement dans BSCS
			annulerPaiementBSCS(username, paiement, motif, true);
		}

		return Boolean.TRUE;
	}

}
