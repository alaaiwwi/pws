package ma.iam.payment.dto.converter;

import ma.iam.payment.dto.PaiementDto;
import ma.iam.services.payment.rq.PaymentRQ;

/**
 * This class represent .
 * <br/><br/>
 * @author mei
 *
 */
public class PayerAvanceDtoConv {
	
	/**
	 * Crée un objet de DTO .</br></br>
	 * @param costId
	 * @param locationId
	 * @return PaiementDto
	 */
	public static PaiementDto creerPaiementDto(PaymentRQ paymentRQ, Integer costId, Integer locationId) {
		PaiementDto paiementDto = new PaiementDto();
		paiementDto.setCodeClient(paymentRQ.getCodeClient());
		paiementDto.setCostCenterId(costId);
		paiementDto.setModePaiement(paymentRQ.getModePaiement());
		paiementDto.setMontant(paymentRQ.getMontant());
		paiementDto.setNumAppel(paymentRQ.getNumAppel());
		paiementDto.setNumCommande(paymentRQ.getNumCommande());
		paiementDto.setRefPaiement(paymentRQ.getRefPaiement());
		paiementDto.setTypeTrx(paymentRQ.getTypeAvance());
		paiementDto.setUserLocationId(locationId);
		paiementDto.setUtilisateur(paymentRQ.getUtilisateur());
		return paiementDto;
	}
	

}
