package ma.iam.payment.security;

import java.io.IOException;
import java.util.logging.Logger;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.web.context.ContextManager;
import ma.iam.services.authentication.Constants;
import ma.iam.services.authentication.UserDetails;

import org.apache.cxf.ws.security.tokenstore.SecurityToken;
import org.apache.cxf.ws.security.tokenstore.TokenStore;
import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.message.token.SecurityTokenReference;
import org.springframework.security.core.userdetails.UserCache;

public class CustomCallbackHandler implements CallbackHandler {

	private CallbackHandler internal;
	private TokenStore store;
	private static final Logger LOGGER = Logger
			.getLogger(CustomCallbackHandler.class.getName());

	public CustomCallbackHandler(CallbackHandler in, TokenStore st) {
		internal = in;
		store = st;
	}

	public CustomCallbackHandler() {
		super();
	}

	private AuthenticationManager authenticationManager;

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(
			AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {

		for (int i = 0; i < callbacks.length; i++) {
			WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];

			String id = pc.getIdentifier();
			String password = "";
			if (SecurityTokenReference.ENC_KEY_SHA1_URI.equals(pc.getType())
					|| WSConstants.WSS_KRB_KI_VALUE_TYPE.equals(pc.getType())) {
				for (String tokenIdentifier : store.getTokenIdentifiers()) {

					SecurityToken token = store.getToken(tokenIdentifier);
					if (id.equals(token.getSHA1())) {
						pc.setKey(token.getSecret());
						return;
					}
				}
			} else {
				SecurityToken tok = store.getToken(id);
				if (tok != null) {
					pc.setKey(tok.getSecret());
					pc.setCustomToken(tok.getToken());
					return;
				}
			}
			try {
				UserCache userCache = (UserCache) ContextManager.getInstance()
						.getBean(Constants.BEAN_USER_CACHE);

				UserDetails userDetails = (UserDetails) authenticationManager
						.authenticate(id, password);
				userCache.putUserInCache(userDetails);
			} catch (FunctionalException e) {
				LOGGER.warning(e.getMessage());
			} catch (TechnicalException e) {
				LOGGER.warning(e.getMessage());
			}

		}
		if (internal != null) {
			internal.handle(callbacks);
		}
	}

}
