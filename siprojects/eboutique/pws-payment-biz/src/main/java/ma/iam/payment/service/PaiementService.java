/*
 * Copyright Atos.  All rights reserved.
 */
package ma.iam.payment.service;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.dto.PaiementDto;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public interface PaiementService {
	
	Long payerAvance(PaiementDto paiementDto) throws TechnicalException, FunctionalException;
	
	Boolean annulerAvance(String username, String numCommande, String motif) throws TechnicalException, FunctionalException;

}
