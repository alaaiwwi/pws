/*
 * Copyright Atos.
 */
package ma.iam.payment.service.impl;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.bscs.UserDao;
import ma.iam.payment.dao.ge.LocationDao;
import ma.iam.payment.domaine.bscs.User;
import ma.iam.payment.domaine.ge.Location;
import ma.iam.payment.dto.UserDto;
import ma.iam.payment.service.UserService;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class UserServiceImpl implements UserService {
	
	private  UserDao userDao = null;
	
	private LocationDao locationDao = null;

	/**
	 * The setter method for the field userDao.
	 * @param userDao the userDao to set.
	 */
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}


	/**
	 * The setter method for the field locationDao.
	 * @param locationDao the locationDao to set.
	 */
	public void setLocationDao(LocationDao locationDao) {
		this.locationDao = locationDao;
	}


	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.service.UserService#getUserByLogin(java.lang.String).
	 * @param login
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException 
	 */
	public UserDto getUserByLogin(String login) throws TechnicalException, FunctionalException {
		User user = userDao.getUserByLogin(login);
		if (user == null) {
			throw new FunctionalException(Constantes.ERR_CODE_04, Constantes.ERR_AUTHORISATION);
		}
		Location location = locationDao.getLocationByCostId(user.getCostCenter());
		return new UserDto(login, user.getCostCenter(), (location != null ?  location.getId(): null));
	}

}
