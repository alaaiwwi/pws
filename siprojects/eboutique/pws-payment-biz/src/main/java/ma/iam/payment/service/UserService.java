/*
 * Copyright Atos.
 */
package ma.iam.payment.service;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.dto.UserDto;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public interface UserService {
	
	/**
	 * @param login username
	 * @return User
	 * @throws TechnicalException
	 * @throws FunctionalException 
	 */
	public UserDto getUserByLogin(String login) throws TechnicalException, FunctionalException;

}
