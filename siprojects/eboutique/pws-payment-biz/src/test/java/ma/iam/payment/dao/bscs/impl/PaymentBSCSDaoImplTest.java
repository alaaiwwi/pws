package ma.iam.payment.dao.bscs.impl;

import java.util.Date;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.bscs.PaymentBSCSDao;
import ma.iam.payment.domaine.bscs.Client;
import ma.iam.payment.domaine.bscs.PaymentBSCS;
import ma.iam.payment.domaine.bscs.PaymentBSCSDetail;
import ma.iam.payment.domaine.bscs.Ticket;

public class PaymentBSCSDaoImplTest extends DbUnitTestCase {

	private PaymentBSCSDao paymentBscsDao = (PaymentBSCSDao) contextManager.getBean("paymentBSCSDao");
	
	public void setPaymentBscsDao(PaymentBSCSDao paymentBscsDao) {
		this.paymentBscsDao = paymentBscsDao;
	}
	
	public void testSaveMemoBSCS() throws TechnicalException {
		Ticket ticket = new Ticket();
		paymentBscsDao.saveMemoBSCS(ticket);
	}
	
	public void testSavePaiementDetailBSCS() throws TechnicalException {
		PaymentBSCSDetail paymentBSCSDetail = new PaymentBSCSDetail();
		paymentBscsDao.savePaiementDetailBSCS(paymentBSCSDetail);
	}
	
	public void testGetPaiementByID() throws TechnicalException {
		Long id = null;
		paymentBscsDao.getPaiementByID(id);
	}
	
	public void testSavePaiement() throws TechnicalException {
		Client client = null;
		Date trxDate = null;
		String chkUm = null;
		String glCash = null;
		String glDis = null;
		Long caType = null;
		String carem = null;
		String cAPP = null;
		String causerName = null;
		String debitInfo1 = null;
		Integer costCenter = null;
		Long convrateTypeGl = null;
		Long convrateTypeDoc = null;
		Double chkAmtGl = null;
		Double disAmtGl = null;
		Double curAmtGl = null;
		Double chkAmtPay = null;
		Double disAmtPay = null;
		Double curAmtPay = null;
		Double balanceHome = null;
		Long codeRaison = null;
		paymentBscsDao.savePaiement(client, trxDate, chkUm, glCash, glDis, caType, carem, cAPP, 
				causerName, debitInfo1, costCenter, convrateTypeGl, convrateTypeDoc, chkAmtGl, 
				disAmtGl, curAmtGl, chkAmtPay, disAmtPay, curAmtPay, balanceHome, codeRaison);
	}
	
	public void testGetDetailPaymentByFacture() throws TechnicalException {
		PaymentBSCS paiement = new PaymentBSCS();
		paymentBscsDao.savePaiement(paiement );
	}
}
