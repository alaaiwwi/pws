package ma.iam.payment.dao.bscs.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.bscs.FactureDao;
import ma.iam.payment.domaine.bscs.Facture;

public class FactureDaoImplTest extends DbUnitTestCase {
	
	FactureDao factureDao = (FactureDao) contextManager.getBean("factureDao");

	public void testSaveFacture() throws TechnicalException {
		Facture facture = new Facture();
		factureDao.saveFacture(facture);
	}

	public void testGetPeriodeFacture() throws TechnicalException {
		String resultat = factureDao.getPeriodeFacture();
		assertNotNull("L'objet est null", resultat);
	}

	public void testGetFactureSeq() throws TechnicalException {
		Long resultat = factureDao.getFactureSeq();
		assertNotNull("L'objet est null", resultat);
	}

}
