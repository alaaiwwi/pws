package ma.iam.payment;

import java.io.File;
import java.net.URL;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.web.context.ContextManager;

import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.util.ClassUtils;

public abstract class DbUnitTestCase extends DatabaseTestCase {
	
	/** The Constant DATA_SOURCE_BSCS_FIX. */
    protected static final String DATA_SOURCE_BSCS = "dataSourceBSCS";
    
	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager.getLogger(DbUnitTestCase.class.getName());
	
	/** The context manager. **/
	protected static ContextManager contextManager = null;
	
	/** The data source bscs fix. */
	protected static org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy dataSourceBSCS = null;
	
    /** The database connection bscs fix. */
    private static IDatabaseConnection databaseConnectionBSCS = null;
    
    /** The data set bscs fix. */
    private IDataSet dataSetBSCS = null;
    
	static {
		if (contextManager == null) {
			try {
				ContextManager.init(new FileSystemXmlApplicationContext("src/test/resources/spring/test-applicationContext.xml"));
				contextManager = ContextManager.getInstance();
			} catch (TechnicalException e) {
				LOGGER.fatal(e.getMessage(), e);
			}
		}
	}
	
	@Override
	protected final DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.REFRESH;
	}
	
	@Override
	protected final DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	protected IDatabaseConnection getConnection() throws Exception {
		if (dataSourceBSCS == null) {
            dataSourceBSCS = (org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy) contextManager.getBean(DATA_SOURCE_BSCS);
	        databaseConnectionBSCS = new DatabaseConnection(DataSourceUtils.getConnection(dataSourceBSCS), 
	        		((BasicDataSource) dataSourceBSCS.getTargetDataSource()).getUsername().toUpperCase());
	        databaseConnectionBSCS.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
        }
        return databaseConnectionBSCS;
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return dataSetBSCS;
	}
	
	@Override
	protected final void setUp() throws Exception {
	    String fichierDataBSCS = ClassUtils.getShortName(this.getClass()) + "-BSCS-Data.xml";
	    URL urlBSCS = ClassLoader.getSystemResource(fichierDataBSCS);
	    if (urlBSCS != null) {
	    	LOGGER.info("Chargement du contexte initial " + fichierDataBSCS);
	        databaseConnectionBSCS = getConnection();
	        dataSetBSCS = new FlatXmlDataSetBuilder().build(new File(urlBSCS.getFile()));	
	        getSetUpOperation().execute(databaseConnectionBSCS, dataSetBSCS);
	    }
	}
	
	@Override
    protected final void tearDown() throws Exception {
		URL urlBSCS = ClassLoader.getSystemResource("AllTables-BSCS-Data.xml");
    	if (urlBSCS != null) {
	    	DatabaseOperation.DELETE_ALL.execute(getConnection(), new FlatXmlDataSetBuilder().build(new File(urlBSCS.getFile())));
    	}
        super.tearDown();
    }
}