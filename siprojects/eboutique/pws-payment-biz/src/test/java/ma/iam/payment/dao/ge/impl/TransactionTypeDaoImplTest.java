package ma.iam.payment.dao.ge.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.ge.TransactionTypeDao;
import ma.iam.payment.domaine.ge.TransactionType;

public class TransactionTypeDaoImplTest extends DbUnitTestCase {
	
	private TransactionTypeDao transactionTypeDao = (TransactionTypeDao) contextManager.getBean("transactionTypeDao"); 
	
	public void setTransactionTypeDao(TransactionTypeDao transactionTypeDao) {
		this.transactionTypeDao = transactionTypeDao;
	}
	
	public void testGetTransactionTypeById() throws TechnicalException {
		TransactionType transactionType =  transactionTypeDao.getTransactionTypeById(1);
		assertNotNull("L'objet est null", transactionType);
	}

}
