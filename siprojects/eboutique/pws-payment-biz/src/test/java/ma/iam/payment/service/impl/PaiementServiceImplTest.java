package ma.iam.payment.service.impl;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dto.PaiementDto;
import ma.iam.payment.service.PaiementService;

public class PaiementServiceImplTest extends DbUnitTestCase {
	
	private PaiementService paiementService = (PaiementService) contextManager.getBean("");
	
	public void setPaiementService(PaiementService paiementService) {
		this.paiementService = paiementService;
	}
	
	public PaiementService getPaiementService() {
		return this.paiementService;
	}
	
	public void testPayerAvance() throws TechnicalException, FunctionalException {
		PaiementDto paiementDto = new PaiementDto();
		assertNotNull("Le resultat est null", getPaiementService().payerAvance(paiementDto));
	}
	
	public void testAnnulerPaiementBSCS() throws TechnicalException, FunctionalException {
		assertNotNull("Le resultat est null", getPaiementService().annulerAvance("", "1", ""));
	}
}
