package ma.iam.payment.dao.ge.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.ge.PaymentModeDao;
import ma.iam.payment.domaine.ge.PaymentMode;

public class PaymentModeDaoImplTest extends DbUnitTestCase {
	
	private PaymentModeDao paymentModeDao = (PaymentModeDao) contextManager.getBean("paymentModeDao");
	
	public void setPaymentModeDao(PaymentModeDao paymentModeDao) {
		this.paymentModeDao = paymentModeDao;
	}
	
	public void testGetPaymentModeById() throws TechnicalException {
		PaymentMode paymentMode = paymentModeDao.getPaymentModeById(1);
		assertNotNull("L'objet est null", paymentMode);
	}

}
