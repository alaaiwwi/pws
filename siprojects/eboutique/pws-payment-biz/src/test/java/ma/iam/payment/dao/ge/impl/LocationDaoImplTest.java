package ma.iam.payment.dao.ge.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.ge.LocationDao;

public class LocationDaoImplTest extends DbUnitTestCase {
	
	private LocationDao locationDao = (LocationDao) contextManager.getBean("locationDao");
	
	public void setLocationDao(LocationDao locationDao) {
		this.locationDao = locationDao;
	}
	
	public LocationDao getLocationDao() {
		return this.locationDao;
	}
	
	public void testGetLocationByCostId() throws TechnicalException {
		LocationDao locationDao = new LocationDaoImpl(); 
		locationDao = (LocationDao) locationDao.getLocationByCostId(1);
	}

}
