package ma.iam.payment.service.impl;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dto.UserDto;
import ma.iam.payment.service.UserService;

public class UserServiceImplTest extends DbUnitTestCase {

	private UserService userService = (UserService) contextManager.getBean("userService");
	
	public UserService getUserService() {
		return this.userService;
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public void testGetUserByLogin() throws TechnicalException, FunctionalException {
		UserDto userDto = getUserService().getUserByLogin("");
		assertNotNull("L'objet est null", userDto);
	}
}
