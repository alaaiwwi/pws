package ma.iam.payment.dao.bscs.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.bscs.UserDao;
import ma.iam.payment.domaine.bscs.User;

public class UserDaoImplTest extends DbUnitTestCase {

	private UserDao userDao = (UserDao) contextManager.getBean("userDao");

	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
			
	public void testGetUserByLogin() throws TechnicalException {
		User user = userDao.getUserByLogin("");
		assertNotNull("L'objet est null", user);
	}
	
}
