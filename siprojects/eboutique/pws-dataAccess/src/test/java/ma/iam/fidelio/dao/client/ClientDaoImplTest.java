package ma.iam.fidelio.dao.client;

import java.util.List;

import ma.iam.common.DaoBaseTestCase;
import ma.iam.fidelio.domaine.data.bscs.FidelioClient;
import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.junit.Before;
import org.junit.Test;

public class ClientDaoImplTest extends DaoBaseTestCase {

	private ClientDao dao;

	@Before
	public void setUp() throws Exception {

		super.setUp();
		dao = (ClientDao) getBean("fidelioClientDao");
	}

	@Test
	public void testGetAllNumAppels() throws NumberFormatException,
			TechnicalException {

		List<String> list = dao.getAllNumAppels(Integer
				.parseInt(getProperties().getProperty("clientdao.idclient")));
		assertNotNull((list.isEmpty()) ? null : list);
		list = dao.getAllNumAppels(Integer.parseInt(getProperties()
				.getProperty("clientdao.idclient.bad")));
		assertNull((list.isEmpty()) ? null : list);
	}

	@Test
	public void testGetContratByND() throws TechnicalException {
		FidelioContrat bean = dao.getContratByND(getProperties().getProperty(
				"clientdao.numeroappel"));

		assertNotNull(bean);

		bean = dao.getContratByND(getProperties().getProperty(
				"clientdao.numeroappel.bad"));
		assertNull(bean);
	}

	@Test
	public void testGetListContratByNumAppel() throws TechnicalException {
		List<Long> list = dao.getListContratByNumAppel(getProperties()
				.getProperty("clientdao.numeroappel"));

		assertNotNull((list.isEmpty()) ? null : list);

		list = dao.getListContratByNumAppel(getProperties().getProperty(
				"clientdao.numeroappel.bad"));
		assertNull((list.isEmpty()) ? null : list);
	}

	@Test
	public void testGetTotalImpaye() throws TechnicalException {
		Float total = dao.getTotalImpaye(((Integer.parseInt(getProperties()
				.getProperty("clientdao.idclient")))));

		assertNotNull(total);

	}

	@Test
	public void testGetClientByCode() throws TechnicalException {
		FidelioClient bean = dao.getClientByCode(getProperties().getProperty(
				"clientdao.customeid"));

		assertNotNull(bean);

		bean = dao.getClientByCode(getProperties().getProperty(
				"clientdao.customeid.bad"));
		assertNull(bean);
	}

}
