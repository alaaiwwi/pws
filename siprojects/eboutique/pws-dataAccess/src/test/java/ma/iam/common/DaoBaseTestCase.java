package ma.iam.common;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import ma.iam.fwk.test.BasicTestCase;

public abstract class DaoBaseTestCase extends BasicTestCase {


	@Override
	protected void setUp() throws Exception {
		super.setUp();
		properties = (Properties) getBean("junitDataAccessProperties");
	}

	@Override
	protected List<String> getSpringContextUri() {
		return Arrays.asList("test-applicationContext.xml");
	}

}
