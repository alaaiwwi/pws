package ma.iam.fidelio.dao.parametrage.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.AbstractDao;
import ma.iam.fidelio.dao.parametrage.RechercheParametreDao;
import ma.iam.fidelio.domaine.data.fidelio.StatutAffaire;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.BonusEngagement;
import ma.iam.fidelio.domaine.data.parametrage.CategorieClient;
import ma.iam.fidelio.domaine.data.parametrage.ConversionMonsuelle;
import ma.iam.fidelio.domaine.data.parametrage.DelaiPremiereConversion;
import ma.iam.fidelio.domaine.data.parametrage.Exclusion;
import ma.iam.fidelio.domaine.data.parametrage.ModeEngagement;
import ma.iam.fidelio.domaine.data.parametrage.Parametre;
import ma.iam.fidelio.domaine.data.parametrage.PointsEnCoursConversion;
import ma.iam.fidelio.domaine.data.parametrage.Qualite;
import ma.iam.fidelio.domaine.data.parametrage.SeuilImpaye;
import ma.iam.fidelio.domaine.data.parametrage.SeuilMinConversion;
import ma.iam.fidelio.domaine.data.parametrage.SeuilMinPrixPack;
import ma.iam.fidelio.domaine.data.parametrage.SousCategorie;
import ma.iam.fidelio.domaine.data.parametrage.TarifAchatPoint;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

public class RechercheParametreDaoImpl extends AbstractDao implements RechercheParametreDao {
	
	/** Instance du LOGGER */
	private static final Logger TRACE_LOGGER = LogManager.getLogger(Constantes.TRACE_FIDELIO_LOGGER_NAME);

	/**
	 * Méthode getArticleByCode : Permet de récupérer un objet de typt Article 
	 * à partir d'un code.
	 * @param : codeArticle
	 * @return : Article
	 * @throws : TechnicalException
	 */
	public Article getArticleByCode(String codeArticle) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(Article.class);
		criteria.add(Expression.eq(Article.CODE_PRODUIT, codeArticle));
		return (Article) criteria.uniqueResult();
	}
	
	/**
	 * Méthode getArticleById : Permet de récupérer un objet de type Article 
	 * à partir d'un id.
	 * @param : idArticle
	 * @return : Article
	 * @throws : TechnicalException
	 */
	public Article getArticleById(Integer idArticle) throws TechnicalException {
		return (Article) getPersistenceManagerFidelio().findById(Article.class, idArticle, false);
	}

	/**
	 * Permet de de récupérer le bonus de l'engagement
	 * à partir d'un id Categorie Client et Id Mode Engagement.
	 * @param : modeEngagementId
	 * @param : categorieClientId
	 * @return : Integer
	 * @throws : TechnicalException
	 */
	public Integer getBonusEngagement(Integer categorieClientId, Integer modeEngagementId) throws TechnicalException {
		BonusEngagement bonusEngagement = null;
		
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(BonusEngagement.class);
		criteria.add(Expression.le(BonusEngagement.DATE_DEBUT, Calendar.getInstance().getTime()));
		criteria.createCriteria(BonusEngagement.REF_CAT_CLIENT).add(Expression.eq(BonusEngagement.ID, categorieClientId));
		criteria.createCriteria(BonusEngagement.REF_MODE_ENGAGEMENT).add(Expression.eq(BonusEngagement.ID, modeEngagementId));
		criteria.addOrder(Order.desc(BonusEngagement.DATE_DEBUT));
		List<BonusEngagement> result = criteria.list();
		bonusEngagement = !result.isEmpty() ? result.get(0) : null;
		return (bonusEngagement != null ? bonusEngagement.getBonusEngagement() : 0);
	}
	
	/**
	 * Méthode getConversionMonsuelle : Permet de récupérer un objet de type 
	 * ConversionMonsuelle à partir d'un categorieClientId.
	 * @param : categorieClientId
	 * @return : ConversionMonsuelle
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public ConversionMonsuelle getConversionMonsuelle(Integer categorieClientId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(ConversionMonsuelle.class);
		criteria.add(Expression.le(ConversionMonsuelle.DATE_DEBUT, new Date()));
		criteria.createCriteria(ConversionMonsuelle.REF_CAT_CLIENT).add(Expression.eq(CategorieClient.ID, categorieClientId));
		criteria.addOrder(Order.desc(ConversionMonsuelle.DATE_DEBUT));
		List<ConversionMonsuelle> result = criteria.list();
		return !result.isEmpty() ? result.get(0) : null;
	}
	
	/**
	 * Méthode getDelaiPremiereConversion : Permet de récupérer un objet de type 
	 * DelaiPremiereConversion à partir d'un categorieClientId.
	 * @param : categorieClientId
	 * @return : DelaiPremiereConversion
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public DelaiPremiereConversion getDelaiPremiereConversion(Integer categorieClientId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(DelaiPremiereConversion.class);
		criteria.add(Expression.le(DelaiPremiereConversion.DATE_DEBUT, new Date()));
		criteria.createCriteria(DelaiPremiereConversion.REF_CAT_CLIENT).add(Expression.eq(CategorieClient.ID, categorieClientId));
		criteria.addOrder(Order.desc(DelaiPremiereConversion.DATE_DEBUT));
		List<DelaiPremiereConversion> result = criteria.list();
		return !result.isEmpty() ? result.get(0) : null;
	}
	
	/**
	 * Méthode getModeEngagementByBscsId : Permet de récupérer un objet de type 
	 * ModeEngagement à partir d'un engagementId.
	 * @param : engagementId
	 * @return : ModeEngagement
	 * @throws : TechnicalException
	 */
	public ModeEngagement getModeEngagementByBscsId(Integer engagementId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(ModeEngagement.class);
		criteria.add(Expression.eq(ModeEngagement.ENGAGEMENT_BSCS_ID, engagementId));
		return (ModeEngagement) criteria.uniqueResult();
	}
	
	/**
	 * Méthode getModeEngagementById : Permet de récupérer un objet de type 
	 * ModeEngagement à partir d'un engagementId.
	 * @param : engagementId
	 * @return : ModeEngagement
	 * @throws : TechnicalException
	 */
	public ModeEngagement getModeEngagementById(Integer engagementId) throws TechnicalException {
		return (ModeEngagement) getPersistenceManagerFidelio().findById(ModeEngagement.class, engagementId, false);
	}
	
	/**
	 * Méthode getPrixPackMinimalPourRemise : Permet de récupérer le prixPackMinimalPourRemise
	 * à partir d'un categorieId.
	 * @param : categorieId
	 * @return : Float
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Float getPrixPackMinimalPourRemise(Integer categoryId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(SeuilMinPrixPack.class);
		criteria.add(Expression.le(SeuilMinPrixPack.DATE_DEBUT, Calendar.getInstance().getTime()));
		criteria.createCriteria(SeuilMinPrixPack.REF_CAT_CLIENT).add(Expression.eq(SeuilMinPrixPack.ID, categoryId));
		criteria.addOrder(Order.desc(SeuilMinPrixPack.DATE_DEBUT));
		List<SeuilMinPrixPack> result = criteria.list();
		SeuilMinPrixPack  seuilMin = null;
		
		if (result != null && !result.isEmpty()) {
			seuilMin = result.get(0);
		}
		
		return (seuilMin != null) ? seuilMin.getSeuilPrix() : 0;
	}
	
	/**
	 * Méthode getPrixPackXMois : Recuperer le prix d'un pack pour un egagement de X mois.
	 * @param : planTarifaireId
	 * @param : codeProduit
	 * @param : nbreMois
	 * @return : Float
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Float getPrixPackXMois(Integer planTarifaireId, String codeProduit, Integer nbreMois) throws TechnicalException {
		Float result = 0f;
		Hashtable params = new Hashtable();
		params.put("planTarifaireId", planTarifaireId);
		params.put("codeProduit", codeProduit);
		params.put("nbreMois", nbreMois.toString()+"MPA-");
		List<Float> obj = getPersistenceManagerBSCS().getNamedQuery(Constantes.REQ_PRIX_PACK_XMOIS, params);
		if (!obj.isEmpty()) {
			result = obj.get(0);
		}
		return result;
	}
	
	/**
	 * Permet de retourner les prix du poste pour tous les modes engagement et sans engagement
	 * @param codeProduit
	 * @return : Map
	 * @throws : TechnicalException 
	 */
	public Map<String, Float> getPrixPoste(String codeProduit) throws TechnicalException {
		Map<String, Float> resultat = new HashMap<String, Float>();
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		params.put("codeProduit", codeProduit);
		List<Object[]> list = getPersistenceManagerBSCS().getNamedQuery(Constantes.REQ_PRIX_POSTE, params);
		if (list != null) {
			for (Object[] line : list) {
				resultat.put((String) line[0], (Float) line[1]);
				
			}
		}
		return resultat;
	}		
	
	/**
	 * Méthode getPrixRachatPoint : Récupérer le parametre de rachat d'un point.
	 * @return : Double
	 * @throws : TechnicalException 
	 */
	@SuppressWarnings("unchecked")
	public Double getPrixRachatPointByModeEngagement(Integer idModeEngagement) throws TechnicalException {
		Double prixRachatPoint = 0d;
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(TarifAchatPoint.class);
		criteria.add(Expression.le(Parametre.DATE_DEBUT, Calendar.getInstance().getTime()));
		criteria.add(Expression.eq(TarifAchatPoint.REF_MODE_ENGAGEMENT+"."+ModeEngagement.ID, idModeEngagement));
		criteria.addOrder(Order.desc(Parametre.DATE_DEBUT));
		List<TarifAchatPoint> result = criteria.list();
		if (result != null && !result.isEmpty()) {
			prixRachatPoint = result.get(0).getTarifAchatPt();
		}
		return prixRachatPoint;
	}
	
	/**
	 * Méthode getRemisePackPrivilege : Récupérer la remise sur pack privilige.
	 * @param : codeClient
	 * @param : numAppel
	 * @param : prixPack
	 * @param : seuilPrix
	 * @param : remisePrivilege
	 * @return : Float
	 * @throws : TechnicalException
	 */
	public Float getRemisePackPrivilege(String codeClient, String numAppel, Float prixPack, Float seuilPrix, Float remisePrivilege)
			throws TechnicalException {

		String paramsPassed = "\nParamétre de complement d'argent\n" + "\ncodeClient : " + codeClient + "\nnumAppel : " + numAppel + "\nprixPack : "
				+ prixPack + "\nseuilPrix : " + seuilPrix + "\nremisePrivilege : " + remisePrivilege;
		TRACE_LOGGER.debug(paramsPassed);

		final String OUI = "O";
		final String NON = "N";

		String entreprise;
		String privilege;
		Float result = 0f;

		CallableStatement callableStatement = null;
		try {
			TRACE_LOGGER.debug(" Execution de la fonction  SEMA_INT_BSCS_FIDELIO.RECUP_E_P" + result);
			Connection connection = getPersistenceManagerBSCS().getConnection();
			callableStatement = connection.prepareCall("{ ? =  call SEMA_INT_BSCS_FIDELIO.RECUP_E_P(?,?,?,?,?) }");
			callableStatement.registerOutParameter(1, Types.DOUBLE);
			callableStatement.setString(2, codeClient);
			callableStatement.setString(3, numAppel);
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.registerOutParameter(6, Types.NUMERIC);
			callableStatement.executeQuery();
			entreprise = callableStatement.getString(4);
			privilege = callableStatement.getString(5);
			callableStatement.clearParameters();

			TRACE_LOGGER.debug(" Client entreprise : " + entreprise);
			TRACE_LOGGER.debug(getClass(), "getRemisePackPrivilege",
					" Aprés l'évolution 2570 : le résulta retournée pour client entreprise est toujour non " + privilege);
			TRACE_LOGGER.debug(" Client privilege : " + privilege);
			// client non entreprise et contrat privilige
			//
			if (NON.equals(entreprise) && OUI.equals(privilege)) {			
				TRACE_LOGGER.debug(" Contrat privilege ");
				// result = prixPack - remisePrivilege; //asoufi
				// result = remisePrivilege;// Mise a jour le 19-02-2009 suite anomalie 3256

				// Le prix de poste de pack GSM est strictement sépirieur au seuil de la remise
				// Une remise de 300 (Variable) va être appliqué

				if (prixPack > seuilPrix) {
					TRACE_LOGGER.debug(" prixPack > seuilPrix : Remise Privilege accordee = " + remisePrivilege);
					result = remisePrivilege;
					// Sinon la remise va être le prix de poste
				} else {
					TRACE_LOGGER.debug(" prixPack <= seuilPrix : Remise Privilege accordee = " + prixPack);
					result = prixPack;
				}

			}

			Float resultReturn = result > 0 ? result : 0f;
			TRACE_LOGGER.debug(" return : " + resultReturn);
			return resultReturn;
		} catch (SQLException e) {
			throw new TechnicalException(e.getMessage(), e);
		} finally {
			if (callableStatement != null) {
				try {
					callableStatement.close();
				} catch (SQLException e) {
					TRACE_LOGGER.error(e.getMessage(), e);
					callableStatement = null;
				}
			}
		}
	}
	
	/**
	 * Méthode getSeuilImpaye : Permet de récupérer un objet de type 
	 * SeuilImpaye à partir d'un categorieClientId.
	 * @param : categorieClientId
	 * @return : SeuilImpaye
	 * @throws : TechnicalException
	 */	
	@SuppressWarnings("unchecked")
	public SeuilImpaye getSeuilImpaye(Integer categorieClientId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(SeuilImpaye.class);
		criteria.add(Expression.le(SeuilImpaye.DATE_DEBUT, new Date()));
		criteria.createCriteria(SeuilImpaye.REF_CAT_CLIENT).add(Expression.eq(CategorieClient.ID, categorieClientId));
		criteria.addOrder(Order.desc(SeuilImpaye.DATE_DEBUT));
		List<SeuilImpaye> result = criteria.list();
		return !result.isEmpty() ? result.get(0) : null;
	}
	
	/**
	 * Méthode getSeuilMinimalEngagement : Permet de la seuil Minimal Engagement 
	 * à partir d'un id Mode Engagement.
	 * @param : idModeEngagement
	 * @return : Double
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Double getSeuilMinimalEngagement(Integer idModeEngagement, Integer categorieClientId)
			throws TechnicalException {
		double seuilMin = 0.0;
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(SeuilMinConversion.class);
		criteria.add(Expression.le(SeuilMinConversion.DATE_DEBUT, Calendar.getInstance().getTime()));
		criteria.createCriteria(SeuilMinConversion.MODE_ENG).add(Expression.eq(ModeEngagement.ID, idModeEngagement));
		criteria.createCriteria(SeuilMinConversion.REF_CAT_CLIENT).add(Expression.eq(CategorieClient.ID, categorieClientId));
		criteria.addOrder(Order.desc(SeuilMinConversion.DATE_DEBUT));
		List<SeuilMinConversion> result = criteria.list();
		if (result != null && !result.isEmpty()) {
			seuilMin = result.get(0).getSeuilMinConv().doubleValue()/100;
		}
		return seuilMin;
	}
	
	/**
	 * Méthode getStatutAffaireByCode : Permet de récupérer un objet de type 
	 * StatutAffaire à partir d'un statusAffaireCree.
	 * @param : statusAffaireCree
	 * @return : StatutAffaire
	 * @throws TechnicalException
	 */
	public StatutAffaire getStatusAffaireByCode(String statusAffaireCree) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(StatutAffaire.class);
		criteria.add(Expression.eq(StatutAffaire.CODE, statusAffaireCree));
		return (StatutAffaire) criteria.uniqueResult();
	}

	/**
	 * Méthode getTauxConversion : Permet de récupérer le Taux Conversion 
	 * à partir d'un id Categorie Client.
	 * @param : categorieClientId
	 * @return : Double
	 * @throws : TechnicalException 
	 */
	@SuppressWarnings("unchecked")
	public Double getTauxConversion(Integer categorieClientId) throws TechnicalException {
		
		double tauxConversion = 0.0;
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(PointsEnCoursConversion.class);
		criteria.add(Expression.le(PointsEnCoursConversion.DATE_DEBUT, Calendar.getInstance().getTime()));
		criteria.createCriteria(PointsEnCoursConversion.REF_CAT_CLIENT).add(Expression.eq(PointsEnCoursConversion.ID, categorieClientId));
		criteria.addOrder(Order.desc(PointsEnCoursConversion.DATE_DEBUT));
		List<PointsEnCoursConversion> result = criteria.list();
		PointsEnCoursConversion conversion = null;
		if (result != null && !result.isEmpty()) {
			conversion = result.get(0);
		}
		
		if (conversion != null) {
			tauxConversion = conversion.getPctConversion().doubleValue() / 100;
		}
		return tauxConversion;
	}

	/**
	 * Méthode isExclusion : Permet de vérifier si l'article est disponible pour le client ou pas.
	 * @param : articleId
	 * @param : qualiteClient
	 * @param : sousCategoryClient
	 * @param : planTarifaireClient
	 * @return : Boolean
	 * @throws TechnicalException
	 */
	public Boolean isExclusion(Integer articleId, Integer qualiteClient,
			Integer sousCategoryClient, Integer planTarifaireClient)
			throws TechnicalException {

			Criteria criteria = getPersistenceManagerFidelio().createCriteria(Exclusion.class);

			criteria.add(Expression.or(Expression.isNull(Exclusion.PLANT_TARIFAIRE), Expression.eq(
					Exclusion.PLANT_TARIFAIRE, planTarifaireClient)));

			criteria.createCriteria(Exclusion.REF_ARTICLE).add(
					Expression.eq(Article.ID, articleId));

			criteria.add(Expression.or(Expression.isNull(Exclusion.DATE_DEBUT),
					Expression.le(Exclusion.DATE_DEBUT, new Date())));

			criteria.add(Expression.or(Expression.isNull(Exclusion.DATE_FIN),
					Expression.ge(Exclusion.DATE_FIN, new Date())));

			criteria.add(Expression.or(Expression
					.isNull(Exclusion.QUALITE_CLIENT), Expression.eq(
					Qualite.ID, qualiteClient)));

			criteria.add(Expression.or(Expression
					.isNull(Exclusion.SOUS_CATEGORIE), Expression.eq(
					SousCategorie.ID, sousCategoryClient)));

			return !(criteria.list().isEmpty());
	}
}