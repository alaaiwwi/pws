package ma.iam.payment.dao.bscs;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.bscs.User;

/**
 * DAO User.
 * <br/><br/>
 * 
 * @author Atos
 *
 */
public interface UserDao {
	
	/**
	 * Rècupere un utilisateur par son login
	 * @param login username
	 * @return User
	 * @throws TechnicalException
	 */
	public User getUserByLogin(String login) throws TechnicalException;

}
