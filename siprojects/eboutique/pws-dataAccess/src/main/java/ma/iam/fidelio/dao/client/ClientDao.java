package ma.iam.fidelio.dao.client;

import java.util.List;

import ma.iam.fidelio.domaine.data.bscs.FidelioClient;
import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface ClientDao {

	/**
	 * Méthode getAllNumAppels : Permet de récupérer les nuémros d'appel
	 * à partir d'un id client.
	 * @param : idClient
	 * @return : List<String>
	 * @throws : TechnicalException
	 */
	List<String> getAllNumAppels(Integer idClient) throws TechnicalException;
	
	/**
	 * Méthode getContratByND : Permet de récupérer un objet de type FidelioContrat
	 * à partir d'un numéro d'appel.
	 * @param : numeroAppel
	 * @return : FidelioContrat
	 * @throws : TechnicalException
	 */
	FidelioContrat getContratByND(String numeroAppel) throws TechnicalException;
	
	/**
	 * Méthode getListContratByNumAppel: Permet de récupérer la liste des contrats liés à un numéro d'appel
	 * à partir d'un numéro d'appel.
	 * @param : numeroAppel
	 * @return : Contrat
	 * @throws : TechnicalException
	 */
	List<Long> getListContratByNumAppel(String numeroAppel) throws TechnicalException;
	
	
	/**
	 * Méthode getTotalImpaye : Permet de retourner le total impaye 
	 * à partir d'un code client.
	 * @param : codeClient
	 * @return : float
	 * @throws : TechnicalException 
	 */
	Float getTotalImpaye(Integer idClient) throws TechnicalException;

	/**
	 * Recherche Fidelioclient par custcode
	 * @param codeClient
	 * @return
	 * @throws TechnicalException
	 */
	FidelioClient getClientByCode(String codeClient) throws TechnicalException;
	
}
