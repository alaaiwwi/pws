package ma.iam.fidelio.dao.compte.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.AbstractDao;
import ma.iam.fidelio.dao.compte.CompteClientDao;
import ma.iam.fidelio.domaine.data.fidelio.Compensation;
import ma.iam.fidelio.domaine.data.fidelio.CompteClient;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.CategorieClient;
import ma.iam.fidelio.domaine.data.parametrage.SousCategorie;
import ma.iam.fidelio.domaine.data.parametrage.TypeArticle;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

public class CompteClientDaoImpl extends AbstractDao implements CompteClientDao {

	/**
	 * Méthode getCommandeEnInstance : Permet de récupérer 
	 * le nombre de commande en instance pour un seul contrat et le type catalogue. 
	 * @param : contratId
	 * @return : nombre de commande en instance.
	 * @throws : TechnicalException 
	 */
	@SuppressWarnings("unchecked")
	public Integer getNombreCommandeEnInstance(Long contratId) throws TechnicalException {
		List<Integer> resultat = null;
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		params.put("coId", contratId);
		resultat = getPersistenceManagerFidelio().getNamedQuery(Constantes.REQ_NOMBRE_COMMANDES_EN_INSTANCE_ENG, params);
		if (resultat != null) {
			return resultat.get(0);
		}
		return -1;
	}
	
	/**
	 * Méthode getCompteClientByCustomerId : Récupère un objet de type CompteClient 
	 * à partir d'un customerId.
	 * @param : customerId
	 * @return : CompteClient
	 * @throws : TechnicalException
	 */
	public CompteClient getCompteClientByCustomerId(Integer customerId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(CompteClient.class);
		criteria.add(Expression.eq(CompteClient.ID_CLIENT, customerId));
		return (CompteClient) criteria.uniqueResult();
	}
	
	/**
	 * Méthode getCreditCompensationsByClient : permet de récupérer une liste de type Compensation 
	 * à partir d'un id du compte client.
	 * @param : compteClientId
	 * @return : List<Compensation>
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public List<Compensation> getCreditCompensationsByClient(Integer compteClientId) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(Compensation.class);
		criteria.createCriteria(Compensation.REF_COMPTE_CLIENT).add(Expression.eq(CompteClient.ID, compteClientId));
		criteria.add(Expression.gt(Compensation.SOLDE_ACQUIS, new Long(0)));
		criteria.add(Expression.isNull(Compensation.DATE_PURGE));
		criteria.createCriteria(Compensation.REF_ARTICLE).createCriteria(Article.REF_TYPE_ARTICLE).add(Expression.eq(TypeArticle.ID, -1));
		criteria.addOrder(Order.asc(Compensation.DATE_TRANSACTION));
		List<Compensation> list = criteria.list();
		return list;
	}
	
	/**
	 * Méthode getListCompensationAannulerByClient : Permet de récupérer une liste des compensation
	 * à annuler à partir d'id Compte Client. 
	 * @param : idCompteClient
	 * @return : Collection<Compensation>
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Collection<Compensation> getListCompensationAannulerByClient(Integer idCompteClient) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(Compensation.class);
		criteria.createCriteria(Compensation.REF_COMPTE_CLIENT).add(Expression.eq(CompteClient.ID, idCompteClient.intValue()));
		criteria.add(Expression.or(Expression.gt(Compensation.SOLDE_CONVERTIS, new Long("0")), Expression.gt(Compensation.SOLDE_EN_COURS, new Long("0"))));
		criteria.add(Expression.isNull(Compensation.DATE_PURGE));
		criteria.createCriteria(Compensation.REF_ARTICLE).createCriteria(Article.REF_TYPE_ARTICLE).add(Expression.eq(TypeArticle.ID, -1));
		criteria.addOrder(Order.desc(Compensation.DATE_TRANSACTION));
		List<Compensation> list = criteria.list();
		return list;
	}
	
	/**
	 * Méthode getNombreCommandesLivrees : Permet de récupérer le nombre
	 * de commande livrée pour un contrat.
	 * @param : contratId
	 * @return : int
	 * @throws : TechnicalException 
	 */
	@SuppressWarnings("unchecked")
	public int getNombreCommandesLivrees(Long contratId) throws TechnicalException {
  		Hashtable<String, Object> params = new Hashtable<String, Object>();
  		params.put("coId", contratId);
  		List<Integer> resultat = null;
		resultat = getPersistenceManagerFidelio().getNamedQuery(Constantes.REQ_NOMBRE_COMMANDES_LIVREES_ENG, params); 		
  		if (resultat == null ) {			
  			return 0;			
  		} else {			
  			return resultat.get(0);
  		}
  	}
	
	/**
	 * Méthode getEngagementEnCours : Permet de retourner l'engagement en cours
	 * en passant comme paramétre l'id du contrat.
	 * @param contratId
	 * @return : String[]
	 * @throws : TechnicalException 
	 */
	public String[] getEngagementEnCours(Long contratId) throws TechnicalException {
		String[] resultat = new String[2];
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		params.put("contratId", contratId);
		List<Object> obj = getPersistenceManagerBSCS().getNamedQuery(Constantes.REQ_ENGAGEMENT_CONTRAT, params);
		if (!obj.isEmpty()) {
			resultat[0] = String.valueOf(((Object[]) obj.get(0))[0]);
			resultat[1] = String.valueOf(((Object[]) obj.get(0))[1]);
		}
		return resultat;
	}	

	/**
	 * (non javadoc)
	 * See @see ma.iam.fidelio.dao.dao.commande.CompteClientDao#getCategorieClientByPriceGroupe(java.lang.Integer).
	 * @param priceGroup
	 * @return
	 * @throws TechnicalException
	 */
	public CategorieClient getCategorieClientByPriceGroupe(Integer priceGroup) throws TechnicalException {
		CategorieClient categorieClient = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("priceGrp", priceGroup);
		
		List<CategorieClient> list = getPersistenceManagerFidelio().getNamedQuery(Constantes.REQ_CATEGORIE_CLIENT_BY_PRICE_GROUP, params);
		if (list != null && !list.isEmpty()) {
			categorieClient = list.get(0);
		}
		return categorieClient;
	}
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.fidelio.dao.dao.commande.CompteClientDao#getCategorieClientByCode(java.lang.String).
	 * @param code
	 * @return
	 * @throws TechnicalException
	 */
	public CategorieClient getCategorieClientByCode(String code) throws TechnicalException {
		Criteria crit = getPersistenceManagerFidelio().createCriteria(CategorieClient.class);
		crit.add(Expression.eq(CategorieClient.CODE, code));
		return (CategorieClient) crit.uniqueResult();
	}
	
	/**
	 * Méthode getSousCategorieByPriceGroupe : Permet de récupérer la sous catégorie client par price group
	 * @param : priceGroup
	 * @return : sousCategorie
	 * @throws : TechnicalException
	 */
	public SousCategorie getSousCategorieByPriceGroupe(Integer priceGroup) throws TechnicalException {
		SousCategorie sousCategorie = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("priceGrp", priceGroup);
		
		List<SousCategorie> list = getPersistenceManagerFidelio().getNamedQuery(Constantes.REQ_SOUS_CATEGORIE_BY_PRICE_GROUP, params);
		if (list != null && !list.isEmpty()) {
			sousCategorie = list.get(0);
		}
		return sousCategorie;
	}
	
}