package ma.iam.payment.dao.ge;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.ge.GlCode;
import ma.iam.payment.domaine.ge.PaymentCommandeEB;
import ma.iam.payment.domaine.ge.PaymentGE;
import ma.iam.payment.domaine.ge.PaymentGEDetail;

/**
 * Interface DAO du paiement GE .
 * 
 * @author Atos
 * @version 1.0
 */
public interface PaymentGEDao {

	/**
	 * Sauvegarder un paiement sur GE.
	 * 
	 * @param paymentGE the payments ge
	 * @throws TechnicalException the technical exception
	 */
	void savePaymentGE(PaymentGE paymentGE) throws TechnicalException;

	/**
	 * Sauvegarder le detail d'un paiement GE.
	 * 
	 * @param paymentGEDetail the payment details ge
	 * @throws TechnicalException the technical exception
	 */
	void savePaiementDetailGE(PaymentGEDetail paymentGEDetail) throws TechnicalException;

	/**
	 * Récupere un naouveau numéro de bordereau.
	 * 
	 * @return the max bordereau
	 * @throws TechnicalException the technical exception
	 */
	Long getNextNumBordereau() throws TechnicalException;
	
	/**
	 * Modifier le status du paiement GE.
	 * 
	 * @param pGE the gE
	 * @param status the status
	 * @param userName the user name
	 * @param version
	 * @throws TechnicalException the technical exception
	 * @see ma.iam.geve.service.paiementGE.PaymentStatusService#modifierStatusPaiement(ma.iam.geve.modele.data.PaymentGE, java.lang.String,
	 * java.lang.String)
	 */
	public void updatePaymentStatus(PaymentGE pGE, String status, String userName, int version) throws TechnicalException;
	
	/**
	 * Recupère le code GL par type transaction et règie .</br></br>
	 * Created on Sprint #
	 * @param trxId
	 * @param locId
	 * @return GlCode
	 * @throws TechnicalException
	 */
	public GlCode getGlcodeByTrxAndLocation(Integer trxId, Integer locId) throws TechnicalException;

	/**
	 * Crée la liaison entre la commande de la boutique en ligne et le paiement GE  .</br></br>
	 * @param paymentCommandeEB
	 * @throws TechnicalException
	 */
	void savePaymentCommandeEB(PaymentCommandeEB paymentCommandeEB) throws TechnicalException;

	/**
	 * Recherche du paiement GE correspondant au numéro de commande EB .</br></br>
	 * @param numCommande
	 * @return PaymentGE
	 */
	PaymentGE getPaymentByNumCommande(String numCommande) throws TechnicalException;

}
