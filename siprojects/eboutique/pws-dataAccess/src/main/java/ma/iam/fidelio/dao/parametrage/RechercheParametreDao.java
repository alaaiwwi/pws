package ma.iam.fidelio.dao.parametrage;

import java.util.Map;

import ma.iam.fidelio.domaine.data.fidelio.StatutAffaire;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.ConversionMonsuelle;
import ma.iam.fidelio.domaine.data.parametrage.DelaiPremiereConversion;
import ma.iam.fidelio.domaine.data.parametrage.ModeEngagement;
import ma.iam.fidelio.domaine.data.parametrage.SeuilImpaye;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface RechercheParametreDao {
	
	/**
	 * Méthode getArticleByCode : Permet de récupérer un objet de typt Article 
	 * à partir d'un code.
	 * @param : codeArticle
	 * @return : Article
	 * @throws : TechnicalException
	 */
	public Article getArticleByCode(String codeArticle) throws TechnicalException;
	
	/**
	 * Méthode getArticleById : Permet de récupérer un objet de type Article 
	 * à partir d'un id.
	 * @param : idArticle
	 * @return : Article
	 * @throws : TechnicalException
	 */
	public Article getArticleById(Integer idArticle) throws TechnicalException;
	
	/**
	 * Permet de de récupérer le bonus de l'engagement
	 * à partir d'un id Categorie Client et Id Mode Engagement.
	 * @param : modeEngagementId
	 * @param : categorieClientId
	 * @return : Integer
	 * @throws : TechnicalException
	 */
	public Integer getBonusEngagement(Integer categorieClientId, Integer modeEngagementId) throws TechnicalException;
	
	/**
	 * Méthode getConversionMonsuelle : Permet de récupérer un objet de type 
	 * ConversionMonsuelle à partir d'un categorieClientId.
	 * @param : categorieClientId
	 * @return : ConversionMonsuelle
	 * @throws : TechnicalException
	 */
	public ConversionMonsuelle getConversionMonsuelle(Integer categorieClientId) throws TechnicalException;
	
	/**
	 * Méthode getDelaiPremiereConversion : Permet de récupérer un objet de type 
	 * DelaiPremiereConversion à partir d'un categorieClientId.
	 * @param : categorieClientId
	 * @return : DelaiPremiereConversion
	 * @throws : TechnicalException
	 */
	public DelaiPremiereConversion getDelaiPremiereConversion(Integer categorieClientId) throws TechnicalException;
	
	/**
	 * Méthode getModeEngagementByBscsId : Permet de récupérer un objet de type 
	 * ModeEngagement à partir d'un engagementId.
	 * @param : engagementId
	 * @return : ModeEngagement
	 * @throws : TechnicalException
	 */
	public ModeEngagement getModeEngagementByBscsId(Integer engagementId) throws TechnicalException;
	
	/**
	 * Méthode getModeEngagementById : Permet de récupérer un objet de type 
	 * ModeEngagement à partir d'un engagementId.
	 * @param : engagementId
	 * @return : ModeEngagement
	 * @throws : TechnicalException
	 */
	public ModeEngagement getModeEngagementById(Integer engagementId) throws TechnicalException;
	
	/**
	 * Méthode getPrixPackMinimalPourRemise : Permet de récupérer le prixPackMinimalPourRemise
	 * à partir d'un categorieId.
	 * @param : categorieId
	 * @return : Float
	 * @throws : TechnicalException
	 */
	public Float getPrixPackMinimalPourRemise(Integer categorieId) throws TechnicalException;
	
	/**
	 * Méthode getPrixPackXMois : Recuperer le prix d'un pack pour un egagement de X mois.
	 * @param : planTarifaireId
	 * @param : codeProduit
	 * @param : nbreMois
	 * @return : Float
	 * @throws : TechnicalException
	 */
	public Float getPrixPackXMois(Integer planTarifaireId, String codeProduit, Integer nbreMois) throws TechnicalException;
	
	/**
	 * Méthode getPrixRachatPoint : Récupérer le parametre de rachat d'un point.
	 * @param idModeEngagement
	 * @return : Double
	 * @throws : TechnicalException 
	 */
	public Double getPrixRachatPointByModeEngagement(Integer idModeEngagement) throws TechnicalException;
	
	/**
	 * Méthode getRemisePackPrivilege : Récupérer la remise sur pack privilige.
	 * @param : codeClient
	 * @param : numAppel
	 * @param : prixPack
	 * @param : seuilPrix
	 * @param : remisePrivilege
	 * @return : Float
	 * @throws : TechnicalException
	 */
	public Float getRemisePackPrivilege(String codeClient, String numAppel, Float prixPack, Float seuilPrix, Float remisePrivilege)
			throws TechnicalException;
	
	/**
	 * Méthode getSeuilImpaye : Permet de récupérer un objet de type 
	 * SeuilImpaye à partir d'un categorieClientId.
	 * @param : categorieClientId
	 * @return : SeuilImpaye
	 * @throws : TechnicalException
	 */	
	public SeuilImpaye getSeuilImpaye(Integer categorieClientId) throws TechnicalException;
	
	/**
	 * Méthode getSeuilMinimalEngagement : Permet de la seuil Minimal Engagement 
	 * à partir d'un id Mode Engagement.
	 * @param categorieClientId TODO
	 * @param : idModeEngagement
	 * @return : Double
	 * @throws : TechnicalException
	 */
	public Double getSeuilMinimalEngagement(Integer modeEngagementId, Integer categorieClientId) throws TechnicalException;
	
	/**
	 * Méthode getStatutAffaireByCode : Permet de récupérer un objet de type 
	 * StatutAffaire à partir d'un statusAffaireCree.
	 * @param : statusAffaireCree
	 * @return : StatutAffaire
	 * @throws TechnicalException
	 */
	public StatutAffaire getStatusAffaireByCode(String statusAffaireCree) throws TechnicalException;
	
	/**
	 * Méthode getTauxConversion : Permet de récupérer le Taux Conversion 
	 * à partir d'un id Categorie Client.
	 * @param : categorieClientId
	 * @return : Double
	 * @throws : TechnicalException 
	 */
	public Double getTauxConversion(Integer categorieClientId) throws TechnicalException;

	/**
	 * Méthode isExclusion : Permet de vérifier si l'article est disponible pour le client ou pas.
	 * @param : articleId
	 * @param : qualiteClient
	 * @param : sousCategoryClient
	 * @param : planTarifaireClient
	 * @return : Boolean
	 * @throws TechnicalException
	 */
	public Boolean isExclusion(Integer articleId, Integer qualiteClient,
			Integer sousCategoryClient, Integer planTarifaireClient)
			throws TechnicalException;
	
	/**
	 * Permet de retourner les prix du poste pour tous les modes engagement et sans engagement
	 * @param codeProduit
	 * @return : Map
	 * @throws : TechnicalException 
	 */
	public Map<String, Float> getPrixPoste(String codeProduit) throws TechnicalException;
	
}