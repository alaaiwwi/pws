package ma.iam.fidelio.dao.commande.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.AbstractDao;
import ma.iam.fidelio.dao.commande.CommandeDao;
import ma.iam.fidelio.domaine.data.fidelio.Affaire;
import ma.iam.fidelio.domaine.data.fidelio.Commande;
import ma.iam.fidelio.domaine.data.fidelio.CommandeHestory;
import ma.iam.fidelio.domaine.data.fidelio.Compensation;
import ma.iam.fidelio.domaine.data.parametrage.Agence;
import ma.iam.fidelio.domaine.data.parametrage.TypeAffaire;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class CommandeDaoImpl extends AbstractDao implements CommandeDao{

	/**
	 * Méthode ajouterAffaire : Permet d'ajouter une affaire.
	 * @param : affaire
	 * @throws : TechnicalException
	 */
	public void saveAffaire(Affaire affaire) throws TechnicalException {
		getPersistenceManagerFidelio().save(affaire);
	}
	
	/**
	 * Méthode ajouterCompensation : Permet d'ajouter une compensation.
	 * @param : compensationsToSave
	 * @throws : TechnicalException
	 */
	public void saveCompensations(List<Compensation> compensationsToSave) throws TechnicalException {
		for (Compensation compensation : compensationsToSave) {
			getPersistenceManagerFidelio().save(compensation);
		}
	}
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.fidelio.dao.dao.commande.CommandeDao#saveCompensation(ma.iam.fidelio.domaine.data.fidelio.Compensation).
	 * @param compensation
	 * @throws TechnicalException
	 */
	public void saveCompensation(Compensation compensation) throws TechnicalException {
		getPersistenceManagerFidelio().save(compensation);
	}
	
	/**
	 * Méthode getCommandeByNumCommandeEB : Permet de récupérer la commande
	 * à partir du numéro de la commande E-Boutique.
	 * @param : numCommandeEB
	 * @return : Commande
	 * @throws : TechnicalException 
	 */
	public Commande getCommandeByNumCommandeEB(String numCommandeEB) throws TechnicalException{
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(Commande.class);
		criteria.add(Restrictions.eq("numCommandeEB", numCommandeEB));
		return (Commande) criteria.uniqueResult();
	}
	
	/**
	 * Méthode saveCommandeHestory : Permet de sauvegarder un objet de type CommandeHestory.
	 * @param : commandeHestory
	 * @throws : TechnicalException 
	 */
	public void saveCommandeHistory(CommandeHestory commandeHestory) throws TechnicalException {
		getPersistenceManagerFidelio().save(commandeHestory);
	}
	
	/**
	 * Méthode passerCommande : Permet de sauvegarder une commande.
	 * @param : commade
	 * @throws : TechnicalException 
	 */
	public void saveCommande(Commande cmd) throws TechnicalException {
		getPersistenceManagerFidelio().save(cmd);
	}
	
	/**
	 * Méthode getTypeAffaireByCode : Permet de récupérer un objet de type 
	 * TypeAffaire à partir d'un typeAffaireAvecEngagementCode.
	 * @param : typeAffaireAvecEngagementCode
	 * @return : TypeAffaire
	 * @throws : TechnicalException
	 */
	public TypeAffaire getTypeAffaireByCode(String typeAffaireAvecEngagementCode) throws TechnicalException {
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(TypeAffaire.class);
		criteria.add(Restrictions.eq(TypeAffaire.CODE, typeAffaireAvecEngagementCode));
		return (TypeAffaire) criteria.uniqueResult();
	}
	
	/**
	 * Permet de récupérer l'Agence FIDELIO	 à partir du cost center BSCS.
	 * @param : costId
	 * @return : Agence
	 * @throws : TechnicalException
	 */
	public Agence getActiveAgenceByCostId(Integer costId) throws TechnicalException {
		Agence agence = null;
		Criteria criteria = getPersistenceManagerFidelio().createCriteria(Agence.class);
		criteria.add(Restrictions.eq(Agence.COST_CENTER_ID, costId));
		criteria.add(Restrictions.eq(Agence.EN_SERVICE, Boolean.TRUE));
		List list = criteria.list();
		if (list != null && !list.isEmpty()) {
			agence = (Agence) list.get(0);
		}
		return agence;
	}
	
	
	/** (non-Javadoc)
	 * @see ma.iam.fidelio.dao.dao.commande.CommandeDao#getCommandesEBLivreesParPeriode(java.util.Date, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	public List<Commande> getCommandesEBLivreesParPeriode(Date dateDebut, Date dateFin) throws TechnicalException {
		
		StringBuilder lRequete = new StringBuilder("");
		Map<String, Object> lParametres =  new HashMap<String, Object>();
		
		lRequete.append("SELECT commande FROM Commande commande JOIN FETCH commande.refAffaire affaire JOIN FETCH affaire.refHistStatutAffaires histo ");
		lRequete.append("WHERE commande.numCommandeEB is not null ");
		lRequete.append("AND commande.dateLivraison >= :dateDebut ");
		
		if(dateFin != null) {
			lRequete.append("AND commande.dateLivraison <= :dateFin ");
			lParametres.put("dateFin", dateFin);
		}
		
		lRequete.append("AND histo.dateStatut = (SELECT max(dateStatut) FROM HistStatutAffaire h WHERE h.refAffaire = affaire) ");
		lRequete.append("AND histo.refStatutAffaire.code = :statut");
		
		lParametres.put("dateDebut", dateDebut);
		lParametres.put("statut", Constantes.STATUS_AFFAIRE_LIVRE);
		
		return getPersistenceManagerFidelio().findByQuery(lRequete.toString(), lParametres);
	}
	
}