package ma.iam.payment.dao.ge.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.ge.TransactionTypeDao;
import ma.iam.payment.domaine.ge.TransactionType;

/**
 * The Class TransactionTypeDaoImpl.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class TransactionTypeDaoImpl extends AbstractDao implements TransactionTypeDao {

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.TransactionTypeDao#getTransactionTypeById(java.lang.Integer).
	 * @param id
	 * @return
	 * @throws TechnicalException
	 */
	public TransactionType getTransactionTypeById(Integer id) throws TechnicalException {

		return (TransactionType) getPersistenceManager().findById(TransactionType.class, id, false);
	}
}
