package ma.iam.payment.dao.ge;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.ge.PaymentMode;

/**
 * The Interface PaymentModeDao.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public interface PaymentModeDao {

	/**
	 * Recupère le mode de paiement par ID.
	 * 
	 * @param id the id
	 * @return PaymentMode
	 * @throws TechnicalException
	 */
	PaymentMode getPaymentModeById(Integer id) throws TechnicalException;

}
