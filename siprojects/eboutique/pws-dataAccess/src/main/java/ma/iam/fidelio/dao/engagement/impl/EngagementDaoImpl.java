package ma.iam.fidelio.dao.engagement.impl;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.AbstractDao;
import ma.iam.fidelio.dao.engagement.EngagementDao;
import ma.iam.fidelio.domaine.data.bscs.FidelioProductSAP;
import ma.iam.fidelio.domaine.data.fidelio.Engagement;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public class EngagementDaoImpl extends AbstractDao implements EngagementDao {
	
	/**
	 * Méthode getLigneCatalogue : Permet de récupérer une ligne catalogue 
	 * à partir d'un idArticle planTarifaireId, categorieClientId, modeEngagementId et qualiteClientId.
	 * @param : articleId
	 * @param : planTarifaireId
	 * @param : categorieClientId
	 * @param : modeEngagementId
	 * @param : qualiteClientId
	 * @return : Engagement
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Engagement getLigneCatalogue(Integer idArticle,
			Integer planTarifaireId, Integer catClientId, Integer modeEngagementId, Integer qualiteClientId) throws TechnicalException {
		Engagement engagement = null;
		
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("articleid", idArticle);
		params.put("plantarifaire", planTarifaireId);
		params.put("modeengagement", modeEngagementId);
		params.put("catclient", catClientId);
		params.put("qualite", qualiteClientId);


		List<Engagement>  list = getPersistenceManagerFidelio().getNamedQuery(Constantes.REQ_GET_LINE_CATALOGUE, params);
		if (list != null && !list.isEmpty()) {
			engagement = list.get(0);
		}
		return engagement;
	}
	
	/**
	 * Méthode getListCatalogue : Permet de Récupère tout le catalogue.
	 * @return : List<Engagement>
	 * @throws : TechnicalException 
	 */
	@SuppressWarnings("unchecked")
	public List<Engagement> getListCatalogue(Integer categorieClientId) throws TechnicalException {
		Hashtable param = new Hashtable();
		param.put("categorieClientId", categorieClientId);
		return getPersistenceManagerFidelio().getNamedQuery(Constantes.REQ_GET_ALL_CATALOGUE, param);
	}
	
	/**
	 * Méthode getProductSAPByCodeArticle : Permet de récupérer le code SAP 
	 * à partir d'un code bscs.
	 * @param : codeArticle
	 * @return : FidelioProductSAP
	 * @throws : TechnicalException
	 */
	public FidelioProductSAP getProductSAPByCodeArticle(String codeArticle) throws TechnicalException {
		Criteria criteria = getPersistenceManagerBSCS().createCriteria(FidelioProductSAP.class);
		criteria.add(Expression.eq(FidelioProductSAP.CODE_PRODUCT, codeArticle));
		return (FidelioProductSAP) criteria.uniqueResult();
	}
	
}