package ma.iam.fidelio.dao.commande;

import java.util.Date;
import java.util.List;

import ma.iam.fidelio.domaine.data.fidelio.Affaire;
import ma.iam.fidelio.domaine.data.fidelio.Commande;
import ma.iam.fidelio.domaine.data.fidelio.CommandeHestory;
import ma.iam.fidelio.domaine.data.fidelio.Compensation;
import ma.iam.fidelio.domaine.data.parametrage.Agence;
import ma.iam.fidelio.domaine.data.parametrage.TypeAffaire;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface CommandeDao {
	
	/**
	 * Méthode ajouterAffaire : Permet d'ajouter une affaire.
	 * @param : affaire
	 * @throws : TechnicalException
	 */
	public void saveAffaire(Affaire affaire) throws TechnicalException;
	
	/**
	 * Sauvegarde une liste de compensations.
	 * @param : compensationsToSave
	 * @throws : TechnicalException
	 */
	void saveCompensations(List<Compensation> compensationsToSave) throws TechnicalException;
	
	/**
	 * Sauvegarde une compensation.
	 * @param : compensationsToSave
	 * @throws : TechnicalException
	 */
	void saveCompensation(Compensation compensationsToSave) throws TechnicalException;
	
	/**
	 * Méthode getCommandeByNumCommandeEB : Permet de récupérer la commande
	 * à partir du numéro de la commande E-Boutique.
	 * @param : numCommandeEB
	 * @return : Commande
	 * @throws : TechnicalException 
	 */
	public Commande getCommandeByNumCommandeEB(String numCommandeEB) throws TechnicalException;
	
	/**
	 * Méthode saveCommandeHestory : Permet de sauvegarder un objet de type CommandeHestory.
	 * @param : commandeHestory
	 * @throws : TechnicalException 
	 */
	public void saveCommandeHistory(CommandeHestory commandeHestory) throws TechnicalException;
	
	/**
	 * Méthode passerCommande : Permet de sauvegarder une commande.
	 * @param : commade
	 * @throws : TechnicalException 
	 */
	public void saveCommande(Commande commande) throws TechnicalException;
	
	/**
	 * Permet de récupérer l'Agence FIDELIO à partir du cost center BSCS
	 * @param : costId
	 * @return : Agence
	 * @throws : TechnicalException
	 */
	public Agence getActiveAgenceByCostId(Integer costId) throws TechnicalException;
	
	/**
	 * Méthode getTypeAffaireByCode : Permet de récupérer un objet de type 
	 * TypeAffaire à partir d'un typeAffaireAvecEngagementCode.
	 * @param : typeAffaireAvecEngagementCode
	 * @return : TypeAffaire
	 * @throws : TechnicalException
	 */
	public TypeAffaire getTypeAffaireByCode(String typeAffaireAvecEngagementCode) throws TechnicalException;

	/**
	 * Permet de récupérer les commandes EB livrées pour la période en entrée
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 * @throws TechnicalException 
	 */
	public List<Commande> getCommandesEBLivreesParPeriode(Date dateDebut, Date dateFin) throws TechnicalException;
	
}