package ma.iam.payment.dao.bscs.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.bscs.UserDao;
import ma.iam.payment.domaine.bscs.User;

/**
 * DAO User .
 * <br/><br/>
 * 
 * @author Atos
 *
 */
public class UserDaoImpl extends AbstractDao implements UserDao {
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.bscs.UserDao#getUserByLogin(java.lang.String).
	 * @param login
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public User getUserByLogin(String login) throws TechnicalException {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", login);
		List<User> list = getPersistenceManager().getNamedQuery(Constantes.REQ_GET_BSCS_USER, params);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
