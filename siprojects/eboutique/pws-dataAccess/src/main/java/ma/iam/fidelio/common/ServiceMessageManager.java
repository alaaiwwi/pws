package ma.iam.fidelio.common;
import java.util.ArrayList;
import java.util.List;

import ma.iam.fidelio.dto.MessageDto;

/**
 * Classe permettant d'ajouter un message d'erreur au conteneur de messages d'erreur
 * associé à un processus dans un appel de service donnée.
 *
 * @author mei
 */
public final class ServiceMessageManager {
	
	
	private static final int RES_SUCCESS = 0;
	private static final int RES_ERROR = 1;

    /**
     * instance de la classe ThreadLocal pour assurer que une
     * instance de ServiceMessageManager n'est pas partagé entre threads.
     */
    private static ThreadLocal instance = new ThreadLocal() {
         protected synchronized Object initialValue() {
             return new ServiceMessageManager();
         }
     };

    /**
     * Collection de messages d'erreur.
     */
    private List<MessageDto> messages;

     /**
      * Permet de gérer le code résultat "global" d'une opération.
      */
    private int resultCode = RES_SUCCESS;

    private ServiceMessageManager() {
        this.messages = new ArrayList<MessageDto>();
    }

    /**
     * Renvoie la liste des messages d'erreurs gérées dans ThreadLocal.
     * @return errors de type List.
     */
    public static List<MessageDto> getMessages() {
        return getInstance().messages;
    }

    /**
     * Permet d'ajouter un objet Message (avec paramètres) à la liste des
	 * messages avec une criticité <code>ErrorMessage.CRT_ERROR</code>
	 *
     * @param msgId L'identifiant du message destiné à l'utilisateur.
     * @see ErrorMessage
     */
    public static void addErrorMessage(String code, String msg) {
        addMessage(new MessageDto(code, msg));
        if (getInstance().resultCode == RES_SUCCESS) {
            getInstance().resultCode = RES_ERROR;
        }
    }
	
    /**
     * Permet d'ajouter un objet Message a la liste des messages.
     * le code de résultat passe à RES_ERROR.
     * @param errorMessage L'objet Error à rajouter
     * @see ErrorMessage
     */
    public static void addMessage(MessageDto errorMessage) {
        if (errorMessage != null) {
            getInstance().messages.add(errorMessage);
        }
    }

    /**
     * Retourne le résultat "global" à ThreadLocal.
     * @return le code résultat de l'opération
     * @see ErrorMessage
     * @see Result
     */
    public static boolean isSuccess() {
        return (getInstance().resultCode == RES_SUCCESS);
    }

    /**
     * Permet de réinitialiser l'instance thread local, en
     * vidant la liste des messages et en repassant resultCode
     * à <code>RES_SUCCESS</code>
     * @see Result
     */
    public static void reset() {
    	ServiceMessageManager errMng = getInstance();
    	errMng.messages.clear();
        errMng.resultCode = RES_SUCCESS;
    }
    
    /**
     * Renvoie instance.
     * @return instance de type ThreadLocal.
     */
    public static ServiceMessageManager getInstance() {
        return (ServiceMessageManager) instance.get();
    }
}
