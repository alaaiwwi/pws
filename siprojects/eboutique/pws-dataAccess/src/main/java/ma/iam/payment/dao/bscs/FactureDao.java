package ma.iam.payment.dao.bscs;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.bscs.Facture;

/**
 * DAO Facture.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public interface FactureDao {

	/**
	 * Sauvegarder facture.
	 * 
	 * @param systeme the systeme
	 * @param facture the facture
	 * 
	 * @return the facture
	 * 
	 * @throws TechnicalException the technical exception
	 */
	Facture saveFacture(Facture facture) throws TechnicalException;

	/**
	 * Récupère periode facturation.
	 * 
	 * @return the periode facture
	 * 
	 * @throws TechnicalException the technical exception
	 */
	String getPeriodeFacture() throws TechnicalException;

	/**
	 * Recuperer la valeur suivante de la sequence
	 * @return
	 * @throws TechnicalException
	 */
	Long getFactureSeq() throws TechnicalException;
	
}