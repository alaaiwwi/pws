package ma.iam.payment.dao.bscs;

import java.util.Date;
import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.bscs.Client;
import ma.iam.payment.domaine.bscs.PaymentBSCS;
import ma.iam.payment.domaine.bscs.PaymentBSCSDetail;
import ma.iam.payment.domaine.bscs.Ticket;

/**
 * DAO PaymentBSCS.
 * 
 * @author Atos
 * @version 1.0
 */
public interface PaymentBSCSDao {

	/**
	 * Sauvegarder MEMO BSCS.
	 * 
	 * @param ticket
	 * @throws TechnicalException
	 */
	void saveMemoBSCS(Ticket ticket) throws TechnicalException;

	/**
	 * Sauvegarder detail Paiement.
	 * 
	 * @param paymentBSCSDetail
	 * @throws TechnicalException the technical exception
	 */
	void savePaiementDetailBSCS(PaymentBSCSDetail paymentBSCSDetail) throws TechnicalException;

	/**
	 * Sauvegarder Paiement.
	 * 
	 * @param client the client
	 * @param trxDate the en date
	 * @param chkUm the chk um
	 * @param glCash the gl cash
	 * @param glDis the gl dis
	 * @param caType the ca type
	 * @param carem the carem
	 * @param cAPP the cAPP
	 * @param causerName the causer name
	 * @param debitInfo1 the debit info1
	 * @param costCenter the cost center
	 * @param convrateTypeGl the convrate type gl
	 * @param convrateTypeDoc the convrate type doc
	 * @param chkAmtGl the chk amt gl
	 * @param disAmtGl the dis amt gl
	 * @param curAmtGl the cur amt gl
	 * @param chkAmtPay the chk amt pay
	 * @param disAmtPay the dis amt pay
	 * @param curAmtPay the cur amt pay
	 * @param balanceHome the balance home
	 * @param codeRaison the code raison
	 * @return the paiement
	 * @throws TechnicalException the technical exception
	 */
	PaymentBSCS savePaiement(Client client, Date trxDate, String chkUm, String glCash,
			String glDis, Long caType, String carem, String cAPP,
			String causerName, String debitInfo1, Integer costCenter, Long convrateTypeGl,
			Long convrateTypeDoc, Double chkAmtGl, Double disAmtGl, Double curAmtGl, Double chkAmtPay, Double disAmtPay, Double curAmtPay,
			Double balanceHome, Long codeRaison) throws TechnicalException;

	/**
	 * Recherche un paiement par id.
	 * 
	 * @param id the id
	 * @return the paiement by id
	 * @throws TechnicalException the technical exception
	 */
	PaymentBSCS getPaiementByID(Long id) throws TechnicalException;

	/**
	 * Sauvegarder paiement
	 * @param paiement
	 * @throws TechnicalException
	 */
	void savePaiement(PaymentBSCS paiement) throws TechnicalException;

	/**
	 * Récupères les détails paiements d'uen facture
	 * @param factureId
	 * @return
	 * @throws TechnicalException
	 */
	List<PaymentBSCSDetail> getDetailPaymentByFacture(Long factureId) throws TechnicalException;
	
}
