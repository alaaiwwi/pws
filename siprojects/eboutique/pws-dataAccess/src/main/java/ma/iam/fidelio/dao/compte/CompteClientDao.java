package ma.iam.fidelio.dao.compte;

import java.util.Collection;
import java.util.List;

import ma.iam.fidelio.domaine.data.fidelio.Compensation;
import ma.iam.fidelio.domaine.data.fidelio.CompteClient;
import ma.iam.fidelio.domaine.data.parametrage.CategorieClient;
import ma.iam.fidelio.domaine.data.parametrage.SousCategorie;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface CompteClientDao {
	
	
	/**
	 * Méthode getCommandeEnInstance : Permet de récupérer 
	 * le nombre de commande en instance pour un seul contrat et le type catalogue. 
	 * @param : contratId
	 * @return : nombre de commande en instance.
	 * @throws : TechnicalException 
	 */
	public Integer getNombreCommandeEnInstance(Long contratId) throws TechnicalException;
	
	/**
	 * Méthode getCompteClientByCustomerId : Récupère un objet de type CompteClient 
	 * à partir d'un customerId.
	 * @param : customerId
	 * @return : CompteClient
	 * @throws : TechnicalException
	 */
	public CompteClient getCompteClientByCustomerId(Integer id) throws TechnicalException;
	
	/**
	 * Méthode getCreditCompensationsByClient : permet de récupérer une liste de type Compensation 
	 * à partir d'un id du compte client.
	 * @param : compteClientId
	 * @return : List<Compensation>
	 * @throws : TechnicalException
	 */
	public List<Compensation> getCreditCompensationsByClient(Integer compteClientId) throws TechnicalException;
	
	
	/**
	 * Méthode getListCompensationAannulerByClient : Permet de récupérer une liste des compensation
	 * à annuler à partir d'id Compte Client. 
	 * @param : idCompteClient
	 * @return : Collection<Compensation>
	 * @throws : TechnicalException
	 */
	public Collection<Compensation> getListCompensationAannulerByClient(Integer idCompteClient) throws TechnicalException;
	
	/**
	 * Méthode getNombreCommandesLivrees : Permet de récupérer le nombre
	 * de commande livrée pour un contrat.
	 * @param : contratId
	 * @return : int
	 * @throws : TechnicalException 
	 */
	public int getNombreCommandesLivrees(Long contratId) throws TechnicalException;
	
	/**
	 * Méthode getEngagementEnCours : Permet de retourner l'engagement en cours
	 * en passant comme paramétre l'id du contrat.
	 * @param contratId
	 * @return : String[]
	 * @throws : TechnicalException 
	 */
	public String[] getEngagementEnCours(Long contratId) throws TechnicalException;
	
	
	/**
	 * Permet de récupérer le catégorie client par PRGCODE
	 * @param : priceGroup
	 * @return : CategorieClient
	 * @throws : TechnicalException
	 */
	CategorieClient getCategorieClientByPriceGroupe(Integer priceGroup) throws TechnicalException;
	
	/**
	 * Permet de récupérer le catégorie client par code
	 * @param : priceGroup
	 * @return : CategorieClient
	 * @throws : TechnicalException
	 */
	CategorieClient getCategorieClientByCode(String code) throws TechnicalException;
	
	/**
	 * Méthode getSousCategorieByPriceGroupe : Permet de récupérer la sous catégorie client par price group
	 * @param : priceGroup
	 * @return : sousCategorie
	 * @throws : TechnicalException
	 */
	public SousCategorie getSousCategorieByPriceGroupe(Integer priceGroup) throws TechnicalException;
	
}