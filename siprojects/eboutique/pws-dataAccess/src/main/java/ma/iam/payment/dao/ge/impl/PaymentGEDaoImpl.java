package ma.iam.payment.dao.ge.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.ge.PaymentGEDao;
import ma.iam.payment.domaine.ge.GlCode;
import ma.iam.payment.domaine.ge.PaymentCommandeEB;
import ma.iam.payment.domaine.ge.PaymentGE;
import ma.iam.payment.domaine.ge.PaymentGEDetail;
import ma.iam.payment.domaine.ge.PaymentStatus;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 * DAO Payment GE.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentGEDaoImpl extends AbstractDao implements PaymentGEDao {


	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentGEDao#savePaymentGE(ma.iam.paiement.domaine.ge.PaymentGE).
	 * @param p
	 * @throws TechnicalException
	 */
	public void savePaymentGE(PaymentGE p) throws TechnicalException {
		getPersistenceManager().save(p);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentGEDao#savePaiementDetailGE(ma.iam.paiement.domaine.ge.PaymentDetailsGE).
	 * @param pDetailsGE
	 * @throws TechnicalException
	 */
	public void savePaiementDetailGE(PaymentGEDetail pDetailsGE) throws TechnicalException {
		getPersistenceManager().save(pDetailsGE);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentGEDao#getNextNumBordereau().
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Long getNextNumBordereau() throws TechnicalException {
		// executin the sql statment
		List result = getPersistenceManager().getNamedQuery(Constantes.REQ_PAIEMENT_GE_NEXT_BORDNUM, new HashMap<String, Object>());
		return (Long) result.get(0);
	}

	
	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentGEDao#updatePaymentStatus(ma.iam.paiement.domaine.ge.PaymentGE, java.lang.String, java.lang.String).
	 * @param pGE
	 * @param status
	 * @param userName
	 * @param version
	 * @throws TechnicalException
	 */
	public void updatePaymentStatus(PaymentGE pGE, String status, String userName, int version) throws TechnicalException {
		PaymentStatus paymentStatus = new PaymentStatus();
		paymentStatus.setCaxact(pGE.getCaxact().intValue());
		paymentStatus.setHistno(version);
		paymentStatus.setStatus(status);
		paymentStatus.setStatusDate(new Date());
		paymentStatus.setUsername(userName);
		paymentStatus.setStPyid(pGE);
		getPersistenceManager().save(paymentStatus);
	}
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentGEDao#getGlcodeByTrxAndLocation(java.lang.Integer, java.lang.Integer).
	 * @param trxId
	 * @param locId
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public GlCode getGlcodeByTrxAndLocation(Integer trxId, Integer locId) throws TechnicalException {
		
		List<Criterion> criteriasList = new ArrayList<Criterion>();
		criteriasList.add(Restrictions.eq("opLocation.id", locId));
		criteriasList.add(Restrictions.eq("trx.id", trxId));
		List result = getPersistenceManager().findByCriteria(GlCode.class, criteriasList, null, false);
		if (result != null && !result.isEmpty()) {
			return (GlCode) result.get(0);
		}
		return null;
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentGEDao#savePaymentCommandeEB(ma.iam.paiement.domaine.ge.PaymentCommandeEB).
	 * @param paymentCommandeEB
	 * @throws TechnicalException
	 */
	public void savePaymentCommandeEB(PaymentCommandeEB paymentCommandeEB) throws TechnicalException {
		getPersistenceManager().save(paymentCommandeEB);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.ge.PaymentGEDao#getPaymentByNumCommande(java.lang.Integer).
	 * @param numCommande
	 * @return PaymentGE
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public PaymentGE getPaymentByNumCommande(String numCommande)
			throws TechnicalException {
		PaymentGE paymentGE = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cmd", numCommande);
		List<PaymentGE> list = getPersistenceManager().getNamedQuery(Constantes.REQ_PAYMENT_GE_BY_COMMANDE, params);
		if (list != null && !list.isEmpty()) {
			paymentGE = list.get(0);
		}
		return paymentGE;
	}

}