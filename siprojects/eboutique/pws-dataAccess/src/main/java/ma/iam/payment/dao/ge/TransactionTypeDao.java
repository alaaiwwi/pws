package ma.iam.payment.dao.ge;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.ge.TransactionType;

/**
 * 
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public interface TransactionTypeDao {

	/**
	 * Recupère le type de transaction par Identifiant.
	 * 
	 * @param id the id
	 * @return type de transaction par Identifiant
	 * @throws TechnicalException
	 */
	TransactionType getTransactionTypeById(Integer id) throws TechnicalException;

}
