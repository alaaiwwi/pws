package ma.iam.fidelio.dao.engagement;

import java.util.List;

import ma.iam.fidelio.domaine.data.bscs.FidelioProductSAP;
import ma.iam.fidelio.domaine.data.fidelio.Engagement;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface EngagementDao {
	
	/**
	 * Méthode getLigneCatalogue : Permet de récupérer une ligne catalogue 
	 * à partir d'un idArticle planTarifaireId, categorieClientId, modeEngagementId et qualiteClientId.
	 * @param : articleId
	 * @param : planTarifaireId
	 * @param : categorieClientId
	 * @param : modeEngagementId
	 * @param : qualiteClientId
	 * @return : Engagement
	 * @throws : TechnicalException
	 */
	public Engagement getLigneCatalogue(Integer articleId, Integer planTarifaireId, Integer categorieClientId, Integer modeEngagementId, Integer qualiteClientId)
			throws TechnicalException;
	
	/**
	 * Méthode getListCatalogue : Permet de Récuperer tout le catalogue.
	 * @return : List<Engagement>
	 * @throws : TechnicalException 
	 */
	public List<Engagement> getListCatalogue(Integer categorieClientId) throws TechnicalException;
	
	/**
	 * Méthode getProductSAPByCodeArticle : Permet de récupérer le code SAP 
	 * à partir d'un code bscs.
	 * @param : codeArticle
	 * @return : FidelioProductSAP
	 * @throws : TechnicalException
	 */
	public FidelioProductSAP getProductSAPByCodeArticle(String codeArticle) throws TechnicalException;

}
