package ma.iam.payment.dao.ge.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.ge.PaymentModeDao;
import ma.iam.payment.domaine.ge.PaymentMode;

/**
 * The Class PaymentModeDaoImpl.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentModeDaoImpl extends AbstractDao implements PaymentModeDao {

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentModeDao#getPaymentModeById(java.lang.Long).
	 * @param id
	 * @return
	 * @throws TechnicalException
	 */
	public PaymentMode getPaymentModeById(Integer id) throws TechnicalException {
		return (PaymentMode) getPersistenceManager().findById(
				PaymentMode.class, id, false);
	}

}
