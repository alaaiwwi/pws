package ma.iam.payment.dao.bscs.impl;

import java.util.Date;
import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.util.Utils;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.bscs.PaymentBSCSDao;
import ma.iam.payment.domaine.bscs.Client;
import ma.iam.payment.domaine.bscs.PaymentBSCS;
import ma.iam.payment.domaine.bscs.PaymentBSCSDetail;
import ma.iam.payment.domaine.bscs.Ticket;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 * DAO PaymentBSCS.
 * 
 * @author Atos
 * @version 1.0
 */
public class PaymentBSCSDaoImpl extends AbstractDao implements PaymentBSCSDao {


	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentBSCSDao#saveMemoBSCS(ma.iam.paiement.domaine.bscs.Ticket).
	 * @param ticket
	 * @throws TechnicalException
	 */
	public void saveMemoBSCS(Ticket ticket) throws TechnicalException {
		/* 
		 * added to adapt it with BSCSIX to prevent insertion
		 *  of sequence id
		 */
		if (ticket.getId() == null) {
			ticket.setId(getTicklerSeq());
		}
		getPersistenceManager().save(ticket);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentBSCSDao#savePaiementDetailBSCS(ma.iam.paiement.domaine.bscs.PaymentBSCSDetail).
	 * @param payBSCSDetail
	 * @throws TechnicalException
	 */
	public void savePaiementDetailBSCS(PaymentBSCSDetail payBSCSDetail) throws TechnicalException {
		getPersistenceManager().save(payBSCSDetail);
	}

	public PaymentBSCS savePaiement(Client client, Date trxDate, String chkUm, String glCash,
			String glDis, Long caType, String carem, String cAPP,
			String causerName, String debitInfo1, Integer costCenter, Long convrateTypeGl,
			Long convrateTypeDoc, Double chkAmtGl, Double disAmtGl, Double curAmtGl, Double chkAmtPay, Double disAmtPay, Double curAmtPay,
			Double balanceHome, Long codeRaison) throws TechnicalException {
		PaymentBSCS paiement = new PaymentBSCS();
		Long idPaiment = getPaiementSeq();
		if (idPaiment != null && idPaiment > 0) {
			paiement.setId(idPaiment);
		}
		paiement.setRefClient(client);
		paiement.setDatePaiement(trxDate);
		paiement.setDateReception(trxDate);
		paiement.setNumReference(chkUm);
		paiement.setDateCheque(trxDate);
		paiement.setCodeGL(glCash);
		paiement.setNumGL(glDis);
		paiement.setType(caType);
		paiement.setCaBatch(Long.valueOf(Utils.dateToString(trxDate, Constantes.FORMAT_DATE_DAY)));
		paiement.setCommentaire(carem);
		paiement.setPostingPeriod(cAPP);

		paiement.setCauserName(causerName);
		paiement.setDebitInfo(debitInfo1);
		paiement.setSiModifie(Constantes.PARAM_MODE_FLAG);
		paiement.setCostCenter(costCenter);
		paiement.setRaisonTransaction(codeRaison);
		paiement.setRecVersion(Constantes.VALEUR_1);
		paiement.setConvrateTypeGL(convrateTypeGl);
		paiement.setConvrateTypeDoc(convrateTypeDoc);
		paiement.setMontantRegle(chkAmtGl);
		if (disAmtGl != null) {
			paiement.setCadisamtGL(disAmtGl);
		}

		paiement.setMontantCourant(curAmtGl);
		paiement.setMontantPaye(chkAmtPay);
		if (disAmtPay != null) {
			paiement.setCadisAmtPay(disAmtPay);
		}
		paiement.setCacurAmtPay(curAmtPay);
		paiement.setCabalanceHome(balanceHome);
		getPersistenceManager().save(paiement);

		return paiement;

	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.PaymentBSCSDao#getPaiementByID(java.lang.Long).
	 * @param id
	 * @return
	 * @throws TechnicalException
	 */
	public PaymentBSCS getPaiementByID(Long id) throws TechnicalException {
		return (PaymentBSCS) getPersistenceManager().findById(PaymentBSCS.class, id, false);
	}
	
	/**
	 * Gets the paiement seq.
	 *
	 * @param system the system
	 * @return the paiement seq
	 * @throws TechnicalException the technical exception
	 */
	private Long getPaiementSeq() throws TechnicalException {

		/* for mobile system use nextfree to get next value */
		/* to avoid conflict with other systems using the   */
		/* same table */
		return super.getAppSequence(Constantes.CASHRECEIPT_ALL_SEQUENCE_KEY);
	}
	
	
	/**
	 * 
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @return
	 * @throws TechnicalException
	 */
	private Long getTicklerSeq() throws TechnicalException {

		/* for mobile system use nextfree to get next value */
		/* to avoid conflict with other systems using the   */
		/* same table */
		return super.getAppSequence(Constantes.TICKLER_SEQUENCE_KEY);
	}

	public void savePaiement(PaymentBSCS paiement) throws TechnicalException {
		getPersistenceManager().save(paiement);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.bscs.PaymentBSCSDao#getDetailPaymentByFacture(java.lang.Long).
	 * @param factureId
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public List<PaymentBSCSDetail> getDetailPaymentByFacture(Long factureId)
			throws TechnicalException {
		Criteria crit = getPersistenceManager().createCriteria(PaymentBSCSDetail.class);
		crit.createCriteria("refFacture", "facture");
		crit.add(Restrictions.eq("facture.id", factureId));
		List result = crit.list();
		if (result != null && !result.isEmpty()) {
			return result;
		} else {
			return null;
		}
	}
}
