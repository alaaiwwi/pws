package ma.iam.fidelio.dao;


import ma.iam.common.Constantes;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.dao.PersistenceManager;

public abstract class AbstractDao {
	
	/** Instance du LOGGER */
	protected static final Logger LOGGER = LogManager.getLogger(Constantes.TECHNICAL_FIDELIO_LOGGER_NAME);
	
	/** L'attribut persistenceManagerBSCS. */
	private PersistenceManager persistenceManagerBSCS = null;
	
	/** L'attribut prsistenceManagerFidelio. */
	private PersistenceManager persistenceManagerFidelio = null;
	
	/**
	 * Modifier l'objet persistenceManagerBSCS.
	 * @param : bscsMobileManager
	 */
	public final void setPersistenceManagerBSCS(PersistenceManager bscsMobileManager) {
		this.persistenceManagerBSCS = bscsMobileManager;
	}
	
	/**
	 * Retourner l'objet persistenceManagerBSCS.
	 * @return : PersistenceManagerBSCS
	 */
	public final PersistenceManager getPersistenceManagerBSCS() {
		return persistenceManagerBSCS;
	}
	
	/**
	 * Modifier l'objet persistenceManagerFidelio.
	 * @param : fidelioMobileManager
	 */
	public final void setPersistenceManagerFidelio(PersistenceManager fidelioMobileManager) {
		this.persistenceManagerFidelio = fidelioMobileManager;
	}
	
	/**
	 * Retourner l'objet persistenceManagerFidelio.
	 * @return : PersistenceManagerFidelio
	 */
	public final PersistenceManager getPersistenceManagerFidelio() {
		return persistenceManagerFidelio;
	}
	
}