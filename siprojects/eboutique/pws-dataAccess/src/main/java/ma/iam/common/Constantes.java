package ma.iam.common;

public abstract class Constantes {
	
	/* **************** La Liste des Constantes **************** */
	
	/** La constante CLIENT EXLUS IMPAYES. */
	public static final Integer[] CLIENTS_EXCLUS_IMPAYES = { 4, 6, 9, 12 };
	
	/** La constante CLIENT_STATUT_ACTIF EXLUS IMPAYES. */
	public static final String CLIENT_STATUT_ACTIF = "ACTIF";
	
	/** La constante CODE_CATEGORIE_ARTICLE_CADEAU. */
	public static final String CODE_CATEGORIE_ARTICLE_CADEAU = "C";
	
	/** La constante CODE_CATEGORIE_ARTICLE_SERVICE. */
	public static final String CODE_CATEGORIE_ARTICLE_SERVICE = "S";
	
	/** La constante CODE_MODE_ENGAGEMENT_12. */
	public static final Integer CODE_ENGAGEMENT_12_MOIS = 1;
	
	/** La constante CODE_MODE_ENGAGEMENT_24. */
	public static final Integer CODE_ENGAGEMENT_24_MOIS = 2;
	
	/** La constante CODE_POSTE_SANS_TERMINAL. */
	public static final String CODE_POSTE_SANS_TERMINAL = "SansTerminal";
	
	/** La constante CONTRAT_STATUT_ACTIF. */
	public static final String CONTRAT_STATUT_ACTIF = "A";
	
	/** La constante CONTRAT_STATUT_SUSPENDU. */
	public static final String CONTRAT_STATUT_SUSPENDU = "S";
	
	/** La constante DATE_FORMAT. */
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	
	/** La constante ENGLISH DATE FORMAT. */
	public static final String ENGLISH_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/** La constante FLAG_STATUT_FACTURE. */
	public static final String FACTURE_STATUT_IN = "IN";
	
	/** La constante MODEL_POST_SGE. */
	public static final String MODEL_POST_SGE = "SGE";
	
	/** La constante PLAN_TARIFAIRE_EXCLUS. */
	public static final String[] PLAN_TARIFAIRE_EXCLUS = { "FOLIB" };
	
	/** La constante REMISE_PRIVILEGE. */
	public static final float REMISE_PRIVILEGE = 300f;
	
	/** La constante STATUS_AFFAIRE_ANNULE. */
	public static final String STATUS_AFFAIRE_ANNULE = "A";
	
	/** La constante STATUTS_AFFAIRE_CREE. */
	public static final String STATUS_AFFAIRE_CREE = "C";
	
	/** La constante STATUS_AFFAIRE_LIVRE. */
	public static final String STATUS_AFFAIRE_LIVRE = "L";

	public static final String TYPE_AFFAIRE_EB_AVEC_ENGAGEMENT_CODE = "CPAEEB";
	
	/** La constante TYPE_CATALOGUE_ENGAGEMENT. */
	public static final String TYPE_CATALOGUE_ENGAGEMENT = "ENGAGEMENT";
	
	/** La constante VALEUR_MODE_ENGAGEMENT_24. */
	public static final Integer ID_ENGAGEMENT_BSCS_24_MOIS = 3;

	/** La constante VALEUR_MODE_ENGAGEMENT_12. */
	public static final Integer ID_ENGAGEMENT_BSCS_12_MOIS = 14;
	
	/* **************** Les Requêtes **************** */
	
	/** La constante REQ_CATEGORIE_CLIENT_BY_PRICE_GROUP. */
	public static final String REQ_CATEGORIE_CLIENT_BY_PRICE_GROUP = "getCategorieClientByPriceGroup";
	
	/** La constante REQ_ENGAGEMENT_CONTRAT. */
	public static final String REQ_ENGAGEMENT_CONTRAT = "engagement.contrat";
	
	/** La constante REQ_GET_ALL_CATALOGUE. */
	public static final String REQ_GET_ALL_CATALOGUE = "getAllCatalogue";
	
	/** La constante REQ_GET_LINE_CATALOGUE. */
	public static final String REQ_GET_LINE_CATALOGUE = "getLineCatalogue";
	
	/** La constante REQ_LISTE_CONTRAT_BY_NUMAPPEL. */
	public static final String REQ_LISTE_CONTRAT_BY_NUMAPPEL = "liste.contrat.by.numappel";
	
	/** La constante REQ_LISTE_NUMAPPEL_BY_CLIENT. */
	public static final String REQ_LISTE_NUMAPPEL_BY_CLIENT = "liste.numappels.by.client";
	
	/** La constante REQ_NOMBRE_COMMANDES_EN_INSTANCE_ENG. */
	public static final String REQ_NOMBRE_COMMANDES_EN_INSTANCE_ENG = "nombre.commandes.en.instance.engagement";
	
	/** La constante REQ_NOMBRE_COMMANDES_LIVREES_ENG. */
	public static final String REQ_NOMBRE_COMMANDES_LIVREES_ENG = "nombre.commandes.livrees.engagement";
	
	
	/** La constante REQ_PRIX_PACK_XMOIS. */
	public static final String REQ_PRIX_PACK_XMOIS = "prix.pack.Xmois";
	
	/** La constante REQ_SOUS_CATEGORIE_BY_PRICE_GROUP. */
	public static final String REQ_SOUS_CATEGORIE_BY_PRICE_GROUP = "getSousCategorieByPriceGroup";
	
	/* **************** Code Erreur **************** */

	public static final String ERREUR_CODE_01 = "01";
	
	public static final String ERREUR_CODE_02 = "02";
	
	public static final String ERREUR_CODE_03 = "03";
	
	public static final String ERREUR_CODE_04 = "04";
	
	public static final String ERREUR_CODE_05 = "05";
	
	public static final String ERREUR_CODE_06 = "06";
	
	public static final String ERREUR_CODE_07 = "07";
	
	public static final String ERREUR_CODE_08 = "08";
	
	public static final String ERREUR_CODE_09 = "09";
	
	public static final String ERREUR_CODE_10 = "10";
	
	public static final String ERREUR_CODE_11 = "11";
	
	public static final String ERREUR_CODE_12 = "12";
	
	public static final String ERREUR_CODE_13 = "13";
	
	public static final String ERREUR_CODE_14 = "14";
	
	public static final String ERREUR_CODE_15 = "15";
	
	public static final String ERREUR_CODE_16 = "16";
	
	public static final String ERREUR_CODE_17 = "17";
	
	public static final String ERREUR_CODE_18 = "18";
	
	public static final String ERREUR_CODE_19 = "19";
	
	public static final String ERREUR_CODE_20 = "20";
	
	public static final String ERREUR_CODE_21 = "21";
	
	public static final String ERREUR_CODE_22 = "22";
	
	public static final String ERREUR_CODE_23 = "23";
	
	public static final String ERREUR_CODE_24 = "24";
	
	public static final String ERREUR_CODE_25 = "25";
	
	public static final String ERREUR_CODE_26 = "26";
	
	public static final String ERREUR_CODE_27 = "27";
	
	public static final String ERREUR_CODE_28 = "28";
	
	public static final String ERREUR_CODE_29 = "29";
	
	public static final String ERREUR_CODE_30 = "30";
	
	public static final String ERREUR_CODE_31 = "31";
	
	public static final String ERREUR_CODE_32 = "32";
	
	public static final String ERREUR_CODE_33 = "33";
	
	public static final String ERREUR_CODE_34 = "34";
	
	public static final String ERREUR_ND_SANS_ENGAGEMENT_ECHU = "nd.sans.engagement.echu";
	

	/* **************** Message Erreur **************** */
	
	public static final String ERR_AUTH_04 = "erreur.auth.04";
	
	public static final String ERREUR_TYPE_AFFAIRE_EB_NON_PARAMETRE = "type.affaire.eb.non.parametre";
	
	public static final String CONTRAT_INCOHERENT_DEMANDES_CONVERSION = "contrat.incoherent.demandes.conversion";
	
	public static final String ERREUR_ID_ARTICLE_INEXISTANT = "id.article.inexistant";
	
	public static final String ERREUR_CODE_AGENCE_INEXISTANT = "code.agence.inexistant";
	
	public static final String CONTRAT_COMMANDE_EN_INSTANCE = "contrat.commande.en.instance";
	
	public static final String NOMBRE_CONVERSION_MOIS_NON_PARAMETRE = "max.conversion.mensuel.non.parametre";
	
	public static final String SEUIL_MAX_IMPAYES_ATTEINT = "seuil.max.impayes.atteint";
	
	public static final String ERREUR_ARTICLE_SUSPENDU = "article.suspendu";
	
	public static final String NUMERO_COMMANDE_OBLIGATOIRE = "numero.commande.obligatoire";
	
	public static final String DELAI_CONVERSION_NON_ECOULE = "delai.conversion.non.ecoule";
	
	public static final String NB_COMMANDE_LIVRE_SUPERIEUR_NB_CONVERSION_AUTORISE = "nb.commande.livre.superieur.nb.conversion.autorise";
	
	public static final String ERREUR_COMPLEMENT_OBLIGATOIRE = "complement.obligatoire";
	
	public static final String MODE_ENGAGEMENT_NON_PARAMETRE = "mode.engagement.non.parametre";
	
	public static final String PARAMETRE_RACHAT_NON_PARAMETRE = "parametre.rachat.non.parametre";
	
	public static final String CODE_AGENCE_OBLIGATOIRE = "code.agence.obligatoire";
	
	public static final String ERREUR_CODE_ENGAGEMENT_OBLIGATOIRE = "code.engagement.obligatoire";
	
	public static final String ERREUR_ID_POSTE_OBLIGATOIRE = "id.poste.obligatoire";
	
	public static final String ERREUR_MODE_ENGAGEMENT_INEXISTANT = "mode.engagement.inexistant";
	
	public static final String ERREUR_NUM_APPEL_OBLIGATOIRE = "num.appel.obligatoire";
	public static final String ERREUR_TYPE_ENGAGEMENT_INVALIDE = "type.engagement.invalide";
	
	public static final String PARAMETRE_SEUIL_MAX_IMPAYES_PAS_CONFIGURE = "parametre.seuil.max.impayes.pas.configure";
	
	public static final String ERREUR_ARTICLE_INDISPONIBLE_MODE_ENGAGEMENT = "article.indisponible.mode.engagement";
	
	public static final String NOMBRE_POINTS_ENCOURS_OBLIGATOIRE = "nombre.points.encours.obligatoire";
	
	public static final String NOMBRE_POINTS_ACQUIS_OBLIGATOIRE = "nombre.points.acquis.obligatoire";
	
	public static final String DATE_DEBUT_OBLIGATOIRE = "date.debut.obligatoire";
	
	public static final String INTERVAL_DATE_ERRONE = "interval.date.errone";
	
	public static final String ERREUR_CLIENT_INCONNU = "client.inconnu";
	
	public static final String ERREUR_COMMANDE_INEXISTANTE = "commande.inexistante";
	
	public static final String AUCUNE_COMMANDE_LIVREE = "aucune.commande.livree";
	
	public static final String ERREUR_COMMANDE_LIVRE = "commande.livre";
	
	public static final String ERREUR_COMMANDE_ANNULE = "commande.annule";
	
	public static final String ERREUR_NUM_APPEL_INEXISTANT = "nd.inexistant";
	
	public static final String ERREUR_COMPTE_POINTS_EXCLU = "compte.points.exclu";
	
	public static final String ERREUR_LIGNE_INACTIVE = "ligne.inactive";
	
	public static final String ERREUR_PLAN_TARIFAIRE_INCOMPATIBLE_ENGAGEMENT = "plan.tarifaire.incompatible.engagement";
	
	public static final String ERREUR_SEUIL_MIN_ENGAGEMENT_NON_ATTEINT = "seuil.min.engagement.non.atteint";
	
	public static final String ERREUR_ARTICLE_NON_DISPO_CLIENT = "article.non.disponible.client";
	
	public static final String ERREUR_NUM_COMMANDE_EXISTANT = "num.commande.existant";
	
	public static final String ERREUR_OFFRE_SANS_TERMINAL = "offre.sans.terminale";
	
	public static final String ERREUR_COMPLEMENT_POINTS_UTILSE_INCORRECT = "complement.points.utilse.incorrect";
	public static final String ERREUR_PARAM_DELAI_ADHESION_INEXISTANT = "param.delai.adhesion.inexistant";
	public static final String ERREUR_POINTS_POSTE_NU_INEXISTANT = "points.poste.nu.inexistant";
	public static final String ERREUR_POINTS_ENGAGEMENT_INEXISTANT = "points.engagement.inexistant";
	public static final String ERREUR_ETAT_SUPENSION_INEXISTANT = "etat.supension.inexistant";
	public static final String ERREUR_LIBELLE_POSTE_INEXISTANT = "libelle.poste.inexistant";
	public static final String ERREUR_CODE_PRODUIT_INEXISTANT = "code.produit.inexistant";
	public static final String ERREUR_COMPTE_FIDELIO_INEXISTANT = "erreur.compte.fidelio.inexistant";
	
	public static final String ERREUR_TECHNIQUE = "erreur.technique";
	
	public static final String OPERATION_OK ="operation.ok";
	
	public static final String REQ_GET_CONTRAT_BY_NUM_APPEL = "contrat.by.numappel";
	
	/* **************** Les Logs **************** */
	
	/** La constante TECHNICAL_LOGGER_NAME. */
	public static final String TECHNICAL_FIDELIO_LOGGER_NAME = "ma.iam.fidelio.dao.ws";

	/** La constante TRACE_LOGGER_NAME. */
	public static final String TRACE_FIDELIO_LOGGER_NAME = "ma.iam.fidelio.dao.trace";

	public static final Integer MODE_ENGAGEMENT_FIDELIO_12M_DUREE = 12;
	public static final Integer MODE_ENGAGEMENT_FIDELIO_24M_DUREE = 24;
	

	public static final String ERREUR_COMMANDE_NA_APPARTIENT_PAS_CLIENT = "commande.na.appartient.pas.client";

	public static final String CATEGORIE_CLIENT_GRAND_PUBLIC = "GP";

	public static final String REQ_PRIX_POSTE = "prix.poste";
	
	public static final String BY_PASS_OK="1";
	public static final String BY_PASS_NOK="-1";
	
	/******************************
	 * payment
	 *****************************/


	
	/** Flag résponsable de paiement */
	public static final String FLAG_RESP_PAY = "X";
	
	public static final String ERR_TECHNIQUE = "err.technique";
	public static final String ERR_ND_CLIENT_INCONNU = "nd.client.inconnu";
	public static final String ERR_TYPE_AVANCE_INCONNU = "type.avance.inconnu";
	public static final String ERR_MODE_PAIEMENT_INCONNU = "mode.paiement.inconnu";
	public static final String ERR_AVANCE_DEJA_SAISIE = "avance.deja.saisie";
	public static final String ERR_AUCUNE_AVANCE_TROUVE = "aucune.avance.trouve";
	public static final String ERR_AVANCE_ANNULE = "avance.deja.annule";
	public static final String ERR_AUCUNE_REGIE_UTILISATEUR = "aucune.regie.utilisateur";
	public static final String ERR_AUTHORISATION = "erreur.authorisation";
	
	public static final String ERR_CODE_03 = "03";
	public static final String ERR_CODE_04 = "04";
	public static final String ERR_CODE_05 = "05";
	public static final String ERR_CODE_01 = "01";
	public static final String ERR_CODE_02 = "02";
	public static final String ERR_CODE_06 = "06";
	
	/** Système Mobile */
	public static final String SYSTEME_MOBILE = "M";
	
	/** Valeur VIDE */
	public static final String VAL_VIDE = "";
	
	/*
	 * ############ Status Paiement #############
	 */
	public static final String STATUS_PAIEMENT_ENCAISSE = "E";

	public static final String STATUS_PAIEMENT_REJETE = "R";

	public static final String STATUS_PAIEMENT_ANNULE = "A";

	public static final String STATUS_PAIEMENT_SOUMMIS = "S";

	public static final String STATUS_PAIEMENT_DEPOSE = "D";

	public static final String STATUS_PAIEMENT_VALIDE = "V";

	/** Status CO*/
	public static final String FACTURE_STATUS_CO = "CO";
	
	/** Status IN*/
	public static final String FACTURE_STATUS_IN = "IN";

	public static final Object BOOLEAN_YES = "Y";
	
	public static final String TICKET_STATUS_NOTE = "NOTE";
	
	public static final String TICKET_FOLLOW_UP_STATUS_N = "N";

	public static final Long VALEUR_1 = Long.valueOf(1);
	
	public static final Long TICKET_PRIORITY_5 = Long.valueOf(5);
	
	public static final String AR_DEPOSIT_OUT = "AR DEPOSIT OUT";
	
	public static final String AR_FLAG_B = "B";
	
	public static final String PATTERN_NUM_BORDEREAU = "0000000";
	
	public static final String PARAM_PAIEMENT_DEBIT_INFO = "CUSTDB";

	public static final String GL_DIS_6386000000 = "6386000000";
	
	public static final String GLAR_44211001 = "44211001";
	
	public static final String FACTURE_MODE_X = "X";
	
	public static final String PARAM_MODE_FLAG = "X";
	
	public static final Long DEVISE_MAROC_DIRHAM = Long.valueOf(91);
	
	public static final String CASHRECEIPT_ALL_SEQUENCE_KEY = "MAX_CAXACT";
	
	/** sequence ORDERHDR_ALL*/
	public static final String ORDERHDR_ALL_SEQUENCE_KEY    = "MAX_OHXACT";
	
	/** sequence Tickler */
	public static final String TICKLER_SEQUENCE_KEY         = "MAX_TICKLER_NUMBER";
	
	/** Technical logger name. */
	public static final String TECHNICAL_PAYMENT_LOGGER_NAME = "ma.iam.paiement.ws";
	
	public static final String TRACE_PAYMENT_LOGGER_NAME = "ma.iam.paiement.trace";
	
	public static final String FORMAT_DATE_DAY = "DDD";
	
	public static final Double VAL_DOUBLE_0 = Double.valueOf(0);
	
	public static final Long CA_REASON_CODE_19 = Long.valueOf(19);
	
	public static final String CA_TYPE_3 = "3";
	
	public static final Long CA_REASON_CODE_20 = Long.valueOf(20);
	
	public static final String CAREM_ANNULATION_SUFFIX = " - ADJ";
	
	public static final String TICKET_DESC_AR_BOUNCE_CHECK = "AR BOUNCE CHECK";
	
	public static final Long TICKET_PRIORITY_8 = Long.valueOf(8);
	
	public static final String TICKET_CODE_SYSTEM = "SYSTEM";

	public static final String CMS_CONNEXION_OK = "OK";
	
	public static final String REQ_PAYMENT_GE_BY_COMMANDE = "paymentGEByCommandeEB";
	
	public static final String REQ_PAIEMENT_GE_NEXT_BORDNUM = "nextBordNum";
	
	public static final String REQ_PERIODE_NEW_FACTURE = "getPeriodeFacture";

	public static final String REQ_GET_BSCS_USER = "getUserBSCS";

	public static final String REQ_LOCATION_BY_COST_CENTER = "locationByCostCenter";

		
}
