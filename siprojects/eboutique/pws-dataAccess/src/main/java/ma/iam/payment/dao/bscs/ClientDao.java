package ma.iam.payment.dao.bscs;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.bscs.Client;


/**
 * DAO Client.
 * 
 * @author Atos
 * @version 1.0
 */
public interface ClientDao {

	/**
	 * Récupere un client par son id.
	 * 
	 * @param idClient the id client
	 * @return Client
	 * @throws TechnicalException 
	 */
	Client getClientById(Long idClient) throws TechnicalException;

	/**
	 * Récupere le responsable de paiement du client.
	 * 
	 * @param codeClient the code client
	 * 
	 * @return Client
	 * 
	 * @throws TechnicalException the technical exception
	 */
	Client getResponsablePaiement(String codeClient) throws TechnicalException;
	
	/**
	 * Récuperer un client par son code.
	 * 
	 * @param codeClient the code client
	 * @return the client
	 * @throws TechnicalException the technical exception
	 */
	public Client getClient(String codeClient) throws TechnicalException;



	/**
	 * Mise à jour de la balance client.
	 * 
	 * @param idClient the client
	 * @param montant the montant
	 * 
	 * @throws TechnicalException the technical exception
	 */
	void updateBalanceClient(Long idClient, Double montant) throws TechnicalException;

	/**
	 * Recherche Client par numéro d'Appel.
	 * 
	 * @param dn the num appel
	 * 
	 * @return Client
	 * 
	 * @throws TechnicalException the technical exception
	 */
	Client getClientByDN(String dn) throws TechnicalException;
}