package ma.iam.payment.dao.ge.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.ge.LocationDao;
import ma.iam.payment.domaine.ge.Location;

public class LocationDaoImpl extends AbstractDao implements LocationDao {
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.bscs.UserDao#getLocationByCostId(java.lang.Integer).
	 * @param costId
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Location getLocationByCostId(Integer costId) throws TechnicalException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("costId", costId);
		List<Location> list = getPersistenceManager().getNamedQuery(Constantes.REQ_LOCATION_BY_COST_CENTER, params);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
