/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.contrat;

import java.util.List;

import ma.iam.pws.grc.bean.ContractHistoryWS;
import ma.iam.pws.grc.bean.PeripheriqueHistoryWS;
import ma.iam.pws.grc.bean.PlanTarifaireHistoryWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CustAcctDAOMobileImpl.
 */
@Repository
public class ContractDAOCommon extends BaseDAOFixe {

	private static final Logger LOG = LoggerFactory
			.getLogger(ContractDAOFixeImpl.class);

	/** La constante GET_CONTRACT_HISTORY. */
	protected static final String GET_CONTRACT_HISTORY = ContractDAOFixeImpl.class
			.getName() + ".GET_CONTRACT_HISTORY";

	/** La constante GET_PERIPHERIQUE_HISTORY. */
	protected static final String GET_PERIPHERIQUE_HISTORY = ContractDAOFixeImpl.class
			.getName() + ".GET_PERIPHERIQUE_HISTORY";

	/** La constante GET_PLAN_TARIFAIRE_HISTORY. */
	protected static final String GET_PLAN_TARIFAIRE_HISTORY = ContractDAOFixeImpl.class
			.getName() + ".GET_PLAN_TARIFAIRE_HISTORY";

	protected static final String GET_PERIPHERIQUE_HISTORY_DATA_INTERNET = ContractDAOFixeImpl.class
			.getName() + ".GET_PERIPHERIQUE_HISTORY_DATA_INTERNET";
	protected static final String GET_PERIPHERIQUE_HISTORY_DATA = ContractDAOFixeImpl.class
			.getName() + ".GET_PERIPHERIQUE_HISTORY_DATA";
	protected static final String GET_PERIPHERIQUE_HISTORY_INTERNET = ContractDAOFixeImpl.class
			.getName() + ".GET_PERIPHERIQUE_HISTORY_INTERNET";

	public List<ContractHistoryWS> findContractHistory(
			final String contractIdBsc) {
		LOG.debug("--> findContractHistory contractIdBsc {}", contractIdBsc);
		final List<ContractHistoryWS> listContractHistory;
		final String sql = CustomSQLUtil.get(GET_CONTRACT_HISTORY);

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"contractIdBsc", contractIdBsc);

		final CustomBeanPropertyRowMapper<ContractHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<ContractHistoryWS>(
				ContractHistoryWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContractHistory = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findContractHistory");
		return listContractHistory;
	}

	public List<PeripheriqueHistoryWS> findPeripheriqueHistory(
			final String contractIdBsc) {
		LOG.debug("--> findPeripheriqueHistory contractIdBsc {}", contractIdBsc);
		final List<PeripheriqueHistoryWS> listContractHistory;
		final String sql = CustomSQLUtil
				.get(GET_PERIPHERIQUE_HISTORY_DATA_INTERNET);

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"contractIdBsc", contractIdBsc);

		final CustomBeanPropertyRowMapper<PeripheriqueHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PeripheriqueHistoryWS>(
				PeripheriqueHistoryWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContractHistory = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug("<-- findPeripheriqueHistory");
		return listContractHistory;
	}

	public List<PlanTarifaireHistoryWS> findPlanTarifaireHistory(
			final String contractIdBsc) {
		LOG.debug("--> findPlanTarifaireHistory contractIdBsc {}",
				contractIdBsc);
		final List<PlanTarifaireHistoryWS> listContractHistory;
		final String sql = CustomSQLUtil.get(GET_PLAN_TARIFAIRE_HISTORY);

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"contractIdBsc", contractIdBsc);

		final CustomBeanPropertyRowMapper<PlanTarifaireHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PlanTarifaireHistoryWS>(
				PlanTarifaireHistoryWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContractHistory = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findPlanTarifaireHistory");
		return listContractHistory;
	}

}
