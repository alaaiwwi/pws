package ma.iam.pws.grc.bean;

public class SearchCustomerWS {

	private String nd;
	private String nom;
	private String prenom;
	private String ville;
	private String codeCategorie;
	private String numPieceIdentite;
	private String co_id;
	public String getNd() {
		return nd;
	}
	public void setNd(String nd) {
		this.nd = nd;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getCodeCategorie() {
		return codeCategorie;
	}
	public void setCodeCategorie(String codeCategorie) {
		this.codeCategorie = codeCategorie;
	}
	public String getNumPieceIdentite() {
		return numPieceIdentite;
	}
	public void setNumPieceIdentite(String numPieceIdentite) {
		this.numPieceIdentite = numPieceIdentite;
	}
	public String getCo_id() {
		return co_id;
	}
	public void setCo_id(String co_id) {
		this.co_id = co_id;
	}
	@Override
	public String toString() {
		return "SearchCustomerWS [nd=" + nd + ", nom=" + nom + ", prenom="
				+ prenom + ", ville=" + ville + ", codeCategorie="
				+ codeCategorie + ", numPieceIdentite=" + numPieceIdentite
				+ ", co_id=" + co_id + "]";
	}
	
	
}
