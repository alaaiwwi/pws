/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class PlanTarifaireHistoryWS.
 */
public class PlanTarifaireHistoryWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 1720393848190912514L;

    /** The date plan tarifaire. */
    private String datePlanTarifaire;

    /** The plan tarifaire. */
    private String planTarifaire;

    /** The cree par. */
    private String creePar;

    /**
     * Gets the number.
     * 
     * @return the number
     */
    public String getDatePlanTarifaire() {
        return datePlanTarifaire;
    }

    /**
     * Sets the number.
     * 
     * @param datePlanTarifaire
     *            the new number
     */
    public void setDatePlanTarifaire(final String datePlanTarifaire) {
        this.datePlanTarifaire = datePlanTarifaire;
    }

    /**
     * Gets the statut.
     * 
     * @return the statut
     */
    public String getPlanTarifaire() {
        return planTarifaire;
    }

    /**
     * Sets the statut.
     * 
     * @param planTarifaire
     *            the new statut
     */
    public void setPlanTarifaire(final String planTarifaire) {
        this.planTarifaire = planTarifaire;
    }

    /**
     * Gets the cree par.
     * 
     * @return the cree par
     */
    public String getCreePar() {
        return creePar;
    }

    /**
     * Sets the cree par.
     * 
     * @param creePar
     *            the new cree par
     */
    public void setCreePar(final String creePar) {
        this.creePar = creePar;
    }

}
