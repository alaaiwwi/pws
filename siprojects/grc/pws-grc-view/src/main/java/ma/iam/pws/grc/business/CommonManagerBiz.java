/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import ma.iam.pws.grc.constants.CustAcctCategoryTypeEnum;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import ma.iam.pws.grc.dao.contrat.ContractDAODataImpl;
import ma.iam.pws.grc.dao.contrat.ContractDAOFixeImpl;
import ma.iam.pws.grc.dao.contrat.ContractDAOInternetImpl;
import ma.iam.pws.grc.dao.contrat.ContractDAOMobileImpl;
import ma.iam.pws.grc.dao.contrat.IContractDAO;
import ma.iam.pws.grc.dao.custacct.CustAcctDAODataImpl;
import ma.iam.pws.grc.dao.custacct.CustAcctDAOInternetImpl;
import ma.iam.pws.grc.dao.custacct.CustAcctDAOMobileImpl;
import ma.iam.pws.grc.dao.custacct.ICustAcctDAO;
import ma.iam.pws.grc.dao.detailsappel.DetailsAppelDAODataImpl;
import ma.iam.pws.grc.dao.detailsappel.DetailsAppelDAOFixeImpl;
import ma.iam.pws.grc.dao.detailsappel.DetailsAppelDAOInternetImpl;
import ma.iam.pws.grc.dao.detailsappel.DetailsAppelDAOMobileImpl;
import ma.iam.pws.grc.dao.detailsappel.IDetailsAppelDAO;
import ma.iam.pws.grc.dao.fidelio.FidelioDAODataImpl;
import ma.iam.pws.grc.dao.fidelio.FidelioDAOFixeImpl;
import ma.iam.pws.grc.dao.fidelio.FidelioDAOInternetImpl;
import ma.iam.pws.grc.dao.fidelio.FidelioDAOMobileImpl;
import ma.iam.pws.grc.dao.fidelio.IFidelioDAO;
import ma.iam.pws.grc.dao.invoice.IInvoiceDAO;
import ma.iam.pws.grc.dao.invoice.InvoiceDAOFixeImpl;
import ma.iam.pws.grc.dao.invoice.InvoiceDAOMobileImpl;
import ma.iam.pws.grc.dao.memo.IMemoDAO;
import ma.iam.pws.grc.dao.memo.MemoDAODataImpl;
import ma.iam.pws.grc.dao.memo.MemoDAOFixeImpl;
import ma.iam.pws.grc.dao.memo.MemoDAOInternetImpl;
import ma.iam.pws.grc.dao.memo.MemoDAOMobileImpl;
import ma.iam.pws.grc.dao.service.IServiceDAO;
import ma.iam.pws.grc.dao.service.ServiceDAODataImpl;
import ma.iam.pws.grc.dao.service.ServiceDAOInternetImpl;
import ma.iam.pws.grc.dao.service.ServiceDAOMobileImpl;
import ma.iam.pws.grc.dao.sms.ISmsDAO;
import ma.iam.pws.grc.dao.sms.SmsDAOMobileImpl;
import ma.iam.pws.grc.dao.statsclient.IStatsClientDAO;
import ma.iam.pws.grc.dao.statsclient.StatsClientDAODataImpl;
import ma.iam.pws.grc.dao.statsclient.StatsClientDAOFixeImpl;
import ma.iam.pws.grc.dao.statsclient.StatsClientDAOInternetImpl;
import ma.iam.pws.grc.dao.statsclient.StatsClientDAOMobileImpl;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.springframework.beans.factory.annotation.Autowired;

// TODO: Auto-generated Javadoc
/**
 * The Class CommonManagerBiz.
 */
public class CommonManagerBiz {

	// Constantes

	/** La constante SCORING_UN. */
	protected static final int SCORING_UN = 1;

	/** La constante SCORING_DEUX. */
	protected static final int SCORING_DEUX = 2;

	/** La constante SCORING_TROIS. */
	protected static final int SCORING_TROIS = 3;

	/** La constante SCORING_QUATRE. */
	protected static final int SCORING_QUATRE = 4;

	/** La constante UN_AN. */
	protected static final int UN_AN = 12;

	/** La constante DEUX_ANS. */
	protected static final int DEUX_ANS = 24;

	/** La constante CINQ_ANS. */
	protected static final int CINQ_ANS = 60;

	/** La constante PALIER_UN. */
	protected static final int PALIER_UN = 200;

	/** La constante PALIER_DEUX. */
	protected static final int PALIER_DEUX = 500;

	/** La constante PALIER_TROIS. */
	protected static final int PALIER_TROIS = 1500;

	/** La constante PALIER_QUATRE. */
	protected static final int PALIER_QUATRE = 4000;

	// IOC des DAOS par produit

	/** The demande dao fixe impl. */
	// @Autowired
	// private transient DemandeDAOFixeImpl demandeDAOFixeImpl;

	/** The demande dao mobile impl. */
	// @Autowired
	// private transient DemandeDAOMobileImpl demandeDAOMobileImpl;

	/** The memo dao fixe impl. */
	@Autowired
	private transient MemoDAOFixeImpl memoDAOFixeImpl;

	/** The micro cellule dao fixe impl. */

	/** The details appel fixe impl. */
	@Autowired
	private transient DetailsAppelDAOFixeImpl detailsAppelFixeImpl;

	/** The stats client dao fixe impl. */
	@Autowired
	private transient StatsClientDAOFixeImpl statsClientDAOFixeImpl;

	/** The fidelio dao fixe impl. */
	@Autowired
	private transient FidelioDAOFixeImpl fidelioDAOFixeImpl;

	/** The memo dao mobile impl. */
	@Autowired
	private transient MemoDAOMobileImpl memoDAOMobileImpl;

	/** The service dao mobile impl. */
	@Autowired
	private transient ServiceDAOMobileImpl serviceDAOMobileImpl;

	/** The details appel mobile impl. */
	@Autowired
	private transient DetailsAppelDAOMobileImpl detailsAppelMobileImpl;

	/** The stats client dao mobile impl. */
	@Autowired
	private transient StatsClientDAOMobileImpl statsClientDAOMobileImpl;

	/** The fidelio dao mobile impl. */
	@Autowired
	private transient FidelioDAOMobileImpl fidelioDAOMobileImpl;

	/** The memo dao internet impl. */
	@Autowired
	private transient MemoDAOInternetImpl memoDAOInternetImpl;

	/** The service dao internet impl. */
	@Autowired
	private transient ServiceDAOInternetImpl serviceDAOInternetImpl;

	/** The details appel internet impl. */
	@Autowired
	private transient DetailsAppelDAOInternetImpl detailsAppelInternetImpl;

	/** The stats client dao internet impl. */
	@Autowired
	private transient StatsClientDAOInternetImpl statsClientDAOInternetImpl;

	/** The fidelio dao internet impl. */
	@Autowired
	private transient FidelioDAOInternetImpl fidelioDAOInternetImpl;

	/** The memo dao data impl. */
	@Autowired
	private transient MemoDAODataImpl memoDAODataImpl;

	/** The service dao data impl. */
	@Autowired
	private transient ServiceDAODataImpl serviceDAODataImpl;

	/** The details appel data impl. */
	@Autowired
	private transient DetailsAppelDAODataImpl detailsAppelDataImpl;

	/** The stats client dao data impl. */
	@Autowired
	private transient StatsClientDAODataImpl statsClientDAODataImpl;

	/** The fidelio dao data impl. */
	@Autowired
	private transient FidelioDAODataImpl fidelioDAODataImpl;

	/** The contract dao fixe impl. */
	@Autowired
	private transient ContractDAOFixeImpl contractDAOFixeImpl;

	/** The contract dao internet impl. */
	@Autowired
	private transient ContractDAOInternetImpl contractDAOInternetImpl;

	/** The contract dao mobile impl. */
	@Autowired
	private transient ContractDAOMobileImpl contractDAOMobileImpl;

	/** The contract dao data impl. */
	@Autowired
	private transient ContractDAODataImpl contractDAODataImpl;

	/** The cust acct dao mobile impl. */
	@Autowired
	private transient CustAcctDAOMobileImpl custAcctDAOMobileImpl;

	/** The cust acct dao internet impl. */
	@Autowired
	private transient CustAcctDAOInternetImpl custAcctDAOInternetImpl;

	/** The cust acct dao data impl. */
	@Autowired
	private transient CustAcctDAODataImpl custAcctDAODataImpl;

	/** The sms dao mobile impl. */
	@Autowired
	private transient SmsDAOMobileImpl smsDAOMobileImpl;

	/** The invoice dao fixe impl. */
	@Autowired
	private transient InvoiceDAOFixeImpl invoiceDAOFixeImpl;

	/** The invoice dao mobile impl. */
	@Autowired
	private transient InvoiceDAOMobileImpl invoiceDAOMobileImpl;

	// DAOs for product (Fixe / Mobile / Internet / Data)
	/**
	 * Gets the contract dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the contract dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IContractDAO getContractDAOForProduct(final int product)
			throws GrcWsException {
		IContractDAO iContractDAO = null;

		if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
			iContractDAO = contractDAOFixeImpl;
		} else if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iContractDAO = contractDAOMobileImpl;
		} else if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
			iContractDAO = contractDAOInternetImpl;
		} else if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			iContractDAO = contractDAODataImpl;
		}

		if (null == iContractDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iContractDAO;
	}

	/**
	 * Gets the memo dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the memo dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IMemoDAO getMemoDAOForProduct(final int product)
			throws GrcWsException {
		IMemoDAO iMemoDAO = null;

		if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
			iMemoDAO = memoDAOFixeImpl;
		} else if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iMemoDAO = memoDAOMobileImpl;
		} else if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
			iMemoDAO = memoDAOInternetImpl;
		} else if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			iMemoDAO = memoDAODataImpl;
		}
		if (null == iMemoDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iMemoDAO;
	}

	/**
	 * Gets the service dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the service dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IServiceDAO getServiceDAOForProduct(final int product)
			throws GrcWsException {
		IServiceDAO iServiceDAO = null;

		if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iServiceDAO = serviceDAOMobileImpl;
		} else if ((ProduitTypeEnum.FIXE.getIdTypeProduit() == product)
				|| (ProduitTypeEnum.INTERNET.getIdTypeProduit() == product)
				|| (product == ProduitTypeEnum.DATA.getIdTypeProduit())) {
			iServiceDAO = serviceDAOInternetImpl;
		}
		if (null == iServiceDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iServiceDAO;
	}

	/**
	 * Gets the details appel dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the details appel dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IDetailsAppelDAO getDetailsAppelDAOForProduct(final int product)
			throws GrcWsException {
		IDetailsAppelDAO iDetailsAppelsDAO = null;

		if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
			iDetailsAppelsDAO = detailsAppelFixeImpl;
		} else if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iDetailsAppelsDAO = detailsAppelMobileImpl;
		} else if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
			iDetailsAppelsDAO = detailsAppelInternetImpl;
		} else if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			iDetailsAppelsDAO = detailsAppelDataImpl;
		}
		if (null == iDetailsAppelsDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iDetailsAppelsDAO;
	}

	/**
	 * Gets the stats client dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the stats client dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IStatsClientDAO getStatsClientDAOForProduct(final int product)
			throws GrcWsException {
		IStatsClientDAO iStatsClientDAO = null;

		if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
			iStatsClientDAO = statsClientDAOFixeImpl;
		} else if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iStatsClientDAO = statsClientDAOMobileImpl;
		} else if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
			iStatsClientDAO = statsClientDAOInternetImpl;
		} else if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			iStatsClientDAO = statsClientDAODataImpl;
		}
		if (null == iStatsClientDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iStatsClientDAO;
	}

	/**
	 * Gets the fidelio dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the fidelio dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IFidelioDAO getFidelioDAOForProduct(final int product)
			throws GrcWsException {
		IFidelioDAO iFidelioDAO = null;

		if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
			iFidelioDAO = fidelioDAOFixeImpl;
		} else if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iFidelioDAO = fidelioDAOMobileImpl;
		} else if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
			iFidelioDAO = fidelioDAOInternetImpl;
		} else if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			iFidelioDAO = fidelioDAODataImpl;
		}
		if (null == iFidelioDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iFidelioDAO;
	}

	/**
	 * Gets the cust acct dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the cust acct dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public ICustAcctDAO getCustAcctDAOForProduct(final int product)
			throws GrcWsException {
		ICustAcctDAO iCustAcctDAO = null;

		if (product == ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
			iCustAcctDAO = custAcctDAOMobileImpl;
		} else if ((product == ProduitTypeEnum.INTERNET.getIdTypeProduit())
				|| (product == ProduitTypeEnum.FIXE.getIdTypeProduit())
				|| (product == ProduitTypeEnum.DATA.getIdTypeProduit())) {
			iCustAcctDAO = custAcctDAOInternetImpl;
		} /*
		 * else if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
		 * iCustAcctDAO = custAcctDAODataImpl; }
		 */
		if (null == iCustAcctDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iCustAcctDAO;
	}

	/**
	 * Gets the sms dao for product.
	 * 
	 * @param product
	 *            the product
	 * @return the sms dao for product
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	public ISmsDAO getSmsDAOForProduct(final int product) throws GrcWsException {
		// Quelque soit le produit, l'insertion des SMS est effectuée sur la
		// base BSCS Mobile
		// Table FP_SMS_INTERFACE
		return smsDAOMobileImpl;
	}

	/**
	 * Gets the invoice dao for product.
	 * 
	 * @param product
	 *            le produit
	 * @return the invoice dao for product
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	public IInvoiceDAO getInvoiceDAOForProduct(final int product)
			throws GrcWsException {

		IInvoiceDAO iInvoiceDAO = null;

		if (ProduitTypeEnum.MOBILE.getIdTypeProduit() == product) {
			iInvoiceDAO = invoiceDAOMobileImpl;
		} else if ((ProduitTypeEnum.FIXE.getIdTypeProduit() == product)
				|| (ProduitTypeEnum.INTERNET.getIdTypeProduit() == product)
				|| (ProduitTypeEnum.DATA.getIdTypeProduit() == product)) {
			iInvoiceDAO = invoiceDAOFixeImpl;
		}
		if (null == iInvoiceDAO) {
			throw new GrcWsException(ExceptionCodeTypeEnum.NO_DAO_FOR_PRODUCT,
					String.valueOf(product));
		}

		return iInvoiceDAO;
	}

	/**
	 * Calcul scoring client grand compte pas de retard.
	 * 
	 * @param ancienneteContrat
	 *            the anciennete contrat
	 * @param chiffreAffaireCourant
	 *            the chiffre affaire courant
	 * @return the int
	 */
	private int calculScoringClientGrandComptePasDeRetard(
			final double ancienneteContrat, final double chiffreAffaireCourant) {
		int result = -1;
		if (ancienneteContrat <= DEUX_ANS) {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_TROIS;
			} else if (chiffreAffaireCourant <= PALIER_QUATRE) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		} else if (ancienneteContrat <= CINQ_ANS) {
			if (chiffreAffaireCourant <= PALIER_TROIS) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		} else {
			result = SCORING_UN;
		}
		return result;
	}

	/**
	 * Calcul scoring client grand compte facture retard.
	 * 
	 * @param ancienneteContrat
	 *            the anciennete contrat
	 * @param chiffreAffaireCourant
	 *            the chiffre affaire courant
	 * @return the int
	 */
	private int calculScoringClientGrandCompteFactureRetard(
			final double ancienneteContrat, final double chiffreAffaireCourant) {
		int result = -1;

		if (ancienneteContrat <= DEUX_ANS) {
			if (chiffreAffaireCourant <= PALIER_TROIS) {
				result = SCORING_QUATRE;
			} else {
				result = SCORING_TROIS;
			}
		} else if (ancienneteContrat <= CINQ_ANS) {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_QUATRE;
			} else if (chiffreAffaireCourant <= PALIER_TROIS) {
				result = SCORING_TROIS;
			} else if (chiffreAffaireCourant <= PALIER_QUATRE) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		} else {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_TROIS;
			} else if (chiffreAffaireCourant <= PALIER_TROIS) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		}

		return result;
	}

	/**
	 * Calcul scoring client grand compte.
	 * 
	 * @param facturesRetard
	 *            les factures en retard
	 * @param ancienneteContrat
	 *            l'anciennete du contrat
	 * @param chiffreAffaireCourant
	 *            le chiffre d'affaire courant
	 * @return the int
	 */
	private int calculScoringClientGrandCompte(final double facturesRetard,
			final double ancienneteContrat, final double chiffreAffaireCourant) {
		int result = -1;

		// Grands comptes et entreprises
		if (facturesRetard < 2) {
			result = calculScoringClientGrandComptePasDeRetard(
					ancienneteContrat, chiffreAffaireCourant);
		} else {
			result = calculScoringClientGrandCompteFactureRetard(
					ancienneteContrat, chiffreAffaireCourant);
		}
		return result;
	}

	/**
	 * Calcul scoring client grand publique pas de retard.
	 * 
	 * @param ancienneteContrat
	 *            the anciennete contrat
	 * @param chiffreAffaireCourant
	 *            the chiffre affaire courant
	 * @return the int
	 */
	private int calculScoringClientGrandPubliquePasDeRetard(
			final double ancienneteContrat, final double chiffreAffaireCourant) {
		int result = -1;
		if (ancienneteContrat <= UN_AN) {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		} else if (ancienneteContrat <= CINQ_ANS) {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		} else {
			result = SCORING_UN;
		}

		return result;
	}

	/**
	 * Calcul scoring client grand publique retard.
	 * 
	 * @param ancienneteContrat
	 *            the anciennete contrat
	 * @param chiffreAffaireCourant
	 *            the chiffre affaire courant
	 * @return the int
	 */
	private int calculScoringClientGrandPubliqueRetard(
			final double ancienneteContrat, final double chiffreAffaireCourant) {
		int result = -1;

		if (ancienneteContrat <= UN_AN) {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_TROIS;
			} else {
				result = SCORING_DEUX;
			}
		} else if (ancienneteContrat <= CINQ_ANS) {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_TROIS;
			} else {
				result = SCORING_DEUX;
			}
		} else {
			if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_TROIS;
			} else if (chiffreAffaireCourant <= PALIER_DEUX) {
				result = SCORING_DEUX;
			} else {
				result = SCORING_UN;
			}
		}

		return result;
	}

	/**
	 * Calcul scoring client grand publique.
	 * 
	 * @param facturesRetard
	 *            les factures en retard
	 * @param ancienneteContrat
	 *            l'anciennete du contrat
	 * @param chiffreAffaireCourant
	 *            le chiffre d'affaire courant
	 * @return the int
	 */
	private int calculScoringClientGrandPublique(final double facturesRetard,
			final double ancienneteContrat, final double chiffreAffaireCourant) {
		int result = -1;
		// Hors Grands comptes et Hors entreprises
		if (facturesRetard < 2) {
			result = calculScoringClientGrandPubliquePasDeRetard(
					ancienneteContrat, chiffreAffaireCourant);
		} else {
			result = calculScoringClientGrandPubliqueRetard(ancienneteContrat,
					chiffreAffaireCourant);
		}

		return result;
	}

	/**
	 * Calcul du scoring du client selon la régle de calcul définie dans les
	 * spécs.
	 * 
	 * @param facturesRetard
	 *            le nombre de factures retard
	 * @param ancienneteContrat
	 *            l'ancienneté du contrat
	 * @param chiffreAffaireCourant
	 *            le chiffre d'affaire courant
	 * @param categoryCd
	 *            le code de catégorie du client
	 * @param product
	 *            le product
	 * @return the int
	 */
	protected int calculScoringClient(final double facturesRetard,
			final double ancienneteContrat, final double chiffreAffaireCourant,
			final String categoryCd, final int product) {
		int result = -1;
		if ((product == ProduitTypeEnum.DATA.getIdTypeProduit())
				|| CustAcctCategoryTypeEnum.ENTREPRISE.getCodeCategoryType()
						.toUpperCase().equals(categoryCd.toUpperCase())
				|| CustAcctCategoryTypeEnum.GRANDS_COMPTES
						.getCodeCategoryType().toUpperCase()
						.equals(categoryCd.toUpperCase())) {
			result = calculScoringClientGrandCompte(facturesRetard,
					ancienneteContrat, chiffreAffaireCourant);

		} else {
			result = calculScoringClientGrandPublique(facturesRetard,
					ancienneteContrat, chiffreAffaireCourant);
		}
		return result;
	}

	public static String stripNonValidXMLCharacters(final String in) {
		final StringBuffer out = new StringBuffer(); // Used to hold the output.
		char current; // Used to reference the current character.

		if ((in == null) || ("".equals(in))) {
			return ""; // vacancy test.
		}
		for (int i = 0; i < in.length(); i++) {
			current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught
			// here; it should not happen.
			if ((current == 0x9) || (current == 0xA) || (current == 0xD)
					|| ((current >= 0x20) && (current <= 0xD7FF))
					|| ((current >= 0xE000) && (current <= 0xFFFD))
					|| ((current >= 0x10000) && (current <= 0x10FFFF))) {
				out.append(current);
			}
		}
		return out.toString();
	}

}
