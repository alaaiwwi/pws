/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * La Classe SmsWS contient les informations é insérer dans la base de données
 * BSCS afin d'envoyer un SMS<br>
 * Les informations sont les suivantes: ND, NPCODE, NPSHDES, TEXT
 */
public class SmsWS extends GrcWS {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7118703446323025681L;

    /** Le ND destinataire du SMS é envoyer */
    private String nd;

    /** ND Code. */
 //   private String npCode;

    /** ND Desc. */
   // private String npShdes;

    /** Contenu du SMS. */
    private String smsText;
    
    /**  Application*/
    /*
     * f.tatbi FC 5864 ajout du champ application
     */
    private String application;

    /**
     * Gets the num nd.
     * 
     * @return the num nd
     */
 

    public String getNd() {
		return nd;
	}

	/**
     * Sets the num nd.
     * 
     * @param numNd
     *            the new num nd
     */
    public void setNd(String nd) {
		this.nd = nd;
	}

    /**
     * Gets the np code.
     * 
     * @return the np code
     */
//    public String getNpCode() {
//        return npCode;
//    }

    /**
     * Sets the np code.
     * 
     * @param npCode
     *            the new np code
     */
//    public void setNpCode(final String npCode) {
//        this.npCode = npCode;
//    }

    /**
     * Gets the np shde.
     * 
     * @return the np shde
     */
//    public String getNpShdes() {
//        return npShdes;
//    }

    /**
     * Sets the np shde.
     * 
     * @param npShdes
     *            the new np shdes
     */
//    public void setNpShdes(final String npShdes) {
//        this.npShdes = npShdes;
//    }

    /**
     * Gets the sms text.
     * 
     * @return the sms text
     */
    public String getSmsText() {
        return smsText;
    }

    /**
     * Sets the sms text.
     * 
     * @param smsText
     *            the new sms text
     */
    public void setSmsText(final String smsText) {
        this.smsText = smsText;
    }

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

}
