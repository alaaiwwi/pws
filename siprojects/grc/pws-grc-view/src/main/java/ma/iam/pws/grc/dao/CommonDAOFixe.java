/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.ContractWS;
import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.CustFindContractWS;
import ma.iam.pws.grc.bean.CustSearchWS;
import ma.iam.pws.grc.bean.EntityWS;
import ma.iam.pws.grc.bean.SearchCustomerWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import ma.iam.pws.grc.dao.custacct.CustAcctDAODataImpl;
import ma.iam.pws.grc.dao.custacct.CustAcctDAOFixeImpl;
import ma.iam.pws.grc.dao.custacct.CustAcctDAOInternetImpl;
import ma.iam.pws.grc.dao.service.CompAdress;
import ma.iam.pws.grc.dao.service.ParamAdress;
import ma.iam.pws.grc.dao.service.ServiceDAODataImpl;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import fr.capgemini.iam.grc.core.utils.Validator;

// TODO: Auto-generated Javadoc
/**
 * The Class CustAcctDAOMobileImpl.
 */
@Repository
public class CommonDAOFixe extends BaseDAOFixe {

	private static final String GET_CUSTOMER_BY_CUSTCODE = "select customer_id from customer_all where  ";
	private static final String GET_CUSTOMERS = CustAcctDAOFixeImpl.class
			.getName() + ".GET_CUSTOMERS";
	/* recupe sql de GET_INFO_CONTRACT */
	private static final String GET_INFO_CONTRACT_FIXE = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_CONTRACT_FIXE";
	private static final String GET_INFO_CONTRACT_INTERNET = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_CONTRACT_INTERNET";
	private static final String GET_INFO_CONTRACT_DATA = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_CONTRACT_DATA";
	/* recupe sql de GET_INFO_CUSTOMER */
	private static final String GET_INFO_CUSTOMER = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_CUSTOMER";

	/* recupe sql de GET_INFO_AGENCE_CUSTOMER */
	private static final String GET_INFO_AGENCE_CUSTOMER = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_AGENCE_CUSTOMER";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CommonDAOFixe.class);
	/** La constante GET_ADRESS. */
	private static final String GET_ADRESS = ServiceDAODataImpl.class.getName()
			+ ".GET_ADRESS";

	/** La constante GET_COMMUNE. */
	private static final String GET_COMMUNE = ServiceDAODataImpl.class
			.getName() + ".GET_COMMUNE";

	/** La constante GET_QUARTIER. */
	private static final String GET_QUARTIER = ServiceDAODataImpl.class
			.getName() + ".GET_QUARTIER";

	/** La constante GET_VOIE. */
	private static final String GET_VOIE = ServiceDAODataImpl.class.getName()
			+ ".GET_VOIE";

	/** La constante GET_SLA_CLASS. */
	public static final String GET_SLA_CLASS = ServiceDAODataImpl.class
			.getName() + ".GET_SLA_CLASS";

	public static final String GET_INFOS_INTERNET = CustAcctDAOInternetImpl.class
			.getName() + ".GET_INFOS_INTERNET";

	/** La constante GETLISTECONTRATS. */
	public static final String GETLISTECONTRATS_DATA = CustAcctDAODataImpl.class
			.getName() + ".GETLISTECONTRATS";

	public static final String GET_ND_SECOURS = CustAcctDAODataImpl.class
			.getName() + ".GET_ND_SECOURS";

	protected List<CustAcctWS> getListCustAcctWS(final String appendSql,
			final CustAcctWS searchCustAcctBean, final Boolean allowVIP,
			final int product) throws GrcWsException {
		String sql;
		final List<String> tags;

		if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			tags = getTagsFromSearchBean(searchCustAcctBean, allowVIP,
					DN_DN_NUM_DATA, CU_CUSTOMER_ID_DATA);
			sql = CustomSQLUtil.get(GETLISTECONTRATS_DATA);
		} else {
			tags = getTagsFromSearchBean(searchCustAcctBean, allowVIP,
					DN_DN_NUM, CU_CUSTOMER_ID);
			sql = CustomSQLUtil.get(GETLISTECONTRATS);
		}

		final SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
				searchCustAcctBean);

		final CustomBeanPropertyRowMapper<CustAcctWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);

		if (!GRCStringUtil.isNullOrEmpty(appendSql)) {
			LOG.debug("Rajout du complément SQL  : ", appendSql);
			tags.add("LISTCUSTOMER;" + appendSql);
		}
		if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
			tags.add("PRODUCT;||'_INTERNET'");
		}
		try {
			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		final List<CustAcctWS> listCustAcct;
		if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
			listCustAcct = super.getNamedParameterJdbcTemplateNeto().query(sql,
					namedParameters, custAcctRowMapper);
		} else {
			listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
					namedParameters, custAcctRowMapper);
		}
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		return listCustAcct;
	}

	protected void addCustAcctByCommande(final List<CustAcctWS> listCustAcct,
			final CustAcctWS searchCustAcctBean, final int product)
			throws GrcWsException {
		// Cas particulier: La commande vient d'étre créée sur Netonomy.
		// Le numéro de commande remonte un customer_id mais pas de contrat
		// associé.
		// On doit refaire la recherche pour remonter les clients qui n'ont pas
		// de contrat
		if (listCustAcct.isEmpty()) {
			if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCommande())) {
				final List<CustAcctWS> listCustomerWs = findCustomerByCommande(searchCustAcctBean
						.getCommande());
				if ((listCustomerWs != null) && !listCustomerWs.isEmpty()) {
					for (final CustAcctWS customerWs : listCustomerWs) {
						listCustAcct.addAll(findCustomerByCustcode(
								customerWs.getCustAcctCode(), product));
					}
				}
			} else if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean
					.getCustAcctCode())) {
				listCustAcct.addAll(findCustomerByCustcode(
						searchCustAcctBean.getCustAcctCode(), product));
			}
		}
	}

	/**
	 * Gets the appended sql.
	 * 
	 * @param ccu
	 *            the ccu
	 * @param commande
	 *            the commande
	 * @return the appended sql
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	protected String getAppendedSQLByCCU(final String ccu, final int product)
			throws GrcWsException {
		String appendSql = "";
		// Si le champ CCU est renseigné, on cherche é partir de la base
		// netonomy
		if (!GRCStringUtil.isNullOrEmpty(ccu)) {
			final List<CustAcctWS> listCustomerWs = findCustomerByCcu(ccu);
			appendSql = appendSql.concat(" AND ( CA.CUSTOMER_ID in (");
			if ((listCustomerWs != null) && !listCustomerWs.isEmpty()) {
				for (final CustAcctWS customerWs : listCustomerWs) {
					appendSql = appendSql.concat(customerWs.getCustomerId())
							.concat(",");
				}
			}
			appendSql = appendSql
					.concat("  0 ) or  cu.customer_id_high in  ( ");
			if ((listCustomerWs != null) && !listCustomerWs.isEmpty()) {
				for (final CustAcctWS customerWs : listCustomerWs) {
					appendSql = appendSql.concat(
							String.valueOf(customerWs.getCustomerId())).concat(
							",");
				}
			}
			appendSql = appendSql.concat(" 0  ) )");
		}
		return appendSql;
	}

	protected String getAppendedSQLByCommande(final String commande,
			final int product) throws GrcWsException {
		String appendSql = "";

		if (!GRCStringUtil.isNullOrEmpty(commande)) {
			final List<CustAcctWS> listCustomerWs = findCustomerByCommande(commande);
			if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
				appendSql = appendSql.concat(" AND " + CU_CUSTCODE_DATA
						+ " in (");
			} else {
				appendSql = appendSql.concat(" AND " + CU_CUSTCODE + " in (");
			}

			if ((listCustomerWs != null) && !listCustomerWs.isEmpty()) {
				for (final CustAcctWS customerWs : listCustomerWs) {
					appendSql = appendSql.concat("'")
							.concat(customerWs.getCustAcctCode()).concat("',");
				}
			}
			appendSql = appendSql.concat(" '0'  ) ");
		}

		return appendSql;
	}

	protected void normaliserND(final CustAcctWS searchCustAcctBean) {
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract()
				.getNd())) {
			String nd = searchCustAcctBean.getContract().getNd();
			if ((nd.length() == GRCWSConstantes.PHONE_NUMBER_DIGITS)
					&& nd.startsWith("05")) {
				nd = GRCWSConstantes.PHONE_NUMBER_PREFIX + nd.substring(1);
				searchCustAcctBean.getContract().setNd(nd);
			}
		}
	}

	/**
	 * Gets the sla class.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @return the sla class
	 */
	protected String getSlaClass(final String contractIdBscs) {
		String slaClass = null;
		try {
			if (Validator.isDigit(contractIdBscs)) {// cas particulier data
				// (CUG.xxx)
				final String sql = CustomSQLUtil.get(GET_SLA_CLASS);
				final Map<String, String> namedParameters = new HashMap<String, String>();
				namedParameters.put("contractIdBscs", contractIdBscs);
				LOG.debug("contractIdBscs {}", contractIdBscs);
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
				slaClass = super.getNamedParameterJdbcTemplate()
						.queryForObject(sql, namedParameters, String.class);
				LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, slaClass);
			}
		} catch (final EmptyResultDataAccessException e) {
			slaClass = null;
		}

		if (!GRCWSConstantes.C1_CLASS.equals(slaClass)
				&& !GRCWSConstantes.C2_CLASS.equals(slaClass)
				&& !GRCWSConstantes.C3_CLASS.equals(slaClass)) {
			slaClass = GRCWSConstantes.C0_CLASS;
		}
		return slaClass;
	}

	/**
	 * Gets the infos internet by login.
	 * 
	 * @param loginInternet
	 *            the login internet
	 * @return the infos internet by login
	 */
	private ContractWS getInfosInternetByLogin(final String loginInternet) {
		ContractWS contract = null;
		List<ContractWS> listContracts = null;

		final String sql = CustomSQLUtil.get(GET_INFOS_INTERNET);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("loginInternet", loginInternet);

		final CustomBeanPropertyRowMapper<ContractWS> contractWSRowMapper = new CustomBeanPropertyRowMapper<ContractWS>(
				ContractWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContracts = super.getNamedParameterJdbcTemplateNeto().query(sql,
				namedParameters, contractWSRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, contract);

		if ((listContracts != null) && !listContracts.isEmpty()) {
			contract = listContracts.get(0);
		}

		return contract;
	}

	/**
	 * Gets the adress.
	 * 
	 * @param paramType
	 *            the param type
	 * @param contractIdNeto
	 *            the contract id neto
	 * @return the adress
	 */
	protected String getAdress(final String paramType,
			final String contractIdNeto) {
		LOG.debug("--> getAdress ");
		final String sql = CustomSQLUtil.get(GET_ADRESS);

		// Initialisation des variables
		String adress = "";

		String communeVal = "";
		String quartierVal = "";
		String voieVal = "";

		final Map<String, String> namedParameters = new HashMap<String, String>();

		namedParameters.put("contractIdNeto", contractIdNeto);
		namedParameters.put("paramType", paramType);

		final CustomBeanPropertyRowMapper<ParamAdress> custAcctRowMapper = new CustomBeanPropertyRowMapper<ParamAdress>(
				ParamAdress.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		final List<ParamAdress> paramAdressList = super
				.getNamedParameterJdbcTemplateNeto().query(sql,
						namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		// Pour chaque paramétre récupéré on renseigne les champs correspondants
		final CompAdress compAdress = getInfoFromParamAdressList(paramAdressList);

		// Récupération de la valeur de la commune é partir de l'identifiant
		communeVal = searchCommune(compAdress.getCommuneId());
		// Récupération de la valeur du nom de quartier é partir de
		// l'identifiant
		quartierVal = searchQuartier(compAdress.getCommuneId(),
				compAdress.getQuartierId());

		// Si le quartier est vide, alors on remplace par le complément
		if (!GRCStringUtil.isNullOrEmpty(compAdress.getReliqQuartier())
				&& (GRCStringUtil.isNullOrEmpty(quartierVal)
						|| GRCWSConstantes.VIDE_PARAM.equals(quartierVal) || GRCWSConstantes.VIDE_2_PARAM
							.equals(quartierVal))) {
			quartierVal = compAdress.getReliqQuartier();
		}

		// Si la voie est vide, alors on remplace par le complément
		voieVal = searchVoie(compAdress.getCommuneId(),
				compAdress.getQuartierId(), compAdress.getVoieId());
		if (!GRCStringUtil.isNullOrEmpty(compAdress.getReliqVoie())
				&& (GRCStringUtil.isNullOrEmpty(voieVal)
						|| GRCWSConstantes.VIDE_PARAM.equals(voieVal) || GRCWSConstantes.VIDE_2_PARAM
							.equals(voieVal))) {
			voieVal = compAdress.getReliqVoie();
		}

		if (!GRCStringUtil.isNullOrEmpty(compAdress.getnVoie())) {
			adress = adress + " Né " + compAdress.getnVoie();
		}
		if (!GRCStringUtil.isNullOrEmpty(compAdress.getnEtage())) {
			adress = adress + " ETG " + compAdress.getnEtage();
		}
		if (!GRCStringUtil.isNullOrEmpty(voieVal)) {
			adress = adress + ", " + voieVal;
		}
		if (!GRCStringUtil.isNullOrEmpty(quartierVal)) {
			adress = adress + ", " + quartierVal;
		}

		if (!GRCStringUtil.isNullOrEmpty(communeVal)) {
			adress = adress + ", " + communeVal;
		}

		if (!GRCStringUtil.isNullOrEmpty(compAdress.getCompAddress())) {
			adress = adress + ", (" + compAdress.getCompAddress() + ")";
		}
		LOG.debug("<-- getAdress [{}]", adress);
		return adress;
	}

	/**
	 * Gets the info from param adress list.
	 * 
	 * @param paramAdressList
	 *            the param adress list
	 * @return the info from param adress list
	 */
	private CompAdress getInfoFromParamAdressList(
			final List<ParamAdress> paramAdressList) {
		final CompAdress compAdress = new CompAdress();
		for (final ParamAdress paramAdress : paramAdressList) {
			if (GRCWSConstantes.COMMUNE_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération de l'identifiant de la commune
				compAdress.setCommuneId(paramAdress.getValue());
			} else if (GRCWSConstantes.QUARTIER_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération de l'identifiant du quartier
				compAdress.setQuartierId(paramAdress.getValue());
			} else if (GRCWSConstantes.VOIE_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération de l'identifiant de la voie
				compAdress.setVoieId(paramAdress.getValue());
			} else if (GRCWSConstantes.COMPLT_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération du complément d'adress
				compAdress.setCompAddress(paramAdress.getValue());
			} else if (GRCWSConstantes.QUARTIER_2_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération du complément du quartier
				compAdress.setReliqQuartier(paramAdress.getValue());
			} else if (GRCWSConstantes.VOIE_2_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération du complément voie
				compAdress.setReliqVoie(paramAdress.getValue());
			} else if (GRCWSConstantes.N_VOIE_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération du numéro de la voie
				compAdress.setnVoie(paramAdress.getValue());
			} else if (GRCWSConstantes.N_ETAGE_PARAM_ID.equals(paramAdress
					.getParamId())) {
				// Récupération du numéro d'étage
				compAdress.setnEtage(paramAdress.getValue());
			}
		}
		return compAdress;
	}

	/**
	 * Search commune.
	 * 
	 * @param commune
	 *            the commune
	 * @return the string
	 */
	private String searchCommune(final String commune) {

		String result = "";
		if (!GRCStringUtil.isNullOrEmpty(commune)) {
			final String sql = CustomSQLUtil.get(GET_COMMUNE);
			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put(GRCWSConstantes.COMMUNE, commune);
			try {
				LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + " commune {}", sql,
						commune);
				result = super.getNamedParameterJdbcTemplateNeto()
						.queryForObject(sql, namedParameters, String.class);
				LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);
			} catch (final EmptyResultDataAccessException e) {
				result = null;
			}
		}
		return result;
	}

	/**
	 * Search quartier.
	 * 
	 * @param commune
	 *            the commune
	 * @param quartier
	 *            the quartier
	 * @return the string
	 */
	private String searchQuartier(final String commune, final String quartier) {

		String result = "";
		if (!GRCStringUtil.isNullOrEmpty(commune)
				&& !GRCStringUtil.isNullOrEmpty(quartier)) {
			final String sql = CustomSQLUtil.get(GET_QUARTIER);
			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put(GRCWSConstantes.COMMUNE, commune);
			namedParameters.put(GRCWSConstantes.QUARTIER, quartier);
			try {
				LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + " commune "
						+ commune + " quartier " + quartier, sql);
				result = super.getNamedParameterJdbcTemplateNeto()
						.queryForObject(sql, namedParameters, String.class);
				LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);
			} catch (final EmptyResultDataAccessException e) {
				result = null;
			}
		}
		return result;
	}

	/**
	 * Search voie.
	 * 
	 * @param commune
	 *            the commune
	 * @param quartier
	 *            the quartier
	 * @param voie
	 *            the voie
	 * @return the string
	 */
	private String searchVoie(final String commune, final String quartier,
			final String voie) {

		String result = "";
		if (!GRCStringUtil.isNullOrEmpty(commune)
				&& !GRCStringUtil.isNullOrEmpty(voie)) {
			final String sql = CustomSQLUtil.get(GET_VOIE);

			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put(GRCWSConstantes.COMMUNE, commune);
			namedParameters.put(GRCWSConstantes.QUARTIER, quartier);
			namedParameters.put(GRCWSConstantes.VOIE, voie);
			try {
				LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + "commune" + commune
						+ " quartier " + quartier + " voie " + voie, sql);
				result = super.getNamedParameterJdbcTemplateNeto()
						.queryForObject(sql, namedParameters, String.class);
				LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);
			} catch (final EmptyResultDataAccessException e) {
				result = null;
			}
		}
		return result;
	}

	/** The Constant GET_INFO_DC. */
	protected static final String GET_INFO_DC = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_DC";

	/**
	 * Sets the contract dc.
	 * 
	 * @param customerWs
	 *            the new contract dc
	 */
	protected void setContractDC(final CustAcctWS customerWs) {
		final String sqlDc = CustomSQLUtil.get(GET_INFO_DC);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", customerWs.getContract()
				.getContractIdBscs());
		try {
			LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sqlDc);
			final CustomBeanPropertyRowMapper<EntityWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<EntityWS>(
					EntityWS.class);
			final List<EntityWS> entity = super
					.getNamedParameterJdbcTemplateNeto().query(sqlDc,
							namedParameters, custAcctRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, entity);
			if (!entity.isEmpty()) {
				customerWs.getContract().setDc(entity.get(0));
			}
		} catch (final EmptyResultDataAccessException e) {
			// Aucune ligne trouvée, on passe au client suivant
		}
	}

	/**
	 * Sets the adress.
	 * 
	 * @param customerWs
	 *            the new adress
	 */
	private void setAdress(final CustAcctWS customerWs) {
		LOG.debug("--> setAdress");
		final String adresse = getAdress(GRCWSConstantes.TYPE_L1_ADRESS,
				customerWs.getContract().getContractIdNeto());
		customerWs.getContract().setAdressInstallation(adresse);
		LOG.debug("<-- setAdress");
	}

	/**
	 * Gets the info supplementaires.
	 * 
	 * @param listCustAcctWS
	 *            the list cust acct ws
	 * @return the info supplementaires
	 */
	public void getInfoSupplementaires(final List<CustAcctWS> listCustAcctWS,
			final int product) {
		LOG.debug("--> getInfoSupplementaires");

		for (final CustAcctWS customerWs : listCustAcctWS) {
			if ((customerWs != null) && (customerWs.getContract() != null)
					&& (customerWs.getContract().getContractIdBscs() != null)) {
				setContractIdNeto(customerWs);
				setContractDC(customerWs);
				setAdress(customerWs);
				if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
					setSLA(customerWs.getContract()); // spécifique DATA
					setListND(customerWs);
				}
				// Spécifique Interent : CodeConfidentiel / Débit / Né VOIP
				if (product == ProduitTypeEnum.INTERNET.getIdTypeProduit()) {
					setInfosInternet(customerWs.getContract());
				}

				if (GRCStringUtil.isNullOrEmpty(customerWs.getContract()
						.getNd())) {
					customerWs.getContract().setNd(
							findLineByContract(customerWs.getContract()
									.getContractIdBscs()));
				}
			}

			if ((customerWs != null) && (customerWs.getChildren() != null)
					&& !customerWs.getChildren().isEmpty()) {
				getInfoSupplementaires(customerWs.getChildren(), product);
			}
		}
		LOG.debug("<-- getInfoSupplementaires");
	}

	private void setListND(final CustAcctWS customerWs) {
		final String sql = CustomSQLUtil.get(GET_ND_SECOURS);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractId", customerWs.getContract()
				.getContractIdBscs());
		try {
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			final List<String> listNd = super
					.getNamedParameterJdbcTemplateNeto().queryForList(sql,
							namedParameters, String.class);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, listNd);

			if (listNd != null) {
				customerWs.getContract().setListNd(listNd);
			}
		} catch (final EmptyResultDataAccessException e) {
			// Aucune ligne trouvée, on passe au client suivant
		}
	}

	protected void setSLA(final ContractWS contract) {
		contract.setClassSla(getSlaClass(contract.getContractIdBscs()));
	}

	/**
	 * Sets the infos internet.
	 * 
	 * @param contract
	 *            the new infos internet
	 */
	private void setInfosInternet(final ContractWS contract) {
		if (!GRCStringUtil.isNullOrEmpty(contract.getLoginInternet())) {
			final ContractWS contractReturned = getInfosInternetByLogin(contract
					.getLoginInternet());
			if (null != contractReturned) {
				contract.setCodeConfidentiel(contractReturned
						.getCodeConfidentiel());
				contract.setDebit(contractReturned.getDebit());
				contract.setNumeroVoIp(contractReturned.getNumeroVoIp());
			}
		}

	}

	/** The Constant GET_INFO_NETO. */
	private static final String GET_INFO_NETO = CustAcctDAOFixeImpl.class
			.getName() + ".GET_INFO_NETO";

	/**
	 * Sets the contract id neto.
	 * 
	 * @param customerWs
	 *            the new contract id neto
	 */
	protected void setContractIdNeto(final CustAcctWS customerWs) {
		final String sql = CustomSQLUtil.get(GET_INFO_NETO);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", customerWs.getContract()
				.getContractIdBscs());
		try {
			LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
			final String contractIdNeto = super
					.getNamedParameterJdbcTemplateNeto().queryForObject(sql,
							namedParameters, String.class);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, contractIdNeto);

			if (contractIdNeto != null) {
				customerWs.getContract().setContractIdNeto(contractIdNeto);
			}
		} catch (final EmptyResultDataAccessException e) {
			// Aucune ligne trouvée, on passe au client suivant
		}
	}

	/** The Constant GET_LINE_BY_COID. */
	public static final String GET_LINE_BY_COID = CustAcctDAOInternetImpl.class
			.getName() + ".GET_LINE_BY_COID";
	public static final String GET_COID_FIXE_FROM_INTERNET = CustAcctDAOInternetImpl.class
			.getName() + ".GET_COID_FIXE_FROM_INTERNET";
	public static final String GET_COID_FIXE_FROM_ND = CustAcctDAOInternetImpl.class
			.getName() + ".GET_COID_FIXE_FROM_ND";

	/**
	 * Find line by contract.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @return the string
	 */
	protected String findLineByContract(final String contractIdBscs) {
		String value = null;
		try {
			final String sql = CustomSQLUtil.get(GET_LINE_BY_COID);
			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put("contractIdBscs", contractIdBscs);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + " contractIdBscs {}",
					sql, contractIdBscs);
			value = super.getNamedParameterJdbcTemplateNeto().queryForObject(
					sql, namedParameters, String.class);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);

			if ((value != null)
					&& (value.length() == GRCWSConstantes.PHONE_NUMBER_DIGITS)
					&& value.startsWith("05")) {
				value = GRCWSConstantes.PHONE_NUMBER_PREFIX.concat(value
						.substring(1));
			}

		} catch (final EmptyResultDataAccessException e) {
			value = null;
		}

		return value;
	}

	protected String findContractFixeFromInternet(
			final String contractIdBscsInternet) {
		String value = null;
		try {
			final String sql = CustomSQLUtil.get(GET_COID_FIXE_FROM_INTERNET);
			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put("contractIdBscsInternet",
					contractIdBscsInternet);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO
					+ " contractIdBscsInternet {}", sql, contractIdBscsInternet);
			value = super.getNamedParameterJdbcTemplateNeto().queryForObject(
					sql, namedParameters, String.class);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);
		} catch (final EmptyResultDataAccessException e) {
			value = null;
		}

		return value;
	}

	protected List<String> findContractFixeFromNd(final String ndEntree) {
		final List<String> value = new ArrayList<String>();

		try {

			String nd = ndEntree;
			if ((nd.length() == GRCWSConstantes.PHONE_NUMBER_WITH_PREFIX_DIGITS)
					&& nd.startsWith("212")) {
				nd = "0" + ndEntree.substring(3);
			}

			final String sql = CustomSQLUtil.get(GET_COID_FIXE_FROM_ND);
			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put("nd", nd);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + " nd {}", sql, nd);
			value.addAll(super.getNamedParameterJdbcTemplateNeto()
					.queryForList(sql, namedParameters, String.class));
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);
		} catch (final EmptyResultDataAccessException e) {
		}

		return value;
	}

	/** The Constant DN_DN_NUM. */
	public static final String DN_DN_NUM = "DN.DN_NUM";

	/** The Constant DN_DN_NUM_DATA. */
	public static final String DN_DN_NUM_DATA = "V.LINE_NUMBER";

	/** The Constant CU_CUSTOMER_ID. */
	public static final String CU_CUSTOMER_ID = "DN.DN_NUM";

	/** The Constant CU_CUSTOMER_ID_DATA. */
	public static final String CU_CUSTOMER_ID_DATA = "A.ORGANIZATION_LEGACY_ID";

	public static final String CU_CUSTCODE = "CU.CUSTCODE";

	public static final String CU_CUSTCODE_DATA = "A.ORGANIZATION_REFERENCE_ID";

	/**
	 * Get tags from search bean.
	 * 
	 * @param searchCustAcctBean
	 *            the search cust acct bean
	 * @param allowVIP
	 *            the allow vip
	 * @param ndCol
	 *            the nd col
	 * @param customerIdCol
	 *            the customer id col
	 * @return the tags from search bean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	protected List<String> getTagsFromSearchBean(
			final CustAcctWS searchCustAcctBean, final Boolean allowVIP,
			final String ndCol, final String customerIdCol)
			throws GrcWsException {

		// ndCol pour le fixe, internet, et mobile : DN.DN_NUM
		// ndCol pour la data : V.LINE_NUMBER

		// customerIdCol pour le fixe, internet et mobile : OR CU.CUSTOMER_ID
		// customerIdCol pour la data : A.ORGANIZATION_LEGACY_ID

		final List<String> tags = new ArrayList<String>(0);

		// Cas particulier de la recherche par contact, listNd contient le ND du
		// contact puis la liste des customerId
		// Cette liste sera transformée en :
		// AND (DN.DN_NUM = '212661357891' or CU.CUSTOMER_ID = 123456 or
		// CU.CUSTOMER_ID = 123578 )
		if ((searchCustAcctBean.getContract().getListNd() != null)
				&& (searchCustAcctBean.getContract().getListNd().size() > 0)) {
			final List<String> listValues = searchCustAcctBean.getContract()
					.getListNd();
			String tagValue = "AND (" + ndCol + " = '" + listValues.get(0)
					+ "' ";
			for (int i = 1; i < listValues.size(); i++) {
				tagValue = tagValue.concat(" OR " + customerIdCol + " = "
						+ listValues.get(i));
			}
			tagValue = tagValue.concat(" ) ");
			tags.add("RECHERCHE_CONTACT;" + tagValue);
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract()
				.getNd())) {
			tags.add("ND");
		}
		if (!GRCStringUtil
				.isNullOrEmpty(searchCustAcctBean.getIdentityNumber())) {
			tags.add("PI");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCustAcctCode())) {
			tags.add("CODE");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getLastName())) {
			tags.add("NOM");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getFirstName())) {
			tags.add("PRENOM");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCompanyName())) {
			tags.add("RAISON");
		}
		// Il s'agit du Ncli
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCustomerId())) {
			if (!WSValidator.validateNumber(searchCustAcctBean.getCustomerId())) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
						"Customer_id incorrect :  "
								+ searchCustAcctBean.getCustomerId());
			}
			tags.add("CUSTACCTID");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract()
				.getContractIdBscs())) {
			/*
			 * if (!WSValidator.validateNumber(searchCustAcctBean.getContract().
			 * getContractIdBscs())) { throw new
			 * GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
			 * "ContractIdBscs incorrect :  " +
			 * searchCustAcctBean.getContract().getContractIdBscs()); }
			 */
			if (!searchCustAcctBean.getContract().getContractIdBscs()
					.contains(",0")) {
				tags.add("COID");
			} else {
				/** @see #CustAcctDAOInternetImpl.setContractIdByDns() **/
				tags.add("LISTCOID;AND CA.CO_ID in ("
						+ searchCustAcctBean.getContract().getContractIdBscs()
						+ ")");
				searchCustAcctBean.getContract().setContractIdBscs(null);
			}
		}
		if ((searchCustAcctBean.getCustAcctCategory() != null)
				&& (searchCustAcctBean.getCustAcctCategory().getCategoryCd() != null)) {
			tags.add("CATEGORY");
		}
		if ((searchCustAcctBean.getContract() != null)
				&& (searchCustAcctBean.getContract().getStatus() != null)) {
			tags.add("CONTRACT_STATUS");
		}
		if (searchCustAcctBean.getStatus() != null) {
			tags.add("STATUS");
		}
		if ((searchCustAcctBean.getCcu() != null)
				&& (searchCustAcctBean.getCcu().length() > 0)) {
			tags.add("CCU");
		}
		if (!allowVIP) {
			tags.add("NOVIP");
		}
		// Commande
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCommande())) {
			if (!WSValidator.validateNumber(searchCustAcctBean.getCommande())) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
						"Commande incorrecte :  "
								+ searchCustAcctBean.getCommande());
			}
			tags.add("COMMANDE");
		}
		tags.add("HINT2;/*+ first_rows*/");
		return tags;
	}

	/** La constante GET_CUSTOMER_ID_BY_CCU. */
	public static final String GET_CUSTOMER_ID_BY_CCU = CustAcctDAOFixeImpl.class
			.getName() + ".GET_CUSTOMER_ID_BY_CCU"; // requete

	// du
	// fixe

	/**
	 * Cherche les customer by ccu.
	 * 
	 * @param ccu
	 *            le code client unique (CCU)
	 * @return la liste des résultats
	 */
	protected List<CustAcctWS> findCustomerByCcu(final String ccu) {
		LOG.debug("--> findCustomerByCcu ccu {}", ccu);
		final String sql = CustomSQLUtil.get(GET_CUSTOMER_ID_BY_CCU);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("ccu", ccu);
		final CustomBeanPropertyRowMapper<CustAcctWS> contractRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		final List<CustAcctWS> listContractWS = super
				.getNamedParameterJdbcTemplateNeto().query(sql,
						namedParameters, contractRowMapper);
		LOG.debug("<-- findCustomerByCcu ");
		return listContractWS;
	}

	/**
	 * La constante GET_CUSTOMER_ID_BY_COMMANDE .
	 */
	public static final String GET_CUSTOMER_CODE_BY_COMMANDE = CustAcctDAOFixeImpl.class
			.getName() + ".GET_CUSTOMER_CODE_BY_COMMANDE"; // requete

	// du
	// fixe

	/**
	 * Retourne la liste des CUSTCODE é partir d'un numéro de commande.
	 * 
	 * @param commande
	 *            the commande
	 * @return the list< cust acct w s>
	 */
	protected List<CustAcctWS> findCustomerByCommande(final String commande) {
		LOG.debug("--> findCustomerByCommande commande {}", commande);
		final String sql = CustomSQLUtil.get(GET_CUSTOMER_CODE_BY_COMMANDE);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("commande", commande);
		final CustomBeanPropertyRowMapper<CustAcctWS> contractRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		final List<CustAcctWS> listCustAcctWS = super
				.getNamedParameterJdbcTemplateNeto().query(sql,
						namedParameters, contractRowMapper);
		LOG.debug("<-- findCustomerByCommande ");
		return listCustAcctWS;
	}

	/** The Constant GETCUSTOMER. */
	// DAO DU FIXE
	public static final String GETCUSTOMER = CustAcctDAOFixeImpl.class
			.getName() + ".GETCUSTOMER";

	/** The Constant GETCLIENT. */
	// DAO DU FIXE
	public static final String GETCLIENT = CustAcctDAOFixeImpl.class.getName()
			+ ".GETCLIENT";

	/**
	 * Find customer by customer id.
	 * 
	 * @param customerId
	 *            the customer id
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	public List<CustAcctWS> findCustomerByCustomerId(final String customerId,
			final int product) throws GrcWsException {
		LOG.debug("--> findCustomerByCustomerId customerId {}", customerId);
		List<CustAcctWS> listCustAcct;
		String sql = CustomSQLUtil.get(GETCUSTOMER);
		final List<String> tags = new ArrayList<String>();
		try {
			tags.add("CUSTOMER_ID");
			if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
				tags.add("CUSTCAT;'CUSTCATD'");
			} else {
				tags.add("CUSTCAT;'CUSTCATF'");
			}

			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				GRCWSConstantes.CUSTOMER_ID, customerId);

		final CustomBeanPropertyRowMapper<CustAcctWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listCustAcct == null) || listCustAcct.isEmpty()) {
			sql = CustomSQLUtil.get(GETCLIENT);
			try {
				sql = GRCStringUtil.formatQuery(sql, tags);
			} catch (final Exception e) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(),
						e);
			}
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
					namedParameters, custAcctRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}
		LOG.debug("<-- findCustomerByCustomerId");
		return listCustAcct;
	}

	/** La constante GETLISTECONTRATS. */
	public static final String GETLISTECONTRATS = CustAcctDAOFixeImpl.class
			.getName() + ".GETLISTECONTRATS";

	/**
	 * Find customer by custcode.
	 * 
	 * @param custocode
	 *            the custocode
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	public List<CustAcctWS> findCustomerByCustcode(final String custocode,
			final int product) throws GrcWsException {
		LOG.debug("--> findCustomerByCustcode custocode {}", custocode);
		List<CustAcctWS> listCustAcct;
		String sql = CustomSQLUtil.get(GETCUSTOMER);
		final List<String> tags = new ArrayList<String>();
		try {
			tags.add("CUSTCODE");
			if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
				tags.add("CUSTCAT;'CUSTCATD'");
			} else {
				tags.add("CUSTCAT;'CUSTCATF'");
			}
			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"custcode", custocode);

		final CustomBeanPropertyRowMapper<CustAcctWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listCustAcct == null) || listCustAcct.isEmpty()) {
			sql = CustomSQLUtil.get(GETCLIENT);
			try {
				sql = GRCStringUtil.formatQuery(sql, tags);
			} catch (final Exception e) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(),
						e);
			}
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
					namedParameters, custAcctRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}
		LOG.debug("<-- findCustomerByCustcode");
		return listCustAcct;
	}

	public List<CustSearchWS> searchCustomers(SearchCustomerWS searchCustomerWS) {
		List<CustSearchWS> listCustomers = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> searchCustomers: " + searchCustomerWS.toString());
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CUSTOMER));

		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCo_id())) {
			sqlBuffer.append(" and ca.CO_ID = " + searchCustomerWS.getCo_id());
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNd())) {
			sqlBuffer.append(" and dn.DN_NUM= '" + searchCustomerWS.getNd()
					+ "'");
		}

		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNom())) {
			sqlBuffer.append(" and cca.CCLNAME='" + searchCustomerWS.getNom()
					+ "'");
		}

		if (!GRCStringUtil
				.isNullOrEmpty(searchCustomerWS.getNumPieceIdentite())) {
			sqlBuffer.append(" and cca.PASSPORTNO= '"
					+ searchCustomerWS.getNumPieceIdentite() + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getPrenom())) {
			sqlBuffer.append(" and cca.CCFNAME='"
					+ searchCustomerWS.getPrenom() + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getVille())) {
			sqlBuffer.append(" and cca.CCCITY='" + searchCustomerWS.getVille()
					+ "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCodeCategorie())) {
			sqlBuffer.append(" and pga.PRGCODE= "
					+ searchCustomerWS.getCodeCategorie() );
		}

		sqlBuffer.append(" and rownum <= 200");
		final CustomBeanPropertyRowMapper<CustSearchWS> rowMapper = new CustomBeanPropertyRowMapper<CustSearchWS>(
				CustSearchWS.class);
		listCustomers = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- searchCustomers: " + listCustomers);

		return listCustomers;
	}

	public List<CustSearchWS> searchAgenceCustomers(List<Long> customerIds) {
		List<CustSearchWS> listAgenceCustomers = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> searchAgenceCustomers: " + customerIds);
		}

		final StringBuffer sqlBuffer = new StringBuffer(
				CustomSQLUtil.get(GET_INFO_AGENCE_CUSTOMER));

		if (customerIds != null && !customerIds.isEmpty()) {
			sqlBuffer.append(" and info.CUSTOMER_ID in (");
			for (int i = 0; i < customerIds.size(); i++) {
				sqlBuffer.append(customerIds.get(i));
				if (i != customerIds.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
		} else {
			sqlBuffer.append(" and info.CUSTOMER_ID in (0)");
		}

		final CustomBeanPropertyRowMapper<CustSearchWS> rowMapper = new CustomBeanPropertyRowMapper<CustSearchWS>(
				CustSearchWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer);
		listAgenceCustomers = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- searchAgenceCustomers");
		return listAgenceCustomers;

	}

	public List<CustFindContractWS> findContract(String custcode,
			String customerId, String nd, String coId) {
		List<CustFindContractWS> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CONTRACT_FIXE));

		if (!GRCStringUtil.isNullOrEmpty(custcode)) {
			sqlBuffer.append(" and CU.CUSTCODE = '" + custcode + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(coId)) {
			sqlBuffer.append(" and ca.CO_ID= " + coId);
		}

		if (!GRCStringUtil.isNullOrEmpty(customerId)) {
			sqlBuffer.append(" and cu.CUSTOMER_ID=" + customerId);
		}

		if (!GRCStringUtil.isNullOrEmpty(nd)) {
			sqlBuffer.append(" and dn.DN_NUM= '" + nd + "'");
		}

		sqlBuffer.append(" UNION ALL  "
				+ CustomSQLUtil.get(GET_INFO_CONTRACT_DATA));

		if (!GRCStringUtil.isNullOrEmpty(custcode)) {
			sqlBuffer.append(" and CU.CUSTCODE = '" + custcode + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(coId)) {
			sqlBuffer.append(" and ca.CO_ID= " + coId);
		}

		if (!GRCStringUtil.isNullOrEmpty(customerId)) {
			sqlBuffer.append(" and cu.CUSTOMER_ID=" + customerId);
		}

		if (!GRCStringUtil.isNullOrEmpty(nd)) {
			sqlBuffer.append(" and p.PORT_NUM= '" + nd + "'");
		}

		sqlBuffer.append(" UNION ALL  "
				+ CustomSQLUtil.get(GET_INFO_CONTRACT_INTERNET));

		if (!GRCStringUtil.isNullOrEmpty(custcode)) {
			sqlBuffer.append(" and CU.CUSTCODE = '" + custcode + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(coId)) {
			sqlBuffer.append(" and ca.CO_ID= " + coId);
		}

		if (!GRCStringUtil.isNullOrEmpty(customerId)) {
			sqlBuffer.append(" and cu.CUSTOMER_ID=" + customerId);
		}

		if (!GRCStringUtil.isNullOrEmpty(nd)) {
			sqlBuffer.append(" and p.PORT_NUM= '" + nd + "'");
		}

		final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
				CustFindContractWS.class);
		listContracts = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public List<CustFindContractWS> findChildContract(
			List<CustFindContractWS> list) {
		List<CustFindContractWS> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CONTRACT_FIXE));

		if (list != null && !list.isEmpty()) {

			sqlBuffer.append(" and cu.CUSTOMER_ID_HIGH in (");
			for (int i = 0; i < list.size(); i++) {
				sqlBuffer.append(list.get(i).getCustomerId());
				if (i != list.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");

			sqlBuffer.append(" UNION ALL  "
					+ CustomSQLUtil.get(GET_INFO_CONTRACT_DATA));

			sqlBuffer.append(" and cu.CUSTOMER_ID_HIGH in (");
			for (int i = 0; i < list.size(); i++) {
				sqlBuffer.append(list.get(i).getCustomerId());
				if (i != list.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");

			sqlBuffer.append(" UNION ALL  "
					+ CustomSQLUtil.get(GET_INFO_CONTRACT_INTERNET));

			sqlBuffer.append(" and cu.CUSTOMER_ID_HIGH in (");
			for (int i = 0; i < list.size(); i++) {
				sqlBuffer.append(list.get(i).getCustomerId());
				if (i != list.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");

			final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
					CustFindContractWS.class);
			listContracts = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		}

		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public List<Map<String, Object>> findChildCustomer(List<String> list) {
		List<Map<String, Object>> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_CUSTOMERS));

		if (list != null && !list.isEmpty()) {

			sqlBuffer.append("  CUSTOMER_ID_HIGH in (");
			for (int i = 0; i < list.size(); i++) {
				sqlBuffer.append(list.get(i));
				if (i != list.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			final CustomBeanPropertyRowMapper<String> rowMapper = new CustomBeanPropertyRowMapper<String>(
					String.class);
			listContracts = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().queryForList(sqlBuffer.toString());
		}

		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public List<CustFindContractWS> findContract(List<String> listCustomer,
			String nd, String coId) {
		List<CustFindContractWS> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CONTRACT_FIXE));

		if (!GRCStringUtil.isNullOrEmpty(coId)) {
			sqlBuffer.append(" and ca.CO_ID= " + coId);
		}

		if (!GRCStringUtil.isNullOrEmpty(nd)) {
			sqlBuffer.append(" and dn.DN_NUM= '" + nd + "'");
		}

		if (listCustomer != null && !listCustomer.isEmpty()) {

			sqlBuffer.append(" and cu.CUSTOMER_ID in (");
			for (int i = 0; i < listCustomer.size(); i++) {
				sqlBuffer.append(listCustomer.get(i));
				if (i != listCustomer.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");

			sqlBuffer.append(" UNION ALL  "
					+ CustomSQLUtil.get(GET_INFO_CONTRACT_DATA));

			if (!GRCStringUtil.isNullOrEmpty(coId)) {
				sqlBuffer.append(" and ca.CO_ID= " + coId);
			}

			if (listCustomer != null && !listCustomer.isEmpty()) {

				sqlBuffer.append(" and cu.CUSTOMER_ID in (");
				for (int i = 0; i < listCustomer.size(); i++) {
					sqlBuffer.append(listCustomer.get(i));
					if (i != listCustomer.size() - 1) {
						sqlBuffer.append(" , ");
					}
				}
				sqlBuffer.append(" ) ");
			}

			if (!GRCStringUtil.isNullOrEmpty(nd)) {
				sqlBuffer.append(" and p.PORT_NUM= '" + nd + "'");
			}

			sqlBuffer.append(" UNION ALL  "
					+ CustomSQLUtil.get(GET_INFO_CONTRACT_INTERNET));

			if (!GRCStringUtil.isNullOrEmpty(coId)) {
				sqlBuffer.append(" and ca.CO_ID= " + coId);
			}

			if (listCustomer != null && !listCustomer.isEmpty()) {

				sqlBuffer.append(" and cu.CUSTOMER_ID in (");
				for (int i = 0; i < listCustomer.size(); i++) {
					sqlBuffer.append(listCustomer.get(i));
					if (i != listCustomer.size() - 1) {
						sqlBuffer.append(" , ");
					}
				}
				sqlBuffer.append(" ) ");
			}

			if (!GRCStringUtil.isNullOrEmpty(nd)) {
				sqlBuffer.append(" and p.PORT_NUM= '" + nd + "'");
			}

			final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
					CustFindContractWS.class);
			listContracts = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		}
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public String getCustomerByCustCode(String custcode) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> getCustomerByCustCode: custCode=" + custcode);

		final StringBuilder sqlBuffer = new StringBuilder(
				GET_CUSTOMER_BY_CUSTCODE);
		sqlBuffer.append(" custcode='" + custcode + "'");
		Object result = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations()
				.queryForObject(sqlBuffer.toString(), Object.class);

		LOG.debug("<-- getCustomerByCustCode");
		return String.valueOf(result);
	}

}
