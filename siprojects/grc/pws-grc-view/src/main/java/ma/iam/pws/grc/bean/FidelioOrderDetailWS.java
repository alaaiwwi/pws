/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class FidelioOrderDetailWS.
 */
public class FidelioOrderDetailWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -1611477001670102725L;

    /** The order id. */
    private String orderId;

    /** The statusdate. */
    private String statusdate;

    /** The login id. */
    private String loginId;

    /** The agence code. */
    private String agenceCode;

    /** The description. */
    private String description;

    /** The disponibilite. */
    private String disponibilite;

    /** The entry date. */
    private String entryDate;

    /** The article description. */
    private String articleDescription;

    /** The point unit price. */
    private String pointUnitPrice;

    /** The article comment. */
    private String articleComment;

    /** The type description. */
    private String typeDescription;

    /** The total rachat. */
    private String totalRachat;

    /** The complement argent. */
    private String complementArgent;

    /** The point utilise. */
    private String pointUtilise;

    /** The en cours utilise. */
    private String enCoursUtilise;

    /** The total point. */
    private String totalPoint;

    /**
     * Gets the order id.
     * 
     * @return the order id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the order id.
     * 
     * @param orderId
     *            the new order id
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets the statusdate.
     * 
     * @return the statusdate
     */
    public String getStatusdate() {
        return statusdate;
    }

    /**
     * Sets the statusdate.
     * 
     * @param statusdate
     *            the new statusdate
     */
    public void setStatusdate(final String statusdate) {
        this.statusdate = statusdate;
    }

    /**
     * Gets the login id.
     * 
     * @return the login id
     */
    public String getLoginId() {
        return loginId;
    }

    /**
     * Sets the login id.
     * 
     * @param loginId
     *            the new login id
     */
    public void setLoginId(final String loginId) {
        this.loginId = loginId;
    }

    /**
     * Gets the agence code.
     * 
     * @return the agence code
     */
    public String getAgenceCode() {
        return agenceCode;
    }

    /**
     * Sets the agence code.
     * 
     * @param agenceCode
     *            the new agence code
     */
    public void setAgenceCode(final String agenceCode) {
        this.agenceCode = agenceCode;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the disponibilite.
     * 
     * @return the disponibilite
     */
    public String getDisponibilite() {
        return disponibilite;
    }

    /**
     * Sets the disponibilite.
     * 
     * @param disponibilite
     *            the new disponibilite
     */
    public void setDisponibilite(final String disponibilite) {
        this.disponibilite = disponibilite;
    }

    /**
     * Gets the entry date.
     * 
     * @return the entry date
     */
    public String getEntryDate() {
        return entryDate;
    }

    /**
     * Sets the entry date.
     * 
     * @param entryDate
     *            the new entry date
     */
    public void setEntryDate(final String entryDate) {
        this.entryDate = entryDate;
    }

    /**
     * Gets the article description.
     * 
     * @return the article description
     */
    public String getArticleDescription() {
        return articleDescription;
    }

    /**
     * Sets the article description.
     * 
     * @param articleDescription
     *            the new article description
     */
    public void setArticleDescription(final String articleDescription) {
        this.articleDescription = articleDescription;
    }

    /**
     * Gets the point unit price.
     * 
     * @return the point unit price
     */
    public String getPointUnitPrice() {
        return pointUnitPrice;
    }

    /**
     * Sets the point unit price.
     * 
     * @param pointUnitPrice
     *            the new point unit price
     */
    public void setPointUnitPrice(final String pointUnitPrice) {
        this.pointUnitPrice = pointUnitPrice;
    }

    /**
     * Gets the article comment.
     * 
     * @return the article comment
     */
    public String getArticleComment() {
        return articleComment;
    }

    /**
     * Sets the article comment.
     * 
     * @param articleComment
     *            the new article comment
     */
    public void setArticleComment(final String articleComment) {
        this.articleComment = articleComment;
    }

    /**
     * Gets the type description.
     * 
     * @return the type description
     */
    public String getTypeDescription() {
        return typeDescription;
    }

    /**
     * Sets the type description.
     * 
     * @param typeDescription
     *            the new type description
     */
    public void setTypeDescription(final String typeDescription) {
        this.typeDescription = typeDescription;
    }

    /**
     * Gets the total rachat.
     * 
     * @return the total rachat
     */
    public String getTotalRachat() {
        return totalRachat;
    }

    /**
     * Sets the total rachat.
     * 
     * @param totalRachat
     *            the new total rachat
     */
    public void setTotalRachat(final String totalRachat) {
        this.totalRachat = totalRachat;
    }

    /**
     * Gets the complement argent.
     * 
     * @return the complement argent
     */
    public String getComplementArgent() {
        return complementArgent;
    }

    /**
     * Sets the complement argent.
     * 
     * @param complementArgent
     *            the new complement argent
     */
    public void setComplementArgent(final String complementArgent) {
        this.complementArgent = complementArgent;
    }

    /**
     * Gets the point utilise.
     * 
     * @return the point utilise
     */
    public String getPointUtilise() {
        return pointUtilise;
    }

    /**
     * Sets the point utilise.
     * 
     * @param pointUtilise
     *            the new point utilise
     */
    public void setPointUtilise(final String pointUtilise) {
        this.pointUtilise = pointUtilise;
    }

    /**
     * Gets the en cours utilise.
     * 
     * @return the en cours utilise
     */
    public String getEnCoursUtilise() {
        return enCoursUtilise;
    }

    /**
     * Sets the en cours utilise.
     * 
     * @param enCoursUtilise
     *            the new en cours utilise
     */
    public void setEnCoursUtilise(final String enCoursUtilise) {
        this.enCoursUtilise = enCoursUtilise;
    }

    /**
     * Gets the total point.
     * 
     * @return the total point
     */
    public String getTotalPoint() {
        return totalPoint;
    }

    /**
     * Sets the total point.
     * 
     * @param totalPoint
     *            the new total point
     */
    public void setTotalPoint(final String totalPoint) {
        this.totalPoint = totalPoint;
    }

}
