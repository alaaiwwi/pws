/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.List;

import ma.iam.pws.grc.bean.CreditMobileWS;
import ma.iam.pws.grc.bean.CugWS;
import ma.iam.pws.grc.bean.InfoDataWS;
import ma.iam.pws.grc.bean.RdbWS;
import ma.iam.pws.grc.bean.ServiceCreditWS;
import ma.iam.pws.grc.bean.ServiceDnsWS;
import ma.iam.pws.grc.bean.ServiceHistoryWS;
import ma.iam.pws.grc.bean.ServiceParamWS;
import ma.iam.pws.grc.bean.ServiceWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import ma.iam.pws.grc.dao.service.IServiceDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceManagerBiz.
 */
@Component(value = "serviceManagerBiz")
public class ServiceManagerBiz extends CommonManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ServiceManagerBiz.class);

	/**
	 * Cherche les services.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<ServiceWS> findServiceForContract(final String contractIdBscs,
			final int product) throws GrcWsException {
		LOG.info("-> findServices: contractIdBscs {}", contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final List<ServiceWS> listServices = iServiceDAO
				.findServices(contractIdBscs);
		LOG.info("<- findServices");
		return listServices;
	}

	/**
	 * Cherche les services.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<ServiceDnsWS> findServicesDns(final String contractIdBscs,
			final int product) throws GrcWsException {
		LOG.info("-> findServicesDns: contractIdBscs {}", contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(ProduitTypeEnum.INTERNET
				.getIdTypeProduit());

		final List<ServiceDnsWS> listServices = iServiceDAO
				.findServicesDns(contractIdBscs);
		LOG.info("<- findServicesDns");
		return listServices;
	}

	/**
	 * Find services credit.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	/*
	 * f.tatbi FC 5864 delete parameter produit
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<ServiceCreditWS> findServicesCredit(final String contractIdBscs)
			throws GrcWsException {
		LOG.info("-> findServicesCredit: contractIdBscs {}", contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(ProduitTypeEnum.MOBILE
				.getIdTypeProduit());

		final List<ServiceCreditWS> listServices = iServiceDAO
				.findServicesCredit(contractIdBscs);
		LOG.info("<- findServicesCredit");
		return listServices;
	}

	/**
	 * Find cug.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<CugWS> findCug(final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findCug: contractIdBscs {}", contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final List<CugWS> listServices = iServiceDAO.findCug(contractIdBscs);
		LOG.info("<- findCug");
		return listServices;
	}

	/**
	 * Find credit mobile params.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the credit mobile ws
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public CreditMobileWS findCreditMobileParams(final String contractIdBscs,
			final int product) throws GrcWsException {
		LOG.info("-> findCreditMobileParams: contractIdBscs {}", contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final CreditMobileWS listServices = iServiceDAO
				.findCreditMobileParams(contractIdBscs);
		LOG.info("<- findCreditMobileParams");
		return listServices;
	}

	/**
	 * Cherche l'historique des services.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param sncode
	 *            the sncode
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<ServiceHistoryWS> findServiceHistory(
			final String contractIdBscs, final String shdes, final int product)
			throws GrcWsException {
		LOG.info("-> findServiceHistory: contractIdBscs {} sncode {}",
				contractIdBscs, shdes);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétres incorrectes contractIdBscs: " + contractIdBscs);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final List<ServiceHistoryWS> listServicesHist = iServiceDAO
				.findServiceHistory(contractIdBscs, shdes);
		LOG.info("<- findServiceHistory");
		return listServicesHist;
	}

	/**
	 * Cherche les paramétres du service.
	 * 
	 * @param paramId
	 *            l'identifiant du paramétre
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<ServiceParamWS> findServiceParams(final String shdes,
			final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findServiceParams: shdes {} contractIdBscs {}", shdes,
				contractIdBscs);
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final List<ServiceParamWS> listServicesParam = iServiceDAO
				.findServiceParams(shdes, contractIdBscs);
		LOG.info("<- findServiceParams");
		return listServicesParam;
	}

	/**
	 * Cherche les paramétres du RDB.
	 * 
	 * @param numNd
	 *            the num nd
	 * @param product
	 *            le produit
	 * @return the rdb ws
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	// hors perimetre Atos
	// @Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	// public RdbWS findRdbParams(final String numNd, final int product) throws
	// GrcWsException {
	// LOG.info("-> findRdbParams: numNd {}", numNd);
	// if ((!WSValidator.validateNumber(numNd) || (numNd.length() !=
	// GRCWSConstantes.PHONE_NUMBER_WITH_PREFIX_DIGITS))
	// && (numNd.length() != GRCWSConstantes.PHONE_NUMBER_DIGITS)) {
	// // throw new
	// // GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
	// // GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + numNd);
	// return null;
	// }
	// final IServiceDAO iServiceDAO = getServiceDAORdbImpl();
	//
	// final RdbWS rdbParam = iServiceDAO.findRdbParams(numNd);
	// LOG.info("<- findRdbParams");
	// return rdbParam;
	// }

	/**
	 * Cherche les informations data.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param contractIdNeto
	 *            l'identifiant du contrat netonomy correspondant au client
	 * @param product
	 *            le produit
	 * @return the info data ws
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public InfoDataWS findInfoData(final String contractIdBscs,
			final String contractIdNeto, final int product)
			throws GrcWsException {
		LOG.info("-> findInfoData: contractIdBscs {} contractIdNeto {}",
				contractIdBscs, contractIdNeto);

		if (!WSValidator.validateNumber(contractIdBscs)
				|| !WSValidator.validateNumber(contractIdNeto)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétres incorrectes contractIdBscs: " + contractIdBscs
							+ " contractIdNeto:" + contractIdNeto);
		}
		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final InfoDataWS infoData = iServiceDAO.findInfoData(contractIdBscs,
				contractIdNeto);
		LOG.info("<- findInfoData");
		return infoData;
	}

	/**
	 * Find sla class.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the string
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public String findSLAClass(final String contractIdBscs, final int product)
			throws GrcWsException {
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"findSLAClass : paramétres incorrectes contractIdBscs: "
							+ contractIdBscs);
		}

		final IServiceDAO iServiceDAO = getServiceDAOForProduct(product);

		final String infoSla = iServiceDAO.getSlaClass(contractIdBscs);
		LOG.info("<- findInfoData");
		return infoSla;
	}

}
