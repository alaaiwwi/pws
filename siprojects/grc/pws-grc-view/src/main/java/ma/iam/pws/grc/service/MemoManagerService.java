/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.MemoAddInfoWS;
import ma.iam.pws.grc.bean.MemoWS;
import ma.iam.pws.grc.bean.MemoWiamWS;
import ma.iam.pws.grc.bean.TerminationReasonWS;
import ma.iam.pws.grc.business.MemoManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class MemoManagerService.
 */
@WebService(serviceName = "MemoManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "memoManagerService")
public class MemoManagerService extends SpringBeanAutowiringSupport {
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(MemoManagerService.class);

    /** The customer infos manager biz. */
    @Autowired
    private MemoManagerBiz memoManagerBiz;

    /**
     * Cherche les memos.
     * 
     * @param customerId
     *            l'identifiant client BSCS (CUSTOMER_ID)
     * @param contractIdBscs
     *            le contrat bscs du client (champ CO_ID)
     * @param product
     *            le produit
     * @return la liste des résultats
     * @throws GrcWsException
     *             l'exception GrcWsException
     */
    @WebMethod(operationName = "findMemos")
    @WebResult(name = "ListMemoWS")
    public List<MemoWS> findMemos(@WebParam(name = "customerId") final String customerId, @WebParam(name = "contractIdBscs") final String contractIdBscs,
            @WebParam(name = "produit") final int product) throws GrcWsException {
        LOG.trace("@WebMethod: memoManagerBiz.findMemos customerId={} contractIdBscs={}", customerId, contractIdBscs);
        return memoManagerBiz.findMemos(customerId, contractIdBscs, product);
    }

    /**
     * Insert memo wiam.
     * 
     * @param memoWiamWS
     *            the memo wiam ws
     * @param listMemoAddInfos
     *            the list memo add infos
     * @param product
     *            le produit
     * @return Vrai pour insertion réussie, Faux sinon
     * @throws GrcWsException
     *             the grc ws exception
     */
    @WebMethod(operationName = "insertMemoWiam")
    @WebResult(name = "BooleanResult")
    public Boolean insertMemoWiam(@WebParam(name = "memoWiamWS") final MemoWiamWS memoWiamWS,
            @WebParam(name = "listMemoAddInfos") final List<MemoAddInfoWS> listMemoAddInfos, @WebParam(name = "produit") final int product)
            throws GrcWsException {
        LOG.trace("@WebMethod: memoManagerBiz.insertMemoWiam memoWiamWS={}", memoWiamWS);
        return memoManagerBiz.insertMemoWiam(memoWiamWS, listMemoAddInfos, product);
    }

    /**
     * Remonte les motifs de résiliation.
     * 
     * @param product
     *            the product
     * @return la liste des motifs de résiliation
     * @throws GrcWsException
     *             the grc ws exception
     */
    @WebMethod(operationName = "getTerminationReasons")
    @WebResult(name = "ListTerminationReason")
    public List<TerminationReasonWS> getTerminationReasons(@WebParam(name = "produit") final int product) throws GrcWsException {
        LOG.trace("@WebMethod: memoManagerBiz.getTerminationReasons");
        return memoManagerBiz.getTerminationReasons(product);
    }
    @Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}

}
