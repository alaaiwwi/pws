/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.FidelioAccountWS;
import ma.iam.pws.grc.bean.FidelioActionHistWS;
import ma.iam.pws.grc.bean.FidelioOrderDetailWS;
import ma.iam.pws.grc.bean.FidelioOrderHistWS;
import ma.iam.pws.grc.business.FidelioManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class FidelioManagerService.
 */
@WebService(serviceName = "FidelioManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "fidelioManagerService")
public class FidelioManagerService extends SpringBeanAutowiringSupport {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(FidelioManagerService.class);

	/** The fidelio manager biz. */
	@Autowired
	private FidelioManagerBiz fidelioManagerBiz;

	/*
	 * f.tatbi FC 5864 add parameter customer_id
	 */
	/**
	 * Cherche les fidelio account.
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return the fidelio account ws
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findFidelioAccount")
	@WebResult(name = "FidelioAccountWS")
	public FidelioAccountWS findFidelioAccount(
			@WebParam(name = "customer_id") final String customer_id,
			@WebParam(name = "custCode") final String custCode,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: fidelioManagerBiz.findFidelioAccount custCode={}",
				custCode);
		return fidelioManagerBiz.findFidelioAccount(customer_id, custCode,
				product);
	}

	/*
	 * f.tatbi FC 5864 add parameter customer_id
	 */
	/**
	 * Cherche les fidelio action hist.
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findFidelioActionHist")
	@WebResult(name = "ListFidelioActionHistWS")
	public List<FidelioActionHistWS> findFidelioActionHist(
			@WebParam(name = "customer_id") final String customer_id,
			@WebParam(name = "custCode") final String custCode,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: fidelioManagerBiz.findFidelioActionHist custCode={}",
				custCode);
		return fidelioManagerBiz.findFidelioActionHist(customer_id, custCode,
				product);
	}

	/*
	 * F.tatbi FC 5864 add parameter customer_id
	 */
	/**
	 * Cherche les fidelio order hist.
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findFidelioOrderHist")
	@WebResult(name = "ListFidelioOrderHistWS")
	public List<FidelioOrderHistWS> findFidelioOrderHist(
			@WebParam(name = "customer_id") final String customer_id,
			@WebParam(name = "custCode") final String custCode,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: fidelioManagerBiz.findFidelioOrderHist custCode={}",
				custCode);
		return fidelioManagerBiz.findFidelioOrderHist(customer_id,custCode, product);
	}

	/**
	 * Cherche les fidelio order detail.
	 * 
	 * @param orderId
	 *            the order id
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findFidelioOrderDetail")
	@WebResult(name = "ListFidelioOrderDetailWS")
	public List<FidelioOrderDetailWS> findFidelioOrderDetail(
			@WebParam(name = "orderId") final String orderId,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: fidelioManagerBiz.findFidelioOrderDetail orderId={}",
				orderId);
		return fidelioManagerBiz.findFidelioOrderDetail(orderId, product);
	}

	@Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
