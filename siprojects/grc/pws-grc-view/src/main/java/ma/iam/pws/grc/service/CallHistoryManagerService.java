/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.DetailsAppelWS;
import ma.iam.pws.grc.business.CallHistoryManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class CallHistoryManagerService.
 * 
 * ftatbi probleme RTX WS ne sera pas publié
 */
//@WebService(serviceName = "CallHistoryManagerService")
//@Component(value = "callHistoryManagerService")
public class CallHistoryManagerService extends SpringBeanAutowiringSupport {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(CallHistoryManagerService.class);

    /** The customer infos manager biz. */
    @Autowired
    private CallHistoryManagerBiz callHistoryManagerBiz;

    /**
     * Cherche les details appel.
     * 
     * @param customerId
     *            l'identifiant client BSCS (CUSTOMER_ID)
     * @param contractIdBscs
     *            le contrat bscs du client (champ CO_ID)
     * @param dateDebut
     *            la date de début
     * @param dateFin
     *            la date de fin
     * @param product
     *            le produit
     * @return la liste des résultats
     * @throws GrcWsException
     *             l'exception GrcWsException
     */
    @WebMethod(operationName = "findDetailsAppel")
    @WebResult(name = "ListDetailsAppelWS")
    public List<DetailsAppelWS> findDetailsAppel(@WebParam(name = "customerId") final String customerId,
            @WebParam(name = "contractIdBscs") final String contractIdBscs, @WebParam(name = "dateDebut") final String dateDebut,
            @WebParam(name = "dateFin") final String dateFin, @WebParam(name = "produit") final int product) throws GrcWsException {
        LOG.trace("@WebMethod: callHistoryManagerBiz.findDetailsAppel customerId={} contractIdBscs={}", customerId, contractIdBscs);
        return callHistoryManagerBiz.findDetailsAppel(customerId, contractIdBscs, dateDebut, dateFin, product);
    }
    
    @Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
