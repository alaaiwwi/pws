/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class PaymentWS.
 */
public class PaymentWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2181924681461358187L;

    /** The ref facture. */
    private String refFacture;

    /** The num reference. */
    private String numReference;

    /** The num transaction. */
    private String numTransaction;

    /** The num facture. */
    private String numFacture;

    /** The type. */
    private String type;

    /** The date paiement. */
    private String datePaiement;

    /** The date cheque. */
    private String dateCheque;

    /** The montant regle. */
    private String montantRegle;

    /** The montant courant. */
    private String montantCourant;

    /** The commentaire. */
    private String commentaire;

    /**
     * Gets the ref facture.
     * 
     * @return the ref facture
     */
    public String getRefFacture() {
        return refFacture;
    }

    /**
     * Sets the ref facture.
     * 
     * @param refFacture
     *            the new ref facture
     */
    public void setRefFacture(final String refFacture) {
        this.refFacture = refFacture;
    }

    /**
     * Gets the num reference.
     * 
     * @return the num reference
     */
    public String getNumReference() {
        return numReference;
    }

    /**
     * Sets the num reference.
     * 
     * @param numReference
     *            the new num reference
     */
    public void setNumReference(final String numReference) {
        this.numReference = numReference;
    }

    /**
     * Gets the num transaction.
     * 
     * @return the num transaction
     */
    public String getNumTransaction() {
        return numTransaction;
    }

    /**
     * Sets the num transaction.
     * 
     * @param numTransaction
     *            the new num transaction
     */
    public void setNumTransaction(final String numTransaction) {
        this.numTransaction = numTransaction;
    }

    /**
     * Gets le numéro de la facture.
     * 
     * @return le numéro de la facture
     */
    public String getNumFacture() {
        return numFacture;
    }

    /**
     * Sets le numéro de la facture.
     * 
     * @param numFacture
     *            the new num facture
     */
    public void setNumFacture(final String numFacture) {
        this.numFacture = numFacture;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the date paiement.
     * 
     * @return the date paiement
     */
    public String getDatePaiement() {
        return datePaiement;
    }

    /**
     * Sets the date paiement.
     * 
     * @param datePaiement
     *            the new date paiement
     */
    public void setDatePaiement(final String datePaiement) {
        this.datePaiement = datePaiement;
    }

    /**
     * Gets the date cheque.
     * 
     * @return the date cheque
     */
    public String getDateCheque() {
        return dateCheque;
    }

    /**
     * Sets the date cheque.
     * 
     * @param dateCheque
     *            the new date cheque
     */
    public void setDateCheque(final String dateCheque) {
        this.dateCheque = dateCheque;
    }

    /**
     * Gets the montant regle.
     * 
     * @return the montant regle
     */
    public String getMontantRegle() {
        return montantRegle;
    }

    /**
     * Sets the montant regle.
     * 
     * @param montantRegle
     *            the new montant regle
     */
    public void setMontantRegle(final String montantRegle) {
        this.montantRegle = montantRegle;
    }

    /**
     * Gets the montant courant.
     * 
     * @return the montant courant
     */
    public String getMontantCourant() {
        return montantCourant;
    }

    /**
     * Sets the montant courant.
     * 
     * @param montantCourant
     *            the new montant courant
     */
    public void setMontantCourant(final String montantCourant) {
        this.montantCourant = montantCourant;
    }

    /**
     * Gets the commentaire.
     * 
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets the commentaire.
     * 
     * @param commentaire
     *            the new commentaire
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

}
