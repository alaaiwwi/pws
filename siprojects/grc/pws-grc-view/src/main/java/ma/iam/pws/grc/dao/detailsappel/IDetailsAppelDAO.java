/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.detailsappel;

import java.util.List;

import ma.iam.pws.grc.bean.CustomDetailWS;
import ma.iam.pws.grc.bean.DetailsAppelWS;

/**
 * The Interface IDetailsAppelDAO.
 */
public interface IDetailsAppelDAO {

	/**
	 * Retourne les détails des appels d'un client é partir de sont CUSTOMER_ID,
	 * CO_ID et pour un intervalle de dates donné
	 * 
	 * @param customerId
	 *            l'identifiant du client CUSTOMER_ID de BSCS
	 * @param contractIdBscs
	 *            l'identifiant du contrat BSCS (CO_ID)
	 * @param dateDebut
	 *            la date de début
	 * @param dateFin
	 *            la date de fin
	 * @return the details appels
	 */
	List<DetailsAppelWS> getDetailsAppels(final String customerId,
			final String contractIdBscs, final String dateDebut,
			final String dateFin);

	public CustomDetailWS getContract(final String contractIdBscs);


}
