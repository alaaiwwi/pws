/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


// TODO: Auto-generated Javadoc
/**
 * The Class FeesWS.
 */
public class MemoWiamWS extends GrcWS {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1422961456095660730L;

	/** The id. */
	private Integer memoId;

	/** The type. */
	private Integer memoTypeId;

	/** The statut. */
	private Integer memeoStatusId;

	/** The begin date. */
	private String dateDebut;

	/** The end date. */
	private String dateFin;

	/** The login. */
	private String login;

	/** The agence. */
	private String agence;

	/** The description. */
	private String description;

	/** The object type. */
	private Integer impactedObjectTypeId;

	/** The object impact. */
	private Integer impactedObjectId;

	/** The contract type. */
	private Integer memoContractType;

	/** The remise. */
	private String affectationRemise;

	/** The remise type. */
	private String typeRemise;

	/** The amount. */
	private double amount;

	/** The periode. */
	private Integer applicationPeriod;

	/** The effective date. */
	private String effectiveDate;

	/**
	 * Gets the memo id.
	 * 
	 * @return the memo id
	 */
	public Integer getMemoId() {
		return memoId;
	}

	/**
	 * Sets the memo id.
	 * 
	 * @param memoId
	 *            the new memo id
	 */
	public void setMemoId(final Integer memoId) {
		this.memoId = memoId;
	}

	/**
	 * Gets the memo type id.
	 * 
	 * @return the memo type id
	 */
	public Integer getMemoTypeId() {
		return memoTypeId;
	}

	/**
	 * Sets the memo type id.
	 * 
	 * @param memoTypeId
	 *            the new memo type id
	 */
	public void setMemoTypeId(final Integer memoTypeId) {
		this.memoTypeId = memoTypeId;
	}

	/**
	 * Gets the memeo status id.
	 * 
	 * @return the memeo status id
	 */
	public Integer getMemeoStatusId() {
		return memeoStatusId;
	}

	/**
	 * Sets the memeo status id.
	 * 
	 * @param memeoStatusId
	 *            the new memeo status id
	 */
	public void setMemeoStatusId(final Integer memeoStatusId) {
		this.memeoStatusId = memeoStatusId;
	}

	/**
	 * Gets the date debut.
	 * 
	 * @return the date debut
	 */
	public String getDateDebut() {
		return dateDebut;
	}

	/**
	 * Sets the date debut.
	 * 
	 * @param dateDebut
	 *            the new date debut
	 */
	public void setDateDebut(final String dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * Gets the date fin.
	 * 
	 * @return the date fin
	 */
	public String getDateFin() {
		return dateFin;
	}

	/**
	 * Sets the date fin.
	 * 
	 * @param dateFin
	 *            the new date fin
	 */
	public void setDateFin(final String dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * Gets the login.
	 * 
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login.
	 * 
	 * @param login
	 *            the new login
	 */
	public void setLogin(final String login) {
		this.login = login;
	}

	/**
	 * Gets the agence.
	 * 
	 * @return the agence
	 */
	public String getAgence() {
		return agence;
	}

	/**
	 * Sets the agence.
	 * 
	 * @param agence
	 *            the new agence
	 */
	public void setAgence(final String agence) {
		this.agence = agence;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the impacted object type id.
	 * 
	 * @return the impacted object type id
	 */
	public Integer getImpactedObjectTypeId() {
		return impactedObjectTypeId;
	}

	/**
	 * Sets the impacted object type id.
	 * 
	 * @param impactedObjectTypeId
	 *            the new impacted object type id
	 */
	public void setImpactedObjectTypeId(final Integer impactedObjectTypeId) {
		this.impactedObjectTypeId = impactedObjectTypeId;
	}

	/**
	 * Gets the impacted object id.
	 * 
	 * @return the impacted object id
	 */
	public Integer getImpactedObjectId() {
		return impactedObjectId;
	}

	/**
	 * Sets the impacted object id.
	 * 
	 * @param impactedObjectId
	 *            the new impacted object id
	 */
	public void setImpactedObjectId(final Integer impactedObjectId) {
		this.impactedObjectId = impactedObjectId;
	}

	/**
	 * Gets the memo contract type.
	 * 
	 * @return the memo contract type
	 */
	public Integer getMemoContractType() {
		return memoContractType;
	}

	/**
	 * Sets the memo contract type.
	 * 
	 * @param memoContractType
	 *            the new memo contract type
	 */
	public void setMemoContractType(final Integer memoContractType) {
		this.memoContractType = memoContractType;
	}

	/**
	 * Gets the affectation remise.
	 * 
	 * @return the affectation remise
	 */
	public String getAffectationRemise() {
		return affectationRemise;
	}

	/**
	 * Sets the affectation remise.
	 * 
	 * @param affectationRemise
	 *            the new affectation remise
	 */
	public void setAffectationRemise(final String affectationRemise) {
		this.affectationRemise = affectationRemise;
	}

	/**
	 * Gets the type remise.
	 * 
	 * @return the type remise
	 */
	public String getTypeRemise() {
		return typeRemise;
	}

	/**
	 * Sets the type remise.
	 * 
	 * @param typeRemise
	 *            the new type remise
	 */
	public void setTypeRemise(final String typeRemise) {
		this.typeRemise = typeRemise;
	}

	/**
	 * Gets the amount.
	 * 
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 * 
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(final double amount) {
		this.amount = amount;
	}

	/**
	 * Gets the application period.
	 * 
	 * @return the application period
	 */
	public Integer getApplicationPeriod() {
		return applicationPeriod;
	}

	/**
	 * Sets the application period.
	 * 
	 * @param applicationPeriod
	 *            the new application period
	 */
	public void setApplicationPeriod(final Integer applicationPeriod) {
		this.applicationPeriod = applicationPeriod;
	}

	/**
	 * Gets the effective date.
	 * 
	 * @return the effective date
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the effective date.
	 * 
	 * @param effectiveDate
	 *            the new effective date
	 */
	public void setEffectiveDate(final String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

}
