/*
 * @author Capgemini
 */
package ma.iam.pws.grc.interceptor;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import ma.iam.pws.grc.exceptions.GrcWsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.ThrowsAdvice;

import fr.capgemini.iam.grc.core.utils.GRCReflectUtils;

/**
 * The Class LoggerInterceptor.
 */
public class LoggerInterceptor implements MethodBeforeAdvice,
        AfterReturningAdvice, ThrowsAdvice {

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.aop.AfterReturningAdvice#afterReturning(java.lang
     * .Object, java.lang.reflect.Method, java.lang.Object[], java.lang.Object)
     */
    
    public void afterReturning(final Object result, final Method method,
            final Object[] args, final Object target) {

        final Logger log = LoggerFactory.getLogger(target.getClass());

        if (log.isInfoEnabled()) {
            final StringBuilder buff = new StringBuilder();
            buff.append("End Method [{}] - Result [{}]");

            String stringValue;
            if ((result instanceof List<?>)) {
                stringValue = GRCReflectUtils
                        .getFormattedStringValueFromArray(((List<?>) result)
                                .toArray());
            } else if ((result instanceof Set<?>)
                    && (((Set<?>) result).isEmpty())) {
                stringValue = GRCReflectUtils
                        .getFormattedStringValueFromArray(((Set<?>) result)
                                .toArray());
            } else if (result != null) {
                stringValue = result.toString();
            } else {
                stringValue = "VIDE";
            }

            log.info(buff.toString(), method.getName(), stringValue);
        }

    }

    /**
     * After throwing.
     * 
     * @param method
     *            the method
     * @param args
     *            the args
     * @param target
     *            the target
     * @param exception
     *            the exception
     */
    public void afterThrowing(final Method method, final Object[] args,
            final Object target, final Throwable exception) {
        final Logger log = LoggerFactory.getLogger(target.getClass());
        if (log.isErrorEnabled()) {
            final StringBuilder buff = new StringBuilder();

            buff.append("Exception in Method [{}] - Parameters [{}]");
            if (exception instanceof GrcWsException) {
                if (log.isTraceEnabled()) {
                    log.trace(buff.toString(), method.getName(), args);
                }
                log.error("Trace GrcWsException Exception [{}][{}]", exception
                        .toString(), String
                        .valueOf(((GrcWsException) exception).getFaultInfo()));
            } else {
                log.error(buff.toString(), method.getName(), args);
                log.error("Trace Exception", exception);
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.aop.MethodBeforeAdvice#before(java.lang.reflect.Method
     * , java.lang.Object[], java.lang.Object)
     */
    
    public void before(final Method method, final Object[] args,
            final Object target) {
        final Logger log = LoggerFactory.getLogger(target.getClass());
        if (log.isInfoEnabled()) {
            final StringBuilder buff = new StringBuilder();

            buff.append("Start Method [{}] - Parameters [{}]");
            log.info(buff.toString(), method.getName(), args);
        }
    }
}