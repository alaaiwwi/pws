package ma.iam.pws.grc.interceptor;

import java.sql.SQLException;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;

import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.common.injection.NoJSR250Annotations;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.BadSqlGrammarException;

import com.ctc.wstx.exc.WstxParsingException;

@NoJSR250Annotations
public class ExceptionInterceptor extends AbstractSoapInterceptor {

	private final static int HTTP_200 = 200;

	public ExceptionInterceptor() {
		super(Phase.PRE_LOGICAL);
	}

	// @Override
	public void handleMessage(SoapMessage message) throws Fault {
		Fault fault = (Fault) message.getContent(Exception.class);
		Throwable ex = fault.getCause();
		if (ex instanceof GrcWsException) {
			GrcWsException e = (GrcWsException) ex;
			fault.setStatusCode(HTTP_200);
			generateSoapFault(fault, e);
		} else if (ex instanceof SOAPFaultException
				|| ex instanceof SOAPException
				|| ex instanceof WstxParsingException) {

			GrcWsException e = new GrcWsException(
					ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"Erreur systéme au niveau du webservice ", ex);

			generateSoapFault(fault, e);

		} else if (ex instanceof SQLException
				|| ex instanceof BadSqlGrammarException
				|| ex instanceof DataIntegrityViolationException) {
			GrcWsException e = new GrcWsException(
					ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"Erreur lors de l'execution de la requéte SQL ");
			generateSoapFault(fault, e);

		} else if (ex instanceof WSSecurityException) {

			GrcWsException e = new GrcWsException(
					ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"Erreur lors de l'execution de <wsse:Security> header ");

			generateSoapFault(fault, e);
		}

		else {
			generateSoapFault(fault, ex);
		}
	}

	private void generateSoapFault(Fault fault, GrcWsException e) {
		fault.setFaultCode(createQName(e.getFaultInfo().getCode() != null ? e
				.getFaultInfo().getCode() : "WS-ERR"));
		fault.setMessage((e.getFaultInfo() != null
				&& e.getFaultInfo().getMessage() != null && !"".equals(e
				.getFaultInfo().getMessage())) ? e.getFaultInfo().getMessage()
				: e.getFaultInfo().getContext());

	}

	private static QName createQName(String errorCode) {
		return new QName("ws.iam.ma", String.valueOf(errorCode));
	}

	private void generateSoapFault(Fault fault, Throwable e) {
		fault.setFaultCode(createQName("WS-ERR"));
		fault.setMessage(e.getMessage());
	}
}
