/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.invoice;

import java.util.List;

import ma.iam.pws.grc.bean.InvoiceDetailWS;
import ma.iam.pws.grc.bean.InvoiceWS;
import ma.iam.pws.grc.bean.InvoiceWSFindInvoice;
import ma.iam.pws.grc.bean.PaymentDetailWS;
import ma.iam.pws.grc.bean.PaymentWS;
import ma.iam.pws.grc.bean.SearchInvoiceBean;

// TODO: Auto-generated Javadoc
/**
 * The Interface IInvoiceDAO.
 */
public interface IInvoiceDAO {

	final static String TICKLER_CODE = "HISTINV";
/*
 * f.tatbi FC 5373
 */
	final static String LITIGE_USER="LITIGE";

	/**
	 * Retourne la liste des factures d'un client dans un intervalle de dates
	 * donné
	 * 
	 * @param searchInvoiceBean
	 *            {@link ma.iam.pws.grc.bean.SearchInvoiceBean}
	 * @return la liste des résultats
	 */
	List<InvoiceWSFindInvoice> findInvoicesForCustomer(
			final SearchInvoiceBean searchInvoiceBean);

	/**
	 * Retourne les détails d'une facture é partir de son numéro
	 * 
	 * @param numFacture
	 *            le numéro de la facture
	 * @return la liste des résultats
	 */
	List<InvoiceDetailWS> findInvoicesDetails(final String numFacture);

	/**
	 * Retourne les détails des paiements liés é une facture donnée
	 * 
	 * @param invoiceExternalId
	 *            the invoice external id
	 * @return la liste des résultats
	 */
	List<PaymentDetailWS> findPaymentsDetails(final String invoiceExternalId);

	/**
	 * Retourne la liste des paiements d'un client dans un intervalle de dates
	 * donné
	 * 
	 * @param searchInvoiceBean
	 *            {@link ma.iam.pws.grc.bean.SearchInvoiceBean}
	 * @return la liste des résultats
	 */
	List<PaymentWS> findPayments(final SearchInvoiceBean searchInvoiceBean);

	/*
	 * f.tatbi FC 5864 
	 * add parametre idfacture
	 */
	/**
	 * Cherche les paiements liés é une facture
	 * 
	 * @param refFacture
	 *            Référence de la facture
	 * @param idFacture
	 *        id de la facture
	 * @return la liste des résultats
	 */
	List<PaymentWS> findPaymentsForInvoice(final String refFacture,final Integer idFacture);

	/**
	 * Cherche les montant global ouvert.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @return the double
	 */
	Double findMontantGlobalOuvert(final String customerId);

	/**
	 * Retourne la liste des Factures par rapport é une liste d'identifiants
	 * BSCS des factures (OHXACT / idFacture)
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @return la liste des résultats
	 */
	List<InvoiceWS> findInvoicesByListInvoiceExternalId(
			List<Integer> listInvoiceExternalId);

	/**
	 * Retourne la liste des Factures é partir des références BSCS (OHREFNUM /
	 * refFacture)
	 * 
	 * @param listReferenceFacture
	 *            the list reference facture
	 * @return the list
	 */
	List<InvoiceWS> findInvoicesByListReference(
			List<String> listReferenceFacture);

	/**
	 * Protége les factures données en entrée
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @return the boolean
	 */
	Boolean protectInvoices(final List<Integer> listInvoiceExternalId);

	/**
	 * Déprotége les factures données en entrée
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @return the boolean
	 */
	Boolean unProtectInvoices(final List<Integer> listInvoiceExternalId);

	public List<InvoiceWS> findDetailedInvoicesForCustomer(
			final SearchInvoiceBean searchInvoiceBean);

	/**
	 * Historiser les actions de protége et deprotege des factures
	 * 
	 * @param facture
	 *            id de facture
	 * @param user
	 *            utilisateur qui a fait l'action
	 * @param application
	 *            l'application qui a fait l'action
	 * @param msg
	 *            description de l'historique
	 * @return the boolean
	 */

	public Boolean insertHistory(Integer facture, String user,
			String application, String msg);

	/**
	 * Retourne la liste des Factures par rapport é une liste d'identifiants ou
	 * references BSCS des factures (OHXACT / idFacture,OHREFNUM /refFacture)
	 * 
	 * @param refcature
	 *            list ref Facture
	 * @param idFacture
	 *            list id Facture
	 * @param statut
	 *            statut Facture
	 * @return la liste des résultats
	 */

	public List<InvoiceWSFindInvoice> findInvoiceById(
			final List<String> refFacture, final List<Integer> idFacture,
			final String statut);
	
	
	/*
	 * f.tatbi FC 5373
	 */
	
	/**
	 * @param flag : FLAG
	 * @param order_ref : ref Facture
	 * @return boolean 
	 */
	public Boolean insertUnPaid(Integer flag, Integer order_ref);
}
