/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class ContractHistoryWS.
 */
public class RejetMediationWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    private String customerId;

    private String coId;

    private String dnNum;

    private String typeId;

    private String description;

    private String requestId;

    private String dateDerniereModification;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        sb = new StringBuilder();
        sb.append("[RejetMediationWS:");
        if (customerId != null) {
            sb.append(",customerId=").append(customerId);
        }
        if (coId != null) {
            sb.append(",coId=").append(coId);
        }
        if (dnNum != null) {
            sb.append(",dnNum=").append(dnNum);
        }
        if (typeId != null) {
            sb.append(",typeId=").append(typeId);
        }
        if (description != null) {
            sb.append(",description=").append(description);
        }
        if (requestId != null) {
            sb.append(",requestId=").append(requestId);
        }
        if (dateDerniereModification != null) {
            sb.append(",dateDerniereModification=").append(dateDerniereModification);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the coId
     */
    public String getCoId() {
        return coId;
    }

    /**
     * @param coId
     *            the coId to set
     */
    public void setCoId(final String coId) {
        this.coId = coId;
    }

    /**
     * @return the dnNum
     */
    public String getDnNum() {
        return dnNum;
    }

    /**
     * @param dnNum
     *            the dnNum to set
     */
    public void setDnNum(final String dnNum) {
        this.dnNum = dnNum;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId
     *            the typeId to set
     */
    public void setTypeId(final String typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId
     *            the requestId to set
     */
    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the dateDerniereModification
     */
    public String getDateDerniereModification() {
        return dateDerniereModification;
    }

    /**
     * @param dateDerniereModification
     *            the dateDerniereModification to set
     */
    public void setDateDerniereModification(final String dateDerniereModification) {
        this.dateDerniereModification = dateDerniereModification;
    }

}
