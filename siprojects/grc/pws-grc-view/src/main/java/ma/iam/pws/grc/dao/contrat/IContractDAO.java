/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.contrat;

import java.util.List;

import ma.iam.pws.grc.bean.ContractHistoryWS;
import ma.iam.pws.grc.bean.PeripheriqueHistoryWS;
import ma.iam.pws.grc.bean.PlanTarifaireHistoryWS;
import ma.iam.pws.grc.bean.PreidentificationWS;

/**
 * The Interface IContractDAO.
 */
public interface IContractDAO {

	/**
	 * Cherche l'historique du contrat donné en entrée
	 * 
	 * @param searchCustAcctBean
	 *            le bean de recherche client
	 * @return la liste des résultats
	 */
	List<ContractHistoryWS> findContractHistory(String contractIdBsc);

	/**
	 * Cherche l'historique des périphériques du contrat donné en entrée
	 * 
	 * @param contractIdBsc
	 *            the contract id bsc
	 * @return la liste des résultats
	 */
	List<PeripheriqueHistoryWS> findPeripheriqueHistory(String contractIdBsc);

	/**
	 * Cherche l'historique des plans tarifaires du contrat donné en entrée
	 * 
	 * @param contractIdBsc
	 *            the contract id bsc
	 * @return la liste des résultats
	 */
	List<PlanTarifaireHistoryWS> findPlanTarifaireHistory(String contractIdBsc);

	/**
	 * Cherche les données de préidentification
	 * 
	 * @param contractIdBsc
	 *            the contract id bsc
	 * @return la liste des résultats
	 */
	List<PreidentificationWS> findDonneesPreidentification(String contractIdBsc);

	/**
	 * Cherche les données de préidentification
	 * 
	 * @param contractIdBsc
	 *            the contract id bsc
	 * @return la liste des résultats
	 */
	List<PreidentificationWS> findDonneesPreidentificationOM(
			String contractIdBsc);

	/*
	 * f.tatbi FC 5864 creation Methode pour FIXE pour recuperé le login du
	 * createur du contract
	 */
	/**
	 * Cherche le createur de contract
	 * 
	 * @param contractIdBsc
	 *            the contract id bsc
	 * @return résultats
	 */
	ContractHistoryWS getCreatContract(String contractIdFscs);

}
