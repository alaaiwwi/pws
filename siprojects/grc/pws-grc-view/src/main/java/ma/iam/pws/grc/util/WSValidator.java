/*
 * @author Capgemini
 */
package ma.iam.pws.grc.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ma.iam.pws.grc.bean.IntervalWS;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

/**
 * The Class IntervalWSValidator.
 */
public final class WSValidator {

	/**
	 * Instantiates a new interval ws validator.
	 */
	private WSValidator() {

	}

	/** The FORMAT. */
	private static final String DATE_FORMAT = "dd/MM/yyyy";

	/**
	 * Validate interval.
	 * 
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @return the boolean
	 */
	public static Boolean validateInterval(final IntervalWS interval) {
		return validateInterval(interval, DATE_FORMAT);
	}

	/**
	 * Validate interval.
	 * 
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @param format
	 *            the format
	 * @return the boolean
	 */
	public static Boolean validateInterval(final IntervalWS interval,
			final String format) {
		Boolean result = Boolean.FALSE;
		try {
			if ((null != interval)
					&& (null != interval.getEndDate())
					&& (null != interval.getStartDate())
					&& (stringToDate(interval.getEndDate(), format)
							.after(stringToDate(interval.getStartDate(), format)))) {
				result = Boolean.TRUE;
			}
		} catch (final ParseException e) {
			result = Boolean.FALSE;
		}
		return result;
	}

	/**
	 * Validate date.
	 * 
	 * @param date
	 *            the date
	 * @return the boolean
	 */
	public static Boolean validateDate(final String date) {
		return validateDate(date, DATE_FORMAT);
	}

	/**
	 * Validate date.
	 * 
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return the boolean
	 */
	public static Boolean validateDate(final String date, final String format) {
		Boolean result = Boolean.TRUE;
		try {
			stringToDate(date, format);
		} catch (final ParseException e) {
			result = Boolean.FALSE;
		} catch (final IllegalArgumentException e) {
			result = Boolean.FALSE;
		}
		return result;
	}

	/**
	 * String to date.
	 * 
	 * @param strDate
	 *            the str date
	 * @param format
	 *            the format
	 * @return the date
	 * @throws ParseException
	 *             the parse exception
	 */
	private static Date stringToDate(final String strDate, final String format)
			throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(strDate);
	}

	/**
	 * Validate integer.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateNumber(final String value) {
		Boolean result = Boolean.TRUE;
		if (GRCStringUtil.isNullOrEmpty(value)) {
			result = Boolean.FALSE;
		} else {
			try {
				Long.parseLong(value);
			} catch (final NumberFormatException e) {
				result = Boolean.FALSE;
			}
		}
		return result;
	}

	/**
	 * Validate Mobile Number.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateMobileNumber(final String value) {

		Pattern pattern = Pattern.compile("(0|00212|\\+212|212)6[0-9]{8}");
		Matcher matcher = pattern.matcher(value);
		return matcher.find();

	}

}
