/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class RdbWS.
 */
public class RdbWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7784712066033633311L;

    /** The port isp. */
    private String portIsp;

    /** The port debit. */
    private String portDebit;

    /** The port mod user. */
    private String portModUser;

    /** The port mod date. */
    private String portModDate;

    /** The dsl code. */
    private String dslCode;

    /** The dsl constructeur. */
    private String dslConstructeur;

    /** The dsl model. */
    private String dslModel;

    /** The dsl ip adress. */
    private String dslIpAdress;

    /** The carte type. */
    private String carteType;

    /** The carte capacite. */
    private String carteCapacite;

    /** The carte mod user. */
    private String carteModUser;

    /** The carte mod date. */
    private String carteModDate;

    /** The status. */
    private String status;

    /** The gateway constructeur. */
    private String gatewayConstructeur;

    /** The gateway code. */
    private String gatewayCode;

    /** The gateway ip. */
    private String gatewayIp;

    /** The locale nom. */
    private String localeNom;

    /** The locale code. */
    private String localeCode;

    /** The site nom. */
    private String siteNom;

    /** The site code. */
    private String siteCode;

    /** The site dr. */
    private String siteDr;

    /** The site zem. */
    private String siteZem;

    /**
     * Gets the port isp.
     * 
     * @return the port isp
     */
    public String getPortIsp() {
        return portIsp;
    }

    /**
     * Sets the port isp.
     * 
     * @param portIsp
     *            the new port isp
     */
    public void setPortIsp(final String portIsp) {
        this.portIsp = portIsp;
    }

    /**
     * Gets the port debit.
     * 
     * @return the port debit
     */
    public String getPortDebit() {
        return portDebit;
    }

    /**
     * Sets the port debit.
     * 
     * @param portDebit
     *            the new port debit
     */
    public void setPortDebit(final String portDebit) {
        this.portDebit = portDebit;
    }

    /**
     * Gets the port mod user.
     * 
     * @return the port mod user
     */
    public String getPortModUser() {
        return portModUser;
    }

    /**
     * Sets the port mod user.
     * 
     * @param portModUser
     *            the new port mod user
     */
    public void setPortModUser(final String portModUser) {
        this.portModUser = portModUser;
    }

    /**
     * Gets the port mod date.
     * 
     * @return the port mod date
     */
    public String getPortModDate() {
        return portModDate;
    }

    /**
     * Sets the port mod date.
     * 
     * @param portModDate
     *            the new port mod date
     */
    public void setPortModDate(final String portModDate) {
        this.portModDate = portModDate;
    }

    /**
     * Gets the dsl code.
     * 
     * @return the dsl code
     */
    public String getDslCode() {
        return dslCode;
    }

    /**
     * Sets the dsl code.
     * 
     * @param dslCode
     *            the new dsl code
     */
    public void setDslCode(final String dslCode) {
        this.dslCode = dslCode;
    }

    /**
     * Gets the dsl constructeur.
     * 
     * @return the dsl constructeur
     */
    public String getDslConstructeur() {
        return dslConstructeur;
    }

    /**
     * Sets the dsl constructeur.
     * 
     * @param dslConstructeur
     *            the new dsl constructeur
     */
    public void setDslConstructeur(final String dslConstructeur) {
        this.dslConstructeur = dslConstructeur;
    }

    /**
     * Gets the dsl model.
     * 
     * @return the dsl model
     */
    public String getDslModel() {
        return dslModel;
    }

    /**
     * Sets the dsl model.
     * 
     * @param dslModel
     *            the new dsl model
     */
    public void setDslModel(final String dslModel) {
        this.dslModel = dslModel;
    }

    /**
     * Gets the dsl ip adress.
     * 
     * @return the dsl ip adress
     */
    public String getDslIpAdress() {
        return dslIpAdress;
    }

    /**
     * Sets the dsl ip adress.
     * 
     * @param dslIpAdress
     *            the new dsl ip adress
     */
    public void setDslIpAdress(final String dslIpAdress) {
        this.dslIpAdress = dslIpAdress;
    }

    /**
     * Gets the carte type.
     * 
     * @return the carte type
     */
    public String getCarteType() {
        return carteType;
    }

    /**
     * Sets the carte type.
     * 
     * @param carteType
     *            the new carte type
     */
    public void setCarteType(final String carteType) {
        this.carteType = carteType;
    }

    /**
     * Gets the carte capacite.
     * 
     * @return the carte capacite
     */
    public String getCarteCapacite() {
        return carteCapacite;
    }

    /**
     * Sets the carte capacite.
     * 
     * @param carteCapacite
     *            the new carte capacite
     */
    public void setCarteCapacite(final String carteCapacite) {
        this.carteCapacite = carteCapacite;
    }

    /**
     * Gets the carte mod user.
     * 
     * @return the carte mod user
     */
    public String getCarteModUser() {
        return carteModUser;
    }

    /**
     * Sets the carte mod user.
     * 
     * @param carteModUser
     *            the new carte mod user
     */
    public void setCarteModUser(final String carteModUser) {
        this.carteModUser = carteModUser;
    }

    /**
     * Gets the carte mod date.
     * 
     * @return the carte mod date
     */
    public String getCarteModDate() {
        return carteModDate;
    }

    /**
     * Sets the carte mod date.
     * 
     * @param carteModDate
     *            the new carte mod date
     */
    public void setCarteModDate(final String carteModDate) {
        this.carteModDate = carteModDate;
    }

    /**
     * Gets le statut.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets le statut.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Gets the gateway constructeur.
     * 
     * @return the gateway constructeur
     */
    public String getGatewayConstructeur() {
        return gatewayConstructeur;
    }

    /**
     * Sets the gateway constructeur.
     * 
     * @param gatewayConstructeur
     *            the new gateway constructeur
     */
    public void setGatewayConstructeur(final String gatewayConstructeur) {
        this.gatewayConstructeur = gatewayConstructeur;
    }

    /**
     * Gets the gateway code.
     * 
     * @return the gateway code
     */
    public String getGatewayCode() {
        return gatewayCode;
    }

    /**
     * Sets the gateway code.
     * 
     * @param gatewayCode
     *            the new gateway code
     */
    public void setGatewayCode(final String gatewayCode) {
        this.gatewayCode = gatewayCode;
    }

    /**
     * Gets the gateway ip.
     * 
     * @return the gateway ip
     */
    public String getGatewayIp() {
        return gatewayIp;
    }

    /**
     * Sets the gateway ip.
     * 
     * @param gatewayIp
     *            the new gateway ip
     */
    public void setGatewayIp(final String gatewayIp) {
        this.gatewayIp = gatewayIp;
    }

    /**
     * Gets the locale nom.
     * 
     * @return the locale nom
     */
    public String getLocaleNom() {
        return localeNom;
    }

    /**
     * Sets the locale nom.
     * 
     * @param localeNom
     *            the new locale nom
     */
    public void setLocaleNom(final String localeNom) {
        this.localeNom = localeNom;
    }

    /**
     * Gets the locale code.
     * 
     * @return the locale code
     */
    public String getLocaleCode() {
        return localeCode;
    }

    /**
     * Sets the locale code.
     * 
     * @param localeCode
     *            the new locale code
     */
    public void setLocaleCode(final String localeCode) {
        this.localeCode = localeCode;
    }

    /**
     * Gets the site nom.
     * 
     * @return the site nom
     */
    public String getSiteNom() {
        return siteNom;
    }

    /**
     * Sets the site nom.
     * 
     * @param siteNom
     *            the new site nom
     */
    public void setSiteNom(final String siteNom) {
        this.siteNom = siteNom;
    }

    /**
     * Gets the site code.
     * 
     * @return the site code
     */
    public String getSiteCode() {
        return siteCode;
    }

    /**
     * Sets the site code.
     * 
     * @param siteCode
     *            the new site code
     */
    public void setSiteCode(final String siteCode) {
        this.siteCode = siteCode;
    }

    /**
     * Gets the site dr.
     * 
     * @return the site dr
     */
    public String getSiteDr() {
        return siteDr;
    }

    /**
     * Sets the site dr.
     * 
     * @param siteDr
     *            the new site dr
     */
    public void setSiteDr(final String siteDr) {
        this.siteDr = siteDr;
    }

    /**
     * Gets the site zem.
     * 
     * @return the site zem
     */
    public String getSiteZem() {
        return siteZem;
    }

    /**
     * Sets the site zem.
     * 
     * @param siteZem
     *            the new site zem
     */
    public void setSiteZem(final String siteZem) {
        this.siteZem = siteZem;
    }

}
