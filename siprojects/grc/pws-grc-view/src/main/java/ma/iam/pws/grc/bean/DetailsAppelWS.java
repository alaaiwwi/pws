/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class DetailsAppelWS.
 */
public class DetailsAppelWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 8642249753156776002L;

    /** The date heure. */
    private String dateHeure;

    /** The date heure fin. */
 //   private String dateHeureFin;

    /** The zone. */
    private String zone;

    /** The login. */
 //   private String login;

    /** The service. */
    private String service;

    /** The numero appele. */
    private String numeroAppele;

    /** The duree. */
    private String duree;

    /** The volume valorise. */
    private String volumeValorise;

    /** The volume out. */
    private String volumeOut;

    /** The montant ht. */
    private String montantHT;

    /** The dans le forfait. */
    private String dansLeForfait;

    /**
     * Gets the date heure.
     * 
     * @return the date heure
     */
    public String getDateHeure() {
        return dateHeure;
    }

    /**
     * Sets the date heure.
     * 
     * @param dateHeure
     *            the new date heure
     */
    public void setDateHeure(final String dateHeure) {
        this.dateHeure = dateHeure;
    }

    /**
     * Gets the zone.
     * 
     * @return the zone
     */
    public String getZone() {
        return zone;
    }

    /**
     * Sets the zone.
     * 
     * @param zone
     *            the new zone
     */
    public void setZone(final String zone) {
        this.zone = zone;
    }

    /**
     * Gets the service.
     * 
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * Sets the service.
     * 
     * @param service
     *            the new service
     */
    public void setService(final String service) {
        this.service = service;
    }

    /**
     * Gets the numero appele.
     * 
     * @return the numero appele
     */
    public String getNumeroAppele() {
        return numeroAppele;
    }

    /**
     * Sets the numero appele.
     * 
     * @param numeroAppele
     *            the new numero appele
     */
    public void setNumeroAppele(final String numeroAppele) {
        this.numeroAppele = numeroAppele;
    }

    /**
     * Gets the duree.
     * 
     * @return the duree
     */
    public String getDuree() {
        return duree;
    }

    /**
     * Sets the duree.
     * 
     * @param duree
     *            the new duree
     */
    public void setDuree(final String duree) {
        this.duree = duree;
    }

    /**
     * Gets the volume valorise.
     * 
     * @return the volume valorise
     */
    public String getVolumeValorise() {
        return volumeValorise;
    }

    /**
     * Sets the volume valorise.
     * 
     * @param volumeValorise
     *            the new volume valorise
     */
    public void setVolumeValorise(final String volumeValorise) {
        this.volumeValorise = volumeValorise;
    }

    /**
     * Gets the montant ht.
     * 
     * @return the montant ht
     */
    public String getMontantHT() {
        return montantHT;
    }

    /**
     * Sets the montant HT.
     * 
     * @param montantHT
     *            the new montant HT
     */
    public void setMontantHT(final String montantHT) {
        this.montantHT = montantHT;
    }

    /**
     * Gets the dans le forfait.
     * 
     * @return the dans le forfait
     */
    public String getDansLeForfait() {
        return dansLeForfait;
    }

    /**
     * Sets the dans le forfait.
     * 
     * @param dansLeForfait
     *            the new dans le forfait
     */
    public void setDansLeForfait(final String dansLeForfait) {
        this.dansLeForfait = dansLeForfait;
    }

    /**
     * Gets the date heure fin.
     * 
     * @return the date heure fin
     */
//    public String getDateHeureFin() {
//        return dateHeureFin;
//    }
//
//    /**
//     * Sets the date heure fin.
//     * 
//     * @param dateHeureFin
//     *            the new date heure fin
//     */
//    public void setDateHeureFin(final String dateHeureFin) {
//        this.dateHeureFin = dateHeureFin;
//    }
//
//    /**
//     * Gets the login.
//     * 
//     * @return the login
//     */
//    public String getLogin() {
//        return login;
//    }
//
//    /**
//     * Sets the login.
//     * 
//     * @param login
//     *            the new login
//     */
//    public void setLogin(final String login) {
//        this.login = login;
//    }

    /**
     * Gets the volume out.
     * 
     * @return the volume out
     */
    public String getVolumeOut() {
        return volumeOut;
    }

    /**
     * Sets the volume out.
     * 
     * @param volumeOut
     *            the new volume out
     */
    public void setVolumeOut(final String volumeOut) {
        this.volumeOut = volumeOut;
    }

}
