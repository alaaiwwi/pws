/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class HistoMicroCelluleWS.
 */
public class HistoMicroCelluleWS extends GrcWS {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8830660598870885789L;

    /** The action date. */
    private String actionDate;

    /** The user. */
    private String user;

    /** The entity. */
    private String entity;

    /** The action desc. */
    private String actionDesc;

    /** The dn affected. */
    private String dnAffected;

    /**
     * Gets the action date.
     * 
     * @return the action date
     */
    public String getActionDate() {
        return actionDate;
    }

    /**
     * Sets the action date.
     * 
     * @param actionDate
     *            the new action date
     */
    public void setActionDate(final String actionDate) {
        this.actionDate = actionDate;
    }

    /**
     * Gets the user.
     * 
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the user.
     * 
     * @param user
     *            the new user
     */
    public void setUser(final String user) {
        this.user = user;
    }

    /**
     * Gets the entity.
     * 
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the entity.
     * 
     * @param entity
     *            the new entity
     */
    public void setEntity(final String entity) {
        this.entity = entity;
    }

    /**
     * Gets the action desc.
     * 
     * @return the action desc
     */
    public String getActionDesc() {
        return actionDesc;
    }

    /**
     * Sets the action desc.
     * 
     * @param actionDesc
     *            the new action desc
     */
    public void setActionDesc(final String actionDesc) {
        this.actionDesc = actionDesc;
    }

    /**
     * Gets the dn affected.
     * 
     * @return the dn affected
     */
    public String getDnAffected() {
        return dnAffected;
    }

    /**
     * Sets the dn affected.
     * 
     * @param dnAffected
     *            the new dn affected
     */
    public void setDnAffected(final String dnAffected) {
        this.dnAffected = dnAffected;
    }

}
