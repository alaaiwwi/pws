/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.fidelio;

import java.util.List;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.FidelioAccountWS;
import ma.iam.pws.grc.bean.FidelioActionHistWS;
import ma.iam.pws.grc.bean.FidelioOrderDetailWS;
import ma.iam.pws.grc.bean.FidelioOrderHistWS;

/**
 * The Interface IFidelioDAO.
 */
public interface IFidelioDAO {

	/*
	 * f.tatbi FC 5864 add paratmeter customer_id
	 */
	/**
	 * Retourne le compte fidélio du client é partir du code client
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return the fidelio account ws
	 */
	FidelioAccountWS findFidelioAccount(final String customer_id,
			final String custCode);

	/*
	 * f.tatbi FC 5864 add parameter customer_id
	 */

	/**
	 * Cherche l'historique des actions fidélio
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 */
	List<FidelioActionHistWS> findFidelioActionHist(final String customer_id,
			final String custCode);

	/*
	 * f.tatbi FC 5864 add parameter customer_id
	 */
	/**
	 * Cherche l'historique des orders fidélio
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 */
	List<FidelioOrderHistWS> findFidelioOrderHist(final String customer_id,
			final String custCode);

	/**
	 * Retourne le détail d'un ordre fidélio donné en entrée.
	 * 
	 * @param orderId
	 *            the order id
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 */
	List<FidelioOrderDetailWS> findFidelioOrderDetail(final String orderId);

	/**
	 * Sets the qualite fidelio pour le mobile.
	 * 
	 * @param listCustAcctWS
	 *            the list cust acct ws
	 * @param product
	 *            the product
	 */
	void setQualiteFidelio(List<CustAcctWS> listCustAcctWS);

}
