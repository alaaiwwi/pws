/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class ContractHistoryWS.
 */
public class ContractHistoryWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    /** The number. */
    private String number;

    /** The statut. */
   // private String statut;

    /** The date debut. */
    private String dateDebut;

    /** The date modif. */
    private String dateModif;

    /** The cree par. */
    private String creePar;
    
    
    private String reasonStatus;
    private String contractStatus;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ContractHistWs:");
        sb.append(number);
       
        if (dateDebut != null) {
            sb.append(",dateDebut=" + dateDebut);
        }
        if (dateModif != null) {
            sb.append(",dateModif=" + dateModif);
        }
        if (creePar != null) {
            sb.append(",creePar=" + creePar);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets the number.
     * 
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number.
     * 
     * @param number
     *            the new number
     */
    public void setNumber(final String number) {
        this.number = number;
    }

    /**
     * Gets the statut.
     * 
     * @return the statut
     */
//    public String getStatut() {
//        return statut;
//    }

    /**
     * Sets the statut.
     * 
     * @param statut
     *            the new statut
     */
//    public void setStatut(final String statut) {
//        this.statut = statut;
//    }

    /**
     * Gets la date de début.
     * 
     * @return la date de début
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets la date de début.
     * 
     * @param dateDebut
     *            the new date debut
     */
    public void setDateDebut(final String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets the date modif.
     * 
     * @return the date modif
     */
    public String getDateModif() {
        return dateModif;
    }

    /**
     * Sets the date modif.
     * 
     * @param dateModif
     *            the new date modif
     */
    public void setDateModif(final String dateModif) {
        this.dateModif = dateModif;
    }

    /**
     * Gets the cree par.
     * 
     * @return the cree par
     */
    public String getCreePar() {
        return creePar;
    }

    /**
     * Sets the cree par.
     * 
     * @param creePar
     *            the new cree par
     */
    public void setCreePar(final String creePar) {
        this.creePar = creePar;
    }



	public String getReasonStatus() {
		return reasonStatus;
	}

	public void setReasonStatus(String reasonStatus) {
		this.reasonStatus = reasonStatus;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

}
