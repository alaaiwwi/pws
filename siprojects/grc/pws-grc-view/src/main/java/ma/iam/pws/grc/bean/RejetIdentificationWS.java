/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class ContractHistoryWS.
 */
public class RejetIdentificationWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    /** The order id. */
    private String orderId;

    /** The co id. */
    private String coId;

    /** The insert date. */
    private String insertDate;

    /** The order origin. */
    private String orderOrigin;

    /** The action type. */
    private String actionType;

    /** The msisdn. */
    private String msisdn;

    /** The imsi. */
    private String imsi;

    /** The action details. */
    private String actionDetails;

    /** The lu date. */
    private String luDate;

    /** The status. */
    private String status;

    /** The response. */
    private String response;

    /** The type id. */
    private String typeId;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        sb = new StringBuilder();
        sb.append("[RejetIdentificationWS:");
        if (orderId != null) {
            sb.append(",orderId=").append(orderId);
        }
        if (coId != null) {
            sb.append(",coId=").append(coId);
        }
        if (insertDate != null) {
            sb.append(",insertDate=").append(insertDate);
        }
        if (orderOrigin != null) {
            sb.append(",orderOrigin=").append(orderOrigin);
        }
        if (actionType != null) {
            sb.append(",actionType=").append(actionType);
        }
        if (msisdn != null) {
            sb.append(",msisdn=").append(msisdn);
        }
        if (imsi != null) {
            sb.append(",imsi=").append(imsi);
        }
        if (actionDetails != null) {
            sb.append(",actionDetails=").append(actionDetails);
        }
        if (luDate != null) {
            sb.append(",luDate=").append(luDate);
        }
        if (status != null) {
            sb.append(",status=").append(status);
        }
        if (response != null) {
            sb.append(",response=").append(response);
        }
        if (typeId != null) sb.append(",typeId=").append(typeId);
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets the order id.
     * 
     * @return the order id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the order id.
     * 
     * @param orderId
     *            the new order id
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets the co id.
     * 
     * @return the co id
     */
    public String getCoId() {
        return coId;
    }

    /**
     * Sets the co id.
     * 
     * @param coId
     *            the new co id
     */
    public void setCoId(final String coId) {
        this.coId = coId;
    }

    /**
     * Gets the insert date.
     * 
     * @return the insert date
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * Sets the insert date.
     * 
     * @param insertDate
     *            the new insert date
     */
    public void setInsertDate(final String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * Gets the order origin.
     * 
     * @return the order origin
     */
    public String getOrderOrigin() {
        return orderOrigin;
    }

    /**
     * Sets the order origin.
     * 
     * @param orderOrigin
     *            the new order origin
     */
    public void setOrderOrigin(final String orderOrigin) {
        this.orderOrigin = orderOrigin;
    }

    /**
     * Gets the action type.
     * 
     * @return the action type
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the action type.
     * 
     * @param actionType
     *            the new action type
     */
    public void setActionType(final String actionType) {
        this.actionType = actionType;
    }

    /**
     * Gets the msisdn.
     * 
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the msisdn.
     * 
     * @param msisdn
     *            the new msisdn
     */
    public void setMsisdn(final String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * Gets the imsi.
     * 
     * @return the imsi
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the imsi.
     * 
     * @param imsi
     *            the new imsi
     */
    public void setImsi(final String imsi) {
        this.imsi = imsi;
    }

    /**
     * Gets the action details.
     * 
     * @return the action details
     */
    public String getActionDetails() {
        return actionDetails;
    }

    /**
     * Sets the action details.
     * 
     * @param actionDetails
     *            the new action details
     */
    public void setActionDetails(final String actionDetails) {
        this.actionDetails = actionDetails;
    }

    /**
     * Gets the lu date.
     * 
     * @return the lu date
     */
    public String getLuDate() {
        return luDate;
    }

    /**
     * Sets the lu date.
     * 
     * @param luDate
     *            the new lu date
     */
    public void setLuDate(final String luDate) {
        this.luDate = luDate;
    }

    /**
     * Gets the status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Gets the response.
     * 
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * Sets the response.
     * 
     * @param response
     *            the new response
     */
    public void setResponse(final String response) {
        this.response = response;
    }

    /**
     * Sets the type id.
     * 
     * @param typeId
     *            the new type id
     */
    public void setTypeId(final String typeId) {
        this.typeId = typeId;
    }

    /**
     * Gets the type id.
     * 
     * @return the type id
     */
    public String getTypeId() {
        return typeId;
    }

}
