/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.custacct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.ContractWS;
import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.CustFindContractWS;
import ma.iam.pws.grc.bean.CustSearchWS;
import ma.iam.pws.grc.bean.SearchCustomerWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

// TODO: Auto-generated Javadoc
/**
 * bh The Class CustAcctDAOMobileImpl.
 */
@Repository
public class CustAcctDAOMobileImpl extends BaseDAOMobile implements
		ICustAcctDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CustAcctDAOMobileImpl.class);
	private static final String GET_CUSTOMER_BY_CUSTCODE = "select customer_id from customer_all where ";
	private static final String GET_CUSTOMERS = CustAcctDAOFixeImpl.class
			.getName() + ".GET_CUSTOMERS";
	/* recupe sql de GET_INFO_CONTRACT */
	private static final String GET_INFO_CONTRACT = CustAcctDAOMobileImpl.class
			.getName() + ".GET_INFO_CONTRACT";

	/* recupe sql de GET_INFO_CUSTOMER */
	private static final String GET_INFO_CUSTOMER = CustAcctDAOMobileImpl.class
			.getName() + ".GET_INFO_CUSTOMER";
	/** La constante GETLISTECONTRATS. */
	public static final String GETLISTECONTRATS_MOBILE = CustAcctDAOMobileImpl.class
			.getName() + ".GETLISTECONTRATS";

	/** La constante GETCUSTOMER. */
	public static final String GETCUSTOMER = CustAcctDAOMobileImpl.class
			.getName() + ".GETCUSTOMER";

	/** La constante GETCLIENT. */
	public static final String GETCLIENT = CustAcctDAOMobileImpl.class
			.getName() + ".GETCLIENT";

	/** La constante GET_COID_BY_SIM. */
	public static final String GET_COID_BY_SIM = CustAcctDAOMobileImpl.class
			.getName() + ".GET_COID_BY_SIM";

	/** The Constant GET_ALLND_BY_CUSTOMER_ID. */
	public static final String GET_ALLND_BY_CUSTOMER_ID = CustAcctDAOMobileImpl.class
			.getName() + ".GET_ALLND_BY_CUSTOMER_ID";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findContractsForCustomer
	 * (fr.capgemini.iam.grc.bean.CustAcctWS)
	 */

	public List<CustAcctWS> findContractsForCustomer(
			final CustAcctWS searchCustAcctBean, final Boolean allowVIP,
			final int product) throws GrcWsException {
		LOG.debug(
				"--> findContractsForCustomer allowVIP {} searchCustAcctBean {}",
				allowVIP, searchCustAcctBean);
		final List<CustAcctWS> listCustAcct;
		String sql = CustomSQLUtil.get(GETLISTECONTRATS_MOBILE);
		final List<String> tags = getTagsFromSearchBean(searchCustAcctBean,
				allowVIP, DN_DN_NUM, CU_CUSTOMER_ID);
		String appendSql = "";

		List<ContractWS> listCustomerWs = null;
		// Si le champ Num série est renseigné
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCarteSim())) {
			listCustomerWs = findCustomerBySim(searchCustAcctBean.getCarteSim());
			appendSql = appendSql.concat(" AND CA.CO_ID in (");
			if ((listCustomerWs != null) && !listCustomerWs.isEmpty()) {
				for (final ContractWS customerWs : listCustomerWs) {
					appendSql = appendSql.concat(
							String.valueOf(customerWs.getContractIdBscs()))
							.concat(",");
				}
			}
			appendSql = appendSql.concat(" 0 )");
		}

		if (!GRCStringUtil.isNullOrEmpty(appendSql)) {
			tags.add("LISTCUSTOMER;" + appendSql);
		}
		try {
			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}

		final SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
				searchCustAcctBean);
		final CustomBeanPropertyRowMapper<CustAcctWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		// Cas particulier: Le client est crée sur BSCS AIX Mobile.
		// Le numéro/code client remonte un Customer mais pas de contrat
		// associé.
		// On doit refaire la recherche pour remonter les clients qui n'ont pas
		// de contrat
		if (listCustAcct.isEmpty()) {
			if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean
					.getCustAcctCode())) {
				listCustAcct.addAll(findCustomerByCustcode(
						searchCustAcctBean.getCustAcctCode(), product));
			} else if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean
					.getCustomerId())) {
				listCustAcct.addAll(findCustomerByCustomerId(
						searchCustAcctBean.getCustomerId(), product));
			}
		}

		LOG.debug("<-- findContractsForCustomer");
		return listCustAcct;
	}

	/**
	 * Recherche la liste de contrats mobile é partir d'un numéro de carte SIM
	 * donné
	 * 
	 * @param sim
	 *            the sim
	 * @return la liste des résultats
	 */
	private List<ContractWS> findCustomerBySim(final String sim) {
		List<ContractWS> listContractWS = null;
		final String sql = CustomSQLUtil.get(GET_COID_BY_SIM);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("sim", sim);
		final CustomBeanPropertyRowMapper<ContractWS> contractRowMapper = new CustomBeanPropertyRowMapper<ContractWS>(
				ContractWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContractWS = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, contractRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		return listContractWS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findCustomerByCustomerId
	 * (java.lang.String)
	 */

	public List<CustAcctWS> findCustomerByCustomerId(final String customerId,
			final int product) throws GrcWsException {
		LOG.debug("--> findCustomerByCustomerId customerId {}", customerId);
		List<CustAcctWS> listCustAcct;
		String sql = CustomSQLUtil.get(GETCUSTOMER);
		final List<String> tags = new ArrayList<String>();
		try {
			tags.add("CUSTOMER_ID");
			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				GRCWSConstantes.CUSTOMER_ID, customerId);

		final CustomBeanPropertyRowMapper<CustAcctWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listCustAcct == null) || listCustAcct.isEmpty()) {
			sql = CustomSQLUtil.get(GETCLIENT);
			try {
				sql = GRCStringUtil.formatQuery(sql, tags);
			} catch (final Exception e) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(),
						e);
			}
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
					namedParameters, custAcctRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		LOG.debug("<-- findCustomerByCustomerId");
		return listCustAcct;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findCustomerByCustcode
	 * (int)
	 */

	public List<CustAcctWS> findCustomerByCustcode(final String custocode,
			final int product) throws GrcWsException {
		LOG.debug("--> findCustomerByCustcode custocode {}", custocode);
		List<CustAcctWS> listCustAcct;
		String sql = CustomSQLUtil.get(GETCUSTOMER);
		final List<String> tags = new ArrayList<String>();
		try {
			tags.add("CUSTCODE");
			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"custcode", custocode);

		final CustomBeanPropertyRowMapper<CustAcctWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(
				CustAcctWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listCustAcct == null) || listCustAcct.isEmpty()) {
			sql = CustomSQLUtil.get(GETCLIENT);
			try {
				sql = GRCStringUtil.formatQuery(sql, tags);
			} catch (final Exception e) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(),
						e);
			}
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			listCustAcct = super.getNamedParameterJdbcTemplate().query(sql,
					namedParameters, custAcctRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}
		LOG.debug("<-- findCustomerByCustcode");
		return listCustAcct;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#getInfoSupplementaires
	 * (java.util.List)
	 */

	public void getInfoSupplementaires(final List<CustAcctWS> listCustAcctWS,
			final int product) {
		LOG.debug("--> getInfoSupplementaires");

		for (final CustAcctWS customerWs : listCustAcctWS) {
			if ((customerWs != null) && (customerWs.getContract() != null)
					&& (customerWs.getContract().getContractIdBscs() != null)) {
				setListND(customerWs);
			}

			if ((customerWs != null) && (customerWs.getChildren() != null)
					&& !customerWs.getChildren().isEmpty()) {
				getInfoSupplementaires(customerWs.getChildren(), product);
			}
		}
		LOG.debug("<-- getInfoSupplementaires");
	}

	/**
	 * Renseigne l'attribut listND avec la liste de tous les ND du client en
	 * entrée dans le cas du mobile
	 * 
	 * @param customerWs
	 *            the new list nd
	 */
	private void setListND(final CustAcctWS customerWs) {
		final String sql = CustomSQLUtil.get(GET_ALLND_BY_CUSTOMER_ID);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("customerId", customerWs.getCustomerId());
		try {
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			final List<String> listNd = super.getNamedParameterJdbcTemplate()
					.queryForList(sql, namedParameters, String.class);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, listNd);

			if (listNd != null) {
				customerWs.getContract().setListNd(listNd);
			}
		} catch (final EmptyResultDataAccessException e) {
			// Aucune ligne trouvée, on passe au client suivant
		}
	}

	public static final String DN_DN_NUM = "DN.DN_NUM";
	public static final String DN_DN_NUM_DATA = "V.LINE_NUMBER";

	public static final String CU_CUSTOMER_ID = "DN.DN_NUM";
	public static final String CU_CUSTOMER_ID_DATA = "A.ORGANIZATION_LEGACY_ID";

	/**
	 * Get tags from search bean.
	 * 
	 * @param searchCustAcctBean
	 *            the search cust acct bean
	 * @param allowVIP
	 *            the allow vip
	 * @return the tags from search bean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	protected List<String> getTagsFromSearchBean(
			final CustAcctWS searchCustAcctBean, final Boolean allowVIP,
			final String ndCol, final String customerIdCol)
			throws GrcWsException {

		// ndCol pour le fixe, internet, et mobile : DN.DN_NUM
		// ndCol pour la data : V.LINE_NUMBER

		// customerIdCol pour le fixe, internet et mobile : OR CU.CUSTOMER_ID
		// customerIdCol pour la data : A.ORGANIZATION_LEGACY_ID

		final List<String> tags = new ArrayList<String>(0);

		// Cas particulier de la recherche par contact, listNd contient le ND du
		// contact puis la liste des customerId
		// Cette liste sera transformée en :
		// AND (DN.DN_NUM = '212661357891' or CU.CUSTOMER_ID = 123456 or
		// CU.CUSTOMER_ID = 123578 )
		if ((searchCustAcctBean.getContract().getListNd() != null)
				&& (searchCustAcctBean.getContract().getListNd().size() > 0)) {
			final List<String> listValues = searchCustAcctBean.getContract()
					.getListNd();
			String tagValue = "AND (" + ndCol + " = '" + listValues.get(0)
					+ "' ";
			for (int i = 1; i < listValues.size(); i++) {
				tagValue = tagValue.concat(" OR  " + customerIdCol + " = "
						+ listValues.get(i));
			}
			tagValue = tagValue.concat(" ) ");
			tags.add("RECHERCHE_CONTACT;" + tagValue);
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract()
				.getNd())) {
			tags.add("ND");
		}
		if (!GRCStringUtil
				.isNullOrEmpty(searchCustAcctBean.getIdentityNumber())) {
			tags.add("PI");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCustAcctCode())) {
			tags.add("CODE");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getLastName())) {
			tags.add("NOM");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getFirstName())) {
			tags.add("PRENOM");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCompanyName())) {
			tags.add("RAISON");
		}
		// Il s'agit du Ncli
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCustomerId())) {
			if (!WSValidator.validateNumber(searchCustAcctBean.getCustomerId())) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
						"Customer_id incorrect :  "
								+ searchCustAcctBean.getCustomerId());
			}
			tags.add("CUSTACCTID");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract()
				.getContractIdBscs())) {
			/*
			 * if (!WSValidator.validateNumber(searchCustAcctBean.getContract().
			 * getContractIdBscs())) { throw new
			 * GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
			 * "ContractIdBscs incorrect :  " +
			 * searchCustAcctBean.getContract().getContractIdBscs()); }
			 */
			tags.add("COID");
		}
		if ((searchCustAcctBean.getCustAcctCategory() != null)
				&& (searchCustAcctBean.getCustAcctCategory().getCategoryCd() != null)) {
			tags.add("CATEGORY");
		}
		if ((searchCustAcctBean.getContract() != null)
				&& (searchCustAcctBean.getContract().getStatus() != null)) {
			tags.add("CONTRACT_STATUS");
		}
		if (searchCustAcctBean.getStatus() != null) {
			tags.add("STATUS");
		}
		if (!allowVIP) {
			tags.add("NOVIP");
		}
		// Commande
		if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCommande())) {
			if (!WSValidator.validateNumber(searchCustAcctBean.getCommande())) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
						"Commande incorrecte :  "
								+ searchCustAcctBean.getCommande());
			}
			tags.add("COMMANDE");
		}
		tags.add("HINT2;/*+ first_rows*/");
		return tags;
	}

	public List<CustSearchWS> searchCustomers(SearchCustomerWS searchCustomerWS) {

		List<CustSearchWS> listCustomers = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> searchCustomers: " + searchCustomerWS.toString());
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CUSTOMER));

		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCo_id())) {
			sqlBuffer.append(" and ca.CO_ID = " + searchCustomerWS.getCo_id());
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNd())) {
			sqlBuffer.append(" and dn.DN_NUM= '" + searchCustomerWS.getNd()
					+ "'");
		}

		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNom())) {
			sqlBuffer.append(" and cca.CCLNAME='" + searchCustomerWS.getNom()
					+ "'");
		}

		if (!GRCStringUtil
				.isNullOrEmpty(searchCustomerWS.getNumPieceIdentite())) {
			sqlBuffer.append(" and cca.PASSPORTNO= '"
					+ searchCustomerWS.getNumPieceIdentite() + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getPrenom())) {
			sqlBuffer.append(" and cca.CCFNAME='"
					+ searchCustomerWS.getPrenom() + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getVille())) {
			sqlBuffer.append(" and cca.CCCITY='" + searchCustomerWS.getVille()
					+ "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCodeCategorie())) {
			sqlBuffer.append(" and pga.PRGCODE= "
					+ searchCustomerWS.getCodeCategorie());
		}

		sqlBuffer.append(" and rownum <= 200");
		final CustomBeanPropertyRowMapper<CustSearchWS> rowMapper = new CustomBeanPropertyRowMapper<CustSearchWS>(
				CustSearchWS.class);
		listCustomers = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- searchCustomers: " + listCustomers);

		return listCustomers;

	}

	public List<CustSearchWS> searchAgenceCustomers(List<Long> customerIds) {
		throw new NotImplementedException(
				"fonction non implementé utlisation pour le fixe");
	}

	public List<CustFindContractWS> findContract(String custcode,
			String customerId, String nd, String coId) {
		List<CustFindContractWS> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CONTRACT));

		if (!GRCStringUtil.isNullOrEmpty(custcode)) {
			sqlBuffer.append(" and CU.CUSTCODE = '" + custcode + "'");
		}
		if (!GRCStringUtil.isNullOrEmpty(coId)) {
			sqlBuffer.append(" and ca.CO_ID= " + coId);
		}

		if (!GRCStringUtil.isNullOrEmpty(customerId)) {
			sqlBuffer.append(" and ca.CUSTOMER_ID=" + customerId);
		}

		if (!GRCStringUtil.isNullOrEmpty(nd)) {
			sqlBuffer.append(" and dn.DN_NUM= '" + nd + "'");
		}

		final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
				CustFindContractWS.class);
		listContracts = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public List<CustFindContractWS> findChildContract(
			List<CustFindContractWS> list) {
		List<CustFindContractWS> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CONTRACT));

		if (list != null && !list.isEmpty()) {

			sqlBuffer.append(" and cu.CUSTOMER_ID_HIGH in (");
			for (int i = 0; i < list.size(); i++) {
				sqlBuffer.append(list.get(i).getCustomerId());
				if (i != list.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
					CustFindContractWS.class);
			listContracts = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		}

		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public List<Map<String, Object>> findChildCustomer(List<String> list) {
		List<Map<String, Object>> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_CUSTOMERS));

		if (list != null && !list.isEmpty()) {

			sqlBuffer.append("  CUSTOMER_ID_HIGH in (");
			for (int i = 0; i < list.size(); i++) {
				sqlBuffer.append(list.get(i));
				if (i != list.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			listContracts = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().queryForList(sqlBuffer.toString());
		}

		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findChildCustomer: " + listContracts);

		return listContracts;
	}

	public List<CustFindContractWS> findContract(List<String> listCustomer,
			String nd, String coId) {
		List<CustFindContractWS> listContracts = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findContract: ");
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GET_INFO_CONTRACT));

		if (!GRCStringUtil.isNullOrEmpty(coId)) {
			sqlBuffer.append(" and ca.CO_ID= " + coId);
		}

		if (!GRCStringUtil.isNullOrEmpty(nd)) {
			sqlBuffer.append(" and dn.DN_NUM= '" + nd + "'");
		}

		if (listCustomer != null && !listCustomer.isEmpty()) {

			sqlBuffer.append(" and cu.CUSTOMER_ID in (");
			for (int i = 0; i < listCustomer.size(); i++) {
				sqlBuffer.append(listCustomer.get(i));
				if (i != listCustomer.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");

			final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
					CustFindContractWS.class);
			listContracts = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
		}
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findContract: " + listContracts);

		return listContracts;
	}

	public String getCustomerByCustCode(String custcode) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> getCustomerByCustCode: custCode=" + custcode);

		final StringBuilder sqlBuffer = new StringBuilder(
				GET_CUSTOMER_BY_CUSTCODE);
		sqlBuffer.append(" custcode='" + custcode + "'");
		Object result = null;
		try {
			 result = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations()
					.queryForObject(sqlBuffer.toString(), Object.class);
		} catch (EmptyResultDataAccessException e) {
			LOG.error("<-- getCustomerByCustCode" + e.getMessage());
		}
		LOG.debug("<-- getCustomerByCustCode");
		return String.valueOf(result);
	}
}
