/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

/**
 * The Class RatePlanWS.
 */
public class RatePlanWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 4368053546713044207L;

    /** The rateplan name. */
    private String rateplanName;

    /** The rateplan id. */
    private Integer rateplanId;

    /** The rateplan desc. */
    private String rateplanDesc;

    public Boolean isEmpty() {
        return ((rateplanId == null) || Integer.valueOf(0).equals(rateplanId)) && GRCStringUtil.isNullOrEmpty(rateplanName)
                && GRCStringUtil.isNullOrEmpty(rateplanDesc);
    }

    /**
     * Gets the rateplan desc.
     * 
     * @return the rateplan desc
     */
    public String getRateplanDesc() {
        return rateplanDesc;
    }

    /**
     * Gets the rateplan id.
     * 
     * @return the rateplan id
     */
    public Integer getRateplanId() {
        return rateplanId;
    }

    /**
     * Gets the rateplan name.
     * 
     * @return the rateplan name
     */
    public String getRateplanName() {
        return rateplanName;
    }

    /**
     * Sets the rateplan desc.
     * 
     * @param rateplanDesc
     *            the new rateplan desc
     */
    public void setRateplanDesc(final String rateplanDesc) {
        this.rateplanDesc = rateplanDesc;
    }

    /**
     * Sets the rateplan id.
     * 
     * @param rateplanId
     *            the new rateplan id
     */
    public void setRateplanId(final Integer rateplanId) {
        this.rateplanId = rateplanId;
    }

    /**
     * Sets the rateplan name.
     * 
     * @param rateplanName
     *            the new rateplan name
     */
    public void setRateplanName(final String rateplanName) {
        this.rateplanName = rateplanName;
    }

}
