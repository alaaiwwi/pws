/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CustStatsWS.
 */
public class StatsClientWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -5122014237454763743L;

    /** The contract statuses. */
    private List<ContractStatusWS> contractStatuses;

    /** The chiffre affaire. */
    private double chiffreAffaire;

    /** The chiffre affaire courant. */
    private double chiffreAffaireCourant;

    /** The factures retard. */
    private int facturesRetard;

    /** The anciennete contrat. */
    private int ancienneteContrat;

    /** The anciennete contrat. */
    private int scoring;

    /**
     * Gets the contract statuses.
     * 
     * @return the contract statuses
     */
    public List<ContractStatusWS> getContractStatuses() {
        return contractStatuses;
    }

    /**
     * Sets the contract statuses.
     * 
     * @param contractStatuses
     *            the new contract statuses
     */
    public void setContractStatuses(final List<ContractStatusWS> contractStatuses) {
        this.contractStatuses = contractStatuses;
    }

    /**
     * Gets the chiffre affaire.
     * 
     * @return the chiffre affaire
     */
    public double getChiffreAffaire() {
        return chiffreAffaire;
    }

    /**
     * Sets the chiffre affaire.
     * 
     * @param chiffreAffaire
     *            the new chiffre affaire
     */
    public void setChiffreAffaire(final double chiffreAffaire) {
        this.chiffreAffaire = chiffreAffaire;
    }

    /**
     * Gets the chiffre affaire courant.
     * 
     * @return the chiffre affaire courant
     */
    public double getChiffreAffaireCourant() {
        return chiffreAffaireCourant;
    }

    /**
     * Sets the chiffre affaire courant.
     * 
     * @param chiffreAffaireCourant
     *            the new chiffre affaire courant
     */
    public void setChiffreAffaireCourant(final double chiffreAffaireCourant) {
        this.chiffreAffaireCourant = chiffreAffaireCourant;
    }

    /**
     * Gets the factures retard.
     * 
     * @return the factures retard
     */
    public int getFacturesRetard() {
        return facturesRetard;
    }

    /**
     * Sets the factures retard.
     * 
     * @param facturesRetard
     *            the new factures retard
     */
    public void setFacturesRetard(final int facturesRetard) {
        this.facturesRetard = facturesRetard;
    }

    /**
     * Gets the anciennete contrat.
     * 
     * @return the anciennete contrat
     */
    public int getAncienneteContrat() {
        return ancienneteContrat;
    }

    /**
     * Sets the anciennete contrat.
     * 
     * @param ancienneteContrat
     *            the new anciennete contrat
     */
    public void setAncienneteContrat(final int ancienneteContrat) {
        this.ancienneteContrat = ancienneteContrat;
    }

    /**
     * Gets the scoring.
     * 
     * @return the scoring
     */
    public int getScoring() {
        return scoring;
    }

    /**
     * Sets the scoring.
     * 
     * @param scoring
     *            the new scoring
     */
    public void setScoring(final int scoring) {
        this.scoring = scoring;
    }

}
