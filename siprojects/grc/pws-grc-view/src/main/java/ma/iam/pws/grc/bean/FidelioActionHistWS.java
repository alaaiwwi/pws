/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class FidelioActionHistWS.
 */
public class FidelioActionHistWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7206288778865083462L;

    /** The description. */
    private String description;

    /** The action date. */
    private String actionDate;

    /** The action comment. */
    private String actionComment;

    /** The description action. */
    private String descriptionAction;

    /** The agence code. */
    private String agenceCode;

    /** The added point. */
    private String addedPoint;

    /** The account id. */
    private String accountId;

    /** The solde en cours. */
    private String soldeEnCours;

    /** The montant compensation. */
    private String montantCompensation;

    /** The date application. */
    private String dateApplication;

    /** The solde converti. */
    private String soldeConverti;

    /** The converti. */
    private String converti;

    /** The solde purge. */
    private String soldePurge;

    /** The date purge. */
    private String datePurge;

    /** The num facture. */
    private String numFacture;

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the action date.
     * 
     * @return the action date
     */
    public String getActionDate() {
        return actionDate;
    }

    /**
     * Sets the action date.
     * 
     * @param actionDate
     *            the new action date
     */
    public void setActionDate(final String actionDate) {
        this.actionDate = actionDate;
    }

    /**
     * Gets the action comment.
     * 
     * @return the action comment
     */
    public String getActionComment() {
        return actionComment;
    }

    /**
     * Sets the action comment.
     * 
     * @param actionComment
     *            the new action comment
     */
    public void setActionComment(final String actionComment) {
        this.actionComment = actionComment;
    }

    /**
     * Gets the description action.
     * 
     * @return the description action
     */
    public String getDescriptionAction() {
        return descriptionAction;
    }

    /**
     * Sets the description action.
     * 
     * @param descriptionAction
     *            the new description action
     */
    public void setDescriptionAction(final String descriptionAction) {
        this.descriptionAction = descriptionAction;
    }

    /**
     * Gets the agence code.
     * 
     * @return the agence code
     */
    public String getAgenceCode() {
        return agenceCode;
    }

    /**
     * Sets the agence code.
     * 
     * @param agenceCode
     *            the new agence code
     */
    public void setAgenceCode(final String agenceCode) {
        this.agenceCode = agenceCode;
    }

    /**
     * Gets the added point.
     * 
     * @return the added point
     */
    public String getAddedPoint() {
        return addedPoint;
    }

    /**
     * Sets the added point.
     * 
     * @param addedPoint
     *            the new added point
     */
    public void setAddedPoint(final String addedPoint) {
        this.addedPoint = addedPoint;
    }

    /**
     * Gets the account id.
     * 
     * @return the account id
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the account id.
     * 
     * @param accountId
     *            the new account id
     */
    public void setAccountId(final String accountId) {
        this.accountId = accountId;
    }

    /**
     * Gets the solde en cours.
     * 
     * @return the solde en cours
     */
    public String getSoldeEnCours() {
        return soldeEnCours;
    }

    /**
     * Sets the solde en cours.
     * 
     * @param soldeEnCours
     *            the new solde en cours
     */
    public void setSoldeEnCours(final String soldeEnCours) {
        this.soldeEnCours = soldeEnCours;
    }

    /**
     * Gets the montant compensation.
     * 
     * @return the montant compensation
     */
    public String getMontantCompensation() {
        return montantCompensation;
    }

    /**
     * Sets the montant compensation.
     * 
     * @param montantCompensation
     *            the new montant compensation
     */
    public void setMontantCompensation(final String montantCompensation) {
        this.montantCompensation = montantCompensation;
    }

    /**
     * Gets the date application.
     * 
     * @return the date application
     */
    public String getDateApplication() {
        return dateApplication;
    }

    /**
     * Sets the date application.
     * 
     * @param dateApplication
     *            the new date application
     */
    public void setDateApplication(final String dateApplication) {
        this.dateApplication = dateApplication;
    }

    /**
     * Gets the solde converti.
     * 
     * @return the solde converti
     */
    public String getSoldeConverti() {
        return soldeConverti;
    }

    /**
     * Sets the solde converti.
     * 
     * @param soldeConverti
     *            the new solde converti
     */
    public void setSoldeConverti(final String soldeConverti) {
        this.soldeConverti = soldeConverti;
    }

    /**
     * Gets the converti.
     * 
     * @return the converti
     */
    public String getConverti() {
        return converti;
    }

    /**
     * Sets the converti.
     * 
     * @param converti
     *            the new converti
     */
    public void setConverti(final String converti) {
        this.converti = converti;
    }

    /**
     * Gets the solde purge.
     * 
     * @return the solde purge
     */
    public String getSoldePurge() {
        return soldePurge;
    }

    /**
     * Sets the solde purge.
     * 
     * @param soldePurge
     *            the new solde purge
     */
    public void setSoldePurge(final String soldePurge) {
        this.soldePurge = soldePurge;
    }

    /**
     * Gets the date purge.
     * 
     * @return the date purge
     */
    public String getDatePurge() {
        return datePurge;
    }

    /**
     * Sets the date purge.
     * 
     * @param datePurge
     *            the new date purge
     */
    public void setDatePurge(final String datePurge) {
        this.datePurge = datePurge;
    }

    /**
     * Gets the num facture.
     * 
     * @return the num facture
     */
    public String getNumFacture() {
        return numFacture;
    }

    /**
     * Sets the num facture.
     * 
     * @param numFacture
     *            the new num facture
     */
    public void setNumFacture(final String numFacture) {
        this.numFacture = numFacture;
    }

}
