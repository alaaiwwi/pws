/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseDAOMobile.
 */
@Repository
public class BaseDAOMobile {

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private NamedParameterJdbcTemplate namedParameterJdbcTemplateFidelio;

	private DataSource mobileBSCSDataSource;

	/**
	 * Sets the mobile bscs data source.
	 * 
	 * @param mobileBSCSDataSource
	 *            the new mobile bscs data source
	 */
	@Autowired
	@Qualifier("mobileBSCSDataSource")
	public void setMobileBSCSDataSource(final DataSource mobileBSCSDataSource) {
		this.setNamedParameterJdbcTemplate(new NamedParameterJdbcTemplate(
				mobileBSCSDataSource));

		this.mobileBSCSDataSource = mobileBSCSDataSource;
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplate(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	@Autowired
	public void setMobileFidelioDataSource(
			final DataSource mobileFIDELIODataSource) {
		setNamedParameterJdbcTemplateFidelio(new NamedParameterJdbcTemplate(
				mobileFIDELIODataSource));
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplateFidelio(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplateFidelio) {
		this.namedParameterJdbcTemplateFidelio = namedParameterJdbcTemplateFidelio;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplateFidelio() {
		return namedParameterJdbcTemplateFidelio;
	}

	public DataSource getMobileBSCSDataSource() {
		return mobileBSCSDataSource;
	}

}
