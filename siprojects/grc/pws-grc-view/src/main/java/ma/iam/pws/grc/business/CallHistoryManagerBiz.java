package ma.iam.pws.grc.business;

import java.util.List;

import ma.iam.pws.grc.bean.CustomDetailWS;
import ma.iam.pws.grc.bean.DetailsAppelWS;
import ma.iam.pws.grc.bean.IntervalWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import ma.iam.pws.grc.dao.detailsappel.IDetailsAppelDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

// TODO: Auto-generated Javadoc
/**
 * The Class CallHistoryManagerBiz.
 */
@Component(value = "callHistoryManagerBiz")
public class CallHistoryManagerBiz extends CommonManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CallHistoryManagerBiz.class);

	/**
	 * Cherche les details des appels du client/contrat.
	 * 
	 * @param custAcctId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param dateDebut
	 *            la date de début
	 * @param dateFin
	 *            la date de fin
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<DetailsAppelWS> findDetailsAppel(final String customerId,
			final String contractIdBscs, final String dateDebut,
			final String dateFin, final int product) throws GrcWsException {
		LOG.info("-> findDetailsAppel: customerId {} contractIdBscs {} "
				+ dateDebut + " " + dateFin, customerId, contractIdBscs);
		String dateDebutParam = dateDebut;
		if ((dateDebutParam != null) && (dateDebutParam.length() > 0)) {
			dateDebutParam = dateDebutParam.substring(0, 10);
		}
		String dateFinParam = dateFin;
		if ((dateFinParam != null) && (dateFinParam.length() > 0)) {
			dateFinParam = dateFinParam.substring(0, 10);
		}

		final IntervalWS interval = new IntervalWS();
		interval.setStartDate(dateDebut);
		interval.setEndDate(dateFin);
		if (!WSValidator.validateNumber(contractIdBscs))
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétre incorrecte contractIdBscs:" + contractIdBscs);

		if (!WSValidator.validateNumber(customerId))
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétre incorrecte customerId:" + customerId);
		if (!WSValidator.validateDate(dateDebut))
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétre incorrecte dateDebut:" + dateDebut);
		if (!WSValidator.validateDate(dateFin))
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétre incorrecte dateFin:" + dateFin);
		if (!WSValidator.validateInterval(interval)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétres incorrectes date Debut date Fin:");
		}
		final IDetailsAppelDAO iDetailsAppelDAO = this
				.getDetailsAppelDAOForProduct(product);
		CustomDetailWS customDetailWS = iDetailsAppelDAO
				.getContract(contractIdBscs);
		if (customDetailWS == null)
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétre inexistant contractIdBscs:" + contractIdBscs);
	
		if (!customDetailWS.getCustomer().equals(customerId))
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"pas de liaison entre les paramétres ");

		final List<DetailsAppelWS> listDetailsAppel = iDetailsAppelDAO
				.getDetailsAppels(customerId, contractIdBscs, dateDebutParam,
						dateFinParam);
		LOG.info("<- findDetailsAppel");
		return listDetailsAppel;
	}
}
