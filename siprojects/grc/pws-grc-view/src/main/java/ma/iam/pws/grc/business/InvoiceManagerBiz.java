/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebParam;

import ma.iam.pws.grc.bean.IntervalWS;
import ma.iam.pws.grc.bean.InvoiceDetailWS;
import ma.iam.pws.grc.bean.InvoiceWS;
import ma.iam.pws.grc.bean.InvoiceWSFindInvoice;
import ma.iam.pws.grc.bean.PaymentDetailWS;
import ma.iam.pws.grc.bean.PaymentWS;
import ma.iam.pws.grc.bean.SearchInvoiceBean;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import ma.iam.pws.grc.dao.invoice.IInvoiceDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

// TODO: Auto-generated Javadoc
/**
 * The Class InvoiceManagerBiz.
 */
@Component(value = "invoiceManagerBiz")
public class InvoiceManagerBiz extends CommonManagerBiz {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(InvoiceManagerBiz.class);

	/**
	 * Cherche les invoices details depuis la base GRC.
	 * 
	 * @param idFacture
	 *            l'id de la facture
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceDetailWS> findInvoicesDetails(final String idFacture,
			final int product) throws GrcWsException {
		LOG.info("-> findInvoicesDetails: idFacture {}", idFacture);
		if (GRCStringUtil.isNullOrEmpty(idFacture)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche facture incorrectes: idFacture "
							+ idFacture);
		}

		List<InvoiceDetailWS> listInvoiceDetails = null;
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		listInvoiceDetails = iInvoiceDAO.findInvoicesDetails(idFacture);
		LOG.info("<- findInvoicesDetails");
		return listInvoiceDetails;

	}

	/**
	 * Cherche les factures pour le client en entrée.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceWSFindInvoice> findInvoicesForCustomer(
			final String customerId, final IntervalWS interval,
			final int product) throws GrcWsException {
		LOG.info("-> findInvoicesForCustomer: customerId {} interval {}",
				customerId, interval);
		if (!WSValidator.validateNumber(customerId)
				|| !WSValidator.validateInterval(interval)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche facture incorrectes: customerId "
							+ customerId + " interval " + interval);
		}

		final SearchInvoiceBean searchInvoiceBean = new SearchInvoiceBean(
				customerId, interval);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final List<InvoiceWSFindInvoice> listInvoices = iInvoiceDAO
				.findInvoicesForCustomer(searchInvoiceBean);
		LOG.info("<- findInvoicesForCustomer");
		return listInvoices;

	}

	/*
	 * f.tatbi FC 5864
	 */
	/**
	 * Cherche les factures pour le client en entrée.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @param product
	 *            le produit
	 * @param statut
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceWSFindInvoice> findInvoicesForCustomer(
			final String customerId, final IntervalWS interval,
			final int product, final String statut) throws GrcWsException {
		LOG.info("-> findInvoicesForCustomer: customerId {} interval {}",
				customerId, interval);
		if (!WSValidator.validateNumber(customerId)
				|| !WSValidator.validateInterval(interval)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche facture incorrectes: customerId "
							+ customerId + " interval " + interval);
		}

		final SearchInvoiceBean searchInvoiceBean = new SearchInvoiceBean(
				customerId, interval, statut);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final List<InvoiceWSFindInvoice> listInvoices = iInvoiceDAO
				.findInvoicesForCustomer(searchInvoiceBean);
		LOG.info("<- findInvoicesForCustomer");
		return listInvoices;

	}

	/**
	 * Cherche les détails de paiement d'une facture donnée
	 * 
	 * @param invoiceExternalId
	 *            the invoice external id
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PaymentDetailWS> findPaymentsDetails(
			final String invoiceExternalId, final int product)
			throws GrcWsException {
		LOG.info("-> findPaymentsDetails: numTransaction {}", invoiceExternalId);

		if (GRCStringUtil.isNullOrEmpty(invoiceExternalId)) {

			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche imcomplétes: numTransaction ");
		}

		List<PaymentDetailWS> listPayementsDetail = null;
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		listPayementsDetail = iInvoiceDAO
				.findPaymentsDetails(invoiceExternalId);
		LOG.info("<- findPaymentsDetails");
		return listPayementsDetail;

	}

	/**
	 * Cherche les payments d'un client.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PaymentWS> findPayments(final String customerId,
			final IntervalWS interval, final int product) throws GrcWsException {
		LOG.info("-> findPayments: customerId {} interval {}", customerId,
				interval);
		List<PaymentWS> listPayementsDetail = null;
		final SearchInvoiceBean searchInvoiceBean = new SearchInvoiceBean(
				customerId, interval);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		listPayementsDetail = iInvoiceDAO.findPayments(searchInvoiceBean);
		LOG.info("<- findPayments");
		return listPayementsDetail;

	}

	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceWS> findDetailedInvoicesForCustomer(
			final String customerId, IntervalWS interval, final int product)
			throws GrcWsException {
		LOG.info(
				"-> findDetailedInvoicesForCustomer: customerId {} factureDate {}",
				customerId, interval);

		final SearchInvoiceBean searchInvoiceBean = new SearchInvoiceBean(
				customerId, interval);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		List<InvoiceWS> listPayementsDetail = iInvoiceDAO
				.findDetailedInvoicesForCustomer(searchInvoiceBean);

		LOG.info("<- findDetailedInvoicesForCustomer");
		return listPayementsDetail;

	}

	/*
	 * f.tatbi FC 5864 add param id facture
	 */
	/**
	 * Cherche les payments for invoice.
	 * 
	 * @param refFacture
	 *            the ref facture
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PaymentWS> findPaymentsForInvoice(final String refFacture,
			final Integer idFacture, final int product) throws GrcWsException {
		LOG.info("-> findPaymentsForInvoice: numFacture {}", refFacture);

		if ((idFacture == null || idFacture == 0)
				&& GRCStringUtil.isNullOrEmpty(refFacture))
			throw new GrcWsException(
					ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche imcomplétes: refFacture ou idFacture doit étre renseigné ");
		List<PaymentWS> listPayementsDetail = null;
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		listPayementsDetail = iInvoiceDAO.findPaymentsForInvoice(refFacture,
				idFacture);
		if (LOG.isInfoEnabled())
			LOG.info("<- findPaymentsForInvoice" + listPayementsDetail);
		return listPayementsDetail;

	}

	/**
	 * Cherche les montant global ouvert.
	 * 
	 * @param custAcctCode
	 *            le code client (CUSTCODE dans BSCS)
	 * @param product
	 *            le produit
	 * @return the double
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public Double findMontantGlobalOuvert(final String custAcctCode,
			final int product) throws GrcWsException {
		LOG.info("-> findMontantGlobalOuvert: custAcctCode {}", custAcctCode);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final Double montant = iInvoiceDAO
				.findMontantGlobalOuvert(custAcctCode);
		LOG.info("<- findMontantGlobalOuvert montant {}", montant);
		return montant;

	}

	/**
	 * Retourne la liste des factures é partir d'une liste de numéros de
	 * factures
	 * 
	 * @param findInvoicesByListInvoiceExternalId
	 *            the find invoices by list invoice external id
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceWS> findInvoicesByListInvoiceExternalId(
			final List<Integer> listInvoiceExternalId, final int product)
			throws GrcWsException {
		LOG.info(
				"-> findInvoicesByListInvoiceExternalId: listInvoiceExternalId {}",
				listInvoiceExternalId);
		List<InvoiceWS> listInvoices = new ArrayList<InvoiceWS>();

		if (!listInvoiceExternalId.isEmpty()) {
			final IInvoiceDAO iInvoiceDAO = this
					.getInvoiceDAOForProduct(product);

			listInvoices = iInvoiceDAO
					.findInvoicesByListInvoiceExternalId(listInvoiceExternalId);
		}
		LOG.info("<- findInvoicesByListInvoiceExternalId");
		return listInvoices;

	}

	/*
	 * f.tatbi FC 5864 modifier findInvoicesByListInvoiceExternalId
	 */

	/**
	 * Retourne la liste des factures é partir d'une liste de numéros de
	 * factures
	 * 
	 * @param findInvoicesByListInvoiceExternalId
	 *            the find invoices by list invoice external id
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceWSFindInvoice> findInvoiceById(
			final List<String> refFacture, final List<Integer> idFacture,
			final int product, final String statut) throws GrcWsException {
		LOG.info("-> findInvoiceById : refFacture {}", refFacture);
		List<InvoiceWSFindInvoice> listInvoices = new ArrayList<InvoiceWSFindInvoice>();

		if ((refFacture == null && idFacture == null)
				|| (refFacture != null && idFacture != null
						&& refFacture.isEmpty() && idFacture.isEmpty())) {

			throw new GrcWsException(
					ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche imcomplétes: refFacture ou idFacture doit étre renseigné ");
		} else {
			final IInvoiceDAO iInvoiceDAO = this
					.getInvoiceDAOForProduct(product);

			listInvoices = iInvoiceDAO.findInvoiceById(refFacture, idFacture,
					statut);

		}
		LOG.info("<- findInvoiceById");
		return listInvoices;

	}

	/**
	 * Retourne la liste des factures é partir d'une liste des références.
	 * 
	 * @param findInvoicesByListInvoiceExternalId
	 *            * Retourne la liste des factures é partir d'une liste des
	 *            références.
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<InvoiceWS> findInvoicesByListReference(
			final List<String> listReferenceFacture, final int product)
			throws GrcWsException {
		LOG.info("-> findInvoicesByListReference: listReferenceFacture {}",
				listReferenceFacture);
		List<InvoiceWS> listInvoices = new ArrayList<InvoiceWS>();

		if (!listReferenceFacture.isEmpty()) {
			final IInvoiceDAO iInvoiceDAO = this
					.getInvoiceDAOForProduct(product);

			listInvoices = iInvoiceDAO
					.findInvoicesByListReference(listReferenceFacture);
		}
		LOG.info("<- findInvoicesByListInvoiceExternalId");
		return listInvoices;

	}

	/**
	 * Protect invoices.
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @param product
	 *            the product
	 * @return the boolean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, timeout = GRCProperties.TIMEOUT)
	public Boolean protectInvoices(final List<Integer> listInvoiceExternalId,
			final int product) throws GrcWsException {
		if (LOG.isInfoEnabled())
			LOG.info("-> protectInvoices: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final Boolean result = iInvoiceDAO
				.protectInvoices(listInvoiceExternalId);
		LOG.info("<- protectInvoices {}", result);
		return result;
	}

	/**
	 * Un protect invoices.
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @param product
	 *            the product
	 * @return the boolean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, timeout = GRCProperties.TIMEOUT)
	public Boolean unProtectInvoices(final List<Integer> listInvoiceExternalId,
			final int product) throws GrcWsException {
		if (LOG.isInfoEnabled())
			LOG.info("-> unProtectInvoices: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final Boolean result = iInvoiceDAO
				.unProtectInvoices(listInvoiceExternalId);
		LOG.info("<- unProtectInvoices {}", result);
		return result;
	}

	/*
	 * f.tatbi FC 5864 : surcharge de la fct protectInvoices
	 */
	/**
	 * Protect invoices.
	 * 
	 * @param idFacture
	 *            the of facture
	 * @param product
	 *            the product
	 * @param user
	 *            the user
	 * @param application
	 *            the application
	 * @return the boolean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, timeout = GRCProperties.TIMEOUT)
	public Boolean protectInvoices(final List<Integer> idFacture, String user,
			String application, final int product) throws GrcWsException {
		if (LOG.isInfoEnabled())
			LOG.info("-> protectInvoices: listInvoiceExternalId=" + idFacture);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final Boolean result = iInvoiceDAO.protectInvoices(idFacture);
		if (idFacture != null && !idFacture.isEmpty() && result) {
			for (Integer facture : idFacture) {
				String msg = "activation de la protection de la facture : "
						+ facture + "  par l'application : " + application
						+ " et l'utilisateur :" + user;
				iInvoiceDAO.insertHistory(facture, user, application, msg);
				if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
					boolean bln = iInvoiceDAO.insertUnPaid(new Integer(0),
							facture);

					if (LOG.isInfoEnabled())
						LOG.info("-> protectInvoices: insertUnPaid facture ="
								+ idFacture
								+ ((bln) ? "inserted" : "not inserted"));
				}
			}

		}
		LOG.info("<- protectInvoices {}", result);
		return result;
	}

	/**
	 * Un protect invoices.
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @param product
	 *            the product
	 * @param user
	 *            the user
	 * @param application
	 *            the application
	 * @return the boolean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, timeout = GRCProperties.TIMEOUT)
	public Boolean unProtectInvoices(final List<Integer> idFacture,
			String user, String application, final int product)
			throws GrcWsException {
		if (LOG.isInfoEnabled())
			LOG.info("-> unProtectInvoices: listInvoiceExternalId=" + idFacture);
		final IInvoiceDAO iInvoiceDAO = this.getInvoiceDAOForProduct(product);

		final Boolean result = iInvoiceDAO.unProtectInvoices(idFacture);
		LOG.info("<- unProtectInvoices {}", result);
		if (idFacture != null && !idFacture.isEmpty() && result) {
			for (Integer facture : idFacture) {
				String msg = "désactivation de la protéction de la facture : "
						+ facture + "  par l'application : " + application
						+ " et l'utilisateur :" + user;
				iInvoiceDAO.insertHistory(facture, user, application, msg);
			}
		}
		return result;
	}

}
