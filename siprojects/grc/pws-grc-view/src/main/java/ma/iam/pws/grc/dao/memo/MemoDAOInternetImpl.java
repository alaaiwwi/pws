/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.memo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.MemoAddInfoWS;
import ma.iam.pws.grc.bean.MemoWS;
import ma.iam.pws.grc.bean.MemoWiamWS;
import ma.iam.pws.grc.bean.TerminationReasonWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class MemoDAOMobileImpl.
 */
@Repository
public class MemoDAOInternetImpl extends BaseDAOFixe implements IMemoDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(MemoDAOInternetImpl.class);

    /** La constante GETLISTEMEMO. */
    public static final String GETLISTEMEMO = MemoDAOInternetImpl.class.getName() + ".GETLISTEMEMO";

    /** The Constant GET_DN_NUM_FROM_CO_ID. */
    public static final String GET_DN_NUM_FROM_CO_ID = MemoDAOInternetImpl.class.getName() + ".GET_DN_NUM_FROM_CO_ID";

    /** The Constant INSERTMEMOWIAM. */
    public static final String INSERTMEMOWIAM = MemoDAOInternetImpl.class.getName() + ".INSERTMEMOWIAM";

    public static final String INSERTMEMOADDINFOVALUE = MemoDAOInternetImpl.class.getName() + ".INSERTMEMOADDINFOVALUE";

    /** The Constant INSERTMEMOADDINFO. */
    public static final String INSERTMEMOADDINFO = MemoDAOInternetImpl.class.getName() + ".INSERTMEMOADDINFO";

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.memo.IMemoDAO#findMemos(java.lang.String,
     * java.lang.String)
     */
    
    public List<MemoWS> findMemos(final String customerId, final String contractIdBscs) {
        LOG.debug("--> findMemos contractIdBsc {} customerId {}", contractIdBscs, customerId);
        List<MemoWS> listMemos = null;

        final String sql = CustomSQLUtil.get(GETLISTEMEMO);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        final String dnNum = getNdFromContractIdBscs(contractIdBscs);

        if (dnNum != null) {
            namedParameters.put("dn_num", dnNum);

            final CustomBeanPropertyRowMapper<MemoWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<MemoWS>(MemoWS.class);

            LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
            listMemos = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, invoiceRowMapper);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
        }
        LOG.debug("<-- findMemos");
        return listMemos;
    }

    /**
     * Retourne la liste des ND d'un contrat donné
     * 
     * @param contractIdBscs
     *            the contract id bscs
     * @return the nd from contract id bscs
     */
    private String getNdFromContractIdBscs(final String contractIdBscs) {
        LOG.debug("--> getNdFromContractIdBscs {}", contractIdBscs);
        final String sqlNd = CustomSQLUtil.get(GET_DN_NUM_FROM_CO_ID);
        String nd;
        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("contractIdBscs", contractIdBscs);
        try {
            LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + " contractIdBscs {}", sqlNd, contractIdBscs);
            nd = super.getNamedParameterJdbcTemplateNeto().queryForObject(sqlNd, namedParameters, String.class);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, nd);
        } catch (final EmptyResultDataAccessException e) {
            nd = null;
        }
        LOG.debug("<-- getNdFromContractIdBscs liste des ND={}", nd);
        return nd;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.memo.IMemoDAO#insertMemoWiam(ma.iam
     * .grc.bean.MemoWiamWS)
     */
    
    public Boolean insertMemoWiam(final MemoWiamWS memoWiamWS, final List<MemoAddInfoWS> listMemoAddInfos) {
        LOG.debug("--> insertMemoWiam memoWiamWS {}", memoWiamWS);
        final String sql = CustomSQLUtil.get(INSERTMEMOWIAM);

        final String sqlMemoAddInfoValue = CustomSQLUtil.get(INSERTMEMOADDINFOVALUE);
        final String sqlMemoAddInfo = CustomSQLUtil.get(INSERTMEMOADDINFO);

        final SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(memoWiamWS);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        super.getNamedParameterJdbcTemplateNeto().update(sql, namedParameters);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        SqlParameterSource namedParametersMemoAddInfoWS = null;

        if (null != listMemoAddInfos) {
            for (final MemoAddInfoWS memoAddInfoWS : listMemoAddInfos) {
                namedParametersMemoAddInfoWS = new BeanPropertySqlParameterSource(memoAddInfoWS);
                // Insertion dans MEMO_ADD_INFO_VALUE
                LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlMemoAddInfoValue);
                super.getNamedParameterJdbcTemplateNeto().update(sqlMemoAddInfoValue, namedParametersMemoAddInfoWS);
                LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

                // Insertion dans MEMO_ADD_INFO
                LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlMemoAddInfo);
                super.getNamedParameterJdbcTemplateNeto().update(sqlMemoAddInfo, namedParametersMemoAddInfoWS);
                LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
            }

        }

        LOG.debug("<-- insertMemoWiam - return true");
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.memo.IMemoDAO#getTerminationReasons()
     */
    
    public List<TerminationReasonWS> getTerminationReasons() {
        LOG.debug("--> insertMemoWiam getTerminationReasons ");
        throw new NotImplementedException("Service de remontée de la liste des motifs de résiliation non disponible pour le produit Internet");
    }

}
