/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.contrat;

import java.util.List;

import ma.iam.pws.grc.bean.ContractHistoryWS;
import ma.iam.pws.grc.bean.PeripheriqueHistoryWS;
import ma.iam.pws.grc.bean.PlanTarifaireHistoryWS;
import ma.iam.pws.grc.bean.PreidentificationWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

/**
 * The Class ContractDAOFixeImpl.
 */
@Repository
public class ContractDAOFixeImpl extends ContractDAOCommon implements
		IContractDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ContractDAOFixeImpl.class);

	private static final String GET_CREAT_CONTRACT = ContractDAOFixeImpl.class
			.getName() + ".GET_VALUE_STRING";

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findContractHistory
	 * (fr.capgemini.iam.grc.bean.CustAcctWS)
	 */

	public List<ContractHistoryWS> findContractHistory(
			final String contractIdBsc) {
		LOG.debug("--> findContractHistory contractIdBsc {}", contractIdBsc);
		final List<ContractHistoryWS> listContractHistory = super
				.findContractHistory(contractIdBsc);
		LOG.debug("<-- findContractHistory");
		return listContractHistory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.contrat.IContractDAO#findPeripheriqueHistory
	 * (java.lang.String)
	 */

	// public List<PeripheriqueHistoryWS> findPeripheriqueHistory(
	// final String contractIdBsc) {
	// LOG.debug("--> findPeripheriqueHistory contractIdBsc {}", contractIdBsc);
	// final List<PeripheriqueHistoryWS> listContractHistory = super
	// .findPeripheriqueHistory(contractIdBsc);
	// LOG.debug("<-->findPeripheriqueHistory contractIdBsc {}", contractIdBsc);
	// return listContractHistory;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.contrat.IContractDAO#findPlanTarifaireHistory
	 * (java.lang.String)
	 */

	public List<PlanTarifaireHistoryWS> findPlanTarifaireHistory(
			final String contractIdBsc) {
		LOG.debug("--> findPlanTarifaireHistory contractIdBsc {}",
				contractIdBsc);
		final List<PlanTarifaireHistoryWS> listContractHistory = super
				.findPlanTarifaireHistory(contractIdBsc);
		LOG.debug("<-- findPlanTarifaireHistory");
		return listContractHistory;
	}

	public List<PreidentificationWS> findDonneesPreidentification(
			String contractIdBsc) {
		return null;
	}

	public List<PreidentificationWS> findDonneesPreidentificationOM(
			String contractIdBsc) {
		return null;
	}

	public ContractHistoryWS getCreatContract(String contractIdBscs) {
		LOG.debug("--> getCreatContract contractIdBsc {}", contractIdBscs);
		final List<ContractHistoryWS> listContractHistory;

		final String sql = CustomSQLUtil.get(GET_CREAT_CONTRACT);

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"co_id", contractIdBscs);

		final CustomBeanPropertyRowMapper<ContractHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<ContractHistoryWS>(
				ContractHistoryWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContractHistory = super.getNamedParameterJdbcTemplateNeto().query(
				sql, namedParameters, custAcctRowMapper);
		LOG.debug("<-- getCreatContract");
		return (listContractHistory != null && !listContractHistory.isEmpty()) ? listContractHistory
				.get(0) : null;
	}

	public List<PeripheriqueHistoryWS> findPeripheriqueHistory(
			final String contractIdBsc) {
		LOG.debug("--> findPeripheriqueHistory contractIdBsc {}", contractIdBsc);
		final List<PeripheriqueHistoryWS> listContractHistory;
		final String sql = CustomSQLUtil.get(GET_PERIPHERIQUE_HISTORY);

		final SqlParameterSource namedParameters = new MapSqlParameterSource(
				"contractIdBsc", contractIdBsc);

		final CustomBeanPropertyRowMapper<PeripheriqueHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PeripheriqueHistoryWS>(
				PeripheriqueHistoryWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listContractHistory = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, custAcctRowMapper);
		LOG.debug("<-- findPeripheriqueHistory");
		return listContractHistory;
	}

}
