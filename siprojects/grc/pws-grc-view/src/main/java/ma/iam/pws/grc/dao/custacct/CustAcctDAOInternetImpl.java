/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.custacct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.ContractWS;
import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import ma.iam.pws.grc.dao.CommonDAOFixe;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CustAcctDAOInternetImpl.
 */
@Repository
public class CustAcctDAOInternetImpl extends CommonDAOFixe implements ICustAcctDAO {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(CustAcctDAOInternetImpl.class);

    /** The Constant GET_CONTRACTS_INTERNET. */
    public static final String GET_CONTRACTS_INTERNET = CustAcctDAOInternetImpl.class.getName() + ".GET_CONTRACTS_INTERNET";

    /** The Constant GET_COID_BY_INFO_NETO. */
    public static final String GET_COID_BY_INFO_NETO = CustAcctDAOInternetImpl.class.getName() + ".GET_COID_BY_INFO_NETO";
    public static final String GET_COID_BY_DNS = CustAcctDAOInternetImpl.class.getName() + ".GET_COID_BY_DNS";

    /** The Constant GET_COID_BY_INFO_BSCS. */
    public static final String GET_COID_BY_INFO_BSCS = CustAcctDAOInternetImpl.class.getName() + ".GET_COID_BY_INFO_BSCS";

    /** The Constant GET_INFO_BY_COID. */
    public static final String GET_INFO_BY_COID = CustAcctDAOInternetImpl.class.getName() + ".GET_INFO_BY_COID";

    /**
     * The Constant GET_COID_BY_CUSTOMERID .
     */
    public static final String GET_COID_BY_CUSTOMERID = CustAcctDAOInternetImpl.class.getName() + ".GET_COID_BY_CUSTOMERID";

    /** The Constant GET_CUSTOMERID_BY_COID. */
    public static final String GET_CUSTOMERID_BY_COID = CustAcctDAOInternetImpl.class.getName() + ".GET_CUSTOMERID_BY_COID";

    /** The Constant GET_COID_BY_CUSTCODE. */
    public static final String GET_COID_BY_CUSTCODE = CustAcctDAOInternetImpl.class.getName() + ".GET_COID_BY_CUSTCODE";

    public static final String GET_COID_BY_ND = CustAcctDAOInternetImpl.class.getName() + ".GET_COID_BY_ND";

    /** The Constant QUOTE. */
    public static final String QUOTE = "'";

    /** The Constant COMMA. */
    public static final String COMMA = ",";

    /**
     * Retourne la clause SQL correspondant aux customer_id trouvés é partir des
     * informations internet données en entrée<br>
     * .
     * 
     * @param info
     *            la valeur de l'informations (MAIL, IP, LOGIN)
     * @param code
     *            le code de l'informations :
     *            <ul>
     *            <li>
     * @return the appended sql with internet info
     * @throws GrcWsException
     *             the grc ws exception
     *             </ul>
     */
    private String getAppendedSQLWithInternetInfo(final String info, final String code) throws GrcWsException {
        String appendSql = "";
        if (!GRCStringUtil.isNullOrEmpty(info)) {
            final List<CustAcctWS> listCustomerWs = findCustomerByInfo(info, code);
            appendSql = appendSql.concat(" AND CA.CUSTOMER_ID in (");
            if (!listCustomerWs.isEmpty()) {
                for (final CustAcctWS customerWs : listCustomerWs) {
                    appendSql = appendSql.concat(String.valueOf(customerWs.getCustomerId())).concat(",");
                }
            }
            appendSql = appendSql.concat(" 0) ");
        }
        return appendSql;
    }

    /**
     * A partir des informations CustomerId, custAcctCode ou coId, recherche le
     * ND du client.
     * 
     * @param searchCustAcctBean
     *            the search cust acct bean
     * @throws GrcWsException
     *             the grc ws exception
     */
    private void findNdFromInternetInfos(final CustAcctWS searchCustAcctBean) throws GrcWsException {

        if (searchCustAcctBean.getContract() == null) {
            searchCustAcctBean.setContract(new ContractWS());
        }

        final String nd = searchCustAcctBean.getContract().getNd();
        Boolean continuer = Boolean.TRUE;


        if (continuer && !GRCStringUtil.isNullOrEmpty(nd)) {

            if (((nd.length() != GRCWSConstantes.PHONE_NUMBER_WITH_PREFIX_DIGITS) || !nd.startsWith(GRCWSConstantes.PHONE_NUMBER_PREFIX))) {
                /* il s'agit d'un ND bizarre */
                /* on récupére le contrat plutét .. */
                final String coId = findCoIdByNd(searchCustAcctBean.getContract().getNd());
                if ((coId != null) && (coId.length() > 0)) {
                    searchCustAcctBean.getContract().setContractIdBscs(coId);
                    searchCustAcctBean.getContract().setNd(null);
                } else {
                    continuer = false;
                }
            }
        }

    }

    /**
     * Find co id by cust acct code.
     * 
     * @param custAcctCode
     *            the cust acct code
     * @return the string
     */
    private String findCoIdByCustAcctCode(final String custAcctCode) {
        String value = null;
        try {
            final String sql = CustomSQLUtil.get(GET_COID_BY_CUSTCODE);
            final Map<String, String> namedParameters = new HashMap<String, String>();
            namedParameters.put("custAcctCode", custAcctCode);
            LOG.debug(GRCWSConstantes.EXEC_QUERY + " custAcctCode {}", sql, custAcctCode);
            value = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);

        } catch (final EmptyResultDataAccessException e) {
            value = null;
        }
        return value;
    }

    private String findCoIdByNd(final String nd) {
        String value = null;
        try {
            final String sql = CustomSQLUtil.get(GET_COID_BY_ND);
            final Map<String, String> namedParameters = new HashMap<String, String>();
            namedParameters.put("nd", nd);
            LOG.debug(GRCWSConstantes.EXEC_QUERY + " nd {}", sql, nd);
            value = super.getNamedParameterJdbcTemplateNeto().queryForObject(sql, namedParameters, String.class);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);

        } catch (final EmptyResultDataAccessException e) {
            value = null;
        }
        return value;
    }

    /**
     * Find co id by cust acct id.
     * 
     * @param customerId
     *            the customer id
     * @return the string
     */
    private String findCoIdByCustAcctId(final String customerId) {
        String value = null;
        try {
            final String sql = CustomSQLUtil.get(GET_COID_BY_CUSTOMERID);
            final Map<String, String> namedParameters = new HashMap<String, String>();
            namedParameters.put("customerId", customerId);
            LOG.debug(GRCWSConstantes.EXEC_QUERY + " customerId {}", sql, customerId);
            value = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);

        } catch (final EmptyResultDataAccessException e) {
            value = null;
        }
        return value;
    }
    
    private String findCustomerIdByCoId(final String coId) {
        String value = null;
        try {
            final String sql = CustomSQLUtil.get(GET_CUSTOMERID_BY_COID);
            final Map<String, String> namedParameters = new HashMap<String, String>();
            namedParameters.put("coId", coId);
            LOG.debug(GRCWSConstantes.EXEC_QUERY + " coId {}", sql, coId);
            value = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);

        } catch (final EmptyResultDataAccessException e) {
            value = null;
        }
        return value;
    }

    private String getAppendedSQL(final CustAcctWS searchCustAcctBean, final int product) throws GrcWsException {
        // Si customer_id, co_id, custcode sont renseignés (et qu'il
        // correspondent é des infos internet), il faudra récupérer le ND depuis
        // netonomy
        //
        // CCU

        String appendSql = "";

        appendSql = getAppendedSQLByCCU(searchCustAcctBean.getCcu(), product);

        appendSql = appendSql.concat(getAppendedSQLByCommande(searchCustAcctBean.getCommande(), product));

        // Si le champ login est renseigné:
        // alors on cherche tous les contracts fixe correspondants au
        // contrat internet recherché
        appendSql = appendSql.concat(getAppendedSQLWithInternetInfo(searchCustAcctBean.getContract().getLoginInternet(), GRCWSConstantes.LOGIN_NETO_CODE));

        // Si le champ mail est renseigné:
        // alors on cherche tous les contracts fixe correspondants au
        // contrat internet recherché

        appendSql = appendSql.concat(getAppendedSQLWithInternetInfo(searchCustAcctBean.getMail(), GRCWSConstantes.MAIL_CODES));

        // Si le champ IP est renseigné:
        // alors on cherche tous les contracts fixe correspondants au
        // contrat internet recherché
        appendSql = appendSql.concat(getAppendedSQLWithInternetInfo(searchCustAcctBean.getIp(), GRCWSConstantes.IP_NETO_CODE));

        if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCcu()) || !GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getCommande())
                || !GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract().getLoginInternet())
                || !GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getMail()) || !GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getIp())) {
            if (GRCStringUtil.isNullOrEmpty(appendSql)) {
                appendSql = "AND 1=2 ";
            }
        }

        return appendSql;
    }

    private boolean setContractIdByDns(final CustAcctWS searchCustAcctBean) throws GrcWsException {
        Boolean resultEmpty = false;
        // Si le champ DNS est renseigné:
        // alors on cherche tous les contracts fixe correspondants au
        // contrat internet recherché

        final List<String> listCustomerWs = getListContractIdNetoByInfo(searchCustAcctBean.getDns(), GRCWSConstantes.DNS_CODES);

        for (final String dns : getListDNSNeto(searchCustAcctBean.getDns())) {
            if ( ! listCustomerWs.contains(dns)) {
                listCustomerWs.add(dns);
            }
        }

        if ((listCustomerWs == null) || listCustomerWs.isEmpty()) {
            resultEmpty = Boolean.TRUE;
        } else {
            String coIdDns = "";
            if (listCustomerWs.size() > 1) {
                for(final String coid : listCustomerWs ) {
                    coIdDns = coIdDns.concat(coid).concat(",");
                }
                if (coIdDns.length() > 0) {
                    coIdDns = coIdDns.concat("0");
                }
            }
            else {
                coIdDns = listCustomerWs.get(0);
            }

            if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getContract().getContractIdBscs())
                    && !searchCustAcctBean.getContract().getContractIdBscs().equals(coIdDns)) {
                resultEmpty = Boolean.TRUE;
            } else {
                searchCustAcctBean.getContract().setContractIdBscs(coIdDns);
            }
        }
        return resultEmpty;
    }

    private List<String> getListDNSNeto(final String dns) {
        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("dns", dns);
        final String sqlNeto = CustomSQLUtil.get(GET_COID_BY_DNS);

        LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sqlNeto);
        final List<String> listContractIdNeto = super.getNamedParameterJdbcTemplateNeto().queryForList(sqlNeto, namedParameters, String.class);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, listContractIdNeto);

        return listContractIdNeto;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findContractsForCustomer
     * (fr.capgemini.iam.grc.bean.CustAcctWS)
     */
    
    public List<CustAcctWS> findContractsForCustomer(final CustAcctWS searchCustAcctBean, final Boolean allowVIP, final int product) throws GrcWsException {
        LOG.debug("--> findContractsForCustomer allowVIP {} searchCustAcctBean {}", allowVIP, searchCustAcctBean);
        List<CustAcctWS> listCustAcct = new ArrayList<CustAcctWS>();

        String appendSql = "";
        if ((product != ProduitTypeEnum.DATA.getIdTypeProduit())) {
            normaliserND(searchCustAcctBean);
            appendSql = getAppendedSQL(searchCustAcctBean, product);
            findNdFromInternetInfos(searchCustAcctBean);
        }


        // DNS
        if (!GRCStringUtil.isNullOrEmpty(searchCustAcctBean.getDns())) {
            final boolean resultEmpty = setContractIdByDns(searchCustAcctBean);
            if (resultEmpty) {
                return listCustAcct;
            }
        }


        listCustAcct = getListCustAcctWS(appendSql, searchCustAcctBean, allowVIP, product);

        if ((listCustAcct != null) && !listCustAcct.isEmpty() && (product != ProduitTypeEnum.DATA.getIdTypeProduit())) {
            final List<String> listCoId = new ArrayList<String>();
            boolean useCustomerHigh = true;
            final String nd = searchCustAcctBean.getContract().getNd();
            if ((nd != null) && (nd.length() > 0)) {
                listCoId.addAll(findContractFixeFromNd(nd));
                //on a le ND donc on est censé avoir le contrat internet sans passer par le customerIdHigh
                useCustomerHigh = false;
            }
            final String login = searchCustAcctBean.getContract().getLoginInternet();
            if ((login != null) && (login.length() > 0)) {
                listCoId.addAll(getListContractIdNetoByInfo(login, GRCWSConstantes.LOGIN_NETO_CODE));
                //on a le login donc on est censé avoir le contrat internet sans passer par le customerIdHigh
                useCustomerHigh = false;
            }

            final String listCustomerIdHigh = getListCustomerIdHigh(listCustAcct, listCoId);
            listCustAcct.addAll(findInternetCustomer(listCustomerIdHigh, allowVIP, useCustomerHigh));
        }

        addCustAcctByCommande(listCustAcct, searchCustAcctBean, product);

        LOG.debug("<-- findContractsForCustomer");
        return listCustAcct;
    }

    private String getListCustomerIdHigh(final List<CustAcctWS> listCustAcct, final List<String> listCoId) {
        String customerIdHigh = "(";
        int countMax = 0;
        for (final CustAcctWS custAcctWS : listCustAcct) {
            if (countMax++ > GRCWSConstantes.MAX_RETRIEVED_CUSTOMERS) {
                break;
            }
            final String parentCustomerId = String.valueOf(custAcctWS.getCustomerIdHigh());
            if (!customerIdHigh.contains(parentCustomerId)) {
                customerIdHigh = customerIdHigh.concat(parentCustomerId).concat(",");
            }
            if (!customerIdHigh.contains(custAcctWS.getCustomerId())) {
                customerIdHigh = customerIdHigh.concat(custAcctWS.getCustomerId()).concat(",");
            }
        }

        for (final String coId : listCoId) {
            final String customerId = findCustomerIdByCoId(coId);
            if (!customerIdHigh.contains(customerId)) {
                customerIdHigh = customerIdHigh.concat(customerId).concat(",");
            }
        }
        customerIdHigh = customerIdHigh.concat(" 0 ) ");
        return customerIdHigh;
    }

    /**
     * Find customer by info.
     * 
     * @param value
     *            the value
     * @param type
     *            the type
     * @return the list< cust acct w s>
     * @throws GrcWsException
     */
    private List<CustAcctWS> findCustomerByInfo(final String value, final String type) throws GrcWsException {
        final List<CustAcctWS> listCustAcctWSresult = new ArrayList<CustAcctWS>();

        final String sqlBscs = CustomSQLUtil.get(GET_COID_BY_INFO_BSCS);

        // namedParameters.put("type", type);

        final List<String> listContractIdNeto = getListContractIdNetoByInfo(value, type);

        final CustomBeanPropertyRowMapper<CustAcctWS> contractRowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(CustAcctWS.class);

        for (final String contractIdNeto : listContractIdNeto) {
            List<CustAcctWS> listCustAcctWS = null;
            final Map<String, String> tmpNamedParameters = new HashMap<String, String>();
            tmpNamedParameters.put("contractIdNeto", contractIdNeto);
            LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBscs);
            listCustAcctWS = super.getNamedParameterJdbcTemplate().query(sqlBscs, tmpNamedParameters, contractRowMapper);
            LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
            listCustAcctWSresult.addAll(listCustAcctWS);
        }

        return listCustAcctWSresult;
    }

    /**
     * Gets the list contract id neto by info.
     * 
     * @param value
     *            the value
     * @param type
     *            the type
     * @return the list contract id neto by info
     * @throws GrcWsException
     *             the grc ws exception
     */
    private List<String> getListContractIdNetoByInfo(final String value, final String type) throws GrcWsException {

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("value", value);
        String sqlNeto = CustomSQLUtil.get(GET_COID_BY_INFO_NETO);

        try {
            final List<String> tags = new ArrayList<String>();
            tags.add("SERVICE_CODE;" + type);
            sqlNeto = GRCStringUtil.formatQuery(sqlNeto, tags);
        } catch (final Exception e) {
            throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(), e);
        }

        LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sqlNeto);
        final List<String> listContractIdNeto = super.getNamedParameterJdbcTemplateNeto().queryForList(sqlNeto, namedParameters, String.class);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, listContractIdNeto);

        return listContractIdNeto;

    }

    /**
     * Find info by contract.
     * 
     * @param contractIdBscs
     *            the contract id bscs
     * @param type
     *            the type
     * @return the string
     * @throws GrcWsException
     */
    private String findInfoByContract(final String contractIdBscs, final String type) throws GrcWsException {
        String value = null;
        try {
            String sql = CustomSQLUtil.get(GET_INFO_BY_COID);
            final Map<String, String> namedParameters = new HashMap<String, String>();
            namedParameters.put("contractIdBscs", contractIdBscs);

            try {
                final List<String> tags = new ArrayList<String>();
                tags.add("SERVICE_CODE;" + type);
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(), e);
            }

            LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO + " type {}", sql, type);
            final List<String> values = super.getNamedParameterJdbcTemplateNeto().queryForList(sql, namedParameters, String.class);

            // try {
            // final List<String> tags = new ArrayList<String>();
            // tags.add("SERVICE_CODE;" + type);
            // sqlNeto = GRCStringUtil.formatQuery(sqlNeto, tags);
            // } catch (final Exception e) {
            // throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
            // e.getMessage(), e);
            // }

            if (values != null) {
                value = "";
                for (final String val : values) {
                    if (!GRCStringUtil.isNullOrEmpty(value)) {
                        value = value.concat("\n");
                    }
                    value = value.concat(val);
                }
            }

            LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, value);
        } catch (final EmptyResultDataAccessException e) {
            value = null;
        }
        return value;
    }

    /**
     * Find internet customer.
     * 
     * @param customerIdHigh
     *            the customer id high
     * @return the list< cust acct w s>
     * @throws GrcWsException
     */
    private List<CustAcctWS> findInternetCustomer(final String customerIdHigh, final boolean allowVIP, final boolean useCustomerHigh) throws GrcWsException {
        final StringBuilder sqlSB = new StringBuilder(CustomSQLUtil.get(GET_CONTRACTS_INTERNET));
        sqlSB.append(" AND ( ");
        if (useCustomerHigh) {
            sqlSB.append(" CU.CUSTOMER_ID_high in ").append(customerIdHigh);
            sqlSB.append(" OR  ");
        }
        sqlSB.append(" CU.CUSTOMER_ID in ").append(customerIdHigh);
        sqlSB.append(" ) ");
        sqlSB.append("order by CU.CSTYPE, CH.CH_STATUS");

        final List<String> tags = new ArrayList<String>();
        if (!allowVIP) {
            tags.add("NOVIP");
        }
        String sql;
        try {
            sql = GRCStringUtil.formatQuery(sqlSB.toString(), tags);
        } catch (final Exception e) {
            throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR, e.getMessage(), e);
        }

        final SqlParameterSource namedParameters = new MapSqlParameterSource();
        final CustomBeanPropertyRowMapper<CustAcctWS> rowMapper = new CustomBeanPropertyRowMapper<CustAcctWS>(CustAcctWS.class);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        final List<CustAcctWS> listCustAcctWS = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, rowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        for (final CustAcctWS custAcctWS : listCustAcctWS) {
            if (custAcctWS.getIsInternet() != 0) {
                final String contractIdBscs = String.valueOf(custAcctWS.getContract().getContractIdBscs());

                if ((custAcctWS.getContract().getLoginInternet() == null) || (custAcctWS.getContract().getLoginInternet().length() == 0)) {
                    final String login = findInfoByContract(contractIdBscs, GRCWSConstantes.LOGIN_NETO_CODE);
                    custAcctWS.getContract().setLoginInternet(login);
                }
                if ((custAcctWS.getMail() == null) || (custAcctWS.getMail().length() == 0)) {
                    custAcctWS.setMail(findInfoByContract(contractIdBscs, GRCWSConstantes.MAIL_CODES));
                }
                if ((custAcctWS.getIp() == null) || (custAcctWS.getIp().length() == 0)) {
                    custAcctWS.setIp(findInfoByContract(contractIdBscs, GRCWSConstantes.IP_NETO_CODE));
                }
                if ((custAcctWS.getContract().getNd() == null) || (custAcctWS.getContract().getNd().length() == 0)) {
                    String nd = findLineByContract(contractIdBscs);
                    if ((nd == null) || (nd.length() == 0) || ((nd != null) && (nd.length() > 0) && nd.equals(custAcctWS.getContract().getLoginInternet()))) {
                        //Soit le ND vide soit c'est un double play (ND == login)
                        //dans ce cas le ND est celui du contrat fixe
                        final String contractIdBscsFixe = findContractFixeFromInternet(contractIdBscs);
                        final String ndDoublePlay = findLineByContract(contractIdBscsFixe);
                        if ((ndDoublePlay != null) && (ndDoublePlay.length() > 0)) {
                            nd = ndDoublePlay;
                        }
                    }
                    custAcctWS.getContract().setNd(nd);

                    
                }
            }
        }

        return listCustAcctWS;
    }

}
