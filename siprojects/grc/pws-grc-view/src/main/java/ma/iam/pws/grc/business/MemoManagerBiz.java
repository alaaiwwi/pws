/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.List;

import ma.iam.pws.grc.bean.MemoAddInfoWS;
import ma.iam.pws.grc.bean.MemoWS;
import ma.iam.pws.grc.bean.MemoWiamWS;
import ma.iam.pws.grc.bean.TerminationReasonWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import ma.iam.pws.grc.dao.memo.IMemoDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class MemoManagerBiz.
 */
@Component(value = "memoManagerBiz")
public class MemoManagerBiz extends CommonManagerBiz {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(MemoManagerBiz.class);

    /**
     * Cherche les memos.
     * 
     * @param customerId
     *            l'identifiant client BSCS (CUSTOMER_ID)
     * @param contractIdBscs
     *            le contrat bscs du client (champ CO_ID)
     * @param product
     *            le produit
     * @return la liste des résultats
     * @throws GrcWsException
     *             l'exception GrcWsException
     */
    @Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
    public List<MemoWS> findMemos(final String customerId, final String contractIdBscs, final int product) throws GrcWsException {
        LOG.info("-> findMemos: contractIdBscs {} custAccId {}", contractIdBscs, customerId);
        if (!WSValidator.validateNumber(contractIdBscs) || !WSValidator.validateNumber(customerId)) {
            throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS, "Paramétres incorrectes : " + contractIdBscs + " customerId:" + customerId);
        }
        final IMemoDAO iMemoDAO = this.getMemoDAOForProduct(product);

        final List<MemoWS> listMemos = iMemoDAO.findMemos(customerId, contractIdBscs);
        LOG.info("<- findMemos");
        return listMemos;
    }

    /**
     * Insert memo wiam.
     * 
     * @param memoWiamWS
     *            the memo wiam ws
     * @param listMemoAddInfos
     *            the list memo add infos
     * @param product
     *            the product
     * @return the boolean
     * @throws GrcWsException
     *             the grc ws exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = GRCProperties.TIMEOUT)
    public Boolean insertMemoWiam(final MemoWiamWS memoWiamWS, final List<MemoAddInfoWS> listMemoAddInfos, final int product) throws GrcWsException {
        LOG.info("-> insertMemoWiam: memoWiamWS {}", memoWiamWS);
        final IMemoDAO iMemoDAO = this.getMemoDAOForProduct(product);

        final Boolean result = iMemoDAO.insertMemoWiam(memoWiamWS, listMemoAddInfos);
        LOG.info("<- insertMemoWiam {}", result);
        return result;
    }

    /**
     * Remonte les motifs de résiliation.
     * 
     * @param product
     *            the product
     * @return la liste des motifs de résiliation
     * @throws GrcWsException
     *             the grc ws exception
     */
    public List<TerminationReasonWS> getTerminationReasons(final int product) throws GrcWsException {
        LOG.info("-> getTerminationReasons");
        final IMemoDAO iMemoDAO = this.getMemoDAOForProduct(product);

        final List<TerminationReasonWS> listReasons = iMemoDAO.getTerminationReasons();
        LOG.info("<- getTerminationReasons");
        return listReasons;
    }

}
