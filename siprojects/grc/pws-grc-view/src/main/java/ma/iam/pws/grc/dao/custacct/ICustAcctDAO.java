/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.custacct;

import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.CustFindContractWS;
import ma.iam.pws.grc.bean.CustSearchWS;
import ma.iam.pws.grc.bean.SearchCustomerWS;
import ma.iam.pws.grc.exceptions.GrcWsException;

/**
 * The Interface ICustAcctDAO.
 */
public interface ICustAcctDAO {

    /**
     * Cherche les contracts pour le client donné en entrée.<br>
     * Le résultat est donné sous forme d'une liste de Clients Parents, chacun
     * contenant ses fils (si disponibles) d'une maniére récurrente<br>
     * La valeur allowVIP détermine si les clients d'une certaine catégorie
     * (VIP) sont remontés ou non dans le résultat de la recherche<br>
     * 
     * @param searchCustAcctBean
     *            le bean de recherche client
     * @return la liste des résultats @
     * @throws GrcWsException
     */
    List<CustAcctWS> findContractsForCustomer(CustAcctWS searchCustAcctBean, final Boolean allowVIP, final int product) throws GrcWsException;

    /**
     * Cherche les client par CUSTOMER_ID<br>
     * Les clients remontés n'ont pas de contrat associé
     * 
     * @param customerId
     *            l'identifiant client BSCS (CUSTOMER_ID)
     * @return la liste des résultats
     * @throws GrcWsException
     */
    List<CustAcctWS> findCustomerByCustomerId(final String customerId, final int product) throws GrcWsException;

    /**
     * Remonte la liste des clients par CUSTCODE<br>
     * Les clients remontés n'ont pas de contrat associé
     * 
     * @param custocode
     *            the custocode
     * @return the list
     * @throws GrcWsException
     *             the grc ws exception
     */
    List<CustAcctWS> findCustomerByCustcode(final String custocode, final int product) throws GrcWsException;

    /**
     * Récupére des informations supplémentaires sur les clients trouvés<br>
     * Cette fonction est récursive et parcours les "Children" des clients si
     * disponibles
     * 
     * @param listCustAcctWS
     *            the list cust acct ws
     * @return the info supplementaires
     */
    void getInfoSupplementaires(final List<CustAcctWS> listCustAcctWS, final int product);

    /* 
     * f.tatbi FC 5864 fct retourne une liste de client 
     */
	List<CustSearchWS> searchCustomers(SearchCustomerWS searchCustomerWS);

    /* 
     * f.tatbi FC 5864 fct retourne une liste de client avec le codes agence
     */
	List<CustSearchWS> searchAgenceCustomers(List<Long> customerIds);

	List<CustFindContractWS> findContract(String custcode, String customerId,
			String nd, String coId);

	List<CustFindContractWS> findChildContract(
			List<CustFindContractWS> listChild);
	
	public List<Map<String, Object>> findChildCustomer(
			List<String> list);

	List<CustFindContractWS> findContract(List<String> listCustomer, String nd,
			String coId);

	String getCustomerByCustCode(String custcode);
}
