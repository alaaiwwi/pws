/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.service;

import java.util.List;

import ma.iam.pws.grc.bean.CreditMobileWS;
import ma.iam.pws.grc.bean.CugWS;
import ma.iam.pws.grc.bean.InfoDataWS;
import ma.iam.pws.grc.bean.RdbWS;
import ma.iam.pws.grc.bean.ServiceCreditWS;
import ma.iam.pws.grc.bean.ServiceDnsWS;
import ma.iam.pws.grc.bean.ServiceHistoryWS;
import ma.iam.pws.grc.bean.ServiceParamWS;
import ma.iam.pws.grc.bean.ServiceWS;
import ma.iam.pws.grc.exceptions.GrcWsException;

// TODO: Auto-generated Javadoc
/**
 * The Interface IServiceDAO.
 */
public interface IServiceDAO {

    /**
     * Retourne la liste des services du client
     * 
     * @param contractIdBscs
     *            le contrat bscs du client (champ CO_ID)
     * @return la liste des résultats
     */
    List<ServiceWS> findServices(final String contractIdBscs);

    /**
     * Retourne la liste des services DNS du client.
     * 
     * @param contractIdBscs
     *            the contract id bscs
     * @return the list
     * @throws GrcWsException
     */
    List<ServiceDnsWS> findServicesDns(final String contractIdBscs) throws GrcWsException;

    /**
     * Retourne la liste des services Crédit du client
     * 
     * @param contractIdBscs
     *            the contract id bscs
     * @return the list
     */
    List<ServiceCreditWS> findServicesCredit(final String contractIdBscs);

    /**
     * Retourne la liste des CUG/Groupe fermés d'abonnés du client mobile
     * (Closed User Group)
     * 
     * @param contractIdBscs
     *            the contract id bscs
     * @return the list
     */
    List<CugWS> findCug(final String contractIdBscs);

    /**
     * Retourne l'historique d'un service donné
     * 
     * @param contractIdBscs
     *            le contrat bscs du client (champ CO_ID)
     * @param sncode
     *            the sncode
     * @return la liste des résultats
     */
    List<ServiceHistoryWS> findServiceHistory(final String contractIdBscs, final String sncode);

    /**
     * Retourne la liste des péramétres d'un service donné
     * 
     * @param paramId
     *            l'identifiant du paramétre
     * @return la liste des résultats
     */
    List<ServiceParamWS> findServiceParams(final String paramId, final String contractIdNeto);

    /**
     * Retourne les paramétres du RDB
     * 
     * @param nd
     *            the nd
     * @return the rdb ws
     */
    //retirer du perimetre atos
 /**   RdbWS findRdbParams(final String nd); **/

    /**
     * Retourne les crédits mobile du contrat
     * 
     * @param contractId
     *            the contract id
     * @return the credit mobile ws
     */
    CreditMobileWS findCreditMobileParams(final String contractId);

    /**
     * Retourne les informations DATA du client
     * 
     * @param contractIdBscs
     *            le contrat bscs du client (champ CO_ID)
     * @param contractIdNeto
     *            l'identifiant du contrat netonomy correspondant au client
     * @return the info data ws
     */
    InfoDataWS findInfoData(final String contractIdBscs, final String contractIdNeto);

    /**
     * Retourne la liste de tous les HLR disponibles dans BSCS
     * 
     * @return the list
     */
    List<String> findAllHlr();

    /**
     * Retourne l'identifiant du HLR du client é partir de son ND
     * 
     * @param nd
     *            the nd
     * @return the string
     */
    String findCustomerHlr(final String nd);

    /**
     * Retourne la classe SLA du client: C0, C1, C2 ou C3.
     * 
     * @param contractIdBscs
     *            the contract id bscs
     * @return the sla class
     */
    String getSlaClass(String contractIdBscs);

}
