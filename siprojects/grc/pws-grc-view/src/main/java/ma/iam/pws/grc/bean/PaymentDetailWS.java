/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class PaymentDetailWS.
 */
public class PaymentDetailWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -582831967527969981L;

    /** The reference facture. */
    private String referenceFacture;

    /** The num transaction. */
    private String numTransaction;

    /** The num document. */
    private String numDocument;

    /** The montant. */
    private String montant;

    /** The type. */
    private String type;

    /** The date paiement. */
    private String datePaiement;

    /**
     * Gets the reference facture.
     * 
     * @return the reference facture
     */
    public String getReferenceFacture() {
        return referenceFacture;
    }

    /**
     * Sets the reference facture.
     * 
     * @param referenceFacture
     *            the new reference facture
     */
    public void setReferenceFacture(final String referenceFacture) {
        this.referenceFacture = referenceFacture;
    }

    /**
     * Gets the num transaction.
     * 
     * @return the num transaction
     */
    public String getNumTransaction() {
        return numTransaction;
    }

    /**
     * Sets the num transaction.
     * 
     * @param numTransaction
     *            the new num transaction
     */
    public void setNumTransaction(final String numTransaction) {
        this.numTransaction = numTransaction;
    }

    /**
     * Gets the num document.
     * 
     * @return the num document
     */
    public String getNumDocument() {
        return numDocument;
    }

    /**
     * Sets the num document.
     * 
     * @param numDocument
     *            the new num document
     */
    public void setNumDocument(final String numDocument) {
        this.numDocument = numDocument;
    }

    /**
     * Gets the montant.
     * 
     * @return the montant
     */
    public String getMontant() {
        return montant;
    }

    /**
     * Sets the montant.
     * 
     * @param montant
     *            the new montant
     */
    public void setMontant(final String montant) {
        this.montant = montant;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the date paiement.
     * 
     * @return the date paiement
     */
    public String getDatePaiement() {
        return datePaiement;
    }

    /**
     * Sets the date paiement.
     * 
     * @param datePaiement
     *            the new date paiement
     */
    public void setDatePaiement(final String datePaiement) {
        this.datePaiement = datePaiement;
    }

}
