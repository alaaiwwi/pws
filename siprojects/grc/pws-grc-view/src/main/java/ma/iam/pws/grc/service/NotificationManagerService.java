/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import javax.annotation.security.RolesAllowed;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.SmsWS;
import ma.iam.pws.grc.business.SmsManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.security.SecuredObject;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class NotificationManagerService.
 */
@SecuredObject
@WebService(serviceName = "NotificationManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "notificationManagerService")
public class NotificationManagerService extends SpringBeanAutowiringSupport {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(NotificationManagerService.class);

    /** The sms manager biz. */
    @Autowired
    private SmsManagerBiz smsManagerBiz;

    /**
     * Insert sms.
     * 
     * @param smsBean
     *            the sms bean
     * @param product
     *            the product
     * @return the boolean
     * @throws GrcWsException
     *             the grc ws exception
     */
	@RolesAllowed("ROLE_WRITE")
    @WebMethod(operationName = "insertSms")
    @WebResult(name = "BooleanResult")
    public Boolean insertSms(@WebParam(name = "smsBean") final SmsWS smsBean,
            @WebParam(name = "produit") final int product) throws GrcWsException {
        LOG.trace("@WebMethod: smsManagerBiz.insertSms smsBean={}", smsBean);
        return smsManagerBiz.insertSms(smsBean, product);
    }
    @Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
