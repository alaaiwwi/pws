/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc

/**
 * The Class TicketCADWS.
 */
public class TicketCADWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    /** The nd. */
    private String nd;

    /** The ticket id. */
    private String ticketId;

    /** The produit. */
    private String produit;

    /** The categorie. */
    private String categorie;

    /** The categorie cd. */
    private String categorieCd;

    /** The groupe. */
    private String groupe;

    /** The groupe cd. */
    private String groupeCd;

    /** The famille. */
    private String famille;

    /** The famille cd. */
    private String familleCd;

    /** The type. */
    private String type;

    /** The type cd. */
    private String typeCd;

    /** The date. */
    private String date;

    /** The batch id. */
    private String batchId;

    /** The flag malveillant. */
    private String flagMalveillant;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        sb = new StringBuilder();
        sb.append("[TicketCADWS:");
        if (nd != null) {
            sb.append(",nd=").append(nd);
        }
        if (ticketId != null) {
            sb.append(",ticketId=").append(ticketId);
        }
        if (produit != null) {
            sb.append(",produit=").append(produit);
        }
        if (categorie != null) {
            sb.append(",categorie=").append(categorie);
        }
        if (categorieCd != null) {
            sb.append(",categorieCd=").append(categorieCd);
        }
        if (groupe != null) {
            sb.append(",groupe=").append(groupe);
        }
        if (groupeCd != null) {
            sb.append(",groupeCd=").append(groupeCd);
        }
        if (famille != null) {
            sb.append(",famille=").append(famille);
        }
        if (familleCd != null) {
            sb.append(",familleCd=").append(familleCd);
        }
        if (familleCd != null) {
            sb.append(",type=").append(type);
        }
        if (familleCd != null) {
            sb.append(",typeCd=").append(typeCd);
        }
        if (familleCd != null) {
            sb.append(",date=").append(date);
        }
        if (familleCd != null) {
            sb.append(",batchId=").append(batchId);
        }
        if (familleCd != null) {
            sb.append(",flagMalveillant=").append(flagMalveillant);
        }
        sb.append("]");
        return sb.toString();
    }


    /**
     * Gets the nd.
     * 
     * @return the nd
     */
    public String getNd() {
        return nd;
    }

    /**
     * Sets the nd.
     * 
     * @param nd
     *            the new nd
     */
    public void setNd(final String nd) {
        this.nd = nd;
    }

    /**
     * Gets the ticket id.
     * 
     * @return the ticket id
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * Sets the ticket id.
     * 
     * @param ticketId
     *            the new ticket id
     */
    public void setTicketId(final String ticketId) {
        this.ticketId = ticketId;
    }

    /**
     * Gets the produit.
     * 
     * @return the produit
     */
    public String getProduit() {
        return produit;
    }

    /**
     * Sets the produit.
     * 
     * @param produit
     *            the new produit
     */
    public void setProduit(final String produit) {
        this.produit = produit;
    }

    /**
     * Gets the categorie.
     * 
     * @return the categorie
     */
    public String getCategorie() {
        return categorie;
    }

    /**
     * Sets the categorie.
     * 
     * @param categorie
     *            the new categorie
     */
    public void setCategorie(final String categorie) {
        this.categorie = categorie;
    }

    /**
     * Gets the categorie cd.
     * 
     * @return the categorie cd
     */
    public String getCategorieCd() {
        return categorieCd;
    }

    /**
     * Sets the categorie cd.
     * 
     * @param categorieCd
     *            the new categorie cd
     */
    public void setCategorieCd(final String categorieCd) {
        this.categorieCd = categorieCd;
    }

    /**
     * Gets the groupe.
     * 
     * @return the groupe
     */
    public String getGroupe() {
        return groupe;
    }

    /**
     * Sets the groupe.
     * 
     * @param groupe
     *            the new groupe
     */
    public void setGroupe(final String groupe) {
        this.groupe = groupe;
    }

    /**
     * Gets the groupe cd.
     * 
     * @return the groupe cd
     */
    public String getGroupeCd() {
        return groupeCd;
    }

    /**
     * Sets the groupe cd.
     * 
     * @param groupeCd
     *            the new groupe cd
     */
    public void setGroupeCd(final String groupeCd) {
        this.groupeCd = groupeCd;
    }

    /**
     * Gets the famille.
     * 
     * @return the famille
     */
    public String getFamille() {
        return famille;
    }

    /**
     * Sets the famille.
     * 
     * @param famille
     *            the new famille
     */
    public void setFamille(final String famille) {
        this.famille = famille;
    }

    /**
     * Gets the famille cd.
     * 
     * @return the famille cd
     */
    public String getFamilleCd() {
        return familleCd;
    }

    /**
     * Sets the famille cd.
     * 
     * @param familleCd
     *            the new famille cd
     */
    public void setFamilleCd(final String familleCd) {
        this.familleCd = familleCd;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the type cd.
     * 
     * @return the type cd
     */
    public String getTypeCd() {
        return typeCd;
    }

    /**
     * Sets the type cd.
     * 
     * @param typeCd
     *            the new type cd
     */
    public void setTypeCd(final String typeCd) {
        this.typeCd = typeCd;
    }

    /**
     * Gets the date.
     * 
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the date.
     * 
     * @param date
     *            the new date
     */
    public void setDate(final String date) {
        this.date = date;
    }

    /**
     * Gets the batch id.
     * 
     * @return the batch id
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * Sets the batch id.
     * 
     * @param batchId
     *            the new batch id
     */
    public void setBatchId(final String batchId) {
        this.batchId = batchId;
    }

    /**
     * Gets the flag malveillant.
     * 
     * @return the flag malveillant
     */
    public String getFlagMalveillant() {
        return flagMalveillant;
    }

    /**
     * Sets the flag malveillant.
     * 
     * @param flagMalveillant
     *            the new flag malveillant
     */
    public void setFlagMalveillant(final String flagMalveillant) {
        this.flagMalveillant = flagMalveillant;
    }


}
