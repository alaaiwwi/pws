package ma.iam.pws.grc.dao.service;

/**
 * The Class ParamAdress.
 */
public class ParamAdress {

    /**
     * Instantiates a new param adress.
     */
    public ParamAdress() {

    }

    /** The param id. */
    private String paramId;

    /** The value. */
    private String value;

    public String getParamId() {
        return paramId;
    }

    public void setParamId(final String paramId) {
        this.paramId = paramId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
