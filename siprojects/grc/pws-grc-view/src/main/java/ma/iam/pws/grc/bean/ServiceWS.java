/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class ServiceWS.
 */
public class ServiceWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2351881891090393855L;

    
    private String shdes ;
    /** The sncode. */
    private Integer sncode;

//    /** The profile id. */
//    private String profileId;

//    /** The param id. */
//    private String paramId;

    /** The service name. */
    private String serviceName;

    /** The pack. */
    private String servicePack;

    /** The status. */
    private String status;

    /** The billing date. */
    private String billingDate;

//    /** The request id. */
//    private String requestId;

    /** The entry date. */
    private String entryDate;

 

    /**
     * Gets the profile id.
     * 
     * @return the profile id
     */
//    public String getProfileId() {
//        return profileId;
//    }

    public Integer getSncode() {
		return sncode;
	}

	public void setSncode(Integer sncode) {
		this.sncode = sncode;
	}

	/**
     * Sets the profile id.
     * 
     * @param profileId
     *            the new profile id
     */
//    public void setProfileId(final String profileId) {
//        this.profileId = profileId;
//    }

    /**
     * Gets l'identifiant du paramétre.
     * 
     * @return l'identifiant du paramétre
     */
//    public String getParamId() {
//        return paramId;
//    }
//
//    /**
//     * Sets l'identifiant du paramétre.
//     * 
//     * @param paramId
//     *            the new param id
//     */
//    public void setParamId(final String paramId) {
//        this.paramId = paramId;
//    }

    /**
     * Gets the service name.
     * 
     * @return the service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the service name.
     * 
     * @param serviceName
     *            the new service name
     */
    public void setServiceName(final String serviceName) {
        this.serviceName = serviceName;
    }

    
    public String getServicePack() {
		return servicePack;
	}

	public void setServicePack(String servicePack) {
		this.servicePack = servicePack;
	}

	/**
     * Gets le statut.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets le statut.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Gets the billing date.
     * 
     * @return the billing date
     */
    public String getBillingDate() {
        return billingDate;
    }

    /**
     * Sets the billing date.
     * 
     * @param billingDate
     *            the new billing date
     */
    public void setBillingDate(final String billingDate) {
        this.billingDate = billingDate;
    }

    /**
     * Gets the request id.
     * 
     * @return the request id
     */
//    public String getRequestId() {
//        return requestId;
//    }
//
//    /**
//     * Sets the request id.
//     * 
//     * @param requestId
//     *            the new request id
//     */
//    public void setRequestId(final String requestId) {
//        this.requestId = requestId;
//    }

    /**
     * Gets the entry date.
     * 
     * @return the entry date
     */
    public String getEntryDate() {
        return entryDate;
    }

    /**
     * Sets the entry date.
     * 
     * @param entryDate
     *            the new entry date
     */
    public void setEntryDate(final String entryDate) {
        this.entryDate = entryDate;
    }

	public String getShdes() {
		return shdes;
	}

	public void setShdes(String shdes) {
		this.shdes = shdes;
	}
    
    
}
