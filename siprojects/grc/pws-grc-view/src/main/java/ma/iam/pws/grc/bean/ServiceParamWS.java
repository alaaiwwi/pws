/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class ServiceParamWS.
 */
public class ServiceParamWS extends GrcWS {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5251838196144505681L;

	private String shdes;
	private String sncode;
	private String parameterValue;
	private String parameterId;

	public String getShdes() {
		return shdes;
	}

	public void setShdes(String shdes) {
		this.shdes = shdes;
	}

	public String getSncode() {
		return sncode;
	}

	public void setSncode(String sncode) {
		this.sncode = sncode;
	}

	public String getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	public String getParameterId() {
		return parameterId;
	}

	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}

}
