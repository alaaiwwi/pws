/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class MicroCelluleWS.
 */
public class MicroCelluleWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7681542114156548043L;

    /** The id. */
    private Integer id;

    /** The description. */
    private String description;

    /** The type. */
    private String type;

    /** The number. */
    private String number;

    /** The destination. */
    private String destination;

    /** The rate. */
    private String rate;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the number.
     * 
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number.
     * 
     * @param number
     *            the new number
     */
    public void setNumber(final String number) {
        this.number = number;
    }

    /**
     * Gets the destination.
     * 
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the destination.
     * 
     * @param destination
     *            the new destination
     */
    public void setDestination(final String destination) {
        this.destination = destination;
    }

    /**
     * Gets the rate.
     * 
     * @return the rate
     */
    public String getRate() {
        return rate;
    }

    /**
     * Sets the rate.
     * 
     * @param rate
     *            the new rate
     */
    public void setRate(final String rate) {
        this.rate = rate;
    }

}
