/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class IntervalWS.
 */
public class IntervalWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -3353546891826222856L;

    /** The start date. */
    private String startDate;

    /** The end date. */
    private String endDate;

    /**
     * Gets the end date.
     * 
     * @return the end date
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Gets the start date.
     * 
     * @return the start date
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the end date.
     * 
     * @param endDate
     *            the new end date
     */
    public void setEndDate(final String endDate) {
        this.endDate = endDate;
    }

    /**
     * Sets the start date.
     * 
     * @param startDate
     *            the new start date
     */
    public void setStartDate(final String startDate) {
        this.startDate = startDate;
    }

}
