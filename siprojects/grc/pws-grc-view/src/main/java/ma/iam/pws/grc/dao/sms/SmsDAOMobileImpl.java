/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.sms;

import ma.iam.pws.grc.bean.SmsDaoWS;
import ma.iam.pws.grc.bean.SmsWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;
import ma.iam.pws.grc.util.SmsBeanUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

/**
 * The Class FeesDAOFixeImpl.
 */
@Repository
public class SmsDAOMobileImpl extends BaseDAOMobile implements ISmsDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SmsDAOMobileImpl.class);

	/** La constante INSERTFEES. */
	public static final String INSERTSMS = SmsDAOMobileImpl.class.getName()
			+ ".INSERTSMS";
	public static final String GET_ID = SmsDAOMobileImpl.class.getName()
			+ ".GET_ID";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fees.IFeesDAO#saveFeesWS(fr.capgemini.iam.grc
	 * .dao.fees.FeesWS)
	 */

	public Boolean insertSmsWS(final SmsWS smsBean) {
		LOG.debug("--> insertSmsWS smsBean {}", smsBean);
		final String sql = CustomSQLUtil.get(INSERTSMS);
		final String sqlGetId = CustomSQLUtil.get(GET_ID);

		final int lng = super.getNamedParameterJdbcTemplate().queryForInt(
				sqlGetId, new MapSqlParameterSource());
		SmsDaoWS sms = SmsBeanUtil.Sms2SmsDao(smsBean);
		sms.setId(lng);
		final SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
				sms);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		final int nbRowUpdated = super.getNamedParameterJdbcTemplate().update(
				sql, namedParameters);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- insertSmsWS");
		return nbRowUpdated > 0 ? Boolean.TRUE : Boolean.FALSE;
	}

}
