/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.invoice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.InvoiceDetailWS;
import ma.iam.pws.grc.bean.InvoiceWS;
import ma.iam.pws.grc.bean.InvoiceWSFindInvoice;
import ma.iam.pws.grc.bean.PaymentDetailWS;
import ma.iam.pws.grc.bean.PaymentWS;
import ma.iam.pws.grc.bean.SearchInvoiceBean;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class InvoiceDAOFixeImpl.
 */
@Repository
public class InvoiceDAOFixeImpl extends BaseDAOFixe implements IInvoiceDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(InvoiceDAOFixeImpl.class);

	/** La constante GETLISTEFACTURES. */
	public static final String GETLISTEFACTURES = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEFACTURES";

	/** La constante GETLISTEFACTURESDETAILS. */
	public static final String GETLISTEFACTURESDETAILS = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEFACTURESDETAILS";

	/** La constante GETLISTEPAIEMENTSDETAILS. */
	public static final String GETLISTEPAIEMENTSDETAILS = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEPAIEMENTSDETAILS";

	/** La constante GETLISTEPAIEMENTSCLIENT. */
	public static final String GETLISTEPAIEMENTSCLIENT = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEPAIEMENTSCLIENT";

	/** La constante GETLISTEPAIEMENTSFACTURE. */
	public static final String GETLISTEPAIEMENTSFACTURE = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEPAIEMENTSFACTURE";

	/** La constante MONTANTGLOBALOUVERT. */
	public static final String MONTANTGLOBALOUVERT = InvoiceDAOFixeImpl.class
			.getName() + ".MONTANTGLOBALOUVERT";

	/** La constante GETLISTEFACTURESBYINVOICEEXTERNALID. */
	public static final String GETLISTEFACTURESBYINVOICEEXTERNALID = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEFACTURESBYINVOICEEXTERNALID";

	/** La constante GETLISTEFACTURESBYINVOICEEXTERNALID. */
	public static final String GETLISTEFACTURESBYREFERENCE = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEFACTURESBYREFERENCE";

	/** La constante PROTECTINVOICES. */
	public static final String PROTECTINVOICES = InvoiceDAOFixeImpl.class
			.getName() + ".PROTECTINVOICES";

	/** La constante UNPROTECTINVOICES. */
	public static final String UNPROTECTINVOICES = InvoiceDAOFixeImpl.class
			.getName() + ".UNPROTECTINVOICES";

	/** La constante INSERTHISTO. */
	public static final String INSERTHISTO = InvoiceDAOFixeImpl.class.getName()
			+ ".INSERTHISTO";

	/** La constante GETLISTEFACTURESBYID. */
	public static final String GETLISTEFACTURESBYID = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEFACTURESBYID";

	/*
	 * f.tatbi add sql for 5373
	 */

	private static final String INSERT_UNPAID_UNITARY_RETAB_PREPARE = InvoiceDAOFixeImpl.class
			.getName() + ".INSERT_UNPAID_UNITARY_RETAB_PREPARE";

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findInvoicesDetails(ma
	 * .iam.grc.bean.InvoiceWS)
	 */

	public List<InvoiceDetailWS> findInvoicesDetails(final String idFacture) {
		LOG.debug("--> findInvoicesDetails idFacture {}", idFacture);
		List<InvoiceDetailWS> listInvoiceDetails = null;
		final String sql = CustomSQLUtil.get(GETLISTEFACTURESDETAILS);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("idFacture", idFacture);
		final CustomBeanPropertyRowMapper<InvoiceDetailWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceDetailWS>(
				InvoiceDetailWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findInvoicesDetails");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findInvoicesForCustomer(
	 * fr.capgemini.iam.grc.bean.SearchInvoiceBean)
	 */

	public List<InvoiceWSFindInvoice> findInvoicesForCustomer(
			final SearchInvoiceBean searchInvoiceBean) {
		LOG.debug("--> findInvoicesForCustomer searchInvoiceBean {}",
				searchInvoiceBean);
		if (LOG.isDebugEnabled())
			LOG.debug("Customer_id " + searchInvoiceBean.getCustomerId());
		if (LOG.isDebugEnabled())
			LOG.debug("startDate "
					+ searchInvoiceBean.getInterval().getStartDate());
		if (LOG.isDebugEnabled())
			LOG.debug("endDate " + searchInvoiceBean.getInterval().getEndDate());

		List<InvoiceWSFindInvoice> listInvoices = null;

		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURES));

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID,
				searchInvoiceBean.getCustomerId());

		namedParameters.put("startDate", searchInvoiceBean.getInterval()
				.getStartDate());
		namedParameters.put("endDate", searchInvoiceBean.getInterval()
				.getEndDate());

		if (searchInvoiceBean.getStatut() != null) {
			sqlBuffer.append(" AND o.OHSTATUS ='"
					+ searchInvoiceBean.getStatut() + "'");
		}
		sqlBuffer.append("  order by o.OHREFDATE desc");
		final CustomBeanPropertyRowMapper<InvoiceWSFindInvoice> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWSFindInvoice>(
				InvoiceWSFindInvoice.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
		listInvoices = super.getNamedParameterJdbcTemplate().query(
				sqlBuffer.toString(), namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findInvoicesForCustomer");
		return listInvoices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findPaymentsDetails(ma
	 * .iam.grc.bean.InvoiceWS)
	 */

	public List<PaymentDetailWS> findPaymentsDetails(final String numTransaction) {
		List<PaymentDetailWS> listInvoiceDetails = null;

		if (LOG.isDebugEnabled())
			LOG.debug("--> findPaymentsDetails numTransaction="
					+ numTransaction);
		final String sql = CustomSQLUtil.get(GETLISTEPAIEMENTSDETAILS);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("numTransaction", numTransaction);
		final CustomBeanPropertyRowMapper<PaymentDetailWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<PaymentDetailWS>(
				PaymentDetailWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findPaymentsDetails");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findPayments(java.lang.String
	 * )
	 */

	public List<PaymentWS> findPayments(
			final SearchInvoiceBean searchInvoiceBean) {
		LOG.debug("--> findPayments searchInvoiceBean {}", searchInvoiceBean);
		if (LOG.isDebugEnabled())
			LOG.debug("CUSTOMER_ID " + searchInvoiceBean.getCustomerId());
		if (LOG.isDebugEnabled())
			LOG.debug("startDate "
					+ searchInvoiceBean.getInterval().getStartDate());
		if (LOG.isDebugEnabled())
			LOG.debug("endDate " + searchInvoiceBean.getInterval().getEndDate());
		List<PaymentWS> listInvoiceDetails = null;

		final String sql = CustomSQLUtil.get(GETLISTEPAIEMENTSCLIENT);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID,
				searchInvoiceBean.getCustomerId());
		namedParameters.put("startDate", searchInvoiceBean.getInterval()
				.getStartDate());
		namedParameters.put("endDate", searchInvoiceBean.getInterval()
				.getEndDate());

		final CustomBeanPropertyRowMapper<PaymentWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<PaymentWS>(
				PaymentWS.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findPayments");
		return listInvoiceDetails;
	}

	/*
	 * f.tatbi FC 5864 add parametre idFacture
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findPaymentsForInvoice(java
	 * .lang.String)
	 */

	public List<PaymentWS> findPaymentsForInvoice(final String refFacture,
			final Integer idFacture) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> findPaymentsForInvoice: numFacture=" + refFacture
					+ "   idFacture =" + idFacture);
		List<PaymentWS> listInvoiceDetails = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETLISTEPAIEMENTSFACTURE));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("numFacture", refFacture);

		final CustomBeanPropertyRowMapper<PaymentWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<PaymentWS>(
				PaymentWS.class);

		if (idFacture != null)
			sql.append(" AND oa.OHXACT = '" + idFacture + "'");
		if (refFacture != null)
			sql.append(" AND oa.OHREFNUM = '" + refFacture + "'");

		sql.append(" ORDER BY C.CAXACT DESC");

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(
				sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findPaymentsForInvoice");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findMontantGlobalOuvert(
	 * java.lang.String)
	 */

	public Double findMontantGlobalOuvert(final String custAcctCode) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> findMontantGlobalOuvert: custAcctCode="
					+ custAcctCode);
		Double result;
		try {
			final String sql = CustomSQLUtil.get(MONTANTGLOBALOUVERT);

			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put("custAcctCode", custAcctCode);
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
			result = super.getNamedParameterJdbcTemplate().queryForObject(sql,
					namedParameters, Double.class);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);
			if (LOG.isDebugEnabled())
				LOG.debug("<-- findMontantGlobalOuvert: " + result);
		} catch (EmptyResultDataAccessException e) {
			result = null;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#
	 * findInvoicesByListInvoiceExternalId(java.util.List)
	 */

	public List<InvoiceWS> findInvoicesByListInvoiceExternalId(
			final List<Integer> listInvoiceExternalId) {
		List<InvoiceWS> listInvoices = null;
		if (LOG.isDebugEnabled())
			LOG.debug("--> findInvoicesByListInvoiceExternalId: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURESBYINVOICEEXTERNALID));

		if (null != listInvoiceExternalId) {
			for (int i = 0; i < listInvoiceExternalId.size(); i++) {
				sqlBuffer.append(listInvoiceExternalId.get(i));
				if (i != listInvoiceExternalId.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			sqlBuffer.append(" order by OHREFDATE desc ");
			final CustomBeanPropertyRowMapper<InvoiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWS>(
					InvoiceWS.class);
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			listInvoices = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations()
					.query(sqlBuffer.toString(), invoiceRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findInvoicesByListInvoiceExternalId: "
					+ listInvoices);

		return listInvoices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findInvoicesByListReference
	 * (java.util.List)
	 */

	public List<InvoiceWS> findInvoicesByListReference(
			final List<String> listReferenceFacture) {
		List<InvoiceWS> listInvoices = null;
		if (LOG.isDebugEnabled())
			LOG.debug("--> findInvoicesByListReference: listReferenceFacture="
					+ listReferenceFacture);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURESBYREFERENCE));

		if (null != listReferenceFacture) {
			for (int i = 0; i < listReferenceFacture.size(); i++) {
				sqlBuffer.append("'").append(listReferenceFacture.get(i))
						.append("'");
				if (i != listReferenceFacture.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			final CustomBeanPropertyRowMapper<InvoiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWS>(
					InvoiceWS.class);
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			listInvoices = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations()
					.query(sqlBuffer.toString(), invoiceRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findInvoicesByListReference: " + listInvoices);

		return listInvoices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#
	 * findInvoicesByListInvoiceExternalId(java.util.List)
	 */

	public Boolean protectInvoices(final List<Integer> listInvoiceExternalId) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> protectInvoices: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(PROTECTINVOICES));
		int rowsUpdated = 0;
		if ((null != listInvoiceExternalId) && !listInvoiceExternalId.isEmpty()) {
			for (final Integer invoiceExternalId : listInvoiceExternalId) {
				sqlBuffer.append(invoiceExternalId.toString()).append(",");
			}
			sqlBuffer.append(" 0 ) ");
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			rowsUpdated = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().update(sqlBuffer.toString());
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		LOG.debug("<-- protectInvoices");

		return rowsUpdated > 0 ? true : false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#unProtectInvoices(java.util
	 * .List)
	 */

	public Boolean unProtectInvoices(final List<Integer> listInvoiceExternalId) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> unProtectInvoices: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(UNPROTECTINVOICES));

		int rowsUpdated = 0;
		if ((null != listInvoiceExternalId) && !listInvoiceExternalId.isEmpty()) {
			for (final Integer invoiceExternalId : listInvoiceExternalId) {
				sqlBuffer.append(invoiceExternalId.toString()).append(",");
			}
			sqlBuffer.append(" 0 ) ");
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			rowsUpdated = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().update(sqlBuffer.toString());
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		LOG.debug("<-- unProtectInvoices");

		return rowsUpdated > 0 ? true : false;
	}

	public List<InvoiceWS> findDetailedInvoicesForCustomer(
			SearchInvoiceBean searchInvoiceBean) {
		throw new NotImplementedException(
				"findDetailedInvoicesForCustomer non disponible pour le produit Fixe");
	}

	public Boolean insertHistory(Integer facture, String user,
			String application, String msg) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> insertHistory: facture=" + facture + " user " + user
					+ " application " + application + " msg " + msg);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(INSERTHISTO));

		SqlParameterSource mapSqlParameters = new MapSqlParameterSource()
				.addValue("factureId", facture)
				.addValue("ticklerCode", TICKLER_CODE).addValue("user", user)
				.addValue("application", application).addValue("message", msg);
		final int nbRowUpdated = super.getNamedParameterJdbcTemplate().update(
				sqlBuffer.toString(), mapSqlParameters);

		LOG.debug("<-- insertHistory");
		return nbRowUpdated > 0 ? Boolean.TRUE : Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#
	 * findInvoiceById(java.util.List,java.util.List,java.lang.String)
	 */

	public List<InvoiceWSFindInvoice> findInvoiceById(
			final List<String> refFacture, final List<Integer> idFacture,
			final String statut) {
		List<InvoiceWSFindInvoice> listInvoices = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findInvoiceById: refFacture=" + refFacture
					+ " idFacture=" + idFacture + " statut =" + statut);
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURESBYID));

		if (refFacture != null && !refFacture.isEmpty()) {
			sqlBuffer.append(" OHREFNUM in (");
			for (int i = 0; i < refFacture.size(); i++) {
				sqlBuffer.append("'" + refFacture.get(i) + "'");
				if (i != refFacture.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
		}

		if (idFacture != null && !idFacture.isEmpty()) {
			if (refFacture != null && !refFacture.isEmpty())
				sqlBuffer.append(" AND OHXACT in (");
			else
				sqlBuffer.append(" OHXACT in (");

			for (int i = 0; i < idFacture.size(); i++) {
				sqlBuffer.append(idFacture.get(i));
				if (i != idFacture.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
		}

		if (!GRCStringUtil.isNullOrEmpty(statut)) {
			sqlBuffer.append(" AND OHSTATUS ='" + statut + "'");
		}

		sqlBuffer.append(" order by OHREFDATE desc ");
		final CustomBeanPropertyRowMapper<InvoiceWSFindInvoice> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWSFindInvoice>(
				InvoiceWSFindInvoice.class);
		if (LOG.isDebugEnabled())
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
		listInvoices = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations()
				.query(sqlBuffer.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findInvoiceById: " + listInvoices);

		return listInvoices;
	}

	public Boolean insertUnPaid(Integer flag, Integer order_ref) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> insertUnPaid: facture=" + order_ref + " user "
					+ LITIGE_USER + " flag " + flag);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(INSERT_UNPAID_UNITARY_RETAB_PREPARE));

		SqlParameterSource mapSqlParameters = new MapSqlParameterSource()
				.addValue("factureId", order_ref)
				.addValue("order_ref", order_ref).addValue("flag", flag)
				.addValue("user_name", LITIGE_USER);
		final int nbRowUpdated = super.getNamedParameterJdbcTemplate().update(
				sqlBuffer.toString(), mapSqlParameters);

		LOG.debug("<-- insertUnPaid");
		return nbRowUpdated > 0 ? Boolean.TRUE : Boolean.FALSE;
	}
}
