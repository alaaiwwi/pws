/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.fidelio;

import java.util.List;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.FidelioAccountWS;
import ma.iam.pws.grc.bean.FidelioActionHistWS;
import ma.iam.pws.grc.bean.FidelioOrderDetailWS;
import ma.iam.pws.grc.bean.FidelioOrderHistWS;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * The Class FidelioDAOMobileImpl.
 */
@Repository
public class FidelioDAODataImpl extends BaseDAOFixe implements IFidelioDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(FidelioDAODataImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioAccount(java.
	 * lang.String, int)
	 */

	public FidelioAccountWS findFidelioAccount(final String customer_id,
			final String custCode) {
		LOG.debug("--> findFidelioAccount custCode {}", custCode);
		throw new NotImplementedException(
				"Service Fidelio non disponible pour le produit Data");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioActionHist(java
	 * .lang.String, int)
	 */

	public List<FidelioActionHistWS> findFidelioActionHist(
			final String Customer_id, final String custCode) {
		LOG.debug("--> findFidelioActionHist custCode {}", custCode);
		throw new NotImplementedException(
				"Service Fidelio non disponible pour le produit Data");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioOrderHist(java
	 * .lang.String, int)
	 */

	public List<FidelioOrderHistWS> findFidelioOrderHist(
			final String customer_id, final String custCode) {
		LOG.debug("--> findFidelioOrderHist custCode {}", custCode);
		throw new NotImplementedException(
				"Service Fidelio non disponible pour le produit Data");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioOrderDetail(java
	 * .lang.String, int)
	 */

	public List<FidelioOrderDetailWS> findFidelioOrderDetail(
			final String orderId) {
		LOG.debug("--> findFidelioOrderDetail orderId {}", orderId);
		throw new NotImplementedException(
				"Service Fidelio non disponible pour le produit Data");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#setQualiteFidelio(java.util
	 * .List)
	 */

	public void setQualiteFidelio(final List<CustAcctWS> listCustAcctWS) {
		throw new NotImplementedException(
				"Service Fidelio non disponible pour le produit Data");
	}

}
