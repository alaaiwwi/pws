/*
 * @author Capgemini
 */
package ma.iam.pws.grc.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.ws.WebFault;

import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;

/**
 * The Class GrcWsException.
 */
@WebFault(name = "GrcWsException", faultBean = "fr.capgemini.iam.grc.exceptions.FaultBean")
public class GrcWsException extends Exception {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -1127071073519514459L;

    /** The fault bean. */
    private final FaultBean faultBean;

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param cause
     *            the cause
     */
    public GrcWsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final Throwable cause) {
        super(cause);
        final StringBuilder result = new StringBuilder();
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        result.append(sw.toString());
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode());
        this.faultBean.setText(result.toString());
    }

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public GrcWsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final String message, final Throwable cause) {
        super(cause);
        final StringBuilder result = new StringBuilder();
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        result.append(sw.toString());
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode(), message);
        this.faultBean.setText(result.toString());
    }

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     */
    public GrcWsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum) {
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode());
    }

    /**
     * Instantiates a new grc ws exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param message
     *            the message
     */
    public GrcWsException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final String message) {
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode(), message);
    }

    /**
     * Gets the fault info.
     * 
     * @return the fault info
     */
    public FaultBean getFaultInfo() {
        return faultBean;
    }

}
