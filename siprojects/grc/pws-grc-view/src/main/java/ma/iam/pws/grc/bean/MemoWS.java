/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class MemoWS.
 */
public class MemoWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7318058910340003739L;

    /** The id. */
    private Integer id;

    /** The number. */
    private String number;

    /** The code. */
    private String code;

    /** The status. */
    private String status;

    /** The short desc. */
    private String shortDesc;

    /** The creation date. */
    private String creationDate;

    /** The created by. */
    private String createdBy;

    /** The mod date. */
    private String modDate;

    /** The mod by. */
    private String modBy;

    /** The close date. */
    private String closeDate;

    /** The closed by. */
    private String closedBy;

    /** The long desc. */
    private String longDesc;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Gets the number.
     * 
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number.
     * 
     * @param number
     *            the new number
     */
    public void setNumber(final String number) {
        this.number = number;
    }

    /**
     * Gets the code.
     * 
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     * 
     * @param code
     *            the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets le statut.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets le statut.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Gets the short desc.
     * 
     * @return the short desc
     */
    public String getShortDesc() {
        return shortDesc;
    }

    /**
     * Sets the short desc.
     * 
     * @param shortDesc
     *            the new short desc
     */
    public void setShortDesc(final String shortDesc) {
        this.shortDesc = shortDesc;
    }

    /**
     * Gets the creation date.
     * 
     * @return the creation date
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creation date.
     * 
     * @param creationDate
     *            the new creation date
     */
    public void setCreationDate(final String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the created by.
     * 
     * @return the created by
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by.
     * 
     * @param createdBy
     *            the new created by
     */
    public void setCreatedBy(final String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the mod date.
     * 
     * @return the mod date
     */
    public String getModDate() {
        return modDate;
    }

    /**
     * Sets the mod date.
     * 
     * @param modDate
     *            the new mod date
     */
    public void setModDate(final String modDate) {
        this.modDate = modDate;
    }

    /**
     * Gets the mod by.
     * 
     * @return the mod by
     */
    public String getModBy() {
        return modBy;
    }

    /**
     * Sets the mod by.
     * 
     * @param modBy
     *            the new mod by
     */
    public void setModBy(final String modBy) {
        this.modBy = modBy;
    }

    /**
     * Gets the close date.
     * 
     * @return the close date
     */
    public String getCloseDate() {
        return closeDate;
    }

    /**
     * Sets the close date.
     * 
     * @param closeDate
     *            the new close date
     */
    public void setCloseDate(final String closeDate) {
        this.closeDate = closeDate;
    }

    /**
     * Gets the closed by.
     * 
     * @return the closed by
     */
    public String getClosedBy() {
        return closedBy;
    }

    /**
     * Sets the closed by.
     * 
     * @param closedBy
     *            the new closed by
     */
    public void setClosedBy(final String closedBy) {
        this.closedBy = closedBy;
    }

    /**
     * Gets the long desc.
     * 
     * @return the long desc
     */
    public String getLongDesc() {
        return longDesc;
    }

    /**
     * Sets the long desc.
     * 
     * @param longDesc
     *            the new long desc
     */
    public void setLongDesc(final String longDesc) {
        this.longDesc = longDesc;
    }

}
