/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.CustFindContractWS;
import ma.iam.pws.grc.bean.CustSearchWS;
import ma.iam.pws.grc.bean.SearchCustomerWS;
import ma.iam.pws.grc.business.CustomerManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class CustomerManagerService.
 */
@WebService(serviceName = "CustomerManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "customerManagerService")
public class CustomerManagerService extends SpringBeanAutowiringSupport {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CustomerManagerService.class);

	/** The customer manager biz. */
	@Autowired
	private CustomerManagerBiz customerManagerBiz;

	/**
	 * Cherche les contracts for customer.
	 * 
	 * @param searchCustAcctBean
	 *            le bean de recherche client
	 * @param allowVIP
	 *            indique si la recherche des clients VIP est autorisée
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	// @WebMethod(operationName = "findContractsForCustomer")
	// @WebResult(name = "ListCustAcctWs")
	// public List<CustAcctWS> findContractsForCustomer(@WebParam(name =
	// "searchCustAcctBean") final CustAcctWS searchCustAcctBean,
	// @WebParam(name = "allowVIP") final Boolean allowVIP, @WebParam(name =
	// "produit") final int product) throws GrcWsException {
	// LOG.trace("@WebMethod: customerManagerBiz.findContractsForCustomer searchCustAcctBean={}",
	// searchCustAcctBean);
	// return customerManagerBiz.findContractsForCustomer(searchCustAcctBean,
	// allowVIP, product);
	// }

	// @WebMethod(operationName = "addFidelioInfos")
	// @WebResult(name = "ListCustAcctWs")
	// public CustAcctWS addFidelioInfos(@WebParam(name = "searchCustAcctBean")
	// final CustAcctWS searchCustAcctBean, @WebParam(name = "produit") final
	// int product)
	// throws GrcWsException {
	// LOG.trace("@WebMethod: Before customerManagerBiz.addFidelioInfos searchCustAcctBean={}",
	// searchCustAcctBean);
	// customerManagerBiz.addFidelioInfos(searchCustAcctBean, product);
	// LOG.trace("@WebMethod: After customerManagerBiz.addFidelioInfos searchCustAcctBean={}",
	// searchCustAcctBean);
	// return searchCustAcctBean;
	// }

	/**
	 * Cherche les clients.
	 * 
	 * @param searchCustAcctBean
	 *            le bean de recherche client
	 * @param allowVIP
	 *            indique si la recherche des clients VIP est autorisée
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "searchCustomer")
	@WebResult(name = "ListClientsWs")
	public List<CustSearchWS> searchCustomer(
			@WebParam(name = "searchCustomerBean") final SearchCustomerWS searchCustomerWS,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: customerManagerBiz.searchCustomer searchCustomerWS={}",
				searchCustomerWS);
		return customerManagerBiz.searchCustomer(searchCustomerWS, product);
	}

	@WebMethod(operationName = "findContract")
	@WebResult(name = "listContract")
	public List<CustFindContractWS> findContract(
			@WebParam(name = "custcode") final String custcode,
			@WebParam(name = "customerId") final String customerId,
			@WebParam(name = "nd") final String nd,
			@WebParam(name = "coId") final String coId,
			@WebParam(name = "hierarchique") final boolean hierarchique,
			@WebParam(name = "produit") final int produit) throws GrcWsException {
		return customerManagerBiz.findContract(custcode, customerId, nd,
				coId, hierarchique, produit);

	}

	@Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}