/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.memo;

import java.util.List;

import ma.iam.pws.grc.bean.MemoAddInfoWS;
import ma.iam.pws.grc.bean.MemoWS;
import ma.iam.pws.grc.bean.MemoWiamWS;
import ma.iam.pws.grc.bean.TerminationReasonWS;

// TODO: Auto-generated Javadoc
/**
 * The Interface IMemoDAO.
 */
public interface IMemoDAO {

    /**
     * Cherche les memos d'un client/contrat
     * 
     * @param customerId
     *            l'identifiant client BSCS (CUSTOMER_ID)
     * @param contractIdBscs
     *            the contract id
     * @return la liste des résultats
     */
    List<MemoWS> findMemos(final String customerId, final String contractIdBscs);

    /**
     * Insére un mémo dans WIAM
     * 
     * @param memoWiamWS
     *            the memo wiam ws
     * @return the boolean
     */
    Boolean insertMemoWiam(final MemoWiamWS memoWiamWS, final List<MemoAddInfoWS> listMemoAddInfos);

    /**
     * Recherche les motifs de résiliation.
     * 
     * @return Liste des motifs de résiliation
     */
    List<TerminationReasonWS> getTerminationReasons();

}
