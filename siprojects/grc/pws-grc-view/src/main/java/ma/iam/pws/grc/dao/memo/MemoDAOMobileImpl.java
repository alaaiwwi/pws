/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.memo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.MemoAddInfoWS;
import ma.iam.pws.grc.bean.MemoWS;
import ma.iam.pws.grc.bean.MemoWiamWS;
import ma.iam.pws.grc.bean.TerminationReasonWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

/**
 * The Class MemoDAOMobileImpl.
 */
@Repository
public class MemoDAOMobileImpl extends BaseDAOMobile implements IMemoDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(MemoDAOMobileImpl.class);

    /** La constante GETLISTEMEMO. */
    public static final String GETLISTEMEMO = MemoDAOMobileImpl.class.getName() + ".GETLISTEMEMO";

    /** The Constant GETTERMINATIONREASON. */
    public static final String GETTERMINATIONREASON = MemoDAOMobileImpl.class.getName() + ".GETTERMINATIONREASON";

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.memo.IMemoDAO#findMemos(java.lang.String,
     * java.lang.String)
     */
    
    public List<MemoWS> findMemos(final String customerId, final String contractIdBscs) {
        LOG.debug("--> findMemos contractIdBsc {} customerId {}", contractIdBscs, customerId);
        List<MemoWS> listMemos = null;
        final String sql = CustomSQLUtil.get(GETLISTEMEMO);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("customerId", customerId);
        namedParameters.put("contractIdBscs", contractIdBscs);

        final CustomBeanPropertyRowMapper<MemoWS> memoRowMapper = new CustomBeanPropertyRowMapper<MemoWS>(MemoWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listMemos = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, memoRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- findMemos");
        return listMemos;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.memo.IMemoDAO#insertMemoWiam(ma.iam
     * .grc.bean.MemoWiamWS)
     */
    
    public Boolean insertMemoWiam(final MemoWiamWS memoWiamWS, final List<MemoAddInfoWS> listMemoAddInfos) {
        throw new NotImplementedException("Insertion memos non disponible pour le Mobile ");
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.memo.IMemoDAO#getTerminationReasons()
     */
    
    public List<TerminationReasonWS> getTerminationReasons() {
        LOG.debug("--> getTerminationReasons");
        
        final String sql = CustomSQLUtil.get(GETTERMINATIONREASON);
        
        final CustomBeanPropertyRowMapper<TerminationReasonWS> reasonRowMapper = new CustomBeanPropertyRowMapper<TerminationReasonWS>(TerminationReasonWS.class);
        
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        final List<TerminationReasonWS> listReasons = super.getNamedParameterJdbcTemplate().query(sql, new HashMap<String, String>(),
                reasonRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
        
        LOG.debug("<-- getTerminationReasons");
        return listReasons;
    }

}
