/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import ma.iam.pws.grc.bean.SmsWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import ma.iam.pws.grc.dao.sms.ISmsDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

// TODO: Auto-generated Javadoc
/**
 * The Class FeesManagerBiz.
 */
@Component(value = "smsManagerBiz")
public class SmsManagerBiz extends CommonManagerBiz {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SmsManagerBiz.class);

	/**
	 * Insert fees.
	 * 
	 * @param smsBean
	 *            the sms bean
	 * @param product
	 *            le produit
	 * @return the boolean
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, timeout = GRCProperties.TIMEOUT)
	public Boolean insertSms(final SmsWS smsBean, final int product)
			throws GrcWsException {
		LOG.info("-> insertSms: smsBean {}", smsBean);

		/*
		 * f.tatbi FC5864
		 */

		if (GRCStringUtil.isNullOrEmpty(smsBean.getApplication())) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"application doit etre renseigné");
		}

		if (GRCStringUtil.isNullOrEmpty(smsBean.getNd())) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"nd doit etre renseigné");
		}

//		if (!WSValidator.validateNumber(smsBean.getNpCode())) {
//			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
//					"npCode doit etre un nombre");
//		}
//		if (GRCStringUtil.isNullOrEmpty(smsBean.getNpShdes())) {
//			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
//					"NpShdes doit etre non vide  ");
//		}
//
//		if (smsBean.getNpShdes().length() > 5) {
//			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
//					"NpShdes ne doit pas depasser 5 caractéres ");
//
//		}
		if (GRCStringUtil.isNullOrEmpty(smsBean.getSmsText())) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"SmsText doit etre non vide  ");
		}

		if (!WSValidator.validateMobileNumber(smsBean.getNd())) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"nd doit etre numéro Mobile valide");
		}

		final ISmsDAO iSmsDAO = this.getSmsDAOForProduct(product);

		final Boolean result = iSmsDAO.insertSmsWS(smsBean);
		LOG.info("<- insertSms {}", result);
		return result;
	}
}
