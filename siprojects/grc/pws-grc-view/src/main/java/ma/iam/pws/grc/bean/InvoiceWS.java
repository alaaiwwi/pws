/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class InvoiceWS.
 */
public class InvoiceWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2181924681461358187L;

    /** The id. */
    private String idFacture;

    /** The ref facture. */
    private String refFacture;

    /** The statut. */
    private String statut;

    /** The type. */
    private String type;

    /** The date facture. */
    private String dateFacture;

    /** The motant facture. */
    private String motantFacture;

    /** The date echeance. */
    private String dateEcheance;

    /** The montant ouvert. */
    private String montantOuvert;

    /** The flag reclame. */
    private String flagReclame;

    /** The num document. */
    private String numDocument;

    /** The code segment. */
    private String codeSegment;

    /** The code organisme. */
    private String codeOrganisme;

    /** The customerId de BSCS */
    private String customerId;

    /** The periode. */
    private String periode;

    private String nd;

    private String montantRoaming;

    private String coId;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public String getIdFacture() {
        return idFacture;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setIdFacture(final String id) {
        this.idFacture = id;
    }

    /**
     * Gets the ref facture.
     * 
     * @return the ref facture
     */
    public String getRefFacture() {
        return refFacture;
    }

    /**
     * Sets the ref facture.
     * 
     * @param refFacture
     *            the new ref facture
     */
    public void setRefFacture(final String refFacture) {
        this.refFacture = refFacture;
    }

    /**
     * Gets the statut.
     * 
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * Sets the statut.
     * 
     * @param statut
     *            the new statut
     */
    public void setStatut(final String statut) {
        this.statut = statut;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the date facture.
     * 
     * @return the date facture
     */
    public String getDateFacture() {
        return dateFacture;
    }

    /**
     * Sets the date facture.
     * 
     * @param dateFacture
     *            the new date facture
     */
    public void setDateFacture(final String dateFacture) {
        this.dateFacture = dateFacture;
    }

    /**
     * Gets the motant facture.
     * 
     * @return the motant facture
     */
    public String getMotantFacture() {
        return motantFacture;
    }

    /**
     * Sets the motant facture.
     * 
     * @param motantFacture
     *            the new motant facture
     */
    public void setMotantFacture(final String motantFacture) {
        this.motantFacture = motantFacture;
    }

    /**
     * Gets the date echeance.
     * 
     * @return the date echeance
     */
    public String getDateEcheance() {
        return dateEcheance;
    }

    /**
     * Sets the date echeance.
     * 
     * @param dateEcheance
     *            the new date echeance
     */
    public void setDateEcheance(final String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    /**
     * Gets the montant ouvert.
     * 
     * @return the montant ouvert
     */
    public String getMontantOuvert() {
        return montantOuvert;
    }

    /**
     * Sets the montant ouvert.
     * 
     * @param montantOuvert
     *            the new montant ouvert
     */
    public void setMontantOuvert(final String montantOuvert) {
        this.montantOuvert = montantOuvert;
    }

    /**
     * Gets the flag reclame.
     * 
     * @return the flag reclame
     */
    public String getFlagReclame() {
        return flagReclame;
    }

    /**
     * Sets the flag reclame.
     * 
     * @param flagReclame
     *            the new flag reclame
     */
    public void setFlagReclame(final String flagReclame) {
        this.flagReclame = flagReclame;
    }

    /**
     * Gets the num document.
     * 
     * @return the num document
     */
    public String getNumDocument() {
        return numDocument;
    }

    /**
     * Sets the num document.
     * 
     * @param numDocument
     *            the new num document
     */
    public void setNumDocument(final String numDocument) {
        this.numDocument = numDocument;
    }

    /**
     * Gets the code segment.
     * 
     * @return the code segment
     */
    public String getCodeSegment() {
        return codeSegment;
    }

    /**
     * Sets the code segment.
     * 
     * @param codeSegment
     *            the new code segment
     */
    public void setCodeSegment(final String codeSegment) {
        this.codeSegment = codeSegment;
    }

    /**
     * Gets the code organisme.
     * 
     * @return the code organisme
     */
    public String getCodeOrganisme() {
        return codeOrganisme;
    }

    /**
     * Sets the code organisme.
     * 
     * @param codeOrganisme
     *            the new code organisme
     */
    public void setCodeOrganisme(final String codeOrganisme) {
        this.codeOrganisme = codeOrganisme;
    }

    /**
     * CUSTOMER_ID de BSCS
     * 
     * @return the cust acct id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * CUSTOMER_ID de BSCS
     * 
     * @param customerId
     *            CUSTOMER_ID de BSCS
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the periode.
     * 
     * @return the periode
     */
    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getNd() {
        return nd;
    }

    public void setNd(String nd) {
        this.nd = nd;
    }

    public String getMontantRoaming() {
        return montantRoaming;
    }

    public void setMontantRoaming(String montantRoaming) {
        this.montantRoaming = montantRoaming;
    }

    public String getCoId() {
        return coId;
    }

    public void setCoId(String coId) {
        this.coId = coId;
    }

}
