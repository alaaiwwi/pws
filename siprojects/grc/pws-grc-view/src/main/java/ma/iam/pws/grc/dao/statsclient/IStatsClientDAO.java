/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.statsclient;

import java.util.List;

import ma.iam.pws.grc.bean.ContractStatusWS;

/**
 * The Interface IStatsClientDAO.
 */
public interface IStatsClientDAO {

	/**
	 * Retourne le chiffre d'affaire du client sur une année glissante
	 * 
	 * @param customerId
	 *            the customer id
	 * @return the chiffre affaire
	 */
	/*
	 * f.tatbi FC 5864 update function
	 */
	double getChiffreAffaire(final String customerId);

	/**
	 * Retourne le chiffre d'affaire courant
	 * 
	 * @param customerId
	 *            the customer id
	 * @param customerIdHigh
	 *            l'identifiant client du pére (CUSTOMER_ID_HIGH)
	 * @return the chiffre affaire courant
	 */
	/*
	 * f.tatbi FC 5864
	 */
	double getChiffreAffaireCourant(final String customerId);

	/**
	 * Retourne les statuts de tous les contrats du client
	 * 
	 * @param customerId
	 *            the cust acct id
	 * @param customerIdHigh
	 *            l'identifiant client du pére (CUSTOMER_ID_HIGH)
	 * @return the contract statuses
	 */
	/*
	 * f.tatbi FC 5864
	 */
	List<ContractStatusWS> getContractStatuses(final String customerId);

	/**
	 * Retounr le nombre de factures en retard du client
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @return the factures retard
	 */
	int getFacturesRetard(final String customerId);

	/**
	 * Retourne l'ancienneté du contrat du client
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @return the int
	 */
	int ancienneteContrat(final String customerId);
	
	/*
	 * f.tatbi FC 5864 get category by customerId
	 */
	String getCategory(String customer_id);

}
