package ma.iam.pws.grc.dao;

import java.sql.Types;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class TicklerNumber extends StoredProcedure {

	private static final String proc_name = "SYSADM.NEXTFREE.GETVALUE";

	public TicklerNumber(DataSource datasource) {
		super(datasource, proc_name);
		declareParameter(new SqlParameter("in_id", Types.VARCHAR));
		declareParameter(new SqlOutParameter("out", Types.BIGINT));
		compile();
	}

	public Long getSequenceValue(String value) {
		Map<String, Object> results = super.execute(value);

		return (Long)results.get("out");

		
	}

}
