/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.List;

import ma.iam.pws.grc.bean.ContractStatusWS;
import ma.iam.pws.grc.bean.StatsClientWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import ma.iam.pws.grc.dao.statsclient.IStatsClientDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class StatManagerBiz.
 */
@Component(value = "statManagerBiz")
public class StatManagerBiz extends CommonManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(StatManagerBiz.class);

	/**
	 * Cherche les statistiques client.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param customerIdHigh
	 *            l'identifiant client du pére (CUSTOMER_ID_HIGH)
	 * @param categoryCd
	 *            le code de la catégorie du client
	 * @param product
	 *            le produit
	 * @return les statistiques client
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public StatsClientWS findStatsClient(final String customerId,
			final String customerIdHigh, final String categoryCd,
			final int product) throws GrcWsException {
		LOG.info(
				"-> findStatsClient: customerId {} customerIdHigh {} categoryCd "
						+ categoryCd, customerId, customerIdHigh);

		if (GRCStringUtil.isNullOrEmpty(customerId)
				|| !WSValidator.validateNumber(customerId)
				|| !GRCStringUtil.isNullOrEmpty(customerIdHigh)
				&& !WSValidator.validateNumber(customerIdHigh))

		{
			// Le champ custAcctId ne doit pas étre null
			// et le champs customerIdHigh s'il n'et pas nul doit étre correct
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétres incorrects customerId=" + customerId
							+ " customerIdHigh=" + customerIdHigh);
		}

		final StatsClientWS statsClient = new StatsClientWS();
		final IStatsClientDAO iStatsClientDAO = this
				.getStatsClientDAOForProduct(product);

		// Calcule de l'ancienneté du contrat
		final int ancienneteContrat = iStatsClientDAO
				.ancienneteContrat(customerId);
		statsClient.setAncienneteContrat(ancienneteContrat);

		// Calcul du chiffre d'affaire
		final double chiffreAffaire = iStatsClientDAO
				.getChiffreAffaire(customerId);
		statsClient.setChiffreAffaire(chiffreAffaire);

		// Calcul du chiffre d'affaire courant
		final double chiffreAffaireCourant = iStatsClientDAO
				.getChiffreAffaireCourant(customerId);
		statsClient.setChiffreAffaireCourant(chiffreAffaireCourant);

		// Recherche des status des contrats du client
		final List<ContractStatusWS> contractStatuses = iStatsClientDAO
				.getContractStatuses(customerId);
		statsClient.setContractStatuses(contractStatuses);

		// calcul du nombre de factures en retard
		final int facturesRetard = iStatsClientDAO
				.getFacturesRetard(customerId);
		statsClient.setFacturesRetard(facturesRetard);

		// Calcul du scoring
		statsClient.setScoring(calculScoringClient(facturesRetard,
				ancienneteContrat, chiffreAffaireCourant, categoryCd, product));

		LOG.info("<- findStatsClient statsClient {}", statsClient);
		return statsClient;
	}

	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public StatsClientWS findStatsClient(final String customerId,
			final int product) throws GrcWsException {
		LOG.info("-> findStatsClient: customerId {}    " + customerId);

		if (GRCStringUtil.isNullOrEmpty(customerId)
				|| !WSValidator.validateNumber(customerId))

		{
			// Le champ custAcctId ne doit pas étre null
			// et le champs customerIdHigh s'il n'et pas nul doit étre correct
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"paramétres incorrects customerId=" + customerId);
		}

		final StatsClientWS statsClient = new StatsClientWS();
		final IStatsClientDAO iStatsClientDAO = this
				.getStatsClientDAOForProduct(product);

		// Calcule de l'ancienneté du contrat
		final int ancienneteContrat = iStatsClientDAO
				.ancienneteContrat(customerId);
		statsClient.setAncienneteContrat(ancienneteContrat);

		// Calcul du chiffre d'affaire
		final double chiffreAffaire = iStatsClientDAO
				.getChiffreAffaire(customerId);
		statsClient.setChiffreAffaire(chiffreAffaire);

		// Calcul du chiffre d'affaire courant
		final double chiffreAffaireCourant = iStatsClientDAO
				.getChiffreAffaireCourant(customerId);
		statsClient.setChiffreAffaireCourant(chiffreAffaireCourant);

		// Recherche des status des contrats du client
		final List<ContractStatusWS> contractStatuses = iStatsClientDAO
				.getContractStatuses(customerId);
		statsClient.setContractStatuses(contractStatuses);

		// calcul du nombre de factures en retard
		final int facturesRetard = iStatsClientDAO
				.getFacturesRetard(customerId);
		statsClient.setFacturesRetard(facturesRetard);
		// getCategory

		String categorie = iStatsClientDAO.getCategory(customerId);
		// Calcul du scoring
		statsClient.setScoring(calculScoringClient(facturesRetard,
				ancienneteContrat, chiffreAffaireCourant, categorie, product));

		LOG.info("<- findStatsClient statsClient {}", statsClient);
		return statsClient;
	}

}
