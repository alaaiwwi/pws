/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class ContractHistoryWS.
 */
public class RejetADSLWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    /** The nd. */
    private String nd;

    /** The new nd. */
    private String newNd;

    /** The n commande. */
    private String nCommande;

    /** The debit. */
    private String debit;

    /** The debit n. */
    private String debitN;

    /** The debit d. */
    private String debitD;

    /** The debit i. */
    private String debitI;

    /** The date dem. */
    private String dateDem;

    /** The action. */
    private String action;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        sb = new StringBuilder();
        sb.append("[RejetADSLWS:");
        if (nCommande != null) {
            sb.append(",nCommande=").append(nCommande);
        }
        if (nd != null) {
            sb.append(",nd=").append(nd);
        }
        if (newNd != null) {
            sb.append(",newNd=").append(newNd);
        }
        if (debit != null) {
            sb.append(",debit=").append(debit);
        }
        if (debitN != null) {
            sb.append(",debitN=").append(debitN);
        }
        if (debitD != null) {
            sb.append(",debitD=").append(debitD);
        }
        if (debitI != null) {
            sb.append(",debitI=").append(debitI);
        }
        if (dateDem != null) {
            sb.append(",dateDem=").append(dateDem);
        }
        if (action != null) {
            sb.append(",action=").append(action);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets the nd.
     * 
     * @return the nd
     */
    public String getNd() {
        return nd;
    }

    /**
     * Sets the nd.
     * 
     * @param nd
     *            the new nd
     */
    public void setNd(final String nd) {
        this.nd = nd;
    }

    /**
     * Gets the new nd.
     * 
     * @return the new nd
     */
    public String getNewNd() {
        return newNd;
    }

    /**
     * Sets the new nd.
     * 
     * @param newNd
     *            the new new nd
     */
    public void setNewNd(final String newNd) {
        this.newNd = newNd;
    }

    /**
     * Gets the n commande.
     * 
     * @return the n commande
     */
    public String getnCommande() {
        return nCommande;
    }

    /**
     * Sets the n commande.
     * 
     * @param nCommande
     *            the new n commande
     */
    public void setnCommande(final String nCommande) {
        this.nCommande = nCommande;
    }

    /**
     * Gets the debit.
     * 
     * @return the debit
     */
    public String getDebit() {
        return debit;
    }

    /**
     * Sets the debit.
     * 
     * @param debit
     *            the new debit
     */
    public void setDebit(final String debit) {
        this.debit = debit;
    }

    /**
     * Gets the debit n.
     * 
     * @return the debit n
     */
    public String getDebitN() {
        return debitN;
    }

    /**
     * Sets the debit n.
     * 
     * @param debitN
     *            the new debit n
     */
    public void setDebitN(final String debitN) {
        this.debitN = debitN;
    }

    /**
     * Gets the debit d.
     * 
     * @return the debit d
     */
    public String getDebitD() {
        return debitD;
    }

    /**
     * Sets the debit d.
     * 
     * @param debitD
     *            the new debit d
     */
    public void setDebitD(final String debitD) {
        this.debitD = debitD;
    }

    /**
     * Gets the debit i.
     * 
     * @return the debit i
     */
    public String getDebitI() {
        return debitI;
    }

    /**
     * Sets the debit i.
     * 
     * @param debitI
     *            the new debit i
     */
    public void setDebitI(final String debitI) {
        this.debitI = debitI;
    }

    /**
     * Gets the date dem.
     * 
     * @return the date dem
     */
    public String getDateDem() {
        return dateDem;
    }

    /**
     * Sets the date dem.
     * 
     * @param dateDem
     *            the new date dem
     */
    public void setDateDem(final String dateDem) {
        this.dateDem = dateDem;
    }

    /**
     * Gets the action.
     * 
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the action.
     * 
     * @param action
     *            the new action
     */
    public void setAction(final String action) {
        this.action = action;
    }

}
