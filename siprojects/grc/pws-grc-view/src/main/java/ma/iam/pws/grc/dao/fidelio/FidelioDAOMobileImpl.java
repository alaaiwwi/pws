/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.fidelio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.FidelioAccountWS;
import ma.iam.pws.grc.bean.FidelioActionHistWS;
import ma.iam.pws.grc.bean.FidelioOrderDetailWS;
import ma.iam.pws.grc.bean.FidelioOrderHistWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

/**
 * The Class FidelioDAOMobileImpl.
 */
@Repository
public class FidelioDAOMobileImpl extends BaseDAOMobile implements IFidelioDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(FidelioDAOMobileImpl.class);

	/** La constante GETFIDELIOSTATUS. */
	public static final String GETFIDELIOSTATUS = FidelioDAOMobileImpl.class
			.getName() + ".GETFIDELIOSTATUS";

	/** La constante GETFIDELIACTHISTORY. */
	public static final String GETFIDELIOACTHISTORY = FidelioDAOMobileImpl.class
			.getName() + ".GETFIDELIOACTHISTORY";

	/** La constante GETFIDELIOORDHIST. */
	public static final String GETFIDELIOORDHIST = FidelioDAOMobileImpl.class
			.getName() + ".GETFIDELIOORDHIST";

	/** La constante GETFIDELIOORDDETAIL. */
	public static final String GETFIDELIOORDDETAIL = FidelioDAOMobileImpl.class
			.getName() + ".GETFIDELIOORDDETAIL";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioAccount(java.
	 * lang.String, int)
	 */

	public FidelioAccountWS findFidelioAccount(final String customer_id,
			final String custCode) {
		LOG.debug("--> findFidelioAccount custCode {}", custCode);
		FidelioAccountWS fidelioAccountWS = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETFIDELIOSTATUS));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("custCode", custCode);

		final CustomBeanPropertyRowMapper<FidelioAccountWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioAccountWS>(
				FidelioAccountWS.class);
		if (!GRCStringUtil.isNullOrEmpty(customer_id))
			sql.append(" AND cp.customer_id=" + customer_id);
		if (!GRCStringUtil.isNullOrEmpty(custCode))
			sql.append(" AND mv.custcode='" + custCode + "'");
		LOG.debug(GRCWSConstantes.EXEC_QUERY_FIDELIO, sql);
		final List<FidelioAccountWS> listFidelioAccountWS = super
				.getNamedParameterJdbcTemplateFidelio().query(sql.toString(),
						invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listFidelioAccountWS != null) && !listFidelioAccountWS.isEmpty()) {
			fidelioAccountWS = listFidelioAccountWS.get(0);
		}

		LOG.debug("<-- findFidelioAccount");
		return fidelioAccountWS;
	}

	public void setQualiteFidelio(final List<CustAcctWS> listCustAcctWS) {
		LOG.debug("--> setQualiteFidelio");

		for (final CustAcctWS customerWs : listCustAcctWS) {
			if ((customerWs != null) && (customerWs.getCustomerId() != null)) {
				final FidelioAccountWS compteFidelio = findFidelioAccount(
						customerWs.getCustomerId(), null);
				if (compteFidelio != null) {
					customerWs.getContract().setQualiteFidelio(
							compteFidelio.getQualiteFidelio());
				}
			}

			if ((customerWs != null) && (customerWs.getChildren() != null)
					&& !customerWs.getChildren().isEmpty()) {
				setQualiteFidelio(customerWs.getChildren());
			}
		}
		LOG.debug("<-- setQualiteFidelio");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioActionHist(java
	 * .lang.String, int)
	 */

	public List<FidelioActionHistWS> findFidelioActionHist(
			final String customer_id, final String custCode) {
		LOG.debug("--> findFidelioActionHist custCode {}", custCode);
		List<FidelioActionHistWS> listFidelioActionHistWS = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETFIDELIOACTHISTORY));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("custCode", custCode);

		final CustomBeanPropertyRowMapper<FidelioActionHistWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioActionHistWS>(
				FidelioActionHistWS.class);
		if (!GRCStringUtil.isNullOrEmpty(customer_id))
			sql.append(" AND cc.customer_id=" + customer_id);
		if (!GRCStringUtil.isNullOrEmpty(custCode))
			sql.append(" AND mv.custcode='" + custCode + "'");

		sql.append(" order by date_application desc");

		LOG.debug(GRCWSConstantes.EXEC_QUERY_FIDELIO, sql);
		listFidelioActionHistWS = super.getNamedParameterJdbcTemplateFidelio()
				.query(sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findFidelioActionHist");
		return listFidelioActionHistWS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioOrderHist(java
	 * .lang.String, int)
	 */

	public List<FidelioOrderHistWS> findFidelioOrderHist(
			final String customer_id, final String custCode) {
		LOG.debug("--> findFidelioOrderHist custCode {}", custCode);
		List<FidelioOrderHistWS> listFidelioOrderHistWS = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETFIDELIOORDHIST));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("custCode", custCode);

		final CustomBeanPropertyRowMapper<FidelioOrderHistWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioOrderHistWS>(
				FidelioOrderHistWS.class);

		if (!GRCStringUtil.isNullOrEmpty(customer_id))
			sql.append("AND cc.customer_id=" + customer_id);
		if (!GRCStringUtil.isNullOrEmpty(custCode))
			sql.append("AND mv.custcode='" + custCode + "'");

		sql.append("  order by af.id desc");
		
		LOG.debug(GRCWSConstantes.EXEC_QUERY_FIDELIO, sql);
		listFidelioOrderHistWS = super.getNamedParameterJdbcTemplateFidelio()
				.query(sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findFidelioOrderHist");
		return listFidelioOrderHistWS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioOrderDetail(java
	 * .lang.String, int)
	 */

	public List<FidelioOrderDetailWS> findFidelioOrderDetail(
			final String orderId) {
		LOG.debug("--> findFidelioOrderDetail orderId {}", orderId);
		List<FidelioOrderDetailWS> listFidelioOrderDetailWS = null;
		final String sql = CustomSQLUtil.get(GETFIDELIOORDDETAIL);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("orderId", orderId);

		final CustomBeanPropertyRowMapper<FidelioOrderDetailWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioOrderDetailWS>(
				FidelioOrderDetailWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY_FIDELIO, sql);
		listFidelioOrderDetailWS = super.getNamedParameterJdbcTemplateFidelio()
				.query(sql, namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findFidelioOrderDetail");
		return listFidelioOrderDetailWS;
	}

}
