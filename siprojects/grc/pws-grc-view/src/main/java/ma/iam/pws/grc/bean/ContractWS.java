/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import java.util.List;

import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ContractWS.
 */
public class ContractWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 1649628320022235202L;

    /** The contract id bscs. */
    private String contractIdBscs;

    /** The contract id neto. */
    private String contractIdNeto;

    /** Le contrat Fixe correspondant au client Internet. */
    private String contractIdFixe;

    /** The nd. */
    private String nd;

    /** The login. */
    private String loginInternet;

    /** The code confidentiel. */
    private String codeConfidentiel;

    /** The debit. */
    private String debit;

    /** The numero vo ip. */
    private String numeroVoIp;

    /**
     * Une liste de ND<br>
     * Dans le cas WS -> BIZ : cette liste contient la liste des ND associé au
     * contrat (mobile)<br>
     * Dans le cas BIZ -> WS : cette est utilisée pour la recherche du client é
     * partir du contact, elle contient le ND du contact comme premier element,
     * puis tous les customer_id des client liés é ce contact.
     */
    private List<String> listNd;

    /** The status. */
    private String status;

    /** The status desc. */
    private String statusDesc;

    /** The date status. */
    private String dateStatus;

    /** The rate plan. */
    private RatePlanWS ratePlan = new RatePlanWS();

    /** The ag. */
    private EntityWS ag = new EntityWS();

    /** The dr. */
    private EntityWS dr = new EntityWS();

    /** The dr. */
    private EntityWS dc = new EntityWS();

    /** The adress. */
    private String adressFacturation;

    /** The adress installation. */
    private String adressInstallation;

    /** The is gold. */
    private Integer isGold = 0;

    /** The qualite fidelio. */
    private String qualiteFidelio;

    /** La classe SLA du client :C0, C1, C2, C3. */
    private String classSla;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ContractWS:");
        sb.append(contractIdBscs);
        if (!GRCStringUtil.isNullOrEmpty(contractIdBscs)) {
            sb.append(",CO_ID_BSCS=").append(contractIdBscs);
        }
        if (!GRCStringUtil.isNullOrEmpty(contractIdNeto)) {
            sb.append(",CO_ID_NETO=").append(contractIdNeto);
        }
        if (!GRCStringUtil.isNullOrEmpty(contractIdFixe)) {
            sb.append(",CO_ID_FIXE=").append(contractIdFixe);
        }
        if (!GRCStringUtil.isNullOrEmpty(nd)) {
            sb.append(",nd=").append(nd);
        }
        if ((listNd != null) && !listNd.isEmpty()) {
            sb.append(",listNd=").append(listNd);
        }
        if ((!GRCStringUtil.isNullOrEmpty(status))) {
            sb.append(",status=").append(status);
        }
        if ((!GRCStringUtil.isNullOrEmpty(statusDesc))) {
            sb.append(",statusDesc=").append(statusDesc);
        }
        if ((!GRCStringUtil.isNullOrEmpty(adressFacturation))) {
            sb.append(",adressFacturation=").append(adressFacturation);
        }
        if ((!GRCStringUtil.isNullOrEmpty(adressInstallation))) {
            sb.append(",adressInstallation=").append(adressInstallation);
        }
        if ((!GRCStringUtil.isNullOrEmpty(loginInternet))) {
            sb.append(",loginInternet=").append(loginInternet);
        }
        if ((!GRCStringUtil.isNullOrEmpty(codeConfidentiel))) {
            sb.append(",codeConfidentiel=").append(codeConfidentiel);
        }
        if ((!GRCStringUtil.isNullOrEmpty(debit))) {
            sb.append(",debit=").append(debit);
        }
        if ((!GRCStringUtil.isNullOrEmpty(numeroVoIp))) {
            sb.append(",numeroVoIp=").append(numeroVoIp);
        }
        if (((ratePlan != null) && !ratePlan.isEmpty())) {
            sb.append(",ratePlan=").append(ratePlan.getRateplanId()).append(ratePlan.getRateplanName());
        }
        if (((ag != null) && !ag.isEmpty())) {
            sb.append(",ag=").append(ag.getEntityId()).append(" ").append(ag.getEntityName());
        }
        if (((dr != null) && !dr.isEmpty())) {
            sb.append(",dr=").append(dr.getEntityId()).append(" ").append(dr.getEntityName());
        }
        if (((dc != null) && !dc.isEmpty())) {
            sb.append(",dc=").append(dc.getEntityId()).append(" ").append(dc.getEntityName());
        }
        if (Integer.valueOf(1).equals(isGold)) {
            sb.append(",CLIENT GOLD");
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Get the ag.
     * 
     * @return the ag
     */
    public EntityWS getAg() {
        return ag;
    }

    /**
     * Get le contrat bscs du client (champ CO_ID).
     * 
     * @return le contrat bscs du client (champ CO_ID)
     */
    public String getContractIdBscs() {
        return contractIdBscs;
    }

    /**
     * Get l'identifiant du contrat netonomy correspondant au client.
     * 
     * @return l'identifiant du contrat netonomy correspondant au client
     */
    public String getContractIdNeto() {
        return contractIdNeto;
    }

    /**
     * Set l'identifiant du contrat netonomy correspondant au client.
     * 
     * @param contractIdNeto
     *            the new contract id neto
     */
    public void setContractIdNeto(final String contractIdNeto) {
        this.contractIdNeto = contractIdNeto;
    }

    /**
     * Get the date status.
     * 
     * @return the date status
     */
    public String getDateStatus() {
        return dateStatus;
    }

    /**
     * Get the dr.
     * 
     * @return the dr
     */
    public EntityWS getDr() {
        return dr;
    }

    /**
     * Get the nd.
     * 
     * @return the nd
     */
    public String getNd() {
        return nd;
    }

    /**
     * Get the rate plan.
     * 
     * @return the rate plan
     */
    public RatePlanWS getRatePlan() {
        return ratePlan;
    }

    /**
     * Get le statut.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Get le statut desc.
     * 
     * @return le statut desc
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * Set le statut desc.
     * 
     * @param statusDesc
     *            the new status desc
     */
    public void setStatusDesc(final String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * Set the ag.
     * 
     * @param ag
     *            the new ag
     */
    public void setAg(final EntityWS ag) {
        this.ag = ag;
    }

    /**
     * Set le contrat bscs du client (champ CO_ID).
     * 
     * @param contractIdBscs
     *            the new contract id bscs
     */
    public void setContractIdBscs(final String contractIdBscs) {
        this.contractIdBscs = contractIdBscs;
    }

    /**
     * Set the date status.
     * 
     * @param dateStatus
     *            the new date status
     */
    public void setDateStatus(final String dateStatus) {
        this.dateStatus = dateStatus;
    }

    /**
     * Set the dr.
     * 
     * @param dr
     *            the new dr
     */
    public void setDr(final EntityWS dr) {
        this.dr = dr;
    }

    /**
     * Set the nd.
     * 
     * @param nd
     *            the new nd
     */
    public void setNd(final String nd) {
        this.nd = nd;
    }

    /**
     * Set the rate plan.
     * 
     * @param ratePlan
     *            the new rate plan
     */
    public void setRatePlan(final RatePlanWS ratePlan) {
        this.ratePlan = ratePlan;
    }

    /**
     * Set le statut.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Sets the adress.
     * 
     * @param adressFacturation
     *            the new adress facturation
     */
    public void setAdressFacturation(final String adressFacturation) {
        this.adressFacturation = adressFacturation;
    }

    /**
     * Gets the adress.
     * 
     * @return the adress
     */
    public String getAdressFacturation() {
        return adressFacturation;
    }

    /**
     * Gets the contract id fixe.
     * 
     * @return the contract id fixe
     */
    public String getContractIdFixe() {
        return contractIdFixe;
    }

    /**
     * Sets the contract id fixe.
     * 
     * @param contractIdFixe
     *            the new contract id fixe
     */
    public void setContractIdFixe(final String contractIdFixe) {
        this.contractIdFixe = contractIdFixe;
    }

    /**
     * Gets the dc.
     * 
     * @return the dc
     */
    public EntityWS getDc() {
        return dc;
    }

    /**
     * Sets the dc.
     * 
     * @param dc
     *            the new dc
     */
    public void setDc(final EntityWS dc) {
        this.dc = dc;
    }

    /**
     * Gets the adress installation.
     * 
     * @return the adress installation
     */
    public String getAdressInstallation() {
        return adressInstallation;
    }

    /**
     * Sets the adress installation.
     * 
     * @param adressInstallation
     *            the new adress installation
     */
    public void setAdressInstallation(final String adressInstallation) {
        this.adressInstallation = adressInstallation;
    }

    /**
     * Gets the checks if is gold.
     * 
     * @return the checks if is gold
     */
    public Integer getIsGold() {
        return isGold;
    }

    /**
     * Sets the checks if is gold.
     * 
     * @param isGold
     *            the new checks if is gold
     */
    public void setIsGold(final Integer isGold) {
        this.isGold = isGold;
    }

    /**
     * Une liste de ND<br>
     * Dans le cas WS -> BIZ : cette liste contient la liste des ND associé au
     * contrat (mobile)<br>
     * Dans le cas BIZ -> WS : cette est utilisée pour la recherche du client é
     * partir du contact, elle contient le ND du contact comme premier element,
     * puis tous les customer_id des client liés é ce contact.
     * 
     * @return the list nd
     */
    public List<String> getListNd() {
        return listNd;
    }

    /**
     * Une liste de ND<br>
     * Dans le cas WS -> BIZ : cette liste contient la liste des ND associé au
     * contrat (mobile)<br>
     * Dans le cas BIZ -> WS : cette est utilisée pour la recherche du client é
     * partir du contact, elle contient le ND du contact comme premier element,
     * puis tous les customer_id des client liés é ce contact.
     * 
     * @param listNd
     *            the new list nd
     */
    public void setListNd(final List<String> listNd) {
        this.listNd = listNd;
    }

    /**
     * Gets the login internet.
     * 
     * @return the login internet
     */
    public String getLoginInternet() {
        return loginInternet;
    }

    /**
     * Sets the login internet.
     * 
     * @param login
     *            the new login internet
     */
    public void setLoginInternet(final String login) {
        loginInternet = login;
    }

    /**
     * Gets the code confidentiel.
     * 
     * @return the code confidentiel
     */
    public final String getCodeConfidentiel() {
        return codeConfidentiel;
    }

    /**
     * Sets the code confidentiel.
     * 
     * @param codeConfidentiel
     *            the new code confidentiel
     */
    public final void setCodeConfidentiel(final String codeConfidentiel) {
        this.codeConfidentiel = codeConfidentiel;
    }

    /**
     * Gets the debit.
     * 
     * @return the debit
     */
    public final String getDebit() {
        return debit;
    }

    /**
     * Sets the debit.
     * 
     * @param debit
     *            the new debit
     */
    public final void setDebit(final String debit) {
        this.debit = debit;
    }

    /**
     * Gets the numero vo ip.
     * 
     * @return the numero vo ip
     */
    public final String getNumeroVoIp() {
        return numeroVoIp;
    }

    /**
     * Sets the numero vo ip.
     * 
     * @param numeroVoIp
     *            the new numero vo ip
     */
    public final void setNumeroVoIp(final String numeroVoIp) {
        this.numeroVoIp = numeroVoIp;
    }

    /**
     * La classe SLA du client :C0, C1, C2, C3.
     * 
     * @return the classSla
     */
    public String getClassSla() {
        return classSla;
    }

    /**
     * La classe SLA du client :C0, C1, C2, C3.
     * 
     * @param classSla
     *            the classSla to set
     */
    public void setClassSla(final String classSla) {
        this.classSla = classSla;
    }

    /**
     * Gets the qualite fidelio.
     * 
     * @return the qualite fidelio
     */
    public String getQualiteFidelio() {
        return qualiteFidelio;
    }

    /**
     * Sets the qualite fidelio.
     * 
     * @param qualiteFidelio
     *            the new qualite fidelio
     */
    public void setQualiteFidelio(final String qualiteFidelio) {
        this.qualiteFidelio = qualiteFidelio;
    }
}
