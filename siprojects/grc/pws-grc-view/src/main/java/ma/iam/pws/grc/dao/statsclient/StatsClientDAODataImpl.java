/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.statsclient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.ContractStatusWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

/**
 * The Class StatsClientDAOMobileImpl.
 */
@Repository
public class StatsClientDAODataImpl extends BaseDAOFixe implements IStatsClientDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(StatsClientDAODataImpl.class);

    /** La constante GETCHIFFREAFFAIRE. */
    public static final String GETCHIFFREAFFAIRE = StatsClientDAODataImpl.class.getName() + ".GETCHIFFREAFFAIRE";

    /** La constante GETSTATUTCONTRAT. */
    public static final String GETSTATUTCONTRAT = StatsClientDAODataImpl.class.getName() + ".GETSTATUTCONTRAT";

    /** La constante GETFACTURESRETARD. */
    public static final String GETFACTURESRETARD = StatsClientDAODataImpl.class.getName() + ".GETFACTURESRETARD";

    /** La constante GETANCIENNETECONTRAT. */
    public static final String GETANCIENNETECONTRAT = StatsClientDAODataImpl.class.getName() + ".GETANCIENNETECONTRAT";

    /** La constante GETCHIFFREAFFAIRECOURANT. */
    public static final String GETCHIFFREAFFAIRECOURANT = StatsClientDAODataImpl.class.getName() + ".GETCHIFFREAFFAIRECOURANT";

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.statsclient.IStatsClientDAO#getChiffreAffaire
     * (int)
     */
    
    public double getChiffreAffaire(final String customerId, final String customerIdHigh) {
        LOG.debug("--> getChiffreAffaire customerId {} customerIdHigh {}", customerId, customerIdHigh);
        double result = 0;
        final String sql = CustomSQLUtil.get(GETCHIFFREAFFAIRE);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("customerIdHigh", customerIdHigh);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        result = super.getNamedParameterJdbcTemplate().queryForLong(sql, namedParameters);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);

        LOG.debug("<-- getChiffreAffaire");
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.statsclient.IStatsClientDAO#getChiffreAffaireCourant
     * (int)
     */
    
    public double getChiffreAffaireCourant(final String customerId, final String customerIdHigh) {
        LOG.debug("--> getChiffreAffaireCourant customerId {} customerIdHigh {}", customerId, customerIdHigh);
        double result = 0;
        final String sql = CustomSQLUtil.get(GETCHIFFREAFFAIRECOURANT);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("customerIdHigh", customerIdHigh);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        result = super.getNamedParameterJdbcTemplate().queryForLong(sql, namedParameters);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);

        LOG.debug("<-- getChiffreAffaireCourant");
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.statsclient.IStatsClientDAO#getContractStatuses
     * (int)
     */
    
    public List<ContractStatusWS> getContractStatuses(final String customerId, final String customerIdHigh) {
        LOG.debug("--> getContractStatuses customerId {} customerIdHigh {}", customerId, customerIdHigh);
        List<ContractStatusWS> listContractStatus = null;
        final String sql = CustomSQLUtil.get(GETSTATUTCONTRAT);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("customerIdHigh", customerIdHigh);

        final CustomBeanPropertyRowMapper<ContractStatusWS> statusesRowMapper = new CustomBeanPropertyRowMapper<ContractStatusWS>(ContractStatusWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listContractStatus = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, statusesRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- getContractStatuses");
        return listContractStatus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.statsclient.IStatsClientDAO#getFacturesRetard
     * (int)
     */
    
    public int getFacturesRetard(final String customerId) {
        LOG.debug("--> getFacturesRetard customerId {}", customerId);
        int result = 0;
        final String sql = CustomSQLUtil.get(GETFACTURESRETARD);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put(GRCWSConstantes.CUSTOMER_ID, customerId);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        result = super.getNamedParameterJdbcTemplate().queryForInt(sql, namedParameters);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);

        LOG.debug("<-- getFacturesRetard");
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.statsclient.IStatsClientDAO#ancienneteContrat
     * (int)
     */
    
    public int ancienneteContrat(final String customerId) {
        LOG.debug("--> ancienneteContrat customerId {}", customerId);
        int result = 0;
        final String sql = CustomSQLUtil.get(GETANCIENNETECONTRAT);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put(GRCWSConstantes.CUSTOMER_ID, customerId);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        try {
            result = super.getNamedParameterJdbcTemplate().queryForInt(sql, namedParameters);
        } catch (final DataIntegrityViolationException e) {
            result = 0;
        }
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);

        LOG.debug("<-- ancienneteContrat");
        return result;
    }

	public double getChiffreAffaire(String customerId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public double getChiffreAffaireCourant(String customerId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public List<ContractStatusWS> getContractStatuses(String customerId) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCategory(String customerId) {

		LOG.debug("--> getCategory customerId {}", customerId);
		String result = null;
		final String sql = CustomSQLUtil.get(StatsClientDAOMobileImpl.GETCATEGORY_BY_ID);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID, customerId);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		try {
			result = super.getNamedParameterJdbcTemplate().queryForObject(sql,
					namedParameters, String.class);
		} catch (final DataIntegrityViolationException e) {
			result = null;
		}
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);
		LOG.debug("<-- getCategory");
		return result;
	}
}
