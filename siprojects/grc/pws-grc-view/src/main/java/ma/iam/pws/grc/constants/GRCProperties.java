/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * The Interface GRCProperties.
 */
public interface GRCProperties {

    /** La constante TIMEOUT. */
    int TIMEOUT = 120;
}
