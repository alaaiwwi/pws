/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * Ce bean englobe les informations nécessaires pour la recherche des Factures:<br>
 * - customerId : CUSTOMER_ID du client<br>
 * - interval : intervalle de recherche, avec deux attribus startDate et endDate
 */
public class SearchInvoiceBean {

	/** CUSTOMER_ID dans BSCS */
	private String customerId;

	/** Intervalle de recherche contenant la date de début et la date de fin */
	private IntervalWS interval;

	/** STATUT **/

	private String statut;

	/**
	 * Instantiates a new search invoice bean.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 */
	public SearchInvoiceBean(final String customerId, final IntervalWS interval) {
		this.customerId = customerId;
		this.interval = interval;
	}

	public SearchInvoiceBean(final String customerId,
			final IntervalWS interval, final String statut) {
		this.customerId = customerId;
		this.interval = interval;
		this.statut = statut;
	}

	/**
	 * Gets l'identifiant client BSCS (CUSTOMER_ID).
	 * 
	 * @return l'identifiant client BSCS (CUSTOMER_ID)
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Gets l'interval contenant la date de début et de fin de la période de
	 * recherche.
	 * 
	 * @return l'interval contenant la date de début et de fin de la période de
	 *         recherche
	 */
	public IntervalWS getInterval() {
		return interval;
	}

	/**
	 * Sets l'identifiant client BSCS (CUSTOMER_ID).
	 * 
	 * @param customerId
	 *            the new cust acct id
	 */
	public void setCustomerId(final String customerId) {
		this.customerId = customerId;
	}

	/**
	 * Sets l'interval contenant la date de début et de fin de la période de
	 * recherche.
	 * 
	 * @param interval
	 *            the new interval
	 */
	public void setInterval(final IntervalWS interval) {
		this.interval = interval;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

}
