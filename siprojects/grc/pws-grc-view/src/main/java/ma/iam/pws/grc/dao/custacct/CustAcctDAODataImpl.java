/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.custacct;

import java.util.List;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.dao.CommonDAOFixe;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

// TODO: Auto-generated Javadoc
/**
 * The Class CustAcctDAOMobileImpl.
 */
@Repository
@Deprecated
public class CustAcctDAODataImpl extends CommonDAOFixe implements ICustAcctDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(CustAcctDAODataImpl.class);

    /**
     * Gets the appended sql.
     * 
     * @param ccu
     *            the ccu
     * @param commande
     *            the commande
     * @return the appended sql
     * @throws GrcWsException
     *             the grc ws exception
     */
    private String getAppendedSQL(final CustAcctWS searchCustAcctBean, final int product) throws GrcWsException {
        final String appendSql = getAppendedSQLByCommande(searchCustAcctBean.getCommande(), product);

        return appendSql;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findContractsForCustomer
     * (fr.capgemini.iam.grc.bean.CustAcctWS)
     */
    
    public List<CustAcctWS> findContractsForCustomer(final CustAcctWS searchCustAcctBean, final Boolean allowVIP, final int product) throws GrcWsException {
        LOG.debug("--> findContractsForCustomer allowVIP {} searchCustAcctBean {}", allowVIP, searchCustAcctBean);

        // Pas pour la DATA
        // normaliserND(searchCustAcctBean);

        final String appendSql = getAppendedSQL(searchCustAcctBean, product);

        final List<CustAcctWS> listCustAcct = getListCustAcctWS(appendSql, searchCustAcctBean, allowVIP, product);

        LOG.debug("<-- findContractsForCustomer");
        return listCustAcct;
    }

}
