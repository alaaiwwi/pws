/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CreditMobileWS;
import ma.iam.pws.grc.bean.CugWS;
import ma.iam.pws.grc.bean.InfoDataWS;
import ma.iam.pws.grc.bean.ServiceCreditWS;
import ma.iam.pws.grc.bean.ServiceDnsWS;
import ma.iam.pws.grc.bean.ServiceHistoryWS;
import ma.iam.pws.grc.bean.ServiceParamWS;
import ma.iam.pws.grc.bean.ServiceWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceDAOMobileImpl.
 */
@Repository
public class ServiceDAOMobileImpl extends BaseDAOMobile implements IServiceDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ServiceDAOMobileImpl.class);

	/** La constante GETLISTESERVICES. */
	public static final String GETLISTESERVICES = ServiceDAOMobileImpl.class
			.getName() + ".GETLISTESERVICES";

	/** The Constant GETLISTESERVICESCREDIT. */
	public static final String GETLISTESERVICESCREDIT = ServiceDAOMobileImpl.class
			.getName() + ".GETLISTESERVICESCREDIT";

	/** The Constant GETCREDITMOBILE. */
	public static final String GETCREDITMOBILE = ServiceDAOMobileImpl.class
			.getName() + ".GETCREDITMOBILE";

	/** La constante GETLISTESERVICEHIST. */
	public static final String GETLISTESERVICEHIST = ServiceDAOMobileImpl.class
			.getName() + ".GETLISTESERVICEHIST";

	/** La constante GETLISTESERVICEPARAM. (méme requete que le fixe) */
	public static final String GETLISTESERVICEPARAM = ServiceDAOFixeImpl.class
			.getName() + ".GETLISTESERVICEPARAM";

	/** The Constant GET_HLR_BY_ND. */
	public static final String GET_HLR_BY_ND = ServiceDAOMobileImpl.class
			.getName() + ".GET_HLR_BY_ND";

	/** The Constant GET_ALL_HLR. */
	public static final String GET_ALL_HLR = ServiceDAOMobileImpl.class
			.getName() + ".GET_ALL_HLR";

	/** The Constant GETCUG. */
	public static final String GETCUG = ServiceDAOMobileImpl.class.getName()
			+ ".GETCUG";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServices(java.lang.String
	 * )
	 */

	public List<ServiceWS> findServices(final String contractIdBscs) {
		LOG.debug("--> findServices contractIdBsc {}", contractIdBscs);
		List<ServiceWS> listServices = null;
		final String sql = CustomSQLUtil.get(GETLISTESERVICES);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);

		final CustomBeanPropertyRowMapper<ServiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceWS>(
				ServiceWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listServices = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findServices");
		return listServices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServicesDns(java.lang
	 * .String)
	 */

	public List<ServiceDnsWS> findServicesDns(final String contractIdBscs) {
		LOG.debug("--> findServicesDns contractIdBsc {}", contractIdBscs);
		throw new NotImplementedException(
				"Service DNS non disponible pour le produit MOBILE");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServiceHistory(java.
	 * lang.String, java.lang.String)
	 */

	public List<ServiceHistoryWS> findServiceHistory(
			final String contractIdBscs, final String shdes) {
		LOG.debug("--> findServiceHistory contractIdBsc {} shdes {}",
				contractIdBscs, shdes);
		List<ServiceHistoryWS> listServicesHist = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETLISTESERVICEHIST));

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);
		namedParameters.put("shdes", shdes);
		sql.append("   AND psh.CO_ID = " + contractIdBscs);
		if (shdes != null && !"".equals(shdes)) {
			sql.append(" AND SN.SHDES= '" + shdes + "'");
		}
		sql.append(" ORDER BY VALID_FROM_DATE DESC ");
		final CustomBeanPropertyRowMapper<ServiceHistoryWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceHistoryWS>(
				ServiceHistoryWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listServicesHist = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findServiceHistory");
		return listServicesHist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServiceParams(java.lang
	 * .String)
	 */

//	public List<ServiceParamWS> findServiceParams(final String paramId,
//			final String contractIdNeto) {
//		LOG.debug("--> findServiceParams paramId {}", paramId);
//		List<ServiceParamWS> listServicesParams = null;
//		final String sql = CustomSQLUtil.get(GETLISTESERVICEPARAM);
//
//		final Map<String, String> namedParameters = new HashMap<String, String>();
//		namedParameters.put("paramId", paramId);
//		final CustomBeanPropertyRowMapper<ServiceParamWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceParamWS>(
//				ServiceParamWS.class);
//
//		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
//		listServicesParams = super.getNamedParameterJdbcTemplate().query(sql,
//				namedParameters, invoiceRowMapper);
//		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
//
//		LOG.debug("<-- findServiceParams");
//		return listServicesParams;
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findRdbParams(java.lang.
	 * String)
	 */
	// // hors perimetre AtoS
	//
	// public RdbWS findRdbParams(final String nd) {
	// LOG.debug("--> findRdbParams nd {}", nd);
	// throw new
	// NotImplementedException("Service RDB non disponible pour le produit MOBILE");
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findInfoData(java.lang.String
	 * , java.lang.String)
	 */

	public InfoDataWS findInfoData(final String contractIdBscs,
			final String contractIdNeto) {
		LOG.debug("--> findInfoData contractIdBsc {} contractIdNeto {}",
				contractIdBscs, contractIdNeto);
		throw new NotImplementedException(
				"Service INFO DATA non disponible pour le produit MOBILE");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.service.IServiceDAO#findAllHlr()
	 */

	public List<String> findAllHlr() {
		LOG.debug("--> findAllHlr {}");
		final String sql = CustomSQLUtil.get(GET_ALL_HLR);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		final List<String> listHlr = super.getNamedParameterJdbcTemplate()
				.queryForList(sql, namedParameters, String.class);
		LOG.debug("<-- findAllHlr {}");
		return listHlr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCustomerHlr(java.lang
	 * .String)
	 */

	public String findCustomerHlr(final String nd) {
		LOG.debug("--> findCustomerHlr {}", nd);
		String value = null;
		try {
			final String sql = CustomSQLUtil.get(GET_HLR_BY_ND);
			final Map<String, String> namedParameters = new HashMap<String, String>();
			namedParameters.put("nd", nd);
			value = super.getNamedParameterJdbcTemplate().queryForObject(sql,
					namedParameters, String.class);
		} catch (final EmptyResultDataAccessException e) {
			value = null;
		}
		LOG.debug("<-- findCustomerHlr {}", value);
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServicesCredit(java.
	 * lang.String)
	 */

	public List<ServiceCreditWS> findServicesCredit(final String contractIdBscs) {
		LOG.debug("--> findServicesCredit contractIdBsc {}", contractIdBscs);
		List<ServiceCreditWS> listServices = null;
		final String sql = CustomSQLUtil.get(GETLISTESERVICESCREDIT);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);

		final CustomBeanPropertyRowMapper<ServiceCreditWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceCreditWS>(
				ServiceCreditWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listServices = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findServicesCredit");
		return listServices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCreditMobileParams(java
	 * .lang.String)
	 */

	public CreditMobileWS findCreditMobileParams(final String contractIdBscs) {
		LOG.debug("--> findCreditMobileParams contractIdBscs {}",
				contractIdBscs);

		List<CreditMobileWS> listCreditMobile = null;
		CreditMobileWS creditMobile = null;
		final String sql = CustomSQLUtil.get(GETCREDITMOBILE);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);
		final CustomBeanPropertyRowMapper<CreditMobileWS> rdbRowMapper = new CustomBeanPropertyRowMapper<CreditMobileWS>(
				CreditMobileWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listCreditMobile = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, rdbRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listCreditMobile != null) && !listCreditMobile.isEmpty()) {
			creditMobile = listCreditMobile.get(0);
		}

		LOG.debug("<-- findCreditMobileParams");
		return creditMobile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCug(java.lang.String)
	 */

	public List<CugWS> findCug(final String contractIdBscs) {
		LOG.debug("--> findCug contractIdBsc {}", contractIdBscs);
		final String sql = CustomSQLUtil.get(GETCUG);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);

		final CustomBeanPropertyRowMapper<CugWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<CugWS>(
				CugWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		final List<CugWS> listServices = super.getNamedParameterJdbcTemplate()
				.query(sql, namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findCug");
		return listServices;
	}

	public String getSlaClass(final String contractIdBscs) {
		throw new NotImplementedException(
				"Service SLA non disponible pour le produit MOBILE");
	}
	
	
	public List<ServiceParamWS> findServiceParams(final String shdes,
			final String contractIdBscs) {
		LOG.debug("--> findServiceParams shdes {}", shdes);
		String sql = CustomSQLUtil.get(ServiceDAOInternetImpl.GETLISTESERVICEPARAM_WITHOUT_SHDES);

		final Map<String, String> namedParameters = new HashMap<String, String>();

		namedParameters.put("contractIdBscs", contractIdBscs);
		if (shdes != null && !"".equals(shdes)) {
			namedParameters.put("shdes", shdes);
			sql = CustomSQLUtil.get(ServiceDAOInternetImpl.GETLISTESERVICEPARAM_WITH_SHDES);

		}

		final CustomBeanPropertyRowMapper<ServiceParamWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceParamWS>(
				ServiceParamWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY_END, sql);
		final List<ServiceParamWS> listServicesParams = super
				.getNamedParameterJdbcTemplate().query(sql, namedParameters,
						invoiceRowMapper);

		LOG.debug("<-- findServiceParams");
		return listServicesParams;
	}

}
