/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class FidelioOrderHistWS.
 */
public class FidelioOrderHistWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 1490970332125384238L;

    /** The order id. */
    private String orderId;

    /** The entry date. */
    private String entryDate;

    /** The agence code. */
    private String agenceCode;

    /** The description. */
    private String description;

    /** The total point price. */
    private String totalPointPrice;

    /**
     * Gets the order id.
     * 
     * @return the order id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the order id.
     * 
     * @param orderId
     *            the new order id
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets the entry date.
     * 
     * @return the entry date
     */
    public String getEntryDate() {
        return entryDate;
    }

    /**
     * Sets the entry date.
     * 
     * @param entryDate
     *            the new entry date
     */
    public void setEntryDate(final String entryDate) {
        this.entryDate = entryDate;
    }

    /**
     * Gets the agence code.
     * 
     * @return the agence code
     */
    public String getAgenceCode() {
        return agenceCode;
    }

    /**
     * Sets the agence code.
     * 
     * @param agenceCode
     *            the new agence code
     */
    public void setAgenceCode(final String agenceCode) {
        this.agenceCode = agenceCode;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the total point price.
     * 
     * @return the total point price
     */
    public String getTotalPointPrice() {
        return totalPointPrice;
    }

    /**
     * Sets the total point price.
     * 
     * @param totalPointPrice
     *            the new total point price
     */
    public void setTotalPointPrice(final String totalPointPrice) {
        this.totalPointPrice = totalPointPrice;
    }
}
