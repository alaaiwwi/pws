/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pws.grc.business.CommonManagerBiz;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

/**
 * The Class CustAcctWS.
 */
public class CustAcctWS extends GrcWS {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4655092732493887266L;

    /** The cust acct category. */
    private CustAcctCategoryWS custAcctCategory = new CustAcctCategoryWS();

    /** The cust acct s category. */
    private CustAcctCategoryWS custAcctSCategory = new CustAcctCategoryWS();

    /** The contract. */
    private ContractWS contract = new ContractWS();

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The company name. */
    private String companyName;

    /** The ccu. */
    private String ccu;

    /** The identity number. */
    private String identityNumber;

    /** The cust acct code. */
    private String custAcctCode;

    /** The cust acct code high. */
    private String custAcctCodeHigh;

    /** The product. */
    private String product;

    /** The status. */
    private String status;

    /** The city. */
    private String city;

    /** The mail. */
    private String mail;

    /** The ip. */
    private String ip;

    /** The os. */
    private String os;

    /** The equipment. */
    private String equipment;

    /** The carte sim. */
    private String carteSim;

    /** The site. */
    private String site;

    /** The niveau. */
    private String niveau;

    /** The commande. */
    private String commande;

    /** The customer id. */
    private String customerId;

    /** The customer id high. */
    private String customerIdHigh;

    /** The customer id root. */
    private String customerIdRoot;

    /** The port status. */
    private String portStatus;

    /** The customer id fixe. */
    private String customerIdFixe;

    /** The is internet. */
    private Integer isInternet;

    /** The children. */
    private List<CustAcctWS> children;

    /** The dns. */
    private String dns;

    /** The tab level. */
    public int tabLevel = 0;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final StringBuilder tabs = new StringBuilder();
        for (int i = 0; i < tabLevel; i++) {
            tabs.append('\t');
        }
        sb.append(tabs);
        sb.append("[CustAcctWS:");
        if (!GRCStringUtil.isNullOrEmpty(firstName)) {
            sb.append(",Prénom=").append(firstName);
        }
        if (!GRCStringUtil.isNullOrEmpty(lastName)) {
            sb.append(",Nom=").append(lastName);
        }
        if (!GRCStringUtil.isNullOrEmpty(custAcctCode)) {
            sb.append(",CUST_CODE=").append(custAcctCode);
        }
        if (!GRCStringUtil.isNullOrEmpty(customerId)) {
            sb.append(",CUSTOMER_ID=").append(customerId);
        }
        if (!GRCStringUtil.isNullOrEmpty(customerIdHigh)) {
            sb.append(",CUSTOMER_ID_HIGH=").append(customerIdHigh);
        }
        if (!GRCStringUtil.isNullOrEmpty(customerIdFixe)) {
            sb.append(",CUSTOMER_ID_FIXE=").append(customerIdFixe);
        }
        if (!GRCStringUtil.isNullOrEmpty(companyName)) {
            sb.append(",companyName=").append(companyName);
        }
        if ((custAcctCategory != null) && (custAcctCategory.getCategoryCd() != null)) {
            sb.append(",custAcctCategory=").append(custAcctCategory.getCategoryCd()).append(" ").append(custAcctCategory.getCategoryDesc());
        }
        if ((custAcctSCategory != null) && (custAcctSCategory.getCategoryCd() != null)) {
            sb.append(",custAcctSCategory=").append(custAcctSCategory.getCategoryCd()).append(" ").append(custAcctSCategory.getCategoryDesc());
        }
        if (!GRCStringUtil.isNullOrEmpty(ccu)) {
            sb.append(",ccu=").append(ccu);
        }
        if (!GRCStringUtil.isNullOrEmpty(identityNumber)) {
            sb.append(",PI=").append(identityNumber);
        }
        if (!GRCStringUtil.isNullOrEmpty(product)) {
            sb.append(",product=").append(product);
        }
        if (!GRCStringUtil.isNullOrEmpty(status)) {
            sb.append(",status=").append(status);
        }
        if (!GRCStringUtil.isNullOrEmpty(city)) {
            sb.append(",city=").append(city);
        }
        if (!GRCStringUtil.isNullOrEmpty(mail)) {
            sb.append(",mail=").append(mail);
        }
        if (!GRCStringUtil.isNullOrEmpty(ip)) {
            sb.append(",ip=").append(ip);
        }
        if (!GRCStringUtil.isNullOrEmpty(os)) {
            sb.append(",os=").append(os);
        }
        if (!GRCStringUtil.isNullOrEmpty(dns)) {
            sb.append(",dns=").append(dns);
        }
        if (Integer.valueOf(1).equals(isInternet)) {
            sb.append(",CLIENT INTERNET");
        }
        if (contract != null) {
            sb.append(contract);
        }
        if (children != null) {
            for (final CustAcctWS child : children) {
                if (child != null) {
                    child.tabLevel = tabLevel + 1;
                    sb.append('\n').append('\t').append(child);
                }
            }

        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Get the site.
     * 
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * Set the site.
     * 
     * @param site
     *            the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }

    /**
     * Get the niveau.
     * 
     * @return the niveau
     */
    public String getNiveau() {
        return niveau;
    }

    /**
     * Set the niveau.
     * 
     * @param niveau
     *            the new niveau
     */
    public void setNiveau(final String niveau) {
        this.niveau = niveau;
    }

    /**
     * Get the carte sim.
     * 
     * @return the carte sim
     */
    public String getCarteSim() {
        return carteSim;
    }

    /**
     * Set the carte sim.
     * 
     * @param carteSim
     *            the new carte sim
     */
    public void setCarteSim(final String carteSim) {
        this.carteSim = carteSim;
    }

    /**
     * Get the commande.
     * 
     * @return the commande
     */
    public String getCommande() {
        return commande;
    }

    /**
     * Set the commande.
     * 
     * @param commande
     *            the new commande
     */
    public void setCommande(final String commande) {
        this.commande = commande;
    }

    /**
     * Get the children.
     * 
     * @return the children
     */
    public List<CustAcctWS> getChildren() {
        if (children == null) {
            children = new ArrayList<CustAcctWS>();
        }
        return children;
    }

    /**
     * Set the children.
     * 
     * @param children
     *            the new children
     */
    public void setChildren(final List<CustAcctWS> children) {
        this.children = children;
    }

    /**
     * Get the ccu.
     * 
     * @return the ccu
     */
    public String getCcu() {
        return ccu;
    }

    /**
     * Get the city.
     * 
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Get the company name.
     * 
     * @return the company name
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Get the contract.
     * 
     * @return the contract
     */
    public ContractWS getContract() {
        return contract;
    }

    /**
     * Get the cust acct category.
     * 
     * @return the cust acct category
     */
    public CustAcctCategoryWS getCustAcctCategory() {
        return custAcctCategory;
    }

    /**
     * Get the cust acct code.
     * 
     * @return the cust acct code
     */
    public String getCustAcctCode() {
        return custAcctCode;
    }

    /**
     * Get the first name.
     * 
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get the identity number.
     * 
     * @return the identity number
     */
    public String getIdentityNumber() {
        return identityNumber;
    }

    /**
     * Get the last name.
     * 
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Get the product.
     * 
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Get the status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the ccu.
     * 
     * @param ccu
     *            the new ccu
     */
    public void setCcu(final String ccu) {
        this.ccu = ccu;
    }

    /**
     * Set the city.
     * 
     * @param city
     *            the new city
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * Set the company name.
     * 
     * @param companyName
     *            the new company name
     */
    public void setCompanyName(final String companyName) {
        this.companyName = CommonManagerBiz.stripNonValidXMLCharacters(companyName);
    }

    /**
     * Set the contract.
     * 
     * @param contract
     *            the new contract
     */
    public void setContract(final ContractWS contract) {
        this.contract = contract;
    }

    /**
     * Set the cust acct category.
     * 
     * @param custAcctCategory
     *            the new cust acct category
     */
    public void setCustAcctCategory(final CustAcctCategoryWS custAcctCategory) {
        this.custAcctCategory = custAcctCategory;
    }

    /**
     * Set the cust acct code.
     * 
     * @param custAcctCode
     *            the new cust acct code
     */
    public void setCustAcctCode(final String custAcctCode) {
        this.custAcctCode = custAcctCode;
    }

    /**
     * Set the first name.
     * 
     * @param firstName
     *            the new first name
     */
    public void setFirstName(final String firstName) {
        this.firstName = CommonManagerBiz.stripNonValidXMLCharacters(firstName);
    }

    /**
     * Set the identity number.
     * 
     * @param identityNumber
     *            the new identity number
     */
    public void setIdentityNumber(final String identityNumber) {
        this.identityNumber = CommonManagerBiz.stripNonValidXMLCharacters(identityNumber);
    }

    /**
     * Set the last name.
     * 
     * @param lastName
     *            the new last name
     */
    public void setLastName(final String lastName) {
        this.lastName = CommonManagerBiz.stripNonValidXMLCharacters(lastName);
    }

    /**
     * Set the product.
     * 
     * @param product
     *            the new product
     */
    public void setProduct(final String product) {
        this.product = product;
    }

    /**
     * Set the status.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Get the root cust acct id.
     * 
     * @return the root cust acct id
     */
    public String getCustomerIdRoot() {
        return customerIdRoot;
    }

    /**
     * Set the root cust acct id.
     * 
     * @param customerIdRoot
     *            the new root cust acct id
     */
    public void setCustomerIdRoot(final String customerIdRoot) {
        this.customerIdRoot = customerIdRoot;
    }

    public int compareType = 0;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    
    public boolean equals(final Object obj) {
        if ((compareType != 0) || ((obj != null) && (obj instanceof CustAcctWS) && (((CustAcctWS) obj).compareType != 0))) {
            return super.equals(obj);
        }
        Boolean result = false;
        if ((obj != null) && (obj instanceof CustAcctWS) && (((CustAcctWS) obj).getCustomerId() != null)) {
            if (((CustAcctWS) obj).getCustomerId().equals(customerId)) {
                final String login2 = ((CustAcctWS) obj).getContract() == null ? null : ((CustAcctWS) obj).getContract().getLoginInternet();
                final String contractId2 = ((CustAcctWS) obj).getContract() == null ? null : ((CustAcctWS) obj).getContract().getContractIdBscs();
                final String nd2 = ((CustAcctWS) obj).getContract() == null ? null : ((CustAcctWS) obj).getContract().getNd();
                final String login = contract == null ? null : contract.getLoginInternet();
                final String contractId = contract == null ? null : contract.getContractIdBscs();
                final String nd = contract == null ? null : contract.getNd();
                if ((contract == null) || (((CustAcctWS) obj).getContract() == null) || ( (login2 == null) && (contractId2 == null) && (login==null) &&  (contractId == null) && (nd == null) && (nd2 == null))) {
                    result = true;
                }
                else if (isNullOrEqual(contractId, contractId2) && isNullOrEqual(login, login2) && isNullOrEqual(nd2, nd)) {
                    result = true;
                }

            }
        }
        return result;
    }

    private static boolean isNullOrEqual(final String o1, final String o2) {
        return ((o1==null) && (o2==null)) || ((o1 != null) && o1.equals(o2));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    
    public int hashCode() {
        int result = GRCWSConstantes.HASH_SEED1;

        result = GRCWSConstantes.HASH_SEED2 * result + (getCustomerId() == null ? 0 : getCustomerId().hashCode());
        return result;
    }

    /**
     * Get the mail.
     * 
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * Set the mail.
     * 
     * @param mail
     *            the new mail
     */
    public void setMail(final String mail) {
        this.mail = mail;
    }

    /**
     * Get the ip.
     * 
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set the ip.
     * 
     * @param ip
     *            the new ip
     */
    public void setIp(final String ip) {
        this.ip = ip;
    }

    /**
     * Get the os.
     * 
     * @return the os
     */
    public String getOs() {
        return os;
    }

    /**
     * Set the os.
     * 
     * @param os
     *            the new os
     */
    public void setOs(final String os) {
        this.os = os;
    }

    /**
     * Get the equipment.
     * 
     * @return the equipment
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     * Set the equipment.
     * 
     * @param equipment
     *            the new equipment
     */
    public void setEquipment(final String equipment) {
        this.equipment = equipment;
    }

    /**
     * Get the is internet.
     * 
     * @return the is internet
     */
    public Integer getIsInternet() {
        return isInternet;
    }

    /**
     * Set the is internet.
     * 
     * @param isInternet
     *            the new is internet
     */
    public void setIsInternet(final Integer isInternet) {
        this.isInternet = isInternet;
    }

    /**
     * Get the port status.
     * 
     * @return the port status
     */
    public String getPortStatus() {
        return portStatus;
    }

    /**
     * Set the port status.
     * 
     * @param portStatus
     *            the new port status
     */
    public void setPortStatus(final String portStatus) {
        this.portStatus = portStatus;
    }

    /**
     * Get pour les contrats internet, il s'agit du client fixe correspondant *.
     * 
     * @return the pour les contrats internet, il s'agit du client fixe
     *         correspondant *
     */
    public String getCustomerIdFixe() {
        return customerIdFixe;
    }

    /**
     * Set pour les contrats internet, il s'agit du client fixe correspondant *.
     * 
     * @param customerIdFixe
     *            the new pour les contrats internet, il s'agit du client fixe
     *            correspondant *
     */
    public void setCustomerIdFixe(final String customerIdFixe) {
        this.customerIdFixe = customerIdFixe;
    }

    /**
     * Get the cust acct sous category (data).
     * 
     * @return the cust acct sous category (data)
     */
    public CustAcctCategoryWS getCustAcctSCategory() {
        return custAcctSCategory;
    }

    /**
     * Set the cust acct sous category (data).
     * 
     * @param custAcctSCategory
     *            the new cust acct sous category (data)
     */
    public void setCustAcctSCategory(final CustAcctCategoryWS custAcctSCategory) {
        this.custAcctSCategory = custAcctSCategory;
    }

    /**
     * Get the customer id.
     * 
     * @return the customer id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Set the customer id.
     * 
     * @param customerId
     *            the new customer id
     */
    public void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }

    /**
     * Get the customer id high.
     * 
     * @return the customer id high
     */
    public String getCustomerIdHigh() {
        return customerIdHigh;
    }

    /**
     * Set the customer id high.
     * 
     * @param customerIdHigh
     *            the new customer id high
     */
    public void setCustomerIdHigh(final String customerIdHigh) {
        this.customerIdHigh = customerIdHigh;
    }

    /**
     * Get dns.
     * 
     * @return the dns
     */
    public String getDns() {
        return dns;
    }

    /**
     * Set dns.
     * 
     * @param dns
     *            the dns
     */
    public void setDns(final String dns) {
        this.dns = dns;
    }

    /**
     * Gets the cust acct code high.
     * 
     * @return the cust acct code high
     */
    public String getCustAcctCodeHigh() {
        return custAcctCodeHigh;
    }

    /**
     * Sets the cust acct code high.
     * 
     * @param custAcctCodeHigh
     *            the new cust acct code high
     */
    public void setCustAcctCodeHigh(final String custAcctCodeHigh) {
        this.custAcctCodeHigh = custAcctCodeHigh;
    }
}
