/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceWS.
 */
public class ServiceDnsWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2351881891090393855L;

    /** The login. */
    private String login;

    /** The service id. */
    private String serviceId;

    /** The service name. */
    private String serviceName;

    /** The domaine. */
    private String domaine;

    /** The type. */
    private String type;

    /** The montant. */
    private String montant;

    /**
     * Gets the login.
     * 
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login.
     * 
     * @param login
     *            the new login
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * Gets the service id.
     * 
     * @return the service id
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets the service id.
     * 
     * @param serviceId
     *            the new service id
     */
    public void setServiceId(final String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * Gets the service name.
     * 
     * @return the service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the service name.
     * 
     * @param serviceName
     *            the new service name
     */
    public void setServiceName(final String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Gets the domaine.
     * 
     * @return the domaine
     */
    public String getDomaine() {
        return domaine;
    }

    /**
     * Sets the domaine.
     * 
     * @param domaine
     *            the new domaine
     */
    public void setDomaine(final String domaine) {
        this.domaine = domaine;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the montant.
     * 
     * @return the montant
     */
    public String getMontant() {
        return montant;
    }

    /**
     * Sets the montant.
     * 
     * @param montant
     *            the new montant
     */
    public void setMontant(final String montant) {
        this.montant = montant;
    }

}
