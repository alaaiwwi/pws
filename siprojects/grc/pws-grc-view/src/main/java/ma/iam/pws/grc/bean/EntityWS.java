/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import fr.capgemini.iam.grc.core.utils.GRCStringUtil;


/**
 * The Class EntityWS.
 */
public class EntityWS extends GrcWS {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2389378705400331955L;

    /** The entity id. */
    private Integer entityId;

    /** The entity type. */
    private String entityType;

    /** The entity name. */
    private String entityName;

    /** The external cd. */
    private String externalCd;

    public Boolean isEmpty() {
        return ((entityId == null) || Integer.valueOf(0).equals(entityId)) && GRCStringUtil.isNullOrEmpty(entityType)
                && GRCStringUtil.isNullOrEmpty(entityName) && GRCStringUtil.isNullOrEmpty(externalCd);
    }

    /**
     * Get the entity id.
     * 
     * @return the entity id
     */
    public Integer getEntityId() {
        return entityId;
    }

    /**
     * Get the entity name.
     * 
     * @return the entity name
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Get the entity type.
     * 
     * @return the entity type
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     * Set the entity id.
     * 
     * @param entityId
     *            the new entity id
     */
    public void setEntityId(final Integer entityId) {
        this.entityId = entityId;
    }

    /**
     * Set the entity name.
     * 
     * @param entityName
     *            the new entity name
     */
    public void setEntityName(final String entityName) {
        this.entityName = entityName;
    }

    /**
     * Set the entity type.
     * 
     * @param entityType
     *            the new entity type
     */
    public void setEntityType(final String entityType) {
        this.entityType = entityType;
    }

    /**
     * Get external cd.
     * 
     * @return the external cd
     */
    public String getExternalCd() {
        return externalCd;
    }

    /**
     * Set external cd.
     * 
     * @param externalCd
     *            the external cd
     */
    public void setExternalCd(final String externalCd) {
        this.externalCd = externalCd;
    }
}
