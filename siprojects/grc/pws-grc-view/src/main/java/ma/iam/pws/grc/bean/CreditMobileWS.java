/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class RdbWS.
 */
public class CreditMobileWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7784712066033633311L;

    /** The dernier appel. */
    private String dernierAppel;

    /** The forfait base restant. */
    private String forfaitBaseRestant;

    /** The forfait sw restant. */
    private String forfaitSWRestant;

    /** The hors forfait. */
    private String horsForfait;

    /** The forfait report restant. */
    private String forfaitReportRestant;

    /** The duree eligible. */
    private String dureeEligible;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[CreditMobileWS:");
        if (dernierAppel != null) {
            sb.append(",dernierAppel=").append(dernierAppel);
        }
        if (forfaitBaseRestant != null) {
            sb.append(",forfaitBaseRestant=").append(forfaitBaseRestant);
        }
        if (forfaitSWRestant != null) {
            sb.append(",forfaitSWRestant=").append(forfaitSWRestant);
        }
        if (horsForfait != null) {
            sb.append(",horsForfait=").append(horsForfait);
        }
        if (forfaitReportRestant != null) {
            sb.append(",forfaitReportRestant=").append(forfaitReportRestant);
        }
        if (dureeEligible != null) {
            sb.append(",dureeEligible=").append(dureeEligible);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets the dernier appel.
     * 
     * @return the dernier appel
     */
    public String getDernierAppel() {
        return dernierAppel;
    }

    /**
     * Sets the dernier appel.
     * 
     * @param dernierAppel
     *            the new dernier appel
     */
    public void setDernierAppel(final String dernierAppel) {
        this.dernierAppel = dernierAppel;
    }

    /**
     * Gets the forfait base restant.
     * 
     * @return the forfait base restant
     */
    public String getForfaitBaseRestant() {
        return forfaitBaseRestant;
    }

    /**
     * Sets the forfait base restant.
     * 
     * @param forfaitBaseRestant
     *            the new forfait base restant
     */
    public void setForfaitBaseRestant(final String forfaitBaseRestant) {
        this.forfaitBaseRestant = forfaitBaseRestant;
    }

    /**
     * Gets the forfait sw restant.
     * 
     * @return the forfait sw restant
     */
    public String getForfaitSWRestant() {
        return forfaitSWRestant;
    }

    /**
     * Sets the forfait sw restant.
     * 
     * @param forfaitSWRestant
     *            the new forfait sw restant
     */
    public void setForfaitSWRestant(final String forfaitSWRestant) {
        this.forfaitSWRestant = forfaitSWRestant;
    }

    /**
     * Gets the hors forfait.
     * 
     * @return the hors forfait
     */
    public String getHorsForfait() {
        return horsForfait;
    }

    /**
     * Sets the hors forfait.
     * 
     * @param horsForfait
     *            the new hors forfait
     */
    public void setHorsForfait(final String horsForfait) {
        this.horsForfait = horsForfait;
    }

    /**
     * Gets the forfait report restant.
     * 
     * @return the forfait report restant
     */
    public String getForfaitReportRestant() {
        return forfaitReportRestant;
    }

    /**
     * Sets the forfait report restant.
     * 
     * @param forfaitReportRestant
     *            the new forfait report restant
     */
    public void setForfaitReportRestant(final String forfaitReportRestant) {
        this.forfaitReportRestant = forfaitReportRestant;
    }

    /**
     * Gets the duree eligible.
     * 
     * @return the duree eligible
     */
    public String getDureeEligible() {
        return dureeEligible;
    }

    /**
     * Sets the duree eligible.
     * 
     * @param dureeEligible
     *            the new duree eligible
     */
    public void setDureeEligible(final String dureeEligible) {
        this.dureeEligible = dureeEligible;
    }

}
