/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.StatsClientWS;
import ma.iam.pws.grc.business.StatManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class StatManagerService.
 */
@WebService(serviceName = "StatManagerService", targetNamespace = "http://service.grc.iam.capgemini.fr/")
@Component(value = "statManagerService")
public class StatManagerService extends SpringBeanAutowiringSupport {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(StatManagerService.class);

	/** The stat manager biz. */
	@Autowired
	private StatManagerBiz statManagerBiz;

	/**
	 * Cherche les stats client.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param customerIdHigh
	 *            l'identifiant client du pére (CUSTOMER_ID_HIGH)
	 * @param categoryCd
	 *            le code de la catégorie du client
	 * @param product
	 *            le produit
	 * @return les statistiques client
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	/*
	 * f.tatbi FC 5864 delete parameters customerIdHigh,categoryCd
	 */
	@WebMethod(operationName = "findStatsClient")
	@WebResult(name = "StatsClientWS")
	public StatsClientWS findStatsClient(
			@WebParam(name = "customerId") final String customerId,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace("@WebMethod: statManagerBiz.findStatsClient customerId="
				+ customerId);
		return statManagerBiz.findStatsClient(customerId, product);
	}

	@Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
