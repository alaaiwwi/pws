/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CreditMobileWS;
import ma.iam.pws.grc.bean.CugWS;
import ma.iam.pws.grc.bean.InfoDataWS;
import ma.iam.pws.grc.bean.ServiceCreditWS;
import ma.iam.pws.grc.bean.ServiceDnsWS;
import ma.iam.pws.grc.bean.ServiceHistoryWS;
import ma.iam.pws.grc.bean.ServiceParamWS;
import ma.iam.pws.grc.bean.ServiceWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.CommonDAOFixe;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceDAODataImpl.
 */
@Repository
@Deprecated
public class ServiceDAODataImpl extends CommonDAOFixe implements IServiceDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(ServiceDAODataImpl.class);

    /** La constante GETLISTESERVICES. */
    public static final String GETLISTESERVICES = ServiceDAODataImpl.class.getName() + ".GETLISTESERVICES";

    /** La constante GETLISTESERVICEHIST. */
    public static final String GETLISTESERVICEHIST = ServiceDAODataImpl.class.getName() + ".GETLISTESERVICEHIST";

    /** La constante GETLISTESERVICEPARAM. (méme requete que le fixe) */
    public static final String GETLISTESERVICEPARAM = ServiceDAOFixeImpl.class.getName() + ".GETLISTESERVICEPARAM";

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServices(java.lang.String
     * )
     */
    
    public List<ServiceWS> findServices(final String contractIdBscs) {
        LOG.debug("--> findServices contractIdBsc {}", contractIdBscs);
        List<ServiceWS> listServices = null;
        final String sql = CustomSQLUtil.get(GETLISTESERVICES);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("contractIdBscs", contractIdBscs);

        final CustomBeanPropertyRowMapper<ServiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceWS>(ServiceWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listServices = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, invoiceRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- findServices");
        return listServices;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServiceHistory(java.
     * lang.String, java.lang.String)
     */
    
    public List<ServiceHistoryWS> findServiceHistory(final String contractIdBscs, final String sncode) {
        LOG.debug("--> findServiceHistory contractIdBsc {} sncode {}", contractIdBscs, sncode);
        List<ServiceHistoryWS> listServicesHist = null;
        final String sql = CustomSQLUtil.get(GETLISTESERVICEHIST);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("contractIdBscs", contractIdBscs);
        namedParameters.put("sncode", sncode);
        final CustomBeanPropertyRowMapper<ServiceHistoryWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceHistoryWS>(ServiceHistoryWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listServicesHist = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, invoiceRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- findServiceHistory");
        return listServicesHist;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServicesDns(java.lang
     * .String)
     */
    
    public List<ServiceDnsWS> findServicesDns(final String contractIdBscs) {
        LOG.debug("--> findServicesDns contractIdBsc {}", contractIdBscs);
        throw new NotImplementedException("Service DNS non disponible pour le produit DATA");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServiceParams(java.lang
     * .String)
     */
    
    public List<ServiceParamWS> findServiceParams(final String paramId, final String contractIdNeto) {
        LOG.debug("--> findServiceParams paramId {}", paramId);
        List<ServiceParamWS> listServicesParams = null;
        final String sql = CustomSQLUtil.get(GETLISTESERVICEPARAM);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("paramId", paramId);
        final CustomBeanPropertyRowMapper<ServiceParamWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceParamWS>(ServiceParamWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listServicesParams = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, invoiceRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- findServiceParams");
        return listServicesParams;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findRdbParams(java.lang.
     * String)
     */
   // hors perimetre AtoS
    //
//    public RdbWS findRdbParams(final String nd) {
//        LOG.debug("--> findRdbParams nd {}", nd);
//        throw new NotImplementedException("Service RDB non disponible pour le produit DATA");
//    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findInfoData(java.lang.String
     * , java.lang.String)
     */
    
    public InfoDataWS findInfoData(final String contractIdBscs, final String contractIdNeto) {
        LOG.debug("--> findInfoData contractIdBsc {} contractIdNeto {}", contractIdBscs, contractIdNeto);

        final InfoDataWS info = new InfoDataWS();

        // Getting the SLA
        String gtrValue = null;
        String delaiValue = null;
        String gtdValue = null;

        final String slaClass = getSlaClass(contractIdBscs);

        if (GRCWSConstantes.C1_CLASS.equals(slaClass)) {
            gtrValue = GRCWSConstantes.C1_GTR;
            delaiValue = GRCWSConstantes.C1_DELAI;
            gtdValue = GRCWSConstantes.C1_GTD;
        } else if (GRCWSConstantes.C2_CLASS.equals(slaClass)) {
            gtrValue = GRCWSConstantes.C2_GTR;
            delaiValue = GRCWSConstantes.C2_DELAI;
            gtdValue = GRCWSConstantes.C2_GTD;

        } else if (GRCWSConstantes.C3_CLASS.equals(slaClass)) {
            gtrValue = GRCWSConstantes.C3_GTR;
            delaiValue = GRCWSConstantes.C3_DELAI;
            gtdValue = GRCWSConstantes.C3_GTD;

        } else {
            gtrValue = GRCWSConstantes.C0_GTR;
            delaiValue = GRCWSConstantes.C0_DELAI;
            gtdValue = GRCWSConstantes.C0_GTD;
        }

        info.setClasse1(slaClass);
        info.setGtrValue(gtrValue);
        info.setDelaiValue(delaiValue);
        info.setGtdValue(gtdValue);

        // Getting the Adress1
        info.setAdressL1(getAdress(GRCWSConstantes.TYPE_L1_ADRESS, contractIdNeto));

        // Getting the Adress2
        info.setAdressL2(getAdress(GRCWSConstantes.TYPE_L2_ADRESS, contractIdNeto));

        LOG.debug("<-- findInfoData info {}", info);
        return info;
    }

    
    public String getSlaClass(final String contractIdBscs) {
        return super.getSlaClass(contractIdBscs);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.service.IServiceDAO#findAllHlr()
     */
    
    public List<String> findAllHlr() {
        throw new NotImplementedException("Service HLR non disponible pour le produit DATA");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCustomerHlr(java.lang
     * .String)
     */
    
    public String findCustomerHlr(final String nd) {
        throw new NotImplementedException("Service HLR non disponible pour le produit DATA");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServicesCredit(java.
     * lang.String)
     */
    
    public List<ServiceCreditWS> findServicesCredit(final String contractIdBscs) {
        throw new NotImplementedException("Service Credit non disponible pour le produit DATA");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCreditMobileParams(java
     * .lang.String)
     */
    
    public CreditMobileWS findCreditMobileParams(final String contractId) {
        throw new NotImplementedException("Service Credit non disponible pour le produit DATA");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCug(java.lang.String)
     */
    
    public List<CugWS> findCug(final String contractIdBscs) {
        throw new NotImplementedException("Service CUG non disponible pour le produit DATA");
    }

}
