/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.invoice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.InvoiceDetailWS;
import ma.iam.pws.grc.bean.InvoiceWS;
import ma.iam.pws.grc.bean.InvoiceWSFindInvoice;
import ma.iam.pws.grc.bean.PaymentDetailWS;
import ma.iam.pws.grc.bean.PaymentWS;
import ma.iam.pws.grc.bean.SearchInvoiceBean;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;
import ma.iam.pws.grc.dao.TicklerNumber;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

/**
 * The Class InvoiceDAOMobileImpl.
 */
@Repository
public class InvoiceDAOMobileImpl extends BaseDAOMobile implements IInvoiceDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(InvoiceDAOMobileImpl.class);

	/** La constante GETLISTEFACTURES. */
	public static final String GETLISTEFACTURES = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEFACTURES";

	/** La constante GETLISTEFACTURESDETAILS. */
	public static final String GETLISTEFACTURESDETAILS = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEFACTURESDETAILS";

	/** La constante GETLISTEPAIEMENTSDETAILS. */
	public static final String GETLISTEPAIEMENTSDETAILS = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEPAIEMENTSDETAILS";

	/** La constante GETLISTEPAIEMENTSCLIENT. */
	public static final String GETLISTEPAIEMENTSCLIENT = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEPAIEMENTSCLIENT";

	/** La constante GETLISTEPAIEMENTSFACTURE. */
	public static final String GETLISTEPAIEMENTSFACTURE = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEPAIEMENTSFACTURE";

	/** La constante MONTANTGLOBALOUVERT. */
	public static final String MONTANTGLOBALOUVERT = InvoiceDAOMobileImpl.class
			.getName() + ".MONTANTGLOBALOUVERT";

	/** La constante GETLISTEFACTURESBYINVOICEEXTERNALID. */
	public static final String GETLISTEFACTURESBYINVOICEEXTERNALID = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEFACTURESBYINVOICEEXTERNALID";

	/** The Constant GETLISTEFACTURESBYREFERENCE. */
	public static final String GETLISTEFACTURESBYREFERENCE = InvoiceDAOMobileImpl.class
			.getName() + ".GETLISTEFACTURESBYREFERENCE";

	/** La constante PROTECTINVOICES. */
	public static final String PROTECTINVOICES = InvoiceDAOMobileImpl.class
			.getName() + ".PROTECTINVOICES";

	/** La constante UNPROTECTINVOICES. */
	public static final String UNPROTECTINVOICES = InvoiceDAOMobileImpl.class
			.getName() + ".UNPROTECTINVOICES";

	public static final String DETAILED_INVOICE = InvoiceDAOMobileImpl.class
			.getName() + ".DETAILED_INVOICE";

	/** La constante INSERTHISTO. */
	public static final String INSERTHISTO = InvoiceDAOMobileImpl.class
			.getName() + ".INSERTHISTO";

	/** La constante GETLISTEFACTURESBYID. */
	public static final String GETLISTEFACTURESBYID = InvoiceDAOFixeImpl.class
			.getName() + ".GETLISTEFACTURESBYID";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findInvoicesDetails(ma
	 * .iam.grc.bean.InvoiceWS)
	 */

	public List<InvoiceDetailWS> findInvoicesDetails(final String idFacture) {
		LOG.debug("--> findInvoicesDetails idFacture {}", idFacture);
		List<InvoiceDetailWS> listInvoiceDetails = null;

		final String sql = CustomSQLUtil.get(GETLISTEFACTURESDETAILS);

		final CustomBeanPropertyRowMapper<InvoiceDetailWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceDetailWS>(
				InvoiceDetailWS.class);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("idFacture", idFacture);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findInvoicesDetails");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findInvoicesForCustomer(
	 * fr.capgemini.iam.grc.bean.SearchInvoiceBean)
	 */

	public List<InvoiceWSFindInvoice> findInvoicesForCustomer(
			final SearchInvoiceBean searchInvoiceBean) {
		LOG.debug("--> findInvoicesForCustomer searchInvoiceBean {}",
				searchInvoiceBean);
		List<InvoiceWSFindInvoice> listInvoices = null;

		final StringBuffer sqlBuffer = new StringBuffer(
				CustomSQLUtil.get(GETLISTEFACTURES));

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID,
				searchInvoiceBean.getCustomerId());
		namedParameters.put("startDate", searchInvoiceBean.getInterval()
				.getStartDate());
		namedParameters.put("endDate", searchInvoiceBean.getInterval()
				.getEndDate());
		if (searchInvoiceBean.getStatut() != null) {
			sqlBuffer.append(" AND OHSTATUS ='"
					+ searchInvoiceBean.getStatut() + "'");
		}
		sqlBuffer.append("  order by OHREFDATE desc");

		final CustomBeanPropertyRowMapper<InvoiceWSFindInvoice> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWSFindInvoice>(
				InvoiceWSFindInvoice.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer);
		listInvoices = super.getNamedParameterJdbcTemplate().query(
				sqlBuffer.toString(), namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findInvoicesForCustomer");
		return listInvoices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findPaymentsDetails(ma
	 * .iam.grc.bean.InvoiceWS)
	 */

	public List<PaymentDetailWS> findPaymentsDetails(final String numTransaction) {
		LOG.debug("--> findPaymentsDetails numTransaction {}", numTransaction);
		List<PaymentDetailWS> listInvoiceDetails = null;

		final String sql = CustomSQLUtil.get(GETLISTEPAIEMENTSDETAILS);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("numTransaction", numTransaction);
		final CustomBeanPropertyRowMapper<PaymentDetailWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<PaymentDetailWS>(
				PaymentDetailWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findPaymentsDetails");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findPayments(java.lang.String
	 * )
	 */

	public List<PaymentWS> findPayments(
			final SearchInvoiceBean searchInvoiceBean) {
		LOG.debug("--> findPayments searchInvoiceBean {}", searchInvoiceBean);
		List<PaymentWS> listInvoiceDetails = null;

		final String sql = CustomSQLUtil.get(GETLISTEPAIEMENTSCLIENT);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID,
				searchInvoiceBean.getCustomerId());
		namedParameters.put("startDate", searchInvoiceBean.getInterval()
				.getStartDate());
		namedParameters.put("endDate", searchInvoiceBean.getInterval()
				.getEndDate());

		final CustomBeanPropertyRowMapper<PaymentWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<PaymentWS>(
				PaymentWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- searchInvoiceBean");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findPaymentsForInvoice(java
	 * .lang.String)
	 */

	public List<PaymentWS> findPaymentsForInvoice(final String refFacture,final Integer idFacture) {
		LOG.debug("--> findPaymentsForInvoice numFacture {}", refFacture);
		List<PaymentWS> listInvoiceDetails = null;
		final StringBuffer sql = new StringBuffer(CustomSQLUtil.get(GETLISTEPAIEMENTSFACTURE));

	//	final Map<String, String> namedParameters = new HashMap<String, String>();
	//	namedParameters.put("numFacture", refFacture);
		if (idFacture != null)
			sql.append(" AND oa.OHXACT = '" + idFacture + "'");
		if (refFacture != null)
			sql.append(" AND oa.OHREFNUM = '" + refFacture + "'");

		sql.append(" ORDER BY C.CAXACT DESC");
		
		final CustomBeanPropertyRowMapper<PaymentWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<PaymentWS>(
				PaymentWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listInvoiceDetails = super.getNamedParameterJdbcTemplate().query(sql.toString(),
				 invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		LOG.debug("<-- findPaymentsForInvoice");
		return listInvoiceDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findMontantGlobalOuvert(
	 * java.lang.String)
	 */

	public Double findMontantGlobalOuvert(final String custAcctCode) {
		LOG.debug("--> findMontantGlobalOuvert custAcctCode {}", custAcctCode);

		Double result;
		final String sql = CustomSQLUtil.get(MONTANTGLOBALOUVERT);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("custAcctCode", custAcctCode);
		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		result = super.getNamedParameterJdbcTemplate().queryForObject(sql,
				namedParameters, Double.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END_RESULT, result);

		LOG.debug("<-- findMontantGlobalOuvert");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#
	 * findInvoicesByListInvoiceExternalId(java.util.List)
	 */

	public List<InvoiceWS> findInvoicesByListInvoiceExternalId(
			final List<Integer> listInvoiceExternalId) {
		LOG.debug(
				"--> findInvoicesByListInvoiceExternalId listInvoiceExternalId {}",
				listInvoiceExternalId);
		List<InvoiceWS> listInvoices = null;

		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURESBYINVOICEEXTERNALID));

		if (null != listInvoiceExternalId) {
			for (int i = 0; i < listInvoiceExternalId.size(); i++) {
				sqlBuffer.append(listInvoiceExternalId.get(i));
				if (i != listInvoiceExternalId.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			sqlBuffer.append(" order by OHREFDATE desc ");
			final CustomBeanPropertyRowMapper<InvoiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWS>(
					InvoiceWS.class);
			listInvoices = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations()
					.query(sqlBuffer.toString(), invoiceRowMapper);
		}

		LOG.debug("<-- findInvoicesByListInvoiceExternalId");
		return listInvoices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#findInvoicesByListReference
	 * (java.util.List)
	 */

	public List<InvoiceWS> findInvoicesByListReference(
			final List<String> listReferenceFacture) {
		List<InvoiceWS> listInvoices = null;
		if (LOG.isDebugEnabled())
			LOG.debug("--> findInvoicesByListReference: listReferenceFacture="
					+ listReferenceFacture);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURESBYREFERENCE));

		if (null != listReferenceFacture) {
			for (int i = 0; i < listReferenceFacture.size(); i++) {
				sqlBuffer.append("'").append(listReferenceFacture.get(i))
						.append("'");
				if (i != listReferenceFacture.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
			final CustomBeanPropertyRowMapper<InvoiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWS>(
					InvoiceWS.class);
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			listInvoices = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations()
					.query(sqlBuffer.toString(), invoiceRowMapper);
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findInvoicesByListReference: " + listInvoices);

		return listInvoices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#
	 * findInvoicesByListInvoiceExternalId(java.util.List)
	 */

	public Boolean protectInvoices(final List<Integer> listInvoiceExternalId) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> protectInvoices: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(PROTECTINVOICES));

		int rowsUpdated = 0;
		if ((null != listInvoiceExternalId) && !listInvoiceExternalId.isEmpty()) {
			for (final Integer invoiceExternalId : listInvoiceExternalId) {
				sqlBuffer.append(invoiceExternalId.toString()).append(",");
			}
			sqlBuffer.append(" 0 ) ");
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			rowsUpdated = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().update(sqlBuffer.toString());
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		LOG.debug("<-- protectInvoices");

		return rowsUpdated > 0 ? true : false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#unProtectInvoices(java.util
	 * .List)
	 */

	public Boolean unProtectInvoices(final List<Integer> listInvoiceExternalId) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> unProtectInvoices: listInvoiceExternalId="
					+ listInvoiceExternalId);
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(UNPROTECTINVOICES));

		int rowsUpdated = 0;
		if ((null != listInvoiceExternalId) && !listInvoiceExternalId.isEmpty()) {
			for (final Integer invoiceExternalId : listInvoiceExternalId) {
				sqlBuffer.append(invoiceExternalId.toString()).append(",");
			}
			sqlBuffer.append(" 0 ) ");
			if (LOG.isDebugEnabled())
				LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
			rowsUpdated = super.getNamedParameterJdbcTemplate()
					.getJdbcOperations().update(sqlBuffer.toString());
			LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		}

		LOG.debug("<-- unProtectInvoices");

		return rowsUpdated > 0 ? true : false;
	}

	public List<InvoiceWS> findDetailedInvoicesForCustomer(
			final SearchInvoiceBean searchInvoiceBean) {
		LOG.debug("--> findDetailedInvoicesForCustomer searchInvoiceBean {}",
				searchInvoiceBean);

		final String sql = CustomSQLUtil.get(DETAILED_INVOICE);
		String startDate = searchInvoiceBean.getInterval().getStartDate();
		if (startDate != null && startDate.length() > 19) {
			startDate = startDate.substring(0, 19);
		}

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID,
				searchInvoiceBean.getCustomerId());
		namedParameters.put("dateFacture", startDate);
		final CustomBeanPropertyRowMapper<InvoiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWS>(
				InvoiceWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		List<InvoiceWS> listInvoices = super.getNamedParameterJdbcTemplate()
				.query(sql, namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findDetailedInvoicesForCustomer");
		return listInvoices;
	}

	public Boolean insertHistory(Integer facture, String user,
			String application, String msg) {
		if (LOG.isDebugEnabled())
			LOG.debug("--> insertHistory: facture=" + facture + " user " + user
					+ " application " + application + " msg " + msg);

		TicklerNumber number = new TicklerNumber(
				super.getMobileBSCSDataSource());
		Long ticklerNumber = number.getSequenceValue("MAX_TICKLER_NUMBER");

		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(INSERTHISTO));

		SqlParameterSource mapSqlParameters = new MapSqlParameterSource()
				.addValue("factureId", facture)
				.addValue("ticklerCode", TICKLER_CODE).addValue("user", user)
				.addValue("application", application).addValue("message", msg)
				.addValue("ticklerNumber", ticklerNumber);
		final int nbRowUpdated = super.getNamedParameterJdbcTemplate().update(
				sqlBuffer.toString(), mapSqlParameters);

		LOG.debug("<-- insertHistory");
		return nbRowUpdated > 0 ? Boolean.TRUE : Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.invoice.IInvoiceDAO#
	 * findInvoiceById(java.util.List,java.util.List,java.lang.String)
	 */

	public List<InvoiceWSFindInvoice> findInvoiceById(
			final List<String> refFacture, final List<Integer> idFacture,
			final String statut) {
		List<InvoiceWSFindInvoice> listInvoices = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("--> findInvoiceById: refFacture=" + refFacture
					+ " idFacture=" + idFacture + " statut =" + statut);
		}
		final StringBuilder sqlBuffer = new StringBuilder(
				CustomSQLUtil.get(GETLISTEFACTURESBYID));

		if (refFacture != null && !refFacture.isEmpty()) {
			sqlBuffer.append(" OHREFNUM in (");
			for (int i = 0; i < refFacture.size(); i++) {
				sqlBuffer.append("'" + refFacture.get(i) + "'");
				if (i != refFacture.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
		}

		if (idFacture != null && !idFacture.isEmpty()) {
			if (refFacture != null && !refFacture.isEmpty())
				sqlBuffer.append(" AND OHXACT in (");
			else
				sqlBuffer.append(" OHXACT in (");

			for (int i = 0; i < idFacture.size(); i++) {
				sqlBuffer.append(idFacture.get(i));
				if (i != idFacture.size() - 1) {
					sqlBuffer.append(" , ");
				}
			}
			sqlBuffer.append(" ) ");
		}

		if (statut != null) {
			sqlBuffer.append(" AND OHSTATUS ='" + statut + "'");
		}

		sqlBuffer.append(" order by OHREFDATE desc ");
		final CustomBeanPropertyRowMapper<InvoiceWSFindInvoice> invoiceRowMapper = new CustomBeanPropertyRowMapper<InvoiceWSFindInvoice>(
				InvoiceWSFindInvoice.class);
		if (LOG.isDebugEnabled())
			LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlBuffer.toString());
		listInvoices = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations()
				.query(sqlBuffer.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if (LOG.isDebugEnabled())
			LOG.debug("<-- findInvoiceById: " + listInvoices);

		return listInvoices;
	}

	public Boolean insertUnPaid(Integer flag, Integer order_ref) {
		throw new NotImplementedException(
				"Service HLR non disponible pour le produit INTERNET");
	}
}
