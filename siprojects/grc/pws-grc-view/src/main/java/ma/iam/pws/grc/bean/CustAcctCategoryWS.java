/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class CustAcctCategoryWS.
 */
public class CustAcctCategoryWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7588188942726791856L;

    /** The category cd. */
    private String categoryCd;

    /** The category desc. */
    private String categoryDesc;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[CustAcctCategoryWS:");
        if (categoryCd != null) {
            sb.append(",categoryCd=").append(categoryCd);
        }
        if (categoryDesc != null) {
            sb.append(",categoryDesc=").append(categoryDesc);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets le code de la catégorie du client.
     * 
     * @return le code de la catégorie du client
     */
    public String getCategoryCd() {
        return categoryCd;
    }

    /**
     * Gets the category desc.
     * 
     * @return the category desc
     */
    public String getCategoryDesc() {
        return categoryDesc;
    }

    /**
     * Sets le code de la catégorie du client.
     * 
     * @param categoryCd
     *            the new category cd
     */
    public void setCategoryCd(final String categoryCd) {
        this.categoryCd = categoryCd;
    }

    /**
     * Sets the category desc.
     * 
     * @param categoryDesc
     *            the new category desc
     */
    public void setCategoryDesc(final String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

}
