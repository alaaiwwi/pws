/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.CustFindContractWS;
import ma.iam.pws.grc.bean.CustSearchWS;
import ma.iam.pws.grc.bean.SearchCustomerWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import ma.iam.pws.grc.dao.custacct.ICustAcctDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.capgemini.iam.grc.core.utils.GRCReflectUtils;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerManagerBiz.
 */
@Component(value = "customerManagerBiz")
public class CustomerManagerBiz extends CommonManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CustomerManagerBiz.class);

	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public void addFidelioInfos(final CustAcctWS searchCustAcctBean,
			final int product) throws GrcWsException {
		final List<CustAcctWS> listCust = new ArrayList<CustAcctWS>();
		listCust.add(searchCustAcctBean);
		getFidelioDAOForProduct(ProduitTypeEnum.MOBILE.getIdTypeProduit())
				.setQualiteFidelio(listCust);
	}

	/**
	 * Cherche les contracts pour le client donné.
	 * 
	 * @param searchCustBean
	 *            le bean contenant les critéres de recherche du client
	 * @param allowVip
	 *            indique si la recherche des clients VIP est autorisée
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<CustAcctWS> findContractsForCustomer(
			final CustAcctWS searchCustBean, final Boolean allowVip,
			final int product) throws GrcWsException {

		LOG.info("-> findContractsForCustomer: " + searchCustBean + " "
				+ allowVip);

		final List<CustAcctWS> listCustResult = new ArrayList<CustAcctWS>();

		final ICustAcctDAO iCustAcctDAO = getCustAcctDAOForProduct(product);

		if (GRCReflectUtils.isEmptyBean(searchCustBean)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"Données de recherche vides");
		}

		// Dans le cadre de la DATA, permettre la recherche par raison site doit
		// obligatoirement renseigner raison pére
		// dans les cadres FIXE, INTERNET et MOBILE, la recherche par prénom
		// doit obligatoirement renseigner le nom
		if (GRCStringUtil.isNullOrEmpty(searchCustBean.getLastName())
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getFirstName())) {
			String msgError;
			if (product == ProduitTypeEnum.DATA.getIdTypeProduit()) {
				msgError = "Si le champ 'Raison Site' est renseigné le champ 'Raison Pére' doit aussi étre renseigné";
			} else {
				msgError = "Si le champ 'Prénom' est renseigné le champ 'Nom' doit aussi étre renseigné";
			}
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					msgError);
		}

		// Recherche des contrats suivant les critéres de recherche donnés
		final List<CustAcctWS> listCustAcct = iCustAcctDAO
				.findContractsForCustomer(searchCustBean, allowVip, product);

		// Pour chaque element trouvé, on cherche son pére
		for (final CustAcctWS custAcctFound : listCustAcct) {
			LOG.trace("findContractsForCustomer: recherche pére de custAcctFound"
					+ custAcctFound);
			if ((custAcctFound == null)
					|| (custAcctFound.getCustomerIdHigh() == null)
					|| (Integer.valueOf(custAcctFound.getCustomerIdHigh()) == 0)) {
				// Client orphelin.. on le rajoute directement dans la liste
				listCustResult.add(custAcctFound);
			} else {
				// Le pére du client est indiqué, on recherche son pére
				final CustAcctWS fatherCustFound = findFatherCustAcctWS(
						custAcctFound, listCustResult, iCustAcctDAO, product);

				if (fatherCustFound == null) {
					// pére non trouvé, on rajoute le client directement dans la
					// liste
					listCustResult.add(custAcctFound);
				} else {
					// pére trouvé
					// CustomerIdRoot est l'ancétre (premier pére) du client.
					custAcctFound.setCustomerIdRoot(fatherCustFound
							.getCustomerId());
					// Si le pére trouvé on le rajoute dans la liste
					// (s'il y figure déjé le fils est déjé rajouté dedans)
					if (!listCustResult.contains(fatherCustFound)) {
						listCustResult.add(fatherCustFound);
					}
				}
			}
		}

		// Pour le produit internet, on ne laisse que les contrats avec des
		// contrats internet associés
		if ((product == ProduitTypeEnum.INTERNET.getIdTypeProduit())
				|| (product == ProduitTypeEnum.FIXE.getIdTypeProduit())) {
			// filterDuplicateChildren(listCustResult);
			filtreContractFromSearch(listCustResult, searchCustBean);
			filterDupplicateChildrenRecurs(listCustResult);

		}
		iCustAcctDAO.getInfoSupplementaires(listCustResult, product);

		// TRI DE LA LISTE DES RESULTATS
		trierParStatut(listCustResult);
		// FIN

		LOG.info("<- findContractsForCustomer");

		return listCustResult;

	}

	private void trierParStatut(final List<CustAcctWS> listCustResult) {
		for (final CustAcctWS custAcctWS : listCustResult) {
			if ((custAcctWS != null) && (custAcctWS.getChildren() != null)
					&& !custAcctWS.getChildren().isEmpty()) {
				trierParStatut(custAcctWS.getChildren());
			}
		}
		Collections.sort(listCustResult, new CustomComparator(PAR_STATUT));
	}

	private void filterDupplicateChildrenRecurs(
			final List<CustAcctWS> listCustAcctWS) {
		Collections.sort(listCustAcctWS, new CustomComparator(PAR_CO_ID));
		filterDuplicateChildren(listCustAcctWS);
		for (final CustAcctWS custAcctFound : listCustAcctWS) {
			if ((custAcctFound.getChildren() != null)
					&& (custAcctFound.getChildren().size() > 0)) {
				Collections.sort(custAcctFound.getChildren(),
						new CustomComparator(PAR_CO_ID));
				filterDupplicateChildrenRecurs(custAcctFound.getChildren());
			}
		}
	}

	/**
	 * Cherche les contracts fixe avec des contrats internet correspondants.
	 * 
	 * @param custAcctWS
	 *            le contrat fixe recherché
	 * @param searchCustBean
	 *            le bean contenant les critéres de recherche du client
	 * @return le contrat fixe ayant un contrat internet correspondant
	 */
	private void filtreContractFromSearch(
			final List<CustAcctWS> listCustAcctWS,
			final CustAcctWS searchCustBean) {

		if ((listCustAcctWS != null) && !listCustAcctWS.isEmpty()) {
			addFixeCustIdForInternetContracts(listCustAcctWS);
			final List<CustAcctWS> childrenToRemove = new ArrayList<CustAcctWS>();
			for (final CustAcctWS custAcctChildren : listCustAcctWS) {

				final Boolean boolAdd = compareCustAcctBean(custAcctChildren,
						searchCustBean);

				if (!boolAdd) {

					if (!custAcctChildren.getChildren().isEmpty()) {
						filtreContractFromSearch(
								custAcctChildren.getChildren(), searchCustBean);
					}

					if (custAcctChildren.getChildren().isEmpty()) {
						custAcctChildren.compareType = 1;
						childrenToRemove.add(custAcctChildren);
					}

				} else {
					filtreContractFromSearch(custAcctChildren.getChildren(),
							searchCustBean);
				}
			}

			listCustAcctWS.removeAll(childrenToRemove);

		}
	}

	/**
	 * Dans le cadre du double play, le méme client/contrat peut apparaitre dans
	 * le produit fixe et internet<br>
	 * Dans ce cas il faut garder qu'un seul des deux:<br>
	 * - S'il existe un client internet "normal" (hors double play donc avec
	 * login renseigné) alors ne garder que le contrat fixe du double play<br>
	 * - S'il n'existe pas de client internet "normal", alors garder le client
	 * internet normal<br>
	 * 
	 * 
	 * @param newChildrenList
	 *            the new children list
	 */
	private void filterDuplicateChildren(final List<CustAcctWS> listCustAcctWS) {

		CustAcctWS previousCustAcct = null;
		String currentContract;

		final ArrayList<CustAcctWS> toRemove = new ArrayList<CustAcctWS>();

		for (final CustAcctWS custAcct : listCustAcctWS) {
			LOG.debug("Traitement: {}", custAcct);
			currentContract = custAcct.getContract() != null ? custAcct
					.getContract().getContractIdBscs() : null;

			if ((previousCustAcct != null)
					&& (previousCustAcct.getContract() != null)
					&& (currentContract != null)) {
				final String previousContract = previousCustAcct.getContract()
						.getContractIdBscs();
				if (currentContract.equals(previousContract)) {
					if (!Integer.valueOf(1).equals(
							previousCustAcct.getIsInternet())
							|| "N/R".equals(previousCustAcct.getContract()
									.getNd())) {
						toRemove.add(previousCustAcct);
					} else {
						toRemove.add(custAcct);
					}
				}
			}
			previousCustAcct = custAcct;
		}

		debug(listCustAcctWS, "Before         : ");
		debug(toRemove, "List to remove : ");
		for (final CustAcctWS remove : toRemove) {
			listCustAcctWS.remove(remove);
		}
		// listCustAcctWS.removeAll(toRemove);
		debug(listCustAcctWS, "After          : ");

	}

	private void debug(final List<CustAcctWS> toDebug, final String toShow) {
		for (final CustAcctWS custAcctWS : toDebug) {
			LOG.debug(toShow + " {}", custAcctWS);
		}
	}

	/**
	 * Rajoute des informations é partir du ND support fixe du client internet
	 * (customerid, contractId, agence, dr, adresse d'installation).<br>
	 * La liste est ordonnée: Client internet d'abord, puis tous les clients
	 * fixes ayant le méme ND (l'actif en premier)<br>
	 * Ensuite, pour chaque client internet, on prend le client fixe qui vient
	 * juste aprés.
	 * 
	 * @param listCustAcctWS
	 *            La liste des clients avec l'information <i>customerIdFixe</i>
	 *            renseignée.
	 */
	private void addFixeCustIdForInternetContracts(
			final List<CustAcctWS> listCustAcctWS) {
		Collections.sort(listCustAcctWS, new CustomComparator(PAR_ND));

		final int length = listCustAcctWS.toArray().length;
		for (int i = 0; i < length - 1; i++) {
			if (isInternet(listCustAcctWS.get(i))) {
				if (!isInternet(listCustAcctWS.get(i + 1))
						&& (listCustAcctWS.get(i + 1).getContract().getNd() != null)
						&& listCustAcctWS
								.get(i + 1)
								.getContract()
								.getNd()
								.equals(listCustAcctWS.get(i).getContract()
										.getNd())) {
					listCustAcctWS.get(i).setCustomerIdFixe(
							listCustAcctWS.get(i + 1).getCustomerId());
					listCustAcctWS
							.get(i)
							.getContract()
							.setContractIdFixe(
									listCustAcctWS.get(i + 1).getContract()
											.getContractIdBscs());
					listCustAcctWS
							.get(i)
							.getContract()
							.setAg(listCustAcctWS.get(i + 1).getContract()
									.getAg());
					listCustAcctWS
							.get(i)
							.getContract()
							.setDr(listCustAcctWS.get(i + 1).getContract()
									.getDr());
					listCustAcctWS
							.get(i)
							.getContract()
							.setAdressInstallation(
									listCustAcctWS.get(i + 1).getContract()
											.getAdressInstallation());
				}
			}
		}

	}

	private boolean isInternet(final CustAcctWS custAcctWS) {
		return (custAcctWS.getIsInternet() != null)
				&& (custAcctWS.getIsInternet() == 1)
				&& !GRCStringUtil.isNullOrEmpty(custAcctWS.getContract()
						.getLoginInternet());
	}

	public static int PAR_ND = 0;
	public static int PAR_CO_ID = 1;
	public static int PAR_STATUT = 2;

	/**
	 * The Class CustomComparator.
	 */
	public class CustomComparator implements Comparator<CustAcctWS> {

		public CustomComparator(final int type) {
			this.type = type;
		}

		public final int type;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(final CustAcctWS custAcctOne,
				final CustAcctWS custAcctTwo) {
			// On classe les fils par ordre de :
			// ND
			// Puis les clients internet en premier
			// Puis les status actif en premier
			String info1 = "";
			String info2 = "";
			if (type == PAR_ND) {
				info1 = custAcctOne.getContract().getNd();
				info2 = custAcctTwo.getContract().getNd();
			} else if (type == PAR_CO_ID) {
				info1 = custAcctOne.getContract().getContractIdBscs();
				info2 = custAcctTwo.getContract().getContractIdBscs();
			} else if (type == PAR_STATUT) {
				info1 = custAcctOne.getContract().getStatus();
				if ((info1 == null) && !custAcctOne.getChildren().isEmpty()) {
					info1 = custAcctOne.getChildren().get(0).getContract()
							.getStatus();
					// Assuming the children are already sorted
				}
				info2 = custAcctTwo.getContract().getStatus();
				if ((info2 == null) && !custAcctTwo.getChildren().isEmpty()) {
					info2 = custAcctTwo.getChildren().get(0).getContract()
							.getStatus();
					// Assuming the children are already sorted
				}
			}
			int result;
			if (info1 == null) {
				result = 1;
			} else if (info2 == null) {
				result = -1;
			} else {
				result = info1.compareTo(info2);
			}

			if ((result == 0) && (type != PAR_STATUT)) {
				final int internet2 = custAcctTwo.getIsInternet() == null ? 0
						: custAcctTwo.getIsInternet();
				final int internet1 = custAcctOne.getIsInternet() == null ? 0
						: custAcctOne.getIsInternet();
				result = internet2 - internet1;
			}
			if ((result == 0) && (type != PAR_STATUT)) {
				result = ("" + custAcctTwo.getContract().getLoginInternet())
						.compareTo(""
								+ custAcctOne.getContract().getLoginInternet());
			}
			if ((result == 0) && (type != PAR_STATUT)) {
				result = custAcctOne.getContract().getStatus()
						.compareTo(custAcctTwo.getContract().getStatus());
			}

			return result;
		}

	}

	/**
	 * Compare un contrat avec les critéres de recherche pour determiner si
	 * celui si correspond ou pas.<br>
	 * Les champs comparés sont:
	 * <ul>
	 * <li>IP</li>
	 * <li>MAIL</li>
	 * <li>LOGIN</li>
	 * <li>CODE CLIENT</li>
	 * <li>CONTRACT ID BSCS</li>
	 * <li>ND</li>
	 * <li>CUST ACCT ID</li>
	 * <li>CCU</li>
	 * <li>PI</li>
	 * </ul>
	 * 
	 * @param tmpChild
	 *            Le client é comparer
	 * @param searchCustBean
	 *            le bean contenant les critéres de recherche du client pour
	 *            comparaison
	 * @return true, si comparaison réussie
	 * @autor capgemini
	 */
	private boolean compareCustAcctBean(final CustAcctWS tmpChild,
			final CustAcctWS searchCustBean) {
		// Compare certains champs de recherche
		LOG.trace("----> compareCustAcctBean tmpChild {} searchCustBean {}",
				tmpChild, searchCustBean);
		Boolean boolAdd = true;

		if (tmpChild == null) {
			boolAdd = false;
		}
		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getIp())
				&& ((tmpChild.getIp() == null) || !tmpChild.getIp().contains(
						searchCustBean.getIp()))) {
			// /&& !GRCStringUtil.isNullOrEmpty(tmpChild.getIp())
			LOG.debug("Le champ IP ne correspond pas");
			boolAdd = false;
		}
		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getMail())
				&& ((tmpChild.getMail() == null) || !tmpChild.getMail()
						.contains(searchCustBean.getMail()))) {
			// && !GRCStringUtil.isNullOrEmpty(tmpChild.getMail())
			LOG.debug("Le champ Mail ne correspond pas");
			boolAdd = false;
		}
		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getContract()
						.getLoginInternet())
				&& ((tmpChild.getContract().getLoginInternet() == null) || !tmpChild
						.getContract()
						.getLoginInternet()
						.contains(
								searchCustBean.getContract().getLoginInternet()))) {
			// && !GRCStringUtil.isNullOrEmpty(tmpChild.getLogin())
			LOG.debug("Le champ Login ne correspond pas");
			boolAdd = false;
		}
		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getContract()
						.getContractIdBscs())
				&& ((tmpChild.getContract().getContractIdBscs() == null) || !tmpChild
						.getContract()
						.getContractIdBscs()
						.contains(
								searchCustBean.getContract()
										.getContractIdBscs()))) {
			// && !GRCStringUtil.isNullOrEmpty(tmpChild.getLogin())
			LOG.debug("Le champ CO_ID ne correspond pas");
			boolAdd = false;
		}

		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean
						.getCustAcctCode())
				&& ((tmpChild.getCustAcctCode() == null) || !tmpChild
						.getCustAcctCode().contains(
								searchCustBean.getCustAcctCode()))) {
			LOG.trace("Le champ CustAcctCode ne correspond pas");
			boolAdd = false;
		}
		if (boolAdd && (searchCustBean.getContract() != null)
				&& (tmpChild.getContract() != null)) {
			if (!GRCStringUtil.isNullOrEmpty(searchCustBean.getContract()
					.getContractIdBscs())
					&& !searchCustBean.getContract().getContractIdBscs()
							.equals(tmpChild.getContract().getContractIdBscs())) {
				LOG.trace("Le champ ContractIdBscs ne correspond pas");
				boolAdd = false;
			}

		}

		if (boolAdd && (searchCustBean.getContract() != null)
				&& (tmpChild.getContract() != null)) {
			if (!GRCStringUtil.isNullOrEmpty(searchCustBean.getContract()
					.getNd())
					&& !searchCustBean.getContract().getNd()
							.equals(tmpChild.getContract().getNd())) {
				LOG.trace("Le champ Nd ne correspond pas");
				if ((tmpChild.getContract().getNd() == null)
						|| !tmpChild
								.getContract()
								.getNd()
								.equals(tmpChild.getContract()
										.getLoginInternet())) {
					boolAdd = false;
				}
			}

		}
		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getCustomerId())
				&& !searchCustBean.getCustomerId().equals(
						tmpChild.getCustomerId())) {
			LOG.debug("Le champ CustomerId ne correspond pas");
			boolAdd = false;
		}

		// if (boolAdd && !GRCStringUtil.isNullOrEmpty(searchCustBean.getCcu())
		// && !searchCustBean.getCcu().equals(tmpChild.getCcu())) {
		// LOG.debug("Le champ Ccu ne correspond pas");
		// boolAdd = false;
		// }
		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean
						.getIdentityNumber())
				&& !searchCustBean.getIdentityNumber().equals(
						tmpChild.getIdentityNumber())) {
			LOG.debug("Le champ IdentityNumber ne correspond pas");
			boolAdd = false;
		}

		if (boolAdd
				&& !GRCStringUtil.isNullOrEmpty(searchCustBean.getCustomerId())
				&& !searchCustBean.getCustomerId().equals(
						tmpChild.getCustomerId())) {
			LOG.debug("Le champ getCustomerId ne correspond pas");
			boolAdd = false;
		}

		LOG.trace("<---- compareCustAcctBean boolAdd {}", boolAdd);
		return boolAdd;
	}

	/**
	 * Cherche les client pére du client demandé.<br>
	 * remonte jusqu'au premier client pére (recursive).<br>
	 * La recherche est effectuée dans la liste fournie avant de passer en BDD.
	 * 
	 * @param childCustAcctWS
	 *            le client fils
	 * @param listCustAcct
	 *            la liste des clients péres déjé remontée
	 * @param iCustAcctDAO
	 *            le DAO utilisé
	 * @return le pére du client
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	private CustAcctWS findFatherCustAcctWS(final CustAcctWS childCustAcctWS,
			final List<CustAcctWS> listCustAcct,
			final ICustAcctDAO iCustAcctDAO, final int product)
			throws GrcWsException {
		// Recherche le pére de childCustAcctWS dans listCustAcct
		// Si trouvé, il le rajoute parmi les fils du pére
		// Si non trouvé, il cherche le pére dans la base de données et le
		// rajoute é la liste
		LOG.trace("----> findFatherCustAcctWS childCustAcctWS {}",
				childCustAcctWS);

		CustAcctWS fatherCustAcctWS = null;

		// si pas de parent, ou la liste est vide, on ne fait rien
		if ((childCustAcctWS != null)
				&& (childCustAcctWS.getCustomerIdHigh() != null)
				&& (Integer.valueOf(childCustAcctWS.getCustomerIdHigh()) != 0)
				&& (listCustAcct != null)) {
			// recherche dans la liste si un pére correspond déjé
			fatherCustAcctWS = findFatherInList(childCustAcctWS, listCustAcct);
			if (fatherCustAcctWS == null) {
				// pas dans la liste on le cherche dans la bdd
				fatherCustAcctWS = new CustAcctWS();
				fatherCustAcctWS.setCustomerId(childCustAcctWS
						.getCustomerIdHigh());
				List<CustAcctWS> listCustFather = iCustAcctDAO
						.findContractsForCustomer(fatherCustAcctWS, true,
								product);

				if (((listCustFather == null) || listCustFather.isEmpty())
						&& !GRCStringUtil.isNullOrEmpty(fatherCustAcctWS
								.getCustomerId())) {
					listCustFather = iCustAcctDAO.findCustomerByCustomerId(
							fatherCustAcctWS.getCustomerId(), product);
				}

				if ((listCustFather == null) || listCustFather.isEmpty()) {
					fatherCustAcctWS = null;
				} else {
					// on rajoute son fils
					fatherCustAcctWS = listCustFather.get(0);
					fatherCustAcctWS.getChildren().add(childCustAcctWS);
					// si lui méme a un pére, alors re..
					if ((fatherCustAcctWS.getCustomerIdHigh() != null)
							&& (Integer.valueOf(fatherCustAcctWS
									.getCustomerIdHigh()) != 0)) {
						fatherCustAcctWS = findFatherCustAcctWS(
								fatherCustAcctWS, listCustAcct, iCustAcctDAO,
								product);
					}
				}
			}
			if (fatherCustAcctWS != null) {
				childCustAcctWS.setCustAcctCodeHigh(fatherCustAcctWS
						.getCustAcctCode());
			}
		}
		LOG.trace("<---- findFatherCustAcctWS fatherCustAcctWS {}",
				fatherCustAcctWS);
		return fatherCustAcctWS;
	}

	/**
	 * Cherche le pére du client dans la liste donnée.
	 * 
	 * @param childCustAcctWS
	 *            le client fils
	 * @param listCustAcct
	 *            la liste des clients péres
	 * @return the cust acct ws
	 */
	private CustAcctWS findFatherInList(final CustAcctWS childCustAcctWS,
			final List<CustAcctWS> listCustAcct) {
		CustAcctWS custAcctWSResult = null;

		final String customerId = childCustAcctWS.getCustomerIdHigh();
		for (final CustAcctWS custAcctWS : listCustAcct) {
			if (custAcctWS.getCustomerId().equals(customerId)) {
				custAcctWS.getChildren().add(childCustAcctWS);
				custAcctWSResult = custAcctWS;
			} else if ((custAcctWS.getChildren() != null)
					&& !custAcctWS.getChildren().isEmpty()) {
				final CustAcctWS result = findFatherInList(childCustAcctWS,
						custAcctWS.getChildren());
				if (result != null) {
					custAcctWSResult = custAcctWS;
				}
			}
			if (custAcctWSResult != null) {
				break;
			}
		}
		return custAcctWSResult;
	}

	public List<CustSearchWS> searchCustomer(SearchCustomerWS searchCustomerWS,
			int product) throws GrcWsException {
		
			LOG.info("-> searchCustomer: searchCustomerWS {}",
					searchCustomerWS.toString());
			if (GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCo_id())
					&& GRCStringUtil.isNullOrEmpty(searchCustomerWS
							.getCodeCategorie())
					&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNd())
					&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNom())
					&& GRCStringUtil.isNullOrEmpty(searchCustomerWS
							.getNumPieceIdentite())
					&& GRCStringUtil
							.isNullOrEmpty(searchCustomerWS.getPrenom())
					&& GRCStringUtil.isNullOrEmpty(searchCustomerWS.getVille())) {
				throw new GrcWsException(
						ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
						"au moins un critére doit étre renseignée");

			}
			try {
			final ICustAcctDAO iCustAcctDAO = getCustAcctDAOForProduct(product);
			List<CustSearchWS> listCustomers = iCustAcctDAO
					.searchCustomers(searchCustomerWS);
			if (product != ProduitTypeEnum.MOBILE.getIdTypeProduit()) {
				List<Long> list = new ArrayList<Long>();
				for (CustSearchWS custSearchWS : listCustomers) {
					if (WSValidator
							.validateNumber(custSearchWS.getCustomerId()))
						list.add(new Long(custSearchWS.getCustomerId()));
				}

				List<CustSearchWS> listAgenceCustomers = iCustAcctDAO
						.searchAgenceCustomers(list);

				for (CustSearchWS custSearchWS : listCustomers) {
					for (CustSearchWS cust : listAgenceCustomers) {
						if (cust.getCustomerId().equals(
								custSearchWS.getCustomerId())) {
							custSearchWS.setCodeAgence(cust.getCodeAgence());
							custSearchWS.setLibelleAgence(cust
									.getLibelleAgence());
							break;
						}
					}
				}
			}
			return listCustomers;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw new GrcWsException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"veuillez contactez l'administrateur");
		}
	}

	public List<CustFindContractWS> findContract(String custcode,
			String customerId, String nd, String coId, boolean hierarchique,
			int produit) throws GrcWsException {
		LOG.info("-> findContract: custcode , customer_id ", custcode + " "
				+ customerId);
		List<CustFindContractWS> list = null;
		if (GRCStringUtil.isNullOrEmpty(customerId)
				&& GRCStringUtil.isNullOrEmpty(custcode)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"custCode ou custoerId doit étre renseignée");

		}
		final ICustAcctDAO iCustAcctDAO = getCustAcctDAOForProduct(produit);

		if (hierarchique) {
			if (GRCStringUtil.isNullOrEmpty(customerId)) {
				customerId = iCustAcctDAO.getCustomerByCustCode(custcode);
			}
			if (!WSValidator.validateNumber(customerId))
				return list;
			List<String> listCustomer = new ArrayList<String>();
			listCustomer.add(customerId);
			List<String> listCustomerChild = new ArrayList<String>();
			listCustomerChild.add(customerId);
			do {

				List<Map<String, Object>> listMap = iCustAcctDAO
						.findChildCustomer(listCustomerChild);
				listCustomerChild = new ArrayList<String>();
				for (Map<String, Object> map : listMap) {
					for (Object obj : map.values()) {
						listCustomerChild.add(String.valueOf(obj));
					}
				}
				listCustomer.addAll(listCustomerChild);
			} while (listCustomerChild != null && !listCustomerChild.isEmpty());
			list = iCustAcctDAO.findContract(listCustomer, nd, coId);
		} else {
			list = iCustAcctDAO.findContract(custcode, customerId, nd, coId);
		}
		return list;
	}
}
