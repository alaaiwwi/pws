/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.fidelio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.bean.FidelioAccountWS;
import ma.iam.pws.grc.bean.FidelioActionHistWS;
import ma.iam.pws.grc.bean.FidelioOrderDetailWS;
import ma.iam.pws.grc.bean.FidelioOrderHistWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

/**
 * The Class FidelioDAOFixeImpl.
 */
@Repository
public class FidelioDAOFixeImpl extends BaseDAOFixe implements IFidelioDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(FidelioDAOFixeImpl.class);

	/** La constante GETFIDELIOSTATUS. */
	public static final String GETFIDELIOSTATUS = FidelioDAOFixeImpl.class
			.getName() + ".GETFIDELIOSTATUS";

	/** La constante GETFIDELIACTHISTORY. */
	public static final String GETFIDELIOACTHISTORY = FidelioDAOFixeImpl.class
			.getName() + ".GETFIDELIOACTHISTORY";

	/** La constante GETFIDELIOORDHIST. */
	public static final String GETFIDELIOORDHIST = FidelioDAOFixeImpl.class
			.getName() + ".GETFIDELIOORDHIST";

	/** La constante GETFIDELIOORDDETAIL. */
	public static final String GETFIDELIOORDDETAIL = FidelioDAOFixeImpl.class
			.getName() + ".GETFIDELIOORDDETAIL";

	/*
	 * f.tatbi FC 5864 Add parameter costumer_id
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioAccount(java.
	 * lang.String, int)
	 */

	public FidelioAccountWS findFidelioAccount(final String customer_id,
			final String custCode) {
		LOG.debug("--> findFidelioAccount custCode {}", custCode);
		FidelioAccountWS fidelioAccountWS = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETFIDELIOSTATUS));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("custCode", custCode);

		final CustomBeanPropertyRowMapper<FidelioAccountWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioAccountWS>(
				FidelioAccountWS.class);

		if (!GRCStringUtil.isNullOrEmpty(customer_id))
			sql.append(" AND o.ORGANIZATION_LEGACY_ID='" + customer_id + "'");

		if (!GRCStringUtil.isNullOrEmpty(custCode))
			sql.append(" AND FIS.num_client ='" + custCode + "'");

		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		final List<FidelioAccountWS> listFidelioAccountWS = super
				.getNamedParameterJdbcTemplateNeto().query(sql.toString(),
						invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		if ((listFidelioAccountWS != null) && !listFidelioAccountWS.isEmpty()) {
			fidelioAccountWS = listFidelioAccountWS.get(0);
		}

		LOG.debug("<-- findFidelioAccount");
		return fidelioAccountWS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioActionHist(java
	 * .lang.String, int)
	 */

	public List<FidelioActionHistWS> findFidelioActionHist(
			final String customer_id, final String custCode) {
		LOG.debug("--> findFidelioActionHist custCode {}", custCode);
		List<FidelioActionHistWS> listFidelioActionHistWS = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETFIDELIOACTHISTORY));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("custCode", custCode);

		final CustomBeanPropertyRowMapper<FidelioActionHistWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioActionHistWS>(
				FidelioActionHistWS.class);
		if (!GRCStringUtil.isNullOrEmpty(customer_id))
			sql.append(" AND o.ORGANIZATION_LEGACY_ID='" + customer_id + "'");

		if (!GRCStringUtil.isNullOrEmpty(custCode))
			sql.append(" AND FIS.num_client ='" + custCode + "'");

		sql.append(" ORDER BY action_date DESC");
		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		listFidelioActionHistWS = super.getNamedParameterJdbcTemplateNeto()
				.query(sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findFidelioActionHist");
		return listFidelioActionHistWS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioOrderHist(java
	 * .lang.String, int)
	 */

	public List<FidelioOrderHistWS> findFidelioOrderHist(
			final String customer_id, final String custCode) {
		LOG.debug("--> findFidelioOrderHist custCode {}", custCode);
		List<FidelioOrderHistWS> listFidelioOrderHistWS = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETFIDELIOORDHIST));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("custCode", custCode);

		final CustomBeanPropertyRowMapper<FidelioOrderHistWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioOrderHistWS>(
				FidelioOrderHistWS.class);
		if (!GRCStringUtil.isNullOrEmpty(customer_id))
			sql.append(" AND o.ORGANIZATION_LEGACY_ID='" + customer_id + "'");

		if (!GRCStringUtil.isNullOrEmpty(custCode))
			sql.append(" AND FIS.num_client ='" + custCode + "'");

		sql.append(" ORDER BY 1 DESC	");
		
		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		listFidelioOrderHistWS = super.getNamedParameterJdbcTemplateNeto()
				.query(sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findFidelioOrderHist");
		return listFidelioOrderHistWS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.fidelio.IFidelioDAO#findFidelioOrderDetail(java
	 * .lang.String, int)
	 */

	public List<FidelioOrderDetailWS> findFidelioOrderDetail(
			final String orderId) {
		LOG.debug("--> findFidelioOrderDetail orderId {}", orderId);
		List<FidelioOrderDetailWS> listFidelioOrderDetailWS = null;
		final String sql = CustomSQLUtil.get(GETFIDELIOORDDETAIL);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("orderId", orderId);

		final CustomBeanPropertyRowMapper<FidelioOrderDetailWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<FidelioOrderDetailWS>(
				FidelioOrderDetailWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		listFidelioOrderDetailWS = super.getNamedParameterJdbcTemplateNeto()
				.query(sql, namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findFidelioOrderDetail");
		return listFidelioOrderDetailWS;
	}

	public void setQualiteFidelio(final List<CustAcctWS> listCustAcctWS) {
		throw new NotImplementedException(
				"Qualité Fidelio non disponible pour le produit Fixe");

	}

}
