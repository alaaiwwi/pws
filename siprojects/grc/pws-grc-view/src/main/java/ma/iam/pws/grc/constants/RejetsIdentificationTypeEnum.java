/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * Enumération contenant pour chaque paramétre et action_id, le type de ticket é
 * créer ainsi que le commentaire é rajouter é l'action<br>
 * Attention: l'ordre des elements est important:
 */
public enum RejetsIdentificationTypeEnum {

    NO_ERROR("NO ERROR", RejetsIdentificationConstantes.NO_TYPE_ERROR), //
    VMS("VMS", RejetsIdentificationConstantes.VMS_TYPE_ERROR), //
    KPSAIN_00("KPSAIN2-ERROR(00) Fichier OUT de la commande CIB est mal formate", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    KPSAIN_55("KPSAIN2-ERROR(55) MSISDN est introuvable", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    KPSAIN_56("KPSAIN2-ERROR(56) Erreur retournee par l IN", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    KPSAIN("HUAWEI-OCS", RejetsIdentificationConstantes.IN_TYPE_ERROR), //
    MMS("MMS", RejetsIdentificationConstantes.MMS_TYPE_ERROR), //
    DASP("DASP", RejetsIdentificationConstantes.DASP_TYPE_ERROR), //
    IMPS("IMPS", RejetsIdentificationConstantes.IMPS_TYPE_ERROR), //
    PCRF("PCRF", RejetsIdentificationConstantes.PCRF_TYPE_ERROR), //
    MSISDN_ALREADY_EXISTS("MSISDN already exists in the BDD", RejetsIdentificationConstantes.MTV_TYPE_ERROR), //
    MSISDN_DOES_NOT_EXIST("MSISDN does not exist on the BDD", RejetsIdentificationConstantes.MTV_TYPE_ERROR), //
    MSISDN_IS_NOT_VALID("MSISDN is not valid in the BDD", RejetsIdentificationConstantes.MTV_TYPE_ERROR), //
    KPSAPOC("KPSAPOC-ERROR(020) MDN ou MIN non existant dans la BDD PoC", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    HLR_NOKIA("hlr nokia 4-Cart session is unavailable", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    MMSI_CART("mmsi-Cart session is unavailable", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    MMSI_PROBLEME("mmsi-Probleme de connexion Maximum tentatives de connexion atteint", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    HLR_CART("HLR Cart connection failed", RejetsIdentificationConstantes.PLATFORM_TYPE_ERROR), //
    CONNEXION_PF("Erreur de connexion PF", RejetsIdentificationConstantes.PLATFORM_TYPE_ERROR), //
    BLACKBERRY_RIM("KPSAé BlackBerry RIM - Code Erreur (61040) inconnue", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    JETMULTIMEDIA_406("KPSAé JetMultiMedia - (406) - Type d operation invalide", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    JETMULTIMEDIA_407("KPSAé JetMultiMedia - (407) - Service inconnu", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    KPSA_UNKNOWN_RROR("KPSA UNKNOWN ERROR", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    KPSA_ERROR("KPSA-ERROR(1) Operation inconnue sur XEV 25", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    KPSA_3003("KPSA (3003)  HLR Huawei - Template non definie", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    KPSA_ERROR_GPRS("KPSA-ERROR(1) GPRS Data service ne peut pas etre supprime pour ce contrat", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    KPSA_ERROR_SPEAT("KPSA-ERROR(1) GPRS-Data services ou SPEAT, SPEDT ne sont pas trouves ", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    KPSA_ERROR_MSISDN("KPSA-ERROR(1) MSISDN introuvable", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    KPSA_ERROR_FOM("KPSA-ERROR(1) Plusieurs FoM services trouves sur le contract", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    KPSA_ERROR_AHAH("KPSA-ERROR(1) Service inconnue AH//AH", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    KPSAIN2_ERROR("KPSAIN2-ERROR(1) IN destination address not found", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    OK_IN("OK-IN INDEX not defined in SOD- ", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    NPTE("NPTE Parsing error ...", RejetsIdentificationConstantes.USER_TYPE_ERROR), //
    FOP("FOP Parsing error", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    ABONNE_NON_RECONNU("Abonne non reconnu dans l HLR", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    ABONNE_EXISTE_DEJA("Abonne existe deja sur l HLR", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    CLIENT_DEJA_EXISTANT("Client deja existant", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    MSISDN_DEJA_ATTRIBUE("MSISDN deja attribue", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_006("KPSA (006)  HLR Nokia - Service basique existe deja", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    CLIENT_INEXISTANT("Client inexistant", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    CARTE_SIM_INCONNUE("Carte SIM inconnue au HLR", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    TYPE_DE_CARTE_INVALIDE("Type de carte invalide", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    CARTE_NON_AUTHENTIFIEE("Carte non authentifiee", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    AUC("AUC", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_001("KPSA (001)  HLR Nokia - Abonne non reconnu dans l HLR", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_022("KPSA (022)  HLR Nokia - Erreur DX inconnue", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_3001("KPSA (3001)  HLR Huawei - Client inexistant", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_3007("KPSA (3007)  HLR Huawei - Enregistrement non defini", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_3016("KPSA (3016)  HLR Huawei - Service basique non active", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    KPSA_3007_X("KPSA (3007)  HLR Huawei - Enregistrement non defini", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    ERREUR_INTERNE_X("Erreur interne dans la base des donnees", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    INCOMPATIBILITE_SERVICES_X("Incompatibilite entre services supplementaires", RejetsIdentificationConstantes.HLR_TYPE_ERROR), //
    IN_NON_DEFINI("numero plateforme IN non defini", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    PROCESS_HLR("processHLR Parsing error", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    PROCESS_NIFI("processNIFI Parsing error", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    BSG_PARAMETER("OK-BSG PARAMETER not defined in SOD", RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //
    AUTRE(null, RejetsIdentificationConstantes.KPSA_TYPE_ERROR), //

    ;

    /** The code category type. */
    private final String prmValue;

    /** The desc category type. */
    private final String typeId;

    /**
     * Instantiates a new cust acct category type enum.
     * 
     * @param codeCategoryType
     *            the code category type
     * @param descCategoryType
     *            the desc category type
     */
    RejetsIdentificationTypeEnum(final String prmValue, final String typeId) {
        this.prmValue = prmValue;
        this.typeId = typeId;
    }

    /**
     * @return the prmValue
     */
    public String getPrmValue() {
        return prmValue;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

}
