/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class ContractStatusWS.
 */
public class ContractStatusWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 3658887720768140722L;

    /** The status. */
    private String status;

    /** The nombre. */
    private Integer nombre;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ContractStatusWS:");
        sb.append(status);
        if (status != null) {
            sb.append(",status=").append(status);
        }
        if (nombre != null) {
            sb.append(",nombre=").append(nombre);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets l'identifiant client BSCS (CUSTOMER_ID).
     * 
     * @return l'identifiant client BSCS (CUSTOMER_ID)
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets l'identifiant client BSCS (CUSTOMER_ID).
     * 
     * @param status
     *            the new cust acct id
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Gets the cust acct category.
     * 
     * @return the cust acct category
     */
    public Integer getNombre() {
        return nombre;
    }

    /**
     * Sets the cust acct category.
     * 
     * @param nombre
     *            the new cust acct category
     */
    public void setNombre(final Integer nombre) {
        this.nombre = nombre;
    }

}
