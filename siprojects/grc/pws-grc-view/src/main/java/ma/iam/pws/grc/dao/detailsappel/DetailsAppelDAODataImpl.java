/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.detailsappel;

import java.util.List;

import ma.iam.pws.grc.bean.CustomDetailWS;
import ma.iam.pws.grc.bean.DetailsAppelWS;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * The Class DetailsAppelMobileImpl.
 */
@Repository
public class DetailsAppelDAODataImpl extends BaseDAOFixe implements IDetailsAppelDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(DetailsAppelDAODataImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.detailsappel.IDetailsAppelDAO#getDetailsAppels
     * (java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    
    public List<DetailsAppelWS> getDetailsAppels(final String customerId, final String contractIdBscs, final String dateDebut, final String dateFin) {
        LOG.debug("-- getDetailsAppels contractIdBsc {} customerId {} dateDebut " + dateDebut + " dateFin " + dateFin, contractIdBscs, customerId);
        throw new NotImplementedException("Détails des appels pour le produit DATA non disponible");
    }

	public CustomDetailWS getContract(String contractIdBscs) {
		 LOG.debug("-- isContractExist contractIdBsc {}  ", contractIdBscs);
	        throw new NotImplementedException("Détails des appels pour le produit DATA non disponible");
	}

	

}
