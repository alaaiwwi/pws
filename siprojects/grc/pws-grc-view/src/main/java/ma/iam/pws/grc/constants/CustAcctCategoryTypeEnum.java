/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * The Enum CustAcctCategoryTypeEnum.
 */
public enum CustAcctCategoryTypeEnum {

    /** The RESIDENTIEL. */
    RESIDENTIEL("CUSTCAT1", "RESIDENTIEL"),

    /** The PROFESSIONNEL. */
    PROFESSIONNEL("CUSTCAT2", "PROFESSIONNEL"),

    /** The ENTREPRISE. */
  //  ENTREPRISE("CUSTCAT3", "ENTREPRISE"),

    /** The GRAND s_ comptes. */
//    GRANDS_COMPTES("CUSTCAT4", "GRANDS_COMPTES"),

    /** The OFFICIELS. */
    OFFICIELS("CUSTCAT6", "Officiels"),

    /** The AMBASSADES. */
    AMBASSADES("CUSTCAT9", "Ambassades"),

    /** The VIP. */
    VIP("CUSTCAT61", "VIP"),

    /** The VI p_ residentiel. */
    VIP_RESIDENTIEL("CUSTCAT62", "VIP résidentiel"),

    /** The VI p_ professionnel. */
    VIP_PROFESSIONNEL("CUSTCAT63", "VIP professionnel"),

    /** The VI p_ entreprise. */
    VIP_ENTREPRISE("CUSTCAT64", "VIP entreprise"),

    /** The DEFAUL t_ categor y_ degrade. */
    DEFAULT_CATEGORY_DEGRADE("CUSTCATGRC", "Catégorie par défaut(Mode dégradé)"),
    
    /*
     * f.tatbi FC 5864 add ENTREPIRSE and GRAND public
     */
    
    ENTREPRISE("Entreprises","Entreprises"),
    GRANDS_COMPTES("Grands comptes","Grands comptes");

    /** The code category type. */
    private final String codeCategoryType;

    /** The desc category type. */
    private final String descCategoryType;

    /**
     * Instantiates a new cust acct category type enum.
     * 
     * @param codeCategoryType
     *            the code category type
     * @param descCategoryType
     *            the desc category type
     */
    CustAcctCategoryTypeEnum(final String codeCategoryType, final String descCategoryType) {
        this.codeCategoryType = codeCategoryType;
        this.descCategoryType = descCategoryType;
    }

    /**
     * Gets the code category type.
     * 
     * @return the code category type
     */
    public String getCodeCategoryType() {
        return codeCategoryType;
    }

    /**
     * Gets the desc category type.
     * 
     * @return the desc category type
     */
    public String getDescCategoryType() {
        return descCategoryType;
    }

}
