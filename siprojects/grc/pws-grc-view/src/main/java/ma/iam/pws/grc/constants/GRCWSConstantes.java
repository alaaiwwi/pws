/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * The Class GRCWSConstantes.
 */
public final class GRCWSConstantes {

    /**
     * Instantiates a new gRCWS constantes.
     */
    private GRCWSConstantes() {
    }

    /** The Constant MAX_RETRIEVED_CUSTOMERS. */
    public static final int MAX_RETRIEVED_CUSTOMERS = 100;

    /** The Constant HASH_SEED1. */
    public static final int HASH_SEED1 = 17;

    /** The Constant HASH_SEED2. */
    public static final int HASH_SEED2 = 37;

    /** The Constant EXEC_QUERY. */
    public static final String EXEC_QUERY = "Execution de la requéte {}";

    /** The Constant EXEC_QUERY_NETO. */
    public static final String EXEC_QUERY_NETO = "Execution de la requéte NETO {}";

    public static final String EXEC_QUERY_FIDELIO = "Execution de la requéte FIDELIO {}";

    /** The Constant EXEC_QUERY_END. */
    public static final String EXEC_QUERY_END = "Fin de l'execution de la requéte";

    /** The Constant EXEC_QUERY_END_RESULT. */
    public static final String EXEC_QUERY_END_RESULT = "Fin de l'execution de la requéte --> result [{}]";

    /** La constante CONTRACTIDBSCS_INCORRECT. */
    public static final String CONTRACTIDBSCS_INCORRECT = "Identifiant contrat BSCS incorrect : ";

    /** La constante CONTRACTIDBSCS_INCORRECT. */
    public static final String ND_INCORRECT = "ND incorrect : ";

    /** The Constant PHONE_NUMBER_PREFIX = "212" */
    public static final String PHONE_NUMBER_PREFIX = "212";

    /** The Constant PHONE_NUMBER_DIGITS = 10 */
    public static final int PHONE_NUMBER_DIGITS = 10;

    /** The Constant PHONE_NUMBER_WITH_PREFIX_DIGITS = 12 */
    public static final int PHONE_NUMBER_WITH_PREFIX_DIGITS = 12;

    /** The Constant PHONE_NUMBER_PREFIX_LENGTH = 3 */
    public static final int PHONE_NUMBER_PREFIX_LENGTH = 3;

    /** The Constant C0_CLASS. */
    public static final String C0_CLASS = "C0";

    /** The Constant C2_CLASS. */
    public static final String C1_CLASS = "C1";

    /** The Constant C3_CLASS. */
    public static final String C2_CLASS = "C2";

    /** The Constant C4_CLASS. */
    public static final String C3_CLASS = "C3";

    /** The Constant C0_GTR. */
    public static final String C0_GTR = "8 heures ouvrées (HO)";

    /** The Constant C1_GTR. */
    public static final String C1_GTR = "4 heures ouvrées (HO)";

    /** The Constant C2_GTR. */
    public static final String C2_GTR = "4 heures 24/24 7/7";

    /** The Constant C3_GTR. */
    public static final String C3_GTR = "2 heures ouvrées, 4 heures hors heures ouvrées";

    /** The Constant C0_GTD. */
    public static final String C0_GTD = "99.95% HO";

    /** The Constant C1_GTD. */
    public static final String C1_GTD = "99.95% HO";

    /** The Constant C2_GTD. */
    public static final String C2_GTD = "99.96% 24h/24";

    /** The Constant C3_GTD. */
    public static final String C3_GTD = "99.97% 24h/24";

    /** The Constant C0_DELAI. */
    public static final String C0_DELAI = "4";

    /** The Constant C1_DELAI. */
    public static final String C1_DELAI = "2";

    /** The Constant C2_DELAI. */
    public static final String C2_DELAI = "2";

    /** The Constant C3_DELAI. */
    public static final String C3_DELAI = "1";

    /** The Constant TYPE_L1_ADRESS. */
    public static final String TYPE_L1_ADRESS = "1307";

    /** The Constant TYPE_L2_ADRESS. */
    public static final String TYPE_L2_ADRESS = "1308";

    /** The Constant COMMUNE_PARAM_ID. */
    public static final String COMMUNE_PARAM_ID = "3000";

    /** The Constant QUARTIER_PARAM_ID. */
    public static final String QUARTIER_PARAM_ID = "3001";

    /** The Constant VOIE_PARAM_ID. */
    public static final String VOIE_PARAM_ID = "3002";

    /** The Constant COMPLT_PARAM_ID. */
    public static final String COMPLT_PARAM_ID = "3010";

    /** The Constant QUARTIER_2_PARAM_ID. */
    public static final String QUARTIER_2_PARAM_ID = "3024";

    /** The Constant VOIE_2_PARAM_ID. */
    public static final String VOIE_2_PARAM_ID = "3025";

    /** The Constant N_VOIE_PARAM_ID. */
    public static final String N_VOIE_PARAM_ID = "3003";

    /** The Constant N_ETAGE_PARAM_ID. */
    public static final String N_ETAGE_PARAM_ID = "3006";

    /** The Constant VIDE_PARAM. */
    public static final String VIDE_PARAM = "XXXX";

    /** The Constant VIDE_2_PARAM. */
    public static final String VIDE_2_PARAM = "NONE";

    /** The Constant COMMUNE. */
    public static final String COMMUNE = "commune";

    /** The Constant QUARTIER. */
    public static final String QUARTIER = "quartier";

    /** The Constant VOIE. */
    public static final String VOIE = "voie";

    /** The Constant CUST_ACCT_ID. */
    public static final String CUSTOMER_ID = "customerId";

    /** Le code de l'information LOGIN dans NETO : RA110 */
    public static final String LOGIN_NETO_CODE = "'RA110','RA117','RA130'";

    /** Les code des informations MAIL dans NETO */
    public static final String MAIL_NETO_CODE = "RS150";
    public static final String MAIL_SUPP_NETO_CODE = "RS151";
    public static final String MAIL_SUPP_IND_NETO_CODE = "RS160";
    public static final String MAIL_SUPP_PAQ_LL_NETO_CODE = "RS174";
    public static final String MAIL_WEB_NETO_CODE = "RS164";
    public static final String MAIL_LL_NETO_CODE = "RS165";
    public static final String MAIL_SUPP_LL_NETO_CODE = "RS166";
    public static final String MAIL_WEB_LL_NETO_CODE = "RS169";

    public static final String QUOTE = "'";
    public static final String COMMA = ",";

    public static final String MAIL_CODES;

    static {
        final StringBuilder sbNetoMailCode = new StringBuilder();
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_SUPP_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_SUPP_IND_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_SUPP_PAQ_LL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_WEB_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_LL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_SUPP_LL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoMailCode.append(QUOTE).append(GRCWSConstantes.MAIL_WEB_LL_NETO_CODE).append(QUOTE);
        MAIL_CODES = sbNetoMailCode.toString();
    }

    /** Le code de l'information IP dans NETO : RS172 */
    public static final String IP_NETO_CODE = "'RS172'";

    /** Les code des informations DNS dans NETO */
    public static final String DNS_ND_NATIONAL_NETO_CODE = "VP231";
    public static final String DNS_ND_INTERNATIONAL_NETO_CODE = "VP232";
    public static final String DNS_NATIONAL_NETO_CODE = "VP316";
    public static final String DNS_INTERNATIONAL_NETO_CODE = "VP326";
    public static final String DNS_NATIONAL_GRATUIT_NETO_CODE = "VP327";
    public static final String DNS_INTERNATIONAL_GRATUIT_NETO_CODE = "VP328";
    public static final String DNS_GROS_NETO_CODE = "VP342";

    public static final String DNS_CODES;

    static {
        final StringBuilder sbNetoCode = new StringBuilder();
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_ND_NATIONAL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_ND_INTERNATIONAL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_NATIONAL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_INTERNATIONAL_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_NATIONAL_GRATUIT_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_INTERNATIONAL_GRATUIT_NETO_CODE).append(QUOTE).append(COMMA);
        sbNetoCode.append(QUOTE).append(GRCWSConstantes.DNS_GROS_NETO_CODE).append(QUOTE);
        DNS_CODES = sbNetoCode.toString();
    }

    /** La liste des codes de l'information DNS dans NETO (Différent des OCC) */

    /** Constants URI authentification web services */
    public static final String URI_CUSTOMER_SERVICE = "http://service.grc.iam.capgemini.fr/customer/auth";
    public static final String URI_CALL_HISTORY_SERVICE = "http://service.grc.iam.capgemini.fr/callhistory/auth";
    public static final String URI_CONTRACT_HISTORY_SERVICE = "http://service.grc.iam.capgemini.fr/contracthistory/auth";
    public static final String URI_DEMANDE_SERVICE = "http://service.grc.iam.capgemini.fr/demande/auth";
    public static final String URI_FEES_SERVICE = "http://service.grc.iam.capgemini.fr/fees/auth";
    public static final String URI_FIDELIO_SERVICE = "http://service.grc.iam.capgemini.fr/fidelio/auth";
    public static final String URI_HLR_SERVICE = "http://service.grc.iam.capgemini.fr/hlr/auth";
    public static final String URI_INVOICE_SERVICE = "http://service.grc.iam.capgemini.fr/invoice/auth";
    public static final String URI_MEMO_SERVICE = "http://service.grc.iam.capgemini.fr/memo/auth";
    public static final String URI_MICROCELL_SERVICE = "http://service.grc.iam.capgemini.fr/microcell/auth";
    public static final String URI_NOTIFICATION_SERVICE = "http://service.grc.iam.capgemini.fr/notification/auth";
    public static final String URI_STAT_SERVICE = "http://service.grc.iam.capgemini.fr/stat/auth";
    public static final String URI_SERVICE_SERVICE = "http://service.grc.iam.capgemini.fr/service/auth";
    public static final String URI_REJETS_SERVICE = "http://service.grc.iam.capgemini.fr/rejetsmediation/auth";
    public static final String URI_ADSL_SERVICE = "http://service.grc.iam.capgemini.fr/rejetsadsl/auth";
    public static final String URI_CAD_SERVICE = "http://service.grc.iam.capgemini.fr/ticketcad/auth";

}
