/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class InvoiceDetailWS.
 */
public class InvoiceDetailWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -4466541076850920242L;

    /** The numero. */
    private String numero;

    /** The service. */
    private String service;

    /** The charge. */
    private String charge;

    /** The prix. */
    private String prix;

    /** The montant ht. */
    private String montantHT;

    /** The montant tva. */
    private String montantTVA;

    /** The montant ttc. */
    private String montantTTC;

    /** The id facture. */
    private String idFacture;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the id.
     * 
     * @param numero
     *            the new id
     */
    public void setNumero(final String numero) {
        this.numero = numero;
    }

    /**
     * Gets the service.
     * 
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * Sets the service.
     * 
     * @param service
     *            the new service
     */
    public void setService(final String service) {
        this.service = service;
    }

    /**
     * Gets the charge.
     * 
     * @return the charge
     */
    public String getCharge() {
        return charge;
    }

    /**
     * Sets the charge.
     * 
     * @param charge
     *            the new charge
     */
    public void setCharge(final String charge) {
        this.charge = charge;
    }

    /**
     * Gets the prix.
     * 
     * @return the prix
     */
    public String getPrix() {
        return prix;
    }

    /**
     * Sets the prix.
     * 
     * @param prix
     *            the new prix
     */
    public void setPrix(final String prix) {
        this.prix = prix;
    }

    /**
     * Gets the montant ht.
     * 
     * @return the montant ht
     */
    public String getMontantHT() {
        return montantHT;
    }

    /**
     * Sets the montant ht.
     * 
     * @param montantHT
     *            the new montant ht
     */
    public void setMontantHT(final String montantHT) {
        this.montantHT = montantHT;
    }

    /**
     * Gets the montant tva.
     * 
     * @return the montant tva
     */
    public String getMontantTVA() {
        return montantTVA;
    }

    /**
     * Sets the montant tva.
     * 
     * @param montantTVA
     *            the new montant tva
     */
    public void setMontantTVA(final String montantTVA) {
        this.montantTVA = montantTVA;
    }

    /**
     * Gets the montant ttc.
     * 
     * @return the montant ttc
     */
    public String getMontantTTC() {
        return montantTTC;
    }

    /**
     * Sets the montant ttc.
     * 
     * @param montantTTC
     *            the new montant ttc
     */
    public void setMontantTTC(final String montantTTC) {
        this.montantTTC = montantTTC;
    }

    /**
     * Gets the id facture.
     * 
     * @return the id facture
     */
    public String getIdFacture() {
        return idFacture;
    }

    /**
     * Sets the id facture.
     * 
     * @param idFacture
     *            the new id facture
     */
    public void setIdFacture(final String idFacture) {
        this.idFacture = idFacture;
    }

}
