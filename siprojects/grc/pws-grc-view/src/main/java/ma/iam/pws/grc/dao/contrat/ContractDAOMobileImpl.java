/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.contrat;

import java.util.List;

import ma.iam.pws.grc.bean.ContractHistoryWS;
import ma.iam.pws.grc.bean.PeripheriqueHistoryWS;
import ma.iam.pws.grc.bean.PlanTarifaireHistoryWS;
import ma.iam.pws.grc.bean.PreidentificationWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOMobile;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;

/**
 * The Class ContractDAOMobileImpl.
 */
@Repository
public class ContractDAOMobileImpl extends BaseDAOMobile implements IContractDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(ContractDAOMobileImpl.class);

    /** La constante GET_CONTRACT_HISTORY. */
    public static final String GET_CONTRACT_HISTORY = ContractDAOMobileImpl.class.getName() + ".GET_CONTRACT_HISTORY";

    /** La constante GET_PERIPHERIQUE_HISTORY. */
    public static final String GET_PERIPHERIQUE_HISTORY = ContractDAOMobileImpl.class.getName() + ".GET_PERIPHERIQUE_HISTORY";

    /** La constante GET_PLAN_TARIFAIRE_HISTORY. */
    public static final String GET_PLAN_TARIFAIRE_HISTORY = ContractDAOMobileImpl.class.getName() + ".GET_PLAN_TARIFAIRE_HISTORY";
    
    public static final String GET_PREIDENTIFICATION = ContractDAOMobileImpl.class.getName() + ".GET_PREIDENTIFICATION";
    
    public static final String GET_OM_PREIDENTIFICATION = ContractDAOMobileImpl.class.getName() + ".GET_OM_PREIDENTIFICATION";
    

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findContractHistory
     * (fr.capgemini.iam.grc.bean.CustAcctWS)
     */
    public List<ContractHistoryWS> findContractHistory(final String contractIdBsc) {
        LOG.debug("--> findContractHistory - Mobile contractIdBsc {}", contractIdBsc);
        final List<ContractHistoryWS> listContractHistory;
        final String sql = CustomSQLUtil.get(GET_CONTRACT_HISTORY);

        final SqlParameterSource namedParameters = new MapSqlParameterSource("contractIdBsc", contractIdBsc);

        final CustomBeanPropertyRowMapper<ContractHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<ContractHistoryWS>(ContractHistoryWS.class);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listContractHistory = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custAcctRowMapper);
        LOG.debug("<-- findContractHistory");
        return listContractHistory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.contrat.IContractDAO#findPeripheriqueHistory
     * (java.lang.String)
     */
    public List<PeripheriqueHistoryWS> findPeripheriqueHistory(final String contractIdBscs) {
        LOG.debug("--> findPeripheriqueHistory contractIdBscs {}", contractIdBscs);
        final List<PeripheriqueHistoryWS> listContractHistory;
        final String sql = CustomSQLUtil.get(GET_PERIPHERIQUE_HISTORY);

        final SqlParameterSource namedParameters = new MapSqlParameterSource("contractIdBsc", contractIdBscs);

        final CustomBeanPropertyRowMapper<PeripheriqueHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PeripheriqueHistoryWS>(
                PeripheriqueHistoryWS.class);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listContractHistory = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custAcctRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
        LOG.debug("<-- findPeripheriqueHistory");
        return listContractHistory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.contrat.IContractDAO#findPlanTarifaireHistory
     * (java.lang.String)
     */
    public List<PlanTarifaireHistoryWS> findPlanTarifaireHistory(final String contractIdBsc) {
        LOG.debug("--> findPlanTarifaireHistory contractIdBsc {}", contractIdBsc);
        final List<PlanTarifaireHistoryWS> listContractHistory;
        final String sql = CustomSQLUtil.get(GET_PLAN_TARIFAIRE_HISTORY);

        final SqlParameterSource namedParameters = new MapSqlParameterSource("contractIdBsc", contractIdBsc);

        final CustomBeanPropertyRowMapper<PlanTarifaireHistoryWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PlanTarifaireHistoryWS>(
                PlanTarifaireHistoryWS.class);
        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listContractHistory = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custAcctRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
        LOG.debug("<-- findPlanTarifaireHistory");
        return listContractHistory;
    }
    
	public List<PreidentificationWS> findDonneesPreidentification(
			String contractIdBscs) {
    	 LOG.debug("--> findDonneesPreidentification contractIdBsc {}", contractIdBscs);
         final List<PreidentificationWS> listContractHistory;
         String sql = CustomSQLUtil.get(GET_PREIDENTIFICATION);
         
         final SqlParameterSource namedParameters = new MapSqlParameterSource("contractIdBscs", contractIdBscs);

         final CustomBeanPropertyRowMapper<PreidentificationWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PreidentificationWS>(
        		 PreidentificationWS.class);
         LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
         listContractHistory = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custAcctRowMapper);
         LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
         LOG.debug("<-- findDonneesPreidentification");
         return listContractHistory;
	}
    
    
	public List<PreidentificationWS> findDonneesPreidentificationOM(
			String contractIdBscs) {
    	 LOG.debug("--> findDonneesPreidentificationOM contractIdBsc {}", contractIdBscs);
         final List<PreidentificationWS> listContractHistory;
         String sql = CustomSQLUtil.get(GET_OM_PREIDENTIFICATION);
         
         final SqlParameterSource namedParameters = new MapSqlParameterSource("contractIdBscs", contractIdBscs);

         final CustomBeanPropertyRowMapper<PreidentificationWS> custAcctRowMapper = new CustomBeanPropertyRowMapper<PreidentificationWS>(
        		 PreidentificationWS.class);
         LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
         listContractHistory = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custAcctRowMapper);
         LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
         LOG.debug("<-- findDonneesPreidentificationOM");
         return listContractHistory;
	}    
    
	public ContractHistoryWS getCreatContract(String contractIdBscs) {
		LOG.debug("-- getCreatContract contractIdBsc {} " , contractIdBscs);
        throw new NotImplementedException("details sur le createur du contract est non implementé");
	}

}
