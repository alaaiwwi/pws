/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.sms;

import ma.iam.pws.grc.bean.SmsWS;

/**
 * Interface ISmsDAO pour l'envoi des SMS (insértion de l'information dans BSCS
 * afin qu'elle soit traitée par la suite)
 */
public interface ISmsDAO {

    /**
     * Insére un SMS dans BSCS qui sera envoyé au client<br>
     * .
     * 
     * @param smsBean
     *            le bean contenant les informations du SMS é envoyer
     *            {@link ma.iam.pws.grc.bean.SmsWS}
     * @return true si SMS inséré dans la base de données
     */
    Boolean insertSmsWS(SmsWS smsBean);

}
