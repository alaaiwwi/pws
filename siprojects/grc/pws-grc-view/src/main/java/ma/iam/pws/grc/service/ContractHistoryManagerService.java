/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.ContractHistoryWS;
import ma.iam.pws.grc.bean.PeripheriqueHistoryWS;
import ma.iam.pws.grc.business.ContractHistoryManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class ContractHistoryManagerService.
 */
@WebService(serviceName = "ContractHistoryManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "ContractHistoryManagerService")
public class ContractHistoryManagerService extends SpringBeanAutowiringSupport {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(ContractHistoryManagerService.class);

    /** The contract history manager biz. */
    @Autowired
    private ContractHistoryManagerBiz contractHistoryManagerBiz;

    /**
     * Cherche les contract history.
     * 
     * @param contractIdBsc
     *            the contract id bsc
     * @param product
     *            le produit
     * @return la liste des résultats
     * @throws GrcWsException
     *             l'exception GrcWsException
     */
    @WebMethod(operationName = "findContractHistory")
    @WebResult(name = "ListContractHistoryWS")
    public List<ContractHistoryWS> findContractHistory(@WebParam(name = "contractIdBsc") final String contractIdBsc,
            @WebParam(name = "produit") final int product) throws GrcWsException {
        LOG.trace("@WebMethod: contractHistoryManagerBiz.findContractHistory contractIdBsc={}", contractIdBsc);
        return contractHistoryManagerBiz.findContractHistory(contractIdBsc, product);
    }

    /**
     * Cherche les peripherique history.
     * 
     * @param contractIdBsc
     *            the contract id bsc
     * @param product
     *            le produit
     * @return la liste des résultats
     * @throws GrcWsException
     *             l'exception GrcWsException
     */
    @WebMethod(operationName = "findPeripheriqueHistory")
    @WebResult(name = "ListPeripheriqueHistoryWS")
    public List<PeripheriqueHistoryWS> findPeripheriqueHistory(
            @WebParam(name = "contractIdBsc") final String contractIdBsc, @WebParam(name = "produit") final int product)
            throws GrcWsException {
        LOG.trace("@WebMethod: contractHistoryManagerBiz.findPeripheriqueHistory contractIdBsc={}", contractIdBsc);
        return contractHistoryManagerBiz.findPeripheriqueHistory(contractIdBsc, product);
    }
//    
//    @WebMethod(operationName = "findDonneesPreidentification")
//    @WebResult(name = "ListPreidentificationWS")
//    public List<PreidentificationWS> findDonneesPreidentification(
//            @WebParam(name = "contractIdBsc") final String contractIdBsc, @WebParam(name = "produit") final int product)
//            throws GrcWsException {
//        LOG.trace("@WebMethod: contractHistoryManagerBiz.findDonneesPreidentification contractIdBsc={}", contractIdBsc);
//        return contractHistoryManagerBiz.findDonneesPreidentification(contractIdBsc, product);
//    }
//    
//    @WebMethod(operationName = "findDonneesPreidentificationOM")
//    @WebResult(name = "ListPreidentificationWS")
//    public List<PreidentificationWS> findDonneesPreidentificationOM(
//            @WebParam(name = "contractIdBsc") final String contractIdBsc, @WebParam(name = "produit") final int product)
//            throws GrcWsException {
//        LOG.trace("@WebMethod: contractHistoryManagerBiz.findDonneesPreidentificationOM contractIdBsc={}", contractIdBsc);
//        return contractHistoryManagerBiz.findDonneesPreidentificationOM(contractIdBsc, product);
//    }
//    
//
//    /**
//     * Cherche les plan tarifaire history.
//     * 
//     * @param contractIdBsc
//     *            the contract id bsc
//     * @param product
//     *            le produit
//     * @return la liste des résultats
//     * @throws GrcWsException
//     *             l'exception GrcWsException
//     */
//    @WebMethod(operationName = "findPlanTarifaireHistory")
//    @WebResult(name = "ListPlanTarifaireHistoryWS")
//    public List<PlanTarifaireHistoryWS> findPlanTarifaireHistory(
//            @WebParam(name = "contractIdBsc") final String contractIdBsc, @WebParam(name = "produit") final int product)
//            throws GrcWsException {
//        LOG.trace("@WebMethod: contractHistoryManagerBiz.findPlanTarifaireHistory : contractIdBsc={}", contractIdBsc);
//        return contractHistoryManagerBiz.findPlanTarifaireHistory(contractIdBsc, product);
//    }

    
    @Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
