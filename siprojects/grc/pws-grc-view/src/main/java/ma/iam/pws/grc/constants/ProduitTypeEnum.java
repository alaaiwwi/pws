/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * The Enum ProduitTypeEnum.
 */
public enum ProduitTypeEnum {

    /** The FIXE. */
    FIXE(1, "FIXE"),

    /** The MOBILE. */
    MOBILE(2, "MOBILE"),

    /** The INTERNET. */
    INTERNET(3, "INTERNET"),

    /** The DATA. */
    DATA(4, "DATA");

    /** The id type produit. */
    private final int idTypeProduit;

    /** The label type produit. */
    private final String labelTypeProduit;

    /**
     * Instantiates a new produit type enum.
     * 
     * @param idTypeProduit
     *            the id type produit
     * @param labelTypeProduit
     *            the label type produit
     */
    ProduitTypeEnum(final int idTypeProduit, final String labelTypeProduit) {
        this.idTypeProduit = idTypeProduit;
        this.labelTypeProduit = labelTypeProduit;
    }

    /**
     * Gets the id type produit.
     * 
     * @return the id type produit
     */
    public int getIdTypeProduit() {
        return idTypeProduit;
    }

    /**
     * Gets the label type produit.
     * 
     * @return the label type produit
     */
    public String getLabelTypeProduit() {
        return labelTypeProduit;
    }

}
