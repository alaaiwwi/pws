/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class PeripheriqueHistoryWS.
 */
public class PeripheriqueHistoryWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 4386015476722702121L;
    
    private String type_ressource ;
    private String nd ;
    /*
     * f.tatbi FC 5864 
     * chgt de nature par numeSerie
     */
//    /** The nature. */
//    private String nature;
    
    private String numSerie;

    /*
     * f.tatbi FC 5864 
     * chgt de numeSerie par IMSI
     */
    /** The num serie. */
//    private String numSerie
    private String IMSI;

    /** The statut. */
    private String statut;

    /** The date debut. */
    private String dateDebut;

    /** The date saisie. */
    private String dateSaisie;

    /** The saisie par. */
    private String saisiePar;

    /** The date modification. */
    private String dateModification;

    /** The ref raison statut. */
    private String refRaisonStatut;
    
    /**
     * FC6704-BEGIN
     * Personal Unblocking key : first
     */
    private String puk;
    
    /**
     * FC6704-BEGIN
     * Personal Unblocking key : second
     */
    private String puk2;

//    /**
//     * Gets the nature.
//     * 
//     * @return the nature
//     */
//    public String getNature() {
//        return nature;
//    }

    /**
     * Gets the ref raison statut.
     * 
     * @return the ref raison statut
     */
    public String getRefRaisonStatut() {
        return refRaisonStatut;
    }

    /**
     * Sets the ref raison statut.
     * 
     * @param refRaisonStatut
     *            the new ref raison statut
     */
    public void setRefRaisonStatut(final String refRaisonStatut) {
        this.refRaisonStatut = refRaisonStatut;
    }

//    /**
//     * Sets the nature.
//     * 
//     * @param nature
//     *            the new nature
//     */
//    public void setNature(final String nature) {
//        this.nature = nature;
//    }

	/**
	 * Gets the num serie.
	 * 
	 * @return the num serie
	 */
	public String getNumSerie() {
		return numSerie;
	}

	/**
	 * Sets the num serie.
	 * 
	 * @param numSerie
	 *            the new num serie
	 */
	public void setNumSerie(final String numSerie) {
		this.numSerie = numSerie;
	}

    /**
     * Gets the statut.
     * 
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * Sets the statut.
     * 
     * @param statut
     *            the new statut
     */
    public void setStatut(final String statut) {
        this.statut = statut;
    }

    /**
     * Gets la date de début.
     * 
     * @return la date de début
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets la date de début.
     * 
     * @param dateDebut
     *            the new date debut
     */
    public void setDateDebut(final String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets the date saisie.
     * 
     * @return the date saisie
     */
    public String getDateSaisie() {
        return dateSaisie;
    }

    /**
     * Sets the date saisie.
     * 
     * @param dateSaisie
     *            the new date saisie
     */
    public void setDateSaisie(final String dateSaisie) {
        this.dateSaisie = dateSaisie;
    }

    /**
     * Gets the saisie par.
     * 
     * @return the saisie par
     */
    public String getSaisiePar() {
        return saisiePar;
    }

    /**
     * Sets the saisie par.
     * 
     * @param saisiePar
     *            the new saisie par
     */
    public void setSaisiePar(final String saisiePar) {
        this.saisiePar = saisiePar;
    }

    /**
     * Gets the date modification.
     * 
     * @return the date modification
     */
    public String getDateModification() {
        return dateModification;
    }

    /**
     * Sets the date modification.
     * 
     * @param dateModification
     *            the new date modification
     */
    public void setDateModification(final String dateModification) {
        this.dateModification = dateModification;
    }

	public String getIMSI() {
		return IMSI;
	}

	public void setIMSI(String iMSI) {
		IMSI = iMSI;
	}

	public String getType_ressource() {
		return type_ressource;
	}

	public void setType_ressource(String type_ressource) {
		this.type_ressource = type_ressource;
	}

	public String getNd() {
		return nd;
	}

	public void setNd(String dn) {
		this.nd = dn;
	}

	public String getPuk() {
		return puk;
	}

	public void setPuk(String puk) {
		this.puk = puk;
	}

	public String getPuk2() {
		return puk2;
	}

	public void setPuk2(String puk2) {
		this.puk2 = puk2;
	}
}
