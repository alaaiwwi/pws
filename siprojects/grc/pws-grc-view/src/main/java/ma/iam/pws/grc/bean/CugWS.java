/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceWS.
 */
public class CugWS extends GrcWS {

    /**
     * 
     */
    private static final long serialVersionUID = 144605047287018408L;

    /** The cug id. */
    private String cugId;

    /** The num ressource. */
    private String numRessource;

    /** The cug status. */
    private String cugStatus;

    /** The guf name. */
    private String gufName;

    /** The cug type. */
    private String cugType;

    /** The cug index. */
    private String cugIndex;

    /** The code client. */
    private String codeClient;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[Cug:");
        if (cugId != null) {
            sb.append(",cugId=").append(cugId);
        }
        if (numRessource != null) {
            sb.append(",numRessource=").append(numRessource);
        }
        if (gufName != null) {
            sb.append(",gufName=").append(gufName);
        }
        if (cugType != null) {
            sb.append(",cugType=").append(cugType);
        }
        if (cugType != null) {
            sb.append(",cugType=").append(cugType);
        }
        if (cugType != null) {
            sb.append(",cugType=").append(cugType);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Gets the cug id.
     * 
     * @return the cug id
     */
    public String getCugId() {
        return cugId;
    }

    /**
     * Sets the cug id.
     * 
     * @param cugId
     *            the new cug id
     */
    public void setCugId(final String cugId) {
        this.cugId = cugId;
    }

    /**
     * Gets the num ressource.
     * 
     * @return the num ressource
     */
    public String getNumRessource() {
        return numRessource;
    }

    /**
     * Sets the num ressource.
     * 
     * @param numRessource
     *            the new num ressource
     */
    public void setNumRessource(final String numRessource) {
        this.numRessource = numRessource;
    }

    /**
     * Gets the cug status.
     * 
     * @return the cug status
     */
    public String getCugStatus() {
        return cugStatus;
    }

    /**
     * Sets the cug status.
     * 
     * @param cugStatus
     *            the new cug status
     */
    public void setCugStatus(final String cugStatus) {
        this.cugStatus = cugStatus;
    }

    /**
     * Gets the guf name.
     * 
     * @return the guf name
     */
    public String getGufName() {
        return gufName;
    }

    /**
     * Sets the guf name.
     * 
     * @param gufName
     *            the new guf name
     */
    public void setGufName(final String gufName) {
        this.gufName = gufName;
    }

    /**
     * Gets the cug type.
     * 
     * @return the cug type
     */
    public String getCugType() {
        return cugType;
    }

    /**
     * Sets the cug type.
     * 
     * @param cugType
     *            the new cug type
     */
    public void setCugType(final String cugType) {
        this.cugType = cugType;
    }

    /**
     * Gets the cug index.
     * 
     * @return the cug index
     */
    public String getCugIndex() {
        return cugIndex;
    }

    /**
     * Sets the cug index.
     * 
     * @param cugIndex
     *            the new cug index
     */
    public void setCugIndex(final String cugIndex) {
        this.cugIndex = cugIndex;
    }

    /**
     * Gets the code client.
     * 
     * @return the code client
     */
    public String getCodeClient() {
        return codeClient;
    }

    /**
     * Sets the code client.
     * 
     * @param codeClient
     *            the new code client
     */
    public void setCodeClient(final String codeClient) {
        this.codeClient = codeClient;
    }

}
