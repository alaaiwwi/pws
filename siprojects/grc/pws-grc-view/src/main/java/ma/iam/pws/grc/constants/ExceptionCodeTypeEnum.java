/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * The Enum ExceptionCodeTypeEnum.
 */
public enum ExceptionCodeTypeEnum {

    /** The N o_ da o_ fo r_ product. */
    NO_DAO_FOR_PRODUCT("ERR_GR_00_001", "Impossible de charger le context du produit"),

    /** The EMPT y_ resul t_ ws. */
    EMPTY_RESULT_WS("ERR_GR_00_002", "Aucun résultat trouvé"),

    /** The PROBLE m_ criteri a_ ws. */
    PROBLEM_CRITERIA_WS("ERR_GR_00_003", "Probléme dans les critéres de recherche"),

    /** The ERRW s1000. */
    SYSTEM_ERROR_WS("ERR_GR_00_004", "Erreur systéme au niveau du webservice"),

    /** The QUER y_ error. */
    GRC_SYNTAX_ERROR("ERR_GR_00_005", "QUERY_ERROR Erreur de format de la requête utilisée")

    /** The ERRW s1001. */
    // ERRWS1000("ERRWS1000", "Erreur lors de la recherche factures")
    // //
    // /** The ERRW s1002. */
    // ERRWS1002("ERRWS1002", "Erreur lors de la recherche détails factures"),
    // //
    // /** The ERRW s1003. */
    // ERRWS1003("ERRWS1003",
    // "Erreur lors de la recherche des paiements de la facture")

    /** The error code. */
    ;

    /** The error code. */
    private final String errorCode;

    /** The error context. */
    private final String errorContext;

    /**
     * Instantiates a new exception code type enum.
     * 
     * @param errorCode
     *            the error code
     * @param errorContext
     *            the error context
     */
    ExceptionCodeTypeEnum(final String errorCode, final String errorContext) {
        this.errorCode = errorCode;
        this.errorContext = errorContext;
    }

    /**
     * Gets the error code.
     * 
     * @return the error code
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Gets the error name.
     * 
     * @return the error name
     */
    public String getErrorContext() {
        return errorContext;
    }

}
