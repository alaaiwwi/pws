/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.IntervalWS;
import ma.iam.pws.grc.bean.InvoiceWSFindInvoice;
import ma.iam.pws.grc.bean.PaymentDetailWS;
import ma.iam.pws.grc.bean.PaymentWS;
import ma.iam.pws.grc.business.InvoiceManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class InvoiceManagerService.
 */
@WebService(serviceName = "InvoiceManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "invoiceManagerService")
public class InvoiceManagerService extends SpringBeanAutowiringSupport {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(InvoiceManagerService.class);

	/** The invoice manager biz. */
	@Autowired
	private InvoiceManagerBiz invoiceManagerBiz;

	// /**
	// * Cherche les invoices details.
	// *
	// * @param idFacture
	// * the id facture
	// * @param product
	// * le produit
	// * @return la liste des résultats
	// * @throws GrcWsException
	// * l'exception GrcWsException
	// */
	// @WebMethod(operationName = "findInvoicesDetails")
	// @WebResult(name = "ListInvoiceDetailWS")
	// public List<InvoiceDetailWS> findInvoicesDetails(@WebParam(name =
	// "idFacture") final String idFacture, @WebParam(name = "produit") final
	// int product)
	// throws GrcWsException {
	// LOG.trace("@WebMethod: invoiceManagerBiz.findInvoicesDetails idFacture={}",
	// idFacture);
	// return invoiceManagerBiz.findInvoicesDetails(idFacture, product);
	// }

	/*
	 * f.tatbi FC 5864 add statut to webParam delete nd, numDocumente te periode
	 * from result
	 */
	/**
	 * Cherche les invoices for customer.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findInvoicesForCustomer")
	@WebResult(name = "ListInvoiceWS")
	public List<InvoiceWSFindInvoice> findInvoicesForCustomer(
			@WebParam(name = "customerId") final String customerId,
			@WebParam(name = "interval") final IntervalWS interval,
			@WebParam(name = "produit") final int product,
			@WebParam(name = "statut") final String statut)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: invoiceManagerBiz.findContractsForCustomer customerId={} interval={}",
				customerId, interval);
		return invoiceManagerBiz.findInvoicesForCustomer(customerId, interval,
				product, statut);
	}

	/*
	 * f.tatbi FC 5864 update name of operation findPaymentsDetails ==>
	 * findPaymentsForTransaction update name of webParam invoiceExternalId ==>
	 * numTransaction
	 */
	/**
	 * Cherche les payments details.
	 * 
	 * @param invoiceExternalId
	 *            the invoice external id
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */

	@WebMethod(operationName = "findPaymentsForTransaction")
	@WebResult(name = "ListPaymentDetailWS")
	public List<PaymentDetailWS> findPaymentsDetails(
			@WebParam(name = "numTransaction") final String invoiceExternalId,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: invoiceManagerBiz.findPaymentsDetails numTransaction={}",
				invoiceExternalId);
		return invoiceManagerBiz
				.findPaymentsDetails(invoiceExternalId, product);
	}

	/*
	 * f.tatbi FC 5864 update name of operation findPayments ==>
	 * findPaymentsForCustomer
	 */
	/**
	 * Cherche les payments.
	 * 
	 * @param customerId
	 *            l'identifiant client BSCS (CUSTOMER_ID)
	 * @param interval
	 *            l'interval contenant la date de début et de fin de la période
	 *            de recherche
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findPaymentsForCustomer")
	@WebResult(name = "ListPaymentWS")
	public List<PaymentWS> findPayments(
			@WebParam(name = "customerId") final String customerId,
			@WebParam(name = "interval") final IntervalWS interval,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: invoiceManagerBiz.findPayments customerId={} interval={}",
				customerId, interval);
		return invoiceManagerBiz.findPayments(customerId, interval, product);
	}

	// @WebMethod(operationName = "findDetailedInvoicesForCustomer")
	// @WebResult(name = "ListInvoiceWS")
	// public List<InvoiceWS> findDetailedInvoicesForCustomer(@WebParam(name =
	// "customerId") final String customerId,
	// @WebParam(name = "interval") final IntervalWS interval, @WebParam(name =
	// "produit") final int product) throws GrcWsException {
	// LOG.trace("@WebMethod: invoiceManagerBiz.findDetailedInvoicesForCustomer customerId={} interval={}",
	// customerId, interval);
	// return invoiceManagerBiz.findDetailedInvoicesForCustomer(customerId,
	// interval, product);
	// }
	/*
	 * f.tatbi FC 5864 add WebParam idFacture
	 */
	/**
	 * Cherche les payments for invoice.
	 * 
	 * @param refFacture
	 *            the ref facture
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findPaymentsForInvoice")
	@WebResult(name = "ListPaymentWS")
	public List<PaymentWS> findPaymentsForInvoice(
			@WebParam(name = "refFacture") final String refFacture,
			@WebParam(name = "idFacture") final Integer idFacture,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: invoiceManagerBiz.findPaymentsForInvoice numFacture={}",
				refFacture);
		return invoiceManagerBiz.findPaymentsForInvoice(refFacture, idFacture,
				product);
	}

	/*
	 * f.tatbi FC 5864 "findMontantGlobalOuvert" renommer custAcctCode ==>
	 * CustCode renommer DoubleMontantGlobalOuvert ==> MontantOuvert
	 */
	/**
	 * Cherche les montant global ouvert.
	 * 
	 * @param custAcctCode
	 *            le code client (CUSTCODE dans BSCS)
	 * @param product
	 *            le produit
	 * @return the double
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findMontantGlobalOuvert")
	@WebResult(name = "MontantOuvert")
	public Double findMontantGlobalOuvert(
			@WebParam(name = "CustCode") final String custAcctCode,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: invoiceManagerBiz.findMontantGlobalOuvert custAcctCode={}",
				custAcctCode);
		return invoiceManagerBiz.findMontantGlobalOuvert(custAcctCode, product);
	}

	/*
	 * f.tatbi FC 5864 "findInvoicesByListInvoiceExternalId" renommer
	 * findInvoicesByListInvoiceExternalId ==> findInvoiceById ajout de
	 * refFacture et idFacture
	 */
	/**
	 * Retourne la liste des factures é partir d'une liste de numéros de
	 * factures
	 * 
	 * @param listInvoiceExternalId
	 *            Liste des Num Facture (exemple 180532450)
	 * @param product
	 *            le produit (fixe, mobile, data, internet)
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findInvoiceById")
	@WebResult(name = "ListInvoiceWS")
	public List<InvoiceWSFindInvoice> findInvoicesByListInvoiceExternalId(
			@WebParam(name = "refFacture") final List<String> refFacture,
			@WebParam(name = "idFacture") final List<Integer> idFacture,
			@WebParam(name = "produit") final int product,
			@WebParam(name = "statut") final String statut)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: invoiceManagerBiz.findInvoiceById listInvoiceExternalId={}",
				refFacture);
		return invoiceManagerBiz.findInvoiceById(refFacture, idFacture,
				product, statut);

	}

	// /**
	// * Retourne la liste des factures é partir d'une liste de numéros de
	// * factures
	// *
	// * @param listInvoiceExternalId
	// * Liste des Num Facture (exemple 180532450)
	// * @param product
	// * le produit (fixe, mobile, data, internet)
	// * @return la liste des résultats
	// * @throws GrcWsException
	// * l'exception GrcWsException
	// */
	// @WebMethod(operationName = "findInvoiceById")
	// @WebResult(name = "ListInvoiceWS")
	// public List<InvoiceWS> findInvoicesByListInvoiceExternalId(
	// @WebParam(name = "listInvoiceExternalId") final List<Integer>
	// listInvoiceExternalId,
	// @WebParam(name = "produit") final int product)
	// throws GrcWsException {
	// LOG.trace(
	// "@WebMethod: invoiceManagerBiz.findInvoicesByListInvoiceExternalId listInvoiceExternalId={}",
	// listInvoiceExternalId);
	// return invoiceManagerBiz.findInvoicesByListInvoiceExternalId(
	// listInvoiceExternalId, product);
	// }

	// /**
	// * Retourne la liste des factures é partir d'une liste des références.
	// *
	// * @param listReference
	// * Liste des Références Facture (exemple "0000852994052009")
	// * @param product
	// * le produit (fixe, mobile, data, internet)
	// * @return La liste des factures
	// * @throws GrcWsException
	// * the grc ws exception
	// */
	// @WebMethod(operationName = "findInvoicesByListReference")
	// @WebResult(name = "ListInvoiceWS")
	// public List<InvoiceWS> findInvoicesByListReference(@WebParam(name =
	// "listReference") final List<String> listReference,
	// @WebParam(name = "produit") final int product) throws GrcWsException {
	// LOG.trace("@WebMethod: invoiceManagerBiz.findInvoicesByListReference listInvoiceExternalId={}",
	// listReference);
	// return invoiceManagerBiz.findInvoicesByListReference(listReference,
	// product);
	// }

	/**
	 * Protect invoices.
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @param product
	 *            the product
	 * @return the boolean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	/*
	 * f.tatbi FC 5864 renommer listInvoiceExternalId par idFacture FC 5864
	 * ajout du USER et Apllication
	 */
	@WebMethod(operationName = "protectInvoices")
	@WebResult(name = "BooleanResult")
	public Boolean protectInvoices(
			@WebParam(name = "idFacture") final List<Integer> listInvoiceExternalId,
			@WebParam(name = "user") final String user,
			@WebParam(name = "application") final String application,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		if (LOG.isTraceEnabled())
			LOG.trace("@WebMethod: invoiceManagerBiz.protectInvoices listInvoiceExternalId="
					+ listInvoiceExternalId);
		// return invoiceManagerBiz.protectInvoices(listInvoiceExternalId,
		// product);
		return invoiceManagerBiz.protectInvoices(listInvoiceExternalId, user,
				application, product);
	}

	/**
	 * Un protect invoices.
	 * 
	 * @param listInvoiceExternalId
	 *            the list invoice external id
	 * @param product
	 *            the product
	 * @return the boolean
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	@WebMethod(operationName = "unProtectInvoices")
	@WebResult(name = "BooleanResult")
	public Boolean unProtectInvoices(
			@WebParam(name = "idFacture") final List<Integer> listInvoiceExternalId,
			@WebParam(name = "user") final String user,
			@WebParam(name = "application") final String application,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		if (LOG.isTraceEnabled())
			LOG.trace("@WebMethod: invoiceManagerBiz.unProtectInvoices listInvoiceExternalId="
					+ listInvoiceExternalId);
		// return invoiceManagerBiz.unProtectInvoices(listInvoiceExternalId,
		// product);

		return invoiceManagerBiz.unProtectInvoices(listInvoiceExternalId, user,
				application, product);
	}

	@Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
