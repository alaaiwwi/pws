/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * Les statuts de traitement des rejets de mediation.<br>
 * <b>Doit avoir un équivalent cété WS</b>
 */
public enum RejetMediationStatutEnum {

    CLOTURE_GRC("CLOTURE_GRC", "Cléturé sur GRC"),

    CLOTURE_MEDIATION("CLOTURE_MEDIATION", "Cléturé sur la Médiation"),

    CLOTURE_OM_ORDER("CLOTURE_OM_ORDER", "Cléturé sur BSCS"),

    ERREUR_FR("ERREUR", "Erreur lors du FR manuel"),

    ERREUR_FOI("ERREUR", "Erreur lors du Finish Order Identification"),

    ERREUR_WS("ERREUR_WS", "Erreur lors de l'appel du Web Service"),

    OK("OK", "Traitement OK"), ;

    /** The ticket category cd. */
    private final String statusCode;

    /** The ticket category cd. */
    private final String statusDesc;

    /**
     * Instantiates a new ticket category enum.
     * 
     * @param statusCode
     *            the status code
     * @param statusDesc
     *            the status desc
     */
    RejetMediationStatutEnum(final String statusCode, final String statusDesc) {
        this.statusCode = statusCode;
        this.statusDesc = statusDesc;

    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @return the statusDesc
     */
    public String getStatusDesc() {
        return statusDesc;
    }

}
