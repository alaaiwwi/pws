/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceWS.
 */
public class ServiceCreditWS extends GrcWS {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 144605047287018408L;

    /** The id. */
    private String id;

    /** The reference credit. */
    private String referenceCredit;

    /** The unitees gratuites. */
    private String uniteesGratuites;

    /** The numero sequence. */
    private String numeroSequence;

    /** The date debit. */
    private String dateDebit;

    /** The date fin. */
    private String dateFin;

    /** The date validite. */
    private String dateValidite;

    /** The ug attribue. */
    private String ugAttribue;

    /** The report accorde. */
    private String reportAccorde;

    /** The ug utilise. */
    private String ugUtilise;

    /** The report utilise. */
    private String reportUtilise;

    /** The dernier appel. */
    private String dernierAppel;

    /** The montant forfait. */
    private String montantForfait;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Gets the reference credit.
     * 
     * @return the reference credit
     */
    public String getReferenceCredit() {
        return referenceCredit;
    }

    /**
     * Sets the reference credit.
     * 
     * @param referenceCredit
     *            the new reference credit
     */
    public void setReferenceCredit(final String referenceCredit) {
        this.referenceCredit = referenceCredit;
    }

    /**
     * Gets the unitees gratuites.
     * 
     * @return the unitees gratuites
     */
    public String getUniteesGratuites() {
        return uniteesGratuites;
    }

    /**
     * Sets the unitees gratuites.
     * 
     * @param uniteesGratuites
     *            the new unitees gratuites
     */
    public void setUniteesGratuites(final String uniteesGratuites) {
        this.uniteesGratuites = uniteesGratuites;
    }

    /**
     * Gets the numero sequence.
     * 
     * @return the numero sequence
     */
    public String getNumeroSequence() {
        return numeroSequence;
    }

    /**
     * Sets the numero sequence.
     * 
     * @param numeroSequence
     *            the new numero sequence
     */
    public void setNumeroSequence(final String numeroSequence) {
        this.numeroSequence = numeroSequence;
    }

    /**
     * Gets the date debit.
     * 
     * @return the date debit
     */
    public String getDateDebit() {
        return dateDebit;
    }

    /**
     * Sets the date debit.
     * 
     * @param dateDebit
     *            the new date debit
     */
    public void setDateDebit(final String dateDebit) {
        this.dateDebit = dateDebit;
    }

    /**
     * Gets the date fin.
     * 
     * @return the date fin
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     * Sets the date fin.
     * 
     * @param dateFin
     *            the new date fin
     */
    public void setDateFin(final String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Gets the date validite.
     * 
     * @return the date validite
     */
    public String getDateValidite() {
        return dateValidite;
    }

    /**
     * Sets the date validite.
     * 
     * @param dateValidite
     *            the new date validite
     */
    public void setDateValidite(final String dateValidite) {
        this.dateValidite = dateValidite;
    }

    /**
     * Gets the ug attribue.
     * 
     * @return the ug attribue
     */
    public String getUgAttribue() {
        return ugAttribue;
    }

    /**
     * Sets the ug attribue.
     * 
     * @param ugAttribue
     *            the new ug attribue
     */
    public void setUgAttribue(final String ugAttribue) {
        this.ugAttribue = ugAttribue;
    }

    /**
     * Gets the report accorde.
     * 
     * @return the report accorde
     */
    public String getReportAccorde() {
        return reportAccorde;
    }

    /**
     * Sets the report accorde.
     * 
     * @param reportAccorde
     *            the new report accorde
     */
    public void setReportAccorde(final String reportAccorde) {
        this.reportAccorde = reportAccorde;
    }

    /**
     * Gets the ug utilise.
     * 
     * @return the ug utilise
     */
    public String getUgUtilise() {
        return ugUtilise;
    }

    /**
     * Sets the ug utilise.
     * 
     * @param ugUtilise
     *            the new ug utilise
     */
    public void setUgUtilise(final String ugUtilise) {
        this.ugUtilise = ugUtilise;
    }

    /**
     * Gets the report utilise.
     * 
     * @return the report utilise
     */
    public String getReportUtilise() {
        return reportUtilise;
    }

    /**
     * Sets the report utilise.
     * 
     * @param reportUtilise
     *            the new report utilise
     */
    public void setReportUtilise(final String reportUtilise) {
        this.reportUtilise = reportUtilise;
    }

    /**
     * Gets the dernier appel.
     * 
     * @return the dernier appel
     */
    public String getDernierAppel() {
        return dernierAppel;
    }

    /**
     * Sets the dernier appel.
     * 
     * @param dernierAppel
     *            the new dernier appel
     */
    public void setDernierAppel(final String dernierAppel) {
        this.dernierAppel = dernierAppel;
    }

    /**
     * Gets the montant forfait.
     * 
     * @return the montant forfait
     */
    public String getMontantForfait() {
        return montantForfait;
    }

    /**
     * Sets the montant forfait.
     * 
     * @param montantForfait
     *            the new montant forfait
     */
    public void setMontantForfait(final String montantForfait) {
        this.montantForfait = montantForfait;
    }

}
