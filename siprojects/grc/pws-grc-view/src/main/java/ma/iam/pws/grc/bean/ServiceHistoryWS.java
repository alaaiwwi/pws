/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class ServiceHistoryWS.
 */
public class ServiceHistoryWS extends GrcWS {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1421504339479695158L;

	private String sncode;
	private String shdes;

	// /** The id. */
	// private String id;

	/** The status. */
	private String status;

	/** The change date. */
	private String changeDate;

	//
	// /** The reason. */
	// private String reason;
	//
	// /** The user. */
	// private String user;

	/**
	 * Gets le statut.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets le statut.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Gets the change date.
	 * 
	 * @return the change date
	 */
	public String getChangeDate() {
		return changeDate;
	}

	/**
	 * Sets the change date.
	 * 
	 * @param changeDate
	 *            the new change date
	 */
	public void setChangeDate(final String changeDate) {
		this.changeDate = changeDate;
	}

	public String getSncode() {
		return sncode;
	}

	public void setSncode(String sncode) {
		this.sncode = sncode;
	}

	public String getShdes() {
		return shdes;
	}

	public void setShdes(String shdes) {
		this.shdes = shdes;
	}

}
