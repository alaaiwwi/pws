/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * The Class BaseDAOFixe.
 */
@Repository
public class BaseDAOFixe {

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplateNeto;

	/** The jdbc template. */
	private JdbcTemplate jdbcTemplate;

	/**
	 * Sets the fixe bscs data source.
	 * 
	 * @param fixeBSCSDataSource
	 *            the new fixe bscs data source
	 */
	@Autowired
	@Qualifier("fixeBSCSDataSource")
	public void setFixeBSCSDataSource(final DataSource fixeBSCSDataSource) {
		setNamedParameterJdbcTemplate(new NamedParameterJdbcTemplate(
				fixeBSCSDataSource));
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplate(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	/**
	 * Sets the fixe bscs data source.
	 * 
	 * @param fixeBSCSDataSource
	 *            the new fixe bscs data source
	 */
	@Autowired
	public void setFixeNETODataSource(final DataSource fixeNETODataSource) {
		setNamedParameterJdbcTemplateNeto(new NamedParameterJdbcTemplate(
				fixeNETODataSource));
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplateNeto(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplateNeto) {
		this.namedParameterJdbcTemplateNeto = namedParameterJdbcTemplateNeto;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplateNeto() {
		return namedParameterJdbcTemplateNeto;
	}

	/**
	 * Gets the jdbc template.
	 * 
	 * @return the jdbc template
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * Sets the jdbc template.
	 * 
	 * @param jdbcTemplate
	 *            the new jdbc template
	 */
	public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
