/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.service;

/**
 * The Class ParamAdress.
 */
public class CompAdress {

    /**
     * Instantiates a new param adress.
     */
    public CompAdress() {

    }

    /** The commune id. */
    private String communeId;

    /** The quartier id. */
    private String quartierId;

    /** The voie id. */
    private String voieId;

    /** The comp address. */
    private String compAddress;

    /** The reliq quartier. */
    private String reliqQuartier;

    /** The reliq voie. */
    private String reliqVoie;

    /** The n voie. */
    private String nVoie;

    /** The n etage. */
    private String nEtage;

    public String getCommuneId() {
        return communeId;
    }

    public void setCommuneId(final String communeId) {
        this.communeId = communeId;
    }

    public String getQuartierId() {
        return quartierId;
    }

    public void setQuartierId(final String quartierId) {
        this.quartierId = quartierId;
    }

    public String getVoieId() {
        return voieId;
    }

    public void setVoieId(final String voieId) {
        this.voieId = voieId;
    }

    public String getCompAddress() {
        return compAddress;
    }

    public void setCompAddress(final String compAddress) {
        this.compAddress = compAddress;
    }

    public String getReliqQuartier() {
        return reliqQuartier;
    }

    public void setReliqQuartier(final String reliqQuartier) {
        this.reliqQuartier = reliqQuartier;
    }

    public String getReliqVoie() {
        return reliqVoie;
    }

    public void setReliqVoie(final String reliqVoie) {
        this.reliqVoie = reliqVoie;
    }

    public String getnVoie() {
        return nVoie;
    }

    public void setnVoie(final String nVoie) {
        this.nVoie = nVoie;
    }

    public String getnEtage() {
        return nEtage;
    }

    public void setnEtage(final String nEtage) {
        this.nEtage = nEtage;
    }

}
