
package ma.iam.mobile.customer.bean;

public class Rateplan {

    private Integer tmcode;
    private String shdes;
    private String des;

    public String getShdes() {
        return shdes;
    }

    public void setShdes(String shdes) {
        this.shdes = shdes;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

	public Integer getTmcode() {
		return tmcode;
	}

	public void setTmcode(Integer tmcode) {
		this.tmcode = tmcode;
	}

}
