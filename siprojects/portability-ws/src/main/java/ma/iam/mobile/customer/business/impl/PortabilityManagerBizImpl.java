package ma.iam.mobile.customer.business.impl;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.iam.mobile.customer.bean.MopOptions;
import ma.iam.mobile.customer.bean.Operateur;
import ma.iam.mobile.customer.bean.PortabiliteRequest;
import ma.iam.mobile.customer.bean.Rateplan;
import ma.iam.mobile.customer.business.PortabilityManagerBiz;
import ma.iam.mobile.customer.dao.PortabilityDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

@Component(value = "portabilityManagerBiz")
public class PortabilityManagerBizImpl implements PortabilityManagerBiz {
    @Autowired
    private Properties config;
    @Autowired
    PortabilityDao portabilityDao;

    @Override
    public String createPortIN(PortabiliteRequest request) throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityDao.createPortIN(request);
    }

    @Override
    public Operateur getOperateurAttributaire(String msisdn) throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityDao.getOperateurAttributaire(msisdn);
    }

    @Override
    public List<Operateur> getOperateur() throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityDao.getOperateur();
    }

    @Override
    public boolean verifiedemandePortageIN(String msisdn) throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityDao.verifiedemandePortageIN(msisdn);
    }

	@Override
	public List<Rateplan> getRateplanByCategory(String prgcode) {
		// TODO Auto-generated method stub
		return portabilityDao.getRateplanByCategory(prgcode);
	}

	@Override
	public List<MopOptions> getMopOptions(String tmcode) {
		// TODO Auto-generated method stub
		return portabilityDao.getMopOptions(tmcode);
	}
}
