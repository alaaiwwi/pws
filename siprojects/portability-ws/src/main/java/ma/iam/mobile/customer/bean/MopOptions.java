
package ma.iam.mobile.customer.bean;

public class MopOptions {

    private Integer sncode;
    private String fu_pack_id;
    private String pack_value;
    private String pack_value_new;
    
	public Integer getSncode() {
		return sncode;
	}
	public void setSncode(Integer sncode) {
		this.sncode = sncode;
	}
	public String getFu_pack_id() {
		return fu_pack_id;
	}
	public void setFu_pack_id(String fu_pack_id) {
		this.fu_pack_id = fu_pack_id;
	}
	public String getPack_value() {
		return pack_value;
	}
	public void setPack_value(String pack_value) {
		this.pack_value = pack_value;
	}
	public String getPack_value_new() {
		return pack_value_new;
	}
	public void setPack_value_new(String pack_value_new) {
		this.pack_value_new = pack_value_new;
	}

}
