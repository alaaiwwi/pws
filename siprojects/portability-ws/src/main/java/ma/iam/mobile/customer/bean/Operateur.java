
package ma.iam.mobile.customer.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;


public class Operateur {

    private Integer plcode;
    private String shdes;
    private String des;
    private Date valideFrom;

    public Integer getPlcode() {
        return plcode;
    }

    public void setPlcode(Integer plcode) {
        this.plcode = plcode;
    }

    public String getShdes() {
        return shdes;
    }

    public void setShdes(String shdes) {
        this.shdes = shdes;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Date getValideFrom() {
        return valideFrom;
    }

    public void setValideFrom(Date valideFrom) {
        this.valideFrom = valideFrom;
    }
}
