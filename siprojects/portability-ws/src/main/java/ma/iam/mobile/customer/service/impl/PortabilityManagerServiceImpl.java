package ma.iam.mobile.customer.service.impl;

import ma.iam.mobile.customer.bean.MopOptions;
import ma.iam.mobile.customer.bean.Operateur;
import ma.iam.mobile.customer.bean.PortabiliteRequest;
import ma.iam.mobile.customer.bean.Rateplan;
import ma.iam.mobile.customer.business.ByPassManagerBiz;
import ma.iam.mobile.customer.business.PortabilityManagerBiz;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.service.PortabilityManagerService;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import net.atos.ma.integration.jee.bypass.JDBCPing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.util.List;

@Component(value = "portabilityManagerService")
public class PortabilityManagerServiceImpl extends SpringBeanAutowiringSupport implements PortabilityManagerService {

    @Autowired
    private ByPassManagerBiz byPassManagerBiz;
    @Autowired
    private JDBCPing pingMobileBscs;

    @Autowired
    private PortabilityManagerBiz portabilityManagerBiz;

    @Override
    public String createPortIN(PortabiliteRequest request) throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityManagerBiz.createPortIN(request);
    }

    @Override
    public Operateur getOperateurAttributaire(String msisdn) throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityManagerBiz.getOperateurAttributaire(msisdn);
    }

    @Override
    public List<Operateur> getOperateur() throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityManagerBiz.getOperateur();
    }

    @Override
    public boolean verifiedemandePortageIN(String msisdn) throws FunctionnalException, SyntaxiqueException, TechnicalException {
        return portabilityManagerBiz.verifiedemandePortageIN(msisdn);
    }

    @Override
    public Integer byPass() {
        return byPassManagerBiz.modeByPass();
    }

	@Override
	public List<Rateplan> getRateplanByCategory(String prgcode)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return portabilityManagerBiz.getRateplanByCategory(prgcode);
	}

	@Override
	public List<MopOptions> getMopOptions(String tmcode)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return portabilityManagerBiz.getMopOptions(tmcode);
	}
}
