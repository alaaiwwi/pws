package ma.iam.mobile.customer.dao;

import java.util.List;

import ma.iam.mobile.customer.bean.MopOptions;
import ma.iam.mobile.customer.bean.Operateur;
import ma.iam.mobile.customer.bean.PortabiliteRequest;
import ma.iam.mobile.customer.bean.Rateplan;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface PortabilityDao extends BaseDAOMobile {

	public String createPortIN(PortabiliteRequest request) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public Operateur getOperateurAttributaire(String msisdn) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public java.util.List<Operateur> getOperateur() throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public boolean verifiedemandePortageIN(String msisdn) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public List<Rateplan> getRateplanByCategory(String prgcode);

	public List<MopOptions> getMopOptions(String tmcode);
}
