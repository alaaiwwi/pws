package ma.iam.ws.tree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CheckDependencyDAO {
    List<Map<String, Object>> loadDependencies();

    List<Map<String, Object>> getPriority();
}
