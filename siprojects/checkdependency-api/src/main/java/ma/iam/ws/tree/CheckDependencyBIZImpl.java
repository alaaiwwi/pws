package ma.iam.ws.tree;

import groovy.lang.GroovyClassLoader;
import ma.iam.mutualisation.fwk.common.exception.ExceptionCodeTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.*;
@Repository
public class CheckDependencyBIZImpl implements CheckDependencyBIZ {
    @Autowired
    private CheckDependencyDAO checkDependencyDAO = null;//new CheckDependencyDAOImpl();

    private static Class theParsedClass = null;

    public GDependencyInterface getgDependencyInterface() throws CheckDependecyException {
        try {
        if (theParsedClass == null) {
            String str = generateScript();
            GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
            theParsedClass = groovyClassLoader.parseClass(str);

        }

            return (GDependencyInterface) theParsedClass.newInstance();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new CheckDependecyException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS, e.getMessage());
        }

    }

    @Override
    public CheckDependencyBean getCheckDependency(Long sncode, Set<Long> contrActivServ) throws CheckDependecyException {
        GDependencyInterface gDepe = getgDependencyInterface();
        CheckDependencyBean checkDependencyBean = gDepe.check(sncode, contrActivServ);
        return checkDependencyBean;
    }

    @Override
    public CheckDependencyBean getCheckListDependency( Set<Long> sncodes) throws CheckDependecyException {
        GDependencyInterface gDepe = getgDependencyInterface();
        CheckDependencyBean checkDependencyBean = gDepe.check(sncodes);
        return checkDependencyBean;
    }

    private String buildExprssion(String item, String operat) {
        String exp = "";
        if (item != null && !"".equals(item.trim())) {
            if ("0123456789".contains(item.substring(0, 1))) {
                exp = "\tb=(updateServices(services,notServices,s," + item + " as long) && b)";
            } else {
                exp = "\tb= (callMethod (services,notServices,\"_" + item + "_\",s) " + operat + " b)";
            }
        }
        return exp;
    }

    private String buildExprssionNot(String item, String operat) {
        String exp = "";
        if (item != null && !"".equals(item.trim())) {
            if ("0123456789".contains(item.substring(0, 1))) {
                exp = "\tb=(updateServicesNot( services,notServices,s," + item + " as long) && b)";
            } else {
                exp = "\tb= (!callMethod(services,notServices,\"_" + item + "_\",s) " + operat + " b)";
            }
        }
        return exp;
    }

    public String generateScript() throws IOException {
        String script = "";
        String scripttemp = "";
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream("GDependenceEval.dtemplate");
        BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
        String fline = null;
        while ((fline = reader.readLine()) != null) {
            script += fline + "\n";
        }
        List<Map<String, Object>> dependencies = checkDependencyDAO.loadDependencies();
        int index = -1;
        for (Map<String, Object> dep : dependencies) {
            Object o = dep.get("SNCODE");
            String depm = o!=null? o.toString():null;
            if (depm == null || "".equals(depm.trim())) {
                depm = (String) dep.get("EBS");
            }
            index++;


            // boolean _259_0 (long[] s)  {def boolean b=true; b=(callMethod ("_E1_",s) && b ) ;   b=(updateServices(s,175) && b )  ;  return b;     }
            String line = "\nboolean _" + depm + "_" + index + " (Set services,Set notServices,Set s)  {\n\tdef boolean b=true \n#EXP#   \treturn b\n  }";
            String notop = (String) dep.get("NOT_OP");
            String needs = (String) dep.get("NEEDS");
            String andop = (String) dep.get("AND_OP");
            String orop = (String) dep.get("OR_OP");
            if (needs != null && !"".equals(needs.trim()))
                line = line.replaceAll("#EXP#", buildExprssion(needs, "&&") + " \n#EXP# ");
            else
                line = line.replaceAll("#EXP#", buildExprssionNot(notop, "&&") + " \n#EXP# ");
            String andexp = buildExprssion(andop, "&&");
            if (!"".equals(andexp.trim()))
                line = line.replaceAll("#EXP#",   andexp + " #EXP# \n");
            String orexp = buildExprssion(orop, "|| ");
            if (!"".equals(orexp.trim()))
                line = line.replaceAll("#EXP#",  orexp + " #EXP# \n");
            line = line.replaceAll("#EXP#", "");
            scripttemp += line;

        }
        script = script.replaceAll("//#DEPMETHODES#", scripttemp);
        return script;
    }


    public static void main(String[] args) {
        try {
            CheckDependencyBIZImpl checkDependencyBIZ = new CheckDependencyBIZImpl();
            String s = checkDependencyBIZ.generateScript();
            System.out.println(s);
            Set<Long> vals = new HashSet<Long>();
            vals.add(new Long(1));
            vals.add(new Long(125));
            vals.add(new Long(14));
            vals.add(new Long(140));
            vals.add(new Long(262));
            vals.add(new Long(30));
            vals.add(new Long(76));
            vals.add(new Long(78));
            vals.add(new Long(79));
            vals.add(new Long(204));
            vals.add(new Long(210));
            vals.add(new Long(175));
            vals.add(new Long(258));
            vals.add(new Long(259));
            vals.add(new Long(365));
            vals.add(new Long(278));
            vals.add(new Long(360));
            vals.add(new Long(358));
            vals.add(new Long(371));
            vals.add(new Long(372));
            vals.add(new Long(373));
            vals.add(new Long(374));
            vals.add(new Long(381));
            vals.add(new Long(174));
            vals.add(new Long(384));
            vals.add(new Long(389));
            vals.add(new Long(2));
            vals.add(new Long(13));
            vals.add(new Long(21));
            vals.add(new Long(168));
            vals.add(new Long(23));
            vals.add(new Long(27));
            vals.add(new Long(28));
            vals.add(new Long(20));
            vals.add(new Long(22));
            vals.add(new Long(26));
            vals.add(new Long(117));
            vals.add(new Long(124));
            vals.add(new Long(18));
            vals.add(new Long(19));
            vals.add(new Long(263));
            vals.add(new Long(270));
            vals.add(new Long(29));
            vals.add(new Long(35));
            vals.add(new Long(377));
            vals.add(new Long(38));
            vals.add(new Long(399));
            vals.add(new Long(413));
            vals.add(new Long(445));
            vals.add(new Long(453));
            vals.add(new Long(486));
            vals.add(new Long(75));
            vals.add(new Long(77));
            vals.add(new Long(15));
            vals.add(new Long(16));
            vals.add(new Long(169));
            vals.add(new Long(17));
            vals.add(new Long(24));
            vals.add(new Long(25));
            vals.add(new Long(126));
            vals.add(new Long(127));
            vals.add(new Long(128));
            vals.add(new Long(129));
            vals.add(new Long(130));
            vals.add(new Long(131));
            vals.add(new Long(132));
            vals.add(new Long(133));
            vals.add(new Long(205));
            vals.add(new Long(206));
            vals.add(new Long(207));
            vals.add(new Long(208));
            vals.add(new Long(209));
            vals.add(new Long(32));
            vals.add(new Long(33));
            vals.add(new Long(34));
            vals.add(new Long(36));
            vals.add(new Long(444));
            vals.add(new Long(460));
            vals.add(new Long(461));
            vals.add(new Long(81));
            vals.add(new Long(83));
            vals.add(new Long(84));
            vals.add(new Long(85));

            CheckDependencyBean ret = checkDependencyBIZ.getCheckListDependency(vals);
            System.out.println(ret.isStatus());

            for (Long l : ret.getDependingServices()
            ) {
                System.out.println(l);
            }
            System.out.println("++++++++++++++++++++++++++++++++++++++++++");
            for (Long l : ret.getNotDependingServices()
            ) {
                System.out.println(l);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

