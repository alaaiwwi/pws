package ma.iam.ws.tree;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckDependencyDAOImpl implements CheckDependencyDAO {
    @Override
    public  List<Map<String, Object>> loadDependencies() {
        List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
        try {
            String data = null;
            BufferedReader lecteurAvecBuffer = new BufferedReader(new FileReader("D:\\01-WORK\\03-TECH\\00-Workspaces\\pws\\siprojects\\checkdependency-api\\src\\main\\resources\\datas.txt"));

            while ((data = lecteurAvecBuffer.readLine()) != null) {
                String[] parts = data.split(";");
                int size = parts.length;
                HashMap hashMap = new HashMap();
                hashMap.put("TEMP_ID",  parts[0].replaceAll("\\.",""));
                hashMap.put("ID", parts[1].replaceAll("\\.",""));
                hashMap.put("SNCODE",parts[2].replaceAll("\\.",""));
                hashMap.put("EBS", parts[3].replaceAll("\\.",""));
                hashMap.put("NEEDS", parts[4].replaceAll("\\.",""));
                hashMap.put("AND_OP",parts[5].replaceAll("\\.",""));
                hashMap.put("OR_OP", parts[6].replaceAll("\\.",""));
                hashMap.put("NOT_OP", parts[7].replaceAll("\\.",""));
                hashMap.put("CONSISTENCY_TYPE",parts[8].replaceAll("\\.",""));
                hashMap.put("DESCRIPTION", parts[9].replaceAll("\\.",""));
                hashMap.put("SOURCE",parts[10]);
                res.add(hashMap);
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
        }
        return res;
    }

    @Override
    public  List<Map<String, Object>> getPriority() {
        List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
        try {
            String data = null;
            BufferedReader lecteurAvecBuffer = new BufferedReader(new FileReader("D:\\01-WORK\\03-TECH\\00-Workspaces\\pws\\siprojects\\checkdependency-api\\src\\main\\resources\\datas-op.txt"));

            while ((data = lecteurAvecBuffer.readLine()) != null) {
                String[] parts = data.split(",");
                HashMap hashMap = new HashMap();
                hashMap.put("ID", parts[0]);
                hashMap.put("TMCODE", parts[1]);
                hashMap.put("SNCODE", new Long(parts[1]));
                hashMap.put("PRIORITY", parts[3]);
                res.add(hashMap);
            }
        } catch (Throwable exc) {
            System.out.println("Erreur d'ouverture");
        }
        return res;
    }

    public static void main (String[] args){
        CheckDependencyDAOImpl checkDependencyDAO =  new CheckDependencyDAOImpl();
        List<Map<String, Object>> loadDependencies = checkDependencyDAO.loadDependencies();
        for (Map<String, Object> h:
             loadDependencies) {
            System.out.println(h);

        }
        System.out.println("------------------------------------------------------------------");
       loadDependencies = checkDependencyDAO.getPriority();
        for (Map<String, Object> h:
                loadDependencies) {
            System.out.println(h);

        }
    }

}
