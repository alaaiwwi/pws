package ma.iam.ws.tree;

import java.util.List;
import java.util.Set;

public interface GDependencyInterface {
    CheckDependencyBean check (long meth, Set<Long> s);
    CheckDependencyBean check ( Set<Long> s);
}
