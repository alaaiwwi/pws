package ma.iam.ws.tree;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CheckDependencyBIZ {
    public CheckDependencyBean getCheckDependency( Long sncode,Set<Long> contrActivServ ) throws CheckDependecyException ;
    CheckDependencyBean getCheckListDependency(Set<Long> sncodes) throws CheckDependecyException;
}
