package ma.iam.ws.tree;

import ma.iam.mutualisation.fwk.common.exception.ExceptionCodeTypeEnum;
import ma.iam.mutualisation.fwk.common.exception.v1.FunctionnalException;

public class CheckDependecyException extends FunctionnalException {
    public CheckDependecyException(ExceptionCodeTypeEnum exceptionCodeTypeEnum, Throwable cause) {
        super(exceptionCodeTypeEnum, cause);
    }

    public CheckDependecyException(ExceptionCodeTypeEnum exceptionCodeTypeEnum, Object message, Throwable cause) {
        super(exceptionCodeTypeEnum, message, cause);
    }

    public CheckDependecyException(ExceptionCodeTypeEnum exceptionCodeTypeEnum) {
        super(exceptionCodeTypeEnum);
    }

    public CheckDependecyException(ExceptionCodeTypeEnum exceptionCodeTypeEnum, Object message) {
        super(exceptionCodeTypeEnum, message);
    }

    public CheckDependecyException(String errorCode, Object errorContext, Object message) {
        super(errorCode, errorContext, message);
    }
}
