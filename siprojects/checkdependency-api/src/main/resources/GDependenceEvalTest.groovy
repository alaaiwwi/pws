import ma.iam.ws.tree.CheckDependencyBean
import ma.iam.ws.tree.GDependencyInterface

public class GDependenceEvalTest  implements GDependencyInterface{


    boolean updateServices (Set services,Set notServices,Set s, sncode){
        if (s.contains(sncode)){
            return true
        }else{
            services.add(sncode as long)
            return false
        }
    }
    boolean updateServicesNot (Set services,Set notServices, Set s, sncode){
        if (s.contains(sncode as long)){
            notServices.add(sncode as long)
            return false
        }else{

            return true
        }
    }

    CheckDependencyBean check (long meth,Set s){
        def CheckDependencyBean checkDependencyBean = new CheckDependencyBean ()
        checkDependencyBean.dependingServices =[] as Set
        checkDependencyBean.notDependingServices = [] as Set
        def b=  callMethod(checkDependencyBean.dependingServices, checkDependencyBean.notDependingServices,"_${meth}_",s)
        checkDependencyBean.status = b
        return checkDependencyBean
    }
    CheckDependencyBean check (Set s){
        def CheckDependencyBean checkDependencyBean = new CheckDependencyBean ()
        checkDependencyBean.dependingServices =[] as Set
        checkDependencyBean.notDependingServices = [] as Set
        checkDependencyBean.status = true
        s.each { meth ->
            def b = callMethod(checkDependencyBean.dependingServices, checkDependencyBean.notDependingServices, "_${meth}_", s)
            checkDependencyBean.status = b &&  checkDependencyBean.status
        }
        return checkDependencyBean
    }

    boolean callMethod (Set services,Set notServices,String meth,Set s){
        def boolean ret=true
        this.metaClass.methods.each { method->
            def String mname = method.name
            if (mname.contains(meth)) {
                ret= ret && this."$mname"(services,notServices,s)
            }
        }
        return ret
    }

    boolean _414_0 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,440 as long) && b)
        return b
    }
    boolean _432_1 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,440 as long) && b)
        return b
    }
    boolean _364_2 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,440 as long) && b)
        return b
    }
    boolean _363_3 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,441 as long) && b)
        return b
    }
    boolean _385_4 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,441 as long) && b)
        return b
    }
    boolean _415_5 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,441 as long) && b)
        return b
    }
    boolean _440_6 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,429 as long) && b)
        return b
    }
    boolean _440_7 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,429 as long) && b)
        return b
    }
    boolean _312_8 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _312_9 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,239 as long) && b)
        return b
    }
    boolean _312_10 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,384 as long) && b)
        return b
    }
    boolean _312_11 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,379 as long) && b)
        return b
    }
    boolean _312_12 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _312_13 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,387 as long) && b)
        return b
    }
    boolean _267_14 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _267_15 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,239 as long) && b)
        return b
    }
    boolean _267_16 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,384 as long) && b)
        return b
    }
    boolean _267_17 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,379 as long) && b)
        return b
    }
    boolean _267_18 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _267_19 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,387 as long) && b)
        return b
    }
    boolean _268_20 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _268_21 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,239 as long) && b)
        return b
    }
    boolean _268_22 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,384 as long) && b)
        return b
    }
    boolean _268_23 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,379 as long) && b)
        return b
    }
    boolean _268_24 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _268_25 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,387 as long) && b)
        return b
    }
    boolean _415_26 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,254 as long) && b)
        return b
    }
    boolean _233_27 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,234 as long) && b)
        return b
    }
    boolean _240_28 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,234 as long) && b)
        return b
    }
    boolean _259_29 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E1_",s) && b)
        b=(updateServices(services,notServices,s,175 as long) && b)
        return b
    }
    boolean _0_30 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _377_31 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E1_",s) && b)
        b= (callMethod (services,notServices,"_E2_",s) ||  b)
        return b
    }
    boolean _0_32 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E3_",s) && b)
        b= (callMethod (services,notServices,"_E4_",s) ||  b)
        return b
    }
    boolean _0_33 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        b=(updateServices(services,notServices,s,240 as long) && b)
        return b
    }
    boolean _0_34 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        b=(updateServices(services,notServices,s,23 as long) && b)
        return b
    }
    boolean _0_35 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        b=(updateServices(services,notServices,s,233 as long) && b)
        return b
    }
    boolean _323_36 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        return b
    }
    boolean _326_37 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        return b
    }
    boolean _327_38 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        return b
    }
    boolean _431_39 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,387 as long) && b)
        b= (callMethod (services,notServices,"_E1_",s) ||  b)
        return b
    }
    boolean _0_40 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_M1_",s) && b)
        b=(updateServices(services,notServices,s,388 as long) && b)
        return b
    }
    boolean _0_41 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,383 as long) && b)
        b=(updateServices(services,notServices,s,502 as long) && b)
        return b
    }
    boolean _174_42 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,259 as long) && b)
        return b
    }
    boolean _314_43 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E1_",s) && b)
        b= (callMethod (services,notServices,"_E2_",s) ||  b)
        return b
    }
    boolean _0_44 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E4_",s) && b)
        b= (callMethod (services,notServices,"_E5_",s) && b)
        return b
    }
    boolean _0_45 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,240 as long) && b)
        return b
    }
    boolean _0_46 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,234 as long) && b)
        b= (callMethod (services,notServices,"_E3_",s) && b)
        return b
    }
    boolean _0_47 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,27 as long) && b)
        b=(updateServices(services,notServices,s,168 as long) && b)
        return b
    }
    boolean _0_48 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,23 as long) && b)
        return b
    }
    boolean _23_49 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,314 as long) && b)
        return b
    }
    boolean _365_50 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,175 as long) && b)
        return b
    }
    boolean _380_51 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,370 as long) && b)
        return b
    }
    boolean _278_52 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,175 as long) && b)
        return b
    }
    boolean _360_53 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,175 as long) && b)
        return b
    }
    boolean _381_54 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,175 as long) && b)
        return b
    }
    boolean _371_55 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        return b
    }
    boolean _372_56 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        return b
    }
    boolean _373_57 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,210 as long) && b)
        return b
    }
    boolean _302_58 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,175 as long) && b)
        return b
    }
    boolean _427_59 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E1_",s) && b)
        b= (callMethod (services,notServices,"_E2_",s) ||  b)
        return b
    }
    boolean _0_60 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,388 as long) && b)
        b=(updateServices(services,notServices,s,387 as long) && b)
        return b
    }
    boolean _0_61 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,352 as long) && b)
        b=(updateServices(services,notServices,s,321 as long) && b)
        return b
    }
    boolean _0_62 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E3_",s) && b)
        b= (callMethod (services,notServices,"_E4_",s) ||  b)
        return b
    }
    boolean _0_63 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,383 as long) && b)
        b=(updateServices(services,notServices,s,502 as long) && b)
        return b
    }
    boolean _0_64 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,382 as long) && b)
        b= (callMethod (services,notServices,"_M1_",s) ||  b)
        return b
    }
    boolean _314_65 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,233 as long) && b)
        return b
    }
    boolean _321_66 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _384_67 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,174 as long) && b)
        return b
    }
    boolean _383_68 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _387_69 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _388_70 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _239_71 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,383 as long) && b)
        return b
    }
    boolean _239_72 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _267_73 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,431 as long) && b)
        return b
    }
    boolean _268_74 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,431 as long) && b)
        return b
    }
    boolean _312_75 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,431 as long) && b)
        return b
    }
    boolean _383_76 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,387 as long) && b)
        return b
    }
    boolean _383_77 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _384_78 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,387 as long) && b)
        return b
    }
    boolean _448_79 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,219 as long) && b)
        return b
    }
    boolean _457_80 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,387 as long) && b)
        b=(updateServices(services,notServices,s,388 as long) && b)
        return b
    }
    boolean _458_81 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,387 as long) && b)
        b=(updateServices(services,notServices,s,388 as long) && b)
        return b
    }
    boolean _239_82 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,174 as long) && b)
        return b
    }
    boolean _469_83 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,379 as long) && b)
        return b
    }
    boolean _471_84 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,320 as long) && b)
        return b
    }
    boolean _479_85 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,449 as long) && b)
        return b
    }
    boolean _469_86 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,379 as long) && b)
        return b
    }
    boolean _471_87 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,320 as long) && b)
        return b
    }
    boolean _502_88 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,174 as long) && b)
        return b
    }
    boolean _239_89 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,502 as long) && b)
        return b
    }
    boolean _502_90 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,387 as long) && b)
        return b
    }
    boolean _502_91 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _506_92 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E1_",s) && b)
        b= (callMethod (services,notServices,"_E2_",s) && b)
        return b
    }
    boolean _0_93 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,352 as long) && b)
        b=(updateServices(services,notServices,s,502 as long) && b)
        return b
    }
    boolean _0_94 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,387 as long) && b)
        b=(updateServices(services,notServices,s,388 as long) && b)
        return b
    }
    boolean _0_95 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E221_",s) && b)
        b= (callMethod (services,notServices,"_E222_",s) ||  b)
        return b
    }
    boolean _0_96 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,267 as long) && b)
        b=(updateServices(services,notServices,s,268 as long) && b)
        return b
    }
    boolean _0_97 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,312 as long) && b)
        b=(updateServices(services,notServices,s,383 as long) && b)
        return b
    }
    boolean _0_98 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,304 as long) && b)
        b=(updateServices(services,notServices,s,365 as long) && b)
        return b
    }
    boolean _0_99 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E21_",s) && b)
        b= (callMethod (services,notServices,"_E22_",s) ||  b)
        return b
    }
    boolean _0_100 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E11_",s) && b)
        b= (callMethod (services,notServices,"_E12_",s) ||  b)
        return b
    }
    boolean _239_101 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,336 as long) && b)
        return b
    }
    boolean _336_102 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,388 as long) && b)
        return b
    }
    boolean _336_103 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,502 as long) && b)
        return b
    }
    boolean _336_104 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,383 as long) && b)
        return b
    }
    boolean _502_105 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServicesNot( services,notServices,s,383 as long) && b)
        return b
    }
    boolean _525_106 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,468 as long) && b)
        b=(updateServices(services,notServices,s,469 as long) && b)
        return b
    }
    boolean _468_107 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E1_",s) && b)
        b= (callMethod (services,notServices,"_E2_",s) ||  b)
        return b
    }
    boolean _0_108 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,502 as long) && b)
        b=(updateServices(services,notServices,s,312 as long) && b)
        return b
    }
    boolean _0_109 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E3_",s) && b)
        b= (callMethod (services,notServices,"_E4_",s) ||  b)
        return b
    }
    boolean _0_110 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,383 as long) && b)
        b=(updateServices(services,notServices,s,267 as long) && b)
        return b
    }
    boolean _0_111 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b= (callMethod (services,notServices,"_E5_",s) && b)
        b= (callMethod (services,notServices,"_E6_",s) ||  b)
        return b
    }
    boolean _0_112 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,268 as long) && b)
        b=(updateServices(services,notServices,s,352 as long) && b)
        return b
    }
    boolean _0_113 (Set services,Set notServices,Set s)  {
        def boolean b=true
        b=(updateServices(services,notServices,s,387 as long) && b)
        b=(updateServices(services,notServices,s,388 as long) && b)
        return b
    }

    static main(args){
        def GDependenceEvalTest dependenceEval = new GDependenceEvalTest();
        //440 -> 145,429

        CheckDependencyBean x =  dependenceEval.check([1,141,513,140,234,368,313,380,78,14, 18, 262, 22, 455, 240, 30] as Set )
        println x

    }

}
