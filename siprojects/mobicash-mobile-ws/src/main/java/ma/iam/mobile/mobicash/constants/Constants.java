package ma.iam.mobile.mobicash.constants;

public interface Constants {

	public final static int MOBICASH_DOMAIN_MOBILE = 1;
	
	public final static int MOBICASH_DOMAIN_FIXE = 2;
	
	public final static int MOBICASH_DOMAIN_INTERNET = 3;
	
	public final static int MOBICASH_DOMAIN_MTBOX = 4;
	

	public final static int MOBICASH_MB_PARTNER_ID = 5;
	
	public final static int MOBICASH_FX_PARTNER_ID = 9;
	
	
	public final static String MOBICASH_PREFIX_MOBILE = "M";
	
	public final static String MOBICASH_PREFIX_FIXE = "F";
	

	public final static String MOBICASH_CODE_SUCCESS = "000";

	public static final String ERRO_DOMAIN_CODE = "Domaine invalide";	
}
