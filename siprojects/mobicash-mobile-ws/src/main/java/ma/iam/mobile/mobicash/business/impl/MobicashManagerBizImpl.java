package ma.iam.mobile.mobicash.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.bean.MobicashOpenInvoice;
import ma.iam.mobile.mobicash.business.MobicashManagerBiz;
import ma.iam.mobile.mobicash.dao.MobicashDao;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;
import ma.iam.mobile.mobicash.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

@Component(value = "mobicashManagerBiz")
public class MobicashManagerBizImpl implements MobicashManagerBiz {
    @Autowired
    private Properties config;
    @Autowired
    MobicashDao mobicashDao;

	@Override
	public CustomerPosition getOpenInvoiceListMobile(String domain, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.getOpenInvoiceListMobile(domain, uniqueId, pin);
	}

	@Override
	public String payInvoiceListMobile(String domain, int customerId, String uniqueId, String pin, String authNum,
			ArrayList<MobicashOpenInvoice> invoiceList)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.payInvoiceListMobile(domain, customerId, uniqueId, pin, authNum, invoiceList);
	}

	@Override
	public String reconcileMobile(String domain, Date day, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.reconcileMobile(domain, day, operationCount, totalPaidAmount);
	}
}
