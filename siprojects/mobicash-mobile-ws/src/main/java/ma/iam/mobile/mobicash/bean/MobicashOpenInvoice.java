/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.iam.mobile.mobicash.bean;

/**
 *
 * @author R00TiamdG
 */
public class MobicashOpenInvoice {
	private String ohrefnum;
	private String month;
	private float openAmount;
	private int ohxact;
	public String getOhrefnum() {
		return ohrefnum;
	}
	public void setOhrefnum(String ohrefnum) {
		this.ohrefnum = ohrefnum;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public float getOpenAmount() {
		return openAmount;
	}
	public void setOpenAmount(float openAmount) {
		this.openAmount = openAmount;
	}
	public int getOhxact() {
		return ohxact;
	}
	public void setOhxact(int ohxact) {
		this.ohxact = ohxact;
	}
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("\n - " + "ohrefnum :" + ohrefnum);
		str.append("\n - " + "month :" + month);
		str.append("\n - " + "openAmount :" + openAmount);
		str.append("\n - " + "ohxact :" + ohxact);		
		return str.toString();
	}

}
