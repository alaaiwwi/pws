/**
 * 
 */
package ma.iam.pws.soi.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import ma.iam.pws.soi.beans.contract.MassServicesEx;
import ma.iam.pws.soi.beans.contract.bServicesList;
import ma.iam.pws.soi.beans.dependency.CheckDependencyBean;
import ma.iam.pws.soi.beans.errors.FaultWS;

/**
 * @author A.ESSA
 *
 */

@javax.jws.WebService(targetNamespace = "http://services.webservice.ws.iam.ma/")
public interface CheckDependency {

	/**
	 * Login.
	 * 
	 * @param login
	 *            the login
	 * @param motDePasse
	 *            the mot de passe
	 * @param messageContext
	 *            the message context
	 * @return the string
	 */
	@javax.jws.WebMethod
	CheckDependencyBean getCheckDependency(
			@javax.jws.WebParam(name = "USERNAME") String username,
			@javax.jws.WebParam(name = "CO_ID") long co_id,
			@javax.jws.WebParam(name = "SNCODE") long sncode,
			@javax.jws.WebParam(name = "OP") String op) throws FaultWS;

	@javax.jws.WebMethod
	MassServicesEx MassActivationDeactivationServicesImpl(
			@javax.jws.WebParam(name = "USERNAME") String username,
			@javax.jws.WebParam(name = "CO_ID") long co_id,
			@javax.jws.WebParam(name = "FirstList") bServicesList List1,
			@javax.jws.WebParam(name = "SecondList") bServicesList List2)
			throws FaultWS;

	/**
	 * byPAss check if the communication its ok/ or not send a fake compte and
	 * chack the return if the compte is not connected
	 * 
	 * @return
	 */
	@WebMethod(operationName = "byPass")
	@WebResult(name = "result")
	Integer modeByPass();
}