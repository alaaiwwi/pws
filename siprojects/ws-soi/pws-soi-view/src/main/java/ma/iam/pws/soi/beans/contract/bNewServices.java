package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
public class bNewServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private long SNCODE;
	private long SPCODE;
	private List<bNewParamValue> BNewParamValue= new ArrayList<bNewParamValue>();

	
	public long getSPCODE() {
		return SPCODE;
	}
	public void setSPCODE(long sPCODE) {
		SPCODE = sPCODE;
	}
	public List<bNewParamValue> getBNewParamValue() {
		return BNewParamValue;
	}
	public void setBNewParamValue(List<bNewParamValue> bNewParamValue) {
		BNewParamValue = bNewParamValue;
	}
	public long getSNCODE() {
		return SNCODE;
	}
	public void setSNCODE(long sNCODE) {
		SNCODE = sNCODE;
	}
	@Override
	public String toString() {
		return " service SNCODE= [ " + SNCODE + "]";
	}

	
}
