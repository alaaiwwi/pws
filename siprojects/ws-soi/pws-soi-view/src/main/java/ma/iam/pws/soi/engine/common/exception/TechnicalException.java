package ma.iam.pws.soi.engine.common.exception;


/**
 * The Class TechnicalException.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class TechnicalException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -764018510574013490L;
	/** The Constant ALIAS. */
	public static final String ALIAS = "TechnicalException";
	/**
	 * Instantiates a new technical exception.
	 * @param message the message
	 */
	public TechnicalException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new technical exception.
	 * @param message the message
	 * @param throwable the throwable
	 */
	public TechnicalException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
