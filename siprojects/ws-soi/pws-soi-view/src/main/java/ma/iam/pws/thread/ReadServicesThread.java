package ma.iam.pws.thread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;

import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;

public class ReadServicesThread extends Thread {

	private String cilName;
	private String cilVersion;
	private String securityName;
	private String securityVersion;
	private Properties msgProperties;
	private String login;
	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;
	private UserCache userCache = new NullUserCache();

	private String prgCode;
	private String spliter;

	public String getSpliter() {
		return spliter;
	}

	public void setSpliter(String spliter) {
		this.spliter = spliter;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	public String getPrgCode() {
		return prgCode;
	}

	public void setPrgCode(String prgCode) {
		this.prgCode = prgCode;
	}

	@Override
	public void run() {

		while (true) {

			try {
				SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(login,
						soiContext);

				if ((soiComm == null)
						|| (soiComm != null && !soiComm.isConnectionOk())) {
					String password = null;
					try {
						String encryptedPass = PasswordUtils.getPassword(login);
						if (encryptedPass != null)
							password = PasswordUtils.decryptPassword(login,
									encryptedPass);
						else {
							throw new FaultWS(
									Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
									msgProperties);
						}
					} catch (Exception e) {
						throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
								msgProperties);
					}

					if (SoiUtils.Authentify(wsContext, login, password,
							msgProperties, securityName, securityVersion,
							cilName, cilVersion, soiContext, userCache) == null)
						throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
								msgProperties);
				}

				soiComm = SoiUtils.getCILSOIServiceByUser2(login, soiContext);
				if (soiComm == null)
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);

				// get all prgcode

				String[] prgCodesList = prgCode.split(spliter);
				Map<String, Map<Long, Map>> READ_SERVICES = new HashMap<String, Map<Long, Map>>();
				prgCodeLoop: for (int pr = 0; pr < prgCodesList.length; pr++) {

					String strPrgCode = prgCodesList[pr];
					HashMap<String, Object> inParams = new HashMap<String, Object>();
					inParams.put("PRG_CODE", strPrgCode);
					HashMap<String, Object> outParamsRatePlans;
					try {
						outParamsRatePlans = soiComm.executeCommand(
								SoiUtils.CMD_ALLOWED_RATEPLANS_READ, inParams);
					}

					catch (Exception e) {
						continue prgCodeLoop;
					}
					// get all rateplans

					HashMap<String, Object>[] iterRateplans = (HashMap<String, Object>[]) outParamsRatePlans
							.get("NUM_RP");

					Map<Long, Map> ratePlansServices = new HashMap<Long, Map>();

					for (int i = 0; i < iterRateplans.length; i++) {
						Long rpCode = Long.parseLong(iterRateplans[i].get(
								"RPCODE").toString());

						List<Long> allowedSncode = new ArrayList<Long>();
						HashMap<String, Object> inParamsAllServices = new HashMap<String, Object>();
						inParamsAllServices.put("PRG_CODE", strPrgCode);
						inParamsAllServices.put("RPCODE", rpCode);
						HashMap<String, Object> outAllowedServices = soiComm
								.executeCommand(
										SoiUtils.CMD_ALLOWED_SERVICES_READ,
										inParamsAllServices);

						HashMap<String, Object>[] outAllowedServicesList = (HashMap<String, Object>[]) outAllowedServices
								.get("LIST_SP_CODE");

						for (int j = 0; j < outAllowedServicesList.length; j++) {
							HashMap<String, Object>[] hashMap = (HashMap<String, Object>[]) outAllowedServicesList[j]
									.get("LIST_SN_CODE");
							for (int g = 0; g < hashMap.length; g++) {
								HashMap<String, Object> hashMap2 = hashMap[g];
								allowedSncode.add(new Long(hashMap2.get(
										"SNCODE").toString()));
							}
						}

						HashMap<String, Object> inParamsPkg = new HashMap<String, Object>();
						inParamsPkg.put("RPCODE", rpCode);
						HashMap<String, Object> outParamspackage = soiComm
								.executeCommand(SoiUtils.CMD_SERVICES_PKG_READ,
										inParamsPkg);
						HashMap<String, Object>[] iter_pckg = (HashMap<String, Object>[]) outParamspackage
								.get("NUM_SP");
						HashMap<String, Object> allowedServicesHash = new HashMap<String, Object>();

						for (int j = 0; j < iter_pckg.length; j++) {
							HashMap<String, Object> hashMap = iter_pckg[j];
							inParamsPkg.put("SPCODE", hashMap.get("SPCODE"));
							HashMap<String, Object> outUncontractedService = soiComm
									.executeCommand(SoiUtils.CMD_SERVICES_READ,
											inParamsPkg);

							allowedServicesHash.put(hashMap.get("SPCODE")
									.toString(), outUncontractedService
									.get("NUM_SV"));
							inParamsPkg.remove("SPCODE");
						}

						Map<String, Object> contrServices = new HashMap<String, Object>();

						contrServices.put(SoiUtils.ALLOWED_SNCODES_CACHING,
								allowedSncode);
						contrServices.put(SoiUtils.ALLOWED_SERVICES_CACHING,
								allowedServicesHash);

						ratePlansServices.put(rpCode, contrServices);

						// PRG ==> RATEPLAN ==>
						// CONTRACTEDService,UNCONTRACTEDService
					}
					READ_SERVICES.put(prgCodesList[pr], ratePlansServices);
				}

				// to remove juste for test

				SoiUtils.READ_SERVICES.keySet().removeAll(
						READ_SERVICES.keySet());
				//
				SoiUtils.READ_SERVICES.putAll(READ_SERVICES);

				Thread.sleep(5000);
			} catch (InterruptedException e) {
				System.out.println("ERROR in the THREAD READALLSERVICES");
				
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
				
				}
				
			} catch (FaultWS e) {
System.out.println("ERROR in the THREAD READALLSERVICES");
				
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
				
				}
			} catch (CMSException e) {
System.out.println("ERROR in the THREAD READALLSERVICES");
				
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
				
				}
			}
			
			
			System.out.println("attent  .......... ");
			
			try {
				Thread.sleep(60*60*24*1000);
			} catch (InterruptedException e1) {
			
			}
		}

	}

	public void init() {

		System.out.println("int");
		this.start();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	public Properties getMsgProperties() {
		return msgProperties;
	}

	public void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	public WebServiceContext getWsContext() {
		return wsContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	public SoiContext getSoiContext() {
		return soiContext;
	}

	public void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

}