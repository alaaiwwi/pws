package ma.iam.pws.soi.engine.common;

public class Constants {

	/** The Constant CONFIRM. */
	public static final int CONFIRM_CODE = 1;

	/** The Constant INFO. */
	public static final int INFO_CODE = 2;

	/** The Constant ERROR. */
	public static final int ERROR_CODE = 4;
	
	/** The Constant ERROR. */
	public static final int HABLITATION_CODE = 3;

	/** The Constant RESULT. */
	public static final int RESULT_CODE = 0;
	
	/** The NO_ALERTE_CODE. */
	public static final int NO_ALERTE_CODE = 5;
	
	/** The MESSAG e_ fil e_ name. */
	public static String MESSAGE_FILE_NAME = "ressource.message";
	
	public static final String MESSAGE_ERREUR_TECHNIQUE = "message.erreur.technique";
	
	/** The Constant AUTHORIZATION_FAILURE_FLG. */
	public static final String AUTHORIZATION_FAILURE_FLG = "authorizationFailureFlg";

	/** The Constant AUTHENTICATION_NOT_FOUND_FLG. */
	public static final String AUTHENTICATION_NOT_FOUND_FLG = "authenticationNotFound";

	/** The Constant AUTHENTICATION_FAILED_MSG. */
	public static final String AUTHENTICATION_FAILED_MSG = "authenticationFailed";

	/** The Constant FORMAT_DATE. */
	public static final String FORMAT_DATE = "dd/MM/yyyy";
	
	/** The Constant CODE_REVENDEUR. */
	public static final String CODE_REVENDEUR = "15";
}
