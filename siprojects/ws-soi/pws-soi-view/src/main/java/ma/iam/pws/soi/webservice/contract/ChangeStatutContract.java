package ma.iam.pws.soi.webservice.contract;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import ma.iam.pws.soi.beans.contract.ContractStatBean;
import ma.iam.pws.soi.beans.errors.FaultWS;

@javax.jws.WebService(targetNamespace="http://contract.webservice.ws.iam.ma/")
public interface ChangeStatutContract {  

	
	/**
	 * ChangeStatutContract.
	 * 
	 * @param CO_ID internal identifiant for a contract
	 * @param CO_STATUS the new Status to submit
	 * @param Reason for the new status
	 * @return the string
	 */
	@javax.jws.WebMethod
	ContractStatBean ChangeStatutContract(@javax.jws.WebParam(name="USERNAME")String username,  @javax.jws.WebParam(name="CO_ID") long co_id,  @javax.jws.WebParam(name="CO_STATUS") Integer co_status ,  @javax.jws.WebParam(name="REASON")	long reason  ) throws FaultWS;

	
	/**
	 * byPAss check if the communication its ok/ or not send a fake compte and
	 * chack the return if the compte is not connected
	 * 
	 * @return
	 */
	@WebMethod(operationName = "byPass")
	@WebResult(name = "result")
	Integer modeByPass();
}
