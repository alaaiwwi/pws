package ma.iam.pws.soi.exception;

/**
 * The Class BaseException : Gestion des exceptions
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class BaseException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7981563176409421377L;

	/**
	 * Instantiates a new base exception.
	 * 
	 * @param message the message
	 */
	public BaseException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new base exception.
	 * 
	 * @param message the message
	 * @param throwable the throwable
	 */
	public BaseException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
