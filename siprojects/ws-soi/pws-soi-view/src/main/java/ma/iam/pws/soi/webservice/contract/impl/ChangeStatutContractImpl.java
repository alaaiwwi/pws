package ma.iam.pws.soi.webservice.contract.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.xml.ws.WebServiceContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapter;
import ma.iam.pws.soi.adapter.BContractedServicesAdapter;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.contract.ContractStatBean;
import ma.iam.pws.soi.beans.contract.MassServicesEx;
import ma.iam.pws.soi.beans.contract.bNewParamValue;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValues;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.CmsINTUtils;
import ma.iam.pws.soi.service.util.Dependency;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.tree.Node;
import ma.iam.pws.soi.tree.TreeUtils;
import ma.iam.pws.soi.webservice.contract.ChangeStatutContract;
import ma.iam.security.SecuredObject;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

/*@javax.jws.WebService(endpointInterface = "ma.iam.ws.webservice.contract.ChangeStatutContract",
 serviceName = "ChangeStatutContract" )*/
@SecuredObject
@javax.jws.WebService(targetNamespace = "http://contract.webservice.ws.iam.ma/")
public class ChangeStatutContractImpl implements ChangeStatutContract {

	private static final Logger logger = LogManager
			.getLogger(ChangeStatutContractImpl.class.getName());
	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private Properties msgProperties;

	ServiceUtils service = new ServiceUtils();
	
	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	private String securityName;
	private String securityVersion;

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	private UserCache userCache = new NullUserCache();

	private String cilName;
	private String cilVersion;

	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	// @Override
	@RolesAllowed("ROLE_WRITE")
	public ContractStatBean ChangeStatutContract(String username, long co_id,
			Integer co_status, long reason) throws FaultWS {

		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);

		ContractStatBean co_statbean = new ContractStatBean();
		Contract contract = null;
		bNewServicesEx resMZ = null;

		List<bNewServices> ServicesToAdd = new ArrayList();
		List<bNewServicesEx> mzServices = null;

		try {
			HashMap<String, Object> inParams = new HashMap<String, Object>();
			HashMap<String, Object> contractReadOutParams = new HashMap<String, Object>();
			HashMap<String, Object> contractReadOutParams2 = new HashMap<String, Object>();
			HashMap<String, Object> outParamsCtrSrv = new HashMap<String, Object>();

			inParams.put("CO_ID", new Long(co_id).longValue());
			inParams.put("SYNC_WITH_DB", true);
			Bcontract bcontract = new Bcontract();

			contractReadOutParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParams);
			bcontract = BContractAdapter
					.getMainContractInfo(contractReadOutParams);
			inParams.remove("SYNC_WITH_DB");
			contractReadOutParams2 = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
			BContractedServicesAdapter.buildContractedServicesBean(
					contractReadOutParams2, bcontract);

			HashMap[] servDeps = ServiceUtils.getServDeps(soiComm);
			Node tree = TreeUtils.loadDependencyTree(soiComm, servDeps,
					msgProperties);
			HashMap[] deps = ServiceUtils.getPriority(soiComm);
			List contrActivServ = ServiceUtils
					.getContrActivOnholdSuspendedServ(bcontract);
			List contrEligibleServ = ServiceUtils.getContrEligibleServ(
					bcontract, soiComm);
			List<Dependency> servicesToAdd = new ArrayList();
			List<Dependency> servicesToDel = new ArrayList();

					if (SoiUtils.SERVICE_STATUT_ON_HOLD.equals(contractReadOutParams
					.get("CO_STATUS").toString())
					&& SoiUtils.SERVICE_STATUT_ACTIVE.equals(co_status
							.toString())) {
				contract = new Contract();
				contract.setCoId(new Long(co_id).longValue());
				contract.setRpCode((Long) contractReadOutParams.get("RPCODE"));
				ServiceUtils.change_fill_Auto_params(soiComm, contract,
						msgProperties, contractReadOutParams);
				resMZ = ServiceUtils.change_fill_Auto_MZ_params(soiComm,
						contract, msgProperties, contractReadOutParams);

				if (resMZ != null) {
					mzServices = new ArrayList();
					// mzServices.add(resMZ);
					Long tmcode = bcontract.getRPCODE();
					List<BContractedServices> services = bcontract
							.getContractedServices();
					HashMap servAddDel = TreeUtils.getServicesToAddDel(tree,
							Long.valueOf(resMZ.getSNCODE()).toString(),
							contrActivServ, "DEACTIVATE", deps, tmcode,
							contrEligibleServ, servDeps, msgProperties);
					if (servAddDel.get("EVALUATION") == null) {
						servAddDel.put("EVALUATION", true);
					}

					if (!(Boolean) servAddDel.get("EVALUATION"))
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_INCONSIS_ERROR,
								msgProperties);
					else {
						servicesToDel = (List) servAddDel
								.get("NODESTODECTIVATE");
						bNewServices newSrv = null;
						if (servicesToDel != null && servicesToDel.size() > 0) {
							Dependency cre = new Dependency();
							cre.setSncode(resMZ.getSNCODE());
							servicesToDel.add(cre);
							for (int ii = 0; ii < servicesToDel.size(); ii++) {
								Dependency temp = servicesToDel.get(ii);
								for (int k = 0; k < services.size(); k++) {
									BContractedServices tmpCoSrv = services
											.get(k);
									if (tmpCoSrv.getSNCODE() == temp
											.getSncode().longValue()) {
										HashMap serviceParametersReadOutParams;
										inParams = new HashMap();
										String snCodeParamName = "SNCODE";
										inParams.put(snCodeParamName,
												temp.getSncode());
										inParams.put("CO_ID", co_id);
										inParams.put("PROFILE_ID", new Long(0));
										try {
											serviceParametersReadOutParams = soiComm
													.executeCommand(
															"CONTRACT_SERVICE_PARAMETERS.READ",
															inParams);
										} catch (Exception e) {
											e.printStackTrace();
											return null;
										}

										newSrv = new bNewServices();
										newSrv.setSNCODE(tmpCoSrv.getSNCODE());
										newSrv.setSPCODE(tmpCoSrv.getSPCODE());

										service.setSnCode(tmpCoSrv.getSNCODE());
										service.setSpCode(tmpCoSrv.getSPCODE());

										HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
												.get("NUM_PARAMS");

										if ((hmNumServ != null)
												&& (hmNumServ.length != 0)
												&& (hmNumServ[0] != null)) {
											List<bNewParamValue> prms = new ArrayList<bNewParamValue>();
											for (int i = 0; i < hmNumServ.length; i++) {
												bNewParamValue tempPrm = new bNewParamValue();
												tempPrm.setPRM_ID((Long) hmNumServ[i]
														.get("PRM_ID"));
												List<bNewValues> tmpVals = new ArrayList<bNewValues>();
												HashMap[] multValueInd = (HashMap[]) hmNumServ[i]
														.get("MULT_VALUES");
												if (multValueInd.length > 0) {
													for (int j = 0; j < multValueInd.length; j++) {
														bNewValues tmpVal = new bNewValues();
														tmpVal.setVALUE_DES(multValueInd[j]
																.get("VALUE_DES")
																.toString());
														tmpVal.setVALUE(multValueInd[j]
																.get("VALUE")
																.toString());
														tmpVals.add(tmpVal);
													}
													tempPrm.setbNewValues(tmpVals);
												}
												prms.add(tempPrm);
											}
											newSrv.setBNewParamValue(prms);
										}
										bNewServicesEx servicesEx = new bNewServicesEx();

										servicesEx = ServiceUtils
												.getServiceParamValueExtends(
														servicesEx, newSrv);
										servicesEx.setCO_ID(co_id);
										servicesEx.setRPCODE(bcontract
												.getRPCODE());

										mzServices.add(servicesEx);
										ServicesToAdd.add(newSrv);
									}

								}

							}
						}

						for (Iterator it = mzServices.iterator(); it.hasNext();) {
							inParams = new HashMap<String, Object>();
							bNewServicesEx Srv = (bNewServicesEx) it.next();
							Long snCode = Srv.getSNCODE();
							inParams.put("CO_ID", new Long(co_id).longValue());
							inParams.put("SNCODE", snCode);

							HashMap<String, Object> inParamss = new HashMap<String, Object>();
							inParamss.put("CO_ID", new Long(co_id).longValue());
							inParamss.put("SNCODE", snCode);
							HashMap<String, Object> outParamsTemp = soiComm
									.executeCommand(
											SoiUtils.CMD_CONTRACT_SERVICES_READ,
											inParamss);

							outParamsTemp = soiComm.executeCommand(
									SoiUtils.CMD_CONTRACT_SERVICES_READ,
									inParams);
							HashMap<String, Object>[] inDeactivateService = (HashMap<String, Object>[]) outParamsTemp
									.get("services");

							contract.setCoId(new Long(co_id).longValue());
							service.setSnCode(snCode);
							ServiceUtils.deactivate(service, contract,
									inDeactivateService[0], soiComm,
									msgProperties);
						}

					}

				}

				soiComm.cmsClient.commit();
				
				
				//START FC7466
				Long libSncode = Long.parseLong(ServiceUtils.getSncodeFromShdes("SFSPR",soiComm));
				Long incomSncode = Long.parseLong(ServiceUtils.getSncodeFromShdes("INCOM",soiComm));
				
				if(contrActivServ.contains(libSncode.toString())){
					HashMap serviceParametersReadOutParams;
					inParams = new HashMap();
					String snCodeParamName = "SNCODE";
					inParams.put(snCodeParamName, libSncode);
					inParams.put("CO_ID",co_id);
					inParams.put("PROFILE_ID", new Long(0));
					try{
					    serviceParametersReadOutParams = soiComm.executeCommand("CONTRACT_SERVICE_PARAMETERS.READ", inParams);  
					}
					catch (Exception e){
					    e.printStackTrace();
					    return null;
					}
						
					HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams.get("NUM_PARAMS");

					if ((hmNumServ != null) && (hmNumServ.length != 0)&& (hmNumServ[0] != null)) {
						for (int i = 0; i < hmNumServ.length; i++) {
						    bNewParamValue tempPrm = new bNewParamValue();
						    tempPrm.setPRM_ID((Long)hmNumServ[i].get("PRM_ID"));
						    HashMap [] multValueInd = (HashMap []) hmNumServ[i].get("MULT_VALUES");
						    if(multValueInd.length>0){
							for (int j = 0; j < multValueInd.length; j++) {
							    bNewValues tmpVal= new bNewValues();
							    if(multValueInd[j].get("VALUE").toString().equals("V0028805SMS0000000")){

								    ArrayList additional_services=new ArrayList();

	        							HashMap serviceaactive = new HashMap();						
	        							serviceaactive.put("SNCODE", incomSncode);
	        							serviceaactive.put("SPCODE", ServiceUtils.getSpCodeForSnCode(incomSncode, bcontract.getRPCODE(), soiComm)); 
	        							
	        							additional_services.add(serviceaactive);
	        							
								    ArrayList returnarrayactivate = new ArrayList();			    
								    String xmlactivateservices = CmsINTUtils.ActivateServicesXML( bcontract.getCO_ID(), additional_services, null, null);
									if (xmlactivateservices != null) {
										HashMap activateservice = new HashMap();
										activateservice.put("ACTION_ID", 4);
										activateservice.put("INPUT_DATA", xmlactivateservices);
										activateservice.put("ORIGIN", "LIBIRTE_5G_WS");					
										returnarrayactivate.add(activateservice);
									} 

									if(returnarrayactivate.size()>0){	
										CmsINTUtils.addDeleteServices(bcontract,returnarrayactivate , username, soiComm);
									}
							    }
							}
						    }
						}					
					}
				}

				//END 7466
			}

			inParams = new HashMap<String, Object>();

			inParams.put("CO_ID", new Long(co_id).longValue());
			inParams.put("SYNC_WITH_DB", true);
			contractReadOutParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParams);

			inParams.remove("SYNC_WITH_DB");
			Long rpCode = (Long) contractReadOutParams.get("RPCODE");
			String rpShdes = " ";

			inParams = new HashMap();
			inParams.put("RPCODE", rpCode);
			contractReadOutParams = soiComm.executeCommand(
					SoiUtils.CMD_RATEPLANS_READ, inParams);
			HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
					.get("NUM_RP");
			if ((serviceToModif != null) || (serviceToModif.length != 0)) {
				rpShdes = serviceToModif[0].get("RPCODE_PUB").toString();
				StringTokenizer notAllowedSrv = new StringTokenizer(
						SoiUtils.listNotAllowedRpSrv, ",");
				while ((notAllowedSrv.hasMoreTokens())) {
					if (rpShdes.equals(notAllowedSrv.nextToken())) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_CHANGESTAT_NOT_ALLOWED,
								msgProperties);
					}
				}
			}

			// ContractStatBean co_statbean = new ContractStatBean();

			// build input parameter

			inParams.clear();
			inParams.put("CO_ID", co_id);
			inParams.put("CO_STATUS", co_status);
			inParams.put("REASON", reason);// CO_STATUS_DATE

			// Call CONTRACT.WRITE to put the new status

			// try {
			//
			// inParams.put("VALID_FROM",
			// SoiUtils.getDate(Calendar.getInstance().getTime(), "dd/MM/yyyy")
			// );//CO_STATUS_DATE
			// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );
			if (soiComm == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
			soiComm.executeCommand(SoiUtils.CMD_CONTRACT_WRITE, inParams);

			soiComm.cmsClient.commit();

			if(resMZ!=null){

				try {
				    HashMap< String , Object> inParamsRead = new HashMap<String , Object>();
					inParamsRead.put("CO_ID", co_id);
					inParamsRead.put("SYNC_WITH_DB",true);
					    HashMap<String, Object >  outParamsReadContract = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParamsRead);
					
					
				    for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices   Srv = new bNewServices();
					MassServicesEx SrvEx = new MassServicesEx();
					bNewServicesEx servicesEx = new bNewServicesEx();
					Srv = (bNewServices) it.next();	
					Long spCode = Srv.getSPCODE();
					Long snCode = Srv.getSNCODE();
					
					for (Iterator itt = mzServices.iterator(); itt.hasNext();) {
					    servicesEx = (bNewServicesEx) itt.next();
					    if(servicesEx.getSNCODE()==Srv.getSNCODE()){
						break;
					    }
					}
					service.setSnCode(snCode);
					service.setSpCode(spCode);
					
					ServiceUtils.CheckServiceWithParameters(service, contract, null,soiComm, msgProperties,servicesEx, SoiUtils.SERVICE_STATUT_UNDEFINED, ServicesToAdd);
					
				    }
				    soiComm.cmsClient.commit(); 
				    } catch (Exception e) {
					try {
					    soiComm.cmsClient.rollback();
					} catch (CMSException e1) {
					    // TODO Auto-generated catch block
					    e1.printStackTrace();
					}
					    e.printStackTrace();
					}
				}
			
				
				
				if(resMZ!=null){

				    ArrayList additional_services=new ArrayList();
				    for (int it = 0;it<ServicesToAdd.size(); it++) {
					bNewServices Srv = (bNewServices)ServicesToAdd.get(it);	
					bNewServicesEx servicesEx = (bNewServicesEx)mzServices.get(it);
					
					HashMap serviceaactive = new HashMap();						
					serviceaactive.put("SNCODE", Srv.getSNCODE());
					serviceaactive.put("SPCODE", Srv.getSPCODE());
					
					servicesEx.setCO_ID(co_id);
					servicesEx.setRPCODE(bcontract.getRPCODE());
					
					
					Long snCode = Srv.getSNCODE();
					ArrayList parameterlistaux = new ArrayList();
					
					for (Iterator ItPrm = servicesEx.getBNewParamValueEx().iterator(); ItPrm.hasNext();) {

						HashMap paramvalue = new HashMap();
						bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();
						HashMap[] tabHmMultValues = null;
						HashMap<String, Object> hmParamValues = new HashMap<String, Object>();

						tabHmMultValues = new HashMap[pRmVal.getBNewValueEx().size()];
						HashMap paramv = new HashMap();			
						for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++)
						{							
							bNewValuesEx vaLue  =    pRmVal.getBNewValueEx().get(nVal);
							if(vaLue.getVALUE_DES()!=null){
							    	paramv.put("VALUE_DES",  vaLue.getVALUE_DES());
								paramv.put("VALUE", vaLue.getVALUE());
							}
							
						}
						paramv.put("PRM_ID", pRmVal.getPRM_ID());
						paramv.put("PRM_NO",pRmVal.getPRM_NO());
						parameterlistaux.add(paramv);
						
					}

					if (parameterlistaux.size() > 0) {
					    serviceaactive.put("ListofParamValues", parameterlistaux);
					}
					
			 		additional_services.add(serviceaactive);
					
				}
				    
				    
				    
				    ArrayList returnarrayactivate = new ArrayList();			    
				    String xmlactivateservices = CmsINTUtils.ActivateServicesXML( bcontract.getCO_ID(), additional_services, null, null);
					if (xmlactivateservices != null) {
						HashMap activateservice = new HashMap();
						activateservice.put("ACTION_ID", 4);
						activateservice.put("INPUT_DATA", xmlactivateservices);
						activateservice.put("ORIGIN", "SDP_SEPARATION");					
						returnarrayactivate.add(activateservice);
					} 

					if(returnarrayactivate.size()>0){	
						CmsINTUtils.addDeleteServices(bcontract,returnarrayactivate , username, soiComm);
					}
				}
				
				HashMap<String, Object> inParamsRead = new HashMap<String, Object>();
			inParamsRead.put("CO_ID", co_id);
			inParamsRead.put("SYNC_WITH_DB", true);
			HashMap<String, Object> outParamsReadContract = soiComm
					.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParamsRead);
			co_statbean = BContractAdapter
					.getContractSatutInfo(outParamsReadContract);
		} catch (CMSException cex) {
			
			if(resMZ!=null){

				try {
				    HashMap< String , Object> inParamsRead = new HashMap<String , Object>();
					inParamsRead.put("CO_ID", co_id);
					inParamsRead.put("SYNC_WITH_DB",true);
					    HashMap<String, Object >  outParamsReadContract = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParamsRead);
					
					
				    for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices   Srv = new bNewServices();
					MassServicesEx SrvEx = new MassServicesEx();
					bNewServicesEx servicesEx = new bNewServicesEx();
					Srv = (bNewServices) it.next();	
					Long spCode = Srv.getSPCODE();
					Long snCode = Srv.getSNCODE();
					
					for (Iterator itt = mzServices.iterator(); itt.hasNext();) {
					    servicesEx = (bNewServicesEx) itt.next();
					    if(servicesEx.getSNCODE()==Srv.getSNCODE()){
						break;
					    }
					}
					service.setSnCode(snCode);
					service.setSpCode(spCode);
					
					
					ServiceUtils.addServiceInContract(service, contract, false, soiComm, servicesEx,msgProperties, ServicesToAdd, true);
				    }
				    soiComm.cmsClient.commit(); 
				    } catch (Exception e) {
					try {
					    soiComm.cmsClient.rollback();
					} catch (CMSException e1) {
					    // TODO Auto-generated catch block
					    e1.printStackTrace();
					}
					    e.printStackTrace();
					}
				}
			   
				
			    try {
				    soiComm.cmsClient.rollback();
				} catch (CMSException e1) {
				    // TODO Auto-generated catch block
				    e1.printStackTrace();
				}
			
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				if (cex.getErrors()[0].getErrorCode().contains(
						"CommonDomain.InvalidReasonForStateTransition")) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHANGESTAT_INVALID_REASN,
							msgProperties);
				} else if (cex.getErrors()[0].getErrorCode().contains(
						"CommonDomain.InvalidStateTransition")) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHANGESTAT_INVALID_TRANSI,
							msgProperties);
				} else if (cex.getErrors()[0].getErrorCode().contains(
						"Contract.PendingContractStatusNotAllowedOnSuspDeact")) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHANGESTAT_PENDING_STAT,
							msgProperties);
				} else if (cex.getErrors()[0].getErrorCode().contains(
						"Contract.InvalidState")) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHANGESTAT_INVALID_STAT,
							msgProperties);
				} else if (cex.getErrors()[0].getErrorCode().contains(
						"CMS.Contract.ConsistencyCheckFailed.HLRUniqueness")) {
					throw new FaultWS("RC6004",
							cex.getErrors()[0].getAdditionalInfo());
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getErrors()[0].getAdditionalInfo());
					throw new FaultWS(cex.getErrors()[0].getErrorCode(),
							cex.getErrors()[0].getAdditionalInfo());
				}
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (FaultWS cex) {
			try {
			    soiComm.cmsClient.rollback();
			} catch (CMSException e1) {
				logger.error(getClass().toString() + " : Exception "
						+ e1.getMessage());
			}
			throw cex;

		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
					msgProperties);
		}

		return co_statbean;
	}

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage()
							.contains(
									"CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());

			return rb;
		}

	}

}
