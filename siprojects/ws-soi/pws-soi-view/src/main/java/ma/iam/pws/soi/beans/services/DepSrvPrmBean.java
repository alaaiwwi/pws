package ma.iam.pws.soi.beans.services;
//>******* Created, A.ESSA DI6717
public class DepSrvPrmBean {
	
	private String value_des;
	private String value;
	private Long tmcode;
	private Long sncode;
	private Long prm_id;	

	private String dep_value_des;
	private String dep_value;
	private Long dep_sncode;
	private Long dep_prm_id;
	Long dep_prm_no;
	String prm;
	
	
	public Long getDep_prm_no() {
		return dep_prm_no;
	}
	public void setDep_prm_no(Long dep_prm_no) {
		this.dep_prm_no = dep_prm_no;
	}
	public String getPrm() {
		return prm;
	}
	public void setPrm(String prm) {
		this.prm = prm;
	}
	public String getValue_des() {
		return value_des;
	}
	public void setValue_des(String value_des) {
		this.value_des = value_des;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Long getTmcode() {
		return tmcode;
	}
	public void setTmcode(Long tmcode) {
		this.tmcode = tmcode;
	}
	public Long getSncode() {
		return sncode;
	}
	public void setSncode(Long sncode) {
		this.sncode = sncode;
	}
	public Long getPrm_id() {
		return prm_id;
	}
	public void setPrm_id(Long prm_id) {
		this.prm_id = prm_id;
	}
	public String getDep_value_des() {
		return dep_value_des;
	}
	public void setDep_value_des(String dep_value_des) {
		this.dep_value_des = dep_value_des;
	}
	public String getDep_value() {
		return dep_value;
	}
	public void setDep_value(String dep_value) {
		this.dep_value = dep_value;
	}
	public Long getDep_sncode() {
		return dep_sncode;
	}
	public void setDep_sncode(Long dep_sncode) {
		this.dep_sncode = dep_sncode;
	}
	public Long getDep_prm_id() {
		return dep_prm_id;
	}
	public void setDep_prm_id(Long dep_prm_id) {
		this.dep_prm_id = dep_prm_id;
	}
	
	
}
