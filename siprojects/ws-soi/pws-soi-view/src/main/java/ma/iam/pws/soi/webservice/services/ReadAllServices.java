package ma.iam.pws.soi.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.services.ConfServiceBeanPub;
import ma.iam.pws.soi.beans.services.ConfServicePackagePub;



/**
 * @author mabchour
 *
 */

@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public interface ReadAllServices {
	
	/**
	 * ReadServices . : WS user to identify activated or not service for contract
	 * 
	 * @param coId internal identifier of contract in billing system
	 * @param CO_CODE public identifier of contract in billing system
	 */
	//>******* Created, A.ESSA DI5440
	@WebMethod
	BcontractAll ReadAllServices( @javax.jws.WebParam(name="USERNAME")String username, @javax.jws.WebParam(name="CO_ID") long co_id) throws FaultWS  ;
	
	/**
	 * ReadConfigService . : WS user to identify configuration Service
	 * 
	 * @param coId internal identifier of contract in billing system
	 * @param CO_CODE public identifier of contract in billing system
	 */
	@WebMethod(operationName = "ReadConfigService")
	@WebResult(name = "ConfServiceBeanPub")
	ConfServiceBeanPub ReadConfigService( @javax.jws.WebParam(name="USERNAME")String username, @javax.jws.WebParam(name="CO_ID") long co_id , @javax.jws.WebParam(name="SNCODE") long sncode ) throws FaultWS  ;
	
	/**
	 * ReadOpenServicePack . : WS user to identify associated service pack
	 * 
	 * @param coId internal identifier of contract in billing system
	 * @param CO_CODE public identifier of contract in billing system
	 */
	@WebMethod(operationName = "ReadOpenServicePack")
	@WebResult(name = "ConfServicePackagePub")
	ConfServicePackagePub ReadOpenServicePack( @javax.jws.WebParam(name="USERNAME")String username, @javax.jws.WebParam(name="CO_ID") long co_id , @javax.jws.WebParam(name="SNCODE") long sncode ) throws FaultWS  ;
	
	
	/**
	 * byPAss check if the communication its ok/ or not send a fake compte and chack the return if the compte is not connected
	 * @return
	 */
	@WebMethod(operationName="byPass")
	@WebResult(name="result")
	Integer modeByPass();

}
