package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
@XmlType(name="Contract" , propOrder = { "CO_ID", "CO_ID_PUB", "RPCODE", "BNewParamValueEx", "msgError" })
public class bNewServicesEx  extends bNewServices {

	/**
	 * 
	 */
	private long CO_ID;
	private String CO_ID_PUB;
	private long RPCODE;
	//>******* Updated, A.ESSA ReactivateService
	private String msgError;
	
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}	
	//<******* Updated, A.ESSA ReactivateService
	public long getRPCODE() {
		return RPCODE;
	}

	public void setRPCODE(long rPCODE) {
		RPCODE = rPCODE;
	}

	
	private List<bNewParamValueEx> BNewParamValueEx= new ArrayList<bNewParamValueEx>();

	public List<bNewParamValueEx> getBNewParamValueEx() {
		return BNewParamValueEx;
	}

	public void setBNewParamValueEx(List<bNewParamValueEx> bNewParamValueEx) {
		BNewParamValueEx = bNewParamValueEx;
	}
	
	public long getCO_ID() {
		return CO_ID;
	}

	public void setCO_ID(long cO_ID) {
		CO_ID = cO_ID;
	}

	public String getCO_ID_PUB() {
		return CO_ID_PUB;
	}

	public void setCO_ID_PUB(String cO_ID_PUB) {
		CO_ID_PUB = cO_ID_PUB;
	}
}
