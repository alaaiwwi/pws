package ma.iam.pws.soi.engine.common.util;

import ma.iam.pws.soi.beans.contract.BBsg_configuration;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.errors.BError;
import ma.iam.pws.soi.engine.beans.ResultatBean;
import ma.iam.pws.soi.engine.common.exception.TechnicalException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * The Class Serializer.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class Serializer {

	/** The xstream. */
	static XStream xstream = null;

	/**
	 * Serialize.
	 * 
	 * @param obj
	 *            the obj
	 * @return the string
	 * @throws TechnicalException 
	 */
	public static String serialize(Object obj) throws TechnicalException {
		try {
			if (null == xstream) {
				init();
			}
			return xstream.toXML(obj);
		} catch (Exception ex) {
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	/**
	 * Inits the.
	 */
	public static void init() {
		xstream = new XStream(new DomDriver());
		
//		xstream.alias(ResultatBean.ALIAS, ResultatBean.class);
//		xstream.alias(BUser.ALIAS, BUser.class);
//		xstream.alias(Bcontract.ALIAS, Bcontract.class);
//		xstream.alias(BError.ALIAS, BError.class);
//		xstream.alias(BBsg_configuration.ALIAS, BBsg_configuration.class);
//		xstream.alias(BContractedServices.ALIAS,BContractedServices.class);
		
	}

	/**
	 * Deserialize.
	 * 
	 * @param xml
	 *            the xml
	 * @return the object
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public static Object deserialize(String xml) throws TechnicalException {
		try {
			if (null == xstream) {
				init();
			}
			return xstream.fromXML(xml);
		} catch (Exception ex) {
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

}
