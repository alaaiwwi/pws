package ma.iam.pws.soi.webservice.plaforme;

import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.plateforme.ResultBean;


@javax.jws.WebService(targetNamespace="http://plaforme.webservice.ws.iam.ma/") 
//(endpointInterface = "ma.iam.ws.webservice.plaforme.CheckAvailability",serviceName = "CheckAvailability"  )
public interface CheckAvailability {
	
	/**
	 * CheckAvailability.
	 * Call fake function to check if WS Container if available or not
	 * @return the string
	 */
@javax.jws.WebMethod	
ResultBean 	CheckAvailability() throws FaultWS;

}
