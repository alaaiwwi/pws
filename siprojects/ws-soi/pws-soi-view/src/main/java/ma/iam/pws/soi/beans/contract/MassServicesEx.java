package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
@XmlType(name="Result" , propOrder = { "msgSuccess", "msgError" })
public class MassServicesEx {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//>******* Updated, A.ESSA ReactivateService
	private String msgError;
	private String msgSuccess;
	
	public String getMsgSuccess() {
		return msgSuccess;
	}
	public void setMsgSuccess(String msgSuccess) {
		this.msgSuccess = msgSuccess;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}	
	//<******* Updated, A.ESSA ReactivateService
	
}
