package ma.iam.pws.soi.service.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class PropertiesUtil extends PropertyPlaceholderConfigurer {
	
	private static final Logger logger = LogManager.getLogger(PropertiesUtil.class.getName()) ;
	public static Map propertiesMap;

	@Override
	   protected void processProperties(ConfigurableListableBeanFactory beanFactory,
	             Properties props) throws BeansException {
	        super.processProperties(beanFactory, props);
	 
	        propertiesMap = new HashMap<String, String>();
	        for (Object key : props.keySet()) {
	            String keyStr = key.toString();
	            propertiesMap.put(keyStr, parseStringValue(props.getProperty(keyStr), props, new HashSet()));
	        }
	        try {
	       	        	logger.info("org.omg.CORBA.ORBInitialHost  : " +  getProperty("org.omg.CORBA.ORBInitialHost"));
	        	logger.info("org.omg.CORBA.ORBInitialPort  : " +  getProperty("org.omg.CORBA.ORBInitialPort"));
	        	logger.info("org.omg.CORBA.ORBInitRef  : " +  getProperty("org.omg.CORBA.ORBInitRef"));
	        	logger.info("org.omg.CORBA.ORBInitRef.NameService  : " +  getProperty("org.omg.CORBA.ORBInitRef.NameService"));
	        	logger.info("soi.cil.name  : " +  getProperty("soi.cil.name"));
	        	logger.info("soi.cil.version  : " +  getProperty("soi.cil.version"));
	        	logger.info("soi.security.name  : " +  getProperty("soi.security.name"));
	        	logger.info("soi.security.version  : " +  getProperty("soi.security.version"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("Erreur Parsing Orb.properties");
			}
	        
	        
	      //  if (!propertiesMap.containsKey("org.omg.CORBA.ORBInitialPort")){
	        //	logger.info("org.omg.CORBA.ORBInitialPort  : " +  getProperty("org.omg.CORBA.ORBInitialPort"));
	        //	logger.info("org.omg.CORBA.ORBInitialHost  : " +   getProperty("org.omg.CORBA.ORBInitialHost"));
	        	
	        /*	propertiesMap.put("org.omg.CORBA.ORBInitRef","corbaloc:iiop:"+getProperty("org.omg.CORBA.ORBInitialHost")+":"+getProperty("org.omg.CORBA.ORBInitialPort") + "/NameService");
	        	propertiesMap.put("ccl.autoloadRpSpSnCodes","false");
	        	propertiesMap.put("cms.clientpoolsize","20");
	        	propertiesMap.put("cms.timetowaitforclient","10000");
	        	propertiesMap.put("cms.version","9");
	        	propertiesMap.put("cms.servername","com/lhs/public/soi/fedfactory1");
	        	propertiesMap.put("cms.username","ATSTEST");
	        	propertiesMap.put("cms.password","ffff");*/
	       // }
	       // if (!propertiesMap.containsKey("org.omg.CORBA.ORBInitialHost")){
	        	//logger.info("org.omg.CORBA.ORBInitialHost  : " +  System.getProperty("org.omg.CORBA.ORBInitialHost"));
	        	
	       // }
	    }
	 
	    public static String getProperty(String name) {
	        return (String) propertiesMap.get(name);
	    }
	}