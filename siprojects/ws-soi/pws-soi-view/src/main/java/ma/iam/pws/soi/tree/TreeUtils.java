package ma.iam.pws.soi.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.beans.dependency.CheckDependencyBean;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.exception.Faul;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.Dependency;
import ma.iam.pws.soi.webservice.services.impl.CheckDependencyImpl;

@SuppressWarnings({ "unchecked", "deprecation", "static-access", "rawtypes" })
public class TreeUtils {

	private static final Logger logger = LogManager
			.getLogger(CheckDependencyImpl.class.getName());


	public static HashMap getHeadNode(ArrayList complexDep, String data) {
		for (int i = 0; i < complexDep.size(); i++) {
			if (data.equals(((HashMap) complexDep.get(i)).get("EBS"))) {
				return (HashMap) complexDep.get(i);
			}
		}
		return null;
	}

	public static Node getNodeFromArray(String data, ArrayList createdNodes) {
		Node node = null;
		for (int i = 0; i < createdNodes.size(); i++) {
			if (((Node) createdNodes.get(i)).getData().equals(data)) {
				node = (Node) createdNodes.get(i);
				break;
			}
		}
		return node;

	}

	public static Node buildTree(HashMap headNode, ArrayList complexDep,
			Node root, List sncodes, ArrayList createdNodes) {
		HashMap hm = getNodeOperation(headNode);
		String op = (String) hm.get("OP");
		String data1 = (String) hm.get("Data1");
		String data2 = (String) hm.get("Data2");

		String data = (Long) headNode.get("SNCODE") > 0 ? headNode
				.get("SNCODE").toString() : headNode.get("EBS").toString();

		Node simpleNode = new Node(op, data, true, root,
				(Long) headNode.get("ID"));

		createdNodes.add(simpleNode);

		if (data1 != null) {
			if ("0123456789".contains(data1.substring(0, 1))) {
				Node childNode1 = getNodeFromRoot(data1, root, new ArrayList());
				if (childNode1 == null) {
					childNode1 = getNodeFromArray(data1, createdNodes);
					if (childNode1 == null) {
						childNode1 = new Node(null, data1, false, root,
								(Long) headNode.get("ID"));
						createdNodes.add(childNode1);
					} else
						childNode1.addID((Long) headNode.get("ID"));
				} else
					childNode1.addID((Long) headNode.get("ID"));
				simpleNode.addChild(childNode1);
			} else {
				HashMap headN = getHeadNode(complexDep, data1);
				Node childNode1 = buildTree(headN, complexDep, root, sncodes,
						createdNodes);
				simpleNode.addChild(childNode1);
			}
		}

		if (data2 != null) {
			if ("0123456789".contains(data2.substring(0, 1))) {
				Node childNode2 = getNodeFromRoot(data2, root, new ArrayList());
				if (childNode2 == null) {
					childNode2 = getNodeFromArray(data2, createdNodes);
					if (childNode2 == null) {
						childNode2 = new Node(null, data2, false, root,
								(Long) headNode.get("ID"));
						createdNodes.add(childNode2);
					} else
						childNode2.addID((Long) headNode.get("ID"));
				} else
					childNode2.addID((Long) headNode.get("ID"));
				simpleNode.addChild(childNode2);
			} else {
				HashMap headN = getHeadNode(complexDep, data2);
				Node childNode2 = buildTree(headN, complexDep, root, sncodes,
						createdNodes);
				simpleNode.addChild(childNode2);
			}
		}
		return simpleNode;
	}

	public static Node buildComplexNode(ArrayList complexDep, Node root,
			Long mergeFlag, HashMap headNode, List sncodes) {

		Node node = buildTree(headNode, complexDep, root, sncodes,
				new ArrayList());
		Node simpleNode = getNodeFromRoot(node.getData(), root, new ArrayList());
		if (simpleNode != null) {
			simpleNode.addID((Long) headNode.get("ID"));
			if (!simpleNode.isConnRoot() && simpleNode.isLeaf()) {
				node.setIds(simpleNode.getIDs());
				grantParents(simpleNode, node);
				simpleNode = null;
				return node;
			} else {// There is already dependency for this service so we have
					// to merge them
				splitNodeFromRoot(headNode.get("SNCODE").toString(), root);// we
																			// split
																			// here
																			// because
																			// we
																			// readd
																			// it
																			// in
																			// method
																			// which
																			// calls
																			// this
																			// one
				simpleNode.setSimple(false);
				Node tempNode = new Node(simpleNode.getOp(),
						"A".concat(mergeFlag.toString()), false, root,
						(Long) headNode.get("ID"));
				grantChilds(simpleNode, tempNode);
				simpleNode.addChild(tempNode);
				simpleNode.addChild(node);
				node.setData("B".concat((mergeFlag).toString()));
				simpleNode.setOp("AND");
			}
		} else {
			return node;
		}

		return simpleNode;
	}

	public static Node buildSimpleNode(HashMap dep, Node root, Long mergeFlag) {

		Node simpleNode = getNodeFromRoot(dep.get("SNCODE").toString(), root,
				new ArrayList());
		if (simpleNode != null) {
			simpleNode.addID((Long) dep.get("ID"));
			if (!simpleNode.isConnRoot() && simpleNode.isLeaf()) {
				simpleNode.setSimple(true);
				HashMap hm = getNodeOperation(dep);
				String op = (String) hm.get("OP");
				simpleNode.setOp(op);
				String data1 = (String) hm.get("Data1");
				String data2 = (String) hm.get("Data2");
				if (op != null && data1 != null) {
					Node childNode = getNodeFromRoot(data1, root,
							new ArrayList());
					if (childNode == null)
						childNode = new Node(null, data1, false, root,
								(Long) dep.get("ID"));
					else
						childNode.addID((Long) dep.get("ID"));
					simpleNode.addChild(childNode);

					if (data2 != null) {

						Node childNode2 = getNodeFromRoot(data2, root,
								new ArrayList());
						if (childNode2 == null)
							childNode2 = new Node(null, data2, false, root,
									(Long) dep.get("ID"));
						else
							childNode2.addID((Long) dep.get("ID"));
						simpleNode.addChild(childNode2);
					}
				}

			} else {// There is already dependency for this service so we have
					// to merge them
				splitNodeFromRoot(dep.get("SNCODE").toString(), root);// we
																		// split
																		// here
																		// because
																		// we
																		// readd
																		// it in
																		// method
																		// which
																		// calls
																		// this
																		// one
				simpleNode.setSimple(false);
				Node tempNode = new Node(simpleNode.getOp(),
						"A".concat(mergeFlag.toString()), false, root,
						(Long) dep.get("ID"));
				simpleNode.setOp("AND");
				grantChilds(simpleNode, tempNode);
				simpleNode.addChild(tempNode);

				HashMap hm = getNodeOperation(dep);
				String op = (String) hm.get("OP");
				String data1 = (String) hm.get("Data1");
				String data2 = (String) hm.get("Data2");
				if (op != null && data1 != null) {
					Node childNode = getNodeFromRoot(data1, root,
							new ArrayList());
					if (childNode == null)
						childNode = new Node(null, data1, false, root,
								(Long) dep.get("ID"));
					else
						childNode.addID((Long) dep.get("ID"));
					if (!"NEEDS".equals(op)) {
						Node complexChildNode = new Node(op,
								"B".concat((mergeFlag).toString()), false,
								root, (Long) dep.get("ID"));
						complexChildNode.addChild(childNode);

						if (data2 != null) {
							Node childNode2 = getNodeFromRoot(data2, root,
									new ArrayList());
							if (childNode2 == null)
								childNode2 = new Node(null, data2, false, root,
										(Long) dep.get("ID"));
							else
								childNode2.addID((Long) dep.get("ID"));
							complexChildNode.addChild(childNode2);
						}

						simpleNode.addChild(complexChildNode);
					} else {
						simpleNode.addChild(childNode);
					}
				} else
					return null;
			}

		} else {
			HashMap hm = getNodeOperation(dep);
			String op = (String) hm.get("OP");
			String data1 = (String) hm.get("Data1");
			String data2 = (String) hm.get("Data2");
			if (op != null && data1 != null) {
				simpleNode = new Node(op, dep.get("SNCODE").toString(), true,
						root, (Long) dep.get("ID"));
				Node childNode = getNodeFromRoot(data1, root, new ArrayList());
				if (childNode == null)
					childNode = new Node(null, data1, false, root,
							(Long) dep.get("ID"));
				else
					childNode.addID((Long) dep.get("ID"));
				simpleNode.addChild(childNode);
				if (data2 != null) {
					// if(!sncodes.contains(data2)){
					// sncodes.add(data2);
					// }
					// else{
					// System.out.println(sncodes);
					// }
					Node childNode2 = getNodeFromRoot(data2, root,
							new ArrayList());
					if (childNode2 == null)
						childNode2 = new Node(null, data2, false, root,
								(Long) dep.get("ID"));
					else
						childNode2.addID((Long) dep.get("ID"));
					simpleNode.addChild(childNode2);
				}

			} else
				return null;
		}

		return simpleNode;
	}

	public static Node getNodeFromRoot(String data, Node root, ArrayList parsed) {
		Node node = null;
		List rootChilds = root.getChilds();
		if (rootChilds != null)
			for (int i = 0; i < rootChilds.size(); i++) {
				Node currNode = (Node) rootChilds.get(i);
				if (!parsed.contains(currNode)) {
					parsed.add(currNode);
				} else {
					continue;
				}
				if (((Node) rootChilds.get(i)).getData().equals(data)) {
					node = (Node) rootChilds.get(i);
					break;
				} else {
					node = getNodeFromRoot(data, (Node) rootChilds.get(i),
							parsed);
					if (node != null)
						break;
				}

			}
		return node;

	}

	public static void splitNodeFromRoot(String data, Node root) {
		List rootChilds = root.getChilds();
		List parents = null;
		for (int i = 0; i < rootChilds.size(); i++) {
			if (((Node) rootChilds.get(i)).getData().equals(data)) {
				parents = ((Node) rootChilds.get(i)).getParents();
				parents.remove(root);
				((Node) rootChilds.get(i)).setParents(parents);
				rootChilds.remove(i);
				root.setChilds(rootChilds);
				break;
			}
		}
	}

	public static void deleteChildParent(Node source) {
		List sourceChilds = source.getChilds();

		for (int i = 0; i < sourceChilds.size(); i++) {
			List destParents = ((Node) sourceChilds.get(i)).getParents();
			for (int j = 0; j < destParents.size(); j++) {
				if (((Node) destParents.get(j)).equals(source)) {
					destParents.remove(j);
				}
			}
			((Node) sourceChilds.get(i)).setParents(destParents);
		}
	}

	public static void deleteParentChild(Node source) {
		List sourceParents = source.getParents();

		for (int i = 0; i < sourceParents.size(); i++) {
			List destChilds = ((Node) sourceParents.get(i)).getChilds();
			for (int j = 0; j < destChilds.size(); j++) {
				if (((Node) destChilds.get(j)).equals(source)) {
					destChilds.remove(j);
				}
			}
			((Node) sourceParents.get(i)).setChilds(destChilds);
		}
	}

	public static void grantChilds(Node source, Node dest) {
		List sourceChilds = source.getChilds();

		for (int i = 0; i < sourceChilds.size(); i++) {
			dest.addChild((Node) sourceChilds.get(i));
		}
		deleteChildParent(source);
		source.setChilds(new ArrayList());
	}

	public static void grantParents(Node source, Node dest) {
		List sourceParents = source.getParents();

		for (int i = 0; i < sourceParents.size(); i++) {
			((Node) sourceParents.get(i)).addChild(dest);
		}
		deleteParentChild(source);
		source.setParents(new ArrayList());
	}

	public static HashMap getNodeOperation(HashMap dep) {
		HashMap hm = new HashMap();
		String op = null;
		String data1 = null;
		String data2 = null;
		if (!"".equals(dep.get("NEEDS"))) {
			op = "NEEDS";
			data1 = dep.get("NEEDS").toString();

			if (!"".equals(dep.get("AND_OP"))) {
				data2 = dep.get("AND_OP").toString();
				op = "AND";
			} else if (!"".equals(dep.get("OR_OP"))) {
				data2 = dep.get("OR_OP").toString();
				op = "OR";
			}
		} else if (!"".equals(dep.get("NOT_OP"))) {
			data1 = dep.get("NOT_OP").toString();
			op = "NOT";
		} else
			return null;

		hm.put("OP", op);
		hm.put("Data1", data1);
		hm.put("Data2", data2);
		return hm;

	}

	public static Node loadDependencyTree(SOIService soiComm, HashMap[] deps,
			Properties msgProperties) throws FaultWS {
		CheckDependencyBean prfElg = new CheckDependencyBean();
		Long mergeFlag = new Long(0);
		Node root = Tree.getTree().getRoot();

		if (root.getChilds().size() > 0) {
			return root;
		}
		try {

			if ((deps == null) || (deps.length == 0)) {
				return null;
			}
			for (int i = 0; i < deps.length; i++) {
				Long sncode = (Long) deps[i].get("SNCODE");
				Node treeNode = null;
				if (deps[i].get("ID").toString().equals("9")) {
					new Long(1);
				}
				if (sncode > 0) {
					if (deps[i].get("CONSISTENCY_TYPE").toString().equals("S")) {
						treeNode = buildSimpleNode(deps[i], root, mergeFlag);
					} else {
						ArrayList complexDep = new ArrayList();
						HashMap headNode = null;
						Long id = (Long) deps[i].get("ID");
						for (int j = i; j < deps.length; j++) {
							if (id.equals(deps[j].get("ID"))) {
								if ((Long) deps[j].get("SNCODE") > 0) {
									headNode = deps[j];
								} else {
									complexDep.add(deps[j]);
								}
							}
						}

						treeNode = buildComplexNode(complexDep, root,
								mergeFlag, headNode, new ArrayList());
					}

				}
				mergeFlag++;
				if (treeNode != null) {
					root.addChild(treeNode);
				}
			}

		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error("CheckDependencyImpl: Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}
		removeComplexNotsFromRoot(root, new ArrayList(), new Long(1));
		checkDuplicationInRoot(root, new ArrayList(), new ArrayList());
		Node node = getNodeFromRoot("312", root, new ArrayList());
		// ArrayList p = getAllParents(node, new ArrayList());
		// ArrayList c = getAllChilds(node, new ArrayList());

		return root;
	}

	public static ArrayList getAllParents(Node node, ArrayList parsed) {
		List nodeParents = node.getParents();
		ArrayList result = new ArrayList();
		if (nodeParents != null)
			for (int i = 0; i < nodeParents.size(); i++) {
				String currsncode = ((Node) nodeParents.get(i)).getData();
				Node currNode = ((Node) nodeParents.get(i));
				if (!parsed.contains(currNode)) {
					parsed.add(currNode);
					if (nodeParents.get(i) != null
							&& currsncode != null
							&& "0123456789"
									.contains(currsncode.substring(0, 1)))
						result.add(currsncode);
					else {
						if (nodeParents.get(i) != null && currsncode != null) {
							result.addAll(getAllParents(
									(Node) nodeParents.get(i), parsed));
						}
					}
				} else {
					continue;
				}

			}
		return result;

	}

	public static ArrayList getAllChilds(Node node, ArrayList parsed) {
		List nodeChilds = node.getChilds();
		ArrayList result = new ArrayList();
		if (nodeChilds != null)
			for (int i = 0; i < nodeChilds.size(); i++) {
				String currsncode = ((Node) nodeChilds.get(i)).getData();
				Node currNode = ((Node) nodeChilds.get(i));
				if (!parsed.contains(currNode)) {
					parsed.add(currNode);
					if (nodeChilds.get(i) != null
							&& currsncode != null
							&& "0123456789"
									.contains(currsncode.substring(0, 1)))
						result.add(currsncode);
					else {
						if (nodeChilds.get(i) != null && currsncode != null) {
							result.addAll(getAllChilds(
									(Node) nodeChilds.get(i), parsed));
						}
					}
				} else {
					continue;
				}

			}
		return result;

	}

	public static void checkDuplicationInRoot(Node root, ArrayList parsed,
			ArrayList sncodes) {
		List rootChilds = root.getChilds();
		if (rootChilds != null)
			for (int i = 0; i < rootChilds.size(); i++) {
				String currsncode = ((Node) rootChilds.get(i)).getData();
				Node currNode = ((Node) rootChilds.get(i));
				if (!parsed.contains(currNode)) {
					parsed.add(currNode);
					if (sncodes.contains(currsncode)) {
						if (rootChilds.get(i) != null
								&& currsncode != null
								&& "0123456789".contains(currsncode.substring(
										0, 1)))
							System.out.println(((Node) rootChilds.get(i))
									.toString());
					} else {
						sncodes.add(currsncode);
					}
				} else {
					continue;
				}

				if (currsncode == null || currsncode == "") {
					continue;
				} else {
					checkDuplicationInRoot((Node) rootChilds.get(i), parsed,
							sncodes);
				}
			}

	}

	// public static ArrayList getServicesToAdd(Node root, String sncode, List
	// contrActivServ, String op, HashMap[] deps){
	// List rootChilds = root.getChilds();
	// ArrayList nodesToActivate = new ArrayList();
	// ArrayList res = new ArrayList();
	// if(rootChilds!=null){
	// Node node = getNodeFromRoot(sncode, root, new ArrayList());
	// if(op.equals("ACTIVATE")){
	// nodesToActivate = getServicesToAddWithActivation( root, sncode,
	// contrActivServ);
	// }
	// }
	// return res;
	//
	// }

	public static HashMap getServicesToAddDel(Node root, String sncode,
			List contrActivServ, String op, HashMap[] deps, Long tmcode,
			List contrEligibleServ, HashMap[] servDeps, Properties msgProperties)
			throws FaultWS {
		List rootChilds = root.getChilds();
		ArrayList servicesToDel = new ArrayList();
		ArrayList servicesToAdd = new ArrayList();
		ArrayList parentServicesToDel = new ArrayList();
		ArrayList childServicesToDel = new ArrayList();
		ArrayList DependsToDel = new ArrayList();
		ArrayList DependsToAdd = new ArrayList();
		HashMap res = new HashMap();
		Node node = null;
		if (rootChilds != null) {
			node = getNodeFromRoot(sncode, root, new ArrayList());

			if (node != null) {
				if (op.equals("ACTIVATE")) {
					parentServicesToDel = getParentServicesToDelWithActivation(
							node, sncode, contrActivServ, new ArrayList());

					res = getTreeContractEvaluationAddDelNodes(node, sncode,
							contrActivServ, contrEligibleServ, new ArrayList(),
							new ArrayList(), deps, tmcode, msgProperties);

					if ((Boolean) res.get("EVALUATION")) {
						childServicesToDel = (ArrayList) res
								.get("NODESTODECTIVATE");
						servicesToAdd = (ArrayList) res.get("NODESTOACTIVATE");
					}

					// childServicesToDel = getChildServicesToDelWithActivation(
					// node, sncode, contrActivServ, new ArrayList(), deps,
					// tmcode);
					servicesToDel = objectUnion(parentServicesToDel,
							childServicesToDel);
				} else {
					servicesToDel = getServicesToDelWithDeactivation(node,
							sncode, contrActivServ, new ArrayList());

				}
			}

		}

		for (int i = 0; i < servicesToDel.size(); i++) {
			ArrayList consistencyDelIDs = longIntersection(
					(ArrayList) node.getIDs(),
					(ArrayList) ((Node) servicesToDel.get(i)).getIDs());
			if (consistencyDelIDs.size() == 0) {
				// if(i>0)
				// consistencyAddIDs =
				// longIntersection((ArrayList)((Node)servicesToAdd.get(i-1)).getIDs(),
				// (ArrayList)((Node)servicesToAdd.get(i)).getIDs());
				// consistencyDelIDs =
				// (ArrayList)((Dependency)DependsToDel.get(i-1)).getDependency_ids();
			}
			if (consistencyDelIDs.size() > 1) {
				ArrayList tempconsistencyDelIDs = new ArrayList();
				for (int j = 0; j < servDeps.length; j++) {
					if (servDeps[j].get("SNCODE") != null
							&& (node.getData().equals(
									servDeps[j].get("SNCODE").toString()) || (((Node) servicesToDel
									.get(i)).getData().equals(servDeps[j].get(
									"SNCODE").toString())))) {
						if (consistencyDelIDs.contains(servDeps[j].get("ID"))) {
							tempconsistencyDelIDs.add(servDeps[j].get("ID"));
						}
					}
				}

				// if(tempconsistencyDelIDs.size()==0){
				// for(int j = 0; j<servDeps.length; j++){
				// if(servDeps[j].get("SNCODE") !=null
				// &&(((Node)servicesToDel.get(i)).getData().equals(servDeps[j].get("SNCODE").toString()))){
				// if(consistencyDelIDs.contains(servDeps[j].get("ID"))){
				// tempconsistencyDelIDs.add(servDeps[j].get("ID"));
				// }
				// }
				// }
				// }
				consistencyDelIDs = tempconsistencyDelIDs;
			}
			Dependency dep = new Dependency(Long.valueOf(((Node) servicesToDel
					.get(i)).getData()), consistencyDelIDs);
			DependsToDel.add(dep);
		}

		for (int i = 0; i < servicesToAdd.size(); i++) {
			ArrayList consistencyAddIDs = longIntersection(
					(ArrayList) node.getIDs(),
					(ArrayList) ((Node) servicesToAdd.get(i)).getIDs());
			if (consistencyAddIDs.size() == 0) {
				// if(i>0)
				// consistencyAddIDs =
				// longIntersection((ArrayList)((Node)servicesToAdd.get(i-1)).getIDs(),
				// (ArrayList)((Node)servicesToAdd.get(i)).getIDs());
				// consistencyAddIDs =
				// (ArrayList)((Dependency)DependsToAdd.get(i-1)).getDependency_ids();
			}
			if (consistencyAddIDs.size() > 1) {
				ArrayList tempconsistencyDelIDs = new ArrayList();
				for (int j = 0; j < servDeps.length; j++) {
					if (servDeps[j].get("SNCODE") != null
							&& node.getData().equals(
									servDeps[j].get("SNCODE").toString())) {
						if (consistencyAddIDs.contains(servDeps[j].get("ID"))) {
							tempconsistencyDelIDs.add(servDeps[j].get("ID"));
						}
					}
				}
				consistencyAddIDs = tempconsistencyDelIDs;
			}
			Dependency dep = new Dependency(Long.valueOf(((Node) servicesToAdd
					.get(i)).getData()), consistencyAddIDs);
			DependsToAdd.add(dep);
		}

		res.put("NODESTODECTIVATE", DependsToDel);
		res.put("NODESTOACTIVATE", DependsToAdd);
		return res;

	}

	public static HashMap getTreeContractEvaluationAddDelNodes(Node root,
			String sncode, List contrActivServ, List contrEligibleServ,
			ArrayList parsed, ArrayList parsedNot, HashMap[] deps, Long tmcode,
			Properties msgProperties) throws FaultWS {
		List<Node> rootChilds = root.getChilds();
		HashMap res = new HashMap();
		ArrayList nodesToDeactivate = new ArrayList();
		ArrayList nodesToActivate = new ArrayList();
		Boolean evaluation = false;

		String currsncode = root.getData();
		if (root != null && rootChilds != null && rootChilds.size() > 0) {
			String nextSncode = rootChilds.get(0).getData();
			if ("NOT".equals(root.getOp())) {
				HashMap hm = getTreeContractEvaluationNodes(root, sncode,
						contrActivServ, parsed, parsedNot, deps, tmcode);
				if ((Boolean) hm.get("EVALUATION")) {
					evaluation = (Boolean) hm.get("EVALUATION");
					nodesToDeactivate.addAll((List) hm.get("NODES"));
				}
			} else if ("NEEDS".equals(root.getOp())) {
				if ("0123456789".contains(nextSncode.substring(0, 1))) {
					if (!contrActivServ.contains(nextSncode)) {
						if (contrEligibleServ.contains(nextSncode)) {
							nodesToActivate.add(rootChilds.get(0));
							evaluation = true;
						} else
							evaluation = false;

					} else
						evaluation = true;
				} else
					evaluation = true;
				// else{
				if (!parsed.contains(rootChilds.get(0))) {
					parsed.add(rootChilds.get(0));
					if (evaluation && rootChilds.get(0).getChilds() != null
							&& !contrActivServ.contains(nextSncode)) {
						HashMap hm = getTreeContractEvaluationAddDelNodes(
								rootChilds.get(0), sncode, contrActivServ,
								contrEligibleServ, parsed, parsedNot, deps,
								tmcode, msgProperties);
						if ((Boolean) hm.get("EVALUATION")) {
							evaluation = (Boolean) hm.get("EVALUATION");
							nodesToActivate.addAll((List) hm
									.get("NODESTOACTIVATE"));
							nodesToDeactivate.addAll((List) hm
									.get("NODESTODECTIVATE"));
						} else {
							nodesToActivate.remove(rootChilds.get(0));
							evaluation = false;
						}
					}
				}

				// return
				// getTreeContractEvaluationAddDelNodes(rootChilds.get(0),sncode,
				// contrActivServ, contrEligibleServ, parsed, deps, tmcode);
				// }
			} else {
				Boolean eval1 = false;
				Boolean eval2 = false;
				ArrayList nodesToActivate1 = new ArrayList();
				ArrayList nodesToActivate2 = new ArrayList();
				ArrayList nodesToDectivate1 = new ArrayList();
				ArrayList nodesToDectivate2 = new ArrayList();
				String nextSncode2 = rootChilds.get(1).getData();
				if ("AND".equals(root.getOp())) {
					if ("0123456789".contains(nextSncode.substring(0, 1))) {
						if (!contrActivServ.contains(nextSncode)) {
							if (contrEligibleServ.contains(nextSncode)) {
								nodesToActivate1.add(rootChilds.get(0));
								eval1 = true;
							} else
								eval1 = false;
						} else
							eval1 = true;
					} else {
						eval1 = true;
					}
					if (!parsed.contains(rootChilds.get(0))) {
						parsed.add(rootChilds.get(0));
						if (eval1 && rootChilds.get(0).getChilds() != null
								&& !contrActivServ.contains(nextSncode)) {
							HashMap hm = getTreeContractEvaluationAddDelNodes(
									rootChilds.get(0), sncode, contrActivServ,
									contrEligibleServ, parsed, parsedNot, deps,
									tmcode, msgProperties);
							nodesToActivate1.addAll((List) hm
									.get("NODESTOACTIVATE"));
							nodesToDectivate1.addAll((List) hm
									.get("NODESTODECTIVATE"));
							eval1 = (Boolean) hm.get("EVALUATION");
						}
					}

					if ("0123456789".contains(nextSncode2.substring(0, 1))) {
						if (!contrActivServ.contains(nextSncode2)) {
							if (contrEligibleServ.contains(nextSncode2)) {
								nodesToActivate2.add(rootChilds.get(1));
								eval2 = true;
							} else
								eval2 = false;
						} else
							eval2 = true;
					} else {
						eval2 = true;
					}
					if (!parsed.contains(rootChilds.get(1))) {
						parsed.add(rootChilds.get(1));
						if (eval2 && rootChilds.get(1).getChilds() != null
								&& !contrActivServ.contains(nextSncode2)) {
							HashMap hm = getTreeContractEvaluationAddDelNodes(
									rootChilds.get(1), sncode, contrActivServ,
									contrEligibleServ, parsed, parsedNot, deps,
									tmcode, msgProperties);
							nodesToActivate2.addAll((List) hm
									.get("NODESTOACTIVATE"));
							nodesToDectivate2.addAll((List) hm
									.get("NODESTODECTIVATE"));
							eval2 = (Boolean) hm.get("EVALUATION");
						}
					}

					// else{
					// HashMap hm =
					// getTreeContractEvaluationAddDelNodes(rootChilds.get(1),sncode,
					// contrActivServ, contrEligibleServ, parsed, deps, tmcode);
					// nodesToActivate2.addAll((List)hm.get("NODESTOACTIVATE"));
					// nodesToDectivate2.addAll((List)hm.get("NODESTODECTIVATE"));
					// eval2 = (Boolean)hm.get("EVALUATION");
					// }
					if (eval1 && eval2) {
						nodesToActivate = objectUnion(nodesToActivate1,
								nodesToActivate2);
						nodesToDeactivate = objectUnion(nodesToDectivate1,
								nodesToDectivate2);

						// Long priorityEval1 = evaluateList(nodesToDectivate1,
						// deps, tmcode);
						// Long priorityEval2 = evaluateList(nodesToDectivate2,
						// deps, tmcode);
						// if(priorityEval1>priorityEval2)
						// nodesToDeactivate = nodesToDectivate1;
						// else
						// nodesToDeactivate = nodesToDectivate2;

						evaluation = true;
					}

				} else if ("OR".equals(root.getOp())) {
					if ("0123456789".contains(nextSncode.substring(0, 1))) {
						if (!contrActivServ.contains(nextSncode)) {
							if (contrEligibleServ.contains(nextSncode)) {
								nodesToActivate1.add(rootChilds.get(0));
								eval1 = true;
							} else
								eval1 = false;
						} else
							eval1 = true;
					} else {
						eval1 = true;
					}
					if (!parsed.contains(rootChilds.get(0))) {
						parsed.add(rootChilds.get(0));
						if (eval1 && rootChilds.get(0).getChilds() != null
								&& !contrActivServ.contains(nextSncode)) {
							HashMap hm = getTreeContractEvaluationAddDelNodes(
									rootChilds.get(0), sncode, contrActivServ,
									contrEligibleServ, parsed, parsedNot, deps,
									tmcode, msgProperties);
							nodesToActivate1.addAll((List) hm
									.get("NODESTOACTIVATE"));
							nodesToDectivate1.addAll((List) hm
									.get("NODESTODECTIVATE"));
							eval1 = (Boolean) hm.get("EVALUATION");
						}
					}

					// else{
					// HashMap hm =
					// getTreeContractEvaluationAddDelNodes(rootChilds.get(0),sncode,
					// contrActivServ, contrEligibleServ, parsed, deps, tmcode);
					// nodesToActivate1.addAll((List)hm.get("NODESTOACTIVATE"));
					// nodesToDectivate1.addAll((List)hm.get("NODESTODECTIVATE"));
					// eval1 = (Boolean)hm.get("EVALUATION");
					// }

					if ("0123456789".contains(nextSncode2.substring(0, 1))) {
						if (!contrActivServ.contains(nextSncode2)) {
							if (contrEligibleServ.contains(nextSncode2)) {
								nodesToActivate2.add(rootChilds.get(1));
								eval2 = true;
							} else
								eval2 = false;
						} else
							eval2 = true;
					} else {
						eval2 = true;
					}
					if (!parsed.contains(rootChilds.get(1))) {
						parsed.add(rootChilds.get(1));
						if (eval2 && rootChilds.get(1).getChilds() != null
								&& !contrActivServ.contains(nextSncode2)) {
							HashMap hm = getTreeContractEvaluationAddDelNodes(
									rootChilds.get(1), sncode, contrActivServ,
									contrEligibleServ, parsed, parsedNot, deps,
									tmcode, msgProperties);
							nodesToActivate2.addAll((List) hm
									.get("NODESTOACTIVATE"));
							nodesToDectivate2.addAll((List) hm
									.get("NODESTODECTIVATE"));
							eval2 = (Boolean) hm.get("EVALUATION");
						}
					}

					// else{
					// HashMap hm =
					// getTreeContractEvaluationAddDelNodes(rootChilds.get(1),sncode,
					// contrActivServ, contrEligibleServ, parsed, deps, tmcode);
					// nodesToActivate2.addAll((List)hm.get("NODESTOACTIVATE"));
					// nodesToDectivate2.addAll((List)hm.get("NODESTODECTIVATE"));
					// eval2 = (Boolean)hm.get("EVALUATION");
					// }
					if (eval1 && eval2) {
						if (nodesToActivate1.size() > 0
								&& nodesToActivate2.size() > 0) {
							Long priorityEval1 = evaluateList(nodesToActivate1,
									deps, tmcode);
							Long priorityEval2 = evaluateList(nodesToActivate2,
									deps, tmcode);
							if (priorityEval1 < priorityEval2) {
								nodesToActivate = nodesToActivate1;
								nodesToDeactivate = nodesToDectivate1;
							} else if (!priorityEval1.equals(new Long(1000))) {
								nodesToActivate = nodesToActivate2;
								nodesToDeactivate = nodesToDectivate2;
							} else {
								throw new FaultWS(
										Constants.ERROR_CODE.MT_WS_ACTIVATESVC_NO_PRIORITY_ERROR,
										msgProperties);
							}
							evaluation = true;
						} else if (nodesToActivate2.size() == 0) {
							nodesToActivate = nodesToActivate2;
							nodesToDeactivate = nodesToDectivate2;
							evaluation = true;
						} else {
							nodesToActivate = nodesToActivate1;
							nodesToDeactivate = nodesToDectivate1;
							evaluation = true;
						}
					} else {

						if (eval1) {
							nodesToActivate = nodesToActivate1;
							nodesToDeactivate = nodesToDectivate1;
							evaluation = true;
						} else if (eval2) {
							nodesToActivate = nodesToActivate2;
							nodesToDeactivate = nodesToDectivate2;
							evaluation = true;
						} else
							evaluation = false;
					}

				}
			}
		} else {
			evaluation = true;
		}
		res.put("EVALUATION", evaluation);
		res.put("NODESTODECTIVATE", nodesToDeactivate);
		res.put("NODESTOACTIVATE", nodesToActivate);
		return res;
	}

	public static ArrayList getIDsIntersection(List servicesToDel, Node root) {
		ArrayList nodeIDs = (ArrayList) root.getIDs();
		ArrayList res = new ArrayList();
		for (int i = 0; i < servicesToDel.size(); i++) {
			res.addAll(longIntersection(nodeIDs,
					(ArrayList) ((Node) servicesToDel.get(i)).getIDs()));
		}
		return longIntersection(res, res);
	}

	public static ArrayList longIntersection(List list1, List list2) {
		ArrayList list = new ArrayList();
		for (Object t : list1) {
			if (list2.contains(t)) {
				if (!list.contains(t))
					list.add(Long.valueOf((Long) t));
			}
		}
		return list;
	}

	public static ArrayList objectUnion(List list1, List list2) {
		ArrayList list = new ArrayList();
		list.addAll(list2);
		for (Object t : list1) {
			if (!list.contains(t))
				list.add(t);
		}
		return list;
	}

	public static ArrayList getServicesToDelWithDeactivation(Node root,
			String sncode, List contrActivServ, ArrayList parsed) {
		List rootParents = root.getParents();
		ArrayList nodesToDeactivate = new ArrayList();
		for (int i = 0; i < rootParents.size(); i++) {
			Node currNode = ((Node) rootParents.get(i));
			String currsncode = (currNode).getData();
			if (!parsed.contains(currNode)) {
				parsed.add(currNode);
				if (currNode != null && currsncode != null) {
					if ("NOT".equals(currNode.getOp())) {
						continue;
					} else if ("NEEDS".equals(currNode.getOp())
							|| "AND".equals(currNode.getOp())
							|| "OR".equals(currNode.getOp())) {
						if ("0123456789".contains(currsncode.substring(0, 1))) {
							if (contrActivServ.contains(currsncode)) {
								nodesToDeactivate.add(currNode);
								nodesToDeactivate
										.addAll(getServicesToDelWithDeactivation(
												currNode, sncode,
												contrActivServ, parsed));
							}
						} else {
							nodesToDeactivate
									.addAll(getServicesToDelWithDeactivation(
											currNode, sncode, contrActivServ,
											parsed));
						}
					}
				}
			} else {
				continue;
			}

		}
		return nodesToDeactivate;

	}

	public static ArrayList getParentServicesToDelWithActivation(Node root,
			String sncode, List contrActivServ, ArrayList parsed) {
		List rootParents = root.getParents();
		ArrayList nodesToDeactivate = new ArrayList();
		for (int i = 0; i < rootParents.size(); i++) {
			Node currNode = ((Node) rootParents.get(i));
			String currsncode = (currNode).getData();
			if (!parsed.contains(currNode)) {
				parsed.add(currNode);
				if (currNode != null && currsncode != null) {
					if ("NOT".equals(currNode.getOp())) {

						if ("0123456789".contains(currsncode.substring(0, 1))) {
							if (contrActivServ.contains(currsncode)) {
								nodesToDeactivate.add(currNode);
								nodesToDeactivate
										.addAll(getServicesToDelWithDeactivation(
												currNode, sncode,
												contrActivServ, parsed));
							}
						} else {
							nodesToDeactivate
									.addAll(getServicesToDelWithDeactivation(
											currNode, sncode, contrActivServ,
											parsed));
						}
					} else if ("NEEDS".equals(currNode.getOp())
							|| "AND".equals(currNode.getOp())
							|| "OR".equals(currNode.getOp())) {
						if (!"0123456789".contains(currsncode.substring(0, 1))) {
							nodesToDeactivate
									.addAll(getParentServicesToDelWithActivation(
											currNode, sncode, contrActivServ,
											parsed));
						}
						// nodesToDeactivate.addAll(getServicesToDelWithDeactivation(currNode,currsncode,
						// contrActivServ, parsed));
					}
				}
			} else {
				continue;
			}
		}
		return nodesToDeactivate;

	}

	public static ArrayList getChildServicesToDelWithActivation(Node root,
			String sncode, List contrActivServ, ArrayList parsed,
			HashMap[] deps, Long tmcode) {

		List rootChilds = root.getChilds();
		ArrayList nodesToDeactivate = new ArrayList();
		if ("NOT".equals(root.getOp())) {
			HashMap hm = getTreeContractEvaluationNodes(root, sncode,
					contrActivServ, new ArrayList(), new ArrayList(), deps,
					tmcode);
			if (hm.get("EVALUATION") != null && (Boolean) hm.get("EVALUATION"))
				nodesToDeactivate.addAll((List) hm.get("NODES"));
		} else
			for (int i = 0; i < rootChilds.size(); i++) {
				Node currNode = ((Node) rootChilds.get(i));
				String currsncode = (currNode).getData();
				if (!parsed.contains(currNode)) {
					parsed.add(currNode);
					if (currNode != null && currsncode != null) {
						if ("NOT".equals(currNode.getOp())) {
							String nextSncode = currNode.getChilds().get(0)
									.getData();
							if ("0123456789".contains(nextSncode
									.substring(0, 1))) {
								if (contrActivServ.contains(nextSncode)) {
									nodesToDeactivate.add(currNode.getChilds()
											.get(0));
								}
							} else {
								HashMap res = getTreeContractEvaluationNodes(
										currNode.getChilds().get(0), sncode,
										contrActivServ, parsed, parsed, deps,
										tmcode);
								if (res.get("EVALUATION") != null
										&& (Boolean) res.get("EVALUATION"))
									nodesToDeactivate.addAll((List) res
											.get("NODES"));
							}
						} else if ("NEEDS".equals(currNode.getOp())
								|| "AND".equals(currNode.getOp())
								|| "OR".equals(currNode.getOp())) {
							if (!"0123456789".contains(currsncode.substring(0,
									1))) {
								nodesToDeactivate
										.addAll(getChildServicesToDelWithActivation(
												currNode, sncode,
												contrActivServ, parsed, deps,
												tmcode));
							}
						}
					}
				} else {
					continue;
				}
			}
		return nodesToDeactivate;

	}

	public static HashMap getTreeContractEvaluationNodes(Node root,
			String sncode, List contrActivServ, ArrayList parsed,
			ArrayList parsedNot, HashMap[] deps, Long tmcode) {
		List<Node> rootChilds = root.getChilds();
		HashMap res = new HashMap();
		ArrayList nodesToDeactivate = new ArrayList();
		Boolean evaluation = false;

		String currsncode = root.getData();
		if (root != null && rootChilds != null && rootChilds.size() > 0) {
			String nextSncode = rootChilds.get(0).getData();
			if ("NOT".equals(root.getOp())) {
				if ("0123456789".contains(nextSncode.substring(0, 1))) {
					if (!contrActivServ.contains(nextSncode)) {
						evaluation = true;
					} else {
						nodesToDeactivate.add(rootChilds.get(0));
						nodesToDeactivate
								.addAll(getParentServicesToDelWithActivation(
										rootChilds.get(0), sncode,
										contrActivServ, parsedNot));// ---
						evaluation = true;
					}
				} else {

					HashMap hm = getTreeContractEvaluationNodes(
							rootChilds.get(0), sncode, contrActivServ, parsed,
							parsedNot, deps, tmcode);
					if ((Boolean) hm.get("EVALUATION")) {
						evaluation = (Boolean) hm.get("EVALUATION");
						nodesToDeactivate.addAll((List) hm.get("NODES"));
					}
				}
			} else if ("NEEDS".equals(root.getOp())) {
				if ("0123456789".contains(nextSncode.substring(0, 1))) {
					if (contrActivServ.contains(nextSncode)) {
						nodesToDeactivate.add(rootChilds.get(0));
						nodesToDeactivate
								.addAll(getParentServicesToDelWithActivation(
										rootChilds.get(0), sncode,
										contrActivServ, parsedNot));// ---
						evaluation = true;
					}
				} else {
					return getTreeContractEvaluationNodes(rootChilds.get(0),
							sncode, contrActivServ, parsed, parsedNot, deps,
							tmcode);
				}
			} else {
				Boolean eval1 = false;
				Boolean eval2 = false;
				ArrayList nodesToDeactivate1 = new ArrayList();
				ArrayList nodesToDeactivate2 = new ArrayList();
				String nextSncode2 = rootChilds.get(1).getData();
				if ("AND".equals(root.getOp())) {
					if ("0123456789".contains(nextSncode.substring(0, 1))) {
						if (contrActivServ.contains(nextSncode)) {
							nodesToDeactivate1.add(rootChilds.get(0));
							nodesToDeactivate
									.addAll(getParentServicesToDelWithActivation(
											rootChilds.get(0), sncode,
											contrActivServ, parsedNot));// ---
							eval1 = true;
						} else
							eval1 = true;// false;
					} else {
						HashMap hm = getTreeContractEvaluationNodes(
								rootChilds.get(0), sncode, contrActivServ,
								parsed, parsedNot, deps, tmcode);
						nodesToDeactivate1.addAll((List) hm.get("NODES"));
						eval1 = (Boolean) hm.get("EVALUATION");
					}

					if ("0123456789".contains(nextSncode2.substring(0, 1))) {
						if (contrActivServ.contains(nextSncode2)) {
							nodesToDeactivate2.add(rootChilds.get(1));
							nodesToDeactivate
									.addAll(getParentServicesToDelWithActivation(
											rootChilds.get(1), sncode,
											contrActivServ, parsedNot));// ---
							eval2 = true;
						} else
							eval2 = true;// false;
					} else {
						HashMap hm = getTreeContractEvaluationNodes(
								rootChilds.get(1), sncode, contrActivServ,
								parsed, parsedNot, deps, tmcode);
						nodesToDeactivate2.addAll((List) hm.get("NODES"));
						eval2 = (Boolean) hm.get("EVALUATION");
					}
					if (eval1 && eval2) {
						Long priorityEval1 = evaluateList(nodesToDeactivate1,
								deps, tmcode);
						Long priorityEval2 = evaluateList(nodesToDeactivate2,
								deps, tmcode);
						if (priorityEval1 > priorityEval2)
							nodesToDeactivate = nodesToDeactivate1;
						else
							nodesToDeactivate = nodesToDeactivate2;
						// nodesToDeactivate =
						// objectUnion(nodesToDeactivate1,nodesToDeactivate2);
						evaluation = true;
					}

				} else if ("OR".equals(root.getOp())) {
					if ("0123456789".contains(nextSncode.substring(0, 1))) {
						if (contrActivServ.contains(nextSncode)) {
							nodesToDeactivate1.add(rootChilds.get(0));
							nodesToDeactivate
									.addAll(getParentServicesToDelWithActivation(
											rootChilds.get(0), sncode,
											contrActivServ, parsedNot));// ---
							eval1 = true;
						} else
							eval1 = true;// false;
					} else {
						HashMap hm = getTreeContractEvaluationNodes(
								rootChilds.get(0), sncode, contrActivServ,
								parsed, parsedNot, deps, tmcode);
						nodesToDeactivate1.addAll((List) hm.get("NODES"));
						eval1 = (Boolean) hm.get("EVALUATION");
					}

					if ("0123456789".contains(nextSncode2.substring(0, 1))) {
						if (contrActivServ.contains(nextSncode2)) {
							nodesToDeactivate2.add(rootChilds.get(1));
							nodesToDeactivate
									.addAll(getParentServicesToDelWithActivation(
											rootChilds.get(1), sncode,
											contrActivServ, parsedNot));// ---
							eval2 = true;
						} else
							eval2 = true;// false;
					} else {
						HashMap hm = getTreeContractEvaluationNodes(
								rootChilds.get(1), sncode, contrActivServ,
								parsed, parsedNot, deps, tmcode);
						nodesToDeactivate2.addAll((List) hm.get("NODES"));
						eval2 = (Boolean) hm.get("EVALUATION");
					}
					if (eval1 || eval2) {
						if (eval1) {
							nodesToDeactivate = objectUnion(nodesToDeactivate,
									nodesToDeactivate1);
						}
						if (eval2) {
							nodesToDeactivate = objectUnion(nodesToDeactivate,
									nodesToDeactivate2);
						}
						evaluation = true;
					}

				}
			}
		}
		res.put("EVALUATION", evaluation);
		res.put("NODES", nodesToDeactivate);
		return res;
	}

	public static Long evaluateList(List<Node> list, HashMap[] deps, Long tmcode) {
		Long res = new Long(1000);
		List sncodes = getAllSncodes(list);
		for (int i = 0; i < deps.length; i++) {
			if (deps[i].get("TMCODE") != null
					&& !deps[i].get("TMCODE").equals(tmcode))
				continue;
			if (deps[i].get("SNCODE") != null
					&& !sncodes.contains(deps[i].get("SNCODE").toString()))
				continue;
			if ((Long) deps[i].get("PRIORITY") < res) {
				res = (Long) deps[i].get("PRIORITY");
			}
		}
		return res;
	}

	public static ArrayList getAllSncodes(List<Node> list) {
		ArrayList result = new ArrayList();
		for (int i = 0; i < list.size(); i++) {
			String currsncode = ((Node) list.get(i)).getData();
			Node currNode = ((Node) list.get(i));
			if (!result.contains(currNode)) {
				if (currsncode != null
						&& "0123456789".contains(currsncode.substring(0, 1)))
					result.add(currsncode);
			}
		}
		return result;

	}

	public static void removeChilds(Node source) {
		List sourceChilds = source.getChilds();
		deleteChildParent(source);
		source.setChilds(new ArrayList());
	}

	public static void removeComplexNotsFromRoot(Node root, ArrayList parsed,
			Long ns) {
		List rootChilds = root.getChilds();
		Long n = ns;
		for (int i = 0; i < rootChilds.size(); i++) {
			n = removeComplexNotsFromChain((Node) rootChilds.get(i), parsed, n);
			n++;
		}
	}

	public static Long removeComplexNotsFromChain(Node root, ArrayList parsed,
			Long ns) {
		Long n = ns;
		if (root == null || root.getChilds() == null
				|| root.getChilds().size() == 0 || root.getOp() == null
				|| root.getData() == null)
			return n;
		List<Node> rootChilds = root.getChilds();
		String currsncode = root.getData();
		if (!parsed.contains(root)) {
			parsed.add(root);
			if (currsncode != null) {
				if ("NOT".equals(root.getOp())) {
					Node nextNode = rootChilds.get(0);
					String nextSncode = rootChilds.get(0).getData();
					String nextOp = rootChilds.get(0).getOp();
					if (!"0123456789".contains(nextSncode.substring(0, 1))) {
						if ("NOT".equals(nextOp)) {
							root.setOp("NEEDS");
							Node childNode = nextNode.getChilds().get(0);
							removeChilds(nextNode);
							removeChilds(root);
							root.addChild(childNode);
							n = removeComplexNotsFromChain(childNode, parsed,
									ns);
						} else if ("NEEDS".equals(nextOp)) {
							root.setOp("NOT");
							Node childNode = nextNode.getChilds().get(0);
							removeChilds(nextNode);
							removeChilds(root);
							root.addChild(childNode);
							n = removeComplexNotsFromChain(childNode, parsed,
									ns);
						} else if ("AND".equals(nextOp)) {
							root.setOp("NEEDS");
							nextNode.setOp("OR");
							Node childNode1 = nextNode.getChilds().get(0);
							Node childNode2 = nextNode.getChilds().get(1);
							removeChilds(nextNode);

							Node simpleNode1 = new Node("NOT", "N".concat(n
									.toString()), false, root,
									childNode1.getIDs());
							n++;
							Node simpleNode2 = new Node("NOT", "N".concat(n
									.toString()), false, root,
									childNode2.getIDs());

							simpleNode1.addChild(childNode1);
							simpleNode2.addChild(childNode2);

							nextNode.addChild(simpleNode1);
							nextNode.addChild(simpleNode2);
							n++;
							n = removeComplexNotsFromChain(simpleNode1, parsed,
									n);
							n++;
							n = removeComplexNotsFromChain(simpleNode2, parsed,
									n);
						} else if ("OR".equals(nextOp)) {
							root.setOp("NEEDS");
							nextNode.setOp("AND");
							Node childNode1 = nextNode.getChilds().get(0);
							Node childNode2 = nextNode.getChilds().get(1);
							removeChilds(nextNode);

							Node simpleNode1 = new Node("NOT", "N".concat(ns
									.toString()), false, root,
									childNode1.getIDs());
							Node simpleNode2 = new Node("NOT",
									"N".concat(new Long(ns + 1).toString()),
									false, root, childNode2.getIDs());

							simpleNode1.addChild(childNode1);
							simpleNode2.addChild(childNode2);

							nextNode.addChild(simpleNode1);
							nextNode.addChild(simpleNode2);
							n++;
							n = removeComplexNotsFromChain(simpleNode1, parsed,
									n);
							n++;
							n = removeComplexNotsFromChain(simpleNode2, parsed,
									n);
						}
					}
				} else {
					rootChilds = root.getChilds();
					for (int i = 0; i < rootChilds.size(); i++) {
						n = removeComplexNotsFromChain(
								(Node) rootChilds.get(i), parsed, n);
						n++;
					}
				}
			}
		}
		return n;
	}

}
