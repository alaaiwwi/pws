package ma.iam.pws.soi.common.logger;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;

/**
 * The Class LOGGER.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public final class WSLogger {
	
	/** The Constant INFO. */
	private static final int INFO = 0;

	/** The Constant DEBUG. */
	private static final int DEBUG = 1;

	/** The Constant WARN. */
	private static final int WARN = 2;

	/** The Constant ERROR. */
	private static final int ERROR = 3;

	/** The Constant FATAL. */
	private static final int FATAL = 4;

    /**
     * Attribut de type Logger.
     */
    private Logger logger = null;

    /**
     * Constructeur.
     * @param name de type String
     */
    protected WSLogger(String name) {
        logger = LogManager.getLogger(name);
    }

	/**
	 * Loggue un message avec le niveau FATAL.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 * @param t the t
	 */
	public void fatal(Class classe, String methode, String message, Throwable t) {
		logger.fatal(getMessage(FATAL, classe, methode, message), t);
	}

	/**
	 * Loggue un message avec le niveau FATAL.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 */
	public void fatal(Class classe, String methode, String message) {
		logger.fatal(getMessage(FATAL, classe, methode, message));
	}

	/**
	 * Loggue un message avec le niveau ERROR.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 * @param t the t
	 */
	public void error(Class classe, String methode, String message, Throwable t) {
		logger.error(getMessage(ERROR, classe, methode, message), t);
	}

	/**
	 * Loggue un message avec le niveau ERROR.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 */
	public void error(Class classe, String methode, String message) {
		logger.error(getMessage(ERROR, classe, methode, message));
	}
	
	/**
	 * Loggue un message avec le niveau WARN.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 */
	public void warn(Class classe, String methode, String message) {
		logger.warn(getMessage(WARN, classe, methode, message));
	}

	/**
	 * Loggue un message avec le niveau DEBUG.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param info the info
	 */
	public void debug(Class classe, String methode, String info) {
		logger.debug(getMessage(DEBUG, classe, methode, info));
	}

	/**
	 * Loggue un message avec le niveau INFO.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 */
	public void info(Class classe, String methode, String message) {
		logger.info(getMessage(INFO, classe, methode, message));
	}

	/**
	 * Construit un message LOG.
	 * 
	 * @param level the level
	 * @param classe the classe
	 * @param methode the methode
	 * @param message the message
	 * @return message à loguer
	 */
	private String getMessage(int level, Class classe, String methode, String message) {
		StringBuffer msg = new StringBuffer();
		switch (level) {
		case INFO:
			msg.append("[ Info ] ");
			break;
		case DEBUG:
			msg.append("[ Debug ] ");
			break;
		case WARN:
			msg.append("[ Warning ] ");
			break;
		case ERROR:
			msg.append("[ Error ] ");
			break;
		case FATAL:
			msg.append("[ Fatal ] ");
			break;
		default:
			break;
		}
		msg.append(message);
		return msg.toString();
	}
	
}
