package ma.iam.pws.soi.beans.services;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ConfServicePackagePub")
@XmlType(name = "ConfServicePackagePub", propOrder = { "sncode", "sp_list" })
public class ConfServicePackagePub {

	private long sncode;
	
	@XmlTransient
	private String sncodepub;
	
	private List<SPackage> sp_list;

	/**
	 * @return the sp_list
	 */
	public List<SPackage> getSp_list() {
		return sp_list;
	}

	/**
	 * @param sp_list
	 *            the sp_list to set
	 */
	public void setSp_list(List<SPackage> sp_list) {
		this.sp_list = sp_list;
	}

	/**
	 * @return the sncodepub
	 */
	public String getSncodepub() {
		return sncodepub;
	}

	/**
	 * @param sncodepub
	 *            the sncodepub to set
	 */
	public void setSncodepub(String sncodepub) {
		this.sncodepub = sncodepub;
	}

	/**
	 * @return the sncode
	 */
	public long getSncode() {
		return sncode;
	}

	/**
	 * @param sncode
	 *            the sncode to set
	 */
	public void setSncode(long sncode) {
		this.sncode = sncode;
	}

}
