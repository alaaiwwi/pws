package ma.iam.pws.soi.tree;

/**
 * @author Alaa ESSA
 *
 */


import java.util.ArrayList;

public class Tree {
	   private static Tree instance = null;
	   private static Node root = null;
	   private static ArrayList srvPriority = null;
	   private static ArrayList srvDeps = null;
	   protected Tree() {
	      root = new Node();
	      srvPriority = new ArrayList();
	      srvDeps = new ArrayList();
	   }
	  
	public static Tree getTree() {
	      if(instance == null) {
	    	  instance = new Tree();
	      }
	      return instance;
	   }
	
	 public static Node getRoot() {
			return root;
		}
	 public static ArrayList getSRVPriority() {
			return srvPriority;
		}
	 public static ArrayList getSRVDeps() {
			return srvDeps;
		}
	}	
