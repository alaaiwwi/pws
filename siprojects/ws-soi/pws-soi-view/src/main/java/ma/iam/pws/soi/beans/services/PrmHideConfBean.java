package ma.iam.pws.soi.beans.services;

public class PrmHideConfBean {
	private String value_des;
	private String value;
	private Long tmcode;
	private Long sncode;
	private String show_conf;
	private Long prm_id;
        //>******* Update, A.ESSA DI6717
	private String prgcode;
	
	public String getValue_des() {
		return value_des;
	}
	public String getPrgcode() {
		return prgcode;
	}
	public void setPrgcode(String prgcode) {
		this.prgcode = prgcode;
	}
        //<******* Update, A.ESSA DI6717
	public void setValue_des(String value_des) {
		this.value_des = value_des;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Long getTmcode() {
		return tmcode;
	}
	public void setTmcode(Long tmcode) {
		this.tmcode = tmcode;
	}
	public Long getSncode() {
		return sncode;
	}
	public void setSncode(Long sncode) {
		this.sncode = sncode;
	}
	public String getShow_conf() {
		return show_conf;
	}
	public void setShow_conf(String show_conf) {
		this.show_conf = show_conf;
	}
	public Long getPrm_id() {
		return prm_id;
	}
	public void setPrm_id(Long prm_id) {
		this.prm_id = prm_id;
	}
	
	
}
