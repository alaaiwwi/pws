package ma.iam.pws.soi.engine.common;

import java.io.Serializable;

public class CugElement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3521927051040081168L;
	
	private Long cugId = null;
	
	private Long[] bsgId = null;
	
	private Parameter[] parameters = null;

	public Long getCugId() {
		return cugId;
	}

	public void setCugId(Long cugId) {
		this.cugId = cugId;
	}

	public Long[] getBsgId() {
		return bsgId;
	}

	public void setBsgId(Long[] bsgId) {
		this.bsgId = bsgId;
	}

	public Parameter[] getParameters() {
		return parameters;
	}

	public void setParameters(Parameter[] parameters) {
		this.parameters = parameters;
	}
	
	

}
