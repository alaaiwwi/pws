package ma.iam.pws.soi.domaine;


import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.engine.common.exception.FunctionalException;
import ma.iam.pws.soi.engine.common.exception.TechnicalException;

public interface CompteFacade {

	
	UserBean getCompte(String username , String password) throws TechnicalException, FunctionalException;

}
