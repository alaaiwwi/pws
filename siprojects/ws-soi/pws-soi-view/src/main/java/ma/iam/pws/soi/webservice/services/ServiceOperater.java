package ma.iam.pws.soi.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.errors.FaultWS;



/**
 * @author mabchour
 *
 */

@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public interface ServiceOperater {
	
	/**
	 * ReadServices . : WS user to identify activated or not service for contract
	 * 
	 * @param coId internal identifier of contract in billing system
	 * @param CO_CODE public identifier of contract in billing system
	 */
	//>******* Created, A.ESSA DI5440
	@WebMethod
	Bcontract ReadServices( @javax.jws.WebParam(name="USERNAME")String username, @javax.jws.WebParam(name="CO_ID") long co_id) throws FaultWS  ;
	
	/**
	 * byPAss check if the communication its ok/ or not send a fake compte and chack the return if the compte is not connected
	 * @return
	 */
	@WebMethod(operationName="byPass")
	@WebResult(name="result")
	Integer modeByPass();
}
