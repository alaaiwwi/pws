package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
@XmlType(name="Contract" , propOrder = { "CO_ID", "CO_ID_PUB", "RPCODE", "ActivateServices" })
public class BAddServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<bNewServicesEx> ActivateServices= new ArrayList<bNewServicesEx>();
	private long CO_ID;
	private String CO_ID_PUB;
	private long RPCODE;

	
	
	public long getRPCODE() {
		return RPCODE;
	}

	public void setRPCODE(long rPCODE) {
		RPCODE = rPCODE;
	}
	

	public List<bNewServicesEx> getActivateServices() {
		return ActivateServices;
	}

	public void setActivateServices(bNewServicesEx servicesEx) {
		ActivateServices = (List<bNewServicesEx>) servicesEx;
	}

	public long getCO_ID() {
		return CO_ID;
	}

	public void setCO_ID(long cO_ID) {
		CO_ID = cO_ID;
	}

	public String getCO_ID_PUB() {
		return CO_ID_PUB;
	}

	public void setCO_ID_PUB(String cO_ID_PUB) {
		CO_ID_PUB = cO_ID_PUB;
	}

}
