/**
 * 
 */
package ma.iam.pws.soi.webservice.plaforme.impl;

import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.plateforme.ResultBean;
import ma.iam.pws.soi.webservice.plaforme.CheckAvailability;

/**
 * @author mounir_mabchour
 *
 */
//@javax.jws.WebService(endpointInterface = "ma.iam.ws.webservice.plaforme.CheckAvailability",serviceName = "CheckAvailability"  )
public class CheckAvailabilityImpl implements CheckAvailability {

	//@Override
	public ResultBean CheckAvailability() throws FaultWS {
		// TODO Auto-generated method stub
		ResultBean rb = new ResultBean();
		rb.setCodeRetour("OK");
		rb.setDescRetour("PlateForme available");
		return rb;
	}

}
