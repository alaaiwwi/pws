/**
 * 
 */
package ma.iam.pws.soi.webservice.security;



import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.beans.errors.FaultWS;




/**
 * @author mabchour
 *
 */

@javax.jws.WebService
public interface AuthenticationWService {

	/**
	 * Login.
	 * 
	 * @param login the login
	 * @param motDePasse the mot de passe
	 * @param messageContext the message context
	 * @return the string
	 */
	@javax.jws.WebMethod
	UserBean Authentify( @javax.jws.WebParam(name="login") String login,  @javax.jws.WebParam(name="password") String motDePasse) throws FaultWS;
}