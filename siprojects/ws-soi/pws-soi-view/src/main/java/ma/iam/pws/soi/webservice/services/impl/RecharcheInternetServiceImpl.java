package ma.iam.pws.soi.webservice.services.impl;

/**
 * @author Alaa ESSA
 *
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapter;
import ma.iam.pws.soi.adapter.BContractedServicesAdapter;
import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.services.RecharcheInternetService;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

/**
 * dont used
 * 
 * @author a596968
 *
 */

@javax.jws.WebService(targetNamespace = "http://services.webservice.ws.iam.ma/")
@SuppressWarnings({ "unchecked", "deprecation", "static-access", "rawtypes" })
public class RecharcheInternetServiceImpl implements RecharcheInternetService {

	Contract contract = new Contract();
	ServiceUtils service = new ServiceUtils();

	private static final Logger logger = LogManager
			.getLogger(RecharcheInternetServiceImpl.class.getName());
	public static final String NUMBER_REGEX = "^[0-9]*$";

	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private UserCache userCache = new NullUserCache();

	/** Sets the users cache. Not required, but can benefit performance. */
	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	private String cilName;
	private String cilVersion;
	private String securityName;
	private String securityVersion;
	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	public final String getCilName() {
		return cilName;
	}

	public final void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public final String getCilVersion() {
		return cilVersion;
	}

	public final void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public final String getSecurityName() {
		return securityName;
	}

	public final void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public final String getSecurityVersion() {
		return securityVersion;
	}

	public final void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	public long getEligibility(String username, String msisdn) throws FaultWS {
		MessageContext msgContext = wsContext.getMessageContext();

		UserBean myUser = new UserBean();
		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		logger.info("Utilisateur : " + wsContext.getUserPrincipal().getName()
				+ " try to connect.");

		// SOIService soiComm =
		// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}

		// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);

		if (msisdn == null || !msisdn.startsWith("2126")
				|| msisdn.length() != 12) {
			throw new FaultWS(
					ma.iam.pws.soi.common.Constants.ERROR_CODE.MT_WS_MSISDN_ERROR,
					msgProperties);
		}

		long res = 0;
		try {

			HashMap<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("USE_INSTALLATION_ADDRESS", Boolean.FALSE);
			inParams.put("DIRNUM", msisdn);

			inParams.put("SEARCHER", "ContractSearchWithoutHistory");

			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACTS_SEARCH, inParams);

			HashMap[] contracts = (HashMap[]) outParams.get("contracts");
			if ((contracts == null) || (contracts.length == 0)
					|| contracts[0].get("CO_ID") == null) {
				res = 1;
				return res;
			} else {
				Long co_id = null;
				int nContract = 0;
				boolean notFound = false;
				Long deactiveCo_id = null;
				while ((co_id == null) && (nContract < contracts.length)) {
					Integer coStatus = (Integer) contracts[nContract]
							.get("CO_STATUS");
					if (coStatus.toString().equals(
							SoiUtils.SERVICE_STATUT_ACTIVE)) {
						co_id = (Long) contracts[nContract].get("CO_ID");

						if (co_id == null) {
							notFound = true; // throw new
												// FaultWS(Constants.ERROR_CODE.MT_CONTRACTNOTFOUND,msgProperties);
						}
					} else {
						deactiveCo_id = (Long) contracts[nContract]
								.get("CO_ID");
						nContract += 1;
					}
				}

				if (deactiveCo_id != null) {
					res = 2;
					return res;
				}

				if (notFound) {
					res = 1;
					return res;
				}

				inParams = new HashMap<String, Object>();
				inParams.put("CO_ID", co_id.longValue());
				inParams.put("SYNC_WITH_DB", true);

				outParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ,
						inParams);
				Bcontract bcontract = BContractAdapter
						.getMainContractInfo(outParams);

				Integer coStatus = (Integer) outParams.get("CO_STATUS");

				inParams.remove("SYNC_WITH_DB");
				outParams = soiComm.executeCommand(
						SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
				BContractedServicesAdapter.buildContractedServicesBean(
						outParams, bcontract);

				Long tmcode = bcontract.getRPCODE();
				List contrActivServ = ServiceUtils
						.getContrActivServShdes(bcontract);
				if (",20,30,35,44,37,40,8,34,".contains(",".concat(
						tmcode.toString()).concat(","))) {
					if (contrActivServ.contains("INT3G")
							&& CheckServiceWithParameters("INT3G", bcontract,
									soiComm)) {
						HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
						inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
						HashMap<String, Object> outParamsCustomer = soiComm
								.executeCommand(SoiUtils.CMD_CUSTOMER_READ,
										inParamsCustomer);
						String prgCode = outParamsCustomer.get("PRG_CODE")
								.toString();
						if ("12,6,7,9,4,3,8,20".contains(",".concat(prgCode)
								.concat(","))) {
							if (contrActivServ.contains("RFI3G")) {
								return res;
							} else {
								res = 5;
								return res;
							}
						} else {
							return res;
						}
					} else {
						res = 4;
						return res;
					}
				} else if (",28,29,41,".contains(",".concat(tmcode.toString())
						.concat(","))) {
					if (contrActivServ.contains("F1G05")) {
						HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
						inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
						HashMap<String, Object> outParamsCustomer = soiComm
								.executeCommand(SoiUtils.CMD_CUSTOMER_READ,
										inParamsCustomer);
						String prgCode = outParamsCustomer.get("PRG_CODE")
								.toString();
						if ("12,6,7,9,4,3,8,20".contains(",".concat(prgCode)
								.concat(","))) {
							if (contrActivServ.contains("RFI3G")) {
								return res;
							} else {
								res = 5;
								return res;
							}
						} else {
							return res;
						}
					} else {
						res = 4;
						return res;
					}
				} else {
					res = 3;
					return res;
				}

			}
		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(),
						cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}

	}

	// @Override
	public long rechargeInternet(String username, String msisdn, String dateRech)
			throws FaultWS {

		MessageContext msgContext = wsContext.getMessageContext();

		UserBean myUser = new UserBean();
		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		logger.info("Utilisateur : " + wsContext.getUserPrincipal().getName()
				+ " try to connect.");

		// SOIService soiComm =
		// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}

		// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);

		if (msisdn == null || !msisdn.startsWith("2126")
				|| msisdn.length() != 12) {
			throw new FaultWS(
					ma.iam.pws.soi.common.Constants.ERROR_CODE.MT_WS_MSISDN_ERROR,
					msgProperties);
		}

		if (dateRech.length() > 15) {
			throw new FaultWS(Constants.ERROR_CODE.MT_DATE_FORMAT,
					msgProperties);
		}
		DateFormat df = new SimpleDateFormat("ddMMyy HH:mm:ss");
		Date date = null;
		try {
			date = df.parse(dateRech);
		} catch (ParseException e1) {
			throw new FaultWS(Constants.ERROR_CODE.MT_DATE_FORMAT,
					msgProperties);
		}

		long res = 0;
		try {

			HashMap<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("USE_INSTALLATION_ADDRESS", Boolean.FALSE);
			inParams.put("DIRNUM", msisdn);

			inParams.put("SEARCHER", "ContractSearchWithoutHistory");

			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACTS_SEARCH, inParams);

			HashMap[] contracts = (HashMap[]) outParams.get("contracts");
			if ((contracts == null) || (contracts.length == 0)
					|| contracts[0].get("CO_ID") == null) {
				res = 1;
				return res;
			} else {
				Long co_id = null;
				int nContract = 0;
				boolean notFound = false;
				Long deactiveCo_id = null;
				while ((co_id == null) && (nContract < contracts.length)) {
					Integer coStatus = (Integer) contracts[nContract]
							.get("CO_STATUS");
					if (coStatus.toString().equals(
							SoiUtils.SERVICE_STATUT_ACTIVE)) {
						co_id = (Long) contracts[nContract].get("CO_ID");

						if (co_id == null) {
							notFound = true; // throw new
												// FaultWS(Constants.ERROR_CODE.MT_CONTRACTNOTFOUND,msgProperties);
						}
					} else {
						deactiveCo_id = (Long) contracts[nContract]
								.get("CO_ID");
						nContract += 1;
					}
				}

				if (deactiveCo_id != null) {
					res = 2;
					return res;
				}

				if (notFound) {
					res = 1;
					return res;
				}

				inParams = new HashMap<String, Object>();
				inParams.put("ACTION_CODE", 'I');
				inParams.put("FEE_CLASS", new Integer(4));
				inParams.put("RPCODE",
						(Long) contracts[nContract].get("RPCODE"));
				inParams.put("SPCODE_PUB", "SPVAS");
				inParams.put("SNCODE_PUB", "REFI2");
				inParams.put("NUM_PERIODS", new Integer(1));
				inParams.put("FEE_TYPE", "N");
				inParams.put("CO_ID", (Long) contracts[nContract].get("CO_ID"));
				// inParams.put("VALID_FROM", date);

				outParams = soiComm.executeCommand(
						SoiUtils.CMD_BOOKING_REQUEST_WRITE, inParams);

				inParams = new HashMap<String, Object>();
				inParams.put("TICK_CODE", "SYSTEM");
				inParams.put("TICK_SHDES", "FORINT2GO");
				inParams.put("TICK_LDES",
						"Offre Forfait Internet 2 Go via SELFCARE MOBILE");
				inParams.put("CO_ID", (Long) contracts[nContract].get("CO_ID"));
				inParams.put("CS_ID", (Long) contracts[nContract].get("CS_ID"));

				outParams = soiComm.executeCommand(SoiUtils.CMD_TICKLER_NEW,
						inParams);

				soiComm.cmsClient.commit();

				inParams = new HashMap<String, Object>();
				inParams.put("TICK_NUMBER", (Long) outParams.get("TICK_NUMBER"));
				inParams.put("TICK_XCOORD", msisdn);
				inParams.put("TICK_YCOORD", dateRech);
				outParams = soiComm.executeCommand(SoiUtils.CMD_TICKLER_WRITE,
						inParams);

				inParams = new HashMap<String, Object>();
				inParams.put("DIR_NUM", msisdn);
				inParams.put("SMS_TEXT",
						"Le forfait Internet que vous avez demandé a été activé avec succès.");
				inParams.put("MODULE", "MOB");
				outParams = soiComm.executeCommand(SoiUtils.CMD_SEND_SMS,
						inParams);

				soiComm.cmsClient.commit();

			}
		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(),
						cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}

		return res;

	}

	public List getContrActivSuspendedServ(Bcontract bcontract)
			throws CMSException, FaultWS {

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		List contrActivServ = new ArrayList();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
					|| serv.getCOS_STATUS().toString()
							.equals(SoiUtils.SERVICE_STATUT_SUSPENDED)) {
				contrActivServ.add(Long.valueOf(serv.getSNCODE()).toString());
			}

		}
		return contrActivServ;
	}

	public boolean CheckServiceWithParameters(String snShdes,
			Bcontract bcontract, SOIService soiComm) throws CMSException,
			FaultWS {
		logger.debug("=> CheckServiceWithParameters()");

		boolean isServiceWithParameters = false;
		HashMap serviceParametersReadOutParams;

		HashMap inParams = new HashMap();
		String snCodeParamName = "SNCODE_PUB";
		logger.debug("SNCODE=" + snShdes);
		inParams.put(snCodeParamName, snShdes);
		inParams.put("CO_ID", bcontract.getCO_ID());
		inParams.put("PROFILE_ID", new Long(0));

		serviceParametersReadOutParams = soiComm.executeCommand(
				"CONTRACT_SERVICE_PARAMETERS.READ", inParams);// PRM_DES=Débit
																// Mb/S

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_PARAMS");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			for (int i = 0; i < hmNumServ.length; i++) {
				if (hmNumServ[i].get("PRM_DES").toString().contains("Débit")) {
					HashMap[] multValueInd = (HashMap[]) hmNumServ[i]
							.get("MULT_VALUES");
					if (multValueInd[0].get("VALUE").toString().contains("3.6")) {
						isServiceWithParameters = true;
					}
					break;
				}
			}
		}

		logger.debug("<= CheckServiceWithParameters() : "
				+ isServiceWithParameters);
		return isServiceWithParameters;
	}

}
