package ma.iam.pws.soi.webservice.services.impl;

/**
 * @author Alaa ESSA
 *
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.xml.ws.WebServiceContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.beans.contract.bNewParamValue;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValues;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.services.ChangeParamService;
import ma.iam.security.SecuredObject;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

@SecuredObject
@javax.jws.WebService(targetNamespace = "http://services.webservice.ws.iam.ma/")
public class ChangeParamServiceImpl implements ChangeParamService {

	private static final Logger logger = LogManager.getLogger(ServiceOperaterImpl.class.getName());
	CheckDependencyImpl CheckDependencyImpl = new CheckDependencyImpl();
	Contract contract = new Contract();
	ServiceUtils service = new ServiceUtils();
	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	private String securityName;
	private String securityVersion;

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	private UserCache userCache = new NullUserCache();

	private String cilName;
	private String cilVersion;

	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	@RolesAllowed("ROLE_WRITE")
	public List<bNewServicesEx> ChangeParamServiceImpl(String username, long co_id, List<bNewServices> Services)
			throws FaultWS {
		logger.info(getClass().toString() + " Activate Service : " + Services + " Contract : " + co_id);

		UserDetails user = userCache.getUserFromCache(wsContext.getUserPrincipal().getName());
		// wsContext.getMessageContext().
		// SOIService soiComm =
		// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username, encryptedPass);
				else {
					throw new FaultWS(Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO, msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO, msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password, msgProperties, securityName, securityVersion,
					cilName, cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED, msgProperties);
		}

		// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED, msgProperties);

		try {
			List<bNewServicesEx> ServicesEx = new ArrayList<bNewServicesEx>();
			// SOIService soiComm = SOIService.getInstance("CIL", "2_IAM_1");
			boolean result = false;
			if (co_id == 0 || Services == null || Services.size() == 0) {
				logger.error(getClass().toString()
						+ "WS ChangeParamService : Erreur Input value Parameters CO_ID | Services");
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_INPUT_MISSING, msgProperties);
			}

			HashMap<String, Object> inParams = new HashMap<String, Object>();
			HashMap<String, Object> contractReadOutParams = new HashMap<String, Object>();
			inParams.put("CO_ID", new Long(co_id).longValue());
			inParams.put("SYNC_WITH_DB", true);
			contractReadOutParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParams);
			inParams.remove("SYNC_WITH_DB");
			contract.setCoId(new Long(co_id).longValue());
			Long rpCode = (Long) contractReadOutParams.get("RPCODE");
			contract.setRpCode(rpCode);

			for (Iterator it = Services.iterator(); it.hasNext();) {

				bNewServices Srv = new bNewServices();
				bNewServicesEx SrvEx = new bNewServicesEx();
				Srv = (bNewServices) it.next();
				Long spCode = Srv.getSPCODE();
				Long snCode = Srv.getSNCODE();
				service.setSnCode(snCode);

				try {
					// >******* Update, A.ESSA DI6717
					// Checking if the user has access to this service

					if (SoiUtils.is_service_clcode(rpCode, snCode, spCode.longValue(), 7)
							|| SoiUtils.is_service_clcode(rpCode, snCode, spCode.longValue(), 8)
							|| SoiUtils.is_service_clcode(rpCode, snCode, spCode.longValue(), 9)) {
						throw new FaultWS(Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATHORIZED, msgProperties);
					}
					// <******* Update, A.ESSA DI6717

					if (ServiceUtils.isAllowedToBeModified(snCode, soiComm)) {

						if (ServiceUtils.isProhibitedToBeModified(Srv, soiComm)) {
							throw new FaultWS(Srv, Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_NOTALLOWED, msgProperties);
						}

						// >******* Updated, A.ESSA FC5197
						HashMap hmService = ServiceUtils.checkIfServiceBelongToContract(service, contract,
								contractReadOutParams, soiComm);
						// if
						// (ServiceUtils.checkIfServiceBelongToContract(service,
						// contract,contractReadOutParams,soiComm)) {
						if (hmService != null) {
							// <******* Updated, A.ESSA FC5197

							// Extend All Input Param
							SrvEx = getServiceParamValueExtends(SrvEx, Srv);

							HashMap<String, Object> outParamsCtrSrv = new HashMap<String, Object>();
							inParams.put("CO_ID", new Long(co_id).longValue());
							SrvEx.setCO_ID(co_id);
							SrvEx.setRPCODE(rpCode);
							service.setSnCode(new Long(Srv.getSNCODE()).longValue());
							service.setSpCode(new Long(Srv.getSPCODE()).longValue());

							// >******* Update, A.ESSA DI6717
							HashMap<String, List<String>> eligSrvPrm = SoiUtils.getFilteredPrmValues(soiComm, co_id,
									snCode, msgProperties, null, null, Services);

							for (Iterator ItPrm = SrvEx.getBNewParamValueEx().iterator(); ItPrm.hasNext();) {
								bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();

								for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
									bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
									if (vaLue.getVALUE_DES() != null) {
										if (!eligSrvPrm.get((new Long(pRmVal.getPRM_ID())).toString())
												.contains(vaLue.getVALUE_DES())) {
											throw new FaultWS(Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
													msgProperties);
										}

									}
								}
							}
							// <******* Update, A.ESSA DI6717

							if (ServiceUtils.CheckServiceWithParameters(service, contract, null, soiComm, msgProperties,
									SrvEx, SoiUtils.SERVICE_STATUT_ACTIVE, Services)) {
								// modify the Service Parameters
								ServiceUtils.modifyServiceParameters(service, false, co_id, soiComm, SrvEx);
								result = true;
							} else {
								throw new FaultWS(SrvEx, Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_INVALID_CFG,
										msgProperties);
							}
							if (result) {
								ServicesEx.add(SrvEx);
								soiComm.cmsClient.commit();
							}
						} else {
							throw new FaultWS(Srv, Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_UNATTACHED_CTR,
									msgProperties);
						}
					} else {
						throw new FaultWS(Srv, Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_NOTALLOWED, msgProperties);
					}
				} catch (CMSException cex) {
					// >******* Updated, A.ESSA QC12126
					if (soiComm != null) {
						soiComm.cmsClient.rollback();
					}
					// <******* Updated, A.ESSA QC12126
					if (cex.getErrors() != null && cex.getErrors().length > 0) {
						logger.error(
								getClass().toString() + " : CMSException " + cex.getErrors()[0].getAdditionalInfo());
						throw new FaultWS(cex.getErrors()[0].getErrorCode(), cex.getErrors()[0].getAdditionalInfo());
					} else {
						if (cex.getMessage().contains("CMS client not connected")) {

						} else {
							logger.error(getClass().toString() + " : CMSException " + cex.getMessage());
							throw new FaultWS("RC1000", cex.getMessage());
						}
					}
				} catch (Exception e) {
					// >******* Updated, A.ESSA QC12126
					if (soiComm != null) {
						soiComm.cmsClient.rollback();
					}
					// <******* Updated, A.ESSA QC12126
					if (e instanceof FaultWS)
						throw (FaultWS) e;
					logger.error(getClass().toString() + " : Exception " + e.getMessage());
					e.printStackTrace();
					throw new FaultWS("RC1000", "Internal Error");
				}
			}

			ServiceUtils.remove_Auto_params(soiComm, ServicesEx);
			return ServicesEx;

		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException " + cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(), cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED, msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException " + cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR, msgProperties);
		}
	}

	private bNewServicesEx getServiceParamValueExtends(bNewServicesEx srvEx, bNewServices srv) throws FaultWS {
		try {
			List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
			for (Iterator ItPrm = srv.getBNewParamValue().iterator(); ItPrm.hasNext();) {
				bNewParamValue bNewParamValue = (bNewParamValue) ItPrm.next();
				bNewParamValueEx pRm = new bNewParamValueEx();
				pRm.setPRM_ID(bNewParamValue.getPRM_ID());
				List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();
				for (Iterator ItValt = bNewParamValue.getbNewValues().iterator(); ItValt.hasNext();) {
					bNewValues bNewValues = (bNewValues) ItValt.next();
					bNewValuesEx value = new bNewValuesEx();
					value.setVALUE_DES(bNewValues.getVALUE_DES());
					BNewValuesEx.add(value);
				}
				pRm.setBNewValueEx(BNewValuesEx);
				BNewParamValueEx.add(pRm);

			}
			srvEx.setSNCODE(srv.getSNCODE());
			srvEx.setSPCODE(srv.getSPCODE());
			srvEx.setBNewParamValueEx(BNewParamValueEx);

		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR, msgProperties);
		}
		return srvEx;
	}

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage().contains("CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception " + e.getMessage());

			return rb;
		}

	}
}
