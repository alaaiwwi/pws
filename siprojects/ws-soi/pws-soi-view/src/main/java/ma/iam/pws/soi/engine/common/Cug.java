package ma.iam.pws.soi.engine.common;

import java.io.Serializable;

public class Cug implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6180347243243600311L;

	private CugElement[] cugElements;
	
	private Bsg[] bsg;

	public CugElement[] getCugElements() {
		return cugElements;
	}

	public void setCugElements(CugElement[] cugElements) {
		this.cugElements = cugElements;
	}

	public Bsg[] getBsg() {
		return bsg;
	}

	public void setBsg(Bsg[] bsg) {
		this.bsg = bsg;
	}
	
	

}
