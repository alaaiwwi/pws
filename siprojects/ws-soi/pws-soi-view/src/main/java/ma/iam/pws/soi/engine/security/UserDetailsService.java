package ma.iam.pws.soi.engine.security;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.domaine.CompteFacade;
import ma.iam.pws.soi.engine.common.exception.FunctionalException;
import ma.iam.pws.soi.engine.common.exception.TechnicalException;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsService implements
		org.springframework.security.core.userdetails.UserDetailsService {
	//
	private CompteFacade compteFacade = null;

	public CompteFacade getCompteFacade() {
		return compteFacade;
	}

	public void setCompteFacade(CompteFacade compteFacade) {
		this.compteFacade = compteFacade;
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		UserDetails user = null;

		try {
			
			UserBean compte = compteFacade.getCompte(username, "");
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			//new GrantedAuthorityImpl[0];
			user = new User(username, compte.getPassword(), true, true, true,
					true, authorities);
		} catch (FunctionalException e) {
			throw new UsernameNotFoundException(e.getMessage());
		} catch (TechnicalException e) {
			throw new DataRetrievalFailureException(e.getMessage());
		}
		return user;
	}
}
