package ma.iam.pws.soi.webservice.security.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BuserAdapter;
import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.security.AuthenticationWService;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;

@javax.jws.WebService(endpointInterface = "ma.iam.ws.webservice.security.AuthenticationWService", serviceName = "authenticationService")
public class AuthenticationWServiceImpl implements AuthenticationWService {

	private static final Logger logger = LogManager
			.getLogger(AuthenticationWServiceImpl.class.getName());

	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private UserCache userCache = new NullUserCache();

	/** Sets the users cache. Not required, but can benefit performance. */
	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	private String cilName;
	private String cilVersion;
	private String securityName;
	private String securityVersion;
	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	public final String getCilName() {
		return cilName;
	}

	public final void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public final String getCilVersion() {
		return cilVersion;
	}

	public final void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public final String getSecurityName() {
		return securityName;
	}

	public final void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public final String getSecurityVersion() {
		return securityVersion;
	}

	public final void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	@WebMethod
	public UserBean Authentify(String login, String password) throws FaultWS {
		MessageContext msgContext = wsContext.getMessageContext();

		logger.info("Utilisateur : " + login + " try to connect.");
		UserBean myUser = new UserBean();

		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("NAME", login);
		if ((login == null || "".equals(login))
				|| (password == null || "".equals(password)))
			throw new FaultWS(Constants.ERROR_CODE.MT_WS_AUTHEN_INPUT_MISSING,
					msgProperties);
		try {
			// Authenticate valid connection
			SOIService soiSecurity = new SOIService(securityName,
					securityVersion);

			soiSecurity.login(login, password);

			// Get info users
			logger.info(getClass().toString() + " :  Utilisateur : " + login
					+ " successfully connected .");
			HashMap<String, Object> outParams = soiSecurity.executeCommand(
					SoiUtils.CMD_USERS_READ, inParams);

			BuserAdapter.retrieveInfoUser(outParams, myUser);

			// Expiration Date

			if (myUser.getPassword_expires().compareTo(
					Calendar.getInstance().getTime()) == -1) {
				soiSecurity.cmsClient.disconnect();
				// SOIService.reset();
				SecurityContextHolder.clearContext();
				userCache.removeUserFromCache(login);
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_AUTHEN_PASSWD_EXPIRE,
						msgProperties);
			}

			// Get users rights
			HashMap<String, Object> inParamsRight = SoiUtils
					.getUserRightParameters(login, password);
			HashMap outParams_right = soiSecurity.executeCommand(
					SoiUtils.CMD_USER_RIGHT_CHECK, inParamsRight);

			List listProfiles = soiSecurity
					.convertListProfile((HashMap<String, Object>[]) outParams_right
							.get("USER_PERMISSIONS"));

			myUser.setRights(listProfiles);

			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			// new GrantedAuthorityImpl[0];
			myUser.setPassword(password);

			UserDetails user = new User(myUser.getName(), myUser.getPassword(),
					true, true, true, true, authorities);

			UsernamePasswordAuthenticationToken resulttoken = new UsernamePasswordAuthenticationToken(
					user, myUser.getPassword(), user.getAuthorities());

			SecurityContextHolder.getContext().setAuthentication(resulttoken);
			// soiSecurity.cmsClient.disconnect();
			// soiSecurity.reset();
			logger.info(getClass().toString()
					+ " : Change  Soi name Server to CIL ");
			// soiComm = new SOIService ("CIL","2");
			SOIService soiComm = new SOIService(cilName, cilVersion);
			soiComm.login(login, password);
			// Cache Offer
			SoiUtils.initCache(soiComm, login);
			// MessageContext mc = MessageContext.getCurrentMessageContext();
			soiContext.getContexts().put("SECURITY_" + login,
					(SOIService) soiSecurity);
			soiContext.getContexts().put("CIL_" + login, (SOIService) soiComm);

		} catch (CMSException cex) {

			if (!cex.getMessage().contains(
					"This CMS client is already connected")) {
				soiContext.getContexts().remove("SECURITY_" + login);
				soiContext.getContexts().remove("CIL_" + login);
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getMessage());
				SecurityContextHolder.clearContext();
				userCache.removeUserFromCache(login);
				myUser = null;
				if (cex.getErrors() != null && cex.getErrors().length > 0) {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getErrors()[0].getAdditionalInfo());
					throw new FaultWS(cex.getErrors()[0].getErrorCode(),
							cex.getErrors()[0].getAdditionalInfo());
				} else {
					if (cex.getMessage().contains("CMS client not connected")) {
						throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
								msgProperties);
					} else {
						logger.error(getClass().toString() + " : CMSException "
								+ cex.getMessage());
						throw new FaultWS("RC1000", cex.getMessage());
					}
				}

			} else {
				SOIService soiSecurity = SoiUtils.getSecuritySOIServiceByUser(
						wsContext, soiContext);
				try {
					HashMap<String, Object> outParams = soiSecurity
							.executeCommand(SoiUtils.CMD_USERS_READ, inParams);
					BuserAdapter.retrieveInfoUser(outParams, myUser);
				} catch (CMSException cex2) {
					if (cex.getErrors() != null && cex.getErrors().length > 0) {
						logger.error(getClass().toString() + " : CMSException "
								+ cex.getErrors()[0].getAdditionalInfo());
						throw new FaultWS(cex.getErrors()[0].getErrorCode(),
								cex.getErrors()[0].getAdditionalInfo());
					} else {
						if (cex.getMessage().contains(
								"CMS client not connected")) {
							throw new FaultWS(
									Constants.ERROR_CODE.MT_USERUNLOGGED,
									msgProperties);
						} else {
							logger.error(getClass().toString()
									+ " : CMSException " + cex.getMessage());
							throw new FaultWS("RC1000", cex.getMessage());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getLocalizedMessage());
			e.printStackTrace();
			SecurityContextHolder.clearContext();
			userCache.removeUserFromCache(login);
			myUser = null;
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
					msgProperties);
		}
		return myUser;
	}

}
