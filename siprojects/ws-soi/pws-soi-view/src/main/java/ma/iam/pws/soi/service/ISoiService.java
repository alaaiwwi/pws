package ma.iam.pws.soi.service;

import java.util.HashMap;
import java.util.List;

import com.alcatel.ccl.client.CMSClient;
import com.alcatel.ccl.client.CMSException;

//import ma.iam.geve.common.exception.TechnicalException;

/**
 * The Interface Connecteur.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public interface ISoiService {   

  
	@SuppressWarnings("unchecked")
	void login(String login, String motDePasse) throws CMSException	 ;

}
