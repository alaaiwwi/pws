package ma.iam.pws.soi.engine.security.filter;
//package ma.iam.ws.engine.security.filter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//import javax.xml.ws.WebServiceContext;
//import javax.xml.ws.handler.MessageContext;
//
//import ma.iam.ws.engine.common.Constants;
//import ma.iam.ws.engine.common.util.MessageUtils;
//import ma.iam.ws.engine.common.util.Serializer;
//
//import org.aopalliance.intercept.MethodInterceptor;
//import org.aopalliance.intercept.MethodInvocation;
////import org.springframework.security.AuthenticationCredentialsNotFoundException;
////import org.springframework.security.AuthorizationServiceException;
////import org.springframework.security.intercept.AbstractSecurityInterceptor;
////import org.springframework.security.intercept.InterceptorStatusToken;
////import org.springframework.security.intercept.ObjectDefinitionSource;
////import org.springframework.security.intercept.method.MethodDefinitionSource;
//
//import org.springframework.security.access.AuthorizationServiceException;
//import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
//import org.springframework.security.access.intercept.InterceptorStatusToken;
//import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
//
///**
// * TODO comment.
// * <br/><br/>
// * Projet : 
// * Créé le 27 juin 2011
// * 
// * @author mei
// */
//public class MethodSecurityInterceptor extends AbstractSecurityInterceptor implements MethodInterceptor {
//    //~ Instance fields ================================================================================================
//
//    private MethodDefinitionSource objectDefinitionSource;
//    
//    private WebServiceContext webServiceContext;
//
//    //~ Methods ========================================================================================================
//
//    public MethodDefinitionSource getObjectDefinitionSource() {
//        return this.objectDefinitionSource;
//    }
//
//    public Class getSecureObjectClass() {
//        return MethodInvocation.class;
//    }
//
//    /**
//     * This method should be used to enforce security on a <code>MethodInvocation</code>.
//     *
//     * @param mi The method being invoked which requires a security decision
//     *
//     * @return The returned value from the method invocation
//     *
//     * @throws Throwable if any error occurs
//     */
//    public Object invoke(MethodInvocation mi) throws Throwable {
//        Object result = null;
//        InterceptorStatusToken token = null;
//        
//        try {
//        	this.checkAuthorization();
//        	token = super.beforeInvocation(mi);
//        	result = mi.proceed();
//        } catch (AuthenticationCredentialsNotFoundException authNotFoundException) {
//        	result = Serializer.serialize(MessageUtils.getHabilitaionMessage("user.not.authentified"));
//        } catch (AuthorizationServiceException authorizationException) {
//        	result = Serializer.serialize(MessageUtils.getHabilitaionMessage("authorization.error"));
//        } finally {
//            result = super.afterInvocation(token, result);
//        }
//
//        return result;
//    }
//    
//	/**
//	 * Gets the authentification error.
//	 *
//	 * @see ma.iam.geve.domaine.security.SecurityMngt#checkAuthorization(org.codehaus.xfire.MessageContext)
//	 */
//	public void checkAuthorization() {
//		
//		Boolean authorizationFailure = null;
//		Boolean authenticationNotFound = null;
//		MessageContext messageContext = webServiceContext.getMessageContext();
//		if (messageContext != null) {
//			HttpServletRequest request = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);
//			HttpSession session = request.getSession();
//			authorizationFailure = (Boolean) session.getAttribute(Constants.AUTHORIZATION_FAILURE_FLG);
//			authenticationNotFound = (Boolean) session.getAttribute(Constants.AUTHENTICATION_NOT_FOUND_FLG);
//		}
//
//		if (authenticationNotFound == null || authenticationNotFound.booleanValue()) {
//			throw new AuthenticationCredentialsNotFoundException("user.not.authentified");
//		}
//		if (authorizationFailure == null || authorizationFailure.booleanValue()) {
//			throw new AuthorizationServiceException("authorization.error");
//		}
//	}
//
//    public ObjectDefinitionSource obtainObjectDefinitionSource() {
//        return this.objectDefinitionSource;
//    }
//
//    public void setObjectDefinitionSource(MethodDefinitionSource newSource) {
//        this.objectDefinitionSource = newSource;
//    }
//
//	/**
//	 * The setter method for the field webServiceContext.
//	 * @param webServiceContext the webServiceContext to set.
//	 */
//	public void setWebServiceContext(WebServiceContext webServiceContext) {
//		this.webServiceContext = webServiceContext;
//	}
//
//}
