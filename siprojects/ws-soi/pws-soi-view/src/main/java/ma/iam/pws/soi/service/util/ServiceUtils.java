package ma.iam.pws.soi.service.util;

/**
 * @author Mo.Aissa
 *
 */

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapter;
import ma.iam.pws.soi.adapter.BContractAdapterAll;
import ma.iam.pws.soi.adapter.BContractedServicesAdapter;
import ma.iam.pws.soi.adapter.BContractedServicesAdapterAll;
import ma.iam.pws.soi.adapter.ServiceParamAdapter;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.BContractedServicesAll;
import ma.iam.pws.soi.beans.contract.BDeactivatedSRV;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.contract.bNewParamValue;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValues;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.services.DepSrvPrmBean;
import ma.iam.pws.soi.beans.services.ParamValueBeanPub;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.engine.common.Cug;
import ma.iam.pws.soi.engine.common.Parameter;
import ma.iam.pws.soi.engine.common.util.Utils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.tree.Tree;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.client.CMSPendingStateException;
import com.alcatel.ccl.common.Contract;
import com.alcatel.ccl.common.ServicePendingStatus;
import com.alcatel.ccl.utils.Utilities;
import com.lhs.ccb.soi.types.DateTimeI;

/**
 * This class represents a service.
 */
public class ServiceUtils implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -27066732622954200L;
	private static final Logger logger = LogManager
			.getLogger(ServiceUtils.class.getName());
	public static final String NUMBER_REGEX = "^[0-9]*$";

	/** Service code. */
	private Long _snCode = null;

	/** Service package code. */
	private Long _spCode = null;

	/** Directory number. */
	private String _dirNum = null;

	/**
	 * Indicates if the directory number is the main directory number or not. It
	 * must be set if directory number is set. The value must be true if the
	 * directory number specified is the main directory number.
	 */
	private Boolean _mainDirNum = null;

	/**
	 * Indicates if the main directory number must appear on bill. The value
	 * must be true if the directory number specified must be on bill.
	 */
	private Boolean _dirNumOnBill = null;

	/**
	 * Numbering plan code of directory number. It must be set if directory
	 * number is set.
	 */
	private Long _npCode = null;

	/**
	 * Indicates if the directory number must be created into BSCS if it does
	 * not already exist. It must be set if directory number is set.
	 */
	private Boolean _createDirNumIfUnexisting = null;

	/**
	 * Lenght of the directory number prefix Local Area Code. It must be set if
	 * directory number is set and if directory number must be created if it
	 * does not exist.
	 */
	private Long _lacLengh = null;

	/**
	 * Physical HLR to use if the directory number must be created into BSCS. It
	 * must be set if directory number is set and if directory number must be
	 * created if it does not exist.
	 */
	private Long _hlCode = null;

	/**
	 * Logical HLR to use if the directory number must be created into BSCS. It
	 * must be set if directory number is set and if directory number must be
	 * created if it does not exist.
	 */
	private Long _hmCode = null;

	/** Lowest variable part of the new eventual directory number block. */
	private String _lowerExt = null;

	/** Highest variable part of the new eventual directory number block. */
	private String _upperExt = null;

	/** Subscription fee (one-time charge). */
	private Double _cosSubfeeOvw = null;

	/**
	 * Type of the one-time charge : "A" when subscription fee is absolute (lump
	 * sum) "R" when subscription fee is relative (percentage)
	 */
	private String _cosSubfeeOvwType = null;

	/** Parameters. */
	private Parameter[] _parameters = null;

	private Double _accessFee = null;

	private String _accessFeeType = null;

	private Integer _accessFeePrd = null;

	private Cug cug = null;

	public String getAccessFeeType() {
		return _accessFeeType;
	}

	public void setAccessFeeType(String accessFeeType) {
		this._accessFeeType = accessFeeType;
	}

	public Double getAccesFee() {
		return _accessFee;
	}

	public void setAccesFee(Double accesFee) {
		this._accessFee = accesFee;
	}

	public Long getSnCode() {
		return _snCode;
	}

	public void setSnCode(Long snCode) {
		_snCode = snCode;
	}

	public Long getSpCode() {
		return _spCode;
	}

	public void setSpCode(Long spCode) {
		_spCode = spCode;
	}

	public String getDirNum() {
		return _dirNum;
	}

	public void setDirNum(String dirNum) {
		_dirNum = dirNum;
	}

	public Boolean getMainDirNum() {
		return _mainDirNum;
	}

	public void setMainDirNum(Boolean mainDirNum) {
		_mainDirNum = mainDirNum;
	}

	public Long getNpCode() {
		return _npCode;
	}

	public void setNpCode(Long npCode) {
		_npCode = npCode;
	}

	public Parameter[] getParameters() {
		return _parameters;
	}

	public void setParameters(Parameter[] parameters) {
		_parameters = parameters;
	}

	public Boolean getCreateDirNumIfUnexisting() {
		return _createDirNumIfUnexisting;
	}

	public void setCreateDirNumIfUnexisting(Boolean createDirNumIfUnexisting) {
		_createDirNumIfUnexisting = createDirNumIfUnexisting;
	}

	public Long getLacLengh() {
		return _lacLengh;
	}

	public void setLacLengh(Long lacLengh) {
		_lacLengh = lacLengh;
	}

	public Long getHlCode() {
		return _hlCode;
	}

	public void setHlCode(Long hlCode) {
		_hlCode = hlCode;
	}

	public Long getHmCode() {
		return _hmCode;
	}

	public void setHmCode(Long hmCode) {
		_hmCode = hmCode;
	}

	public String getLowerExt() {
		return _lowerExt;
	}

	public void setLowerExt(String lowerExt) {
		_lowerExt = lowerExt;
	}

	public String getUpperExt() {
		return _upperExt;
	}

	public void setUpperExt(String upperExt) {
		_upperExt = upperExt;
	}

	public Double getCosSubfeeOvw() {
		return _cosSubfeeOvw;
	}

	public void setCosSubfeeOvw(Double cosSubfeeOvw) {
		_cosSubfeeOvw = cosSubfeeOvw;
	}

	public String getCosSubfeeOvwType() {
		return _cosSubfeeOvwType;
	}

	public void setCosSubfeeOvwType(String cosSubfeeOvwType) {
		_cosSubfeeOvwType = cosSubfeeOvwType;
	}

	public Integer getAccessFeePrd() {
		return _accessFeePrd;
	}

	public void setAccessFeePrd(Integer accessFeePrd) {
		_accessFeePrd = accessFeePrd;
	}

	/**
	 * This method converts this Service to a String.
	 * 
	 * @return A String representation of this Service.
	 */
	public String toString() {
		String dump = new String();

		dump += "Service._snCode = " + _snCode;
		dump += "\nService._spCode = " + _spCode;
		dump += "\nService._dirNum = " + _dirNum;
		dump += "\nService._mainDirNum = " + _mainDirNum;
		dump += "\nService._npCode = " + _npCode;
		dump += "\nService._createDirNumIfUnexistingt = "
				+ _createDirNumIfUnexisting;
		dump += "\nService._lacLength = " + _lacLengh;
		dump += "\nService._hmCode = " + _hmCode;
		dump += "\nService._hlCode = " + _hlCode;
		dump += "\nService._lowerExt = " + _lowerExt;
		dump += "\nService._upperExt = " + _upperExt;
		dump += "\nService._cosSubfeeOvw = " + _cosSubfeeOvw;
		dump += "\nService._cosSubfeeOvwType = " + _cosSubfeeOvwType;

		if (_parameters != null) {
			for (int nParam = 0; nParam < _parameters.length; nParam++) {
				dump += "\nService._parameters[" + nParam + "] =>";
				dump += "\n" + _parameters[nParam];
				dump += "\nService._parameters[" + nParam + "] <=";
			}
		}

		return dump;
	}

	public void setCug(Cug cug) {
		this.cug = cug;
	}

	public Cug getCug() {
		return cug;
	}

	public Boolean getDirNumOnBill() {
		return _dirNumOnBill;
	}

	public void setDirNumOnBill(Boolean numOnBill) {
		_dirNumOnBill = numOnBill;
	}

	public static Long getSpCodeForSnCode(Long snCode, Long rPcode,
			SOIService soiComm) throws CMSException {
		logger.debug("=> getSpCodeForSnCode()");
		HashMap _rpCodes = new HashMap();
		Long spCodeFound = null;
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		HashMap<String, Object> outParams = new HashMap<String, Object>();
		String rpCodeParamName = "RPCODE";
		_rpCodes.put(rPcode, new HashMap());
		HashMap spCodes = new HashMap();
		logger.debug("Reading service packages for RPCODE=" + rPcode);
		inParams = new HashMap();
		inParams.put(rpCodeParamName, rPcode);
		outParams = soiComm.cmsClient.executeCommand("SERVICE_PACKAGES.READ",
				inParams);
		HashMap[] numSps = (HashMap[]) outParams.get("NUM_SP");
		if (numSps != null) {
			for (int nSp = 0; nSp < numSps.length; nSp++) {
				String spCodeParamName = "SPCODE";
				Long spCode = (Long) numSps[nSp].get(spCodeParamName);

				ArrayList snCodes = new ArrayList();

				logger.debug("Reading services for RPCODE = " + rPcode
						+ " and SPCODE = " + spCode);
				inParams = new HashMap();
				inParams.put(rpCodeParamName, rPcode);
				inParams.put(spCodeParamName, spCode);
				outParams = soiComm.cmsClient.executeCommand("SERVICES.READ",
						inParams);

				if (outParams != null) {
					HashMap[] numSvs = (HashMap[]) outParams.get("NUM_SV");
					for (int nSv = 0; nSv < numSvs.length; nSv++) {
						if (new Long((numSvs[nSv].get("SNCODE")).toString())
								.longValue() == snCode) {
							spCodeFound = spCode;
							return spCode;
						}

					}
				}

			}
		}
		logger.debug("<= getSpCodeForSnCode()");
		return spCodeFound;
	}

	public static boolean isServiceWithParameters(ServiceUtils service,
			HashMap serviceParametersReadOutParams, SOIService soiComm,
			long snCode) throws CMSException {
		logger.debug("=> isServiceWithParameters()");

		boolean isServiceWithParameters = false;

		if (serviceParametersReadOutParams == null) {
			HashMap inParams = new HashMap();
			String snCodeParamName = "SNCODE";
			logger.debug("SNCODE=" + snCode);
			inParams.put(snCodeParamName, snCode);

			serviceParametersReadOutParams = soiComm.executeCommand(
					"SERVICE_PARAMETERS.READ", inParams);
		}

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_SERV");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			HashMap[] hmNumParam = (HashMap[]) hmNumServ[0].get("NUM_PARAM");

			if ((hmNumParam != null) && (hmNumParam.length > 0)) {
				isServiceWithParameters = true;
			}
		}

		logger.debug("<= isServiceWithParameters() : "
				+ isServiceWithParameters);
		return isServiceWithParameters;
	}

	public static boolean CheckServiceWithParameters(ServiceUtils service,
			Contract contract, HashMap serviceParametersReadOutParams,
			SOIService soiComm, Properties msgProperties,
			bNewServicesEx servicesEx, String status,
			List<bNewServices> Services) throws CMSException, FaultWS {
		logger.debug("=> CheckServiceWithParameters()");

		boolean isServiceWithParameters = false;

		if (serviceParametersReadOutParams == null) {
			HashMap inParams = new HashMap();
			String snCodeParamName = "SNCODE";
			Long snCode = service.getSnCode();
			logger.debug("SNCODE=" + snCode);
			inParams.put(snCodeParamName, snCode);

			serviceParametersReadOutParams = soiComm.executeCommand(
					"SERVICE_PARAMETERS.READ", inParams);
		}

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_SERV");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			HashMap[] hmNumParam = (HashMap[]) hmNumServ[0].get("NUM_PARAM");

			if ((hmNumParam != null) && (hmNumParam.length > 0)) {

				if ((servicesEx.getBNewParamValueEx() != null)
						&& (servicesEx.getBNewParamValueEx().size() > 0)) {

					for (Iterator iterator = servicesEx.getBNewParamValueEx()
							.iterator(); iterator.hasNext();) {
						bNewParamValueEx pRmVal = (bNewParamValueEx) iterator
								.next();
						isServiceWithParameters = false;

						for (int i = 0; i < hmNumParam.length; i++) {
							Boolean isPrmMultipleValues = false;
							Boolean multValueInd = (Boolean) hmNumParam[i]
									.get("MULT_VALUE_IND");

							if (multValueInd == null) {
								isPrmMultipleValues = false;
							} else {
								isPrmMultipleValues = multValueInd
										.booleanValue();
							}
							if (new Long(hmNumParam[i].get("PRM_ID").toString())
									.longValue() == pRmVal.getPRM_ID()) {

								pRmVal.setPRM_NO(new Long(hmNumParam[i].get(
										"PRM_NO").toString()).longValue());
								pRmVal.setPRM_DES((hmNumParam[i].get("PRM_DES")
										.toString()));
								pRmVal.setPRM_ID_PUB(hmNumParam[i].get(
										"PRM_ID_PUB").toString());
								pRmVal.setMULT_VALUE_IND(multValueInd);
								isServiceWithParameters = true;

								getAndCompareDefaultPrmValues(
										contract.getRpCode(),
										service.getSnCode(),
										pRmVal.getPRM_ID(), soiComm,
										msgProperties, null, pRmVal,
										isPrmMultipleValues);

							}

						}
						if (!isServiceWithParameters)
							throw new FaultWS(
									Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_SVC,
									msgProperties);

					}
				} else if ((servicesEx.getBNewParamValueEx() != null)
						&& SoiUtils.SERVICE_STATUT_DEACTIVE.equals(status)) {
					return false;
				} else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_SVC_ERROR,
							msgProperties);
				}

			} else {
				if ((servicesEx.getBNewParamValueEx() != null)
						&& (servicesEx.getBNewParamValueEx().size() > 0)) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_ACTIVATESVC_SVC_NOPRM,
							msgProperties);

				} else {
					isServiceWithParameters = false;
				}
			}

		}

		fill_Auto_params(soiComm, servicesEx, contract, Services);

		// FTAT ADD MZ
		fill_Auto_MZ_params(soiComm, servicesEx, contract, Services);

		logger.debug("<= CheckServiceWithParameters() : "
				+ isServiceWithParameters);
		return isServiceWithParameters;
	}

	public static void getAndCompareDefaultPrmValues(Long rpCode, Long snCode,
			Long prmId, SOIService soiComm, Properties msgProperties,
			HashMap serviceParametersReadOutParams, bNewParamValueEx pRmVal,
			Boolean isPrmMultipleValues) throws CMSException, FaultWS {
		logger.debug("=> getAndCompareDefaultPrmValues()");
		boolean GET_VALUES = true;
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		;
		inParams.put("SNCODE", snCode);
		inParams.put("RPCODE", rpCode);
		inParams.put("PRM_ID", prmId);
		inParams.put("SCCODE", (long) 1);
		inParams.put("GET_VALUES", GET_VALUES);

		serviceParametersReadOutParams = soiComm.executeCommand(
				"PARAMETER.READ", inParams);

		HashMap[] hmNumValue = (HashMap[]) serviceParametersReadOutParams
				.get("N_VALUES");

		if ((hmNumValue != null) && (hmNumValue.length > 0)) {
			if ((pRmVal.getBNewValueEx() != null)
					&& (pRmVal.getBNewValueEx().size() > 0)) {

				if (!isPrmMultipleValues) {
					if (pRmVal.getBNewValueEx().size() == 1) {
						for (Iterator it = pRmVal.getBNewValueEx().iterator(); it
								.hasNext();) {
							bNewValuesEx vaLue = (bNewValuesEx) it.next();
							boolean isParamWithValue = false;

							// >******* Updated, A.ESSA QC10355
							if (hmNumValue[0].get("VALUE_DES").toString()
									.trim().equals("Min value")
									|| hmNumValue[0].get("VALUE_DES")
											.toString().trim()
											.equals("Max value")) {
								if (serviceParametersReadOutParams
										.get("DATA_TYPE").toString()
										.equals("NU")) {
									if (vaLue.getVALUE_DES().matches(
											NUMBER_REGEX)) {
										vaLue.setVALUE((vaLue.getVALUE_DES()));
										isParamWithValue = true;
									} else {
										throw new FaultWS(
												Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_TYPE_ERROR,
												msgProperties);
									}
								} else {
									vaLue.setVALUE((vaLue.getVALUE_DES()));
									isParamWithValue = true;
								}
							} else if (serviceParametersReadOutParams
									.get("TYPE").toString().equals("CB")) {// Boolean
																			// value
								// >******* Updated, A.ESSA QC12440
								if (vaLue.getVALUE_DES().equals("N")) {
									// <******* Updated, A.ESSA QC12440
									vaLue.setVALUE("N");
									isParamWithValue = true;
								} else
								// >******* Updated, A.ESSA QC12440
								if (vaLue.getVALUE_DES().equals("Y")) {
									// <******* Updated, A.ESSA QC12440
									vaLue.setVALUE("Y");
									isParamWithValue = true;
								}
							}
							// <******* Updated, A.ESSA QC10355
							else {
								for (int vaL = 0; vaL < hmNumValue.length; vaL++) {
									if (hmNumValue[vaL]
											.get("VALUE_DES")
											.toString()
											.trim()
											.equals(vaLue.getVALUE_DES().trim())) {

										if (hmNumValue[vaL]
												.containsKey("VALUE_SEQNO"))
											vaLue.setVALUE((hmNumValue[vaL]
													.get("VALUE_SEQNO")
													.toString()));
										else
											vaLue.setVALUE((hmNumValue[vaL]
													.get("VALUE").toString()));

										isParamWithValue = true;
										break;
									}
								}
								if (!isParamWithValue)
									throw new FaultWS(
											Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
											msgProperties);
							}
						}
					} else {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_MX_ONEVALUE,
								msgProperties);
					}
				} else {
					if (pRmVal.getBNewValueEx().size() < 2) {
						for (Iterator it = pRmVal.getBNewValueEx().iterator(); it
								.hasNext();) {
							bNewValuesEx vaLue = (bNewValuesEx) it.next();
							boolean isParamWithValue = false;
							for (int vaL = 0; vaL < hmNumValue.length; vaL++) {
								if (hmNumValue[vaL].get("VALUE_DES").toString()
										.trim()
										.equals(vaLue.getVALUE_DES().trim())) {

									if (hmNumValue[vaL]
											.containsKey("VALUE_SEQNO"))
										vaLue.setVALUE((hmNumValue[vaL]
												.get("VALUE_SEQNO").toString()));
									else
										vaLue.setVALUE((hmNumValue[vaL]
												.get("VALUE").toString()));

									isParamWithValue = true;
								}
							}
							if (!isParamWithValue)
								throw new FaultWS(
										Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_UN_VALUE,
										msgProperties);

						}
					} else {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_MORE_VALUE,
								msgProperties);
					}
				}

			} else {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VALUE_ONE_ATlEAST,
						msgProperties);
			}
		} else {
			if ((pRmVal.getBNewValueEx() != null)
					&& (pRmVal.getBNewValueEx().size() > 0)) {
				for (int v = 0; v < pRmVal.getBNewValueEx().size(); v++) {
					bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(v);
					vaLue.setVALUE(vaLue.getVALUE_DES().toString());
				}
			}
		}

		logger.debug("=> getAndCompareDefaultPrmValues()");
	}

	public static void initializeServiceParametersToDefault(
			ServiceUtils service, Long coId, SOIService soiComm,
			bNewServicesEx servicesEx) throws CMSException {
		logger.debug("=> initializeServiceParametersToDefault()");

		if (servicesEx.getBNewParamValueEx().size() != 0) {
			for (int i = 0; i < servicesEx.getBNewParamValueEx().size(); i++) {
				bNewParamValueEx parameterValeus = servicesEx
						.getBNewParamValueEx().get(i);
				if (isServiceParameterWithMultipleValues(service, soiComm,
						parameterValeus))
					return;
			}
		}

		HashMap<String, Object> inParams1 = new HashMap<String, Object>();
		String snCodeParamName = "SNCODE";
		Long snCode = service.getSnCode();
		logger.debug("SNCODE=" + snCode);
		inParams1.put(snCodeParamName, snCode);
		Long TEMPL_ID = null;
		HashMap serviceParametersTemplatesReadOutParams = soiComm
				.executeCommand("SERVICE_PARAMETER_TEMPLATES.READ", inParams1);
		HashMap[] hmNumServ = (HashMap[]) serviceParametersTemplatesReadOutParams
				.get("NUM_SERV");
		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			HashMap[] hmNumTEMPL = (HashMap[]) hmNumServ[0].get("NUM_TEMPL");
			if ((hmNumTEMPL != null) && (hmNumTEMPL.length > 0)
					&& (hmNumTEMPL[0] != null)) {
				TEMPL_ID = (Long) hmNumTEMPL[0].get("TEMPL_ID");
			}
		}
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("PROFILE_ID", new Long(0));
		Long spCode = service.getSpCode();
		inParams.put("SPCODE", spCode);
		inParams.put("CO_ID", coId);
		inParams.put("SNCODE", snCode);
		HashMap<String, Character> hmParamValues = new HashMap<String, Character>();
		hmParamValues.put("COS_ACTION", new Character('i'));
		inParams.put("PARAM_VALUES", new HashMap[] { hmParamValues });
		if (TEMPL_ID != null) {

			inParams.put("TEMPL_ID", TEMPL_ID);

		}

		soiComm.executeCommand("CONTRACT_SERVICE_PARAMETERS.WRITE", inParams);
		logger.debug("<= initializeServiceParametersToDefault()");
	}

	public static void getDefaultParametersValues(Long coId,
			SOIService soiComm, HashMap serviceParametersReadOutParams,
			bNewServicesEx servicesEx) throws CMSException {
		logger.debug("=> getDefaultParametersValues()");

		HashMap inParams1 = new HashMap();
		String snCodeParamName = "SNCODE";
		Long snCode = servicesEx.getSNCODE();
		inParams1.put("SNCODE", snCode);

		serviceParametersReadOutParams = soiComm.executeCommand(
				"SERVICE_PARAMETERS.READ", inParams1);

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_SERV");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			HashMap[] hmNumParam = (HashMap[]) hmNumServ[0].get("NUM_PARAM");

			if ((hmNumParam != null) && (hmNumParam.length > 0)
					&& (hmNumParam[0] != null)) {
				bNewParamValueEx NewParam[] = new bNewParamValueEx[1];
				List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
				for (int i = 0; i < hmNumParam.length; i++) {
					HashMap numParam = hmNumParam[i];
					Long currPrmId = (Long) numParam.get("PRM_ID");
					NewParam[i] = new bNewParamValueEx();
					bNewParamValueEx pRm = new bNewParamValueEx();
					pRm.setPRM_ID(currPrmId);
					pRm.setPRM_DES(numParam.get("PRM_DES").toString());
					pRm.setPRM_ID_PUB(numParam.get("PRM_ID_PUB").toString());
					pRm.setPRM_NO((Long) numParam.get("PRM_NO"));

					HashMap[] hmNumValue = (HashMap[]) numParam.get("N_VALUES");

					if ((hmNumValue != null) && (hmNumValue.length > 0)
							&& (hmNumValue[0] != null)) {
						List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();
						for (int j = 0; j < hmNumValue.length; j++) {
							HashMap numValue = hmNumValue[i];
							bNewValuesEx value = new bNewValuesEx();
							value.setVALUE_DES(numValue.get("VALUE_DES")
									.toString());
							value.setVALUE(numValue.get("VALUE_SEQNO")
									.toString());
							BNewValuesEx.add(value);
						}
						pRm.setBNewValueEx(BNewValuesEx);

						BNewParamValueEx.add(pRm);

					}

				}
				servicesEx.setBNewParamValueEx(BNewParamValueEx);
			}
		}

		logger.debug("<= getDefaultParametersValues()");
	}

	public static boolean isServiceParameterWithMultipleValues(
			ServiceUtils service, SOIService soiComm,
			bNewParamValueEx parameterValeus) throws CMSException {
		logger.debug("=> isServiceParameterWithMultipleValues()");

		boolean isServiceParameterWithMultipleValues = false;

		HashMap inParams = new HashMap();
		String snCodeParamName = "SNCODE";
		Long snCode = service.getSnCode();
		inParams.put(snCodeParamName, snCode);

		HashMap outParams = soiComm.executeCommand("SERVICE_PARAMETERS.READ",
				inParams);

		HashMap[] hmNumServ = (HashMap[]) outParams.get("NUM_SERV");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			HashMap[] hmNumParam = (HashMap[]) hmNumServ[0].get("NUM_PARAM");

			if (hmNumParam != null) {
				Long prmId = parameterValeus.getPRM_ID();
				Long prmNo = parameterValeus.getPRM_NO();
				boolean stop = false;
				int nNumParam = 0;

				while ((!stop) && (nNumParam < hmNumParam.length)) {
					HashMap numParam = hmNumParam[nNumParam];

					if (numParam != null) {
						Long currPrmId = (Long) numParam.get("PRM_ID");

						String prmNoParamName = "PRM_NO";
						Long currPrmNo = (Long) numParam.get(prmNoParamName);

						if ((prmId.compareTo(currPrmId) == 0)
								&& (prmNo.compareTo(currPrmNo) == 0)) {
							Boolean multValueInd = (Boolean) numParam
									.get("MULT_VALUE_IND");

							if (multValueInd == null) {
								isServiceParameterWithMultipleValues = false;
							} else {
								isServiceParameterWithMultipleValues = multValueInd
										.booleanValue();
							}

							stop = true;
						}
					}

					nNumParam += 1;
				}

				if (stop == false) {
					String msg = new String(
							"Unable to find MULT_VALUE_IND in SERVICE_PARAMETERS.READ out parameters for prmId = "
									+ prmId + " and prmNo = " + prmNo);
					logger.error(msg);
					throw new CMSException(msg);
				}
			}
		}

		logger.debug("<= isServiceParameterWithMultipleValues() : "
				+ isServiceParameterWithMultipleValues);
		return isServiceParameterWithMultipleValues;
	}

	public static void modifyServiceParameters(ServiceUtils service,
			boolean isForNewContract, Long coId, SOIService soiComm,
			bNewServicesEx servicesEx) throws CMSException {
		logger.debug("=> modifyServiceParameters()");

		HashMap[] serviceParamMap = getServiceParameter(service, soiComm);

		ArrayList<HashMap<String, Object>> paramsToWrite = new ArrayList();

		for (Iterator ItPrm = servicesEx.getBNewParamValueEx().iterator(); ItPrm
				.hasNext();) {
			bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();
			HashMap[] tabHmMultValues = null;
			HashMap<String, Object> hmParamValues = new HashMap<String, Object>();

			boolean overwritable = isServiceParameterOverwritable(
					serviceParamMap, pRmVal.getPRM_ID(), pRmVal.getPRM_NO());
			boolean isServiceParameterWithMultipleValues = isServiceParameterWithMultipleValues(
					service, soiComm, pRmVal);

			if (isServiceParameterWithMultipleValues == true) {
				hmParamValues.put("COS_ACTION", new Character('a'));
			} else {
				if (overwritable) {
					hmParamValues.put("COS_ACTION", new Character('m'));
				}
			}

			tabHmMultValues = new HashMap[pRmVal.getBNewValueEx().size()];

			for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
				bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
				if (vaLue.getVALUE_DES() != null) {
					HashMap<String, String> hmMultValues = new HashMap<String, String>();
					hmMultValues.put("VALUE", vaLue.getVALUE());
					hmMultValues.put("VALUE_DES", vaLue.getVALUE_DES());
					tabHmMultValues[nVal] = hmMultValues;
				}
			}
			HashMap<String, Object> hmTargetParamValues = new HashMap<String, Object>();
			hmTargetParamValues.put("TARGET_SIBLING_NO", new Long(1));
			hmTargetParamValues.put("MULT_VALUES", tabHmMultValues);
			hmParamValues.put("TARGET_PARAM_VALUES",
					new HashMap[] { hmTargetParamValues });
			hmParamValues.put("PRM_NO", pRmVal.getPRM_NO());
			hmParamValues.put("SIBLING_NO", new Long(1));
			hmParamValues.put("PARENT_NO", new Long(1));
			hmParamValues.put("COMPLEX_NO", new Long(1));
			hmParamValues.put("COMPLEX_LEVEL", new Long(1));
			hmParamValues.put("COS_ACTION", new Character('a'));
			paramsToWrite.add(hmParamValues);

		}

		if (!paramsToWrite.isEmpty()) {
			HashMap inParams = new HashMap();
			inParams.put("PROFILE_ID", new Long(0));

			String spCodeParamName = "SPCODE";
			Long spCode = service.getSpCode();
			inParams.put(spCodeParamName, spCode);

			String snCodeParamName = "SNCODE";
			Long snCode = service.getSnCode();
			inParams.put(snCodeParamName, snCode);

			int nbParamsToWrite = paramsToWrite.size();
			inParams.put("PARAM_VALUES",
					paramsToWrite.toArray(new HashMap[nbParamsToWrite]));

			if (!isForNewContract) {
				if (coId != null) {
					inParams.put("CO_ID", coId);
				}
			}

			soiComm.executeCommand("CONTRACT_SERVICE_PARAMETERS.WRITE",
					inParams);
		}

		logger.debug("<= modifyServiceParameters()");
	}

	public static HashMap[] getServiceParameter(ServiceUtils service,
			SOIService exec) throws CMSException {

		HashMap inParams = new HashMap();
		String snCodeParamName = "SNCODE";
		Long snCode = service.getSnCode();
		inParams.put(snCodeParamName, snCode);

		HashMap outParams = exec.executeCommand("SERVICE_PARAMETERS.READ",
				inParams);

		return (HashMap[]) outParams.get("NUM_SERV");
	}

	public static boolean isServiceParameterOverwritable(HashMap[] hmNumServ,
			Long prmId, Long prmNo) throws CMSException {
		logger.debug("=> isServiceParameterOverwritable()");
		boolean isServiceParameterOverwritable = false;
		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			HashMap[] hmNumParam = (HashMap[]) hmNumServ[0].get("NUM_PARAM");
			if (hmNumParam != null) {
				boolean stop = false;
				int nNumParam = 0;

				while ((!stop) && (nNumParam < hmNumParam.length)) {
					HashMap numParam = hmNumParam[nNumParam];

					if (numParam != null) {
						Long currPrmId = (Long) numParam.get("PRM_ID");
						String prmNoParamName = "PRM_NO";
						Long currPrmNo = (Long) numParam.get(prmNoParamName);
						if ((prmId.compareTo(currPrmId) == 0)
								&& (prmNo.compareTo(currPrmNo) == 0)) {
							Boolean overrideValueInd = (Boolean) numParam
									.get("OVERRIDE_IND");
							if (overrideValueInd == null) {
								isServiceParameterOverwritable = false;
							} else {
								isServiceParameterOverwritable = overrideValueInd
										.booleanValue();
							}

							stop = true;
						}
					}

					nNumParam += 1;
				}
				if (stop == false) {
					String msg = new String(
							"Unable to find OVERRIDE_IND in SERVICE_PARAMETERS.READ out parameters for prmId = "
									+ prmId + " and prmNo = " + prmNo);
					logger.error(msg);
					throw new CMSException(msg);
				}
			}
		}

		logger.debug("<= isServiceParameterOverwritable() : "
				+ isServiceParameterOverwritable);
		return isServiceParameterOverwritable;
	}

	public static boolean isServiceWithPendingRequest(ServiceUtils service,
			Contract contract, HashMap serviceDataInContract)
			throws CMSException {
		logger.debug("=> isServiceWithPendingRequest()");

		if (serviceDataInContract == null) {
			Long snCode = service.getSnCode();
			Long coId = contract.getCoId();
			String msg = new String(
					"Unable to find service data in CONTRACT.READ out parameters for service with SNCODE = "
							+ snCode + " of contract with CO_ID = " + coId);
			// Logger.error(msg);
			throw new CMSException(msg);
		}

		Integer coSnPendingStatus = (Integer) serviceDataInContract
				.get("CO_SN_PENDING_STATUS");
		boolean isWithPending = (coSnPendingStatus != null)
				&& (!coSnPendingStatus.equals(ServicePendingStatus.UNDEFINED
						.getValue()));

		logger.debug("<= isServiceWithPendingRequest() : " + isWithPending);
		return isWithPending;
	}

	public static HashMap checkIfServiceBelongToContract(ServiceUtils service,
			Contract contract, HashMap contractReadOutParams, SOIService soiComm)
			throws CMSException {
		HashMap hmService = null;
		boolean serviceFound = false;
		// If the CONTRACT.READ CMS command out parameters are not provided.
		if (contractReadOutParams == null) {
			HashMap inParams = new HashMap();
			Long coId = contract.getCoId();
			inParams.put("CO_ID", coId);
			contractReadOutParams = soiComm.executeCommand("CONTRACT.READ",
					inParams);
		}

		HashMap[] servicesInContract = (HashMap[]) contractReadOutParams
				.get("services");

		if ((servicesInContract == null) || (servicesInContract.length == 0)) {
			// Nothing to do. The service does not belong to the contract.
		} else {
			Long snCodeSearched = service.getSnCode();
			int nService = 0;

			while ((!serviceFound) && (nService < servicesInContract.length)) {
				Long snCode = (Long) servicesInContract[nService].get("SNCODE");

				if ((snCode != null) && (snCode.equals(snCodeSearched))) {
					serviceFound = true;
					hmService = servicesInContract[nService];
				} else {
					nService += 1;
				}
			}
		}
		// >******* Updated, A.ESSA FC5197
		return hmService;
		// return serviceFound;
		// <******* Updated, A.ESSA FC5197
	}

	public static boolean deactivate(ServiceUtils service, Contract contract,
			HashMap inDeactivateService, SOIService soiComm,
			Properties msgProperties) throws CMSException,
			CMSPendingStateException, FaultWS {
		logger.debug("=> DeactivateService");

		boolean result = false;
		Long snCode = service.getSnCode();
		Long coId = contract.getCoId();

		// If the service has a pending request.
		if (ServiceUtils.isServiceWithPendingRequest(service, contract,
				inDeactivateService)) {
			String msg = new String("Service with SNCODE = " + snCode
					+ " of contract with CO_ID = " + coId
					+ " has pending request");
			logger.error(msg);
			throw new CMSPendingStateException(msg);
		}

		// If the service is active or on hold, then it can be deactivated or
		// deleted.
		if ((ServiceUtils.isServiceWithStatus(service, contract,
				inDeactivateService, SoiUtils.SERVICE_STATUT_ACTIVE))
				|| (ServiceUtils.isServiceWithStatus(service, contract,
						inDeactivateService, SoiUtils.SERVICE_STATUT_ON_HOLD))) {
			HashMap<String, Object> hmService = new HashMap<String, Object>();
			hmService.put("COS_PENDING_STATUS",
					ServicePendingStatus.DEACTIVE.getValue());
			hmService.put("PROFILE_ID", new Long(0));
			hmService.put("SNCODE", snCode);

			HashMap<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("CO_ID", coId);
			inParams.put("services", new HashMap[] { hmService });

			soiComm.executeCommand(SoiUtils.CMD_CONTRACT_SERVICES_WRITE,
					inParams);
			result = true;
		}
		// If the service is deactive.
		else if (ServiceUtils.isServiceWithStatus(service, contract,
				inDeactivateService, SoiUtils.SERVICE_STATUT_DEACTIVE)) {
			result = false;
			// throw new FaultWS("RC7001", "Service :" + service.getSnCode()+
			// "for the Contract: " + coId + " is already in the new state");
			throw new FaultWS(
					Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_ERROR_NEW_STAT,
					msgProperties);

		} else {
			result = true;
			// String msg = new String("Service with SNCODE = " + snCode+ " of
			// contract with CO_ID = " + coId+ " is not active or on hold");
			result = false;
			throw new FaultWS(
					Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_ERROR_STAT,
					msgProperties);
		}

		logger.debug("<= DeactivateService");
		return result;
	}

	public static BContractedServices buildDeactivateServicesBean(
			HashMap<String, Object> inDeactivateService[],
			BDeactivatedSRV bcontract) {
		BContractedServices bServices = new BContractedServices();
		try {
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy MM dd HH:mm:ss");
			for (int i = 0; i < inDeactivateService.length; i++) {
				bServices
						.setSNCODE(inDeactivateService[i].get("SNCODE") != null ? new Long(
								inDeactivateService[i].get("SNCODE").toString())
								.longValue()
								: 0);
				bServices.setSNCODE_PUB(inDeactivateService[i]
						.get("SNCODE_PUB") != null ? inDeactivateService[i]
						.get("SNCODE_PUB").toString() : null);
				bServices
						.setSPCODE(inDeactivateService[i].get("SPCODE") != null ? new Long(
								inDeactivateService[i].get("SPCODE").toString())
								.longValue()
								: 0);
				bServices.setSPCODE_PUB(inDeactivateService[i]
						.get("SPCODE_PUB") != null ? inDeactivateService[i]
						.get("SPCODE_PUB").toString() : null);
				bServices.setCOS_PENDING_STATUS(inDeactivateService[i]
						.get("COS_PENDING_STATUS") != null ? new Integer(
						inDeactivateService[i].get("COS_PENDING_STATUS")
								.toString()) : 0);
				bServices.setCOS_STATUS(inDeactivateService[i]
						.get("COS_STATUS") != null ? new Integer(
						inDeactivateService[i].get("COS_STATUS").toString())
						: 0);
				bServices
						.setCOS_PENDING_STATUS_DATE(Utils.converttoDatefromDateTimeI(
								(DateTimeI) inDeactivateService[i]
										.get("COS_PENDING_STATUS_DATE"), format));

			}
		} catch (Exception ex) {
			// TODO
			System.out.println(ex.getMessage());
		}
		return bServices;
	}

	public static boolean isServiceWithStatus(ServiceUtils service,
			Contract contract, HashMap contractReadOutParams,
			String statusToCheck) throws CMSException {
		logger.debug("=> isServiceWithStatus()");
		logger.debug("Expected status = " + statusToCheck);

		boolean isExpectedStatus = false;

		// Get the service data in contract for the service provided.
		// HashMap serviceDataInContract =
		// getServiceDataInContractReadOutParams(service, contract,
		// contractReadOutParams);

		logger.debug("contractReadOutParams = "
				+ Utilities.convertHashMapToString(contractReadOutParams));

		if (contractReadOutParams == null) {
			Long snCode = service.getSnCode();
			Long coId = contract.getCoId();
			String msg = new String(
					"Unable to find service data in CONTRACT.READ out parameters for service with SNCODE = "
							+ snCode + " of contract with CO_ID = " + coId);

		} else {
			String coSnStatus = contractReadOutParams.get("COS_STATUS")
					.toString();
			logger.debug("Current status for snCode " + service.getSnCode()
					+ " = " + coSnStatus);
			isExpectedStatus = (coSnStatus != null)
					&& (coSnStatus.equals(statusToCheck));
		}
		logger.debug("<= isServiceWithStatus() : " + isExpectedStatus);
		return isExpectedStatus;
	}

	public static Long getPrmIDFromShdes(String shdes, SOIService soiComm)
			throws FaultWS, CMSException {

		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("GET_VALUES", false);
		inParams.put("PRM_ID_PUB", shdes);
		inParams.put("SCCODE", (long) 1);
		HashMap<String, Object> outParamsBean = soiComm.executeCommand(
				SoiUtils.CMD_PARAMETER_READ, inParams);

		return (Long) outParamsBean.get("PRM_ID");

	}

	public static boolean isProhibitedToBeModified(bNewServices srv,
			SOIService soiComm) throws FaultWS, CMSException {
		List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		for (Iterator ItPrm = srv.getBNewParamValue().iterator(); ItPrm
				.hasNext();) {
			bNewParamValue bNewParamValue = (bNewParamValue) ItPrm.next();
			bNewParamValue.getPRM_ID();
			inParams.clear();
			inParams.put("GET_VALUES", false);
			inParams.put("PRM_ID", bNewParamValue.getPRM_ID());
			inParams.put("SCCODE", (long) 1);
			HashMap<String, Object> outParamsBean = soiComm.executeCommand(
					SoiUtils.CMD_PARAMETER_READ, inParams);

			if ("PRSDP".equals(outParamsBean.get("PRM_ID_PUB"))) {
				return true;
			}

		}
		return false;
	}

	public static boolean isAllowedToBeModified(long snCode, SOIService soiComm)
			throws CMSException {
		// >******* Updated, A.ESSA FC6195
		String listNotAllowedSrv = "GOLD,NUMPR,FEF,FORHC,NMPPR,NPMPO,NUILM,NUILF,NPT17,FONI,FONII,FNILG,NIGI2,GPRSF,FGPRS,INT3G,IND3G,PACK,SNSEC";
		// <******* Updated, A.ESSA FC6195
		boolean serviceAllowed = true;// listNotAllowedSrv="f"
		HashMap inParams = new HashMap();
		inParams.put("SNCODE", snCode);
		HashMap contractReadOutParams = soiComm.executeCommand("SERVICES.READ",
				inParams);
		HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
				.get("NUM_SV");

		if ((serviceToModif != null) || (serviceToModif.length != 0)) {
			String des = serviceToModif[0].get("SV_DES").toString();
			if (des.toUpperCase().startsWith("forfait".toUpperCase())) {
				serviceAllowed = false;
			} else {
				String sHdes = serviceToModif[0].get("SNCODE_PUB").toString();
				StringTokenizer notAllowedSrv = new StringTokenizer(
						listNotAllowedSrv, ",");

				while ((serviceAllowed) && (notAllowedSrv.hasMoreTokens())) {
					if (sHdes.equals(notAllowedSrv.nextToken())) {
						serviceAllowed = false;
					}
				}
			}
		}
		return serviceAllowed;
	}

	public static boolean isAllowedToBeDeactivated(long rpcode, long snCode,
			SOIService soiComm) throws CMSException {
		String listNotAllowedSrv = "GRSIM,PACPR,PRO3G,FONII,FVOIP,FONI,MOHV3,FNILG,NIGI2,ODDS3";

		//start FC7466
		 listNotAllowedSrv.concat(",INCOM"); 
		//end  FC7466
		String listNotAllowedSrvRp = "DVG,FGPRS,I3ILL,IND3G,MHV4B,MOHV4,SMSMO,SPEAT,SPEDT";
		String sHdes = " ";
		String rpShdes = " ";
		boolean serviceAllowed = true;
		HashMap inParams = new HashMap();
		inParams.put("SNCODE", snCode);
		HashMap contractReadOutParams = soiComm.executeCommand("SERVICES.READ",
				inParams);
		HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
				.get("NUM_SV");

		if ((serviceToModif != null) || (serviceToModif.length != 0)) {
			sHdes = serviceToModif[0].get("SNCODE_PUB").toString();
			StringTokenizer notAllowedSrv = new StringTokenizer(
					listNotAllowedSrv, ",");
			while ((serviceAllowed) && (notAllowedSrv.hasMoreTokens())) {
				if (sHdes.equals(notAllowedSrv.nextToken())) {
					serviceAllowed = false;
					break;
				}
			}
		}
		if (serviceAllowed) {
			inParams = new HashMap();
			inParams.put("RPCODE", rpcode);
			contractReadOutParams = soiComm.executeCommand(
					SoiUtils.CMD_RATEPLANS_READ, inParams);
			serviceToModif = (HashMap[]) contractReadOutParams.get("NUM_RP");
			if ((serviceToModif != null) || (serviceToModif.length != 0)) {
				rpShdes = serviceToModif[0].get("RPCODE_PUB").toString();
				StringTokenizer notAllowedSrv = new StringTokenizer(
						SoiUtils.listNotAllowedRpSrv, ",");
				while ((serviceAllowed) && (notAllowedSrv.hasMoreTokens())) {
					if (rpShdes.equals(notAllowedSrv.nextToken())) {
						notAllowedSrv = new StringTokenizer(
								listNotAllowedSrvRp, ",");
						while ((serviceAllowed)
								&& (notAllowedSrv.hasMoreTokens())) {
							if (sHdes.equals(notAllowedSrv.nextToken())) {
								return false;
							}
						}
					}
				}
			}
		}
		return serviceAllowed;
	}

	public static boolean activateServiceInContract(ServiceUtils service,
			Contract contract, SOIService soiComm, Boolean onhold)
			throws CMSException, CMSPendingStateException {
		logger.debug("=> activateServiceInContract()");
		Long snCode = service.getSnCode();
		Long coId = contract.getCoId();
		boolean result = false;

		// Activation of the service.
		HashMap hmService = new HashMap();
		if (onhold == null || !onhold)
			hmService.put("COS_PENDING_STATUS",
					ServicePendingStatus.ACTIVE.getValue());
		hmService.put("PROFILE_ID", new Long(0));
		hmService.put("SNCODE", snCode);

		HashMap inParams = new HashMap();
		inParams.put("CO_ID", coId);
		inParams.put("services", new HashMap[] { hmService });

		soiComm.executeCommand("CONTRACT_SERVICES.WRITE", inParams);
		result = true;

		logger.debug("<= activateServiceInContract()");

		return result;
	}

	// >******* Updated, A.ESSA ReactivateService
	public static String getServiceStatus(ServiceUtils service,
			Contract contract, HashMap contractReadOutParams)
			throws CMSException {
		logger.debug("=> getServiceStatus()");
		logger.debug("contractReadOutParams = "
				+ Utilities.convertHashMapToString(contractReadOutParams));

		HashMap[] servicesInContract = (HashMap[]) contractReadOutParams
				.get("services");
		String status = SoiUtils.SERVICE_STATUT_UNDEFINED;
		if (contractReadOutParams == null) {
			return status;
		} else {

			Long snCodeSearched = service.getSnCode();
			for (int i = 0; i < servicesInContract.length; i++) {
				Long snCode = (Long) servicesInContract[i].get("SNCODE");
				if ((snCode != null) && (snCode.equals(snCodeSearched))) {
					status = servicesInContract[i].get("COS_STATUS").toString();
					;
				}
			}

			logger.debug("Current status for snCode " + service.getSnCode()
					+ " = " + status);
		}
		logger.debug("<= getServiceStatus() : " + status);
		return status;
	}

	// <******* Updated, A.ESSA ReactivateService
	// >******* Updated, A.ESSA CheckDependency
	public static List getContrEligibleServ(Bcontract bcontract,
			SOIService soiComm) throws CMSException, FaultWS {

		List<String> allowedSncode = new ArrayList<String>();
		Long tmcode = bcontract.getRPCODE();

		HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
		inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
		HashMap<String, Object> outParamsCustomer = soiComm.executeCommand(
				SoiUtils.CMD_CUSTOMER_READ, inParamsCustomer);
		String prgCode = outParamsCustomer.get("PRG_CODE").toString();

		HashMap<String, Object> inParamsAllServices = new HashMap<String, Object>();
		inParamsAllServices.put("PRG_CODE", prgCode);
		inParamsAllServices.put("RPCODE", bcontract.getRPCODE());
		HashMap<String, Object> outAllowedServices = soiComm.executeCommand(
				SoiUtils.CMD_ALLOWED_SERVICES_READ, inParamsAllServices);

		HashMap<String, Object>[] outAllowedServicesList = (HashMap<String, Object>[]) outAllowedServices
				.get("LIST_SP_CODE");

		for (int i = 0; i < outAllowedServicesList.length; i++) {
			HashMap<String, Object>[] hashMap = (HashMap<String, Object>[]) outAllowedServicesList[i]
					.get("LIST_SN_CODE");
			for (int j = 0; j < hashMap.length; j++) {
				HashMap<String, Object> hashMap2 = hashMap[j];
				allowedSncode.add(hashMap2.get("SNCODE").toString());
			}
		}

		return allowedSncode;
	}

	public static List getContrActivServ(Bcontract bcontract)
			throws CMSException, FaultWS {

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		List contrActivServ = new ArrayList();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
				contrActivServ.add(Long.valueOf(serv.getSNCODE()).toString());
			}

		}
		return contrActivServ;
	}

	// <******* Updated, A.ESSA CheckDependency

	// >******* Updated, A.ESSA ReactivateService
	public static boolean activateServiceInContract(ServiceUtils service,
			Contract contract, boolean isServiceToBeActiveNow,
			SOIService soiComm, bNewServicesEx servicesEx, String status,
			Properties msgProperties, List<bNewServices> Services)
			throws CMSException, FaultWS, CMSPendingStateException {

		logger.debug("=> activateServiceInContract");

		boolean result = false;
		Long snCode = service.getSnCode();
		Long coId = contract.getCoId();
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("CO_ID", new Long(servicesEx.getCO_ID()).longValue());
		inParams.put("SNCODE", servicesEx.getSNCODE());
		HashMap<String, Object> outParamsCtrSrv = soiComm.executeCommand(
				SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
		// If the service has a pending request.
		if (ServiceUtils.isServiceWithPendingRequest(service, contract,
				outParamsCtrSrv)) {
			String msg = new String("Service with SNCODE = " + snCode
					+ " of contract with CO_ID = " + coId
					+ " has pending request");
			logger.error(msg);
			throw new CMSPendingStateException(msg);
		}

		HashMap<String, Object> hmService = new HashMap<String, Object>();
		hmService.put("COS_PENDING_STATUS",
				ServicePendingStatus.ACTIVE.getValue());
		hmService.put("PROFILE_ID", new Long(0));
		hmService.put("SNCODE", snCode);

		inParams = new HashMap<String, Object>();
		inParams.put("CO_ID", coId);
		inParams.put("services", new HashMap[] { hmService });

		soiComm.executeCommand(SoiUtils.CMD_CONTRACT_SERVICES_WRITE, inParams);
		if (ServiceUtils.CheckServiceWithParameters(service, contract, null,
				soiComm, msgProperties, servicesEx, status, Services)) {
			ServiceUtils.modifyServiceParameters(service, false, coId, soiComm,
					servicesEx);
		}
		result = true;

		logger.debug("<= activateServiceInContract");
		return result;

	}

	// <******* Updated, A.ESSA ReactivateService
/***
** DI 6717 FTAT delete bloc
	public static bNewServicesEx getServiceParamValueExtends(
			bNewServicesEx srvEx, bNewServices srv) throws FaultWS {
		try {
			List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
			for (Iterator ItPrm = srv.getBNewParamValue().iterator(); ItPrm
					.hasNext();) {
				bNewParamValue bNewParamValue = (bNewParamValue) ItPrm.next();
				bNewParamValueEx pRm = new bNewParamValueEx();
				pRm.setPRM_ID(bNewParamValue.getPRM_ID());
				List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();
				for (Iterator ItValt = bNewParamValue.getbNewValues()
						.iterator(); ItValt.hasNext();) {
					bNewValues bNewValues = (bNewValues) ItValt.next();
					bNewValuesEx value = new bNewValuesEx();
					value.setVALUE_DES(bNewValues.getVALUE_DES());
					BNewValuesEx.add(value);
				}
				pRm.setBNewValueEx(BNewValuesEx);
				BNewParamValueEx.add(pRm);

			}
			srvEx.setSNCODE(srv.getSNCODE());
			srvEx.setSPCODE(srv.getSPCODE());
			srvEx.setBNewParamValueEx(BNewParamValueEx);

		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error("ServiceUtils : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}
		return srvEx;
	}

	public static boolean addServiceInContract(ServiceUtils service,
			Contract contract, boolean isServiceToBeActiveNow,
			SOIService soiComm, bNewServicesEx servicesEx,
			Properties msgProperties, List<bNewServices> Services,
			Boolean onhold) throws CMSException, FaultWS,
			CMSPendingStateException {
		boolean result = false;

		logger.debug("=> addServiceInContract()");
		Long coId = contract.getCoId();
		HashMap<String, Comparable> hmService = new HashMap<String, Comparable>();
		hmService.put("PROFILE_ID", new Long(0));
		Long snCode = service.getSnCode();
		hmService.put("SNCODE", snCode);
		Long spCode = service.getSpCode();
		Long rpCode = contract.getRpCode();
		hmService.put("SPCODE", spCode);
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("CO_ID", coId);
		inParams.put("services", new HashMap[] { hmService });
		soiComm.executeCommand("CONTRACT_SERVICES.ADD", inParams);
		result = true;

		// check if default Servcie Parameter Value Match With Service Parameter
		// Value in Input
		if (ServiceUtils.CheckServiceWithParameters(service, contract, null,
				soiComm, msgProperties, servicesEx,
				SoiUtils.SERVICE_STATUT_UNDEFINED, Services)) {
			// initialize Service Parameters To Default
			ServiceUtils.initializeServiceParametersToDefault(service, coId,
					soiComm, servicesEx);
			// Add the Service Parameters
			ServiceUtils.modifyServiceParameters(service, false, coId, soiComm,
					servicesEx);
		}
		// if result of principal service is OK, Add the Dependencies services
		if (result) {
			// Activate The new Principal Service
			ServiceUtils.activateServiceInContract(service, contract, soiComm,
					onhold);
		}
		logger.debug("<= addServiceInContract()");
		return result;

	}

	public static List getContrActivServShdes(Bcontract bcontract)
			throws CMSException, FaultWS {

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		List contrActivServ = new ArrayList();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
				contrActivServ.add(serv.getSNCODE_PUB().toString());
			}

		}
		return contrActivServ;
	}

	// >******* Updated, A.ESSA FC5197
	// >******* Updated, A.ESSA FC5937
	public static HashMap getSlavesDNs(Long co_id, String shdes,
			SOIService soiComm) throws CMSException {
		HashMap inParams = new HashMap();
		inParams.put("CO_ID", co_id);
		if (shdes != null) {
			inParams.put("SHDES", shdes);
		}
		HashMap contractReadOutParams = soiComm.executeCommand(
				"DUAL_SIM_SLAVES.READ", inParams);
		HashMap[] contracts = (HashMap[]) contractReadOutParams.get("dual_sim");

		HashMap res = new HashMap();
		ArrayList coList = new ArrayList();

		if ((contracts != null) && (contracts.length > 0)) {
			String dns = "";
			for (int j = 0; j < contracts.length; j++) {
				if ("A".equals(contracts[j].get("STATUS"))) {
					dns = dns.concat(contracts[j].get("DN_NUM").toString())
							.concat(", ");
					coList.add(contracts[j].get("CO_ID"));
				}
			}
			if (coList.size() > 0) {
				res.put("DNS", dns);
				res.put("COS", coList);
				return res;
			}
		}
		return res;
	}

	// <******* Updated, A.ESSA FC5937
	public static String getSncodeFromShdes(String shdes, SOIService soiComm)
			throws CMSException {
		HashMap inParams = new HashMap();
		inParams.put("SNCODE_PUB", shdes);
		HashMap contractReadOutParams = soiComm.executeCommand("SERVICES.READ",
				inParams);
		HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
				.get("NUM_SV");

		if ((serviceToModif != null) || (serviceToModif.length != 0)) {
			return serviceToModif[0].get("SNCODE").toString();

		}
		return null;
	}

	public static String getShdesFromSncode(Long sncode, SOIService soiComm)
			throws CMSException {
		HashMap inParams = new HashMap();
		inParams.put("SNCODE", sncode);
		HashMap contractReadOutParams = soiComm.executeCommand("SERVICES.READ",
				inParams);
		HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
				.get("NUM_SV");

		if ((serviceToModif != null) || (serviceToModif.length != 0)) {
			return serviceToModif[0].get("SNCODE_PUB").toString();

		}
		return null;
	}

	// <******* Updated, A.ESSA FC5197
	// >******* Updated, A.ESSA FC5937
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static boolean searchContractMaster4G(String msisdn,
			SOIService soiComm) throws CMSException, FaultWS {

		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("USE_INSTALLATION_ADDRESS", Boolean.FALSE);
		inParams.put("DIRNUM", msisdn);

		inParams.put("SEARCHER", "ContractSearchWithoutHistory");

		HashMap<String, Object> outParams = soiComm.executeCommand(
				SoiUtils.CMD_CONTRACTS_SEARCH, inParams);

		HashMap[] contracts = (HashMap[]) outParams.get("contracts");
		if ((contracts == null) || (contracts.length == 0)
				|| contracts[0].get("CO_ID") == null) {
			return false;
		} else {
			Long co_id = null;
			int nContract = 0;
			boolean notFound = false;
			Long deactiveCo_id = null;
			while ((co_id == null) && (nContract < contracts.length)) {
				Integer coStatus = (Integer) contracts[nContract]
						.get("CO_STATUS");
				if (coStatus.toString().equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
					co_id = (Long) contracts[nContract].get("CO_ID");

					if (co_id == null) {
						notFound = true; // throw new
											// FaultWS(Constants.ERROR_CODE.MT_CONTRACTNOTFOUND,msgProperties);
					}
				} else {
					deactiveCo_id = (Long) contracts[nContract].get("CO_ID");
					nContract += 1;
				}
			}

			if (deactiveCo_id != null) {
				return false;
			}

			if (notFound) {
				return false;
			}

			inParams = new HashMap<String, Object>();
			inParams.put("CO_ID", co_id.longValue());
			inParams.put("SYNC_WITH_DB", true);

			outParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ,
					inParams);
			Bcontract bcontract = BContractAdapter
					.getMainContractInfo(outParams);

			Integer coStatus = (Integer) outParams.get("CO_STATUS");

			inParams.remove("SYNC_WITH_DB");
			outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
			BContractedServicesAdapter.buildContractedServicesBean(outParams,
					bcontract);

			Long tmcode = bcontract.getRPCODE();
			List contrActivServ = ServiceUtils
					.getContrActivServShdes(bcontract);

			if (contrActivServ.contains("IN4FA")
					|| contrActivServ.contains("INT4G")
					|| contrActivServ.contains("INP4G")) {
				return true;
			}
		}

		return false;
	}
*/
	// <******* Updated, A.ESSA FC5937

	public static boolean existing_Auto_Services_with_params(
			BcontractAll contractmodel, String sncode, String prmID,
			String des, SOIService soiComm, List<bNewServices> Services,
			String status) throws CMSException, FaultWS {
		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		if ((csm == null) || (csm.size() == 0))
			return false;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString().equals(status)
					&& bContractedServices.getSNCODE_PUB() != null
					&& bContractedServices.getSNCODE() == (new Long(sncode))
							.longValue()) {
				String value_des = ServiceParamAdapter
						.GetValueDesServicefromParametersId(sncode,
								contractmodel, soiComm, prmID);
				if (value_des != null && value_des.equals(des)) {
					found = true;
				}
			}
		}

		bNewServicesEx SrvEx = null;
		if (Services != null) {
			for (Iterator it = Services.iterator(); it.hasNext();) {
				bNewServices Srv = new bNewServices();
				SrvEx = new bNewServicesEx();
				Srv = (bNewServices) it.next();
				Long snCode = Srv.getSNCODE();
				SrvEx = ServiceUtils.getServiceParamValueExtends(SrvEx, Srv);

				for (Iterator ItPrm = SrvEx.getBNewParamValueEx().iterator(); ItPrm
						.hasNext();) {
					bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();

					for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
						bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
						if (vaLue.getVALUE_DES() != null) {
							if (prmID.equals((new Long(pRmVal.getPRM_ID()))
									.toString())) {
								if (des.equals(vaLue.getVALUE_DES())) {
									return true;
								} else {
									return false;
								}

							}
						}
					}
				}
			}
		}
		return found;
	}

	public static List getContrActivOnholdSuspendedServ(Bcontract bcontract)
			throws CMSException, FaultWS {

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		List contrActivServ = new ArrayList();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (!serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_DEACTIVE)) {
				contrActivServ.add(Long.valueOf(serv.getSNCODE()).toString());
			}

		}
		return contrActivServ;
	}

	public static List getContrToActivateServ(Bcontract bcontract,
			List<bNewServices> ServicesToAdd) throws CMSException, FaultWS {

		List contrActivServ = new ArrayList();
		for (int i = 0; i < ServicesToAdd.size(); i++) {
			bNewServices serv = ServicesToAdd.get(i);
			contrActivServ.add(Long.valueOf(serv.getSNCODE()).toString());
		}

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (!serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_DEACTIVE)) {
				contrActivServ.add(Long.valueOf(serv.getSNCODE()).toString());
			}

		}
		return contrActivServ;
	}

	public static HashMap[] getPriority(SOIService soiComm) throws Exception {

		List srvPriority = Tree.getTree().getSRVPriority();
		if (srvPriority.size() > 0) {
			return (HashMap[]) srvPriority.get(0);
		}
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		HashMap<String, Object> outParams = soiComm.executeCommand(
				SoiUtils.CMD_CHECK_PRIORITY_READ, inParams);
		HashMap[] deps = (HashMap[]) outParams.get("CheckPriority_LIST");
		srvPriority.add(deps);
		return deps;
	}

	public static HashMap[] getServDeps(SOIService soiComm) throws Exception {

		List srvDeps = Tree.getTree().getSRVDeps();
		if (srvDeps.size() > 0) {
			return (HashMap[]) srvDeps.get(0);
		}
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		HashMap<String, Object> outParams = soiComm.executeCommand(
				SoiUtils.CMD_CHECK_DEPENDENCY_READ, inParams);
		HashMap[] deps = (HashMap[]) outParams.get("CheckDependency_LIST");
		srvDeps.add(deps);
		return deps;

	}

	public static bNewServicesEx change_fill_Auto_MZ_params(SOIService soiComm,
			Contract contract, Properties msgProperties,
			HashMap<String, Object> contractReadOutParams) throws FaultWS,
			CMSException {
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		long co_id = contract.getCoId();
		BcontractAll contractmodel = null;

		contractmodel = BContractAdapterAll
				.getMainContractInfo(contractReadOutParams);
		BContractedServicesAdapterAll.buildContractedServicesBean(
				contractReadOutParams, contractmodel);

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_SEMA_PROFIL_SDP
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		bNewParamValueEx pRmCo = null;

		bNewParamValueEx pRmServices = null;

		Long mzSncode = Long.parseLong(ServiceUtils.getSncodeFromShdes("WVG",
				soiComm));

		if ((csm == null) || (csm.size() == 0))
			return null;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ON_HOLD)) {

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					// if(ParamConfigArray[ssp].getPrm()!=null){
					if (ParamConfigArray[ssp].getTmcode() != null
							&& ParamConfigArray[ssp].getTmcode() > 0
							&& !ParamConfigArray[ssp]
									.getTmcode()
									.toString()
									.equals((new Long(contractmodel.getRPCODE()))
											.toString())) {
						continue;
					}
					if (mzSncode != bContractedServices.getSNCODE()) {
						continue;
					}
					if (ParamConfigArray[ssp].getSncode() != null
							&& ParamConfigArray[ssp].getSncode() > 0) {

						String prmId = " ";
						String valDes = " ";
						if (ParamConfigArray[ssp].getPrm_id() != null) {
							prmId = ParamConfigArray[ssp].getPrm_id()
									.toString();
						}
						if (ParamConfigArray[ssp].getValue_des() != null) {
							valDes = ParamConfigArray[ssp].getValue_des()
									.toString();
						}

						boolean exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getSncode().toString(), prmId, valDes,
								soiComm, null, SoiUtils.SERVICE_STATUT_ON_HOLD);

						if (!exists) {// exists = false;
							continue;
						}
					}

					try {
						pRmCo = new bNewParamValueEx();
						pRmCo.setPRM_ID(getPrmIDFromShdes("PRSDP", soiComm));
						pRmCo.setPRM_NO((long) 2);

						List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();

						bNewValuesEx value = new bNewValuesEx();
						value.setVALUE_DES(ParamConfigArray[ssp]
								.getDep_value_des().toString());
						value.setVALUE(ParamConfigArray[ssp].getDep_value_des()
								.toString());
						BNewValuesEx.add(value);
						pRmCo.setBNewValueEx(BNewValuesEx);

						bNewServicesEx srvEx = new bNewServicesEx();
						List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
						BNewParamValueEx.add(pRmCo);
						srvEx.setBNewParamValueEx(BNewParamValueEx);
						srvEx.setSNCODE(bContractedServices.getSNCODE());
						srvEx.setSPCODE(bContractedServices.getSPCODE());
						srvEx.setCO_ID(contract.getCoId());
						srvEx.setRPCODE(contract.getRpCode());
						ServiceUtils service = new ServiceUtils();

						service.setSnCode(bContractedServices.getSNCODE());
						service.setSpCode(bContractedServices.getSPCODE());

						if (ServiceUtils.CheckServiceWithParameters(service,
								contract, null, soiComm, msgProperties, srvEx,
								SoiUtils.SERVICE_STATUT_ON_HOLD, null)) {
							ServiceUtils.modifyServiceParameters(service,
									false, co_id, soiComm, srvEx);

						} else {
							throw new FaultWS(
									srvEx,
									Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_INVALID_CFG,
									msgProperties);
						}

						return srvEx;

					} catch (Exception e) {
						if (e instanceof FaultWS)
							throw (FaultWS) e;
						logger.error("ServiceUtils : Exception "
								+ e.getMessage());
						e.printStackTrace();
						throw new FaultWS("RC1000", "Internal Error");
					}

					// }

				}

			}

		}

		return null;
	}

	public static boolean change_fill_Auto_params(SOIService soiComm,
			Contract contract, Properties msgProperties,
			HashMap<String, Object> contractReadOutParams) throws FaultWS,
			CMSException {
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		long co_id = contract.getCoId();
		BcontractAll contractmodel = null;

		contractmodel = BContractAdapterAll
				.getMainContractInfo(contractReadOutParams);
		BContractedServicesAdapterAll.buildContractedServicesBean(
				contractReadOutParams, contractmodel);

		/*
		 * try{ if (co_id!=0) { inParams.put("CO_ID", new
		 * Long(co_id).longValue()); inParams.put("SYNC_WITH_DB",true); }
		 * if(contractmodel == null){ contractmodel = new BcontractAll();
		 * HashMap<String, Object > outParams =
		 * soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParams);
		 * inParams.remove("SYNC_WITH_DB"); contractmodel=
		 * BContractAdapterAll.getMainContractInfo(outParams);
		 * BContractedServicesAdapterAll.buildContractedServicesBean(outParams,
		 * contractmodel); contract.setRpCode((Long)outParams.get("RPCODE")); }
		 * 
		 * } catch (Exception e) { if (e instanceof FaultWS) throw (FaultWS) e;
		 * logger.error("ServiceUtils : Exception " + e.getMessage());
		 * e.printStackTrace(); throw new FaultWS("RC1000", "Internal Error"); }
		 */

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_IAM_AUTO_DEP_SRV_PRM
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		bNewParamValueEx pRmCo = null;

		bNewParamValueEx pRmServices = null;

		if ((csm == null) || (csm.size() == 0))
			return false;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ON_HOLD)) {

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					if (ParamConfigArray[ssp].getPrm() != null) {
						if (ParamConfigArray[ssp].getTmcode() != null
								&& ParamConfigArray[ssp].getTmcode() > 0
								&& !ParamConfigArray[ssp]
										.getTmcode()
										.toString()
										.equals((new Long(contractmodel
												.getRPCODE())).toString())) {
							continue;
						}
						if (ParamConfigArray[ssp].getDep_sncode() != null
								&& ParamConfigArray[ssp].getDep_sncode() > 0
								&& ParamConfigArray[ssp].getDep_sncode()
										.longValue() != bContractedServices
										.getSNCODE()) {
							continue;
						}

						String prmId = " ";
						String valDes = " ";
						if (ParamConfigArray[ssp].getPrm_id() != null) {
							prmId = ParamConfigArray[ssp].getPrm_id()
									.toString();
						}
						if (ParamConfigArray[ssp].getValue_des() != null) {
							valDes = ParamConfigArray[ssp].getValue_des()
									.toString();
						}

						boolean exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getSncode().toString(), prmId, valDes,
								soiComm, null, SoiUtils.SERVICE_STATUT_ON_HOLD);

						if (!exists) {// exists = false;
							continue;
						}

						exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getDep_sncode().toString(),
								ParamConfigArray[ssp].getDep_prm_id()
										.toString(),
								ParamConfigArray[ssp].getDep_value_des(),
								soiComm, null, SoiUtils.SERVICE_STATUT_ON_HOLD);

						if (exists)
							return false;

						try {
							pRmCo = new bNewParamValueEx();
							pRmCo.setPRM_ID(ParamConfigArray[ssp]
									.getDep_prm_id());
							pRmCo.setPRM_NO(ParamConfigArray[ssp]
									.getDep_prm_no());

							List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();

							bNewValuesEx value = new bNewValuesEx();
							value.setVALUE_DES(ParamConfigArray[ssp]
									.getDep_value_des().toString());
							value.setVALUE(ParamConfigArray[ssp].getDep_value()
									.toString());
							BNewValuesEx.add(value);
							pRmCo.setBNewValueEx(BNewValuesEx);

							bNewServicesEx srvEx = new bNewServicesEx();
							List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
							BNewParamValueEx.add(pRmCo);
							srvEx.setBNewParamValueEx(BNewParamValueEx);
							srvEx.setSNCODE(ParamConfigArray[ssp]
									.getDep_sncode());
							srvEx.setSPCODE(bContractedServices.getSPCODE());
							srvEx.setCO_ID(contract.getCoId());
							srvEx.setRPCODE(contract.getRpCode());
							ServiceUtils service = new ServiceUtils();

							service.setSnCode(ParamConfigArray[ssp]
									.getDep_sncode());
							service.setSpCode(bContractedServices.getSPCODE());

							if (ServiceUtils.CheckServiceWithParameters(
									service, contract, null, soiComm,
									msgProperties, srvEx,
									SoiUtils.SERVICE_STATUT_ON_HOLD, null)) {
								ServiceUtils.modifyServiceParameters(service,
										false, co_id, soiComm, srvEx);

							} else {
								throw new FaultWS(
										srvEx,
										Constants.ERROR_CODE.MT_WS_CHANGESVCPRM_INVALID_CFG,
										msgProperties);
							}

							return true;

						} catch (Exception e) {
							if (e instanceof FaultWS)
								throw (FaultWS) e;
							logger.error("ServiceUtils : Exception "
									+ e.getMessage());
							e.printStackTrace();
							throw new FaultWS("RC1000", "Internal Error");
						}

					}

				}

			}

		}

		return false;
	}

	public static bNewServicesEx fill_Auto_MZ_params(SOIService soiComm,
			bNewServicesEx srvEx, Contract contract, List<bNewServices> Services)
			throws FaultWS, CMSException {
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		long co_id = contract.getCoId();
		BcontractAll contractmodel = null;

		Long prmSDP_id = getPrmIDFromShdes("PRSDP", soiComm);

		Long mzSncode = Long.parseLong(ServiceUtils.getSncodeFromShdes("WVG",
				soiComm));
		try {
			if (co_id != 0) {
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("SYNC_WITH_DB", true);
			}
			if (contractmodel == null) {
				contractmodel = new BcontractAll();
				HashMap<String, Object> outParams = soiComm.executeCommand(
						SoiUtils.CMD_CONTRACT_READ, inParams);
				inParams.remove("SYNC_WITH_DB");
				contractmodel = BContractAdapterAll
						.getMainContractInfo(outParams);
				BContractedServicesAdapterAll.buildContractedServicesBean(
						outParams, contractmodel);
			}

		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error("ServiceUtils : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_SEMA_PROFIL_SDP
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		List<bNewParamValueEx> BNewParamValueEx = srvEx.getBNewParamValueEx();

		int k = -1;

		for (int ItPrm = 0; ItPrm < BNewParamValueEx.size(); ItPrm++) {

			bNewParamValueEx pRmVal = (bNewParamValueEx) BNewParamValueEx
					.get(ItPrm);
			if (pRmVal.getPRM_ID() == prmSDP_id.longValue()) {
				k = ItPrm;
				break;
			}

		}

		if (k > 0) {
			BNewParamValueEx.remove(k);
		}

		bNewParamValueEx pRmCo = null;

		bNewParamValueEx pRmServices = null;

		if ((csm == null) || (csm.size() == 0))
			return srvEx;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					// if(ParamConfigArray[ssp].getPrm()!=null){
					if (ParamConfigArray[ssp].getTmcode() != null
							&& ParamConfigArray[ssp].getTmcode() > 0
							&& !ParamConfigArray[ssp]
									.getTmcode()
									.toString()
									.equals((new Long(contractmodel.getRPCODE()))
											.toString())) {
						continue;
					}
					if (mzSncode != bContractedServices.getSNCODE()) {
						continue;
					}
					if (ParamConfigArray[ssp].getSncode() != null
							&& ParamConfigArray[ssp].getSncode() > 0) {

						String prmId = " ";
						String valDes = " ";
						if (ParamConfigArray[ssp].getPrm_id() != null) {
							prmId = ParamConfigArray[ssp].getPrm_id()
									.toString();
						}
						if (ParamConfigArray[ssp].getValue_des() != null) {
							valDes = ParamConfigArray[ssp].getValue_des()
									.toString();
						}

						boolean exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getDep_sncode().toString(), prmId,
								valDes, soiComm, null,
								SoiUtils.SERVICE_STATUT_ACTIVE);

						if (!exists) {// exists = false;
							continue;
						}
					}

					try {
						pRmCo = new bNewParamValueEx();
						pRmCo.setPRM_ID(prmSDP_id);
						pRmCo.setPRM_NO((long) 2);

						List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();

						bNewValuesEx value = new bNewValuesEx();
						value.setVALUE_DES(ParamConfigArray[ssp]
								.getDep_value_des().toString());
						value.setVALUE(ParamConfigArray[ssp].getDep_value_des()
								.toString());
						BNewValuesEx.add(value);
						pRmCo.setBNewValueEx(BNewValuesEx);

					} catch (Exception e) {
						if (e instanceof FaultWS)
							throw (FaultWS) e;
						logger.error("ServiceUtils : Exception "
								+ e.getMessage());
						e.printStackTrace();
						throw new FaultWS("RC1000", "Internal Error");
					}

					// }

				}

			}

		}

		bNewServicesEx SrvEx = null;
		if (Services != null) {
			for (Iterator it = Services.iterator(); it.hasNext();) {
				bNewServices Srv = new bNewServices();
				SrvEx = new bNewServicesEx();
				Srv = (bNewServices) it.next();
				Long snCode = Srv.getSNCODE();
				SrvEx = ServiceUtils.getServiceParamValueExtends(SrvEx, Srv);

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					// if(ParamConfigArray[ssp].getPrm()!=null){
					if (ParamConfigArray[ssp].getTmcode() != null
							&& ParamConfigArray[ssp].getTmcode() > 0
							&& !ParamConfigArray[ssp]
									.getTmcode()
									.toString()
									.equals((new Long(contractmodel.getRPCODE()))
											.toString())) {
						continue;
					}
					if (!mzSncode.equals(snCode)) {
						continue;
					}
					if (ParamConfigArray[ssp].getSncode() != null
							&& ParamConfigArray[ssp].getSncode() > 0) {

						String prmId = " ";
						String valDes = " ";
						if (ParamConfigArray[ssp].getPrm_id() != null) {
							prmId = ParamConfigArray[ssp].getPrm_id()
									.toString();
						}
						if (ParamConfigArray[ssp].getValue_des() != null) {
							valDes = ParamConfigArray[ssp].getValue_des()
									.toString();
						}

						boolean exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getSncode().toString(), prmId, valDes,
								soiComm, Services,
								SoiUtils.SERVICE_STATUT_ACTIVE);

						if (!exists) {// exists = false;
							continue;
						}
					}

					try {
						pRmServices = new bNewParamValueEx();
						pRmServices.setPRM_ID(prmSDP_id);
						pRmServices.setPRM_NO((long) 2);

						List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();

						bNewValuesEx value = new bNewValuesEx();
						value.setVALUE_DES(ParamConfigArray[ssp]
								.getDep_value_des().toString());
						value.setVALUE(ParamConfigArray[ssp].getDep_value_des()
								.toString());
						BNewValuesEx.add(value);
						pRmServices.setBNewValueEx(BNewValuesEx);

					} catch (Exception e) {
						if (e instanceof FaultWS)
							throw (FaultWS) e;
						logger.error("ServiceUtils : Exception "
								+ e.getMessage());
						e.printStackTrace();
						throw new FaultWS("RC1000", "Internal Error");
					}

					// }

				}

			}

		}
		if (mzSncode.longValue() == srvEx.getSNCODE()) {
			if (pRmServices != null)
				BNewParamValueEx.add(pRmServices);
			else if (pRmCo != null) {
				BNewParamValueEx.add(pRmCo);
			}
/***
** DI 6717 FTAT delete bloc
			int k = 0;

			for (int ItPrm = 0; ItPrm < BNewParamValueEx.size(); ItPrm++) {

				bNewParamValueEx pRmVal = (bNewParamValueEx) BNewParamValueEx
						.get(ItPrm);
				if (pRmVal.getPRM_ID() == prmSDP_id.longValue()) {
					k++;
				}

			}

			if (k > 1) {
				k--;
				for (int ItPrm = 0; ItPrm < BNewParamValueEx.size(); ItPrm++) {

					bNewParamValueEx pRmVal = (bNewParamValueEx) BNewParamValueEx
							.get(ItPrm);
					if (pRmVal.getPRM_ID() == prmSDP_id.longValue() && k > 0) {

						BNewParamValueEx.set(ItPrm, null);
						k--;
					}

				}

				k = 0;
				while (k < BNewParamValueEx.size()) {
					if (BNewParamValueEx.get(k) == null) {
						BNewParamValueEx.remove(k);
					} else
						k++;
				}

			}
*/
			srvEx.setBNewParamValueEx(BNewParamValueEx);
		}

		return srvEx;
	}

	public static bNewServicesEx fill_Auto_params(SOIService soiComm,
			bNewServicesEx srvEx, Contract contract, List<bNewServices> Services)
			throws FaultWS, CMSException {
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		long co_id = contract.getCoId();
		BcontractAll contractmodel = null;

		try {
			if (co_id != 0) {
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("SYNC_WITH_DB", true);
			}
			if (contractmodel == null) {
				contractmodel = new BcontractAll();
				HashMap<String, Object> outParams = soiComm.executeCommand(
						SoiUtils.CMD_CONTRACT_READ, inParams);
				inParams.remove("SYNC_WITH_DB");
				contractmodel = BContractAdapterAll
						.getMainContractInfo(outParams);
				BContractedServicesAdapterAll.buildContractedServicesBean(
						outParams, contractmodel);
			}

		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error("ServiceUtils : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_IAM_AUTO_DEP_SRV_PRM
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		List<bNewParamValueEx> BNewParamValueEx = srvEx.getBNewParamValueEx();

		bNewParamValueEx pRmCo = null;

		bNewParamValueEx pRmServices = null;

		if ((csm == null) || (csm.size() == 0))
			return srvEx;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					if (ParamConfigArray[ssp].getPrm() != null) {
						if (ParamConfigArray[ssp].getTmcode() != null
								&& ParamConfigArray[ssp].getTmcode() > 0
								&& !ParamConfigArray[ssp]
										.getTmcode()
										.toString()
										.equals((new Long(contractmodel
												.getRPCODE())).toString())) {
							continue;
						}
						if (ParamConfigArray[ssp].getDep_sncode() != null
								&& ParamConfigArray[ssp].getDep_sncode() > 0
								&& ParamConfigArray[ssp].getDep_sncode()
										.longValue() != bContractedServices
										.getSNCODE()) {
							continue;
						}

						String prmId = " ";
						String valDes = " ";
						if (ParamConfigArray[ssp].getPrm_id() != null) {
							prmId = ParamConfigArray[ssp].getPrm_id()
									.toString();
						}
						if (ParamConfigArray[ssp].getValue_des() != null) {
							valDes = ParamConfigArray[ssp].getValue_des()
									.toString();
						}

						boolean exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getDep_sncode().toString(), prmId,
								valDes, soiComm, null,
								SoiUtils.SERVICE_STATUT_ACTIVE);

						if (!exists) {// exists = false;
							continue;
						}

						try {
							pRmCo = new bNewParamValueEx();
							pRmCo.setPRM_ID(ParamConfigArray[ssp]
									.getDep_prm_id());
							pRmCo.setPRM_NO(ParamConfigArray[ssp]
									.getDep_prm_no());

							List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();

							bNewValuesEx value = new bNewValuesEx();
							value.setVALUE_DES(ParamConfigArray[ssp]
									.getDep_value_des().toString());
							value.setVALUE(ParamConfigArray[ssp].getDep_value()
									.toString());
							BNewValuesEx.add(value);
							pRmCo.setBNewValueEx(BNewValuesEx);

						} catch (Exception e) {
							if (e instanceof FaultWS)
								throw (FaultWS) e;
							logger.error("ServiceUtils : Exception "
									+ e.getMessage());
							e.printStackTrace();
							throw new FaultWS("RC1000", "Internal Error");
						}

					}

				}

			}

		}

		bNewServicesEx SrvEx = null;
		if (Services != null) {
			for (Iterator it = Services.iterator(); it.hasNext();) {
				bNewServices Srv = new bNewServices();
				SrvEx = new bNewServicesEx();
				Srv = (bNewServices) it.next();
				Long snCode = Srv.getSNCODE();
				SrvEx = ServiceUtils.getServiceParamValueExtends(SrvEx, Srv);

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					if (ParamConfigArray[ssp].getPrm() != null) {
						if (ParamConfigArray[ssp].getTmcode() != null
								&& ParamConfigArray[ssp].getTmcode() > 0
								&& !ParamConfigArray[ssp]
										.getTmcode()
										.toString()
										.equals((new Long(contractmodel
												.getRPCODE())).toString())) {
							continue;
						}
						if (ParamConfigArray[ssp].getDep_sncode() != null
								&& ParamConfigArray[ssp].getDep_sncode() > 0
								&& ParamConfigArray[ssp].getDep_sncode()
										.longValue() != snCode.longValue()) {
							continue;
						}

						String prmId = " ";
						String valDes = " ";
						if (ParamConfigArray[ssp].getPrm_id() != null) {
							prmId = ParamConfigArray[ssp].getPrm_id()
									.toString();
						}
						if (ParamConfigArray[ssp].getValue_des() != null) {
							valDes = ParamConfigArray[ssp].getValue_des()
									.toString();
						}

						boolean exists = existing_Auto_Services_with_params(
								contractmodel, ParamConfigArray[ssp]
										.getDep_sncode().toString(), prmId,
								valDes, soiComm, Services,
								SoiUtils.SERVICE_STATUT_ACTIVE);

						if (!exists) {// exists = false;
							continue;
						}

						try {
							pRmServices = new bNewParamValueEx();
							pRmServices.setPRM_ID(ParamConfigArray[ssp]
									.getDep_prm_id());
							pRmServices.setPRM_NO(ParamConfigArray[ssp]
									.getDep_prm_no());

							List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();

							bNewValuesEx value = new bNewValuesEx();
							value.setVALUE_DES(ParamConfigArray[ssp]
									.getDep_value_des().toString());
							value.setVALUE(ParamConfigArray[ssp].getDep_value()
									.toString());
							BNewValuesEx.add(value);
							pRmServices.setBNewValueEx(BNewValuesEx);

						} catch (Exception e) {
							if (e instanceof FaultWS)
								throw (FaultWS) e;
							logger.error("ServiceUtils : Exception "
									+ e.getMessage());
							e.printStackTrace();
							throw new FaultWS("RC1000", "Internal Error");
						}

					}

				}

			}

		}
		if (pRmServices != null)
			BNewParamValueEx.add(pRmServices);
		else if (pRmCo != null) {
			BNewParamValueEx.add(pRmCo);
		}
		srvEx.setBNewParamValueEx(BNewParamValueEx);
		return srvEx;
	}

	public static List<ParamValueBeanPub> remove_Auto_params2(
			SOIService soiComm, List<ParamValueBeanPub> Services, long sncode)
			throws FaultWS, CMSException {
		HashMap<String, Object> inParams = new HashMap<String, Object>();

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_IAM_AUTO_DEP_SRV_PRM
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		List<ParamValueBeanPub> tmp = new ArrayList<ParamValueBeanPub>();
		boolean toShow = false;
		for (Iterator ItPrm = Services.iterator(); ItPrm.hasNext();) {
			ParamValueBeanPub bNewParamValue = (ParamValueBeanPub) ItPrm.next();

			toShow = true;
			for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
				if (ParamConfigArray[ssp].getPrm() != null) {

					if (ParamConfigArray[ssp].getDep_sncode() != null
							&& ParamConfigArray[ssp].getDep_sncode() > 0
							&& ParamConfigArray[ssp].getDep_sncode()
									.longValue() != sncode) {
						continue;
					}
					if (ParamConfigArray[ssp].getDep_prm_id() != null
							&& ParamConfigArray[ssp].getDep_prm_id() > 0
							&& ParamConfigArray[ssp].getDep_prm_id()
									.longValue() != bNewParamValue.getPrm_id()) {
						continue;
					}
					toShow = false;
					break;

				}
			}
			if (toShow && !"PRSDP".equals(bNewParamValue.getPrm_id_pub()))
				tmp.add(bNewParamValue);

		}

		return tmp;
	}

	public static List<bNewServicesEx> remove_Auto_params(SOIService soiComm,
			List<bNewServicesEx> Services) throws FaultWS, CMSException {
		HashMap<String, Object> inParams = new HashMap<String, Object>();

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_IAM_AUTO_DEP_SRV_PRM
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		bNewServicesEx SrvEx = null;
		if (Services != null) {
			for (Iterator it = Services.iterator(); it.hasNext();) {
				bNewServicesEx Srv = new bNewServicesEx();
				Srv = (bNewServicesEx) it.next();
				Long snCode = Srv.getSNCODE();

				for (int ssp = 0; ssp < ParamConfigArray.length; ssp++) {
					if (ParamConfigArray[ssp].getPrm() != null) {

						if (ParamConfigArray[ssp].getDep_sncode() != null
								&& ParamConfigArray[ssp].getDep_sncode() > 0
								&& ParamConfigArray[ssp].getDep_sncode()
										.longValue() != snCode.longValue()) {
							continue;
						}

						List<bNewParamValueEx> tmp = new ArrayList<bNewParamValueEx>();

						for (Iterator ItPrm = Srv.getBNewParamValueEx()
								.iterator(); ItPrm.hasNext();) {
							bNewParamValueEx bNewParamValue = (bNewParamValueEx) ItPrm
									.next();

							if (ParamConfigArray[ssp].getDep_prm_id() != null
									&& ParamConfigArray[ssp].getDep_prm_id() > 0
									&& ParamConfigArray[ssp].getDep_prm_id()
											.longValue() != bNewParamValue
											.getPRM_ID()) {
								tmp.add(bNewParamValue);
							}

						}
						Srv.setBNewParamValueEx(tmp);

					}
				}

				List<bNewParamValueEx> tmp = new ArrayList<bNewParamValueEx>();

				for (Iterator ItPrm = Srv.getBNewParamValueEx().iterator(); ItPrm
						.hasNext();) {
					bNewParamValueEx bNewParamValue = (bNewParamValueEx) ItPrm
							.next();

					if ((getPrmIDFromShdes("PRSDP", soiComm).longValue() != bNewParamValue
							.getPRM_ID())) {
						tmp.add(bNewParamValue);
					}

				}
				Srv.setBNewParamValueEx(tmp);

			}
		}
		return Services;
	}

	public static bNewServicesEx getServiceParamValueExtends(
			bNewServicesEx srvEx, bNewServices srv) throws FaultWS {
		try {
			List<bNewParamValueEx> BNewParamValueEx = new ArrayList<bNewParamValueEx>();
			for (Iterator ItPrm = srv.getBNewParamValue().iterator(); ItPrm
					.hasNext();) {
				bNewParamValue bNewParamValue = (bNewParamValue) ItPrm.next();
				bNewParamValueEx pRm = new bNewParamValueEx();
				pRm.setPRM_ID(bNewParamValue.getPRM_ID());
				List<bNewValuesEx> BNewValuesEx = new ArrayList<bNewValuesEx>();
				for (Iterator ItValt = bNewParamValue.getbNewValues()
						.iterator(); ItValt.hasNext();) {
					bNewValues bNewValues = (bNewValues) ItValt.next();
					bNewValuesEx value = new bNewValuesEx();
					value.setVALUE_DES(bNewValues.getVALUE_DES());
					BNewValuesEx.add(value);
				}
				pRm.setBNewValueEx(BNewValuesEx);
				BNewParamValueEx.add(pRm);

			}
			srvEx.setSNCODE(srv.getSNCODE());
			srvEx.setSPCODE(srv.getSPCODE());
			srvEx.setBNewParamValueEx(BNewParamValueEx);

		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error("ServiceUtils : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}
		return srvEx;
	}

	public static boolean addServiceInContract(ServiceUtils service,
			Contract contract, boolean isServiceToBeActiveNow,
			SOIService soiComm, bNewServicesEx servicesEx,
			Properties msgProperties, List<bNewServices> Services,
			Boolean onhold) throws CMSException, FaultWS,
			CMSPendingStateException {
		boolean result = false;

		logger.debug("=> addServiceInContract()");
		Long coId = contract.getCoId();
		HashMap<String, Comparable> hmService = new HashMap<String, Comparable>();
		hmService.put("PROFILE_ID", new Long(0));
		Long snCode = service.getSnCode();
		hmService.put("SNCODE", snCode);
		Long spCode = service.getSpCode();
		Long rpCode = contract.getRpCode();
		hmService.put("SPCODE", spCode);
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("CO_ID", coId);
		inParams.put("services", new HashMap[] { hmService });
		soiComm.executeCommand("CONTRACT_SERVICES.ADD", inParams);
		result = true;

		// check if default Servcie Parameter Value Match With Service Parameter
		// Value in Input
		if (ServiceUtils.CheckServiceWithParameters(service, contract, null,
				soiComm, msgProperties, servicesEx,
				SoiUtils.SERVICE_STATUT_UNDEFINED, Services)) {
			// initialize Service Parameters To Default
			ServiceUtils.initializeServiceParametersToDefault(service, coId,
					soiComm, servicesEx);
			// Add the Service Parameters
			ServiceUtils.modifyServiceParameters(service, false, coId, soiComm,
					servicesEx);
		}
		// if result of principal service is OK, Add the Dependencies services
		if (result) {
			// Activate The new Principal Service
			ServiceUtils.activateServiceInContract(service, contract, soiComm,
					onhold);
		}
		logger.debug("<= addServiceInContract()");
		return result;

	}

	public static List getContrActivServShdes(Bcontract bcontract)
			throws CMSException, FaultWS {

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		List contrActivServ = new ArrayList();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
				contrActivServ.add(serv.getSNCODE_PUB().toString());
			}

		}
		return contrActivServ;
	}

	// >******* Updated, A.ESSA FC5197
	// >******* Updated, A.ESSA FC5937
	public static HashMap getSlavesDNs(Long co_id, String shdes,
			SOIService soiComm) throws CMSException {
		HashMap inParams = new HashMap();
		inParams.put("CO_ID", co_id);
		if (shdes != null) {
			inParams.put("SHDES", shdes);
		}
		HashMap contractReadOutParams = soiComm.executeCommand(
				"DUAL_SIM_SLAVES.READ", inParams);
		HashMap[] contracts = (HashMap[]) contractReadOutParams.get("dual_sim");

		HashMap res = new HashMap();
		ArrayList coList = new ArrayList();

		if ((contracts != null) && (contracts.length > 0)) {
			String dns = "";
			for (int j = 0; j < contracts.length; j++) {
				if ("A".equals(contracts[j].get("STATUS"))) {
					dns = dns.concat(contracts[j].get("DN_NUM").toString())
							.concat(", ");
					coList.add(contracts[j].get("CO_ID"));
				}
			}
			if (coList.size() > 0) {
				res.put("DNS", dns);
				res.put("COS", coList);
				return res;
			}
		}
		return res;
	}

	// <******* Updated, A.ESSA FC5937
	public static String getSncodeFromShdes(String shdes, SOIService soiComm)
			throws CMSException {
		HashMap inParams = new HashMap();
		inParams.put("SNCODE_PUB", shdes);
		HashMap contractReadOutParams = soiComm.executeCommand("SERVICES.READ",
				inParams);
		HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
				.get("NUM_SV");

		if ((serviceToModif != null) || (serviceToModif.length != 0)) {
			return serviceToModif[0].get("SNCODE").toString();

		}
		return null;
	}

	public static String getShdesFromSncode(Long sncode, SOIService soiComm)
			throws CMSException {
		HashMap inParams = new HashMap();
		inParams.put("SNCODE", sncode);
		HashMap contractReadOutParams = soiComm.executeCommand("SERVICES.READ",
				inParams);
		HashMap[] serviceToModif = (HashMap[]) contractReadOutParams
				.get("NUM_SV");

		if ((serviceToModif != null) || (serviceToModif.length != 0)) {
			return serviceToModif[0].get("SNCODE_PUB").toString();

		}
		return null;
	}

	// <******* Updated, A.ESSA FC5197
	// >******* Updated, A.ESSA FC5937
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static boolean searchContractMaster4G(String msisdn,
			SOIService soiComm) throws CMSException, FaultWS {

		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("USE_INSTALLATION_ADDRESS", Boolean.FALSE);
		inParams.put("DIRNUM", msisdn);

		inParams.put("SEARCHER", "ContractSearchWithoutHistory");

		HashMap<String, Object> outParams = soiComm.executeCommand(
				SoiUtils.CMD_CONTRACTS_SEARCH, inParams);

		HashMap[] contracts = (HashMap[]) outParams.get("contracts");
		if ((contracts == null) || (contracts.length == 0)
				|| contracts[0].get("CO_ID") == null) {
			return false;
		} else {
			Long co_id = null;
			int nContract = 0;
			boolean notFound = false;
			Long deactiveCo_id = null;
			while ((co_id == null) && (nContract < contracts.length)) {
				Integer coStatus = (Integer) contracts[nContract]
						.get("CO_STATUS");
				if (coStatus.toString().equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
					co_id = (Long) contracts[nContract].get("CO_ID");

					if (co_id == null) {
						notFound = true; // throw new
											// FaultWS(Constants.ERROR_CODE.MT_CONTRACTNOTFOUND,msgProperties);
					}
				} else {
					deactiveCo_id = (Long) contracts[nContract].get("CO_ID");
					nContract += 1;
				}
			}

			if (deactiveCo_id != null) {
				return false;
			}

			if (notFound) {
				return false;
			}

			inParams = new HashMap<String, Object>();
			inParams.put("CO_ID", co_id.longValue());
			inParams.put("SYNC_WITH_DB", true);

			outParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ,
					inParams);
			Bcontract bcontract = BContractAdapter
					.getMainContractInfo(outParams);

			Integer coStatus = (Integer) outParams.get("CO_STATUS");

			inParams.remove("SYNC_WITH_DB");
			outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
			BContractedServicesAdapter.buildContractedServicesBean(outParams,
					bcontract);

			Long tmcode = bcontract.getRPCODE();
			List contrActivServ = ServiceUtils
					.getContrActivServShdes(bcontract);

			if (contrActivServ.contains("IN4FA")
					|| contrActivServ.contains("INT4G")
					|| contrActivServ.contains("INP4G")) {
				return true;
			}
		}

		return false;
	}
	// <******* Updated, A.ESSA FC5937
}