package ma.iam.pws.soi.service.util;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapterAll;
import ma.iam.pws.soi.adapter.BContractedServicesAdapterAll;
import ma.iam.pws.soi.adapter.BuserAdapter;
import ma.iam.pws.soi.adapter.ServiceParamAdapter;
import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.beans.contract.BContractedServicesAll;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.services.AluTmchangeServices;
import ma.iam.pws.soi.beans.services.ClassServiceBean;
import ma.iam.pws.soi.beans.services.ConfServiceBean;
import ma.iam.pws.soi.beans.services.ConfServiceBeanPub;
import ma.iam.pws.soi.beans.services.DebitConfBean;
import ma.iam.pws.soi.beans.services.DepSrvPrmBean;
import ma.iam.pws.soi.beans.services.ParamValueBean;
import ma.iam.pws.soi.beans.services.ParamValueBeanPub;
import ma.iam.pws.soi.beans.services.PrmHideConfBean;
import ma.iam.pws.soi.beans.services.ValueBeanPub;
import ma.iam.pws.soi.beans.services.VoipConfBean;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.service.SOIService;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.client.CMSFactory;
import com.alcatel.ccl.client.ConnectedCMSClientPool;
import com.lhs.ccb.common.soi.ExchangeFormatFactory;
import com.lhs.ccb.common.soi.SVLDateTime;

public class SoiUtils {

	private static final Logger logger = LogManager.getLogger(SoiUtils.class
			.getName());
	/**
	 * Parameters of cms command
	 */
	public static final String PARAMETER_PERMISSION = "PERMISSION";
	public static final String PARAMETER_REQUESTED_PERMISSIONS = "REQUESTED_PERMISSIONS";
	public static final String PARAMETER_USERNAME = "USERNAME";
	public static final String PARAMETER_PASSWORD = "PASSWORD";
	public static final String PARAMETER_USER_PERMISSIONS = "USER_PERMISSIONS";
	public static final String PARAMETER_USERS = "USERS";
	public static final String PARAMETER_PASSWORD_EXPIRES = "PASSWORD_EXPIRES";
	public static final String PARAMETER_NAME = "NAME";

	/**
	 * SOI commands security layer
	 */
	public static final String CMD_USER_RIGHT_CHECK = "USER_RIGHT.CHECK";
	public static final String CMD_USERS_READ = "USER.READ";

	/**
	 * SOI commands ALCI Server
	 */
	public static final String CMD_CHECK_DEPENDENCY_READ = "CHECK_DEPENDENCY.READ";
	public static final String CMD_CHECK_PRIORITY_READ = "CHECK_PRIORITY.READ";
	public static final String CMD_CMSINTERFACE_ADD = "CMSINTERFACE.ADD";
	// >******* Update, A.ESSA DI6717
	public static final String CMD_CLOUD_CONFIG_READ = "CLOUD_CONFIG.READ";
	// <******* Update, A.ESSA DI6717
	public static final String CMD_DEBIT_CONFIG_READ = "DEBIT_CONFIG.READ";
	public static final String CMD_PARAM_CONFIG_READ = "PARAM_CONFIG.READ";
	// >******* Update, A.ESSA DI6717
	public static final String CMD_IAM_DEP_SRV_PRM_READ = "DEPSRVPRM.READ";
	public static final String CMD_IAM_AUTO_DEP_SRV_PRM_READ = "AUTODEPSRVPRM.READ";
	public static final String CMD_SEMA_PROFIL_SDP_READ = "PROFIL_SDP.READ";
	public static final String CMD_TMCHANGE_SERVICES = "TMCHANGE_SERVICES.READ";
	// <******* Update, A.ESSA DI6717
	public static final String CMD_VOIP_CONFIG_READ = "ATOS_VOIP_DEPENDENCY_SERVICES.READ";

	// >******* Updated, A.ESSA FC5937
	public static final String CMD_DUAL_SIM_SLAVES_READ = "DUAL_SIM_SLAVES.READ";
	// <******* Updated, A.ESSA FC5937

	/**
	 * SOI commands contract layer
	 */
	public static final String CMD_CONTRACT_SERVICES_READ = "CONTRACT_SERVICES.READ";
	public static final String CMD_CONTRACT_READ = "CONTRACT.READ";
	public static final String CMD_CONTRACTS_SEARCH = "CONTRACTS.SEARCH";
	public static final String CMD_CUSTOMER_READ = "CUSTOMER.READ";
	public static final String CMD_PARAMETER_READ = "PARAMETER.READ";

	public static final String CMD_RATEPLANS_READ = "RATEPLANS.READ";
	public static final String CMD_DELETEFLAG_UPDATE = "DELETEFLAG.UPDATE";

	public static final String listNotAllowedRpSrv = "GPRSO,AGPRS";

	public static final String CMD_DEPENDENCIES_READ = "DEPENDENCIES.READ";
	public static final String CMD_CONTRACT_WRITE = "CONTRACT.WRITE";
	public static final String CMD_CHECk_SOI_RESPONSE = "REASONS.READ";
	public static final String CMD_BOOKING_REQUEST_WRITE = "BOOKING_REQUEST.WRITE";
	public static final String CMD_TICKLER_NEW = "TICKLER.NEW";
	public static final String CMD_TICKLER_WRITE = "TICKLER.WRITE";
	public static final String CMD_SEND_SMS = "SEND.SMS";

	/***
	 * SOI Commands Offer
	 */
	public static final String CMD_SERVICES_PKG_READ = "SERVICE_PACKAGES.READ";
	public static final String CMD_SERVICES_READ = "SERVICES.READ";
	public static final String CMD_USER_CLASSES_READ = "USER_CLASSES.READ";
	public static final String CMD_SERVICE_CLASSES_READ = "SERVICE_CLASSES.READ";
	public static final String CMD_ALLOWED_SERVICES_READ = "ALLOWED_SERVICES.READ";
	public static final String CMD_SERVICES_PARAMETER_READ = "SERVICE_PARAMETERS.READ";
	/**
	 * SOI commands Deactivate Service
	 */
	public static final String CMD_CONTRACT_SERVICES_WRITE = "CONTRACT_SERVICES.WRITE";

	/**
	 * SOI Cache param�tres
	 */
	public static HashMap<String, String> CACHE_SERVICES = new HashMap<String, String>();
	public static HashMap<String, String> CACHE_SERVICES_PACKAGES = new HashMap<String, String>();
	public static Map<String, ConfServiceBean> CACHE_SERVICES_PARAMETERS = new HashMap<String, ConfServiceBean>();
	public static Map<Long, DebitConfBean> CACHE_DEBIT_CONFIG = new HashMap<Long, DebitConfBean>();
	// >******* Update, A.ESSA DI6717
	public static Map<Long, DebitConfBean> CACHE_CLOUD_CONFIG = new HashMap<Long, DebitConfBean>();
	public static Map<Long, DepSrvPrmBean> CACHE_IAM_DEP_SRV_PRM = new HashMap<Long, DepSrvPrmBean>();
	public static Map<Long, DepSrvPrmBean> CACHE_IAM_AUTO_DEP_SRV_PRM = new HashMap<Long, DepSrvPrmBean>();
	public static Map<Long, DepSrvPrmBean> CACHE_SEMA_PROFIL_SDP = new HashMap<Long, DepSrvPrmBean>();
	public static Map<Long, PrmHideConfBean> CACHE_PRM_HIDE_CONFIG = new HashMap<Long, PrmHideConfBean>();
	public static Map<Long, AluTmchangeServices> CACHE_TMCHANGE_SERVICES = new HashMap<Long, AluTmchangeServices>();
	public static Map<Long, VoipConfBean> CACHE_PRM_VOIP_CONFIG = new HashMap<Long, VoipConfBean>();
	public static ArrayList<String> debitConf = new ArrayList<String>();
	public static ArrayList<String> cloudConf = new ArrayList<String>();
	// <******* Update, A.ESSA DI6717
	public static List<ClassServiceBean> CACHE_SERVICES_CLASSES = new ArrayList<ClassServiceBean>();
	public static List<String> CACHE_USER_CLASSES = new ArrayList<String>();

	/**
	 * SOI SERVICE STATUT
	 */

	/** Undefined. */
	public final static String SERVICE_STATUT_UNDEFINED = "0";
	/** On hold. */
	public final static String SERVICE_STATUT_ON_HOLD = "1";
	/** Active. */
	public final static String SERVICE_STATUT_ACTIVE = "2";
	/** Suspended. */
	public final static String SERVICE_STATUT_SUSPENDED = "3";
	/** Deactive. */
	public final static String SERVICE_STATUT_DEACTIVE = "4";
	/** Invisible. */
	public final static String SERVICE_STATUT_INVISIBLE = "5";

	
	
	
	//// add command for caching
	public final static String CMD_ALLOWED_RATEPLANS_READ="ALLOWED_RATEPLANS.READ";
	
	public final static String ALLOWED_SNCODES_CACHING="ALLOWED_SNCODES_CACHING";
	public final static String ALLOWED_SERVICES_CACHING="ALLOWED_SERVICES_CACHING";
	public static Map<String, Map<Long, Map>> READ_SERVICES = new HashMap<String, Map<Long, Map>>();
	
	
	/**
	 * G CMS properties
	 */
	// public static Properties PropertiesUtil = PropertiesUtil.get;
	/**
	 * CMS Pool Connection
	 */
	public static ConnectedCMSClientPool cmsClientPool = null;
	/**
	 * Last use
	 */
	public static Date lastUsage = new Date();

	/**
	 * Calculate a last usage
	 */
	public static boolean isOldConnection() {
		Date currentdate = new Date();
		Long pExpConnection = Long.parseLong(PropertiesUtil
				.getProperty("cms.idle.period")) * 1000;

		if ((currentdate.getTime() - lastUsage.getTime()) > pExpConnection) {
			return true;
		}
		return false;
	}

	public static HashMap<String, Object> getUserRightParameters(
			String username, String password) {
		HashMap<String, Object> buildParameter = new HashMap<String, Object>();
		HashMap[] paramValues = new HashMap[1];
		paramValues[0] = new HashMap();
		paramValues[0].put(PARAMETER_PERMISSION, "");
		buildParameter.put(PARAMETER_REQUESTED_PERMISSIONS, paramValues);
		buildParameter.put(PARAMETER_USERNAME, username);
		buildParameter.put(PARAMETER_PASSWORD, password);
		return buildParameter;
	}

	/**
	 * Create a pool connection
	 * 
	 * @throws ServletException
	 */
	public static void initializePool() throws ServletException {
		/* get time to wait */
		Long timeToWait = Long.parseLong(PropertiesUtil
				.getProperty("cms.timetowaitforclient"));

		/* get pool size */
		Integer poolSize = Integer.parseInt(PropertiesUtil
				.getProperty("cms.clientpoolsize"));
		try {
			SoiUtils.cmsClientPool = CMSFactory.getInstance()
					.createCMSClientPool(
							mapToProperties(PropertiesUtil.propertiesMap),
							null, poolSize, timeToWait);
		} catch (CMSException e) {
			throw new ServletException(
					"Error when creating CMS pool connection : ", e);
		}
	}

	public static Properties mapToProperties(Map<String, String> map) {
		Properties p = new Properties();
		Set<Map.Entry<String, String>> set = map.entrySet();
		for (Map.Entry<String, String> entry : set) {
			p.put(entry.getKey(), entry.getValue());
		}
		return p;
	}

	public static synchronized void initCache(SOIService soiComm, String login) {
		if (CACHE_SERVICES.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_SERVICES_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sv_list = (HashMap<String, Object>[]) outParams
							.get("NUM_SV");
					for (int i = 0; i < sv_list.length; i++) {
						HashMap<String, Object> hashMap = sv_list[i];
						CACHE_SERVICES.put(sv_list[i].get("SNCODE").toString(),
								sv_list[i].get("SV_DES").toString());
					}
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (CACHE_SERVICES_PACKAGES.isEmpty()) {

			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_SERVICES_PKG_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("NUM_SP");
					for (int i = 0; i < sp_list.length; i++) {
						HashMap<String, Object> hashMap = sp_list[i];
						CACHE_SERVICES_PACKAGES.put(sp_list[i].get("SPCODE")
								.toString(), sp_list[i].get("SP_DES")
								.toString());
					}
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		if (CACHE_SERVICES_CLASSES == null || CACHE_SERVICES_CLASSES.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_SERVICE_CLASSES_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("PARAM_CLASSES_LIST");
					if (sp_list != null) {
						for (int i = 0; i < sp_list.length; i++) {
							ClassServiceBean clSvc = new ClassServiceBean(
									(new Long(sp_list[i].get("SNCODE")
											.toString()).longValue()),
									new Long(sp_list[i].get("SPCODE")
											.toString()).longValue(),
									new Long(sp_list[i].get("TMCODE")
											.toString()).longValue(),
									new Long(sp_list[i].get("CLCODE")
											.toString()).longValue());
							CACHE_SERVICES_CLASSES.add(clSvc);
						}
					}
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (CACHE_USER_CLASSES == null || CACHE_USER_CLASSES.isEmpty()) {
			try {
				HashMap<String, Object> inParams = new HashMap<String, Object>();
				inParams.put("USERNAME", login);
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_USER_CLASSES_READ,
								inParams);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("USERCLASSES_LIST");
					for (int i = 0; i < sp_list.length; i++) {
						HashMap<String, Object> hashMap = sp_list[i];
						CACHE_USER_CLASSES.add(sp_list[i].get("CLCODE")
								.toString());
					}
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (CACHE_SERVICES_PARAMETERS == null
				|| CACHE_SERVICES_PARAMETERS.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_SERVICES_PARAMETER_READ,
								null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("NUM_SERV");
					CACHE_SERVICES_PARAMETERS = ServiceParamAdapter
							.getServiceParamMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (CACHE_DEBIT_CONFIG == null || CACHE_DEBIT_CONFIG.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_DEBIT_CONFIG_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("DebitConfig_LIST");
					CACHE_DEBIT_CONFIG = ServiceParamAdapter
							.getDebitConfBeanMap(sp_list);
					for (Entry<Long, DebitConfBean> entry : SoiUtils.CACHE_DEBIT_CONFIG
							.entrySet()) {
						Long cle = entry.getKey();
						DebitConfBean debitCfgBean = entry.getValue();
						if (!debitConf.contains(debitCfgBean.getDebit())) {
							debitConf.add(debitCfgBean.getDebit());
						}
					}

				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// >******* Update, A.ESSA DI6717
		if (CACHE_CLOUD_CONFIG == null || CACHE_CLOUD_CONFIG.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_CLOUD_CONFIG_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("CloudConfig_LIST");
					CACHE_CLOUD_CONFIG = ServiceParamAdapter
							.getCloudConfBeanMap(sp_list);
					for (Entry<Long, DebitConfBean> entry : SoiUtils.CACHE_CLOUD_CONFIG
							.entrySet()) {
						Long cle = entry.getKey();
						DebitConfBean debitCfgBean = entry.getValue();
						if (!cloudConf.contains(debitCfgBean.getDebit())) {
							cloudConf.add(debitCfgBean.getDebit());
						}
					}

				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// <******* Update, A.ESSA DI6717
		if (CACHE_PRM_HIDE_CONFIG == null || CACHE_PRM_HIDE_CONFIG.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_PARAM_CONFIG_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("paramConfig_LIST");
					CACHE_PRM_HIDE_CONFIG = ServiceParamAdapter
							.getPRMHIDECONFIGMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// >******* Update, A.ESSA DI6717
		if (CACHE_IAM_DEP_SRV_PRM == null || CACHE_IAM_DEP_SRV_PRM.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_IAM_DEP_SRV_PRM_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("dep_all");
					CACHE_IAM_DEP_SRV_PRM = ServiceParamAdapter
							.getDepSrvPrmBeanMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (CACHE_IAM_AUTO_DEP_SRV_PRM == null
				|| CACHE_IAM_AUTO_DEP_SRV_PRM.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_IAM_AUTO_DEP_SRV_PRM_READ,
								null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("dep_all");
					CACHE_IAM_AUTO_DEP_SRV_PRM = ServiceParamAdapter
							.getDepSrvPrmBeanMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (CACHE_SEMA_PROFIL_SDP == null || CACHE_SEMA_PROFIL_SDP.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_SEMA_PROFIL_SDP_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("sdp_all");
					CACHE_SEMA_PROFIL_SDP = ServiceParamAdapter
							.getDepSrvPrmBeanMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (CACHE_TMCHANGE_SERVICES == null
				|| CACHE_TMCHANGE_SERVICES.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_TMCHANGE_SERVICES, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("TMChangeServices_LIST");
					CACHE_TMCHANGE_SERVICES = ServiceParamAdapter
							.getTMCHANGESERVICESMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// <******* Update, A.ESSA DI6717

		if (CACHE_PRM_VOIP_CONFIG == null || CACHE_PRM_VOIP_CONFIG.isEmpty()) {
			try {
				HashMap<String, Object> outParams = soiComm.cmsClient
						.executeCommand(SoiUtils.CMD_VOIP_CONFIG_READ, null);
				if (!outParams.isEmpty()) {
					HashMap<String, Object>[] sp_list = (HashMap<String, Object>[]) outParams
							.get("VOIPServices_LIST");
					CACHE_PRM_VOIP_CONFIG = ServiceParamAdapter
							.getVOIPCONFIGMap(sp_list);
				}
			} catch (CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	// >******* Update, A.ESSA DI6717

	public static synchronized List<ValueBeanPub> filterPrm(
			List<ValueBeanPub> vals, List<String> eligVals) throws FaultWS {
		List<ValueBeanPub> arraylist = new ArrayList<ValueBeanPub>();
		if (vals != null) {
			for (int i = 0; i < vals.size(); i++) {
				if (eligVals.contains(vals.get(i).getValue_des())) {
					arraylist.add(vals.get(i));
				}
			}
		}
		return arraylist;
	}

	public static synchronized HashMap<String, List<String>> getFilteredPrmValues(
			SOIService soiComm, long co_id, long sncode,
			Properties msgProperties, BcontractAll bcontract, String prgcode,
			List<bNewServices> Services) throws FaultWS {
		ArrayList<String> tmp = new ArrayList<String>();
		HashMap<String, List<String>> res = new HashMap<String, List<String>>();

		HashMap<String, Object> inParams = new HashMap<String, Object>();
		try {
			if (co_id != 0) {
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("SYNC_WITH_DB", true);
			}
			if (bcontract == null) {
				bcontract = new BcontractAll();
				HashMap<String, Object> outParams = soiComm.executeCommand(
						SoiUtils.CMD_CONTRACT_READ, inParams);
				inParams.remove("SYNC_WITH_DB");
				bcontract = BContractAdapterAll.getMainContractInfo(outParams);
				BContractedServicesAdapterAll.buildContractedServicesBean(
						outParams, bcontract);
			}

			if (prgcode == null || prgcode.length() == 0) {
				HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
				inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
				HashMap<String, Object> outParamsCustomer = soiComm
						.executeCommand(SoiUtils.CMD_CUSTOMER_READ,
								inParamsCustomer);
				prgcode = outParamsCustomer.get("PRG_CODE").toString();
			}

			ConfServiceBean cfSvcBean = SoiUtils.CACHE_SERVICES_PARAMETERS
					.get(new Long(sncode));

			ConfServiceBeanPub cfgSvcBeanPub = new ConfServiceBeanPub(cfSvcBean);
			List<ParamValueBeanPub> paramValueBeanPub = new ArrayList<ParamValueBeanPub>();
			for (Iterator iterator = cfSvcBean.getNUM_PARAM().iterator(); iterator
					.hasNext();) {
				ParamValueBean prmValueBean = (ParamValueBean) iterator.next();
				inParams.clear();
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("GET_VALUES", true);
				inParams.put("PRM_ID", prmValueBean.getPrm_id());
				inParams.put("SCCODE", cfSvcBean.getSccode());
				inParams.put("SNCODE", cfSvcBean.getSncode());
				inParams.put("RPCODE", bcontract.getRPCODE());
				HashMap<String, Object> outParamsBean = soiComm.executeCommand(
						SoiUtils.CMD_PARAMETER_READ, inParams);
				ParamValueBeanPub prmValueBeanPubtmp = new ParamValueBeanPub();
				prmValueBeanPubtmp = ServiceParamAdapter.getParamValueBeanPub(
						outParamsBean, cfSvcBean);

				List<ValueBeanPub> vals = prmValueBeanPubtmp.getVALUES();
				if (vals != null) {
					vals = SoiUtils.filterHiddenParam(vals,
							prmValueBean.getPrm_id(),
							(new Long(bcontract.getRPCODE())).toString(),
							cfSvcBean.getSncode(), prgcode);

					vals = SoiUtils.filterHiddenParamDebit(vals);

					if (("DEBIT".equals(prmValueBean.getPrm_id_pub()))) {
						vals = SoiUtils.filterParamDebit(bcontract, vals,
								soiComm);
					}

					if (("CLOUD".equals(prmValueBean.getPrm_id_pub()))) {
						vals = SoiUtils.filterParamCloud(bcontract, vals,
								soiComm);
					}

					vals = SoiUtils.filterDepParam(bcontract, vals,
							prmValueBean.getPrm_id(),
							(new Long(bcontract.getRPCODE())).toString(),
							cfSvcBean.getSncode(), soiComm, Services);

					tmp = new ArrayList<String>();
					for (Iterator srvPrm = vals.iterator(); srvPrm.hasNext();) {
						ValueBeanPub srvPrmBean = (ValueBeanPub) srvPrm.next();
						tmp.add(srvPrmBean.getValue_des());
					}
					res.put((new Long(prmValueBean.getPrm_id())).toString(),
							tmp);
				}
			}

		} catch (CMSException ex) {
			// TODO: handle Cmsexception
			if (ex.getMessage() != null)
				logger.info(ex.getMessage());
			if (ex.toString().contains("errorCode = RC6700")) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CONTRACT_NOT_FOUND,
						msgProperties);
			} else {
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_CMS_ERROR,
						msgProperties);
			}

		}

		return res;
	}

	public static boolean stillExists(DepSrvPrmBean[] ParamConfigList, int index) {

		boolean found = false;
		for (int ss = index + 1; ss < ParamConfigList.length; ss++) {
			if (ParamConfigList[ss].getTmcode() != null
					&& !ParamConfigList[ss]
							.getTmcode()
							.toString()
							.equals(ParamConfigList[index].getTmcode()
									.toString())) {
				continue;
			}
			if (ParamConfigList[ss].getSncode() != null
					&& !ParamConfigList[ss]
							.getSncode()
							.toString()
							.equals(ParamConfigList[index].getSncode()
									.toString())) {
				continue;
			}
			if (ParamConfigList[ss].getPrm_id() != null
					&& !ParamConfigList[ss]
							.getPrm_id()
							.toString()
							.equals(ParamConfigList[index].getPrm_id()
									.toString())) {
				continue;
			}
			if (ParamConfigList[ss].getValue() != null
					&& !ParamConfigList[ss]
							.getValue()
							.toString()
							.equals(ParamConfigList[index].getValue()
									.toString())) {
				continue;
			}
			// if (ParamConfigList[ss].getPropertyValue("DEP_SNCODE")!=null
			// &&
			// !ParamConfigList[ss].getPropertyValue("DEP_SNCODE").toString().equals(ParamConfigList[index].getPropertyValue("DEP_SNCODE").toString()))
			// {
			// continue;
			// }
			// if (ParamConfigList[ss].getPropertyValue("DEP_PRM_ID")!=null
			// &&
			// !ParamConfigList[ss].getPropertyValue("DEP_PRM_ID").toString().equals(ParamConfigList[index].getPropertyValue("DEP_PRM_ID").toString()))
			// {
			// continue;
			// }

			found = true;
		}

		return found;

	}

	public static boolean existing_Services_with_params(
			BcontractAll contractmodel, String sncode, String prmID,
			String des, SOIService soiComm, List<bNewServices> Services)
			throws CMSException, FaultWS {
		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		if ((csm == null) || (csm.size() == 0))
			return false;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
					&& bContractedServices.getSNCODE_PUB() != null
					&& bContractedServices.getSNCODE() == (new Long(sncode))
							.longValue()) {
				String value_des = ServiceParamAdapter
						.GetValueDesServicefromParametersId(sncode,
								contractmodel, soiComm, prmID);
				if (value_des != null && value_des.equals(des)) {
					found = true;
				}
			}
		}
		bNewServicesEx SrvEx = null;
		if (Services != null) {
			for (Iterator it = Services.iterator(); it.hasNext();) {
				bNewServices Srv = new bNewServices();
				SrvEx = new bNewServicesEx();
				Srv = (bNewServices) it.next();
				Long snCode = Srv.getSNCODE();
				SrvEx = ServiceUtils.getServiceParamValueExtends(SrvEx, Srv);

				for (Iterator ItPrm = SrvEx.getBNewParamValueEx().iterator(); ItPrm
						.hasNext();) {
					bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();

					for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
						bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
						if (vaLue.getVALUE_DES() != null) {
							if (prmID.equals((new Long(pRmVal.getPRM_ID()))
									.toString())) {
								if (des.equals(vaLue.getVALUE_DES())) {
									found = true;
								} else {
									found = false;
								}

							}
						}
					}
				}
			}

		}

		// ///////////////////////////////

		return found;
	}

	public static boolean must_existing_Services_with_params(
			BcontractAll contractmodel, String sncode, String prmID,
			String des, SOIService soiComm, List<bNewServices> Services)
			throws CMSException, FaultWS {
		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();

		if ((csm == null) || (csm.size() == 0))
			return false;
		boolean found = false;
		for (Iterator<BContractedServicesAll> iterator = csm.iterator(); iterator
				.hasNext();) {
			BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
					.next();
			if (bContractedServices.getCO_SN_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
					&& bContractedServices.getSNCODE_PUB() != null
					&& bContractedServices.getSNCODE() == (new Long(sncode))
							.longValue()) {
				String value_des = ServiceParamAdapter
						.GetValueDesServicefromParametersId(sncode,
								contractmodel, soiComm, prmID);
				if (value_des != null && value_des.equals(des)) {
					found = true;
				}
			}
		}

		bNewServicesEx SrvEx = null;
		if (Services != null) {
			for (Iterator it = Services.iterator(); it.hasNext();) {
				bNewServices Srv = new bNewServices();
				SrvEx = new bNewServicesEx();
				Srv = (bNewServices) it.next();
				Long snCode = Srv.getSNCODE();
				SrvEx = ServiceUtils.getServiceParamValueExtends(SrvEx, Srv);

				for (Iterator ItPrm = SrvEx.getBNewParamValueEx().iterator(); ItPrm
						.hasNext();) {
					bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();

					for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
						bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
						if (vaLue.getVALUE_DES() != null) {
							if (prmID.equals((new Long(pRmVal.getPRM_ID()))
									.toString())) {
								if (des.equals(vaLue.getVALUE_DES())) {
									found = true;
								} else {
									found = false;
								}

							}
						}
					}
				}
			}

		}

		return found;
	}

	public static synchronized List<ValueBeanPub> filterDepParam(
			BcontractAll contractmodel, List<ValueBeanPub> list_all_prm,
			Long prm_id, String rp, Long sncode, SOIService soiComm,
			List<bNewServices> Services) throws CMSException, FaultWS {

		// ContractModel contractmodel, ValueLabelWrapper[] list_all_prm,
		// ContractServiceParameterModel localContractServiceParameterModel,
		// String rp, Long sncode){

		ArrayList arraylist = new ArrayList(list_all_prm);
		ArrayList showArraylist = new ArrayList();

		Set<Map.Entry<Long, DepSrvPrmBean>> ParamConfigList = SoiUtils.CACHE_IAM_DEP_SRV_PRM
				.entrySet();

		DepSrvPrmBean[] ParamConfigArray = new DepSrvPrmBean[ParamConfigList
				.size()];
		int i = 0;
		for (Entry<Long, DepSrvPrmBean> entry : ParamConfigList) {
			DepSrvPrmBean prmCfgBean = entry.getValue();
			ParamConfigArray[i++] = prmCfgBean;
		}

		if ((ParamConfigList == null) || (ParamConfigList.size() == 0))
			return list_all_prm;
		ArrayList<String> list = new ArrayList<String>();
		// >******* Updated, A.ESSA FC6871
		ArrayList listElig = null;
		// <******* Updated, A.ESSA FC6871
		int foundIndex = -1;

		// >******* Updated, A.ESSA FC6796
		ArrayList foundIndexes = new ArrayList();
		// <******* Updated, A.ESSA FC6796

		for (int ss = 0; ss < ParamConfigArray.length; ss++) {
			if (ParamConfigArray[ss].getTmcode() != null
					&& !ParamConfigArray[ss].getTmcode().toString().equals(rp)) {
				continue;
			}
			if (ParamConfigArray[ss].getSncode() != null
					&& !ParamConfigArray[ss].getSncode().toString()
							.equals(sncode.toString())) {
				continue;
			}
			if (ParamConfigArray[ss].getPrm_id() != null
					&& !ParamConfigArray[ss].getPrm_id().toString()
							.equals(prm_id.toString())) {
				continue;
			}
			if (listElig == null) {
				listElig = new ArrayList();
			}
			for (int index = 0; index < arraylist.size(); index++) {
				if (list_all_prm
						.get(index)
						.getValue_des()
						.trim()
						.equals(ParamConfigArray[ss].getValue_des().toString()
								.trim())) {
					boolean isToHide = existing_Services_with_params(
							contractmodel, ParamConfigArray[ss].getDep_sncode()
									.toString(), ParamConfigArray[ss]
									.getDep_prm_id().toString(),
							ParamConfigArray[ss].getDep_value_des().toString(),
							soiComm, Services);
					// if(isToHide){
					// found = true;
					// }
					// >******* Updated, A.ESSA FC6796
					if (!isToHide) {
						if (!stillExists(ParamConfigArray, ss)
								&& !list.contains(ParamConfigArray[ss]
										.getValue()
										.toString()
										.concat(ParamConfigArray[ss]
												.getValue_des().toString()))) {
							arraylist.set(index, null);
							continue;
						}
					} else {
						list.add(ParamConfigArray[ss]
								.getValue()
								.toString()
								.concat(ParamConfigArray[ss].getValue_des()
										.toString()));
						// >******* Updated, A.ESSA FC6871
						listElig.add(arraylist.get(index));
						// <******* Updated, A.ESSA FC6871
					}
					// <******* Updated, A.ESSA FC6796

				}
			}
		}
		// if(!found){
		for (int ss = 0; ss < ParamConfigArray.length; ss++) {
			if (ParamConfigArray[ss].getTmcode() != null
					&& !ParamConfigArray[ss].getTmcode().toString().equals(rp)) {
				continue;
			}
			if (ParamConfigArray[ss].getDep_sncode() != null
					&& !ParamConfigArray[ss].getDep_sncode().toString()
							.equals(sncode.toString())) {
				continue;
			}
			if (ParamConfigArray[ss].getDep_prm_id() != null
					&& !ParamConfigArray[ss].getDep_prm_id().toString()
							.equals(prm_id.toString())) {
				continue;
			}
			if (listElig == null) {
				listElig = new ArrayList();
			}
			for (int index = 0; index < arraylist.size(); index++) {
				if (list_all_prm
						.get(index)
						.getValue_des()
						.toString()
						.trim()
						.equals(ParamConfigArray[ss].getDep_value_des()
								.toString().trim())) {
					boolean isToHide = must_existing_Services_with_params(
							contractmodel, ParamConfigArray[ss].getSncode()
									.toString(), ParamConfigArray[ss]
									.getPrm_id().toString(),
							ParamConfigArray[ss].getValue_des().toString(),
							soiComm, Services);

					if (isToHide) {
						// >******* Updated, A.ESSA FC6796
						// foundIndex = index;
						foundIndexes.add(new Integer(index));
						// break;
						// <******* Updated, A.ESSA FC6796
					}
					// >******* Updated, A.ESSA FC6871
					else {
						listElig.add(arraylist.get(index));
					}
					// <******* Updated, A.ESSA FC6871
				}
			}
		}
		// }

		// >******* Updated, A.ESSA FC6796
		if (foundIndexes.size() > 0) {
			for (int index = 0; index < arraylist.size(); index++) {
				if (!foundIndexes.contains(new Integer(index))) {
					arraylist.set(index, null);
				}
			}
		}

		// >******* Updated, A.ESSA FC6871
		else {
			for (int index = 0; index < arraylist.size(); index++) {
				if (listElig != null
						&& !listElig.contains(arraylist.get(index))) {
					arraylist.set(index, null);
				}
			}
		}
		// <******* Updated, A.ESSA FC6871
		// <******* Updated, A.ESSA FC6796

		int k = 0;
		while (k < arraylist.size()) {
			if (arraylist.get(k) == null) {
				arraylist.remove(k);
			} else
				k++;
		}
		return arraylist;
	}

	@SuppressWarnings("unchecked")
	public static synchronized List<ValueBeanPub> filterHiddenParam(
			List<ValueBeanPub> list_all_prm, Long prm_id, String rp,
			Long sncode, String prgcode) {

		ArrayList arraylist = new ArrayList(list_all_prm);
		ArrayList showArraylist = new ArrayList();
		Set<Map.Entry<Long, PrmHideConfBean>> ParamConfigList = SoiUtils.CACHE_PRM_HIDE_CONFIG
				.entrySet();
		if ((ParamConfigList == null) || (ParamConfigList.size() == 0))
			return list_all_prm;
		for (Entry<Long, PrmHideConfBean> entry : ParamConfigList) {
			PrmHideConfBean prmCfgBean = entry.getValue();
			if (prmCfgBean.getPrgcode() != null
					&& prmCfgBean.getPrgcode().length() > 0
					&& !prmCfgBean.getPrgcode().toString().equals(prgcode)) {
				continue;
			}
			if (prmCfgBean.getTmcode() != null && prmCfgBean.getTmcode() > 0
					&& !prmCfgBean.getTmcode().toString().equals(rp)) {
				continue;
			}
			if (prmCfgBean.getSncode() != null
					&& prmCfgBean.getSncode() > 0
					&& !prmCfgBean.getSncode().toString()
							.equals(sncode.toString())) {
				continue;
			}
			if (prmCfgBean.getPrm_id() != null
					&& prmCfgBean.getPrm_id() > 0
					&& !prmCfgBean.getPrm_id().toString()
							.equals(prm_id.toString())) {
				continue;
			}
			for (int index = 0; index < arraylist.size(); index++) {
				if (("H".equals(prmCfgBean.getShow_conf()) && list_all_prm
						.get(index).getValue_des().toString().trim()
						.equals(prmCfgBean.getValue_des().toString().trim()))) {
					arraylist.set(index, null);
					continue;
				}
				if ("S".equals(prmCfgBean.getShow_conf())
						&& list_all_prm
								.get(index)
								.getValue_des()
								.toString()
								.trim()
								.equals(prmCfgBean.getValue_des().toString()
										.trim())) {
					showArraylist.add(arraylist.get(index));
				}
			}
		}
		if (showArraylist.size() > 0) {
			return showArraylist;
		}

		int k = 0;
		while (k < arraylist.size()) {
			if (arraylist.get(k) == null) {
				arraylist.remove(k);
			} else
				k++;
		}
		return arraylist;
	}

	@SuppressWarnings("unchecked")
	public static synchronized List<ValueBeanPub> filterHiddenParamDebit(
			List<ValueBeanPub> list_all_prm) {
		ArrayList arraylist = new ArrayList(list_all_prm);
		String debitConfSTR = "15Go,30Go,55Go,75Go";
		String arr[] = debitConfSTR.split(",");
		ArrayList debitConf = new ArrayList(Arrays.asList(arr));

		for (int index = 0; index < arraylist.size(); index++) {
			if (debitConf.contains(list_all_prm.get(index).getValue_des()
					.toString())) {

				arraylist.set(index, null);
				continue;
			}
		}

		int k = 0;
		while (k < arraylist.size()) {
			if (arraylist.get(k) == null) {
				arraylist.remove(k);
			} else
				k++;
		}
		return arraylist;
	}

	@SuppressWarnings("unchecked")
	public static synchronized List<ValueBeanPub> filterParamDebit(
			BcontractAll contractmodel, List<ValueBeanPub> list_all_prm,
			SOIService soiComm) throws CMSException, FaultWS {
		ArrayList arraylist = new ArrayList<ValueBeanPub>();
		for (int i = 0; i < list_all_prm.size(); i++) {
			ValueBeanPub nwValueBean = list_all_prm.get(i);
			boolean toPub = true;

			if (SoiUtils.debitConf.contains(nwValueBean.getValue_des())) {
				toPub = false;
				for (Entry<Long, DebitConfBean> entry : SoiUtils.CACHE_DEBIT_CONFIG
						.entrySet()) {
					Long cle = entry.getKey();
					DebitConfBean debitCfgBean = entry.getValue();
					if (debitCfgBean.getTm_shdes() != null
							&& !contractmodel.getRPCODE_PUB().equals(
									debitCfgBean.getTm_shdes())) {
						continue;
					}
					if (new Long(debitCfgBean.getPrm_type()) < 0
							&& nwValueBean.getValue_des().equals(
									debitCfgBean.getDebit())) {
						toPub = true;
					}
					List<BContractedServicesAll> csm = contractmodel
							.getContractedServices();
					for (Iterator<BContractedServicesAll> iterator = csm
							.iterator(); iterator.hasNext();) {
						BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
								.next();
						logger.debug("=> getEligibleDebit() : checking service :"
								+ bContractedServices.getSNCODE_PUB());
						if (bContractedServices.getCO_SN_STATUS().toString()
								.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
								&& bContractedServices.getSNCODE_PUB() != null
								&& bContractedServices.getSNCODE_PUB().equals(
										debitCfgBean.getSn_shdes())
								&& nwValueBean.getValue_des().equals(
										debitCfgBean.getDebit())) {
							if ("3".equals(debitCfgBean.getPrm_type())) {
								toPub = true;
								break;
							} else {
								String value_des = ServiceParamAdapter
										.GetValueDesServicefromParameters(
												debitCfgBean.getSn_shdes(),
												contractmodel, soiComm,
												debitCfgBean.getPrm_shdes());
								if (value_des != null
										&& value_des.equals(debitCfgBean
												.getValue_des())) {
									toPub = true;
									break;
								}
							}
						}
					}
					if (toPub)
						break;
				}
			}

			if (toPub) {
				arraylist.add(nwValueBean);
			}
		}
		return arraylist;
	}

	@SuppressWarnings("unchecked")
	public static synchronized List<ValueBeanPub> filterParamCloud(
			BcontractAll contractmodel, List<ValueBeanPub> list_all_prm,
			SOIService soiComm) throws CMSException, FaultWS {
		ArrayList arraylist = new ArrayList<ValueBeanPub>();
		for (int i = 0; i < list_all_prm.size(); i++) {
			ValueBeanPub nwValueBean = list_all_prm.get(i);
			boolean toPub = true;

			if (SoiUtils.cloudConf.contains(nwValueBean.getValue_des())) {
				toPub = false;
				for (Entry<Long, DebitConfBean> entry : SoiUtils.CACHE_CLOUD_CONFIG
						.entrySet()) {
					Long cle = entry.getKey();
					DebitConfBean debitCfgBean = entry.getValue();
					if (debitCfgBean.getTm_shdes() != null
							&& !contractmodel.getRPCODE_PUB().equals(
									debitCfgBean.getTm_shdes())) {
						continue;
					}
					if (new Long(debitCfgBean.getPrm_type()) < 0
							&& nwValueBean.getValue_des().equals(
									debitCfgBean.getDebit())) {
						toPub = true;
					}
					List<BContractedServicesAll> csm = contractmodel
							.getContractedServices();
					for (Iterator<BContractedServicesAll> iterator = csm
							.iterator(); iterator.hasNext();) {
						BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
								.next();
						logger.debug("=> getEligibleDebit() : checking service :"
								+ bContractedServices.getSNCODE_PUB());
						if (bContractedServices.getCO_SN_STATUS().toString()
								.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
								&& bContractedServices.getSNCODE_PUB() != null
								&& bContractedServices.getSNCODE_PUB().equals(
										debitCfgBean.getSn_shdes())
								&& nwValueBean.getValue_des().equals(
										debitCfgBean.getDebit())) {
							if ("3".equals(debitCfgBean.getPrm_type())) {
								toPub = true;
								break;
							} else {
								String value_des = ServiceParamAdapter
										.GetValueDesServicefromParameters(
												debitCfgBean.getSn_shdes(),
												contractmodel, soiComm,
												debitCfgBean.getPrm_shdes());
								if (value_des != null
										&& value_des.equals(debitCfgBean
												.getValue_des())) {
									toPub = true;
									break;
								}
							}
						}
					}
					if (toPub)
						break;
				}
			}

			if (toPub) {
				arraylist.add(nwValueBean);
			}
		}
		return arraylist;
	}

	public static boolean is_service_clcode(long tmcode, long sncode,
			long spcode, int clcode) {
		List internalCACHE_SERVICES_CLASSES = SoiUtils.CACHE_SERVICES_CLASSES;
		ClassServiceBean clSvc = new ClassServiceBean(sncode, spcode, tmcode,
				clcode);
		if (internalCACHE_SERVICES_CLASSES.contains(clSvc)) {
			return true;
		} else
			return false;
	}

	public static boolean user_has_auth_class(long tmcode, long sncode,
			long spcode) {
		List internalCACHE_SERVICES_CLASSES = CACHE_SERVICES_CLASSES;
		ClassServiceBean clSvc7 = new ClassServiceBean(sncode, spcode, tmcode,
				7);
		ClassServiceBean clSvc8 = new ClassServiceBean(sncode, spcode, tmcode,
				8);
		ClassServiceBean clSvc5 = new ClassServiceBean(sncode, spcode, tmcode,
				5);
		ClassServiceBean clSvc6 = new ClassServiceBean(sncode, spcode, tmcode,
				6);
		if (internalCACHE_SERVICES_CLASSES.contains(clSvc7)
				|| internalCACHE_SERVICES_CLASSES.contains(clSvc8)
				|| internalCACHE_SERVICES_CLASSES.contains(clSvc5)
				|| internalCACHE_SERVICES_CLASSES.contains(clSvc6)) {

			return false;
		} else
			return true;
	}

	// <******* Update, A.ESSA DI6717

	public static synchronized SOIService getCILSOIServiceByUser(
			WebServiceContext ws, SoiContext soiCtx) {
		Principal userPr = ws.getUserPrincipal();
		SOIService soiCtxcl = (SOIService) soiCtx.getContexts().get(
				"CIL_" + userPr.getName());
		if (soiCtxcl != null && userPr.getName() != null
				&& soiCtxcl.getUserName() != null
				&& soiCtxcl.getUserName().equals(userPr.getName())) {
			return soiCtxcl;
		} else
			return null;
	}

	public static synchronized SOIService getCILSOIServiceByUser2(
			String username, SoiContext soiCtx) {
		SOIService soiCtxcl = (SOIService) soiCtx.getContexts().get(
				"CIL_" + username);
		if (soiCtxcl != null && username != null
				&& soiCtxcl.getUserName() != null
				&& soiCtxcl.getUserName().equals(username)) {
			return soiCtxcl;
		} else
			return null;
	}

	public static synchronized SOIService getSecuritySOIServiceByUser(
			WebServiceContext ws, SoiContext soiCtx) {
		Principal userPr = ws.getUserPrincipal();
		return (SOIService) soiCtx.getContexts().get(
				"SECURITY_" + userPr.getName());
	}

	public static SVLDateTime getDate(Date inDate, String strFormat)
			throws CMSException {

		if (inDate == null)
			return null;

		// >******* Update, A.ESSA DI6717
		SVLDateTime svlDate = ExchangeFormatFactory.instance()
				.createSVLDateTime(Calendar.getInstance().getTime());
		// <******* Update, A.ESSA DI6717

		SimpleDateFormat dateFormat = new SimpleDateFormat(strFormat);
		Date date = null;
		try {
			SimpleDateFormat format1 = new SimpleDateFormat(strFormat);
			String date1 = format1.format(inDate);
			date = dateFormat.parse(date1);
		} catch (ParseException e) {
			CMSException cmsexp = new CMSException("INVALID DATE");
			// logError(cmsexp);
			throw cmsexp;
		}
		svlDate.setDateTime(date);
		return svlDate;
	}

	public static UserBean Authentify(WebServiceContext wsContext,
			String login, String password, Properties msgProperties,
			String securityName, String securityVersion, String cilName,
			String cilVersion, SoiContext soiContext, UserCache userCache)
			throws FaultWS {
		MessageContext msgContext = wsContext.getMessageContext();

		logger.info("Utilisateur : " + login + " try to connect.");
		UserBean myUser = new UserBean();

		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("NAME", login);
		if ((login == null || "".equals(login))
				|| (password == null || "".equals(password)))
			throw new FaultWS(Constants.ERROR_CODE.MT_WS_AUTHEN_INPUT_MISSING,
					msgProperties);
		try {
			// Authenticate valid connection
			SOIService soiSecurity = new SOIService(securityName,
					securityVersion);

			soiSecurity.login(login, password);

			// Get info users
			logger.info(" :  Utilisateur : " + login
					+ " successfully connected .");
			HashMap<String, Object> outParams = soiSecurity.executeCommand(
					SoiUtils.CMD_USERS_READ, inParams);

			BuserAdapter.retrieveInfoUser(outParams, myUser);

			// Expiration Date

			// if
			// (myUser.getPassword_expires().compareTo(Calendar.getInstance().getTime())==-1)
			// {
			// soiSecurity.cmsClient.disconnect();
			// //SOIService.reset();
			// SecurityContextHolder.clearContext();
			// userCache.removeUserFromCache(login);
			// throw new
			// FaultWS(Constants.ERROR_CODE.MT_WS_AUTHEN_PASSWD_EXPIRE,msgProperties);
			// }

			// Get users rights
			HashMap<String, Object> inParamsRight = SoiUtils
					.getUserRightParameters(login, password);
			HashMap outParams_right = soiSecurity.executeCommand(
					SoiUtils.CMD_USER_RIGHT_CHECK, inParamsRight);

			List listProfiles = soiSecurity
					.convertListProfile((HashMap<String, Object>[]) outParams_right
							.get("USER_PERMISSIONS"));

			myUser.setRights(listProfiles);

			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			myUser.setPassword(password);
			UserDetails user = new User(login, password, true, true, true,
					true, authorities);

			UsernamePasswordAuthenticationToken resulttoken = new UsernamePasswordAuthenticationToken(
					user, myUser.getPassword(), user.getAuthorities());

			SecurityContextHolder.getContext().setAuthentication(resulttoken);
			// soiSecurity.cmsClient.disconnect();
			// soiSecurity.reset();
			logger.info(" : Change  Soi name Server to CIL ");
			// soiComm = new SOIService ("CIL","2");
			SOIService soiComm = new SOIService(cilName, cilVersion);
			soiComm.login(login, password);
			// Cache Offer
			SoiUtils.initCache(soiComm, login);
			// MessageContext mc = MessageContext.getCurrentMessageContext();
			soiContext.getContexts().put("SECURITY_" + login,
					(SOIService) soiSecurity);
			soiContext.getContexts().put("CIL_" + login, (SOIService) soiComm);

		} catch (CMSException cex) {

			if (!cex.getMessage().contains(
					"This CMS client is already connected")) {
				soiContext.getContexts().remove("SECURITY_" + login);
				soiContext.getContexts().remove("CIL_" + login);
				logger.error(" : CMSException " + cex.getMessage());
				SecurityContextHolder.clearContext();
				userCache.removeUserFromCache(login);
				myUser = null;
				if (cex.getErrors() != null && cex.getErrors().length > 0) {
					logger.error(" : CMSException "
							+ cex.getErrors()[0].getAdditionalInfo());
					throw new FaultWS(cex.getErrors()[0].getErrorCode(),
							cex.getErrors()[0].getAdditionalInfo());
				} else {
					if (cex.getMessage().contains("CMS client not connected")) {
						throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
								msgProperties);
					} else {
						logger.error(" : CMSException " + cex.getMessage());
						throw new FaultWS("RC1000", cex.getMessage());
					}
				}

			} else {
				SOIService soiSecurity = SoiUtils.getSecuritySOIServiceByUser(
						wsContext, soiContext);
				try {
					HashMap<String, Object> outParams = soiSecurity
							.executeCommand(SoiUtils.CMD_USERS_READ, inParams);
					BuserAdapter.retrieveInfoUser(outParams, myUser);
				} catch (CMSException cex2) {
					if (cex.getErrors() != null && cex.getErrors().length > 0) {
						logger.error(" : CMSException "
								+ cex.getErrors()[0].getAdditionalInfo());
						throw new FaultWS(cex.getErrors()[0].getErrorCode(),
								cex.getErrors()[0].getAdditionalInfo());
					} else {
						if (cex.getMessage().contains(
								"CMS client not connected")) {
							throw new FaultWS(
									Constants.ERROR_CODE.MT_USERUNLOGGED,
									msgProperties);
						} else {
							logger.error(" CMSException " + cex.getMessage());
							throw new FaultWS("RC1000", cex.getMessage());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception " + e.getLocalizedMessage());
			e.printStackTrace();
			SecurityContextHolder.clearContext();
			userCache.removeUserFromCache(login);
			myUser = null;
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
					msgProperties);
		}
		return myUser;
	}
}
