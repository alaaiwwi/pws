package ma.iam.pws.soi.exception;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.UnmarshalException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;

import ma.iam.pws.soi.beans.errors.FaultWS;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.common.injection.NoJSR250Annotations;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.wss4j.common.ext.WSSecurityException;

import com.ctc.wstx.exc.WstxParsingException;

@NoJSR250Annotations
public class ExceptionInterceptor extends AbstractSoapInterceptor {

	public ExceptionInterceptor() {
		super(Phase.PRE_LOGICAL);
	}

	// @Override
	public void handleMessage(SoapMessage message) throws Fault {
		Fault fault = (Fault) message.getContent(Exception.class);
		Throwable ex = fault.getCause();
		if (ex instanceof FaultWS) {
			FaultWS e = (FaultWS) ex;
			fault.setStatusCode(HttpServletResponse.SC_OK);
			generateSoapFault(fault, e);
		} else if (ex instanceof UnmarshalException
				|| ex instanceof IllegalArgumentException) {
			FaultWS e = new FaultWS("MT9998",
					"UnmarshalException: For input Arguments Or Incorrect Call");
			generateSoapFault(fault, e);
		} else if (ex instanceof WSSecurityException
				|| ex instanceof SOAPFaultException
				|| ex instanceof SOAPException
				|| ex instanceof WstxParsingException) {
			// WSSecurityException e = (WSSecurityException) ex;
			generateSoapFault(fault, ex);
		} else {
			generateSoapFault(fault, null);
		}
	}

	private void generateSoapFault(Fault fault, FaultWS e) {
		fault.setFaultCode(createQName(e != null ? "WS-ERR"
				: e.getCodefault() != null ? e.getCodefault() : "WS-ERR"));
		fault.setMessage(e != null ? "WS : Internal Exception"
				: e.getMessage() != null ? e.getMessage()
						: "WS : Internal Exception");

	}

	private static QName createQName(String errorCode) {
		return new QName("ws.iam.ma", String.valueOf(errorCode));
	}

	private void generateSoapFault(Fault fault, Throwable e) {
		fault.setFaultCode(createQName("WS-ERR"));
		fault.setMessage(e.getMessage());
	}
}