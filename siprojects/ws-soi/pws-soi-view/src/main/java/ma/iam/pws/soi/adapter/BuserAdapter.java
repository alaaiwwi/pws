package ma.iam.pws.soi.adapter;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import com.lhs.ccb.soi.types.DateI;
import com.lhs.ccb.soi.types.DateIHelper;

import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.engine.common.util.Utils;

public class BuserAdapter {
	
	public static  void retrieveInfoUser(HashMap<String, Object> outParams , UserBean user) {
		
		//_logger.debug("Getting creator name ...");
		if (outParams.containsKey("USERS")) {
			try{
				HashMap [] mapInfos=(HashMap []) outParams.get("USERS");
				user.setName(mapInfos[0].get("NAME")!=null?mapInfos[0].get("NAME").toString():null);
				user.setGroup(mapInfos[0].get("GROUP")!=null?mapInfos[0].get("GROUP").toString():null);
				user.setIs_batch(new Boolean(mapInfos[0].get("IS_BATCH")!=null? mapInfos[0].get("IS_BATCH").toString():"N").booleanValue());
				if (mapInfos[0].get("LNG_CODE")!=null )
					user.setLng_code(new Long(mapInfos[0].get("LNG_CODE").toString()).longValue());
				
				if (mapInfos[0].get("PASSWORD_EXPIRES")!=null) {
					SimpleDateFormat frmtDate = new SimpleDateFormat("dd/MM/yyyy");
					System.out.println(mapInfos[0].get("PASSWORD_EXPIRES").toString());
					user.setPassword_expires(Utils.converttoDatefromDateI( (DateI) mapInfos[0].get("PASSWORD_EXPIRES"), frmtDate));
				}
				user.setPwd_remind_days(new Long(mapInfos[0].get("PWD_REMIND_DAYS").toString()).intValue());
				user.setUser_type(outParams.get(" USER_TYPE").toString());
			}catch(Exception ex){
				//TODO
			}
			//_logger.debug("last modified user is " + _strUserModified);
		}
	}

}

