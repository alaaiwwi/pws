package ma.iam.pws.soi.engine.security.filter;
//package ma.iam.ws.engine.security.filter;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import ma.iam.ws.engine.common.Constants;
//
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.access.intercept.InterceptorStatusToken;
//import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
//import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
//
///**
// * TODO comment.
// * <br/><br/>
// * Projet : WS MT
// * Créé le 
// * 
// * @author ATOS
// */
//public class FilterSecurityInterceptor extends FilterSecurityInterceptor {
//	
//	private static final String IGNORE_SESSION_RETURN_PARAMETER = "wsdl";
//	private static final String SECURITY_FILTER_APPLIED = "__spring_security_filterSecurityInterceptor_filterApplied";
//	
//	/**
//	 * (non javadoc)
//	 * See @see org.springframework.security.intercept.web.FilterSecurityInterceptor#invoke(org.springframework.security.intercept.web.FilterInvocation).
//	 * @param fi
//	 * @throws IOException
//	 * @throws ServletException
//	 */
//	public void invoke(FilterInvocation fi) throws IOException, ServletException {
//        if ((fi.getRequest() != null) && (fi.getRequest().getAttribute(SECURITY_FILTER_APPLIED) != null)
//            && isObserveOncePerRequest()) {
//            // filter already applied to this request and user wants us to observce
//            // once-per-request handling, so don't re-do security checking
//            fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
//        } else {
//            // first time this request being called, so perform security checking
//            if (fi.getRequest() != null) {
//                fi.getRequest().setAttribute(SECURITY_FILTER_APPLIED, Boolean.TRUE);
//            }
//
//            InterceptorStatusToken token = null;
//            HttpServletRequest httpServletRequest = (HttpServletRequest) fi.getRequest();
//            try {
//            	token = super.beforeInvocation(fi);
//            	if (httpServletRequest != null) {
//            		httpServletRequest.getSession().setAttribute(Constants.AUTHORIZATION_FAILURE_FLG, Boolean.FALSE);
//            		httpServletRequest.getSession().setAttribute(Constants.AUTHENTICATION_NOT_FOUND_FLG, Boolean.FALSE);
//            	}
//            } catch (AuthenticationCredentialsNotFoundException authNotFoundException) {
//            	if(httpServletRequest.getQueryString() != null && httpServletRequest.getQueryString().toLowerCase().contains(IGNORE_SESSION_RETURN_PARAMETER)) {
//            		((HttpServletResponse)fi.getResponse()).sendError(500, "Vous n'êtes pas autorisé à utiliser cette application");
//            	}
//            	if (httpServletRequest != null) {
//            		httpServletRequest.getSession().setAttribute(Constants.AUTHENTICATION_NOT_FOUND_FLG, Boolean.TRUE);
//            	}
//            } catch (AccessDeniedException accessDeniedException) {
//            	if (httpServletRequest != null) {
//            		httpServletRequest.getSession().setAttribute(Constants.AUTHORIZATION_FAILURE_FLG, Boolean.TRUE);
//            	}
//            }
//
//            try {
//                fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
//            } finally {
//                super.afterInvocation(token, null);
//            }
//        }
//    }
//}
