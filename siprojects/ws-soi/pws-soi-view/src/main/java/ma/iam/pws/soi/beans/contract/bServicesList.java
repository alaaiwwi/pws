package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
public class bServicesList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private String op;
	private List<bNewServices> SERVICES= new ArrayList<bNewServices>();

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}




	public List<bNewServices> getSERVICES() {
		return SERVICES;
	}

	public void setSERVICES(List<bNewServices> sERVICES) {
		SERVICES = sERVICES;
	}

	@Override
	public String toString() {
		return " service SNCODE= [ " + op + "]";
	}

	
}
