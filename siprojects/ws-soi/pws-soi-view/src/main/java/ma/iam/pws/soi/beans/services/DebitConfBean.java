package ma.iam.pws.soi.beans.services;

public class DebitConfBean {
	private String value_shdes;
	private String value_des;
	private String tm_shdes;
	private String sn_shdes;
	private String prm_type;
	private String prm_shdes;
	private String debit;
	public String getValue_shdes() {
		return value_shdes;
	}
	public void setValue_shdes(String value_shdes) {
		this.value_shdes = value_shdes;
	}
	public String getValue_des() {
		return value_des;
	}
	public void setValue_des(String value_des) {
		this.value_des = value_des;
	}
	public String getTm_shdes() {
		return tm_shdes;
	}
	public void setTm_shdes(String tm_shdes) {
		this.tm_shdes = tm_shdes;
	}
	public String getSn_shdes() {
		return sn_shdes;
	}
	public void setSn_shdes(String sn_shdes) {
		this.sn_shdes = sn_shdes;
	}
	public String getPrm_type() {
		return prm_type;
	}
	public void setPrm_type(String prm_type) {
		this.prm_type = prm_type;
	}
	public String getPrm_shdes() {
		return prm_shdes;
	}
	public void setPrm_shdes(String prm_shdes) {
		this.prm_shdes = prm_shdes;
	}
	public String getDebit() {
		return debit;
	}
	public void setDebit(String debit) {
		this.debit = debit;
	}
	@Override
	public String toString() {
		return "DebitConfBean [value_shdes=" + value_shdes + ", value_des="
				+ value_des + ", tm_shdes=" + tm_shdes + ", sn_shdes="
				+ sn_shdes + ", prm_type=" + prm_type + ", prm_shdes="
				+ prm_shdes + ", debit=" + debit + "]";
	}
	
}
