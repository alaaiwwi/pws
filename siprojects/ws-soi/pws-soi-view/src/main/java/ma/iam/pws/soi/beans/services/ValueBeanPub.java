package ma.iam.pws.soi.beans.services;

public class ValueBeanPub {
	private String value;
	private String value_des;
	private String value_pub;
	private Long value_seqno;

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "ValueBeanPub [value=" + value + ", value_des=" + value_des
				+ ", value_pub=" + value_pub + ", value_seqno=" + value_seqno
				+ "]";
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue_des() {
		return value_des;
	}

	public void setValue_des(String value_des) {
		this.value_des = value_des;
	}

	public String getValue_pub() {
		return value_pub;
	}

	public void setValue_pub(String value_pub) {
		this.value_pub = value_pub;
	}

	public Long getValue_seqno() {
		return value_seqno;
	}

	public void setValue_seqno(Long value_seqno) {
		this.value_seqno = value_seqno;
	}

}
