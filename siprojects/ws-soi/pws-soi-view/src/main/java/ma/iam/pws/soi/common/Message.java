package ma.iam.pws.soi.common;

import java.util.List;
import java.util.Properties;

public final class Message {
	

	
	// private Message msg= new Message();
	
	public  static String getMessage(String key , Properties msgproperties ){
		
		return msgproperties.getProperty(key)!=null?" "+(msgproperties.getProperty(key)):" INVALIDE KEY"+key;
		
	}
	public static String getMessage(Object obj, String key , Properties msgproperties ){

		return msgproperties.getProperty(key)!=null? obj + getMessage(key,msgproperties):(" INVALIDE KEY"+key);
		
	}
	
	
public  static String getMessage(String key , Properties msgproperties, List<String> params){
		
		String res = null;
		if(msgproperties.getProperty(key)!=null){
			res = " " +(msgproperties.getProperty(key));
			if(params!=null){
				for(int i=0;i<params.size();i++){
					res = res.replace("{"+i+"}", params.get(i));
				}
			}
		}
		else{
			res = " INVALIDE KEY"+key;
		}
		return res;
		
	}
	public static String getMessage(Object obj, String key , Properties msgproperties, List<String> params){

		return msgproperties.getProperty(key)!=null? obj + getMessage(key,msgproperties, params):(" INVALIDE KEY"+key);
		
	}
//	public Properties getMsgproperties() {
//		return msgproperties;
//	}
//	public static void setMsgproperties(Properties msgproperties) {
//		msgproperties = msgproperties;
//	}
	
//	public static synchronized  Message getInstance(){
//		if (msg==null) msg=new Message();
//		return msg;
//	}
}
