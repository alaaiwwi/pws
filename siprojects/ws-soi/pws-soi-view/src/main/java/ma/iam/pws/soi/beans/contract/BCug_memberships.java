package ma.iam.pws.soi.beans.contract;

public class BCug_memberships {

	/**
	 * CUG id
	 */
	private long CUG_ID;
	/**
	 * CUG index id
	 */
	private long CUG_INDEX;

	/**
	 * CUG interlock code
	 */
	private String CUG_INTERLOCK_CODE;

	/**
	 * CUG name
	 */
	private String CUG_NAME;

	/**
	 * 
	 CUG pending status
	 * 
	 * 2 = Active 4 = Deactive
	 */
	private Integer CUG_PENDING_STATUS;

	public long getCUG_ID() {
		return CUG_ID;
	}
	/**
	 * SET CUG_ID
	 * @param cUG_ID
	 */
	public void setCUG_ID(long cUG_ID) {
		CUG_ID = cUG_ID;
	}
	/**
	 * GET CUG_INDEX
	 * @return
	 */
	public long getCUG_INDEX() {
		return CUG_INDEX;
	}
	/**
	 * SET CUG_INDEX
	 * @param cUG_INDEX
	 */
	public void setCUG_INDEX(long cUG_INDEX) {
		CUG_INDEX = cUG_INDEX;
	}
	/**
	 * GET : CUG_INTERLOCK_CODE
	 * @return
	 */
	public String getCUG_INTERLOCK_CODE() {
		return CUG_INTERLOCK_CODE;
	}
	/**
	 * SET : CUG_INTERLOCK_CODE
	 * @param cUG_INTERLOCK_CODE
	 */
	public void setCUG_INTERLOCK_CODE(String cUG_INTERLOCK_CODE) {
		CUG_INTERLOCK_CODE = cUG_INTERLOCK_CODE;
	}
	/**
	 * GET : CUG name
	 */
	public String getCUG_NAME() {
		return CUG_NAME;
	}
	/**
	 *  SET : CUG name
	 * @param cUG_NAME
	 */
	public void setCUG_NAME(String cUG_NAME) {
		CUG_NAME = cUG_NAME;
	}
	/**
	 GET CUG pending status
	 * 
	 * 2 = Active 4 = Deactive
	 * @return
	 */
	public Integer getCUG_PENDING_STATUS() {
		return CUG_PENDING_STATUS;
	}
	/**
	 SET CUG pending status
	 * 
	 * 2 = Active 4 = Deactive
	 * @return
	 */
	public void setCUG_PENDING_STATUS(Integer cUG_PENDING_STATUS) {
		CUG_PENDING_STATUS = cUG_PENDING_STATUS;
	}

}
