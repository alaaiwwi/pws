package ma.iam.pws.soi.beans.errors;

public class BError  {

	/** The Constant ALIAS. */
	public static final String ALIAS = "Errors";

	private String ERROR_CODE;
	private String ERROR_DES;

	public String getERROR_CODE() {
		return ERROR_CODE;
	}

	public void setERROR_CODE(String eRROR_CODE) {
		ERROR_CODE = eRROR_CODE;
	}

	public String getERROR_DES() {
		return ERROR_DES;
	}

	public void setERROR_DES(String eRROR_DES) {
		ERROR_DES = eRROR_DES;
	}

	public BError(String error_code, String desc) {
		ERROR_CODE = error_code;
		ERROR_DES = desc;
	}

	public BError() {
		super();
	}

}
