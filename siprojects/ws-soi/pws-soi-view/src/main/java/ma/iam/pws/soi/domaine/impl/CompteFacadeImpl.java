package ma.iam.pws.soi.domaine.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.domaine.CompteFacade;
import ma.iam.pws.soi.engine.common.exception.FunctionalException;
import ma.iam.pws.soi.engine.common.exception.TechnicalException;
import ma.iam.pws.soi.service.ISoiService;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;

public class CompteFacadeImpl implements CompteFacade {

	//@Override
	public UserBean getCompte(String username,String password) throws TechnicalException,
			FunctionalException {
		// TODO Auto-generated method stub
		UserBean myUser = new UserBean();

		HashMap inParams = new HashMap();
		inParams.put("NAME", username);
		try {
			// Authenticate valid connection
			//SOIService soiComm = SOIService.getInstance("CMI","2"); 
			SOIService soiComm = SoiUtils.getCILSOIServiceByUser(null,null );
			//soiComm.login(username, password);

			// Get info users

			Map<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_USERS_READ, inParams);
			SimpleDateFormat frmtDate = new SimpleDateFormat("YYYY-MM-DD");
			myUser.setName(outParams.get("NAME").toString());
			myUser.setGroup(outParams.get("GROUP").toString());
			myUser.setIs_batch(new Boolean(outParams.get("IS_BATCH").toString())
					.booleanValue());
			myUser.setLng_code(new Long(outParams.get("LNG_CODE").toString())
					.longValue());
			myUser.setPassword_expires(frmtDate.parse(outParams.get(
					"PASSWORD_EXPIRES").toString()));
			myUser.setPwd_remind_days(new Long(outParams.get("PWD_REMIND_DAYS")
					.toString()).intValue());
			myUser.setUser_type(outParams.get(" USER_TYPE").toString());

			// Get users rights
			// List<String> profilesList = getProfilesList(cmsClient, inParams);
			HashMap outParams_right = soiComm.cmsClient.executeCommand(
					SoiUtils.CMD_USER_RIGHT_CHECK, inParams);

			List listProfiles = soiComm
					.convertListProfile((HashMap[]) outParams
							.get("USER_PERMISSIONS"));

			myUser.setRights(listProfiles);
					
		}catch (Exception ex){
			// TODO
			throw new FunctionalException(ex.getMessage());
		}
		return myUser;
	}

}