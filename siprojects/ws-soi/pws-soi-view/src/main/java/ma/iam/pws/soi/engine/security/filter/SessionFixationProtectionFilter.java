package ma.iam.pws.soi.engine.security.filter;
//package ma.iam.ws.engine.security.filter;
//
//import java.io.IOException;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.springframework.security.core.context.HttpSessionContextIntegrationFilter;
//import org.springframework.security.core.context.SecurityContext;
//
//public class SessionFixationProtectionFilter extends SessionFixationProtectionFilter {
//	
//	static final String FILTER_APPLIED = "__spring_security_session_fixation_filter_applied";
//	private static final String IGNORE_SESSION_RETURN_PARAMETER = "wsdl";
//	
//	private String authenticationUrl = "/services/AuthenticationWS";
//	
//    public void setAuthenticationUrl(String authenticationUrl) {
//		this.authenticationUrl = authenticationUrl;
//	}
//
//	protected void doFilterHttp(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
//            throws IOException, ServletException {
//        // Session fixation isn't a problem if there's no session
//        if(request.getSession(false) == null || request.getAttribute(SessionFixationProtectionFilter.FILTER_APPLIED) != null || !request.isRequestedSessionIdValid()) {
//            chain.doFilter(request, response);
//            return;
//        }
//        
//        request.setAttribute(FILTER_APPLIED, Boolean.TRUE);
//
//        HttpSession session = request.getSession();
//        SecurityContext sessionSecurityContext = 
//            (SecurityContext) session.getAttribute(HttpSessionContextIntegrationFilter.SPRING_SECURITY_CONTEXT_KEY);
//        
//		if (sessionSecurityContext == null
//				&& request.getRequestURI().contains(this.authenticationUrl)
//				&& (request.getQueryString() == null || !request.getQueryString().toLowerCase().contains(IGNORE_SESSION_RETURN_PARAMETER))) {
//            // The user has been authenticated during the current request, so do the session migration
//            startNewSessionIfRequired(request, response);
//        }
//
//        chain.doFilter(request, response);
//    }
//
//}
