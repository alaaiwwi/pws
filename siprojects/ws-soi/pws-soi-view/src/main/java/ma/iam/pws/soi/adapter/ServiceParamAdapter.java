package ma.iam.pws.soi.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ma.iam.pws.soi.beans.contract.BContractedServicesAll;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.services.AluTmchangeServices;
import ma.iam.pws.soi.beans.services.ConfServiceBean;
import ma.iam.pws.soi.beans.services.DebitConfBean;
import ma.iam.pws.soi.beans.services.DepSrvPrmBean;
import ma.iam.pws.soi.beans.services.ParamValueBean;
import ma.iam.pws.soi.beans.services.ParamValueBeanPub;
import ma.iam.pws.soi.beans.services.PrmHideConfBean;
import ma.iam.pws.soi.beans.services.ValueBeanPub;
import ma.iam.pws.soi.beans.services.VoipConfBean;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;

import org.apache.log4j.Logger;

import com.alcatel.ccl.client.CMSException;

public class ServiceParamAdapter {
	/** Instance du LOGGER */
	private static final Logger logger = Logger
			.getLogger(ServiceParamAdapter.class);

	public static Map getServiceParamMap(HashMap<String, Object>[] paramsOut) {
		Map<Long, ConfServiceBean> mapSvcParam = new HashMap<Long, ConfServiceBean>();
		for (int i = 0; i < paramsOut.length; i++) {
			ConfServiceBean nwSvcBean = new ConfServiceBean();
			HashMap<String, Object> hashMap = paramsOut[i];
			nwSvcBean.setSccode(new Long(hashMap.get("SCCODE").toString())
					.longValue());
			nwSvcBean.setSncode(new Long(hashMap.get("SNCODE").toString())
					.longValue());
			nwSvcBean.setSncode_pub(hashMap.get("SNCODE_PUB").toString());
			nwSvcBean.setSccode_pub(hashMap.get("SCCODE_PUB").toString());
			HashMap<String, Object>[] hashMapParamList = (HashMap<String, Object>[]) hashMap
					.get("NUM_PARAM");
			List<ParamValueBean> paramforService = new ArrayList<ParamValueBean>();
			for (int j = 0; j < hashMapParamList.length; j++) {
				HashMap<String, Object> hashMapParam = hashMapParamList[j];
				ParamValueBean prmBean = new ParamValueBean();
				prmBean.setBe_name(hashMapParam.get("BE_NAME") != null ? hashMapParam
						.get("BE_NAME").toString() : null);
				prmBean.setData_type(hashMapParam.get("DATA_TYPE").toString());
				prmBean.setDef_value(hashMapParam.get("DEF_VALUE") != null ? hashMapParam
						.get("DEF_VALUE").toString() : null);
				prmBean.setFormat(hashMapParam.get("FORMAT") != null ? hashMapParam
						.get("FORMAT").toString() : null);
				prmBean.setMandatory_value(Boolean.parseBoolean(hashMapParam
						.get("MANDATORY_VALUE").toString()));
				prmBean.setMax_values(new Integer(hashMapParam
						.get("MAX_VALUES").toString()));
				prmBean.setMin_values(new Integer(hashMapParam
						.get("MIN_VALUES").toString()));
				prmBean.setMult_value_ind(Boolean.parseBoolean(hashMapParam
						.get("MULT_VALUE_IND").toString()));
				prmBean.setNet_ind(Boolean.parseBoolean(hashMapParam.get(
						"NET_IND").toString()));
				prmBean.setOverride_ind(Boolean.parseBoolean(hashMapParam.get(
						"OVERRIDE_IND").toString()));
				prmBean.setPicture_format(hashMapParam.get("PICTURE_FORMAT") != null ? hashMapParam
						.get("PICTURE_FORMAT").toString() : null);
				prmBean.setPort_ind(Boolean.parseBoolean(hashMapParam.get(
						"PORT_IND").toString()));
				prmBean.setPred_dependent(Boolean.parseBoolean(hashMapParam
						.get("PRED_DEPENDENT").toString()));
				prmBean.setPrm_des(hashMapParam.get("PRM_DES").toString());
				prmBean.setPrm_id(new Long(hashMapParam.get("PRM_ID")
						.toString()));
				prmBean.setPrm_id_pub(hashMapParam.get("PRM_ID_PUB").toString());
				prmBean.setPrm_no(new Long(hashMapParam.get("PRM_NO")
						.toString()));
				prmBean.setType(hashMapParam.get("TYPE").toString());
				paramforService.add(prmBean);
			}
			nwSvcBean.setNUM_PARAM(paramforService);
			mapSvcParam.put(new Long(hashMap.get("SNCODE").toString()),
					nwSvcBean);
			logger.info("nwSvcBean : " + nwSvcBean);
		}
		return mapSvcParam;
	}

	public static ParamValueBeanPub getParamValueBeanPub(
			HashMap<String, Object> paramsOut, ConfServiceBean cfgService) {
		ParamValueBeanPub prmvalueBean = new ParamValueBeanPub();
		prmvalueBean
				.setData_type(paramsOut.get("DATA_TYPE") != null ? paramsOut
						.get("DATA_TYPE").toString() : null);
		prmvalueBean
				.setDef_value(paramsOut.get("DEF_VALUE") != null ? paramsOut
						.get("DEF_VALUE").toString() : null);
		prmvalueBean.setMandatory_value(Boolean.parseBoolean(paramsOut.get(
				"MANDATORY_VALUE").toString()));
		prmvalueBean.setMax_values(new Integer(paramsOut.get("MAX_VALUES")
				.toString()));
		prmvalueBean.setMin_values(new Integer(paramsOut.get("MIN_VALUES")
				.toString()));
		prmvalueBean.setPrm_des(paramsOut.get("PRM_DES").toString());
		prmvalueBean.setPrm_id(new Long(paramsOut.get("PRM_ID").toString()));
		prmvalueBean.setPrm_id_pub(paramsOut.get("PRM_ID_PUB").toString());
		prmvalueBean.setPrm_no(paramsOut.get("PRM_NO") != null ? new Long(
				paramsOut.get("PRM_NO").toString()) : null);
		prmvalueBean.setType(paramsOut.get("TYPE").toString());
		if (!prmvalueBean.getType().equals("DF")) {
			@SuppressWarnings("unchecked")
			HashMap<String, Object>[] hashMapValueList = (HashMap<String, Object>[]) paramsOut
					.get("N_VALUES");
			List<ValueBeanPub> listValue = new ArrayList<ValueBeanPub>();
			for (int i = 0; i < hashMapValueList.length; i++) {
				ValueBeanPub nwValueBean = new ValueBeanPub();
				boolean toPub = true;
				HashMap<String, Object> hashMap = hashMapValueList[i];
				nwValueBean.setValue(hashMap.get("VALUE") != null ? hashMap
						.get("VALUE").toString() : null);
				nwValueBean
						.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
								.get("VALUE_DES").toString() : null);
				// >******* Updated, A.ESSA QC12440
				if (nwValueBean.getValue_des().equals("")
						&& prmvalueBean.getType().equals("CB")) {
					nwValueBean.setValue_des(hashMap.get("VALUE").toString());
				}
				// <******* Updated, A.ESSA QC12440
				nwValueBean
						.setValue_pub(hashMap.get("VALUE_PUB") != null ? hashMap
								.get("VALUE_PUB").toString() : null);
				nwValueBean
						.setValue_seqno(hashMap.get("VALUE_SEQNO") != null ? new Long(
								hashMap.get("VALUE_SEQNO").toString()) : null);
				// >******* Update, A.ESSA DI6717
				// boolean restricted = false;
				// for(Entry<Long, PrmHideConfBean> entry :
				// SoiUtils.CACHE_PRM_HIDE_CONFIG.entrySet()) {
				// PrmHideConfBean prmCfgBean = entry.getValue();
				// if (!restricted && "H".equals(prmCfgBean.getShow_conf())
				// && prmCfgBean.getSncode().equals(cfgService.getSncode())
				// && prmCfgBean.getPrm_id().equals(prmvalueBean.getPrm_id())
				// &&
				// (prmCfgBean.getValue_des().equals(nwValueBean.getValue_des())))
				// {
				// toPub=false;
				// break;
				// }else if ("S".equals(prmCfgBean.getShow_conf())
				// && prmCfgBean.getSncode().equals(cfgService.getSncode())
				// && prmCfgBean.getPrm_id().equals(prmvalueBean.getPrm_id())
				// &&
				// (prmCfgBean.getValue_des().equals(nwValueBean.getValue_des())))
				// {
				// listValue.add(nwValueBean);
				// restricted=true;
				// break;
				// }
				// }
				// if (!restricted && toPub) {
				listValue.add(nwValueBean);
				// }
				// <******* Update, A.ESSA DI6717
			}
			prmvalueBean.setVALUES(listValue);
		}
		logger.info("prmvalueBean : " + prmvalueBean);
		return prmvalueBean;
	}

	public static ParamValueBeanPub getParamValueBeanPubForInt3G(
			HashMap<String, Object> paramsOut, ConfServiceBean cfgService,
			BcontractAll contractmodel, SOIService soiComm)
			throws CMSException, FaultWS {
		ParamValueBeanPub prmvalueBean = new ParamValueBeanPub();
		prmvalueBean
				.setData_type(paramsOut.get("DATA_TYPE") != null ? paramsOut
						.get("DATA_TYPE").toString() : null);
		prmvalueBean
				.setDef_value(paramsOut.get("DEF_VALUE") != null ? paramsOut
						.get("DEF_VALUE").toString() : null);
		prmvalueBean.setMandatory_value(Boolean.parseBoolean(paramsOut.get(
				"MANDATORY_VALUE").toString()));
		prmvalueBean.setMax_values(new Integer(paramsOut.get("MAX_VALUES")
				.toString()));
		prmvalueBean.setMin_values(new Integer(paramsOut.get("MIN_VALUES")
				.toString()));
		prmvalueBean.setPrm_des(paramsOut.get("PRM_DES").toString());
		prmvalueBean.setPrm_id(new Long(paramsOut.get("PRM_ID").toString()));
		prmvalueBean.setPrm_id_pub(paramsOut.get("PRM_ID_PUB").toString());
		prmvalueBean.setPrm_no(paramsOut.get("PRM_NO") != null ? new Long(
				paramsOut.get("PRM_NO").toString()) : null);
		prmvalueBean.setType(paramsOut.get("TYPE").toString());
		if (!prmvalueBean.getType().equals("DF")) {
			@SuppressWarnings("unchecked")
			HashMap<String, Object>[] hashMapValueList = (HashMap<String, Object>[]) paramsOut
					.get("N_VALUES");
			List<ValueBeanPub> listValue = new ArrayList<ValueBeanPub>();
			for (int i = 0; i < hashMapValueList.length; i++) {
				ValueBeanPub nwValueBean = new ValueBeanPub();
				boolean toPub = true;
				HashMap<String, Object> hashMap = hashMapValueList[i];
				nwValueBean.setValue(hashMap.get("VALUE") != null ? hashMap
						.get("VALUE").toString() : null);
				nwValueBean
						.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
								.get("VALUE_DES").toString() : null);
				nwValueBean
						.setValue_pub(hashMap.get("VALUE_PUB") != null ? hashMap
								.get("VALUE_PUB").toString() : null);
				nwValueBean
						.setValue_seqno(hashMap.get("VALUE_SEQNO") != null ? new Long(
								hashMap.get("VALUE_SEQNO").toString()) : null);
				String debitSearch = nwValueBean.getValue_des() != null ? nwValueBean
						.getValue_des() : "";

				if (containsPer(SoiUtils.debitConf, nwValueBean.getValue_des())) {

					toPub = false;
					for (Entry<Long, DebitConfBean> entry : SoiUtils.CACHE_DEBIT_CONFIG
							.entrySet()) {

						if (toPub)
							break;

						Long cle = entry.getKey();

						DebitConfBean debitCfgBean = entry.getValue();
						if (debitCfgBean.getTm_shdes() != null
								&& !debitCfgBean.getTm_shdes()
										.equalsIgnoreCase(
												contractmodel.getRPCODE_PUB())) {
							continue;
						}

						if (!toPub
								&& new Long(debitCfgBean.getPrm_type()) < 0
								&& nwValueBean.getValue_des().equalsIgnoreCase(
										debitCfgBean.getDebit())) {
							listValue.add(nwValueBean);
							toPub = true;
							break;
						}
						List<BContractedServicesAll> csm = contractmodel
								.getContractedServices();
						for (Iterator<BContractedServicesAll> iterator = csm
								.iterator(); iterator.hasNext();) {
							BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
									.next();
							logger.debug("=> getEligibleDebit() : checking service :"
									+ bContractedServices.getSNCODE_PUB());
							if (bContractedServices.getCO_SN_STATUS()
									.toString()
									.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
									&& bContractedServices.getSNCODE_PUB() != null
									&& bContractedServices.getSNCODE_PUB()
											.equals(debitCfgBean.getSn_shdes())
									&& nwValueBean.getValue_des().equals(
											debitCfgBean.getDebit())) {
								if (!toPub
										&& "3".equals(debitCfgBean
												.getPrm_type())) {
									listValue.add(nwValueBean);
									toPub = true;
									break;
								} else {
									String value_des = GetValueDesServicefromParameters(
											debitCfgBean.getSn_shdes(),
											contractmodel, soiComm,
											debitCfgBean.getPrm_shdes());
									if (!toPub
											&& value_des != null
											&& value_des.equals(debitCfgBean
													.getValue_des())) {
										listValue.add(nwValueBean);
										toPub = true;
										break;
									}
								}
							}
						}

					}
				} else {
					listValue.add(nwValueBean);
				}
			}
			prmvalueBean.setVALUES(listValue);
		}
		logger.info("prmvalueBean : " + prmvalueBean);
		return prmvalueBean;
	}

	// >******* Update, A.ESSA DI6717
	public static Map getCloudConfBeanMap(HashMap<String, Object>[] paramsOut) {
		Map<Long, DebitConfBean> mapDebitConfBeanMap = new HashMap<Long, DebitConfBean>();
		for (int i = 0; i < paramsOut.length; i++) {
			DebitConfBean debitBean = new DebitConfBean();
			HashMap<String, Object> hashMap = paramsOut[i];
			debitBean.setDebit(hashMap.get("CLOUD") != null ? hashMap.get(
					"CLOUD").toString() : null);
			debitBean.setPrm_shdes(hashMap.get("PRM_SHDES") != null ? hashMap
					.get("PRM_SHDES").toString() : null);
			debitBean.setPrm_type(hashMap.get("PRM_TYPE") != null ? hashMap
					.get("PRM_TYPE").toString() : null);
			debitBean.setSn_shdes(hashMap.get("SN_SHDES") != null ? hashMap
					.get("SN_SHDES").toString() : null);
			debitBean.setTm_shdes(hashMap.get("TM_SHDES") != null ? hashMap
					.get("TM_SHDES").toString() : null);
			debitBean.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
					.get("VALUE_DES").toString() : null);
			debitBean
					.setValue_shdes(hashMap.get("VALUE_SHDES") != null ? hashMap
							.get("VALUE_SHDES").toString() : null);
			mapDebitConfBeanMap.put(new Long(i), debitBean);
		}
		return mapDebitConfBeanMap;
	}

	// <******* Update, A.ESSA DI6717
	public static Map getDebitConfBeanMap(HashMap<String, Object>[] paramsOut) {
		Map<Long, DebitConfBean> mapDebitConfBeanMap = new HashMap<Long, DebitConfBean>();
		for (int i = 0; i < paramsOut.length; i++) {
			DebitConfBean debitBean = new DebitConfBean();
			HashMap<String, Object> hashMap = paramsOut[i];
			debitBean.setDebit(hashMap.get("DEBIT") != null ? hashMap.get(
					"DEBIT").toString() : null);
			debitBean.setPrm_shdes(hashMap.get("PRM_SHDES") != null ? hashMap
					.get("PRM_SHDES").toString() : null);
			debitBean.setPrm_type(hashMap.get("PRM_TYPE") != null ? hashMap
					.get("PRM_TYPE").toString() : null);
			debitBean.setSn_shdes(hashMap.get("SN_SHDES") != null ? hashMap
					.get("SN_SHDES").toString() : null);
			debitBean.setTm_shdes(hashMap.get("TM_SHDES") != null ? hashMap
					.get("TM_SHDES").toString() : null);
			debitBean.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
					.get("VALUE_DES").toString() : null);
			debitBean
					.setValue_shdes(hashMap.get("VALUE_SHDES") != null ? hashMap
							.get("VALUE_SHDES").toString() : null);
			mapDebitConfBeanMap.put(new Long(i), debitBean);
		}
		return mapDebitConfBeanMap;
	}

	// >******* Update, A.ESSA DI6717
	public static Map<Long, AluTmchangeServices> getTMCHANGESERVICESMap(
			HashMap<String, Object>[] paramsOut) {
		Map<Long, AluTmchangeServices> mapAluTmchangeServices = new HashMap<Long, AluTmchangeServices>();
		for (int i = 0; i < paramsOut.length; i++) {
			AluTmchangeServices prmHideCfgbean = new AluTmchangeServices();
			HashMap<String, Object> hashMap = paramsOut[i];
			prmHideCfgbean.setTmcode(hashMap.get("TMCODE") != null ? new Long(
					hashMap.get("TMCODE").toString()) : null);
			prmHideCfgbean.setPrmId(hashMap.get("PRM_ID") != null ? new Long(
					hashMap.get("PRM_ID").toString()) : null);
			prmHideCfgbean.setSncode(hashMap.get("SNCODE") != null ? new Long(
					hashMap.get("SNCODE").toString()) : null);
			prmHideCfgbean.setSpcode(hashMap.get("SPCODE") != null ? new Long(
					hashMap.get("SPCODE").toString()) : null);
			prmHideCfgbean.setValue(hashMap.get("VALUE") != null ? new Long(
					hashMap.get("VALUE").toString()) : null);
			prmHideCfgbean
					.setValueDes(hashMap.get("VALUE_DES") != null ? hashMap
							.get("VALUE_DES").toString() : null);

			mapAluTmchangeServices.put(new Long(i), prmHideCfgbean);
		}
		return mapAluTmchangeServices;
	}

	// <******* Update, A.ESSA DI6717
	public static Map<Long, PrmHideConfBean> getPRMHIDECONFIGMap(
			HashMap<String, Object>[] paramsOut) {
		Map<Long, PrmHideConfBean> mapPrmHideConfBean = new HashMap<Long, PrmHideConfBean>();
		for (int i = 0; i < paramsOut.length; i++) {
			PrmHideConfBean prmHideCfgbean = new PrmHideConfBean();
			HashMap<String, Object> hashMap = paramsOut[i];
			prmHideCfgbean.setPrm_id(hashMap.get("PRM_ID") != null ? new Long(
					hashMap.get("PRM_ID").toString()) : null);
			prmHideCfgbean
					.setShow_conf(hashMap.get("SHOW_CONF") != null ? hashMap
							.get("SHOW_CONF").toString() : null);
			prmHideCfgbean.setSncode(hashMap.get("SNCODE") != null ? new Long(
					hashMap.get("SNCODE").toString()) : null);
			prmHideCfgbean.setTmcode(hashMap.get("TMCODE") != null ? new Long(
					hashMap.get("TMCODE").toString()) : null);
			prmHideCfgbean.setValue(hashMap.get("VALUE") != null ? hashMap.get(
					"VALUE").toString() : null);
			prmHideCfgbean
					.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
							.get("VALUE_DES").toString() : null);
			// >******* Update, A.ESSA DI6717
			prmHideCfgbean.setPrgcode(hashMap.get("PRGCODE") != null ? hashMap
					.get("PRGCODE").toString() : null);
			// <******* Update, A.ESSA DI6717
			mapPrmHideConfBean.put(new Long(i), prmHideCfgbean);
		}
		return mapPrmHideConfBean;
	}

	// >******* Update, A.ESSA DI6717
	public static Map<Long, DepSrvPrmBean> getDepSrvPrmBeanMap(
			HashMap<String, Object>[] paramsOut) {
		Map<Long, DepSrvPrmBean> mapDepSrvPrmBean = new HashMap<Long, DepSrvPrmBean>();
		for (int i = 0; i < paramsOut.length; i++) {
			DepSrvPrmBean depSrvPrmBean = new DepSrvPrmBean();
			HashMap<String, Object> hashMap = paramsOut[i];
			depSrvPrmBean.setPrm_id(hashMap.get("PRM_ID") != null ? new Long(
					hashMap.get("PRM_ID").toString()) : null);
			depSrvPrmBean.setSncode(hashMap.get("SNCODE") != null ? new Long(
					hashMap.get("SNCODE").toString()) : null);
			depSrvPrmBean.setTmcode(hashMap.get("TMCODE") != null ? new Long(
					hashMap.get("TMCODE").toString()) : null);
			depSrvPrmBean.setValue(hashMap.get("VALUE") != null ? hashMap.get(
					"VALUE").toString() : null);
			depSrvPrmBean
					.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
							.get("VALUE_DES").toString() : null);

			depSrvPrmBean
					.setDep_prm_id(hashMap.get("DEP_PRM_ID") != null ? new Long(
							hashMap.get("DEP_PRM_ID").toString()) : null);
			depSrvPrmBean
					.setDep_sncode(hashMap.get("DEP_SNCODE") != null ? new Long(
							hashMap.get("DEP_SNCODE").toString()) : null);
			depSrvPrmBean
					.setDep_value(hashMap.get("DEP_VALUE") != null ? hashMap
							.get("DEP_VALUE").toString() : null);
			depSrvPrmBean
					.setDep_value_des(hashMap.get("DEP_VALUE_DES") != null ? hashMap
							.get("DEP_VALUE_DES").toString() : null);
			depSrvPrmBean
					.setDep_prm_no(hashMap.get("DEP_PRM_NO") != null ? new Long(
							hashMap.get("DEP_PRM_NO").toString()) : null);
			depSrvPrmBean.setPrm(hashMap.get("PRM") != null ? hashMap
					.get("PRM").toString() : null);

			mapDepSrvPrmBean.put(new Long(i), depSrvPrmBean);
		}
		return mapDepSrvPrmBean;
	}

	// <******* Update, A.ESSA DI6717

	public static Map<Long, VoipConfBean> getVOIPCONFIGMap(
			HashMap<String, Object>[] paramsOut) {
		Map<Long, VoipConfBean> mapVoipConfBean = new HashMap<Long, VoipConfBean>();
		for (int i = 0; i < paramsOut.length; i++) {
			VoipConfBean voipCfgbean = new VoipConfBean();
			HashMap<String, Object> hashMap = paramsOut[i];
			voipCfgbean.setPrm_id(hashMap.get("PRM_ID") != null ? new Long(
					hashMap.get("PRM_ID").toString()) : null);
			voipCfgbean.setSncode(hashMap.get("SNCODE") != null ? new Long(
					hashMap.get("SNCODE").toString()) : null);
			voipCfgbean.setTmcode(hashMap.get("TMCODE") != null ? new Long(
					hashMap.get("TMCODE").toString()) : null);
			voipCfgbean.setValue(hashMap.get("VALUE") != null ? hashMap.get(
					"VALUE").toString() : null);
			voipCfgbean.setValue_des(hashMap.get("VALUE_DES") != null ? hashMap
					.get("VALUE_DES").toString() : null);
			mapVoipConfBean.put(new Long(i), voipCfgbean);
		}
		return mapVoipConfBean;
	}

	/*
	 * Used to managed authorized debit
	 */

	public static List<DebitConfBean> getEligibleDebit(
			BcontractAll contractmodel, SOIService soiComm)
			throws CMSException, FaultWS {

		logger.debug("=> getEligibleDebit()");
		// Check if the cache map is empty
		if ((SoiUtils.CACHE_DEBIT_CONFIG == null)
				|| (SoiUtils.CACHE_DEBIT_CONFIG.isEmpty()))
			return null;
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("CO_ID", contractmodel.getCO_ID());
		HashMap<String, Object> outParams = soiComm.executeCommand(
				SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
		BContractedServicesAdapterAll.buildContractedServicesBean(outParams,
				contractmodel);
		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();
		List<DebitConfBean> res = new ArrayList<DebitConfBean>();

		for (Entry<Long, DebitConfBean> entry : SoiUtils.CACHE_DEBIT_CONFIG
				.entrySet()) {
			Long cle = entry.getKey();
			DebitConfBean debitCfgBean = entry.getValue();
			if (debitCfgBean.getTm_shdes() != null
					&& !contractmodel.getRPCODE_PUB().equals(
							debitCfgBean.getTm_shdes())) {
				continue;
			}
			for (Iterator iterator = csm.iterator(); iterator.hasNext();) {
				BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
						.next();
				logger.debug("=> getEligibleDebit() : checking service :"
						+ bContractedServices);
				if (bContractedServices.getCOS_STATUS().toString()
						.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
						&& bContractedServices.getSNCODE_PUB() != null
						&& bContractedServices.getSNCODE_PUB().equals(
								debitCfgBean.getSn_shdes())) {
					if ("3".equals(debitCfgBean.getPrm_type())) {
						res.add(debitCfgBean);
					} else {
						String value_des = GetValueDesServicefromParameters(
								debitCfgBean.getSn_shdes(), contractmodel,
								soiComm, debitCfgBean.getPrm_shdes());
						if (value_des != null
								&& value_des
										.equals(debitCfgBean.getValue_des())) {
							res.add(debitCfgBean);
						}
					}
				}
			}
		}
		logger.debug("<= getEligibleDebit()");
		return res;
	}

	public static String GetValueDesServicefromParameters(String snShdes,
			BcontractAll bcontract, SOIService soiComm, String prm_id_pub)
			throws CMSException, FaultWS {
		logger.debug("=> GetValueDesServicefromParameters()");

		boolean isServiceWithParameters = false;
		HashMap serviceParametersReadOutParams;

		HashMap inParams = new HashMap();
		String snCodeParamName = "SNCODE_PUB";
		logger.debug("SNCODE=" + snShdes);
		inParams.put(snCodeParamName, snShdes);
		inParams.put("CO_ID", bcontract.getCO_ID());
		inParams.put("PROFILE_ID", new Long(0));

		serviceParametersReadOutParams = soiComm.executeCommand(
				"CONTRACT_SERVICE_PARAMETERS.READ", inParams);

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_PARAMS");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			for (int i = 0; i < hmNumServ.length; i++) {
				if (hmNumServ[i].get("PRM_ID_PUB").toString()
						.equals(prm_id_pub)) {
					HashMap[] multValueInd = (HashMap[]) hmNumServ[i]
							.get("MULT_VALUES");
					// if(multValueInd[0].get("VALUE").toString().contains("3.6")){
					return multValueInd[0].get("VALUE_DES").toString();
				}
				// break;
			}
		}
		logger.debug("<= GetValueDesServicefromParameters() : CoID"
				+ bcontract.getCO_ID() + " CO ID PUB"
				+ bcontract.getCO_ID_PUB() + " PRM_ID_PUB " + prm_id_pub);
		return null;
	}

	/*
	 * Used to managed authorized debit
	 */
	public static String GetValueDesServicefromParametersId(String sncode,
			BcontractAll bcontract, SOIService soiComm, String prm_id)
			throws CMSException, FaultWS {
		logger.debug("=> GetValueDesServicefromParameters()");

		boolean isServiceWithParameters = false;
		HashMap serviceParametersReadOutParams;

		HashMap inParams = new HashMap();
		String snCodeParamName = "SNCODE";
		logger.debug("SNCODE=" + sncode);
		inParams.put(snCodeParamName, Long.parseLong(sncode));
		inParams.put("CO_ID", bcontract.getCO_ID());
		inParams.put("PROFILE_ID", new Long(0));

		serviceParametersReadOutParams = soiComm.executeCommand(
				"CONTRACT_SERVICE_PARAMETERS.READ", inParams);

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_PARAMS");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			for (int i = 0; i < hmNumServ.length; i++) {
				if (hmNumServ[i].get("PRM_ID").toString().equals(prm_id)) {
					HashMap[] multValueInd = (HashMap[]) hmNumServ[i]
							.get("MULT_VALUES");
					// if(multValueInd[0].get("VALUE").toString().contains("3.6")){
					return multValueInd[0].get("VALUE_DES").toString();
				}
				// break;
			}
		}
		logger.debug("<= GetValueDesServicefromParameters() : CoID"
				+ bcontract.getCO_ID() + " CO ID PUB"
				+ bcontract.getCO_ID_PUB() + " PRM_ID_PUB " + prm_id);
		return null;
	}

	public static boolean isFreeVoip(BcontractAll contractmodel,
			SOIService soiComm) throws CMSException, FaultWS {

		logger.debug("=> getEligibleVoip()");
		// Check if the cache map is empty
		if ((SoiUtils.CACHE_PRM_VOIP_CONFIG == null)
				|| (SoiUtils.CACHE_PRM_VOIP_CONFIG.isEmpty()))
			return false;
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("CO_ID", contractmodel.getCO_ID());
		HashMap<String, Object> outParams = soiComm.executeCommand(
				SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
		BContractedServicesAdapterAll.buildContractedServicesBean(outParams,
				contractmodel);
		List<BContractedServicesAll> csm = contractmodel
				.getContractedServices();
		for (Entry<Long, VoipConfBean> entry : SoiUtils.CACHE_PRM_VOIP_CONFIG
				.entrySet()) {
			Long cle = entry.getKey();
			VoipConfBean voipCfgBean = entry.getValue();
			if (voipCfgBean.getTmcode() != null
					&& !(contractmodel.getRPCODE() == voipCfgBean.getTmcode()
							.longValue())) {
				continue;
			}
			for (Iterator iterator = csm.iterator(); iterator.hasNext();) {
				BContractedServicesAll bContractedServices = (BContractedServicesAll) iterator
						.next();
				logger.debug("=> getEligibleVoip() : checking service :"
						+ bContractedServices);
				if (bContractedServices.getCOS_STATUS().toString()
						.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
						&& bContractedServices.getSNCODE() == voipCfgBean
								.getSncode()) {
					String value_des = GetValueIdServicefromParametersId(
							bContractedServices.getSNCODE_PUB(), contractmodel,
							soiComm, voipCfgBean.getPrm_id().toString());
					if (value_des != null
							&& value_des.equals(voipCfgBean.getValue_des())) {
						return true;
					}

				}
			}
		}
		logger.debug("<= getEligibleVoip()");
		return false;
	}

	public static String GetValueIdServicefromParametersId(String snShdes,
			BcontractAll bcontract, SOIService soiComm, String prm_id)
			throws CMSException, FaultWS {
		logger.debug("=> GetValueDesServicefromParameters()");

		boolean isServiceWithParameters = false;
		HashMap serviceParametersReadOutParams;

		HashMap inParams = new HashMap();
		String snCodeParamName = "SNCODE_PUB";
		logger.debug("SNCODE=" + snShdes);
		inParams.put(snCodeParamName, snShdes);
		inParams.put("CO_ID", bcontract.getCO_ID());
		inParams.put("PROFILE_ID", new Long(0));

		serviceParametersReadOutParams = soiComm.executeCommand(
				"CONTRACT_SERVICE_PARAMETERS.READ", inParams);

		HashMap[] hmNumServ = (HashMap[]) serviceParametersReadOutParams
				.get("NUM_PARAMS");

		if ((hmNumServ != null) && (hmNumServ.length != 0)
				&& (hmNumServ[0] != null)) {
			for (int i = 0; i < hmNumServ.length; i++) {
				if (hmNumServ[i].get("PRM_ID").toString().equals(prm_id)) {
					HashMap[] multValueInd = (HashMap[]) hmNumServ[i]
							.get("MULT_VALUES");
					// if(multValueInd[0].get("VALUE").toString().contains("3.6")){
					return multValueInd[0].get("VALUE_DES").toString();
				}
				// break;
			}
		}
		logger.debug("<= GetValueDesServicefromParameters() : CoID"
				+ bcontract.getCO_ID() + " CO ID PUB"
				+ bcontract.getCO_ID_PUB() + " PRM_ID_PUB " + prm_id);
		return null;
	}

	private static boolean equalsPer(String source, String dest, long per) {

		if (dest != null && dest.length() >= ((source.length() * per) / 100))
			for (int i = 0; i < source.length(); i++) {
				if (i >= ((source.length() * per) / 100))
					return true;
				if (source.charAt(i) != dest.charAt(i))
					break;
			}
		return false;
	}

	private static boolean containsPer(List<String> list, String dest) {
		for (String source : list)
			if (equalsPer(source, dest, 90)) {
				return true;
			}

		return false;
	}
}
