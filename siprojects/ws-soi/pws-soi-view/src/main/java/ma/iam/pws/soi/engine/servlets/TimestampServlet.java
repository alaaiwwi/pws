package ma.iam.pws.soi.engine.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Retourne la date du serveur.
 * 
 * <br/><br/>
 * Projet : Mutualisation
 * Cr��e le 15 juil. 2011
 * 
 * @author mei
 *
 */
public class TimestampServlet  extends HttpServlet {

	private static final long serialVersionUID = -204340641234437873L;
	
	/**
	 * (non javadoc)
	 * See @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse).
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		out.println(String.valueOf(System.currentTimeMillis()));
		out.flush();
	}

}