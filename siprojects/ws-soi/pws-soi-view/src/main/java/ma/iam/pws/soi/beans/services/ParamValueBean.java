package ma.iam.pws.soi.beans.services;

public class ParamValueBean {
	@Override
	public String toString() {
		return "ParamValueBean [be_name=" + be_name + ", data_type="
				+ data_type + ", def_value=" + def_value + ", format=" + format
				+ ", mandatory_value=" + mandatory_value + ", max_values="
				+ max_values + ", min_values=" + min_values
				+ ", mult_value_ind=" + mult_value_ind + ", net_ind=" + net_ind
				+ ", override_ind=" + override_ind + ", picture_format="
				+ picture_format + ", port_ind=" + port_ind
				+ ", pred_dependent=" + pred_dependent + ", prm_des=" + prm_des
				+ ", prm_id=" + prm_id + ", prm_id_pub=" + prm_id_pub
				+ ", prm_no=" + prm_no + ", type=" + type + "]";
	}
	private  String    be_name;
	private String    data_type;
	private String    def_value;
	private String    format;
	public String getBe_name() {
		return be_name;
	}
	public void setBe_name(String be_name) {
		this.be_name = be_name;
	}
	public String getData_type() {
		return data_type;
	}
	public void setData_type(String data_type) {
		this.data_type = data_type;
	}
	public String getDef_value() {
		return def_value;
	}
	public void setDef_value(String def_value) {
		this.def_value = def_value;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public boolean isMandatory_value() {
		return mandatory_value;
	}
	public void setMandatory_value(boolean mandatory_value) {
		this.mandatory_value = mandatory_value;
	}
	public int getMax_values() {
		return max_values;
	}
	public void setMax_values(int max_values) {
		this.max_values = max_values;
	}
	public int getMin_values() {
		return min_values;
	}
	public void setMin_values(int min_values) {
		this.min_values = min_values;
	}
	public boolean isMult_value_ind() {
		return mult_value_ind;
	}
	public void setMult_value_ind(boolean mult_value_ind) {
		this.mult_value_ind = mult_value_ind;
	}
	public boolean isNet_ind() {
		return net_ind;
	}
	public void setNet_ind(boolean net_ind) {
		this.net_ind = net_ind;
	}
	public boolean isOverride_ind() {
		return override_ind;
	}
	public void setOverride_ind(boolean override_ind) {
		this.override_ind = override_ind;
	}
	public String getPicture_format() {
		return picture_format;
	}
	public void setPicture_format(String picture_format) {
		this.picture_format = picture_format;
	}
	public boolean isPort_ind() {
		return port_ind;
	}
	public void setPort_ind(boolean port_ind) {
		this.port_ind = port_ind;
	}
	public boolean isPred_dependent() {
		return pred_dependent;
	}
	public void setPred_dependent(boolean pred_dependent) {
		this.pred_dependent = pred_dependent;
	}
	public String getPrm_des() {
		return prm_des;
	}
	public void setPrm_des(String prm_des) {
		this.prm_des = prm_des;
	}
	public long getPrm_id() {
		return prm_id;
	}
	public void setPrm_id(long prm_id) {
		this.prm_id = prm_id;
	}
	public String getPrm_id_pub() {
		return prm_id_pub;
	}
	public void setPrm_id_pub(String prm_id_pub) {
		this.prm_id_pub = prm_id_pub;
	}
	public long getPrm_no() {
		return prm_no;
	}
	public void setPrm_no(long prm_no) {
		this.prm_no = prm_no;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private boolean   mandatory_value;
	private int   max_values;
	private int   min_values;
	private boolean   mult_value_ind;
	private boolean   net_ind;
	private boolean   override_ind;
	private String    picture_format;
	private boolean   port_ind;
	private boolean   pred_dependent;
	private String    prm_des;
	private long     prm_id;
	private String    prm_id_pub;
	private long      prm_no;
	private String    type;
}
