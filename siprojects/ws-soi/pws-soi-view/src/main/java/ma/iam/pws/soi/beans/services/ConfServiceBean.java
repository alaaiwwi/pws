package ma.iam.pws.soi.beans.services;

import java.util.List;

/**
 * @author mabchour
 * 
 */
public class ConfServiceBean {

	@Override
	public String toString() {
		return "ConfServiceBean [sncode=" + sncode + ", NUM_PARAM=" + NUM_PARAM
				+ ", sccode=" + sccode + ", sccode_pub=" + sccode_pub
				+ ", sncode_pub=" + sncode_pub + "]";
	}

	private long sncode;

	public long getSncode() {
		return sncode;
	}

	public void setSncode(long sncode) {
		this.sncode = sncode;
	}

	public List<ParamValueBean> getNUM_PARAM() {
		return NUM_PARAM;
	}

	public void setNUM_PARAM(List<ParamValueBean> nUM_PARAM) {
		NUM_PARAM = nUM_PARAM;
	}

	public long getSccode() {
		return sccode;
	}

	public void setSccode(long sccode) {
		this.sccode = sccode;
	}

	public String getSccode_pub() {
		return sccode_pub;
	}

	public void setSccode_pub(String sccode_pub) {
		this.sccode_pub = sccode_pub;
	}

	public String getSncode_pub() {
		return sncode_pub;
	}

	public void setSncode_pub(String sncode_pub) {
		this.sncode_pub = sncode_pub;
	}

	private List<ParamValueBean> NUM_PARAM;
	private long sccode;
	private String sccode_pub;
	private String sncode_pub;
}
