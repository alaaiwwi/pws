package ma.iam.pws.soi.engine.security;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.lhs.ccb.sfw.application.JdbcLogin;
public class PasswordUtils {
	
          
		public static String decryptPassword(String login, String password) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException{
			
			JdbcLogin jdbcLogin = JdbcLogin.getInstance();
			Method decryptPassword = JdbcLogin.class.getDeclaredMethod("decryptPassword", new Class[]{String.class,String.class});
			decryptPassword.setAccessible(true);
			return (String)decryptPassword.invoke(jdbcLogin, new Object[]{login,password});

		}

		public static String getPassword(String login) throws IOException{
			
			String fileName = System.getProperty("BSCS_PASSWD_SOI")!=null?System.getProperty("BSCS_PASSWD_SOI"):System.getenv("BSCS_PASSWD_SOI");
			System.out.println("file name is " + fileName);
			FileInputStream inputStream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(inputStream);
			BufferedReader br= new BufferedReader(new InputStreamReader(in));
			
			String strLine = null;
			while ((strLine = br.readLine()) != null)   {
					if(strLine.startsWith(login+" ") || strLine.startsWith(login+"\t")){
					int start = 0;
					int end = 0;
					strLine = strLine.substring(login.length()+1);
						for(int i =0;i<strLine.length();i++){
						if(strLine.charAt(i)!=' ' && strLine.charAt(i)!='\t' && start==0){
								start=i;
									
						}
						if(strLine.charAt(i)==' ' || strLine.charAt(i)=='\t' || strLine.charAt(i)=='\n'|| strLine.charAt(i)=='\r' || i==strLine.length()-1){
							if(i==strLine.length()-1)i++; 
							if(start!=0){
								end=i;
							return strLine.substring(start,end);
							}
						}
						
					}
				
				}
			}
			in.close();

			return null;
		}
	
		
	}

