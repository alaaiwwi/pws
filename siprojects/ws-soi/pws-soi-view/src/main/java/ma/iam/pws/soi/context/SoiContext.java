package ma.iam.pws.soi.context;

import java.util.HashMap;
import java.util.Map;





import ma.iam.pws.soi.beans.services.ConfServiceBean;
import ma.iam.pws.soi.service.SOIService;

public class SoiContext {

	Map<String , SOIService> contexts = new HashMap<String , SOIService>();

	
	public synchronized final Map<String , SOIService> getContexts() {
		return contexts;
	}

	public  synchronized  final void setContexts(Map<String , SOIService> contexts) {
		 this.contexts = contexts;
	}
	
}
