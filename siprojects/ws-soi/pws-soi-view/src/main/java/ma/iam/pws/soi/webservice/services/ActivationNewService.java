package ma.iam.pws.soi.webservice.services;

import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.errors.FaultWS;

/**
 * @author mounir_mabchour
 *
 */

@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public interface ActivationNewService {

	/**
	 * ActivationNewServices . : WS for activation the new services
	 * 
	 * @param SNCODE internal identifier of service in Input to get back the depencdencies
	 * @param CO_ID internal identifier of contract 
	 */
	@javax.jws.WebMethod
	Bcontract  ActivationNewServices(@javax.jws.WebParam(name="USERNAME")String username,  @javax.jws.WebParam(name="CO_ID") long co_id , @javax.jws.WebParam(name="SNCODE") long sncode) throws FaultWS  ;
}
