package ma.iam.pws.soi.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import ma.iam.pws.soi.beans.contract.BDeactivatedSRV;
import ma.iam.pws.soi.beans.errors.FaultWS;

/**
 * @author Mo.Aissa
 *
 */

@javax.jws.WebService(targetNamespace = "http://services.webservice.ws.iam.ma/")
public interface DeactivateService {

	/**
	 * DeactivateService . : WS user to Deactivate Service for contract
	 * 
	 * @param coId
	 *            internal identifier of contract in billing system
	 * @param SNCODE
	 *            public identifier of Service to Deactivate
	 */
	@javax.jws.WebMethod
	BDeactivatedSRV DeactivateServiceImpl(
			@javax.jws.WebParam(name = "USERNAME") String username,
			@javax.jws.WebParam(name = "CO_ID") long co_id,
			@javax.jws.WebParam(name = "SNCODE") long SNCODE) throws FaultWS;

	/**
	 * byPAss check if the communication its ok/ or not send a fake compte and
	 * chack the return if the compte is not connected
	 * 
	 * @return
	 */
	@WebMethod(operationName = "byPass")
	@WebResult(name = "result")
	Integer modeByPass();
}
