package ma.iam.pws.soi.tree;

/**
 * @author Alaa ESSA
 *
 */

import java.util.ArrayList;
import java.util.List;
@SuppressWarnings({ "unchecked", "deprecation","static-access","rawtypes" })
public class Node {
	private Node root;
	private String data; //SNCODE or Boolean expression name
	private String op; // Operation between/for child/s
	private List<Node> parents;
	private List<Node> childs;
	private boolean isSimple; // is dependency simple or complex
	private Boolean isConnRoot; // is sncode connected to root or not (Start of seperated consistency)
	private List ids;
	private int seqno;
	
	public int getSeqno() {
		return seqno;
	}

	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	public Node(String operation, String data, boolean isSimple, Node root, Long id) {
		this.root = root;
		this.data = data;
		this.op = operation;
		this .isSimple=isSimple;
		this.seqno = this.root.getSeqno()+1;
		this.root.setSeqno(this.root.getSeqno()+1);
		this.ids = new ArrayList();
		this.ids.add(id);
		
		if("0123456789".contains(data.substring(0, 1)) && getNodeFromRoot(data, root, new ArrayList())!=null){
			System.out.println("Be careful, you are creating node already exists".concat(data));
		}
	}
	
	public Node(String operation, String data, boolean isSimple, Node root, List ids) {
		this.root = root;
		this.data = data;
		this.op = operation;
		this .isSimple=isSimple;
		this.seqno = this.root.getSeqno()+1;
		this.root.setSeqno(this.root.getSeqno()+1);
		this.ids = new ArrayList();
		this.ids = ids;
	}
	
	public Node getNodeFromRoot(String data, Node root, ArrayList parsed){
		Node node = null;
		List rootChilds = root.getChilds();
		if(rootChilds!=null)
		for(int i = 0;i<rootChilds.size();i++){
			Node currNode = (Node)rootChilds.get(i);
			if(!parsed.contains(currNode)){
				parsed.add(currNode);
			}
			else{
				continue;
			}
			if(((Node)rootChilds.get(i)).getData().equals(data)){
				node =  (Node)rootChilds.get(i);
				break;
			}				
			else{
				node = getNodeFromRoot(data, (Node)rootChilds.get(i), parsed);
				if(node !=null)
					break;
			}
				
		}
		return node;
			
	}

	public Node() {
		childs = new ArrayList<Node>();
		parents = new ArrayList<Node>();
		isSimple = false;
		seqno = 0;
		//root = new Node();
	}
	
	
	public void addID(Long id) {
		if (ids == null) {
			ids = new ArrayList();
		}
		ids.add(id);
	}
	
	public List getIDs() {
		return this.ids;
	}

	public void setIds(List ids) {
		this.ids = ids;
	}

	public void addChild(Node node) {
		if (childs == null) {
			childs = new ArrayList<Node>();
		}
		childs.add(node);
		node.addParent(this);
		//this.op = op;
	}
	
	public void addParent(Node node) {
		if (parents == null) {
			parents = new ArrayList<Node>();
		}
		parents.add(node);
	}


	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public List<Node> getParents() {
		return parents;
	}

	public void setParents(List<Node> parents) {
		this.parents = parents;
	}

	public List<Node> getChilds() {
		return childs;
	}

	public void setChilds(List<Node> childs) {
		this.childs = childs;
	}

	public boolean isLeaf() {
		return childs==null ||childs.size()==0;
	}
	
	public boolean isConnRoot() {
		if(this.parents==null || this.parents.size()==0)
			return false;
		else{
			for(int i = 0;i<this.parents.size();i++){
				if(this.parents.get(i).equals(this.root))
					return true;
			}
		}
		return false;
	}
	
	public boolean isSimple() {
		return isSimple;
	}

	public void setSimple(boolean isSimple) {
		this.isSimple = isSimple;
	}
	
	public String toString() {
		String op = "null";
		String data = "null";
		if(this.op!=null && this.op!="")
			op = this.op;
		if(this.data!=null && this.data!="")
			data = this.data;
		return "sncode = ".concat(data).concat(", op = ").concat(op);
	}

}
