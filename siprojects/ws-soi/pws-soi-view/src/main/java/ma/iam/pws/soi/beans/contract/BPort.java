package ma.iam.pws.soi.beans.contract;

public class BPort {

	private String COSP_PORTNUM;

	private Integer COSP_STATUS;

	public String getCOSP_PORTNUM() {
		return COSP_PORTNUM;
	}

	public void setCOSP_PORTNUM(String cOSP_PORTNUM) {
		COSP_PORTNUM = cOSP_PORTNUM;
	}

	/**
	 * GET : Port status
	 * 
	 * 1 -> Not active 2 -> Active 6 -> Removed 4 -> Deactive
	 * 
	 * @return
	 */

	public Integer getCOSP_STATUS() {
		return COSP_STATUS;
	}

	/**
	 * SET Port status
	 * 
	 * 1 -> Not active 2 -> Active 6 -> Removed 4 -> Deactive
	 * 
	 * @param cOSP_STATUS
	 */

	public void setCOSP_STATUS(Integer cOSP_STATUS) {
		COSP_STATUS = cOSP_STATUS;
	}

}
