package ma.iam.pws.soi.beans.services;

public class ClassServiceBean {
	
	private long sncode ;
	private long spcode;
	private long tmcode;
	private long clcode;
	
	public ClassServiceBean(long sncode, long spcode, long tmcode, long clcode) {
		super();
		this.sncode = sncode;
		this.spcode = spcode;
		this.tmcode = tmcode;
		this.clcode = clcode;
	}
	public long getSncode() {
		return sncode;
	}
	public void setSncode(long sncode) {
		this.sncode = sncode;
	}

	public long getSpcode() {
		return spcode;
	}
	public void setSpcode(long spcode) {
		this.spcode = spcode;
	}
	public long getTmcode() {
		return tmcode;
	}
	public void setTmcode(long tmcode) {
		this.tmcode = tmcode;
	}
	public long getClcode() {
		return clcode;
	}
	public void setClcode(long clcode) {
		this.clcode = clcode;
	}
	public int compareTo(ClassServiceBean o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassServiceBean other = (ClassServiceBean) obj;
		if (clcode != other.clcode)
			return false;
		if (sncode != other.sncode)
			return false;
		if (spcode != other.spcode)
			return false;
		if (tmcode != other.tmcode)
			return false;
		return true;
	}
}
