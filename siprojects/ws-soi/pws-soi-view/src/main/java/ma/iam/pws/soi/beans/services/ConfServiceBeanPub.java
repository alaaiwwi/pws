package ma.iam.pws.soi.beans.services;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlRootElement(name = "ConfServiceBeanPub")
@XmlType(name="ConfServiceBeanPub" , propOrder = { "sccode", "sncode", "sccode_pub" , "sncode_pub" , "PARAMETERS"})
public class ConfServiceBeanPub {
	public List<ParamValueBeanPub> getPARAMETERS() {
		return PARAMETERS;
	}
	public void setPARAMETERS(List<ParamValueBeanPub> nUM_PARAM) {
		PARAMETERS = nUM_PARAM;
	}
	@Override
	public String toString() {
		return "ConfServiceBean [sncode=" + sncode + ", NUM_PARAM=" + PARAMETERS
				+ ", sccode=" + sccode + ", sccode_pub=" + sccode_pub
				+ ", sncode_pub=" + sncode_pub + "]";
	}

	private long sncode;
	
	
	
	public ConfServiceBeanPub() {
		super();
	}
	public ConfServiceBeanPub(ConfServiceBean cfgSvcbean ) {
		this.sccode=cfgSvcbean.getSccode();
		this.sncode=cfgSvcbean.getSncode();
		this.sccode_pub=cfgSvcbean.getSccode_pub();
		this.sncode_pub=cfgSvcbean.getSncode_pub();
	}
	public long getSncode() {
		return sncode;
	}

	public void setSncode(long sncode) {
		this.sncode = sncode;
	}


	public long getSccode() {
		return sccode;
	}

	public void setSccode(long sccode) {
		this.sccode = sccode;
	}

	public String getSccode_pub() {
		return sccode_pub;
	}

	public void setSccode_pub(String sccode_pub) {
		this.sccode_pub = sccode_pub;
	}

	public String getSncode_pub() {
		return sncode_pub;
	}

	public void setSncode_pub(String sncode_pub) {
		this.sncode_pub = sncode_pub;
	}

	private List<ParamValueBeanPub> PARAMETERS;
	private long sccode;
	private String sccode_pub;
	private String sncode_pub;
}
