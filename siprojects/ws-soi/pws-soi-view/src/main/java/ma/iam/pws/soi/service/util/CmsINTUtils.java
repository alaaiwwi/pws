package ma.iam.pws.soi.service.util;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ma.iam.pws.soi.beans.contract.BDeactivatedSRV;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.service.SOIService;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

/**
 * @author MMEnnaifer
 * 
 */
public class CmsINTUtils {

	public static DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static ArrayList GetServiceParametersDefaultFirstValue(ServiceUtils service, Contract contract,
			boolean isServiceToBeActiveNow, SOIService soiComm, bNewServicesEx servicesEx, Properties msgProperties,
			List<bNewServices> Services) throws CMSException, FaultWS {

		ArrayList array = new ArrayList();
		ServiceUtils.CheckServiceWithParameters(service, contract, null, soiComm, msgProperties, servicesEx,
				SoiUtils.SERVICE_STATUT_UNDEFINED, Services);

		for (Iterator ItPrm = servicesEx.getBNewParamValueEx().iterator(); ItPrm.hasNext();) {
			HashMap paramvalue = new HashMap();
			bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();
			HashMap[] tabHmMultValues = null;
			HashMap<String, Object> hmParamValues = new HashMap<String, Object>();

			tabHmMultValues = new HashMap[pRmVal.getBNewValueEx().size()];

			for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
				bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
				if (vaLue.getVALUE_DES() != null) {
					paramvalue.put("VALUE", vaLue.getVALUE());
					paramvalue.put("VALUE_DES", vaLue.getVALUE_DES());
				}
			}

			paramvalue.put("PRM_ID", pRmVal.getPRM_ID());
			paramvalue.put("PRM_NO", pRmVal.getPRM_NO());

			array.add(paramvalue);
		}
		return array;
	}

	@SuppressWarnings("unchecked")
	public static String DeactiveServiceXML(ArrayList SNCODES, ArrayList SNCODES_EXCLUDED, Long co_id,
			Long new_rpcode) {
		if (co_id == null)
			return null;
		if (((SNCODES == null) || (SNCODES.size() == 0)) && new_rpcode == null)
			return null;
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			System.setProperty("javax.xml.transform.TransformerFactory",
					"com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
			Element rootcustomer = doc.createElement("customer");
			rootcustomer.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootcustomer.setAttribute("xsi:noNamespaceSchemaLocation", "../del.service.list.xsd");
			doc.appendChild(rootcustomer);
			Element childcontract = doc.createElement("contract");

			Element childcontractid = doc.createElement("id");
			childcontractid.setAttribute("type", "co_id");
			childcontractid.setTextContent(co_id.toString());
			childcontract.appendChild(childcontractid);
			if (new_rpcode == null) {
				for (int i = 0; i < SNCODES.size(); i++) {
					Element childservice = doc.createElement("service");
					Element childserviceSNCODE = doc.createElement("sncode");
					childserviceSNCODE.setTextContent(SNCODES.get(i).toString());
					childservice.appendChild(childserviceSNCODE);
					childcontract.appendChild(childservice);
				}
			} else {
				Element childnew_rpcode = doc.createElement("target_rateplan_code");
				childnew_rpcode.setTextContent(new_rpcode.toString());
				childcontract.appendChild(childnew_rpcode);
			}
			if (SNCODES_EXCLUDED != null && SNCODES_EXCLUDED.size() > 0) {
				for (int i = 0; i < SNCODES_EXCLUDED.size(); i++) {
					Element childexclude_service = doc.createElement("exclude_service");
					Element childserviceexcludeSNCODE = doc.createElement("sncode");
					childserviceexcludeSNCODE.setTextContent(SNCODES_EXCLUDED.get(i).toString());
					childexclude_service.appendChild(childserviceexcludeSNCODE);
					childcontract.appendChild(childexclude_service);
				}
			}
			rootcustomer.appendChild(childcontract);

			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.METHOD, "xml");
			trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();
			return xmlString;

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public static String ActivateServicesXML(Long CO_ID, ArrayList SNCODES_ACT, String DN_NUM, Long NEW_TMCODE) {
		if (SNCODES_ACT == null || SNCODES_ACT.size() == 0)
			return null;
		if (CO_ID == null && DN_NUM == null)
			return null;
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			System.setProperty("javax.xml.transform.TransformerFactory",
					"com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
			Element rootcustomer = doc.createElement("customer");
			rootcustomer.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootcustomer.setAttribute("xsi:noNamespaceSchemaLocation", "../add.service.list.xsd");
			doc.appendChild(rootcustomer);
			Element childcontract = doc.createElement("contract");

			Element childcontractid = doc.createElement("id");
			if (CO_ID != null) {
				childcontractid.setAttribute("type", "co_id");
				childcontractid.setTextContent(CO_ID.toString());
			} else {
				childcontractid.setAttribute("type", "msisdn");
				childcontractid.setTextContent(DN_NUM);
			}

			childcontract.appendChild(childcontractid);

			for (int serviceindex = 0; serviceindex < SNCODES_ACT.size(); serviceindex++) {
				Element childservice = doc.createElement("service");
				Element childserviceSNCODE = doc.createElement("sncode");
				childserviceSNCODE.setTextContent(((HashMap) SNCODES_ACT.get(serviceindex)).get("SNCODE").toString());
				Element childserviceservice_package_code = doc.createElement("service_package_code");
				childserviceservice_package_code
						.setTextContent(((HashMap) SNCODES_ACT.get(serviceindex)).get("SPCODE").toString());

				childservice.appendChild(childserviceSNCODE);
				childservice.appendChild(childserviceservice_package_code);

				ArrayList ListofParamValues = (ArrayList) ((HashMap) SNCODES_ACT.get(serviceindex))
						.get("ListofParamValues");
				if (ListofParamValues != null) {
					for (int paramindex = 0; paramindex < ListofParamValues.size(); paramindex++) {
						HashMap paramvalue = (HashMap) ListofParamValues.get(paramindex);
						Element childserviceparameter = doc.createElement("parameter");
						Element childserviceparameterid = doc.createElement("id");
						childserviceparameterid.setTextContent(paramvalue.get("PRM_ID").toString());
						Element childserviceparameterno = doc.createElement("no");
						childserviceparameterno.setTextContent(paramvalue.get("PRM_NO").toString());
						Element childserviceparametervalue = doc.createElement("value");
						if (paramvalue.get("VALUE_SEQNO") != null) {
							childserviceparametervalue.setTextContent(paramvalue.get("VALUE_SEQNO").toString());
						} else {
							childserviceparametervalue.setTextContent(paramvalue.get("VALUE").toString());
						}
						Element childserviceparameterdes = doc.createElement("des");
						childserviceparameterdes.setTextContent(paramvalue.get("VALUE_DES").toString());
						childserviceparameter.appendChild(childserviceparameterid);
						childserviceparameter.appendChild(childserviceparameterno);
						childserviceparameter.appendChild(childserviceparametervalue);
						childserviceparameter.appendChild(childserviceparameterdes);
						childservice.appendChild(childserviceparameter);
					}
				}

				childcontract.appendChild(childservice);
			}
			rootcustomer.appendChild(childcontract);
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.METHOD, "xml");
			trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			StringWriter sw = new StringWriter();

			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();
			return xmlString;

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return null;
		}

	}

	public static void addDeleteServices(Bcontract bcontract, ArrayList result, String username, SOIService soiComm) {

		String dateString = df.format(new Date());
		Integer parentrequestid = null;
		HashMap<String, Object> inParams = new HashMap<String, Object>();

		for (int index = 0; index < result.size(); index++) {
			HashMap courant = (HashMap) result.get(index);
			inParams = new HashMap<String, Object>();
			if (parentrequestid != null)
				inParams.put("PARENT_REQ_ID", parentrequestid);
			inParams.put("CO_ID", new Integer((int) bcontract.getCO_ID()));
			inParams.put("CUSTOMER_ID", new Integer((int) bcontract.getCS_ID()));
			inParams.put("USERNAME", username);
			if(courant.get("ORIGIN")==null){
			    inParams.put("ORIGIN", "MASS_WEB_SERVICE");
			}
			else{
			    inParams.put("ORIGIN", courant.get("ORIGIN"));
			}
			
			inParams.put("ORIGIN", "MASS_WEB_SERVICE");
			inParams.put("ORIGINAL_ID", new Integer((int) bcontract.getRPCODE()));
			inParams.put("DEST_ID", new Integer((int) bcontract.getRPCODE()));
			inParams.put("ADDITIONAL_INFO", "");
			inParams.put("STATUS", new Integer(2));
			inParams.put("ACTION_ID", Integer.parseInt(courant.get("ACTION_ID").toString()));
			inParams.put("PRIORITY", new Integer(9));
			// inParams.put("INSERTION_DATE", dateString);
			inParams.put("INPUT_DATA", courant.get("INPUT_DATA").toString());
			try {
				HashMap<String, Object> outParams = soiComm.executeCommand(SoiUtils.CMD_CMSINTERFACE_ADD, inParams);
				parentrequestid = Integer.parseInt(((Long) outParams.get("REQUEST_ID")).toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// >******* Updated, A.ESSA FC5197
	public static void addDeleteServices(BDeactivatedSRV bcontract, ArrayList result, String username,
			SOIService soiComm) {

		String dateString = df.format(new Date());
		Integer parentrequestid = null;
		HashMap<String, Object> inParams = new HashMap<String, Object>();

		for (int index = 0; index < result.size(); index++) {
			HashMap courant = (HashMap) result.get(index);
			inParams = new HashMap<String, Object>();
			if (parentrequestid != null)
				inParams.put("PARENT_REQ_ID", parentrequestid);
			inParams.put("CO_ID", new Integer((int) bcontract.getCO_ID()));
			inParams.put("CUSTOMER_ID", new Integer((int) bcontract.getCS_ID()));
			inParams.put("USERNAME", username);
			inParams.put("ORIGIN", "DUALSIM");
			inParams.put("ORIGINAL_ID", new Integer((int) bcontract.getRPCODE()));
			inParams.put("DEST_ID", new Integer((int) bcontract.getRPCODE()));
			inParams.put("ADDITIONAL_INFO", "");
			inParams.put("STATUS", new Integer(2));
			inParams.put("ACTION_ID", Integer.parseInt(courant.get("ACTION_ID").toString()));
			inParams.put("PRIORITY", new Integer(9));
			// inParams.put("INSERTION_DATE", dateString);
			inParams.put("INPUT_DATA", courant.get("INPUT_DATA").toString());
			try {
				HashMap<String, Object> outParams = soiComm.executeCommand(SoiUtils.CMD_CMSINTERFACE_ADD, inParams);
				parentrequestid = Integer.parseInt(((Long) outParams.get("REQUEST_ID")).toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String DeactiveServiceXMLWithDelFlag(ArrayList SNCODES, Long co_id) {
		if (co_id == null)
			return null;
		if (((SNCODES == null) || (SNCODES.size() == 0)))
			return null;
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			System.setProperty("javax.xml.transform.TransformerFactory",
					"com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
			Element rootcustomer = doc.createElement("customer");
			rootcustomer.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootcustomer.setAttribute("xsi:noNamespaceSchemaLocation", "../del.service.list.xsd");
			doc.appendChild(rootcustomer);
			Element childcontract = doc.createElement("contract");

			Element childcontractid = doc.createElement("id");
			childcontractid.setAttribute("type", "co_id");
			childcontractid.setTextContent(co_id.toString());
			childcontract.appendChild(childcontractid);
			for (int i = 0; i < SNCODES.size(); i++) {
				Element childservice = doc.createElement("service");
				Element childserviceSNCODE = doc.createElement("sncode");
				childserviceSNCODE.setTextContent(SNCODES.get(i).toString());
				childservice.appendChild(childserviceSNCODE);
				Element childserviceDelFlag = doc.createElement("delete_flag");
				childserviceDelFlag.setTextContent("X");
				childservice.appendChild(childserviceDelFlag);
				childcontract.appendChild(childservice);
			}
			rootcustomer.appendChild(childcontract);

			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.METHOD, "xml");
			trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();
			return xmlString;

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return null;
		}

	}
	// <******* Updated, A.ESSA FC5197
}
