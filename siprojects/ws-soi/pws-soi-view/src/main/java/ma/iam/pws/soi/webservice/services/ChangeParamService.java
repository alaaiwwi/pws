package ma.iam.pws.soi.webservice.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;

import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;

/**
 * @author Mo.Aissa
 *
 */

@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public interface ChangeParamService {

	/**
	 * DeactivateService . : WS user to Deactivate Service for contract
	 * 
	 * @param coId
	 *            internal identifier of contract in billing system
	 * @param SNCODE
	 *            public identifier of Service to Deactivate
	 */
	@javax.jws.WebMethod
	List<bNewServicesEx> ChangeParamServiceImpl(
			@javax.jws.WebParam(name = "USERNAME") String username,
			@javax.jws.WebParam(name = "CO_ID") long co_id,
			@javax.jws.WebParam(name = "SERVICES") List<bNewServices> SNCODE)
			throws FaultWS;

	/**
	 * byPAss check if the communication its ok/ or not send a fake compte and
	 * chack the return if the compte is not connected
	 * 
	 * @return
	 */
	@WebMethod(operationName = "byPass")
	@WebResult(name = "result")
	Integer modeByPass();
}
