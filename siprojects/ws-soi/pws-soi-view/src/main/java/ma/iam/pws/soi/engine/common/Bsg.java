package ma.iam.pws.soi.engine.common;

import java.io.Serializable;

public class Bsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6032007789727826975L;
	
	private Long bsgId = null;
	
	private Long preferedCug = null;
	
	private Parameter[] parameters = null;

	public Long getBsgId() {
		return bsgId;
	}

	public void setBsgId(Long bsgId) {
		this.bsgId = bsgId;
	}

	public Long getPreferedCug() {
		return preferedCug;
	}

	public void setPreferedCug(Long preferedCug) {
		this.preferedCug = preferedCug;
	}

	public Parameter[] getParameters() {
		return parameters;
	}

	public void setParameters(Parameter[] parameters) {
		this.parameters = parameters;
	}
	
	

}
