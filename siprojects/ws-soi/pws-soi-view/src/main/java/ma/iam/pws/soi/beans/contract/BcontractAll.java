package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="Contract" , propOrder = { "CO_ID", "CO_ID_PUB", "RPCODE", "CS_ID" , "CS_ID_PUB" , "contractedServices" , "unContractedServices" })
public class BcontractAll implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private long CO_ID;
	private String CO_ID_PUB;
	
	private long RPCODE;
	private long CS_ID;
	private String CS_ID_PUB;
	
	@XmlTransient 
	private String RPCODE_PUB;
	
	public long getRPCODE() {
		return RPCODE;
	}

	public void setRPCODE(long rPCODE) {
		RPCODE = rPCODE;
	}

	public long getCS_ID() {
		return CS_ID;
	}

	public void setCS_ID(long cS_ID) {
		CS_ID = cS_ID;
	}

	public String getCS_ID_PUB() {
		return CS_ID_PUB;
	}

	public void setCS_ID_PUB(String cS_ID_PUB) {
		CS_ID_PUB = cS_ID_PUB;
	}

	private List<BContractedServicesAll> contractedServices= new ArrayList<BContractedServicesAll>();
	private List<BContractedServicesAll> unContractedServices= new ArrayList<BContractedServicesAll>();

	public List<BContractedServicesAll> getContractedServices() {
		return contractedServices;
	}

	public void setContractedServices(List<BContractedServicesAll> contractedServices) {
		this.contractedServices = contractedServices;
	}

	public List<BContractedServicesAll> getUnContractedServices() {
		return unContractedServices;
	}

	public void setUnContractedServices(
			List<BContractedServicesAll> unContractedServices) {
		this.unContractedServices = unContractedServices;
	}

	public long getCO_ID() {
		return CO_ID;
	}

	public void setCO_ID(long cO_ID) {
		CO_ID = cO_ID;
	}

	public String getCO_ID_PUB() {
		return CO_ID_PUB;
	}

	public void setCO_ID_PUB(String cO_ID_PUB) {
		CO_ID_PUB = cO_ID_PUB;
	}
	
	public String getRPCODE_PUB() {
		return RPCODE_PUB;
	}

	public void setRPCODE_PUB(String rPCODE_PUB) {
		RPCODE_PUB = rPCODE_PUB;
	}
}
