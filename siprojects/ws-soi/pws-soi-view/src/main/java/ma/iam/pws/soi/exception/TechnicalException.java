package ma.iam.pws.soi.exception;




/**
 * The Class TechnicalException : Gestion des exceptions techniques
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class TechnicalException extends BaseException {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -764018510574013490L;
	/**
	 * Instantiates a new technical exception.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param messageKey the message key
	 */
	//public TechnicalException(Class classe, String methode, String messageKey) {
	//	super(MessageUtils.getMessage(messageKey));
	//}
	

	/**
	 * Instantiates a new technical exception.
	 * @param message the message
	 */
	public TechnicalException(String code , String message) {
		super(code + message);
	}

	/**
	 * Instantiates a new technical exception.
	 * @param message the message
	 * @param throwable the throwable
	 */
	public TechnicalException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	/**
	 * Instantiates a new technical exception.
	 * 
	 * @param classe the classe
	 * @param methode the methode
	 * @param messageKey the message key
	 * @param throwable the throwable
	 */
	//public TechnicalException(Class classe, String methode, String messageKey, Throwable throwable) {
	//	super(MessageUtils.getMessage(messageKey), throwable);
	//}
}

