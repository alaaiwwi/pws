package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
//@XmlType(name="Contract" , propOrder = { "CO_ID", "CO_ID_PUB", "RPCODE", "CS_ID" , "CS_ID_PUB" , "contractedServices" , "unContractedServices","deactivateServices" })
public class bNewServicesExDependencies  extends bNewServicesEx {

	/**
	 * 
	 */
	
	private List<bNewParamValueEx> BNewParamValueEx= new ArrayList<bNewParamValueEx>();

	public List<bNewParamValueEx> getBNewParamValueEx() {
		return BNewParamValueEx;
	}

	public void setBNewParamValueEx(List<bNewParamValueEx> bNewParamValueEx) {
		BNewParamValueEx = bNewParamValueEx;
	}
	
	
}
