package ma.iam.pws.soi.webservice.services.impl;

/**
 * @author Mo.Aissa
 *
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.BDeactivatedSRV;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.CmsINTUtils;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.services.DeactivateService;
import ma.iam.security.SecuredObject;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

@SecuredObject
@WebService(targetNamespace = "http://services.webservice.ws.iam.ma/")
public class DeactivateServiceImpl implements DeactivateService {

	private static final Logger logger = LogManager
			.getLogger(DeactivateServiceImpl.class.getName());
	Contract contract = new Contract();
	ServiceUtils service = new ServiceUtils();

	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	private String securityName;
	private String securityVersion;

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	private UserCache userCache = new NullUserCache();

	private String cilName;
	private String cilVersion;

	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	// @Override
	@SuppressWarnings("unchecked")
	@RolesAllowed("ROLE_WRITE")
	public BDeactivatedSRV DeactivateServiceImpl(String username, long co_id,
			long sncode) throws FaultWS {
		logger.info(getClass().toString() + " Deactivate Service : " + sncode
				+ " Contract : " + co_id);

		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());

		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}

		// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);
		try {
			BDeactivatedSRV bcontract = new BDeactivatedSRV();
			if (co_id == 0 || sncode == 0) {
				logger.error(getClass().toString()
						+ "WS DeactivateService : Erreur Input value Parameters CO_ID | SNCODE");
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_INPUT_MISSING,
						msgProperties);
			}

			HashMap<String, Object> inParams = new HashMap<String, Object>();
			HashMap<String, Object> outParamsCtrSrv = new HashMap<String, Object>();
			contract.setCoId(new Long(co_id).longValue());
			service.setSnCode(new Long(sncode).longValue());
			inParams.put("CO_ID", new Long(co_id).longValue());
			inParams.put("SYNC_WITH_DB", true);
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParams);
			inParams.remove("SYNC_WITH_DB");
			// bcontract= BContractAdapter.getMainContractInfo(outParams);
			bcontract.setRPCODE(new Long(outParams.get("RPCODE").toString())
					.longValue());
			bcontract.setCS_ID(new Long(outParams.get("CS_ID").toString())
					.longValue());
			bcontract.setCS_ID_PUB(outParams.get("CS_ID_PUB").toString());
			bcontract.setCO_ID_PUB(outParams.get("CO_ID_PUB").toString());
			bcontract.setCO_ID(co_id);

			inParams = new HashMap<String, Object>();
			inParams.put("CO_ID", new Long(co_id).longValue());
			outParamsCtrSrv = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);

			if (outParamsCtrSrv.containsKey("services")) {
				if (ServiceUtils.isAllowedToBeDeactivated(
						bcontract.getRPCODE(), sncode, soiComm)) {
					// >******* Updated, A.ESSA FC5197
					HashMap hmService = ServiceUtils
							.checkIfServiceBelongToContract(service, contract,
									outParamsCtrSrv, soiComm);
					// if (ServiceUtils.checkIfServiceBelongToContract(service,
					// contract,outParamsCtrSrv,soiComm)) {
					if (hmService != null) {
						// <******* Updated, A.ESSA FC5197
						List<BContractedServices> listservices = new ArrayList<BContractedServices>();
						boolean result = false;
						inParams = new HashMap<String, Object>();
						inParams.put("CO_ID", new Long(co_id).longValue());
						inParams.put("SNCODE", sncode);
						outParamsCtrSrv = soiComm.executeCommand(
								SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
						HashMap<String, Object>[] inDeactivateService = (HashMap<String, Object>[]) outParamsCtrSrv
								.get("services");

						// >******* Updated, A.ESSA FC5937
						if (",IN4FA,INT4G,INP4G,".contains(",".concat(
								hmService.get("SNCODE_PUB").toString()).concat(
								","))) {
							HashMap slaves = ServiceUtils.getSlavesDNs(
									new Long(co_id), "IND4G", soiComm);

							String dns = (String) slaves.get("DNS");
							if (dns != null && dns != "") {
								ArrayList params = new ArrayList();
								params.add(dns.substring(0, dns.length() - 2));
								throw new FaultWS(
										Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_SLAVES_ERROR,
										msgProperties, params);
							}
						}
						// >******* Updated, A.ESSA FC7045
						// else
						// if("SDM4G".equals(hmService.get("SNCODE_PUB").toString())){
						// throw new
						// FaultWS(ma.iam.ws.common.Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_SDM_ACTIVATION_ERROR,msgProperties);
						// }
						// <******* Updated, A.ESSA FC7045
						// <******* Updated, A.ESSA FC5937

						// >******* Update, A.ESSA DI6717
						// Checking if the user has access to this service

						if (SoiUtils.is_service_clcode(bcontract.getRPCODE(),
								sncode, new Long(hmService.get("SPCODE")
										.toString()).longValue(), 7)
								|| SoiUtils.is_service_clcode(bcontract
										.getRPCODE(), sncode, new Long(
										hmService.get("SPCODE").toString())
										.longValue(), 8)
								|| SoiUtils.is_service_clcode(bcontract
										.getRPCODE(), sncode, new Long(
										hmService.get("SPCODE").toString())
										.longValue(), 9)) {
							throw new FaultWS(
									Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATHORIZED,
									msgProperties);
						}
						// <******* Update, A.ESSA DI6717

						contract.setCoId(new Long(co_id).longValue());
						service.setSnCode(sncode);
						result = ServiceUtils.deactivate(service, contract,
								inDeactivateService[0], soiComm, msgProperties);

						if (result) {
							soiComm.cmsClient.commit();

							inParams = new HashMap<String, Object>();
							inParams.put("CO_ID", co_id);
							inParams.put("SNCODE", sncode);
							outParamsCtrSrv = soiComm.executeCommand(
									SoiUtils.CMD_CONTRACT_SERVICES_READ,
									inParams);
							inDeactivateService = (HashMap<String, Object>[]) outParamsCtrSrv
									.get("services");
							listservices.add(ServiceUtils
									.buildDeactivateServicesBean(
											inDeactivateService, bcontract));

							bcontract.setDeactivateServices(listservices);
							// >******* Updated, A.ESSA FC5197
								if ("INT3G".equals(hmService.get("SNCODE_PUB"))
									|| "WVG".equals(hmService.get("SNCODE_PUB"))) {
								HashMap slaves = ServiceUtils.getSlavesDNs(
										new Long(co_id), null, soiComm);

								inParams = new HashMap<String, Object>();
								inParams.put("CO_ID", co_id);
								inParams.put("SNCODE", sncode);
								outParamsCtrSrv = soiComm.executeCommand(
										SoiUtils.CMD_DELETEFLAG_UPDATE,
										inParams);

								ArrayList co_ids = (ArrayList) slaves
										.get("COS");
								if (co_ids != null) {
									ArrayList returnarray = new ArrayList();
									for (int j = 0; j < co_ids.size(); j++) {
										ArrayList arrayfordesact = new ArrayList();
										ArrayList returnarraydeactivate = new ArrayList();
										arrayfordesact.add(ServiceUtils
												.getSncodeFromShdes("ODDS3",
														soiComm));
										String xmldeactivateservices = null;
										xmldeactivateservices = CmsINTUtils
												.DeactiveServiceXMLWithDelFlag(
														arrayfordesact,
														(Long) co_ids.get(j));
										if (xmldeactivateservices != null) {
											HashMap deactivateservice = new HashMap();
											deactivateservice.put("ACTION_ID",
													3);
											deactivateservice.put("INPUT_DATA",
													xmldeactivateservices);
											deactivateservice.put("ORIGIN",
													"MASS_WEB_SERVICE");
											returnarraydeactivate
													.add(deactivateservice);
										}
										returnarray
												.addAll(returnarraydeactivate);
									}
									CmsINTUtils.addDeleteServices(bcontract,
											returnarray, username, soiComm);
								}
							}

							// <******* Updated, A.ESSA FC5197
						}

					} else {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_UNATTACHED_CTR,
								msgProperties);
					}
				} else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED,
							msgProperties);
				}
			}
			return bcontract;

		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(),
						cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());
			e.printStackTrace();
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
					msgProperties);
		}
	}

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage()
							.contains(
									"CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());

			return rb;
		}

	}
}
