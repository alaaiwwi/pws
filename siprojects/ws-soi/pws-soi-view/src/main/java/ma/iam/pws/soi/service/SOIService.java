package ma.iam.pws.soi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;

import com.alcatel.ccl.client.CMSClient;
import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.client.CMSFactory;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.exception.FunctionalException;
import ma.iam.pws.soi.service.util.PropertiesUtil;
import ma.iam.pws.soi.service.util.SoiUtils;

@SuppressWarnings("unchecked")
public class SOIService implements ISoiService {

	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager.getLogger(SOIService.class
			.getName());
	// .getLogger(Constantes.TECHNICAL_LOGGER_NAME);

	// private static final String RIGHT =
	// ParamLoader.getMessage("ge.gp.profil");
	private static final String RIGHT = "GEGRP";

	private static SOIService instance;

	public CMSClient cmsClient = null;

	private static String servername;

	private String login;

	public String getUserName() {
		return login;
	}

	public SOIService init() {
		return new SOIService();
	}

	private SOIService() {

	}

	// public static SOIService getInstance(String serversoiname , String
	// serverversion) throws CMSException {
	// if (cmsClient== null || null == instance ||
	// !serversoiname.equals(servername) ) {
	// cmsClient=null;
	// instance = new SOIService(serversoiname,serverversion);
	// }
	// return instance;
	// }
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ma.iam.geve.common.cms.pub.ICmsServices#changePassword(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	public SOIService(String serversoiname, String serverversion)
			throws CMSException {
		if (cmsClient == null) {

			Properties cmsPropsVar = SoiUtils
					.mapToProperties(PropertiesUtil.propertiesMap);
			cmsPropsVar.put("cms.soiname", serversoiname);
			cmsPropsVar.put("cms.soiversion", serverversion);
			servername = serversoiname;
			try {
				cmsClient = (CMSClient) CMSFactory.getInstance()
						.createCMSClient(cmsPropsVar);
			} catch (CMSException ex) {
				// TODO
				throw new CMSException("Erreur in Getting Soi Service client");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void login(String login, String motDePasse)
			throws CMSException {

		Properties cmsPropsVar = SoiUtils
				.mapToProperties(PropertiesUtil.propertiesMap);
		cmsClient.connect(login, motDePasse);
		this.login = login;

	}

	public HashMap executeCommand(String command,
			HashMap<String, Object> inputParameters) throws CMSException {
		LOGGER.info("Call :" + command + " Command With following parameters "
				+ inputParameters);
		return cmsClient.executeCommand(command, inputParameters);
	}

	/**
	 * 
	 */
	/*
	 * private List<String> getProfilesList(CMSClient cmsClient, HashMap
	 * inParams){ //throws TechnicalException { try {
	 * 
	 * // execute command // HashMap outParams = cmsClient.executeCommand(
	 * CmsUtils.CMS_USER_RIGHT_CHECK, inParams);
	 * 
	 * return convertListProfile((HashMap[]) outParams.get("USER_PERMISSIONS"));
	 * 
	 * } catch (Exception exception) { //LOGGER.error(getClass(),
	 * "getProfilesList", //"command.execute.problem", exception); //throw new
	 * TechnicalException(getClass(), "getProfilesList",
	 * //"command.execute.unable", exception); } }
	 */
	/**
	 * 
	 * @param cmsClient
	 * @param login
	 * @param motDePasse
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap checkConnection(CMSClient cmsClient, String login,
			String motDePasse) {

		HashMap hashMap = new HashMap();

		try {
			cmsClient.checkConnection(login, motDePasse);
			hashMap.put("CODE_RETOUR", new Long(-1).toString());
			hashMap.put("CMS_CONNECTION", "OK");
			return hashMap;
		} catch (CMSException ex) {
			hashMap.put("CODE_RETOUR", new Long(0).toString());
			String msg = ex.getMessage();
			if (msg.contains("ORA-28000")) {
				hashMap.put("CMS_CONNECTION", "account.locked"/*
															 * MessageUtils.
															 * getMessage
															 * ("account.locked"
															 * )
															 */);
			} else {
				hashMap.put("CMS_CONNECTION", ex.getMessage());
			}

			return hashMap;
		}

	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @param right
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, Object> getUserRightParameters(String username,
			String password) {
		HashMap<String, Object> buildParameter = new HashMap<String, Object>();
		HashMap[] paramValues = new HashMap[1];
		paramValues[0] = new HashMap();
		paramValues[0].put(SoiUtils.PARAMETER_PERMISSION, RIGHT);
		buildParameter.put(SoiUtils.PARAMETER_REQUESTED_PERMISSIONS,
				paramValues);
		buildParameter.put(SoiUtils.PARAMETER_USERNAME, username);
		buildParameter.put(SoiUtils.PARAMETER_PASSWORD, password);
		return buildParameter;
	}

	/**
	 * 
	 * @param objListProfile
	 * @return
	 * @throws FunctionalException
	 */
	public List<String> convertListProfile(Object objListProfile) {
		// throws FunctionalException {

		List<String> listProfiles = null;
		HashMap[] hashMapPorfiles = (HashMap[]) (objListProfile);

		if (hashMapPorfiles != null) {
			/* some profiles are returned */
			listProfiles = new ArrayList<String>();
			for (int i = 0; i < hashMapPorfiles.length; i++) {
				listProfiles.add((String) (hashMapPorfiles[i]
						.get("USER_PERMISSION")));
			}
		}
		return listProfiles;

	}

	// public static void reset(){
	// this.cmsClient=null;
	// }

	/**
	 * 
	 * @throws ServletException
	 */
	private synchronized void initializeIfOldConnection()
			throws ServletException {

		// LOGGER.info(getClass(), "initializeIfOldConnection",
		// "check if connection needs to be initialized");
		if (SoiUtils.isOldConnection()) {
			try {
				SoiUtils.cmsClientPool.close();
			} catch (Exception e) {
				// LOGGER.error(getClass(), "initializeIfOldConnection",
				// "Error when trying to close pool : ", e);
				// LOGGER.error(getClass(), "initializeIfOldConnection",
				// "will try later");
			}

			/* create an other pool */
			SoiUtils.initializePool();

			// LOGGER.info(getClass(), "initializeIfOldConnection",
			// "connection is freshed");

		} else {
			// LOGGER.info(getClass(), "initializeIfOldConnection",
			// "check still fresh");
		}
	}

	public boolean isConnectionOk() {
		LOGGER.info("=> isConnectionOk()");

		boolean isOk = true;
		try {
			executeCommand("WELCOMEPROCS.READ", null);
		} catch (CMSException exception) {
			isOk = false;
			cmsClient.disconnect();

		}

		LOGGER.info("<= isConnectionOk() : " + isOk);
		return isOk;
	}
}
