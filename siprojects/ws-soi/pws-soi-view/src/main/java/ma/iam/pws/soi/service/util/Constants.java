package ma.iam.pws.soi.service.util;

/**
 * The Class Constants : Cette interface a pour objectif de regrouper toutes les constantes fonctionnelles du GE.
 * 
 * <br/><br/>
 * Projet : GE
 * Créé le 24 sept. 2010
 * 
 * @author Atos Origin
 */
public abstract class Constants {

	 
	/** The Constant PATTERN_NUM_BORDEREAU. */
	public static final String PATTERN_NUM_BORDEREAU = "0000000";
	
	/** The Constant PARAM_PAIEMENT_DEBIT_INFO. */
	public static final String PARAM_PAIEMENT_DEBIT_INFO = "CUSTDB";

	/** The Constant GL_DIS_6386000000. */
	public static final String GL_DIS_6386000000 = "6386000000";

	/** The Constant PAYMENT_STATUS_R. */
	public static final String PAYMENT_STATUS_R = "R";

	/** The Constant PAYMENT_STATUS_A. */
	public static final String PAYMENT_STATUS_A = "A";

	/** The Constant VAL_0. */
	public static final String VAL_0 = "0";

	/** The Constant VAL_NAN. */
	public static final String VAL_NAN = "NaN";

	/** The Constant VAL_NEGATIF_1. */
	public static final String VAL_NEGATIF_1 = "-1";

	public static final String SUPERIEUR = ">";

	public static final String INFERIEUR = "<";

	/** The Constant NOTE_DE_CREDIT. */
	public static final String NOTE_DE_CREDIT = "Note de crédit";

	/** The Constant CM. */
	public static final String CM = "CM";

	/** The Constant AUTRE_CHARGE. */
	public static final String AUTRE_CHARGE = "Autre charge";

	/** The Constant FC. */
	public static final String FC = "FC";

	/** The Constant GARANTIE. */
	public static final String GARANTIE = "Garantie";

	/** The Constant AVANCE. */
	public static final String AVANCE = "Avance";

	/** The Constant CO. */
	public static final String CO = "CO";

	/** The Constant FACTURE. */
	public static final String FACTURE = "Facture";

	/** The Constant IN. */
	public static final String IN = "IN";

	/** The Constant OUI. */
	public static final String OUI = "OUI";

	/** The Constant NON. */
	public static final String NON = "NON";

	/** The Constant F. */
	public static final String F = "F";

	/** The Constant ANNULE. */
	public static final String ANNULE = "Annulé";

	/** The Constant DEPOSE. */
	public static final String DEPOSE = "Déposé";

	/** The Constant REJETE. */
	public static final String REJETE = "Rejeté";

	/** The Constant VALIDE. */
	public static final String VALIDE = "Validé";

	/** The Constant SOUMIS. */
	public static final String SOUMIS = "Soumis";

	/** The Constant ENCAISSE. */
	public static final String ENCAISSE = "Encaissé";

	/** The Constant NULL. */
	public static final String NULL = "null";

	/** The Constant VAL_2_TIRET. */
	public static final String VAL_2_TIRET = "--";

	/** The Constant RETOUR_LIGNE_2. */
	public static final String RETOUR_LIGNE_2 = "\n\n";

	/** The Constant FACTURES. */
	public static final String FACTURES = "Factures";

	/** The Constant RETOUR_LIGNE_3. */
	public static final String RETOUR_LIGNE_3 = "\n\n\n";

	/** The Constant NOM_CLIENT_RAISON_SOCIALE. */
	public static final String NOM_CLIENT_RAISON_SOCIALE = "Nom client (Raison sociale)";

	/** The Constant NUMERO_CLIENT. */
	public static final String NUMERO_CLIENT = "Numéro client";

	/** The Constant PERIODE. */
	public static final String PERIODE = "Période";

	/** The Constant RETOUR_LIGNE. */
	public static final String RETOUR_LIGNE = "\n";

	/** The Constant STR. */
	public static final String STR = "#";

	/** The Constant DATE_RELEVE. */
	public static final String DATE_RELEVE = "Date relevé";

	/** The Constant DUP. */
	public static final String DUP = "dup";

	/** The Constant FACTURE_MODE_X. */
	public static final String FACTURE_MODE_X = "X";

	/** The Constant STATUT_CO. */
	public static final String STATUT_CO = "CO";

	/** The Constant GLAR_MOBILE. */
	public static final String GLAR_MOBILE = "4452810000";

	/** The Constant CAREM_MOBILE. */
	public static final String CAREM_MOBILE = "TIMBRE";

	/** The Constant ADRESSE_DROIT_DE. */
	public static final String ADRESSE_DROIT_DE = "DROIT DE";

	/** The Constant CACH_NUM_MOBILE. */
	public static final String CACH_NUM_MOBILE = "DUMMY";

	/** The Constant ETAT_DROIT_DE_TIMBRE. */
	/**
	 * BSCSIX adaptation
	 * the following customer has changed
	 * from ETAT DROIT DETIMBRE TO 5.10006.10022  
	 */
	public static final String ETAT_DROIT_DE_TIMBRE = "5.10006.10022";

	/** The Constant GLAR_FIXE. */
	public static final String GLAR_FIXE = "51614000";

	/** The Constant CAREM_FIXE. */
	public static final String CAREM_FIXE = "XXXX";

	/** The Constant CACH_NUM_FIXE. */
	public static final String CACH_NUM_FIXE = ".STINV";

	/** The Constant CODE_CLIENT_1_1. */
	public static final String CODE_CLIENT_1_1 = "1.1";

	/** The Constant VAL_STRING_0. */
	public static final String VAL_STRING_0 = "0";

	/** The Constant PAIEMENT_DE_GARANTIE. */
	public static final String PAIEMENT_DE_GARANTIE = "Paiement de garantie";

	/** The Constant AR_DEPOSIT_OUT. */
	public static final String AR_DEPOSIT_OUT = "AR DEPOSIT OUT";

	/** The Constant CA_TYPE_12. */
	public static final String CA_TYPE_12 = "12";
	
	public static final String TICKET_CODE_SYSTEM = "SYSTEM";

	/** The Constant TICKET_FOLLOW_UP_STATUS_N. */
	public static final String TICKET_FOLLOW_UP_STATUS_N = "N";

	/** The Constant TICKET_STATUS_NOTE. */
	public static final String TICKET_STATUS_NOTE = "NOTE";

	/** The Constant AR_DEPOSIT_IN. */
	public static final String AR_DEPOSIT_IN = "AR DEPOSIT IN";

	/** The Constant VAL_14874000. */
	public static final String VAL_14874000 = "14874000";

	/** The Constant DP. */
	public static final String DP = "DP";

	/** The Constant AR_PAIEMENT. */
	public static final String AR_PAIEMENT = "AR PAIEMENT";

	/** The Constant CA_TYPE_3. */
	public static final String CA_TYPE_3 = "3";

	/** The Constant VAL_STRING_1. */
	public static final String VAL_STRING_1 = "1";

	/** The Constant PAIEMENT_D_AVANCE. */
	public static final String PAIEMENT_D_AVANCE = "Paiement d'avance";

	/** The Constant MONTANT_AVANCE. */
	public static final String MONTANT_AVANCE = "Montant Avance";

	/** The Constant PAIEMENT_DE_FACTURES. */
	public static final String PAIEMENT_DE_FACTURES = "Paiement de Factures";

	/** The Constant DD_MM_YYYY. */
	public static final String DD_MM_YYYY = "dd/MM/yyyy";

	/** The Constant MODE_PAIEMENT_EFFET_DE_COMMERCE. */
	public static final String MODE_PAIEMENT_EFFET_DE_COMMERCE = "Effet de commerce";

	/** The Constant MODE_PAIEMENT_ESPECE. */
	public static final String MODE_PAIEMENT_ESPECE = "Espèce";

	/** The Constant MODE_PAIEMENT_VIGNETTE. */
	public static final String MODE_PAIEMENT_VIGNETTE = "Vignette";

	/**
	 * Empecher l'instanciation d'une classe utilitaire
	 */
	private Constants() {
	}

	/*
	 * ######## Rib Params ########
	 */
	/** The Constant VALID_CLE_RIB. */
	public static final String VALID_CLE_RIB = "97";

	/*
	 * ######## CMS Params ########
	 */
	/** The Constant CMS_CONNEXION_OK. */
	public static final String CMS_CONNEXION_OK = "-1";
	
	/** The Constant CMS_CONNEXION_NOK. */
	public static final String CMS_CONNEXION_NOK = "-2";

	/** The Constant CMS_EXECUTION_OK. */
	public static final String CMS_EXECUTION_OK = "-1";

	/*
	 * ####### Systeme ############
	 */
	/** The Constant SYSTEM_GEVE. */
	public static final String SYSTEM_GEVE = "geVe";

	/** The Constant SYSTEM_BSCS_FIXE. */
	public static final String SYSTEM_BSCS_FIXE = "F";

	/** The Constant SYSTEM_BSCS_FIXE. */
	public static final String SYSTEM_BSCS_FIXE_TT = "Fixe";

	/** The Constant SYSTEM_BSCS_MOBILE. */
	public static final String SYSTEM_BSCS_MOBILE = "M";

	/** The Constant SYSTEM_BSCS_MOBILE_TT. */
	public static final String SYSTEM_BSCS_MOBILE_TT = "Mobile";

	/** The Constant SYSTEM_MULTI. */
	public static final String SYSTEM_MULTI = "MF";

	/*
	 * ############ Status user #############
	 */
	/** The Constant STATUS_ACTIF. */
	public static final String STATUS_ACTIF = "1";

	/** The Constant STATUS_NOT_ACTIF. */
	public static final String STATUS_NOT_ACTIF = "2";

	/*
	 * ############ Status Paiement actif ou non #############
	 */
	/** The Constant PARAM_PAIEMENT_ACTIF. */
	public static final String PARAM_PAIEMENT_ACTIF = "1";

	/** The Constant PARAM_PAIEMENT_NOT_ACTIF. */
	public static final String PARAM_PAIEMENT_NOT_ACTIF = "2";

	/*
	 * ############ params #############
	 */
	/** The Constant PARAM_DEVISE_DH. */
	public static final Long PARAM_DEVISE_DH = Long.valueOf(91);

	/** The Constant PARAM_CODE_AVANCE. */
	public static final String PARAM_CODE_AVANCE = "CO";

	/** The Constant PARAM_CODE_AVOIR. */
	public static final String PARAM_CODE_AVOIR = "CM";

	/** The Constant PARAM_AVANCE. */
	public static final String PARAM_AVANCE = "Avance";

	/** The Constant PARAM_VERSION. */
	public static final Long PARAM_VERSION = Long.valueOf(1);

	/** The Constant PARAM_ACTION_RETABLIR_TYPE. */
	public static final Long PARAM_ACTION_RETABLIR_TYPE_BO = Long.valueOf(8);
	
	/** The Constant PARAM_ACTION_RETABLIR_TYPE. */
	public static final Long PARAM_ACTION_RETABLIR_TYPE_BI = Long.valueOf(7);

	/** The Constant PARAM_ACTION_RETABLIR_PRIORITE. */
	public static final Long PARAM_ACTION_RETABLIR_PRIORITE = Long.valueOf(1);

	/** The Constant PARAM_REC_VERSION. */
	public static final Long PARAM_REC_VERSION = Long.valueOf(1);

	/** The Constant PARAM_OVER_PAYMENT. */
	public static final String PARAM_OVER_PAYMENT = "AR OVERPAYMENT";

	/** The Constant USER_FIXE_TICKLER. */
	public static final String USER_FIXE_TICKLER = "PTH";

	/*
	 * ############  #############
	 */

	/** The Constant PARAM_BOOLEAN_YES. */
	public static final String PARAM_BOOLEAN_YES = "Y";

	/** The Constant PARAM_BOOLEAN_NO. */
	public static final String PARAM_BOOLEAN_NO = "N";
	
	/*
	 * ############ Status Paiement #############
	 */
	/** The Constant PARAM_PAIEMENT_ENCAISSE. */
	public static final String PARAM_PAIEMENT_ENCAISSE = "E";

	/** The Constant PARAM_PAIEMENT_REJETE. */
	public static final String PARAM_PAIEMENT_REJETE = "R";

	/** The Constant PARAM_PAIEMENT_ANNULE. */
	public static final String PARAM_PAIEMENT_ANNULE = "A";

	/** The Constant PARAM_PAIEMENT_SOUMMIS. */
	public static final String PARAM_PAIEMENT_SOUMMIS = "S";

	/** The Constant PARAM_PAIEMENT_DEPOSE. */
	public static final String PARAM_PAIEMENT_DEPOSE = "D";

	/** The Constant PARAM_PAIEMENT_VALIDE. */
	public static final String PARAM_PAIEMENT_VALIDE = "V";

	/** The Constant PARAM_PAIEMENT_REJETE_SOUMIS. */
	public static final String PARAM_PAIEMENT_REJETE_SOUMIS = "R;S";

	// Types des factures
	/** The Constant PARAM_FACTURE. */
	public static final String PARAM_FACTURE = "IN"; // Type : Facture

	/** The Constant PARAM_FACTURE_NAME. */
	public static final String PARAM_FACTURE_NAME = "Facture";

	/** The Constant PARAM_FACTURE_AVANCE. */
	public static final String PARAM_FACTURE_AVANCE = "CO"; // Type : Avance & Avoir

	/** The Constant PARAM_FACTURE_AVANCE_NAME. */
	public static final String PARAM_FACTURE_AVANCE_NAME = "Avance";

	/** The Constant PARAM_FACTURE_GARANTIE. */
	public static final String PARAM_FACTURE_GARANTIE = "DP"; // Type : Garantie

	/** The Constant PARAM_FACTURE_GARANTIE_NAME. */
	public static final String PARAM_FACTURE_GARANTIE_NAME = "Garantie";

	/** The Constant FLAG_RESP_PAY. */
	public static final String FLAG_RESP_PAY = "X";

	/** The Constant PARAM_MODE_FLAG. */
	public static final String PARAM_MODE_FLAG = "X";

	// TYPE DE PAIEMENTS

	/** The Constant TYPE_PAIEMENT_ESPECE. */
	public static final int TYPE_PAIEMENT_ESPECE = 1;

	/** The Constant TYPE_PAIEMENT_CHEQUE. */
	public static final int TYPE_PAIEMENT_CHEQUE = 2;

	/** The Constant TYPE_PAIEMENT_CHEQUESP. */
	public static final int TYPE_PAIEMENT_CHEQUESP = 3;

	/** The Constant TYPE_PAIEMENT_VIREMENT. */
	public static final int TYPE_PAIEMENT_VIREMENT = 4;

	/** The Constant TYPE_PAIEMENT_TPE. */
	public static final int TYPE_PAIEMENT_TPE = 5;

	/** The Constant TYPE_PAIEMENT_VIGNETTE. */
	public static final int TYPE_PAIEMENT_VIGNETTE = 6;

	/** The Constant TYPE_PAIEMENT_EFFET. */
	public static final int TYPE_PAIEMENT_EFFET = 7;

	/** The Constant TYPE_PAIEMENT_GAB. */
	public static final int TYPE_PAIEMENT_GAB = 8;

	/** The Constant TYPE_PAIEMENT_PRELEVEMENT. */
	public static final int TYPE_PAIEMENT_PRELEVEMENT = 9;

	// RAPPORTS

	/** The Constant PARAM_CAREM_ANNULATION. */
	public static final String PARAM_CAREM_ANNULATION = " - ADJ";

	/** The Constant CONVERTIR_AVANCE_EN_GARANTIE. */
	public static final String CONVERTIR_AVANCE_EN_GARANTIE = "Convertir Avance en garantie";

	/** The Constant VAL_DOUBLE_0. */
	public static final Double VAL_DOUBLE_0 = Double.valueOf(0);
	
	public static final Long VAL_LONG_0 = Long.valueOf(0);

	/** The Constant VAL_LONG_1. */
	public static final Long VAL_LONG_1 = Long.valueOf(1);
	
	/** The Constant VAL_INT_1. */
	public static final Integer VAL_INT_1 = Integer.valueOf(1);

	/** The Constant VAL_LONG_2. */
	public static final Long VAL_LONG_2 = Long.valueOf(2);

	/** The Constant CUSTDB. */
	public static final String CUSTDB = "CUSTDB";

	/** The Constant SOLDER_COMPTE_PAR_ACOMPTE. */
	public static final String SOLDER_COMPTE_PAR_ACOMPTE = "Solder compte par acompte";

	/** The Constant VAL_6386000000. */
	public static final String VAL_6386000000 = "6386000000";

	/** The Constant CA_TYPE_2. */
	public static final String CA_TYPE_2 = "2";

	/** The Constant B. */
	public static final String B = "B";

	/** The Constant VAL_44211001. */
	public static final String VAL_44211001 = "44211001";

	/** The Constant AR BOUNCE CHECK */
	public static final String PARAM_AR_BOUNCE_CHECK = "AR BOUNCE CHECK";

	public static final String DECIMAL_FORMAT_0_00 = "#0.00";
	
	public static final int HEURE_FIN_JOURNNE = 23;
	public static final int MINUTE_FIN_JOURNNE = 59;
	public static final int MILLISECONDE_FIN_JOURNNE = 59999;

	public static final Long GL_CURRENCY_MAROC_DIRHAM = Long.valueOf(91);

	public static final Long DOC_CURRENCY_MAROC_DIRHAM = Long.valueOf(91);

	public static final Long CA_REASON_CODE_9 = Long.valueOf(9);

	public static final Long CA_REASON_CODE_20 = Long.valueOf(20);
	
	public static final Long CA_REASON_CODE_19 = Long.valueOf(19);

	public static final Long CA_REASON_CODE_17 = Long.valueOf(17);

	public static final String CA_TYPE_1 = "1";

	public static final Long TICKET_PRIORITY_8 = Long.valueOf(8);

	public static final Long TICKET_PRIORITY_5 = Long.valueOf(5);

	public static final Long CA_REASON_CODE_18 = Long.valueOf(18);

	public static final Long CA_REASON_CODE_21 = Long.valueOf(21);

	public static final Long CA_REASON_CODE_3 = Long.valueOf(3);

	public static final String CA_TYPE_10 = "10";

	public static final Long CATEGORIE_CLIENT_ENTREPRISE = Long.valueOf(3);
	public static final Long CATEGORIE_CLIENT_GRAND_COMPTE = Long.valueOf(4);
	public static final Long CATEGORIE_CLIENT_OFFICIEL = Long.valueOf(6);
	public static final Long CATEGORIE_CLIENT_OFFICE_ETS_PUBLIC = Long.valueOf(7);
	public static final Long CATEGORIE_CLIENT_COLLECTIVITE_LOCAL = Long.valueOf(8);
	public static final Long CATEGORIE_CLIENT_AMBASSADE = Long.valueOf(9);
	public static final Long CATEGORIE_CLIENT_BANQUE = Long.valueOf(18);
	public static final Long CATEGORIE_CLIENT_FOURNISSEUR_SERVICE = Long.valueOf(27);
	public static final Long CATEGORIE_CLIENT_OFFICIEL_PROVISOIRE = Long.valueOf(59);
	public static final Long CATEGORIE_CLIENT_POSTES_EXPLOITATION = Long.valueOf(10);

	public static final String MODE_PAIEMENT_TPE = "TPE";

	public static final String PAY_MODE_ID_EFFET_DE_COMMERCE_BSCS = "-8";

	public static final Long CA_REASON_CODE_4 = Long.valueOf(4);

}
