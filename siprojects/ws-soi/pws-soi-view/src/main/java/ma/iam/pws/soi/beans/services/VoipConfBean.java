package ma.iam.pws.soi.beans.services;

public class VoipConfBean {
	private String value_des;
	private String value;
	private Long tmcode;
	private Long sncode;
	private Long prm_id;
	public String getValue_des() {
		return value_des;
	}
	public void setValue_des(String value_des) {
		this.value_des = value_des;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Long getTmcode() {
		return tmcode;
	}
	public void setTmcode(Long tmcode) {
		this.tmcode = tmcode;
	}
	public Long getSncode() {
		return sncode;
	}
	public void setSncode(Long sncode) {
		this.sncode = sncode;
	}
	public Long getPrm_id() {
		return prm_id;
	}
	public void setPrm_id(Long prm_id) {
		this.prm_id = prm_id;
	}
	
	
}
