package ma.iam.pws.soi.engine.security;
//package ma.iam.ws.engine.security;
//
//
//import java.io.IOException;
//import java.util.Properties;
//
//import javax.security.auth.callback.Callback;
//import javax.security.auth.callback.UnsupportedCallbackException;
//
//import ma.iam.ws.common.Constants;
//
//import org.apache.cxf.common.security.UsernameToken;
//import org.apache.wss4j.common.ext.WSPasswordCallback;
//import org.apache.wss4j.common.ext.WSSecurityException;
//import org.apache.wss4j.dom.handler.RequestData;
//import org.apache.wss4j.dom.validate.UsernameTokenValidator;
//import org.apache.xml.security.utils.Base64;
//
//
//public class UserNameTokenValidator extends UsernameTokenValidator {
//	
//    private static org.apache.commons.logging.Log log = 
//            org.apache.commons.logging.LogFactory.getLog(UsernameTokenValidator.class);
//	
//	//private static final Properties passMap = new Properties();
//	private Properties passMap = new Properties();
//
//	 public Properties getPassMap() {
//		return passMap;
//	}
//
//	public void setPassMap(Properties passMap) {
//		this.passMap = passMap;
//	}
//
//	 /**
//     * Verify a UsernameToken containing a password digest. It does this by querying a 
//     * CallbackHandler instance to obtain a password for the given username, and then comparing
//     * it against the received password.
//     * @param usernameToken The UsernameToken instance to verify
//     * @throws WSSecurityException on a failed authentication.
//     */
//    protected void verifyDigestPassword(UsernameToken usernameToken,
//                                        RequestData data) throws WSSecurityException {
//        if (data.getCallbackHandler() == null) {
//            throw new WSSecurityException(WSSecurityException.FAILURE, "noCallback");
//        }
//        
//        String user = usernameToken.getName();
//        String password = usernameToken.getPassword();
//        String nonce = usernameToken.getNonce();
//        String createdTime = usernameToken.getCreated();
//        String pwType = usernameToken.getPasswordType();
//        boolean passwordsAreEncoded = usernameToken.getPasswordsAreEncoded();
//        
//        WSPasswordCallback pwCb = 
//            new WSPasswordCallback(user, null, pwType, WSPasswordCallback.USERNAME_TOKEN, data);
//        try {
//            data.getCallbackHandler().handle(new Callback[]{pwCb});
//        } catch (IOException e) {
//            if (log.isDebugEnabled()) {
//                log.debug(e);
//            }
//            throw new WSSecurityException(
//                WSSecurityException.FAILED_AUTHENTICATION, null, null, e
//            );
//        } catch (UnsupportedCallbackException e) {
//            if (log.isDebugEnabled()) {
//                log.debug(e);
//            }
//            throw new WSSecurityException(
//                WSSecurityException.FAILED_AUTHENTICATION, null, null, e
//            );
//        }
//        String origPassword = pwCb.getPassword();
//        if (origPassword == null) {
//            if (log.isDebugEnabled()) {
//                log.debug("Callback supplied no password for: " + user);
//            }
//            throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
//        }
//        if (usernameToken.isHashed()) {
//            String passDigest;
//            if (passwordsAreEncoded) {
//                passDigest = UsernameToken.doPasswordDigest(nonce, createdTime, Base64.decode(origPassword));
//            } else {
//                passDigest = UsernameToken.doPasswordDigest(nonce, createdTime, origPassword);
//            }
//            if (!passDigest.equals(password)) {
//                throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
//            }
//        } else {
//            if (!origPassword.equals(password)) {
//                throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
//            }
//        }
//    }
//	@Override
//     protected void verifyPlaintextPassword(UsernameToken usernameToken,
//       RequestData data) throws WSSecurityException {
////			if (passMap.size()<=0) {
////				try {
////					passMap.load(new FileInputStream(System.getenv("pwdloc").toString()));
////				} catch (FileNotFoundException e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				} catch (IOException e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				}
////			}
//		 	String user = usernameToken.getName();
//	        String password = usernameToken.getPassword();
//	        String nonce = usernameToken.getNonce();
//	        String createdTime = usernameToken.getCreated();
//	        String pwType = usernameToken.getPasswordType();
//	    	String pass = passMap.getProperty(user);
//			String encodedPass = Constants.ENCODER.encodePassword(password , null);
//			
//		/*	System.out.println("notEncodedPass: "+password+"     identifier: "+user+"  *************************   pass:" + pass
//					+ "  *****    pc.getPassword(): " + password + "   *****  encodedPass: "
//					+ encodedPass);*/
//
//			if (pass != null) {
//				if (!encodedPass.equals(pass)) {
//					throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
//				}
//				return;
//			}
//			else{
//				throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
//			}
//	 }
//
//}
//
