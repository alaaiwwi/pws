package ma.iam.pws.soi.beans.errors;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import ma.iam.pws.soi.exception.Faul;

public class FaultWS extends Faul implements Serializable {

	private static final long serialVersionUID = 1L;

	public FaultWS(String message, String codefault, String descfault,
			Throwable cause) {
		super(message, codefault, descfault, cause);
	}

	public FaultWS(String message, String codefault, String descfault) {
		super(message, codefault, descfault);

	}

	public FaultWS(String codefault, String descfault) {
		super(codefault, descfault);

	}

	public FaultWS(String codefault, Properties msgPr) {
		super(codefault, msgPr);

	}

	public FaultWS(Object obj, String codefault, Properties msgPr) {
		super(obj, codefault, msgPr);

	}

	public FaultWS(String codefault, Properties msgPr, List<String> params) {
		super(codefault, msgPr, params);

	}

	public FaultWS(Object obj, String codefault, Properties msgPr,
			List<String> params) {
		super(obj, codefault, msgPr, params);

	}

}
