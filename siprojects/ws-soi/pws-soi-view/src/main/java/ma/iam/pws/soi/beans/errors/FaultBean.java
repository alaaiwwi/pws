package ma.iam.pws.soi.beans.errors;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(name="FaultBean" , propOrder = { "codefault", "descfault" })
public class FaultBean  implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private String codefault;
	private String descfault;

	
	public String getCodefault() {
		return codefault;
	}


	public void setCodefault(String codefault) {
		this.codefault = codefault;
	}

	
	public String getDescfault() {
		return descfault;
	}


	public void setDescfault(String descfault) {
		this.descfault = descfault;
	}



}
