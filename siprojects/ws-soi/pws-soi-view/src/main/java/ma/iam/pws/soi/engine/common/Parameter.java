/*
 * $$RCSfile: Parameter.java,v $$
 * Copyright (c) 2006 Alcatel
 * All rights reserved.
 *
 * @author Jean-Baptiste Binet
 * @version $$Revision: 1.7 $$
 * $$Date: 2010/06/18 12:31:22 $$
 * Last modified by $$Author: lepault $$
 * $$Name: CCL-1_79 $$
 * $$Source: /home/BSCS_CVS/standard/bscs/ccl/src/com/alcatel/ccl/common/Parameter.java,v $$
 */

package ma.iam.pws.soi.engine.common;

import java.io.Serializable;

/**
 * This class represents a service parameter.
 */
public class Parameter implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7810240397160024960L;

	/**
	 * Sequence number of the parameter within the service. It must be a value of the column PRM_NO
	 * of SERVICE_PARAMETER table.
	 */
	private Long _prmNo = null;

	/** Parameter identifier. It must be a value of the column PARAMETER_ID of MKT_PARAMETER table. */
	private Long _prmId = null;

	/**
	 * Parameter value. For CMS V8, it must be a value of the column PRM_VALUE_STRING of
	 * MKT_PARAMETER_DOMAIN table. For CMS V9, it must be a value of the column PRM_VALUE_SEQNO of
	 * MKT_PARAMETER_DOMAIN.
	 */
	private String[] _value = null;

	/**
	 * Parameter value description. It must be a value of the column PRM_VALUE_DES of
	 * MKT_PARAMETER_DOMAIN table.
	 */
	private String[] _valueDes = null;
	
	private Long[] seqnoTab = null;
	
	public Long getPrmId()
	{
		return _prmId;
	}

	public void setPrmId(Long prmId)
	{
		_prmId = prmId;
	}

	public Long getPrmNo()
	{
		return _prmNo;
	}

	public void setPrmNo(Long prmNo)
	{
		_prmNo = prmNo;
	}

	public String getValue(int i)
	{
		return _value[i];
	}
	
	public String[] getValue()
	{
		return _value;
	}
	
	public Long[] getSeqnoTab()
	{
		return seqnoTab;
	}

	public void setSeqno(Long value,int i)
	{
		if(this.seqnoTab==null)
			this.seqnoTab = new Long[1];
		if(i>=seqnoTab.length){
			Long[] tmp = new Long[seqnoTab.length+1];
			for(int j=0;j<seqnoTab.length;j++){
				tmp[j] = seqnoTab[j];
			}
			seqnoTab = tmp;
		}
		this.seqnoTab[i] = value;
	}

	public Long getSeqno(int i)
	{
		return seqnoTab[i];
	}
	

	public void setValueDes(String valueDes,int i)
	{
		if(this._valueDes==null)
			this._valueDes = new String[1];
		if(i>=_valueDes.length){
			String[] tmp = new String[_valueDes.length+1];
			for(int j=0;j<_valueDes.length;j++){
				tmp[j] = _valueDes[j];
			}
			_valueDes = tmp;
		}
			
		_valueDes[i] = valueDes;
	}
	
	public void setValue(String value,int i)
	{
		if(this._value==null)
			this._value = new String[1];
		if(i>=_value.length){
			String[] tmp = new String[_value.length+1];
			for(int j=0;j<_value.length;j++){
				tmp[j] = _value[j];
			}
			_value = tmp;
		}
		this._value[i] = value;
	}

	public String getValueDes(int i)
	{
		return _valueDes[i];
	}
	
	public String[] getValueDes()
	{
		return _valueDes;
	}

	
	/**
	 * This method converts this Parameter to a String.
	 * 
	 * @return A String representation of this Parameter.
	 */
	public String toString()
	{
		String dump = new String();

		dump += "Parameter._prmNo = " + _prmNo;
		dump += "\nParameter._prmId = " + _prmId;
		dump += "\nParameter._value = " + _value;
		dump += "\nParameter._valueDesc = " + _valueDes;

		return dump;
	}
}