package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
//@XmlType(name="Param" , propOrder = { "" })
public class bNewValues implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private String VALUE_DES;
	
	private String VALUE;

	public String getVALUE_DES() {
		return VALUE_DES;
	}
	public void setVALUE_DES(String vALUE_DES) {
		VALUE_DES = vALUE_DES;
	}
	public String getVALUE() {
		return VALUE;
	}
	public void setVALUE(String vALUE) {
		VALUE = vALUE;
	}

}
