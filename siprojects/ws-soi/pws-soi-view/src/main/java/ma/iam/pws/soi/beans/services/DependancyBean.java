package ma.iam.pws.soi.beans.services;

public class DependancyBean {
	
	private long DEP_SNCODE;
	
	private String DEPENDENCY_TYPE;
	
	private String DEP_SNCODE_PUB;

	public long getDEP_SNCODE() {
		return DEP_SNCODE;
	}

	public void setDEP_SNCODE(long dEP_SNCODE) {
		DEP_SNCODE = dEP_SNCODE;
	}

	public String getDEPENDENCY_TYPE() {
		return DEPENDENCY_TYPE;
	}

	public void setDEPENDENCY_TYPE(String dEPENDENCY_TYPE) {
		DEPENDENCY_TYPE = dEPENDENCY_TYPE;
	}

	public String getDEP_SNCODE_PUB() {
		return DEP_SNCODE_PUB;
	}

	public void setDEP_SNCODE_PUB(String dEP_SNCODE_PUB) {
		DEP_SNCODE_PUB = dEP_SNCODE_PUB;
	}
	
	
	
}
