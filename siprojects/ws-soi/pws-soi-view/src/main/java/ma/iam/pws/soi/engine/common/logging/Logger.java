package ma.iam.pws.soi.engine.common.logging;
//package ma.iam.ws.engine.common.logging;
//
///**
// * The Class LOGGER.
// * 
// * @author Atos Origin
// * @version 1.0
// */
//public final class Logger {
//	
//    /**
//     * Attribut de type Logger.
//     */
//    private org.apache.log4j.Logger logger = null;
//    
//    /**
//     * Constructeur.
//     * @param name de type String
//     */
//    protected Logger() {
//        logger = org.apache.log4j.LogManager.getRootLogger();
//    }
//
//    /**
//     * Constructeur.
//     * @param name de type String
//     */
//    protected Logger(String name) {
//        logger = org.apache.log4j.LogManager.getLogger(name);
//    }
//    
//	/**
//	 * Loggue un message avec le niveau FATAL.
//	 * 
//	 * @param message the message
//	 * @param t the t
//	 */
//	public void fatal(String message, Throwable t) {
//		logger.fatal(message, t);
//	}
//
//	/**
//	 * Loggue un message avec le niveau FATAL.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 */
//	public void fatal(String message) {
//		logger.fatal(message);
//	}
//
//	/**
//	 * Loggue un message avec le niveau FATAL.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 * @param t the t
//	 */
//	public void fatal(Class classe, String methode, String message, Throwable t) {
//		logger.fatal(message, t);
//	}
//
//	/**
//	 * Loggue un message avec le niveau FATAL.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 */
//	public void fatal(Class classe, String methode, String message) {
//		logger.fatal(message);
//	}
//	
//	/**
//	 * Loggue un message avec le niveau ERROR.
//	 * 
//	 * @param message the message
//	 * @param t the t
//	 */
//	public void error(String message, Throwable t) {
//		logger.error(message, t);
//	}
//
//	/**
//	 * Loggue un message avec le niveau ERROR.
//	 * 
//	 * @param message the message
//	 */
//	public void error(String message) {
//		logger.error(message);
//	}	
//
//	/**
//	 * Loggue un message avec le niveau ERROR.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 * @param t the t
//	 */
//	public void error(Class classe, String methode, String message, Throwable t) {
//		logger.error(message, t);
//	}
//
//	/**
//	 * Loggue un message avec le niveau ERROR.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 */
//	public void error(Class classe, String methode, String message) {
//		logger.error(message);
//	}
//	
//	/**
//	 * Loggue un message avec le niveau WARN.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 */
//	public void warn(Class classe, String methode, String message) {
//		logger.warn(message);
//	}
//
//	/**
//	 * Loggue un message avec le niveau DEBUG.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message
//	 */
//	public void debug(Class classe, String methode, String message) {
//		logger.debug(message);
//	}
//
//	/**
//	 * Loggue un message avec le niveau INFO.
//	 * 
//	 * @param classe the classe
//	 * @param methode the methode
//	 * @param message the message
//	 */
//	public void info(Class classe, String methode, String message) {
//		logger.info(message);
//	}
//	
//	/**
//	 * Loggue un message avec le niveau WARN.
//	 * 
//	 * @param message the message
//	 */
//	public void warn(String message) {
//		logger.warn(message);
//	}
//
//	/**
//	 * Loggue un message avec le niveau DEBUG.
//	 * 
//	 * @param message
//	 */
//	public void debug(String message) {
//		logger.debug(message);
//	}
//
//	/**
//	 * Loggue un message avec le niveau INFO.
//	 * 
//	 * @param message the message
//	 */
//	public void info(String message) {
//		logger.info(message);
//	}
//
//}
