package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
@XmlType(name="DeactivatedSRV" , propOrder = { "CO_ID", "CO_ID_PUB", "RPCODE", "CS_ID" , "CS_ID_PUB" , "deactivateServices" })
public class BDeactivatedSRV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long CO_ID;
	private String CO_ID_PUB;
	private long RPCODE;
	private long CS_ID;
	private String CS_ID_PUB;
	
	
	public long getRPCODE() {
		return RPCODE;
	}

	public void setRPCODE(long rPCODE) {
		RPCODE = rPCODE;
	}

	public long getCS_ID() {
		return CS_ID;
	}

	public void setCS_ID(long cS_ID) {
		CS_ID = cS_ID;
	}

	public String getCS_ID_PUB() {
		return CS_ID_PUB;
	}

	public void setCS_ID_PUB(String cS_ID_PUB) {
		CS_ID_PUB = cS_ID_PUB;
	}

	private List<BContractedServices> deactivateServices= new ArrayList<BContractedServices>();

	public List<BContractedServices> getDeactivateServices() {
		return deactivateServices;
	}

	public void setDeactivateServices(List<BContractedServices> deactivateServices) {
		this.deactivateServices = deactivateServices;
	}

	public long getCO_ID() {
		return CO_ID;
	}

	public void setCO_ID(long cO_ID) {
		CO_ID = cO_ID;
	}

	public String getCO_ID_PUB() {
		return CO_ID_PUB;
	}

	public void setCO_ID_PUB(String cO_ID_PUB) {
		CO_ID_PUB = cO_ID_PUB;
	}

}
