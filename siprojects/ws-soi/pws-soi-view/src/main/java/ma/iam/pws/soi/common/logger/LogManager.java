package ma.iam.pws.soi.common.logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe assurant la trace des messages de log.
 * <br/><br/>
 * Projet : WS MT
 * Créé le 5 oct. 2010
 * 
 * @author mei
 */
public final class LogManager {

    /**
     * Attribut stockant l'instance unique de LogManager.
     */
     private static LogManager instance;

     /**
      * Attribut pour stocker les loggers par modules.
      */
     private static Map<String, WSLogger> loggers = null;

    /**
     * Constructeur redéfini comme étant privé pour forcer
     * l'appel à la méthode getInstance().
     */
    private LogManager() {
        if (null == loggers) {
        	loggers = new HashMap<String, WSLogger>();
        }
    }

    /**
     * Récupère l'instance unique de la class LogManager.
     * @since 4.0
     * @return LogManager
     */
    public static LogManager getInstance() {
        if (null == instance) {
            instance = new LogManager();
        }
        return instance;
    }
    
	/**
     * Permet de récupérer (ou créer s’il n’existe pas déjà)le Logger associé
     * au module dont le nom est fourni parmi les paramètres.
     * @param name de type String
     * @return Logger
     */
    public WSLogger getLogger(String name) {
    	WSLogger logger = loggers.get(name);
        if (logger == null) {
        	logger = new WSLogger(name);
            loggers.put(name, logger);
        }
        return logger;
    }
}
