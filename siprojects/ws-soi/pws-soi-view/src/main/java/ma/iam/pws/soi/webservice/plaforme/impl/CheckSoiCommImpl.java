/**
 * 
 */
package ma.iam.pws.soi.webservice.plaforme.impl;

import java.util.HashMap;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.plateforme.ResultBean;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.plaforme.CheckSoiComm;

import com.alcatel.ccl.client.CMSException;

/**
 * @author mounir_mabchour
 *
 */
//@javax.jws.WebService(endpointInterface = "ma.iam.ws.webservice.plaforme.CheckSoiComm",
//serviceName = "CheckSoiCommService"  )
public class CheckSoiCommImpl implements CheckSoiComm {
	
	private String securityName; 
	private String securityVersion;
 
	
	
	
	public final String getSecurityName() {
		return securityName;
	}
	public final void setSecurityName(String securityName) {
		this.securityName = securityName;
	}
	public final String getSecurityVersion() {
		return securityVersion;
	}
	public final void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}
	private static final Logger logger = LogManager.getLogger(CheckSoiCommImpl.class.getName());
	//@Override
	public ResultBean CheckSoiComm() throws FaultWS {
		// TODO Auto-generated method stub
		ResultBean rb = new ResultBean();
		rb.setCodeRetour("OK");
		rb.setDescRetour("PlateForme available");	
		try{
			SOIService soiComm = new SOIService(securityName,securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object >  outParams = soiComm.executeCommand(SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return rb; 
		} catch (CMSException cex){
				if (cex.getMessage().contains("CMS client not connected") || cex.getMessage().contains("CMS connection error : Utilisateur ou mot de passe incorrect")) {
					return rb;
				}
			rb.setCodeRetour("RC1505");
			rb.setDescRetour("PlateForme unavailable");
			return rb; 
		} catch (Exception e) {
			logger.error(getClass().toString() +  " : Exception "  + e.getMessage());		
			rb.setCodeRetour("RC1505");
			rb.setDescRetour("PlateForme unavailable");
			return rb; 
	}
	}
}
