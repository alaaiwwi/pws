package ma.iam.pws.soi.beans.plateforme;

public class ResultBean {
	
	private String codeRetour;
	private String descRetour;
	
	public String getCodeRetour() {
		return codeRetour;
	}
	public void setCodeRetour(String codeRetour) {
		this.codeRetour = codeRetour;
	}
	public String getDescRetour() {
		return descRetour;
	}
	public void setDescRetour(String descRetour) {
		this.descRetour = descRetour;
	}
	

}
