package ma.iam.pws.soi.webservice.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapter;
import ma.iam.pws.soi.adapter.BContractedServicesAdapter;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.services.ServiceOperater;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;

//@javax.jws.WebService
public class ServiceOperaterImpl implements ServiceOperater {
	
	private static final Logger logger = LogManager.getLogger(ServiceOperaterImpl.class.getName()) ;
	
	@Resource
	WebServiceContext wsContext;
	
	private SoiContext soiContext ;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}
	
	private Properties msgProperties;
	
	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}
	
	private String securityName; 
	private String securityVersion;
	


	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}
	

	private UserCache userCache = new NullUserCache();

	private String cilName;
	private String cilVersion;
	
	
	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}
	
	
	
	//@Override
	//>******* Created, A.ESSA DI5440
	public Bcontract ReadServices(String username, long co_id) throws FaultWS {
		logger.info( getClass().toString() + " ReadServices : " + co_id  );
		
		
		UserDetails user = userCache.getUserFromCache(wsContext.getUserPrincipal().getName());
		//wsContext.getMessageContext().
		//SOIService soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );
		
				SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,soiContext );
				
				if ((soiComm==null) || (soiComm!=null && !soiComm.isConnectionOk())  ) {
					String password = null;
					try {
						String encryptedPass = PasswordUtils.getPassword(username);
						if(encryptedPass!=null)
							password = PasswordUtils.decryptPassword(username,encryptedPass);
						else{
							throw new FaultWS(Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,msgProperties);
						}
					} catch (Exception e) {
						throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,msgProperties);
					}
					
					if (SoiUtils.Authentify(wsContext, username ,  password, msgProperties, securityName, securityVersion, cilName, cilVersion, soiContext, userCache)==null)
						throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,msgProperties);	
				}
				
				//soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );
				
				soiComm = SoiUtils.getCILSOIServiceByUser2(username,soiContext );
		if (soiComm==null) throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,msgProperties);
		
		try{
			// Get info service for contract
			HashMap<String ,Object> inParams = new HashMap<String ,Object>();
			if (co_id==0) {
				logger.error( getClass().toString() + "WS ReadServices : Erreur Input value Parameters CO_ID "  );
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_DEPNDCY_SNCODE_MISSING,msgProperties);
			}
			if (co_id!=0) {
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("SYNC_WITH_DB",true);
			}
			//if (co_code!=null && !"".equals(co_code)) inParams.put("CO_ID_PUB", co_code);
				
				//Get contract info
				Bcontract bcontract = new Bcontract();
				
				HashMap<String, Object >  outParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParams);
				bcontract= BContractAdapter.getMainContractInfo(outParams);
				inParams.remove("SYNC_WITH_DB");
				outParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
				BContractedServicesAdapter.buildContractedServicesBean(outParams, bcontract);
				// retrieve packages
				inParams = new HashMap<String ,Object>();
				inParams.put("RPCODE", bcontract.getRPCODE());
				HashMap<String, Object >  outParamspackage = soiComm.executeCommand(SoiUtils.CMD_SERVICES_PKG_READ, inParams);
				HashMap<String, Object >[] iter_pckg = (HashMap<String, Object >[]) outParamspackage.get("NUM_SP");
				HashMap < String ,Object > uncontractedService =  new HashMap<String ,Object>();
				
				// Get Info PrgCode
				HashMap<String ,Object> inParamsCustomer = new HashMap<String ,Object>();
				inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
				HashMap<String, Object >  outParamsCustomer = soiComm.executeCommand(SoiUtils.CMD_CUSTOMER_READ, inParamsCustomer);
				String prgCode = outParamsCustomer.get("PRG_CODE").toString() ; 
				
				//Get Allowed Services
				
				List <Long>  allowedSncode = new ArrayList<Long>();
				
				HashMap<String ,Object> inParamsAllServices = new HashMap<String ,Object>();
				inParamsAllServices.put("PRG_CODE", prgCode);
				inParamsAllServices.put("RPCODE",  bcontract.getRPCODE());
				HashMap<String, Object >  outAllowedServices = soiComm.executeCommand(SoiUtils.CMD_ALLOWED_SERVICES_READ, inParamsAllServices);				
				
								
				HashMap<String, Object > []  outAllowedServicesList =  ( HashMap<String, Object > [] ) outAllowedServices.get("LIST_SP_CODE");
				
				for (int i = 0; i < outAllowedServicesList.length; i++) {
					HashMap<String, Object> [] hashMap =  ( HashMap<String, Object > [] )  outAllowedServicesList[i].get("LIST_SN_CODE");
					for (int j = 0; j < hashMap.length; j++) {
						HashMap<String, Object> hashMap2 = hashMap[j];
						allowedSncode.add(new Long(hashMap2.get("SNCODE").toString()));
					}
				}

 				for (int i = 0; i < iter_pckg.length; i++) {
					HashMap<String, Object> hashMap = iter_pckg[i];
					inParams.put("SPCODE", hashMap.get("SPCODE"));
					HashMap<String, Object >  outUncontractedService = soiComm.executeCommand(SoiUtils.CMD_SERVICES_READ, inParams);	
					//((HashMap<String, Object > [])outUncontractedService.get("NUM_SV"))[0].put("SPCODE", hashMap.get("SPCODE"));
					uncontractedService.put(hashMap.get("SPCODE").toString(),outUncontractedService.get("NUM_SV"));
					inParams.remove("SPCODE");
				}
 				BContractedServicesAdapter.buildUnContractedServicesBean(uncontractedService, bcontract , allowedSncode);
 				logger.info( getClass().toString() + " ReadServices : End to load bean services contracted or not for co_id :  " + co_id );
			return bcontract;
			
			} catch (CMSException cex){	
					if (cex.getErrors()!=null && cex.getErrors().length >0 ) {
						logger.error(getClass().toString() +  " : CMSException "  + cex.getErrors()[0].getAdditionalInfo());
						throw new FaultWS(cex.getErrors()[0].getErrorCode(), cex.getErrors()[0].getAdditionalInfo() );	   					
					}else {
						if (cex.getMessage().contains("CMS client not connected")) {
							throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,msgProperties);	
						}else {
							logger.error(getClass().toString() +  " : CMSException "  + cex.getMessage());
							throw new FaultWS("RC1000", cex.getMessage() );	  
						} 
					}   
			} catch (Exception e) {
				if (e instanceof FaultWS) throw (FaultWS)e;
				logger.error(getClass().toString() +  " : Exception "  + e.getMessage());
				e.printStackTrace();
				throw new FaultWS("RC1000", "Internal Error" );
			}
	}

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage()
							.contains(
									"CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());

			return rb;
		}

	}

}
