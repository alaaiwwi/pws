package ma.iam.pws.soi.webservice.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapterAll;
import ma.iam.pws.soi.adapter.BContractedServicesAdapterAll;
import ma.iam.pws.soi.adapter.ServiceParamAdapter;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.services.ConfServiceBean;
import ma.iam.pws.soi.beans.services.ConfServiceBeanPub;
import ma.iam.pws.soi.beans.services.ConfServicePackagePub;
import ma.iam.pws.soi.beans.services.ParamValueBean;
import ma.iam.pws.soi.beans.services.ParamValueBeanPub;
import ma.iam.pws.soi.beans.services.SPackage;
import ma.iam.pws.soi.beans.services.ValueBeanPub;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.services.ReadAllServices;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;

//@javax.jws.WebService
public class ReadAllServicesImpl implements ReadAllServices {

	private static final Logger logger = LogManager
			.getLogger(ReadAllServicesImpl.class.getName());
	private static List myList = new ArrayList();
	private static String[] listservices = new String[] { "NUMPR", "NPT17",
			"FEF", "NUILM", "NUILF", "FNILG", "NMPPR", "NPMPO", "FORHC",
			"FONI", "FONII" };

	static {
		Collections.addAll(myList, listservices);
	}

	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	private String securityName;
	private String securityVersion;

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	private UserCache userCache = new NullUserCache();

	private String cilName;
	private String cilVersion;

	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	// @Override
	// >******* Created, A.ESSA DI5440
	public BcontractAll ReadAllServices(String username, long co_id)
			throws FaultWS {
		logger.info(getClass().toString() + " ReadServices : " + co_id);

		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		// wsContext.getMessageContext().
		// SOIService soiComm =
		// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}

		// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);

		try {
			// Get info service for contract
			HashMap<String, Object> inParams = new HashMap<String, Object>();
			if (co_id == 0) {
				logger.error(getClass().toString()
						+ "WS ReadServices : Erreur Input value Parameters CO_ID ");
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_DEPNDCY_SNCODE_MISSING,
						msgProperties);
			}
			if (co_id != 0) {
				inParams.put("CO_ID", new Long(co_id).longValue());

				inParams.put("SYNC_WITH_DB", true);
			}
			// if (co_code!=null && !"".equals(co_code))
			// inParams.put("CO_ID_PUB", co_code);

			// Get contract info
			BcontractAll bcontract = new BcontractAll();

			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParams);
			inParams.remove("SYNC_WITH_DB");
			bcontract = BContractAdapterAll.getMainContractInfo(outParams);
			outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
			BContractedServicesAdapterAll.buildContractedServicesBean(
					outParams, bcontract);
			// retrieve packages
			inParams = new HashMap<String, Object>();
			inParams.put("RPCODE", bcontract.getRPCODE());
			//

			// Get Info PrgCode
			HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
			inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
			HashMap<String, Object> outParamsCustomer = soiComm.executeCommand(
					SoiUtils.CMD_CUSTOMER_READ, inParamsCustomer);
			String prgCode = outParamsCustomer.get("PRG_CODE").toString();

			Map<Long, Map> prgCodesRatePlans = SoiUtils.READ_SERVICES
					.get(prgCode);
			Map mapServbices = (prgCodesRatePlans != null) ? prgCodesRatePlans
					.get(bcontract.getRPCODE()) : null;

			if (mapServbices != null && !mapServbices.isEmpty()) {
				// Get Allowed Services

				BContractedServicesAdapterAll.buildUnContractedServicesBean(
						(HashMap<String, Object>) mapServbices
								.get(SoiUtils.ALLOWED_SERVICES_CACHING),
						bcontract, (List<Long>) mapServbices
								.get(SoiUtils.ALLOWED_SNCODES_CACHING));
			}

			else {
				// Get Allowed Services
				HashMap<String, Object> outParamspackage = soiComm
						.executeCommand(SoiUtils.CMD_SERVICES_PKG_READ,
								inParams);
				HashMap<String, Object>[] iter_pckg = (HashMap<String, Object>[]) outParamspackage
						.get("NUM_SP");
				HashMap<String, Object> uncontractedService = new HashMap<String, Object>();

				List<Long> allowedSncode = new ArrayList<Long>();

				HashMap<String, Object> inParamsAllServices = new HashMap<String, Object>();
				inParamsAllServices.put("PRG_CODE", prgCode);
				inParamsAllServices.put("RPCODE", bcontract.getRPCODE());
				HashMap<String, Object> outAllowedServices = soiComm
						.executeCommand(SoiUtils.CMD_ALLOWED_SERVICES_READ,
								inParamsAllServices);

				HashMap<String, Object>[] outAllowedServicesList = (HashMap<String, Object>[]) outAllowedServices
						.get("LIST_SP_CODE");

				for (int i = 0; i < outAllowedServicesList.length; i++) {
					HashMap<String, Object>[] hashMap = (HashMap<String, Object>[]) outAllowedServicesList[i]
							.get("LIST_SN_CODE");
					for (int j = 0; j < hashMap.length; j++) {
						HashMap<String, Object> hashMap2 = hashMap[j];
						allowedSncode.add(new Long(hashMap2.get("SNCODE")
								.toString()));
					}
				}

				for (int i = 0; i < iter_pckg.length; i++) {
					HashMap<String, Object> hashMap = iter_pckg[i];
					inParams.put("SPCODE", hashMap.get("SPCODE"));
					HashMap<String, Object> outUncontractedService = soiComm
							.executeCommand(SoiUtils.CMD_SERVICES_READ,
									inParams);
					// ((HashMap<String, Object
					// >[])outUncontractedService.get("NUM_SV"))[0].put("SPCODE",
					// hashMap.get("SPCODE"));
					uncontractedService.put(hashMap.get("SPCODE").toString(),
							outUncontractedService.get("NUM_SV"));
					inParams.remove("SPCODE");
				}
				BContractedServicesAdapterAll.buildUnContractedServicesBean(
						uncontractedService, bcontract, allowedSncode);
			}
			logger.info(getClass().toString()
					+ " ReadServices : End to load bean services contracted or not for co_id :  "
					+ co_id);
			return bcontract;

		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(),
						cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());
			e.printStackTrace();
			throw new FaultWS("RC1000", "Internal Error");
		}
	}

	/*
	 * FC4965 : MMA Description : Used to extract values parameters for services
	 * a specific contract
	 */

	public ConfServiceBeanPub ReadConfigService(String username, long co_id,
			long sncode) throws FaultWS {
		logger.info(getClass().toString() + " ReadConfigService ==> CoId :"
				+ co_id + " SnCode :" + sncode);
		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);
		ConfServiceBeanPub cfgSvcBeanPub = null;
		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}
		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);
		try {
			// checking entry arguments
			HashMap<String, Object> inParams = new HashMap<String, Object>();
			if (co_id == 0) {
				logger.error(getClass().toString()
						+ "WS ReadConfigService : Erreur Input value Parameters CO_ID ");
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_READSVC_INPUT_MISSING,
						msgProperties);
			}
			if (co_id != 0) {
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("SYNC_WITH_DB", true);
			}
			// Get contract info
			BcontractAll bcontract = new BcontractAll();
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParams);
			inParams.remove("SYNC_WITH_DB");
			bcontract = BContractAdapterAll.getMainContractInfo(outParams);
			BContractedServicesAdapterAll.buildContractedServicesBean(
					outParams, bcontract);
			// Check status for contract
			Integer coStatus = (Integer) outParams.get("CO_STATUS");
			if (!coStatus.toString().equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACTIVECONTRACT_ERROR,
						msgProperties);
			}

			// Checking pending request

			if (BContractedServicesAdapterAll.hasPendingRequest(soiComm,
					bcontract)) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CONTRACT_HAS_PENDINGREQUEST,
						msgProperties);
			}

			// Checking sncode in Cache MAP
			ConfServiceBean cfSvcBean = SoiUtils.CACHE_SERVICES_PARAMETERS
					.get(new Long(sncode));

			if (cfSvcBean == null) {
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_SERVICE_NOT_FOUND,
						msgProperties);
			}

			// Checking if service has parameters
			if (cfSvcBean.getNUM_PARAM() == null
					|| cfSvcBean.getNUM_PARAM().size() == 0) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_SERVICE_NO_PARAMETER,
						msgProperties);
			}

			// Checking if the user has access to this service
			if (!BContractedServicesAdapterAll.user_has_auth_class(soiComm,
					bcontract, sncode, msgProperties)) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATHORIZED,
						msgProperties);
			}

			// rejecting restricted services.

			if (myList.contains(cfSvcBean.getSncode_pub())) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_SERVICE_NOT_SUPPORTED,
						msgProperties);
			}

			// >******* Update, A.ESSA DI6717
			HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
			inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
			HashMap<String, Object> outParamsCustomer = soiComm.executeCommand(
					SoiUtils.CMD_CUSTOMER_READ, inParamsCustomer);
			String prgcode = outParamsCustomer.get("PRG_CODE").toString();
			HashMap<String, List<String>> eligSrvPrm = SoiUtils
					.getFilteredPrmValues(soiComm, co_id, sncode,
							msgProperties, bcontract, prgcode, null);

			// <******* Update, A.ESSA DI6717
			// Get Values for Parameters
			cfgSvcBeanPub = new ConfServiceBeanPub(cfSvcBean);
			List<ParamValueBeanPub> paramValueBeanPub = new ArrayList<ParamValueBeanPub>();
			for (Iterator iterator = cfSvcBean.getNUM_PARAM().iterator(); iterator
					.hasNext();) {
				ParamValueBean prmValueBean = (ParamValueBean) iterator.next();
				inParams.clear();
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("GET_VALUES", true);
				inParams.put("PRM_ID", prmValueBean.getPrm_id());
				inParams.put("SCCODE", cfSvcBean.getSccode());
				inParams.put("SNCODE", cfSvcBean.getSncode());
				inParams.put("RPCODE", bcontract.getRPCODE());
				HashMap<String, Object> outParamsBean = soiComm.executeCommand(
						SoiUtils.CMD_PARAMETER_READ, inParams);
				ParamValueBeanPub prmValueBeanPubtmp = new ParamValueBeanPub();
				// if ("INT3G".equals(cfSvcBean.getSncode_pub()) &&
				// ("DEBIT".equals(prmValueBean.getPrm_id_pub()))) {
				// prmValueBeanPubtmp=ServiceParamAdapter.getParamValueBeanPubForInt3G(outParamsBean,
				// cfSvcBean, bcontract, soiComm);
				// }else {
				prmValueBeanPubtmp = ServiceParamAdapter.getParamValueBeanPub(
						outParamsBean, cfSvcBean);
				// }

				// >******* Update, A.ESSA DI6717
				List<ValueBeanPub> vals = SoiUtils.filterPrm(prmValueBeanPubtmp
						.getVALUES(), eligSrvPrm.get((new Long(prmValueBean
						.getPrm_id())).toString()));

				prmValueBeanPubtmp.setVALUES(vals);

				// <******* Update, A.ESSA DI6717
				paramValueBeanPub.add(prmValueBeanPubtmp);
			}
			/*
			 * if ("INT3G".equals(cfSvcBean.getSncode_pub())) {
			 * List<ParamValueBeanPub> paramValueBeanPubnew= new
			 * ArrayList<ParamValueBeanPub>(); // filtre about Service Internet
			 * 3G List<DebitConfBean> debitCfg =
			 * ServiceParamAdapter.getEligibleDebit(bcontract, soiComm); if
			 * (debitCfg.size()>0) { for (Iterator<ParamValueBeanPub> iterator2
			 * = paramValueBeanPub.iterator(); iterator2.hasNext();) {
			 * ParamValueBeanPub object = (ParamValueBeanPub) iterator2.next();
			 * if ("DEBIT".equals(object.getPrm_id_pub())) { List<ValueBeanPub>
			 * valueBeanspub = new ArrayList<ValueBeanPub>(); for
			 * (Iterator<ValueBeanPub> iterator3 =
			 * object.getVALUES().iterator(); iterator3.hasNext();) {
			 * ValueBeanPub valueBean = (ValueBeanPub) iterator3.next(); for
			 * (Iterator iterator = debitCfg.iterator(); iterator .hasNext();) {
			 * DebitConfBean debiCfgm = (DebitConfBean) iterator .next();
			 * if(valueBean.getValue_des().equals(debiCfgm.getDebit())){
			 * valueBeanspub.add(valueBean); } } }
			 * object.setVALUES(valueBeanspub);
			 * paramValueBeanPubnew.add(object); }else{
			 * paramValueBeanPubnew.add(object); } }
			 * cfgSvcBeanPub.setPARAMETERS(paramValueBeanPubnew); }else {
			 * cfgSvcBeanPub.setPARAMETERS(paramValueBeanPub); } }else
			 */
			if ("FVOIP".equals(cfSvcBean.getSncode_pub())) {
				boolean isfree = ServiceParamAdapter.isFreeVoip(bcontract,
						soiComm);
				List<ParamValueBeanPub> paramValueBeanPubnew = new ArrayList<ParamValueBeanPub>();
				for (Iterator<ParamValueBeanPub> iterator2 = paramValueBeanPub
						.iterator(); iterator2.hasNext();) {
					ParamValueBeanPub object = (ParamValueBeanPub) iterator2
							.next();
					if ("FVOIP".equals(object.getPrm_id_pub())) {
						List<ValueBeanPub> valueBeanspub = new ArrayList<ValueBeanPub>();
						for (Iterator<ValueBeanPub> iterator3 = object
								.getVALUES().iterator(); iterator3.hasNext();) {
							ValueBeanPub valueBean = (ValueBeanPub) iterator3
									.next();
							if ((isfree && valueBean.getValue_des().equals(
									"0 DH"))
									|| (!isfree && valueBean.getValue_des()
											.equals("50 DH"))) {
								valueBeanspub.add(valueBean);
							}
						}
						object.setVALUES(valueBeanspub);
						paramValueBeanPubnew.add(object);
					} else {
						paramValueBeanPubnew.add(object);
					}
				}
				cfgSvcBeanPub.setPARAMETERS(paramValueBeanPubnew);
			} else if ("F1G05".equals(cfSvcBean.getSncode_pub())) {
				List<ParamValueBeanPub> paramValueBeanPubnew = new ArrayList<ParamValueBeanPub>();
				for (Iterator<ParamValueBeanPub> iterator2 = paramValueBeanPub
						.iterator(); iterator2.hasNext();) {
					ParamValueBeanPub object = (ParamValueBeanPub) iterator2
							.next();
					if ("VOL512".equals(object.getPrm_id_pub())) {
						List<ValueBeanPub> valueBeanspub = new ArrayList<ValueBeanPub>();
						for (Iterator<ValueBeanPub> iterator3 = object
								.getVALUES().iterator(); iterator3.hasNext();) {
							ValueBeanPub valueBean = (ValueBeanPub) iterator3
									.next();
							if ((!valueBean.getValue_des().equals("300Mo"))) {
								valueBeanspub.add(valueBean);
							}
						}
						object.setVALUES(valueBeanspub);
						paramValueBeanPubnew.add(object);
					} else {
						paramValueBeanPubnew.add(object);
					}
				}
				cfgSvcBeanPub.setPARAMETERS(paramValueBeanPubnew);
			} else if ("Z3G36".equals(cfSvcBean.getSncode_pub())) {
				List<ParamValueBeanPub> paramValueBeanPubnew = new ArrayList<ParamValueBeanPub>();
				for (Iterator<ParamValueBeanPub> iterator2 = paramValueBeanPub
						.iterator(); iterator2.hasNext();) {
					ParamValueBeanPub object = (ParamValueBeanPub) iterator2
							.next();
					if ("DEBIN".equals(object.getPrm_id_pub())) {
						List<ValueBeanPub> valueBeanspub = new ArrayList<ValueBeanPub>();
						for (Iterator<ValueBeanPub> iterator3 = object
								.getVALUES().iterator(); iterator3.hasNext();) {
							ValueBeanPub valueBean = (ValueBeanPub) iterator3
									.next();
							if ((valueBean.getValue().equals("INJAZ15Go") || valueBean
									.getValue().equals("INJAZ30Go"))) {
								valueBeanspub.add(valueBean);
							}
						}
						object.setVALUES(valueBeanspub);
						paramValueBeanPubnew.add(object);
					} else {
						paramValueBeanPubnew.add(object);
					}
				}
				cfgSvcBeanPub.setPARAMETERS(paramValueBeanPubnew);
			} else {
				cfgSvcBeanPub.setPARAMETERS(paramValueBeanPub);
			}
		} catch (CMSException ex) {
			// TODO: handle Cmsexception
			if (ex.getMessage() != null)
				logger.info(ex.getMessage());
			if (ex.toString().contains("errorCode = RC6700")) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CONTRACT_NOT_FOUND,
						msgProperties);
			} else {
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_CMS_ERROR,
						msgProperties);
			}

		}

		try {
			cfgSvcBeanPub.setPARAMETERS(ServiceUtils.remove_Auto_params2(
					soiComm, cfgSvcBeanPub.getPARAMETERS(), sncode));
		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(),
						cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		}
		return cfgSvcBeanPub;
	}

	public ConfServicePackagePub ReadOpenServicePack(String username,
			long co_id, long sncode) throws FaultWS {

		logger.info(getClass().toString() + " ReadConfigService ==> CoId :"
				+ co_id + " SnCode :" + sncode);
		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);
		ConfServicePackagePub cfgSvcPkgBeanPub = new ConfServicePackagePub();
		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}
		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);
		try {
			// checking entry arguments
			HashMap<String, Object> inParamsCtr = new HashMap<String, Object>();
			if (co_id == 0) {
				logger.error(getClass().toString()
						+ "WS ReadConfigService : Erreur Input value Parameters CO_ID ");
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_READSVC_INPUT_MISSING,
						msgProperties);
			}
			if (co_id != 0) {

				inParamsCtr.put("SYNC_WITH_DB", true);
				inParamsCtr.put("CO_ID", new Long(co_id).longValue());
			}
			// Get contract info
			BcontractAll bcontract = new BcontractAll();
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParamsCtr);
			inParamsCtr.remove("SYNC_WITH_DB");
			bcontract = BContractAdapterAll.getMainContractInfo(outParams);
			BContractedServicesAdapterAll.buildContractedServicesBean(
					outParams, bcontract);
			// Check status for contract
			Integer coStatus = (Integer) outParams.get("CO_STATUS");
			if (!coStatus.toString().equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACTIVECONTRACT_ERROR,
						msgProperties);
			}

			// Checking sncode in Cache MAP
			ConfServiceBean cfSvcBean = SoiUtils.CACHE_SERVICES_PARAMETERS
					.get(new Long(sncode));

			if (cfSvcBean == null) {
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_SERVICE_NOT_FOUND,
						msgProperties);
			}

			// Get Info PrgCode
			HashMap<String, Object> inParamsCustomer = new HashMap<String, Object>();
			inParamsCustomer.put("CS_ID", bcontract.getCS_ID());
			HashMap<String, Object> outParamsCustomer = soiComm.executeCommand(
					SoiUtils.CMD_CUSTOMER_READ, inParamsCustomer);
			String prgCode = outParamsCustomer.get("PRG_CODE").toString();

			// Get Allowed Services

			List<Long> allowedSncode = new ArrayList<Long>();

			HashMap<String, Object> inParamsAllServices = new HashMap<String, Object>();
			inParamsAllServices.put("PRG_CODE", prgCode);
			inParamsAllServices.put("RPCODE", bcontract.getRPCODE());

			// retrieve packages
			HashMap<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("RPCODE", bcontract.getRPCODE());
			HashMap<String, Object> outParamspackage = soiComm.executeCommand(
					SoiUtils.CMD_SERVICES_PKG_READ, inParams);
			HashMap<String, Object>[] iter_pckg = (HashMap<String, Object>[]) outParamspackage
					.get("NUM_SP");

			HashMap<String, Object> outAllowedServices = soiComm
					.executeCommand(SoiUtils.CMD_ALLOWED_SERVICES_READ,
							inParamsAllServices);

			HashMap<String, Object>[] outAllowedServicesList = (HashMap<String, Object>[]) outAllowedServices
					.get("LIST_SP_CODE");

			List<SPackage> cfgPakage = new ArrayList<SPackage>();

			if (outAllowedServicesList == null
					|| outAllowedServicesList.length <= 0) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATTACHED,
						msgProperties);
			}

			for (int i = 0; i < outAllowedServicesList.length; i++) {
				HashMap<String, Object>[] hashMap = (HashMap<String, Object>[]) outAllowedServicesList[i]
						.get("LIST_SN_CODE");
				SPackage cfgPakagebean = new SPackage();
				for (int j = 0; j < hashMap.length; j++) {
					HashMap<String, Object> hashMap2 = hashMap[j];
					if (hashMap[j].get("SNCODE").toString()
							.equals(String.valueOf(sncode))) {
						cfgPakagebean.setSpcode(new Long(
								outAllowedServicesList[i].get("SPCODE")
										.toString()));
						cfgPakagebean.setSpcodepub((outAllowedServicesList[i]
								.get("SPCODE_PUB").toString()));
						cfgPakagebean
								.setPackagelibelle((String) SoiUtils.CACHE_SERVICES_PACKAGES.get(String
										.valueOf(outAllowedServicesList[i]
												.get("SPCODE"))));
						cfgPakage.add(cfgPakagebean);
					}
				}
			}
			if (cfgPakage.size() <= 0) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATTACHED,
						msgProperties);
			}
			cfgSvcPkgBeanPub.setSncode(sncode);
			cfgSvcPkgBeanPub.setSp_list(cfgPakage);

		} catch (CMSException ex) {
			// TODO: handle Cmsexception
			if (ex.getMessage() != null)
				logger.info(ex.getMessage());
			if (ex.toString().contains("errorCode = RC6700")) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CONTRACT_NOT_FOUND,
						msgProperties);
			} else {
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_CMS_ERROR,
						msgProperties);
			}

		}
		return cfgSvcPkgBeanPub;
	}

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage()
							.contains(
									"CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());

			return rb;
		}

	}

}
