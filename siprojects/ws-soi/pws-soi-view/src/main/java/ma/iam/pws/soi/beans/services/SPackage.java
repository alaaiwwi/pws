package ma.iam.pws.soi.beans.services;

public class SPackage {
private long spcode;
private String packagelibelle;
private String spcodepub;

/**
 * @return the packagelibelle
 */
public String getPackagelibelle() {
	return packagelibelle;
}
/**
 * @param packagelibelle the packagelibelle to set
 */
public void setPackagelibelle(String packagelibelle) {
	this.packagelibelle = packagelibelle;
}
/**
 * @return the spcode
 */
public long getSpcode() {
	return spcode;
}
/**
 * @param spcode the spcode to set
 */
public void setSpcode(long spcode) {
	this.spcode = spcode;
}
/**
 * @return the spcodepub
 */
public String getSpcodepub() {
	return spcodepub;
}
/**
 * @param spcodepub the spcodepub to set
 */
public void setSpcodepub(String spcodepub) {
	this.spcodepub = spcodepub;
}

}
