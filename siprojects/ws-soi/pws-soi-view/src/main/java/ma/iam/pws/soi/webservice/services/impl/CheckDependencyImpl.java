package ma.iam.pws.soi.webservice.services.impl;

/**
 * @author Alaa ESSA
 *
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.BContractAdapter;
import ma.iam.pws.soi.adapter.BContractedServicesAdapter;
import ma.iam.pws.soi.adapter.ServiceParamAdapter;
import ma.iam.pws.soi.beans.administration.UserBean;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.BDeactivatedSRV;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.contract.MassServicesEx;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.contract.bServicesList;
import ma.iam.pws.soi.beans.dependency.CheckDependencyBean;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.common.Message;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.CmsINTUtils;
import ma.iam.pws.soi.service.util.Dependency;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.tree.Node;
import ma.iam.pws.soi.tree.TreeUtils;
import ma.iam.pws.soi.webservice.services.CheckDependency;
import ma.iam.security.SecuredObject;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

@SecuredObject
@WebService(endpointInterface = "ma.iam.pws.soi.webservice.services.CheckDependency", serviceName = "getCheckDependencyService", targetNamespace = "http://services.webservice.ws.iam.ma/")
@SuppressWarnings({ "unchecked", "deprecation", "static-access", "rawtypes" })
public class CheckDependencyImpl implements CheckDependency {

	Contract contract = new Contract();
	ServiceUtils service = new ServiceUtils();

	private static final Logger logger = LogManager
			.getLogger(CheckDependencyImpl.class.getName());
	public static final String NUMBER_REGEX = "^[0-9]*$";

	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private UserCache userCache = new NullUserCache();

	/** Sets the users cache. Not required, but can benefit performance. */
	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	private String cilName;
	private String cilVersion;
	private String securityName;
	private String securityVersion;
	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	public final String getCilName() {
		return cilName;
	}

	public final void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public final String getCilVersion() {
		return cilVersion;
	}

	public final void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public final String getSecurityName() {
		return securityName;
	}

	public final void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public final String getSecurityVersion() {
		return securityVersion;
	}

	public final void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	public CheckDependencyBean getCheckDependency(String username, long co_id,
			long sncode, String op) throws FaultWS {
		MessageContext msgContext = wsContext.getMessageContext();

		String msisdn = null;

		UserBean myUser = new UserBean();
		CheckDependencyBean checkDep = new CheckDependencyBean();
		UserDetails user = userCache.getUserFromCache(wsContext
				.getUserPrincipal().getName());
		logger.info("Utilisateur : " + wsContext.getUserPrincipal().getName()
				+ " try to connect.");
		List<Dependency> servicesToAdd = new ArrayList();
		List<Dependency> servicesToDel = new ArrayList();

		if (op == null
				|| (!"ACTIVATE".equals(op.toUpperCase()) && !"DEACTIVATE"
						.equals(op.toUpperCase()))) {
			throw new FaultWS(
					Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_OP_TYPE_ERROR,
					msgProperties);
		}

		// SOIService soiComm =
		// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
				soiContext);

		if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
			String password = null;
			try {
				String encryptedPass = PasswordUtils.getPassword(username);
				if (encryptedPass != null)
					password = PasswordUtils.decryptPassword(username,
							encryptedPass);
				else {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
							msgProperties);
				}
			} catch (Exception e) {
				throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
						msgProperties);
			}

			if (SoiUtils.Authentify(wsContext, username, password,
					msgProperties, securityName, securityVersion, cilName,
					cilVersion, soiContext, userCache) == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);
		}

		// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

		soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
		if (soiComm == null)
			throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
					msgProperties);

		if (co_id > 0 && sncode > 0) {
			try {
				HashMap[] servDeps = ServiceUtils.getServDeps(soiComm);
				Node tree = TreeUtils.loadDependencyTree(soiComm, servDeps,
						msgProperties);

				HashMap[] deps = ServiceUtils.getPriority(soiComm);

				// Get info service for contract
				HashMap<String, Object> inParams = new HashMap<String, Object>();
				inParams.put("CO_ID", new Long(co_id).longValue());
				inParams.put("SYNC_WITH_DB", true);

				HashMap<String, Object> outParams = soiComm.executeCommand(
						SoiUtils.CMD_CONTRACT_READ, inParams);
				inParams.remove("SYNC_WITH_DB");
				Bcontract bcontract = BContractAdapter
						.getMainContractInfo(outParams);

				Integer coStatus = (Integer) outParams.get("CO_STATUS");
				if (!coStatus.toString().equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACTIVECONTRACT_ERROR,
							msgProperties);
				}

				outParams = soiComm.executeCommand(
						SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
				BContractedServicesAdapter.buildContractedServicesBean(
						outParams, bcontract);
				logger.info(getClass().toString()
						+ " ReadServices : End to load bean services contracted or not for co_id :  "
						+ bcontract.getCO_ID());

				List contrActivSusServ = getContrActivSuspendedServ(bcontract);
				List contrActivServ = ServiceUtils.getContrActivServ(bcontract);
				if (op.toUpperCase().equals("ACTIVATE")
						&& contrActivSusServ.contains(Long.valueOf(sncode)
								.toString())) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACT_SUS_SRV_ERROR,
							msgProperties);
				} else if (op.toUpperCase().equals("DEACTIVATE")
						&& !contrActivServ.contains(Long.valueOf(sncode)
								.toString())) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACT_SRV_ERROR,
							msgProperties);
				}
				List contrEligibleServ = ServiceUtils.getContrEligibleServ(
						bcontract, soiComm);

				if (!contrEligibleServ
						.contains(Long.valueOf(sncode).toString())) {
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ELG_ERROR,
							msgProperties);
				}

				Long tmcode = bcontract.getRPCODE();
				HashMap servAddDel = TreeUtils.getServicesToAddDel(tree, Long
						.valueOf(sncode).toString(), contrActivServ, op
						.toUpperCase(), deps, tmcode, contrEligibleServ,
						servDeps, msgProperties);

				if (servAddDel.get("EVALUATION") == null) {
					servAddDel.put("EVALUATION", true);
				}

				if (!(Boolean) servAddDel.get("EVALUATION"))
					throw new FaultWS(
							Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_INCONSIS_ERROR,
							msgProperties);
				else {
					servicesToAdd = (List) servAddDel.get("NODESTOACTIVATE");
					servicesToDel = (List) servAddDel.get("NODESTODECTIVATE");
				}

			} catch (CMSException cex) {
				if (cex.getErrors() != null && cex.getErrors().length > 0) {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getErrors()[0].getAdditionalInfo());
					throw new FaultWS(cex.getErrors()[0].getErrorCode(),
							cex.getErrors()[0].getAdditionalInfo());
				} else {
					if (cex.getMessage().contains("CMS client not connected")) {
						throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
								msgProperties);
					} else {
						logger.error(getClass().toString() + " : CMSException "
								+ cex.getMessage());
						throw new FaultWS("RC1000", cex.getMessage());
					}
				}
			} catch (Exception e) {
				if (e instanceof FaultWS)
					throw (FaultWS) e;
				logger.error(getClass().toString() + " : Exception "
						+ e.getMessage());
				e.printStackTrace();
				throw new FaultWS("RC1000", "Internal Error");
			}
		} else {
			throw new FaultWS(
					Constants.ERROR_CODE.MT_WS_ACTIVATESVC_VAL_TYPE_ERROR,
					msgProperties);
		}

		checkDep.setServicesToAdd(servicesToAdd);
		checkDep.setServicesToDel(servicesToDel);
		return checkDep;

	}

	@RolesAllowed("ROLE_WRITE")
	public MassServicesEx MassActivationDeactivationServicesImpl(
			String username, long co_id, bServicesList List1,
			bServicesList List2) throws FaultWS {
		logger.info(getClass().toString() + " Activate/deactivate Services : "
				+ List1 + ", " + List2 + " Contract : " + co_id);
		MassServicesEx SrvEx = new MassServicesEx();
		try {

			// UserDetails user =
			// userCache.getUserFromCache(wsContext.getUserPrincipal().getName());
			// wsContext.getMessageContext().

			// SOIService soiComm =
			// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

			SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username,
					soiContext);

			List coToDelFlag = new ArrayList();
		
			if ((soiComm == null)
					|| (soiComm != null && !soiComm.isConnectionOk())) {
				String password = null;
				try {
					String encryptedPass = PasswordUtils.getPassword(username);
					if (encryptedPass != null)
						password = PasswordUtils.decryptPassword(username,
								encryptedPass);
					else {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO,
								msgProperties);
					}
				} catch (Exception e) {
					throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO,
							msgProperties);
				}

				if (SoiUtils.Authentify(wsContext, username, password,
						msgProperties, securityName, securityVersion, cilName,
						cilVersion, soiContext, userCache) == null)
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
							msgProperties);
			}

			// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

			soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);
			if (soiComm == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED,
						msgProperties);

			List<MassServicesEx> ServicesEx = new ArrayList<MassServicesEx>();

			boolean result = false;
			if (co_id == 0 || (List1 == null && List2 == null)) {
				logger.error(getClass().toString()
						+ "WS ActivateService : Erreur Input value Parameters CO_ID | Services");
				// throw new
				// FaultWS("RC1500","WS Activate Service : Erreur Input value Parameters CO_ID | Services");
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_ACTIVATESVC_INPUT_MISSING,
						msgProperties);
			}

			List<bNewServices> ServicesToDel = new ArrayList();
			List<bNewServices> ServicesToAdd = new ArrayList();

			if (List1.getOp() == null
					|| List2.getOp() == null
					|| (!(List1.getOp().toUpperCase().equals("ACTIVATE") && List2
							.getOp().toUpperCase().equals("DEACTIVATE")))
					&& (!(List2.getOp().toUpperCase().equals("ACTIVATE") && List1
							.getOp().toUpperCase().equals("DEACTIVATE")))) {
				logger.error(getClass().toString()
						+ "WS ActivateService : Erreur in service operation");
				// throw new
				// FaultWS("RC1500","WS Activate Service : Erreur Input value Parameters CO_ID | Services");
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_OP_TYPE_ERROR,
						msgProperties);
			}
			if (List1.getOp().toUpperCase().equals("ACTIVATE")) {
				if (List1.getSERVICES() != null)
					ServicesToAdd = List1.getSERVICES();
				if (List2.getOp().toUpperCase().equals("DEACTIVATE")) {
					if (List2.getSERVICES() != null)
						ServicesToDel = List2.getSERVICES();
				}
			} else if (List1.getOp().toUpperCase().equals("DEACTIVATE")) {
				if (List1.getSERVICES() != null)
					ServicesToDel = List1.getSERVICES();
				if (List2.getOp().toUpperCase().equals("ACTIVATE")) {
					if (List2.getSERVICES() != null)
						ServicesToAdd = List2.getSERVICES();
				}
			}

			HashMap<String, Object> inParams = new HashMap<String, Object>();

			HashMap<String, Object> inParamss = new HashMap<String, Object>();
			inParamss.put("CO_ID", new Long(co_id).longValue());
			inParamss.put("SYNC_WITH_DB", true);

			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_READ, inParamss);
			inParamss.remove("SYNC_WITH_DB");
			Bcontract bcontract = BContractAdapter
					.getMainContractInfo(outParams);
			contract.setCoId(new Long(co_id).longValue());
			contract.setRpCode(bcontract.getRPCODE());

			Integer coStatus = (Integer) outParams.get("CO_STATUS");
			if (!coStatus.toString().equals(SoiUtils.SERVICE_STATUT_ACTIVE)) {
				throw new FaultWS(
						Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACTIVECONTRACT_ERROR,
						msgProperties);
			}

			outParams = soiComm.executeCommand(
					SoiUtils.CMD_CONTRACT_SERVICES_READ, inParamss);
			BContractedServicesAdapter.buildContractedServicesBean(outParams,
					bcontract);
			logger.info(getClass().toString()
					+ " ReadServices : End to load bean services contracted or not for co_id :  "
					+ bcontract.getCO_ID());

			if (ServicesToDel.size() != 0) {
				List contrActivServ = ServiceUtils.getContrActivServ(bcontract);
				for (Iterator it = ServicesToDel.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					Long snCode = Srv.getSNCODE();
					if (!contrActivServ.contains(snCode.toString())) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ACT_SRV_ERROR,
								msgProperties);
					}
					if (!ServiceUtils.isAllowedToBeDeactivated(
							bcontract.getRPCODE(), snCode, soiComm)) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED,
								msgProperties);
					}

					// >******* Update, A.ESSA DI6717
					// Checking if the user has access to this service

					if (SoiUtils.is_service_clcode(bcontract.getRPCODE(),
							snCode, Srv.getSPCODE(), 7)
							|| SoiUtils.is_service_clcode(
									bcontract.getRPCODE(), snCode,
									Srv.getSPCODE(), 8)
							|| SoiUtils.is_service_clcode(
									bcontract.getRPCODE(), snCode,
									Srv.getSPCODE(), 9)) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATHORIZED,
								msgProperties);
					}
					// <******* Update, A.ESSA DI6717

					// >******* Updated, A.ESSA FC5937
					if (",IN4FA,INT4G,INP4G,".contains(",".concat(
							ServiceUtils.getShdesFromSncode(snCode, soiComm))
							.concat(","))) {
						HashMap slaves = ServiceUtils.getSlavesDNs(new Long(
								co_id), "IND4G", soiComm);

						String dns = (String) slaves.get("DNS");
						if (dns != null && dns != "") {
							ArrayList params = new ArrayList();
							params.add(dns.substring(0, dns.length() - 2));
							throw new FaultWS(
									Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_SLAVES_ERROR,
									msgProperties, params);
						}
					}
					// <******* Updated, A.ESSA FC5937
				}
			}
			if (ServicesToAdd.size() != 0) {
				List contrEligibleServ = ServiceUtils.getContrEligibleServ(
						bcontract, soiComm);
				List contrActivServ = ServiceUtils.getContrActivServ(bcontract);
				for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					Long snCode = Srv.getSNCODE();
					if (!contrEligibleServ.contains(snCode.toString())) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_ELG_ERROR,
								msgProperties);
					}

					if (!ServiceUtils.isAllowedToBeDeactivated(
							bcontract.getRPCODE(), snCode, soiComm)) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED,
								msgProperties);
					}

					// >******* Update, A.ESSA DI6717
					// Checking if the user has access to this service

					if (SoiUtils.is_service_clcode(bcontract.getRPCODE(),
							snCode, Srv.getSPCODE(), 7)
							|| SoiUtils.is_service_clcode(
									bcontract.getRPCODE(), snCode,
									Srv.getSPCODE(), 8)
							|| SoiUtils.is_service_clcode(
									bcontract.getRPCODE(), snCode,
									Srv.getSPCODE(), 9)) {
						throw new FaultWS(
								Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATHORIZED,
								msgProperties);
					}
					// <******* Update, A.ESSA DI6717

					// >******* Updated, A.ESSA FC5937
					if ("IND4G".equals(ServiceUtils.getShdesFromSncode(snCode,
							soiComm))) {
						BcontractAll bAllContract = new BcontractAll();
						bAllContract.setCO_ID(bcontract.getCO_ID());
						bAllContract.setCO_ID_PUB(bcontract.getCO_ID_PUB());
						String value_des = ServiceParamAdapter
								.GetValueDesServicefromParameters("ODDS3",
										bAllContract, soiComm, "NDMASTER");
						if (!ServiceUtils.searchContractMaster4G(value_des,
								soiComm)) {
							ArrayList params = new ArrayList();
							params.add(value_des);
							throw new FaultWS(
									Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_MASTER_ERROR,
									msgProperties, params);
						}

					}
					// <******* Updated, A.ESSA FC5937

				}
			}
			// >******* Updated, A.ESSA FC5937
			List contrActivServ = ServiceUtils.getContrActivServ(bcontract);
			check4GServices(ServicesToAdd, ServicesToDel, bcontract, soiComm,
					contrActivServ);
			// <******* Updated, A.ESSA FC5937
			// >******* Updated, A.ESSA FC5197
			ArrayList returnarray = new ArrayList();

			List contrEligibleServ = ServiceUtils.getContrEligibleServ(
					bcontract, soiComm);

			ArrayList<bNewServices> servicesToActivate = new ArrayList<bNewServices>();
			ArrayList<bNewServices> servicesToDeactivate = new ArrayList<bNewServices>();

			Long mzSncode = Long.parseLong(ServiceUtils.getSncodeFromShdes(
					"WVG", soiComm));
			if (ServicesToAdd.size() > 1) {
				for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices Srv = new bNewServices();
					Srv = (bNewServices) it.next();
					Long snCode = Srv.getSNCODE();
					if (mzSncode.equals(snCode)) {

						List contrToActivServ = ServiceUtils
								.getContrToActivateServ(bcontract,
										ServicesToAdd);
						Long tmcode = bcontract.getRPCODE();
						List<Dependency> servicesToDel = new ArrayList();
						HashMap[] servDeps = ServiceUtils.getServDeps(soiComm);
						List<BContractedServices> services = bcontract
								.getContractedServices();
						Node tree = TreeUtils.loadDependencyTree(soiComm,
								servDeps, msgProperties);
						HashMap[] deps = ServiceUtils.getPriority(soiComm);
						HashMap servAddDel = TreeUtils.getServicesToAddDel(
								tree, mzSncode.toString(), contrToActivServ,
								"DEACTIVATE", deps, tmcode, contrEligibleServ,
								servDeps, msgProperties);
						if (servAddDel.get("EVALUATION") == null) {
							servAddDel.put("EVALUATION", true);
						}

						if (!(Boolean) servAddDel.get("EVALUATION"))
							throw new FaultWS(
									Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_INCONSIS_ERROR,
									msgProperties);
						else {
							servicesToDel = (List) servAddDel
									.get("NODESTODECTIVATE");
							bNewServices newSrv = null;
							Dependency cre = new Dependency();
							cre.setSncode(mzSncode);
							servicesToDel.add(cre);

							for (int ii = 0; ii < servicesToDel.size(); ii++) {
								Dependency temp = servicesToDel.get(ii);
								for (int k = 0; k < ServicesToAdd.size(); k++) {
									bNewServices tmpCoSrv = ServicesToAdd
											.get(k);
									if (tmpCoSrv.getSNCODE() == temp
											.getSncode().longValue()) {
										servicesToActivate.add(tmpCoSrv);
									}
								}
							}
						}

						break;
					}
				}
			}

			if (servicesToActivate.size() == ServicesToAdd.size()) {
				servicesToActivate = new ArrayList<bNewServices>();
			}

			if (ServicesToDel.size() > 1) {
				for (Iterator it = ServicesToDel.iterator(); it.hasNext();) {
					bNewServices Srv = new bNewServices();
					Srv = (bNewServices) it.next();
					Long snCode = Srv.getSNCODE();
					if (mzSncode.equals(snCode)) {
						servicesToDeactivate.add(Srv);
					}
				}
			}

			if (servicesToDeactivate.size() == ServicesToDel.size()) {
				servicesToDeactivate = new ArrayList<bNewServices>();
			}

			// >******* Updated, A.ESSA FC5197
			if (ServicesToDel.size() == 0 || ServicesToAdd.size() == 0) {// CMS
																			// treatement
				bNewServicesEx servicesEx = null;
				try {
					if (ServicesToAdd.size() != 0) {
						// >******* Updated, A.ESSA FC5937
						// List contrActivServ =
						// ServiceUtils.getContrActivServ(bcontract);
						// <******* Updated, A.ESSA FC5937

						for (Iterator it = ServicesToAdd.iterator(); it
								.hasNext();) {
							bNewServices Srv = new bNewServices();
							SrvEx = new MassServicesEx();
							servicesEx = new bNewServicesEx();
							Srv = (bNewServices) it.next();
							Long spCode = Srv.getSPCODE();
							Long snCode = Srv.getSNCODE();
							if (ServiceUtils.isAllowedToBeDeactivated(
									bcontract.getRPCODE(), snCode, soiComm)) {// A.ESSA:If
																				// allowed
																				// to
																				// be
																				// deactivated
																				// then
																				// allowed
																				// to
																				// be
																				// activated,
																				// so
																				// we
																				// use
																				// same
																				// method

								// >******* Updated, A.ESSA FC5937
								// if("IND4G".equals(ServiceUtils.getShdesFromSncode(snCode,soiComm))){
								// BcontractAll bAllContract = new
								// BcontractAll();
								// bAllContract.setCO_ID(bcontract.getCO_ID());
								// bAllContract.setCO_ID_PUB(bcontract.getCO_ID_PUB());
								// String value_des =
								// ServiceParamAdapter.GetValueDesServicefromParameters("ODDS3",bAllContract,soiComm,"NDMASTER");
								// if(!ServiceUtils.searchContractMaster4G(value_des,
								// soiComm)){
								// ArrayList params = new ArrayList();
								// params.add(value_des);
								// throw new
								// FaultWS(Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_MASTER_ERROR,
								// msgProperties, params);
								// }
								//
								// }
								// <******* Updated, A.ESSA FC5937

								// Extend All Input Param
								servicesEx = ServiceUtils
										.getServiceParamValueExtends(
												servicesEx, Srv);
								servicesEx.setCO_ID(co_id);
								servicesEx.setRPCODE(bcontract.getRPCODE());
								service.setSnCode(snCode);
								service.setSpCode(spCode);

								// >******* Update, A.ESSA DI6717
								HashMap<String, List<String>> eligSrvPrm = SoiUtils
										.getFilteredPrmValues(soiComm, co_id,
												snCode, msgProperties, null,
												null, ServicesToAdd);

								for (Iterator ItPrm = servicesEx
										.getBNewParamValueEx().iterator(); ItPrm
										.hasNext();) {
									bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm
											.next();

									for (int nVal = 0; nVal < pRmVal
											.getBNewValueEx().size(); nVal++) {
										bNewValuesEx vaLue = pRmVal
												.getBNewValueEx().get(nVal);
										if (vaLue.getVALUE_DES() != null) {
											if (!eligSrvPrm
													.get((new Long(pRmVal
															.getPRM_ID()))
															.toString())
													.contains(
															vaLue.getVALUE_DES())) {
												throw new FaultWS(
														Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
														msgProperties);
											}

										}
									}
								}

								// <******* Update, A.ESSA DI6717
								String srvStatus = ServiceUtils
										.getServiceStatus(service, contract,
												outParams);
								if (SoiUtils.SERVICE_STATUT_UNDEFINED
										.equals(srvStatus)) {
									// Add Service
									if (!servicesToActivate.contains(Srv)) {
										result = ServiceUtils
												.addServiceInContract(service,
														contract, false,
														soiComm, servicesEx,
														msgProperties,
														ServicesToAdd, null);
									}
									// else{
									// ServiceUtils.CheckServiceWithParameters(service,
									// contract, null,soiComm,
									// msgProperties,servicesEx,
									// SoiUtils.SERVICE_STATUT_UNDEFINED,
									// ServicesToAdd);
									// servicesToActivateFinal.add(servicesEx);
									// }
								} else if (SoiUtils.SERVICE_STATUT_DEACTIVE
										.equals(srvStatus)
										|| SoiUtils.SERVICE_STATUT_ON_HOLD
												.equals(srvStatus)) {
									// Activate Service
									if (!servicesToActivate.contains(Srv)) {
										result = ServiceUtils
												.activateServiceInContract(
														service,
														contract,
														false,
														soiComm,
														servicesEx,
														SoiUtils.SERVICE_STATUT_DEACTIVE,
														msgProperties,
														ServicesToAdd);
									}
									// else{
									// ServiceUtils.CheckServiceWithParameters(service,
									// contract, null,soiComm,
									// msgProperties,servicesEx,
									// SoiUtils.SERVICE_STATUT_UNDEFINED,
									// ServicesToAdd);
									// servicesToActivateFinal.add(servicesEx);
									// }

								} else if (SoiUtils.SERVICE_STATUT_ACTIVE
										.equals(srvStatus)) {
									// Service already activate
									SrvEx.setMsgError(Constants.ERROR_CODE.MT_WS_ACTIVATESVC_ACTIVE_SERVICE
											.concat(":")
											.concat(Message
													.getMessage(
															Constants.ERROR_CODE.MT_WS_ACTIVATESVC_ACTIVE_SERVICE,
															msgProperties)));
									// ServicesEx.add(SrvEx);
								}

							} else {
								throw new FaultWS(
										Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED,
										msgProperties);
							}
							if (result) {
								// soiComm.cmsClient.commit();
								// ServicesEx.add(SrvEx);
							}
						}

					} else {
						List<BContractedServices> listservices = new ArrayList<BContractedServices>();
						BDeactivatedSRV bDcontract = new BDeactivatedSRV();
						for (Iterator it = ServicesToDel.iterator(); it
								.hasNext();) {
							SrvEx = new MassServicesEx();
							result = false;
							inParams = new HashMap<String, Object>();
							bNewServices Srv = (bNewServices) it.next();
							Long snCode = Srv.getSNCODE();
							inParams.put("CO_ID", new Long(co_id).longValue());
							inParams.put("SNCODE", snCode);
							outParams = soiComm.executeCommand(
									SoiUtils.CMD_CONTRACT_SERVICES_READ,
									inParams);
							HashMap<String, Object>[] inDeactivateService = (HashMap<String, Object>[]) outParams
									.get("services");

							// SrvEx = getServiceParamValueExtends ( SrvEx,
							// Srv);
							// SrvEx.setCO_ID(co_id);
							// SrvEx.setRPCODE(bcontract.getRPCODE());

							// >******* Updated, A.ESSA FC5937
							// if(",IN4FA,INT4G,INP4G,".contains(",".concat(ServiceUtils.getShdesFromSncode(snCode,soiComm)).concat(","))){
							// HashMap slaves = ServiceUtils.getSlavesDNs(new
							// Long(co_id), "IND4G", soiComm);
							//
							// String dns = (String)slaves.get("DNS");
							// if(dns!=null && dns!=""){
							// ArrayList params = new ArrayList();
							// params.add(dns.substring(0,dns.length()-2));
							// throw new
							// FaultWS(Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_SLAVES_ERROR,
							// msgProperties, params);
							// }
							// }
							// <******* Updated, A.ESSA FC5937

							contract.setCoId(new Long(co_id).longValue());
							service.setSnCode(snCode);
							if (!servicesToDeactivate.contains(Srv)) {
								result = ServiceUtils.deactivate(service,
										contract, inDeactivateService[0],
										soiComm, msgProperties);
							}

							if (result) {
								inParams = new HashMap<String, Object>();
								inParams.put("CO_ID", co_id);
								inParams.put("SNCODE", snCode);
								outParams = soiComm.executeCommand(
										SoiUtils.CMD_CONTRACT_SERVICES_READ,
										inParams);
								inDeactivateService = (HashMap<String, Object>[]) outParams
										.get("services");
								listservices
										.add(ServiceUtils
												.buildDeactivateServicesBean(
														inDeactivateService,
														bDcontract));

								bDcontract.setDeactivateServices(listservices);

								// >******* Updated, A.ESSA FC5937
								if ("INT3G".equals(ServiceUtils
										.getShdesFromSncode(snCode, soiComm))) {
									HashMap slaves = ServiceUtils.getSlavesDNs(
											new Long(co_id), null, soiComm);

									coToDelFlag.add(co_id);
								
									// inParams = new HashMap<String, Object>();
									// inParams.put("CO_ID", co_id);
									// inParams.put("SNCODE", snCode);
									// outParams =
									// soiComm.executeCommand(SoiUtils.CMD_DELETEFLAG_UPDATE,inParams);

									ArrayList co_ids = (ArrayList) slaves
											.get("COS");
									if (co_ids != null) {
										for (int j = 0; j < co_ids.size(); j++) {
											ArrayList arrayfordesact = new ArrayList();
											ArrayList returnarraydeactivate = new ArrayList();
											arrayfordesact.add(ServiceUtils
													.getSncodeFromShdes(
															"ODDS3", soiComm));
											String xmldeactivateservices = null;
											xmldeactivateservices = CmsINTUtils
													.DeactiveServiceXMLWithDelFlag(
															arrayfordesact,
															(Long) co_ids
																	.get(j));
											if (xmldeactivateservices != null) {
												HashMap deactivateservice = new HashMap();
												deactivateservice.put(
														"ACTION_ID", 3);
												deactivateservice.put(
														"INPUT_DATA",
														xmldeactivateservices);
												deactivateservice.put("ORIGIN",
														"MASS_WEB_SERVICE");
												returnarraydeactivate
														.add(deactivateservice);
											}
											returnarray
													.addAll(returnarraydeactivate);
										}
									}
							
								}
								// <******* Updated, A.ESSA FC5937

							} else {
								SrvEx.setMsgError(Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED
										.concat(":")
										.concat(Message
												.getMessage(
														Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED,
														msgProperties)));
							}

							// ServicesEx.add(SrvEx);
						}
					}
				} catch (CMSException cex) {
					// >******* Updated, A.ESSA QC12126
					if (soiComm != null) {
						soiComm.cmsClient.rollback();
					}
					// <******* Updated, A.ESSA QC12126
					if (cex.getErrors() != null && cex.getErrors().length > 0) {
						logger.error(getClass().toString() + " : CMSException "
								+ cex.getErrors()[0].getAdditionalInfo());
						SrvEx.setMsgError(cex.getErrors()[0].getErrorCode()
								.concat(": ")
								.concat(cex.getErrors()[0].getAdditionalInfo()));
						// ServicesEx.add(SrvEx);
						// throw new
						// FaultWS(cex.getErrors()[0].getErrorCode(),cex.getErrors()[0].getAdditionalInfo());
					} else {
						if (cex.getMessage().contains(
								"CMS client not connected")) {
							if (soiComm == null) {
								// >******* Updated, A.ESSA QC12126
								// soiComm.cmsClient.rollback();
								// <******* Updated, A.ESSA QC12126
								throw new FaultWS(
										Constants.ERROR_CODE.MT_USERUNLOGGED,
										msgProperties);
							}
						} else {
							logger.error(getClass().toString()
									+ " : CMSException " + cex.getMessage());

							SrvEx.setMsgError("RC1000: ".concat(cex
									.getMessage()));
							// ServicesEx.add(SrvEx);
							// throw new FaultWS("RC1000", cex.getMessage());
						}
					}
				} catch (Exception e) {
					soiComm.cmsClient.rollback();
					if (e instanceof FaultWS)
						throw (FaultWS) e;
					logger.error(getClass().toString() + " : Exception "
							+ e.getMessage());
					e.printStackTrace();
					throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
							msgProperties);
				} finally {
					result = false;
				}
				result = true;
				soiComm.cmsClient.commit();
				Long snDelFlag = Long.parseLong(ServiceUtils
						.getSncodeFromShdes("INT3G", soiComm));
			
					for (int y = 0; y < coToDelFlag.size(); y++) {
						inParams = new HashMap<String, Object>();
						inParams.put("CO_ID", coToDelFlag.get(y));
						inParams.put("SNCODE", snDelFlag);
						outParams = soiComm.executeCommand(
								SoiUtils.CMD_DELETEFLAG_UPDATE, inParams);
					}
			

				snDelFlag = Long.parseLong(ServiceUtils.getSncodeFromShdes(
						"WVG", soiComm));

			
					for (int y = 0; y < coToDelFlag.size(); y++) {
						inParams = new HashMap<String, Object>();
						inParams.put("CO_ID", coToDelFlag.get(y));
						inParams.put("SNCODE", snDelFlag);
						outParams = soiComm.executeCommand(
								SoiUtils.CMD_DELETEFLAG_UPDATE, inParams);
					}
			

				ArrayList<String> services_to_deactivateMZ = new ArrayList<String>();
				String xmldeactivateservices = null;
				ArrayList returnarraydeactivate = new ArrayList();
				for (Iterator it = ServicesToDel.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					if (servicesToDeactivate.contains(Srv)) {
						Long snCode = Srv.getSNCODE();
						services_to_deactivateMZ.add(snCode.toString());
					}

				}

				xmldeactivateservices = CmsINTUtils.DeactiveServiceXML(
						services_to_deactivateMZ, null, bcontract.getCO_ID(),
						null);
				if (xmldeactivateservices != null) {
					HashMap deactivateservice = new HashMap();
					deactivateservice.put("ACTION_ID", 3);
					deactivateservice.put("INPUT_DATA", xmldeactivateservices);
					deactivateservice.put("ORIGIN", "MASS_WEB_SERVICE");
					returnarraydeactivate.add(deactivateservice);
				}

				String xmlactivateservices = null;
				ArrayList returnarrayactivate = new ArrayList();
				ArrayList additional_servicesMZ = new ArrayList();

				for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					if (servicesToActivate.contains(Srv)) {
						HashMap serviceaactive = new HashMap();
						serviceaactive.put("SNCODE", Srv.getSNCODE());
						serviceaactive.put("SPCODE", Srv.getSPCODE());

						servicesEx = new bNewServicesEx();
						servicesEx = ServiceUtils.getServiceParamValueExtends(
								servicesEx, Srv);
						servicesEx.setCO_ID(co_id);
						servicesEx.setRPCODE(bcontract.getRPCODE());

						// >******* Update, A.ESSA DI6717
						Long snCode = Srv.getSNCODE();
						HashMap<String, List<String>> eligSrvPrm = SoiUtils
								.getFilteredPrmValues(soiComm, co_id, snCode,
										msgProperties, null, null,
										ServicesToAdd);

						for (Iterator ItPrm = servicesEx.getBNewParamValueEx()
								.iterator(); ItPrm.hasNext();) {
							bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm
									.next();

							for (int nVal = 0; nVal < pRmVal.getBNewValueEx()
									.size(); nVal++) {
								bNewValuesEx vaLue = pRmVal.getBNewValueEx()
										.get(nVal);
								if (vaLue.getVALUE_DES() != null) {
									if (!eligSrvPrm.get(
											(new Long(pRmVal.getPRM_ID()))
													.toString()).contains(
											vaLue.getVALUE_DES())) {
										throw new FaultWS(
												Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
												msgProperties);
									}

								}
							}
						}

						service.setSnCode(Srv.getSNCODE());
						service.setSpCode(Srv.getSPCODE());
						String srvStatus = ServiceUtils.getServiceStatus(
								service, contract, outParams);

						ArrayList array_parameters = CmsINTUtils
								.GetServiceParametersDefaultFirstValue(service,
										contract, false, soiComm, servicesEx,
										msgProperties, ServicesToAdd);

						if (array_parameters != null) {
							ArrayList parameterlistaux = new ArrayList();
							for (int kk = 0; kk < array_parameters.size(); kk++) {
								HashMap aux = (HashMap) array_parameters
										.get(kk);
								HashMap paramv = new HashMap();
								paramv.put("PRM_ID", aux.get("PRM_ID"));
								paramv.put("PRM_NO", aux.get("PRM_NO"));
								paramv.put("VALUE_DES", aux.get("VALUE_DES"));
								paramv.put("VALUE", aux.get("VALUE"));
								paramv.put("VALUE_SEQNO",
										aux.get("VALUE_SEQNO"));
								parameterlistaux.add(paramv);
							}
							if (parameterlistaux.size() > 0) {
								serviceaactive.put("ListofParamValues",
										parameterlistaux);
							}
						}

						additional_servicesMZ.add(serviceaactive);
					}
				}

				xmlactivateservices = CmsINTUtils
						.ActivateServicesXML(bcontract.getCO_ID(),
								additional_servicesMZ, null, null);
				if (xmlactivateservices != null) {
					HashMap activateservice = new HashMap();
					activateservice.put("ACTION_ID", 4);
					activateservice.put("INPUT_DATA", xmlactivateservices);
					activateservice.put("ORIGIN", "MASS_WEB_SERVICE");
					returnarrayactivate.add(activateservice);
				}

				if (List1.getOp().toUpperCase().equals("ACTIVATE")) {
					returnarray.addAll(returnarrayactivate);
					returnarray.addAll(returnarraydeactivate);
				} else {
					returnarray.addAll(returnarraydeactivate);
					returnarray.addAll(returnarrayactivate);
				}

			} else { // CMSINT
				ArrayList<String> services_to_deactivate = new ArrayList<String>();
				ArrayList<String> services_to_deactivateMZ = new ArrayList<String>();
				String xmldeactivateservices = null;
				ArrayList returnarraydeactivate = new ArrayList();
				for (Iterator it = ServicesToDel.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					if (!servicesToDeactivate.contains(Srv)) {
						Long snCode = Srv.getSNCODE();
						services_to_deactivate.add(snCode.toString());
					} else {
						Long snCode = Srv.getSNCODE();
						services_to_deactivateMZ.add(snCode.toString());
					}

				}

				xmldeactivateservices = CmsINTUtils.DeactiveServiceXML(
						services_to_deactivate, null, bcontract.getCO_ID(),
						null);
				if (xmldeactivateservices != null) {
					HashMap deactivateservice = new HashMap();
					deactivateservice.put("ACTION_ID", 3);
					deactivateservice.put("INPUT_DATA", xmldeactivateservices);
					deactivateservice.put("ORIGIN", "MASS_WEB_SERVICE");
					returnarraydeactivate.add(deactivateservice);
				}

				xmldeactivateservices = CmsINTUtils.DeactiveServiceXML(
						services_to_deactivateMZ, null, bcontract.getCO_ID(),
						null);
				if (xmldeactivateservices != null) {
					HashMap deactivateservice = new HashMap();
					deactivateservice.put("ACTION_ID", 3);
					deactivateservice.put("INPUT_DATA", xmldeactivateservices);
					deactivateservice.put("ORIGIN", "MASS_WEB_SERVICE");
					returnarraydeactivate.add(deactivateservice);
				}

				ArrayList<String> services_to_activate = new ArrayList<String>();
				String xmlactivateservices = null;
				ArrayList returnarrayactivate = new ArrayList();
				ArrayList additional_services = new ArrayList();
				ArrayList additional_servicesMZ = new ArrayList();
				for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					if (!servicesToActivate.contains(Srv)) {
						HashMap serviceaactive = new HashMap();
						serviceaactive.put("SNCODE", Srv.getSNCODE());
						serviceaactive.put("SPCODE", Srv.getSPCODE());

						// >******* Updated, A.ESSA FC5937
						// if("IND4G".equals(ServiceUtils.getShdesFromSncode(Srv.getSNCODE(),soiComm))){
						// BcontractAll bAllContract = new BcontractAll();
						// bAllContract.setCO_ID(bcontract.getCO_ID());
						// bAllContract.setCO_ID_PUB(bcontract.getCO_ID_PUB());
						// String value_des =
						// ServiceParamAdapter.GetValueDesServicefromParameters("ODDS3",bAllContract,soiComm,"NDMASTER");
						// if(!ServiceUtils.searchContractMaster4G(value_des,
						// soiComm)){
						// ArrayList params = new ArrayList();
						// params.add(value_des);
						// throw new
						// FaultWS(Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_MASTER_ERROR,
						// msgProperties, params);
						// }
						//
						// }
						// <******* Updated, A.ESSA FC5937

						bNewServicesEx servicesEx = new bNewServicesEx();
						servicesEx = ServiceUtils.getServiceParamValueExtends(
								servicesEx, Srv);
						servicesEx.setCO_ID(co_id);
						servicesEx.setRPCODE(bcontract.getRPCODE());

						// >******* Update, A.ESSA DI6717
						Long snCode = Srv.getSNCODE();
						HashMap<String, List<String>> eligSrvPrm = SoiUtils
								.getFilteredPrmValues(soiComm, co_id, snCode,
										msgProperties, null, null,
										ServicesToAdd);

						for (Iterator ItPrm = servicesEx.getBNewParamValueEx()
								.iterator(); ItPrm.hasNext();) {
							bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm
									.next();

							for (int nVal = 0; nVal < pRmVal.getBNewValueEx()
									.size(); nVal++) {
								bNewValuesEx vaLue = pRmVal.getBNewValueEx()
										.get(nVal);
								if (vaLue.getVALUE_DES() != null) {
									if (!eligSrvPrm.get(
											(new Long(pRmVal.getPRM_ID()))
													.toString()).contains(
											vaLue.getVALUE_DES())) {
										throw new FaultWS(
												Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
												msgProperties);
									}

								}
							}
						}
						// <******* Update, A.ESSA DI6717

						service.setSnCode(Srv.getSNCODE());
						service.setSpCode(Srv.getSPCODE());
						String srvStatus = ServiceUtils.getServiceStatus(
								service, contract, outParams);

						ArrayList array_parameters = CmsINTUtils
								.GetServiceParametersDefaultFirstValue(service,
										contract, false, soiComm, servicesEx,
										msgProperties, ServicesToAdd);

						if (array_parameters != null) {
							ArrayList parameterlistaux = new ArrayList();
							for (int kk = 0; kk < array_parameters.size(); kk++) {
								HashMap aux = (HashMap) array_parameters
										.get(kk);
								HashMap paramv = new HashMap();
								paramv.put("PRM_ID", aux.get("PRM_ID"));
								paramv.put("PRM_NO", aux.get("PRM_NO"));
								paramv.put("VALUE_DES", aux.get("VALUE_DES"));
								paramv.put("VALUE", aux.get("VALUE"));
								paramv.put("VALUE_SEQNO",
										aux.get("VALUE_SEQNO"));
								parameterlistaux.add(paramv);
							}
							if (parameterlistaux.size() > 0) {
								serviceaactive.put("ListofParamValues",
										parameterlistaux);
							}
						}

						additional_services.add(serviceaactive);
					}
				}

				for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
					bNewServices Srv = (bNewServices) it.next();
					if (servicesToActivate.contains(Srv)) {
						HashMap serviceaactive = new HashMap();
						serviceaactive.put("SNCODE", Srv.getSNCODE());
						serviceaactive.put("SPCODE", Srv.getSPCODE());

						// >******* Updated, A.ESSA FC5937
						// if("IND4G".equals(ServiceUtils.getShdesFromSncode(Srv.getSNCODE(),soiComm))){
						// BcontractAll bAllContract = new BcontractAll();
						// bAllContract.setCO_ID(bcontract.getCO_ID());
						// bAllContract.setCO_ID_PUB(bcontract.getCO_ID_PUB());
						// String value_des =
						// ServiceParamAdapter.GetValueDesServicefromParameters("ODDS3",bAllContract,soiComm,"NDMASTER");
						// if(!ServiceUtils.searchContractMaster4G(value_des,
						// soiComm)){
						// ArrayList params = new ArrayList();
						// params.add(value_des);
						// throw new
						// FaultWS(Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_MASTER_ERROR,
						// msgProperties, params);
						// }
						//
						// }
						// <******* Updated, A.ESSA FC5937

						bNewServicesEx servicesEx = new bNewServicesEx();
						servicesEx = ServiceUtils.getServiceParamValueExtends(
								servicesEx, Srv);
						servicesEx.setCO_ID(co_id);
						servicesEx.setRPCODE(bcontract.getRPCODE());

						// >******* Update, A.ESSA DI6717
						Long snCode = Srv.getSNCODE();
						HashMap<String, List<String>> eligSrvPrm = SoiUtils
								.getFilteredPrmValues(soiComm, co_id, snCode,
										msgProperties, null, null,
										ServicesToAdd);

						for (Iterator ItPrm = servicesEx.getBNewParamValueEx()
								.iterator(); ItPrm.hasNext();) {
							bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm
									.next();

							for (int nVal = 0; nVal < pRmVal.getBNewValueEx()
									.size(); nVal++) {
								bNewValuesEx vaLue = pRmVal.getBNewValueEx()
										.get(nVal);
								if (vaLue.getVALUE_DES() != null) {
									if (!eligSrvPrm.get(
											(new Long(pRmVal.getPRM_ID()))
													.toString()).contains(
											vaLue.getVALUE_DES())) {
										throw new FaultWS(
												Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
												msgProperties);
									}

								}
							}
						}
						// <******* Update, A.ESSA DI6717

						service.setSnCode(Srv.getSNCODE());
						service.setSpCode(Srv.getSPCODE());
						String srvStatus = ServiceUtils.getServiceStatus(
								service, contract, outParams);

						ArrayList array_parameters = CmsINTUtils
								.GetServiceParametersDefaultFirstValue(service,
										contract, false, soiComm, servicesEx,
										msgProperties, ServicesToAdd);

						if (array_parameters != null) {
							ArrayList parameterlistaux = new ArrayList();
							for (int kk = 0; kk < array_parameters.size(); kk++) {
								HashMap aux = (HashMap) array_parameters
										.get(kk);
								HashMap paramv = new HashMap();
								paramv.put("PRM_ID", aux.get("PRM_ID"));
								paramv.put("PRM_NO", aux.get("PRM_NO"));
								paramv.put("VALUE_DES", aux.get("VALUE_DES"));
								paramv.put("VALUE", aux.get("VALUE"));
								paramv.put("VALUE_SEQNO",
										aux.get("VALUE_SEQNO"));
								parameterlistaux.add(paramv);
							}
							if (parameterlistaux.size() > 0) {
								serviceaactive.put("ListofParamValues",
										parameterlistaux);
							}
						}

						additional_servicesMZ.add(serviceaactive);
					}
				}

				xmlactivateservices = CmsINTUtils.ActivateServicesXML(
						bcontract.getCO_ID(), additional_services, null, null);
				if (xmlactivateservices != null) {
					HashMap activateservice = new HashMap();
					activateservice.put("ACTION_ID", 4);
					activateservice.put("INPUT_DATA", xmlactivateservices);
					activateservice.put("ORIGIN", "MASS_WEB_SERVICE");
					returnarrayactivate.add(activateservice);
				}

				xmlactivateservices = CmsINTUtils
						.ActivateServicesXML(bcontract.getCO_ID(),
								additional_servicesMZ, null, null);
				if (xmlactivateservices != null) {
					HashMap activateservice = new HashMap();
					activateservice.put("ACTION_ID", 4);
					activateservice.put("INPUT_DATA", xmlactivateservices);
					activateservice.put("ORIGIN", "MASS_WEB_SERVICE");
					returnarrayactivate.add(activateservice);
				}

				if (List1.getOp().toUpperCase().equals("ACTIVATE")) {
					returnarray.addAll(returnarrayactivate);
					returnarray.addAll(returnarraydeactivate);
				} else {
					returnarray.addAll(returnarraydeactivate);
					returnarray.addAll(returnarrayactivate);
				}
			}

			// >******* Updated, A.ESSA FC5197
			boolean found3G = Boolean.FALSE;
			boolean found4G = Boolean.FALSE;
			for (Iterator it = ServicesToDel.iterator(); it.hasNext();) {
				bNewServices Srv = (bNewServices) it.next();
				Long snCode = Srv.getSNCODE();
				if ("INT3G".equals(ServiceUtils.getShdesFromSncode(snCode,
						soiComm))) {
					found3G = true;
				}
				// if(",IN4FA,INT4G,INP4G,".contains(",".concat(ServiceUtils.getShdesFromSncode(snCode,soiComm)).concat(","))){
				// found4G = true;
				// }
			}
			if (found3G) {
				HashMap slaves = ServiceUtils.getSlavesDNs(new Long(co_id),
						null, soiComm);
				ArrayList co_ids = (ArrayList) slaves.get("COS");
				if (co_ids != null) {
					for (int j = 0; j < co_ids.size(); j++) {
						ArrayList arrayfordesact = new ArrayList();
						ArrayList returnarraydeactivate = new ArrayList();
						arrayfordesact.add(ServiceUtils.getSncodeFromShdes(
								"ODDS3", soiComm));
						String xmldeactivateservices = null;
						xmldeactivateservices = CmsINTUtils
								.DeactiveServiceXMLWithDelFlag(arrayfordesact,
										(Long) co_ids.get(j));
						if (xmldeactivateservices != null) {
							HashMap deactivateservice = new HashMap();
							deactivateservice.put("ACTION_ID", 3);
							deactivateservice.put("INPUT_DATA",
									xmldeactivateservices);
							deactivateservice.put("ORIGIN", "MASS_WEB_SERVICE");
							returnarraydeactivate.add(deactivateservice);
						}
						returnarray.addAll(returnarraydeactivate);
					}
				}
			}
			// >******* Updated, A.ESSA FC5937
			// else
			// if(found4G){
			// HashMap slaves = ServiceUtils.getSlavesDNs(new Long(co_id),
			// "IND4G", soiComm);
			//
			// String dns = (String)slaves.get("DNS");
			// if(dns!=null && dns!=""){
			// ArrayList params = new ArrayList();
			// params.add(dns.substring(0,dns.length()-2));
			// throw new
			// FaultWS(Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_SLAVES_ERROR,
			// msgProperties, params);
			// }
			// }
			// <******* Updated, A.ESSA FC5937
			if (returnarray.size() > 0) {
				CmsINTUtils.addDeleteServices(bcontract, returnarray, username,
						soiComm);
			}
			// <******* Updated, A.ESSA FC5197

		} catch (CMSException cex) {
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException "
						+ cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(),
						cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
							msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException "
							+ cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());
			e.printStackTrace();
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR,
					msgProperties);
		}

		if (SrvEx.getMsgError() == null)
			SrvEx.setMsgSuccess("Opération effectuée avec succès");
		return SrvEx;
	}

	public List getContrActivSuspendedServ(Bcontract bcontract)
			throws CMSException, FaultWS {

		ArrayList<BContractedServices> servs = (ArrayList) bcontract
				.getContractedServices();
		List contrActivServ = new ArrayList();
		for (int i = 0; i < servs.size(); i++) {
			BContractedServices serv = servs.get(i);
			if (serv.getCOS_STATUS().toString()
					.equals(SoiUtils.SERVICE_STATUT_ACTIVE)
					|| serv.getCOS_STATUS().toString()
							.equals(SoiUtils.SERVICE_STATUT_SUSPENDED)) {
				contrActivServ.add(Long.valueOf(serv.getSNCODE()).toString());
			}

		}
		return contrActivServ;
	}

	// >******* Updated, A.ESSA FC5937
	// >******* Updated, A.ESSA FC7045
	public void check4GServices(List<bNewServices> ServicesToAdd,
			List<bNewServices> ServicesToDel, Bcontract bcontract,
			SOIService soiComm, List contrActivServ) throws CMSException,
			FaultWS {

		boolean serv4GExists = false;
		boolean servNetExists = false;
		Long serv4GSpcode = null;
		List serv4GList = Arrays.asList("INT4G", "INP4G", "IND4G", "IN4FA");
		HashMap<String, Object> inParams = new HashMap<String, Object>();
		for (Iterator it = ServicesToAdd.iterator(); it.hasNext();) {
			bNewServices Srv = (bNewServices) it.next();
			Long snCode = Srv.getSNCODE();

			inParams.put("SNCODE", snCode);
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_SERVICES_READ, inParams);
			if (!outParams.isEmpty()) {
				HashMap<String, Object>[] sv_list = (HashMap<String, Object>[]) outParams
						.get("NUM_SV");
				for (int i = 0; i < sv_list.length; i++) {
					HashMap<String, Object> hashMap = sv_list[i];
					String shdes = (String) sv_list[i].get("SNCODE_PUB");
					if (serv4GList.contains(shdes)) {
						serv4GExists = true;
						serv4GSpcode = Srv.getSPCODE();
					} else {
						if (sv_list[i] != null
								&& sv_list[i].get("SV_NET_PROV_REQ") != null
								&& (Boolean) sv_list[i].get("SV_NET_PROV_REQ")) {
							servNetExists = true;
						}
					}
				}
			}
		}

		if (servNetExists && serv4GExists) {
			throw new FaultWS(
					Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_NETWORK_ERROR,
					msgProperties);
		}

		serv4GExists = false;
		servNetExists = false;

		for (Iterator it = ServicesToDel.iterator(); it.hasNext();) {
			bNewServices Srv = (bNewServices) it.next();
			Long snCode = Srv.getSNCODE();

			inParams.put("SNCODE", snCode);
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_SERVICES_READ, inParams);
			if (!outParams.isEmpty()) {
				HashMap<String, Object>[] sv_list = (HashMap<String, Object>[]) outParams
						.get("NUM_SV");
				for (int i = 0; i < sv_list.length; i++) {
					HashMap<String, Object> hashMap = sv_list[i];
					String shdes = (String) sv_list[i].get("SNCODE_PUB");
					if (serv4GList.contains(shdes)) {
						serv4GExists = true;
						serv4GSpcode = Srv.getSPCODE();
					}
					// else
					// if(shdes.equals("SDM4G")){
					// servSDMExists = true;
					// }
					else {
						if (sv_list[i] != null
								&& sv_list[i].get("SV_NET_PROV_REQ") != null
								&& (Boolean) sv_list[i].get("SV_NET_PROV_REQ")) {
							servNetExists = true;
						}
					}
				}
			}
		}

		if (servNetExists && serv4GExists) {
			throw new FaultWS(
					Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_D4G_NETWORK_ERROR,
					msgProperties);
		}

	}

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(
					SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage()
							.contains(
									"CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception "
					+ e.getMessage());

			return rb;
		}

	}
}
