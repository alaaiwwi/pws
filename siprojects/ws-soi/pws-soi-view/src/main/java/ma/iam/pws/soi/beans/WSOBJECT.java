/**
 * 
 */
package ma.iam.pws.soi.beans;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pws.soi.beans.errors.BError;

/**
 * @author mabchour
 *
 */
public interface WSOBJECT {
	
	public final List<BError> errors = new ArrayList<BError>() ;

}
