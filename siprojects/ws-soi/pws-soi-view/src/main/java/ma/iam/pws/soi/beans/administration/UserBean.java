package ma.iam.pws.soi.beans.administration;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import ma.iam.pws.soi.beans.WSOBJECT;

@XmlAccessorType(XmlAccessType.NONE)
//@XmlRootElement
//@XmlType(name="UserBean" , propOrder = { "costcenter_id", "description" })
public class UserBean {
	
	/** The Constant ALIAS. */
	public static final String ALIAS = "User";
	@XmlElement
	private long costcenter_id ; 
	private String  description;
	private String group; 
	@XmlElement
	private boolean is_batch ;
	@XmlElement
	private long lng_code;
	private String lng_shdes;
	@XmlElement
	private String name;
	@XmlElement
	private String password;
	private List<String> rights;
	
	public List<String> getRights() {
		return rights;
	}
	public void setRights(List<String> rights) {
		this.rights = rights;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getCostcenter_id() {
		return costcenter_id;
	}
	public void setCostcenter_id(long costcenter_id) {
		this.costcenter_id = costcenter_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public boolean isIs_batch() {
		return is_batch;
	}
	public void setIs_batch(boolean is_batch) {
		this.is_batch = is_batch;
	}
	public long getLng_code() {
		return lng_code;
	}
	public void setLng_code(long lng_code) {
		this.lng_code = lng_code;
	}
	public String getLng_shdes() {
		return lng_shdes;
	}
	public void setLng_shdes(String lng_shdes) {
		this.lng_shdes = lng_shdes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getPassword_expires() {
		return password_expires;
	}
	public void setPassword_expires(Date password_expires) {
		this.password_expires = password_expires;
	}
	public long getPwd_remind_days() {
		return pwd_remind_days;
	}
	public void setPwd_remind_days(long pwd_remind_days) {
		this.pwd_remind_days = pwd_remind_days;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	@XmlElement
	private Date password_expires;
	@XmlElement
	private long pwd_remind_days;
	private String user_type ;

}
