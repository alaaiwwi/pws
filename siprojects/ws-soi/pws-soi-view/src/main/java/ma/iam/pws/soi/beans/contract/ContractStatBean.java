/**
 * 
 */
package ma.iam.pws.soi.beans.contract;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

/**
 * @author mounir_mabchour
 *
 */
@XmlType(name="Contract" , propOrder = { "CO_ID", "CO_ID_PUB" , "CO_STATUS", "CO_PENDING_STATUS", "CO_STATUS_DATE" ,"CO_PENDING_DATE"})
public class ContractStatBean {
	
	private long CO_ID;
	private String CO_ID_PUB ;
	private long CO_STATUS;
	private Date CO_STATUS_DATE;
	private long CO_PENDING_STATUS;
	private Date CO_PENDING_DATE;
	public long getCO_ID() {
		return CO_ID;
	}
	public void setCO_ID(long cO_ID) {
		CO_ID = cO_ID;
	}
	public String getCO_ID_PUB() {
		return CO_ID_PUB;
	}
	public void setCO_ID_PUB(String cO_ID_PUB) {
		CO_ID_PUB = cO_ID_PUB;
	}
	public long getCO_STATUS() {
		return CO_STATUS;
	}
	public void setCO_STATUS(long cO_STATUS) {
		CO_STATUS = cO_STATUS;
	}
	public Date getCO_STATUS_DATE() {
		return CO_STATUS_DATE;
	}
	public void setCO_STATUS_DATE(Date cO_STATUS_DATE) {
		CO_STATUS_DATE = cO_STATUS_DATE;
	}
	public long getCO_PENDING_STATUS() {
		return CO_PENDING_STATUS;
	}
	public void setCO_PENDING_STATUS(long cO_PENDING_STATUS) {
		CO_PENDING_STATUS = cO_PENDING_STATUS;
	}
	public Date getCO_PENDING_DATE() {
		return CO_PENDING_DATE;
	}
	public void setCO_PENDING_DATE(Date cO_PENDING_DATE) {
		CO_PENDING_DATE = cO_PENDING_DATE;
	}
	

}
