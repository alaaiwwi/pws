package ma.iam.pws.soi.service;

import ma.iam.pws.soi.exception.TechnicalException;
//import ma.iam.ws.webservice.security.impl.Constantes;
//import ma.iam.geve.common.logger.GEVELogger;
//import ma.iam.geve.common.logger.LogManager;


import org.springframework.context.ApplicationContext;

public final class WSContextManager {
	
	/** Instance du LOGGER */
	//private static final GEVELogger LOGGER = LogManager.getInstance().getLogger(Constantes.TECHNICAL_LOGGER_NAME);

	private static WSContextManager instance;

	private ApplicationContext context;

	/**
	 * Constructeur du manager de contexte.
	 * 
	 * @param applicationContext
	 */
	private WSContextManager(ApplicationContext applicationContext) {
		context = applicationContext;
	}

	/**
	 * Récupération de l'instance unique du manager.
	 * 
	 * @param springFileClassPath
	 *            Le chemin relatif du fichier spring par rapport à la racine
	 *            de l'application.
	 */
	public static WSContextManager getInstance() {
		if (instance == null) {
			throw new IllegalStateException("Context non initialisé !");
		}
		return instance;
	}
	
	/**
	 * Cette méthode permet d'initialiser le context Spring.</br></br>
	 * @param applicationContext
	 * @throws TechnicalException
	 */
	public static synchronized void init(ApplicationContext applicationContext) 
			throws ma.iam.pws.soi.exception.TechnicalException {
		if (instance == null) {
			instance = new WSContextManager(applicationContext);
		} else { 
			//LOGGER.fatal(GEContextManager.class,"init", "Erreur d'initialisation du context : context déja initialisé!");
			//throw new ma.iam.ws.exception.TechnicalException("Erreur d'initialisation du context : context déja initialisé!");
		}
 
	}

	/**
	 * Permet de récuperer un bean Spring
	 * @param beanName
	 * @return Object
	 */
	public Object getBean(String beanName) {
		return this.context.getBean(beanName);
	}
}
