package ma.iam.pws.soi.webservice.services;



import javax.jws.WebResult;

import ma.iam.pws.soi.beans.errors.FaultWS;




/**
 * @author A.ESSA
 *
 */

@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public interface RecharcheInternetService {

	/**.
	 * getEligibility return code eligibility for msisdn in entry.
	 * @param msisdn the msisdn
	 * @return the string
	 * @throws FaultWS
	 */
	@javax.jws.WebMethod
	@WebResult(name="ELIGIBILITY")
	long getEligibility(  @javax.jws.WebParam(name="USERNAME")String username, @javax.jws.WebParam(name="MSISDN") String msisdn) throws FaultWS;
	
	/**.
	 * rechargeInternet add recharge for msisdn in entry.
	 * @param msisdn the msisdn
	 * @return the string
	 * @throws FaultWS
	 */
	@javax.jws.WebMethod
	@WebResult(name="Code_RETOUR")
	long rechargeInternet(  @javax.jws.WebParam(name="USERNAME")String username, @javax.jws.WebParam(name="MSISDN") String msisdn, @javax.jws.WebParam(name="DATE_RECH") String dateRech ) throws FaultWS;
}