package ma.iam.pws.soi.beans.services;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlRootElement(name = "ParamValueBeanPub")
@XmlType(name="ParamValueBeanPub" , propOrder = { "prm_id","prm_id_pub","prm_des","prm_no","type","min_values","max_values","mandatory_value","data_type", "def_value","VALUES"})
public class ParamValueBeanPub {

	private String data_type;
	private String def_value;
	private List<ValueBeanPub> VALUES;

	public String getData_type() {
		return data_type;
	}

	@Override
	public String toString() {
		return "ParamValueBeanPub [data_type=" + data_type + ", def_value="
				+ def_value + ", N_VALUES=" + VALUES + ", mandatory_value="
				+ mandatory_value + ", max_values=" + max_values
				+ ", min_values=" + min_values + ", prm_des=" + prm_des
				+ ", prm_id=" + prm_id + ", prm_id_pub=" + prm_id_pub
				+ ", prm_no=" + prm_no + ", type=" + type + "]";
	}

	public void setData_type(String data_type) {
		this.data_type = data_type;
	}

	public String getDef_value() {
		return def_value;
	}

	public void setDef_value(String def_value) {
		this.def_value = def_value;
	}

	public boolean isMandatory_value() {
		return mandatory_value;
	}

	public void setMandatory_value(boolean mandatory_value) {
		this.mandatory_value = mandatory_value;
	}

	public int getMax_values() {
		return max_values;
	}

	public void setMax_values(int max_values) {
		this.max_values = max_values;
	}

	public int getMin_values() {
		return min_values;
	}

	public void setMin_values(int min_values) {
		this.min_values = min_values;
	}

	public String getPrm_des() {
		return prm_des;
	}

	public void setPrm_des(String prm_des) {
		this.prm_des = prm_des;
	}

	public long getPrm_id() {
		return prm_id;
	}

	public void setPrm_id(long prm_id) {
		this.prm_id = prm_id;
	}

	public String getPrm_id_pub() {
		return prm_id_pub;
	}

	public void setPrm_id_pub(String prm_id_pub) {
		this.prm_id_pub = prm_id_pub;
	}

	public Long getPrm_no() {
		return prm_no;
	}

	public void setPrm_no(Long prm_no) {
		this.prm_no = prm_no;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the n_VALUES
	 */
	public List<ValueBeanPub> getVALUES() {
		return VALUES;
	}

	/**
	 * @param n_VALUES
	 *            the n_VALUES to set
	 */
	public void setVALUES(List<ValueBeanPub> VALUES) {
		this.VALUES = VALUES;
	}

	private boolean mandatory_value;
	private int max_values;
	private int min_values;
	private String prm_des;
	private long prm_id;
	private String prm_id_pub;
	private Long prm_no;
	private String type;
}
