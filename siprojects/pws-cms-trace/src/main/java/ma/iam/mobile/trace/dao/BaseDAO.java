package ma.iam.mobile.trace.dao;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public interface BaseDAO {
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate();
}
