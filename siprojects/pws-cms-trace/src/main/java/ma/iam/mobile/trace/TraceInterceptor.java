package ma.iam.mobile.trace;

public interface TraceInterceptor {
    void performTrace(Object respObject, String operation, String userName,String ticket,String userNameId) throws TraceException;
	
}
