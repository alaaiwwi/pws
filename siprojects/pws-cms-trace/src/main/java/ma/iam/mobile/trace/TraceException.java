package ma.iam.mobile.trace;

public class TraceException extends Exception {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -822078598295945146L;
	
	public TraceException(String message) {
		super(message);
	}

}
