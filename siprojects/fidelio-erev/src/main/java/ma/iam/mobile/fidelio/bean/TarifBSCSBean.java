package ma.iam.mobile.fidelio.bean;

import java.util.Date;

public class TarifBSCSBean {

	/** The id. */

	/** The pv id. */
	private long pvId;

	/** The vs code. */
	private long vsCode;

	private String description;

	/** The plan tarifaire. */
	private String planTarifaire;

	/** The prix. */
	private Float prix;

	/** The date. */
	private Date date;

	/**
	 * Gets the date.
	 * 
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Gets the plan tarifaire.
	 * 
	 * @return the plan tarifaire
	 */
	public String getPlanTarifaire() {
		return planTarifaire;
	}

	/**
	 * Sets the plan tarifaire.
	 * 
	 * @param planTarifaire
	 *            the new plan tarifaire
	 */
	public void setPlanTarifaire(String planTarifaire) {
		this.planTarifaire = planTarifaire;
	}

	/**
	 * Gets the prix.
	 * 
	 * @return the prix
	 */
	public Float getPrix() {
		return prix;
	}

	/**
	 * Sets the prix.
	 * 
	 * @param prix
	 *            the new prix
	 */
	public void setPrix(Float prix) {
		this.prix = prix;
	}

	public long getPvId() {
		return pvId;
	}

	public void setPvId(long pvId) {
		this.pvId = pvId;
	}

	public long getVsCode() {
		return vsCode;
	}

	public void setVsCode(long vsCode) {
		this.vsCode = vsCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
