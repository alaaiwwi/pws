package ma.iam.mobile.fidelio.bean;

import java.util.Date;

public class TarifPackBSCSBean {

	/** The prix. */
	private Double prix;

	/** The ref mode engagement. */
	private Integer idModeEngagement;

	/** The date debut. */
	private Date dateDebut;

	/** The en service. */
	private String enService;

	/**
	 * Gets the prix.
	 * 
	 * @return the prix
	 */
	public Double getPrix() {
		return prix;
	}

	/**
	 * Sets the prix.
	 * 
	 * @param prix
	 *            the new prix
	 */
	public void setPrix(Double prix) {
		this.prix = prix;
	}

	/**
	 * @return
	 */
	public Integer getIdModeEngagement() {
		return idModeEngagement;
	}

	/**
	 * @param idModeEngagement
	 */
	public void setIdModeEngagement(Integer idModeEngagement) {
		this.idModeEngagement = idModeEngagement;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public String getEnService() {
		return enService;
	}

	public void setEnService(String enService) {
		this.enService = enService;
	}

}
