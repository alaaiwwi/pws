package ma.iam.mobile.fidelio.util;

import ma.iam.mobile.fidelio.bean.OpenInvoicesResponse;
import ma.iam.mobile.fidelio.persist.InvoicesPersist;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class HelperUtil {

	public static final OpenInvoicesResponse Invoices2OpenInvoicesResponse(
			InvoicesPersist bean) {
		
		OpenInvoicesResponse persist = null;
		if (bean != null) {
			persist = new OpenInvoicesResponse();
			persist.setAmountRemaining(bean.getAmount());
			persist.setInvoiceReference(bean.getInvoiceRef());
			persist.setMonth(bean.getDateFact());
			persist.setType(bean.getType());
			persist.setHash(HelperUtil.generateHash(bean));
		}
		return persist;
	}

	public static final String generateHash(InvoicesPersist bean) {
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		String hash = bean.getOhxact() + "|" + bean.getInvoiceRef() + "|";
		return encoder.encodePassword(hash, null);
	}
	public static final String padR(String str, int size, char padChar)
	{
	  StringBuffer padded = new StringBuffer(str);
	  while (padded.length() < size)
	  {
	    padded.append(padChar);
	  }
	  return padded.toString();
	}
}
