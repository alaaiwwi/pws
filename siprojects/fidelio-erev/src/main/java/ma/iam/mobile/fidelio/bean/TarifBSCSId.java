package ma.iam.mobile.fidelio.bean;

import java.io.Serializable;

/**
 * The Class TarifBSCSId.
 * @version 1.0.0
 * @author Atos Origin
 */
public class TarifBSCSId implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -12547885693215478L;

	/** The pv id. */
	private long pvId;

	/** The vs code. */
	private long vsCode;
	
	private String description;

	/**
	 * Gets the pv id.
	 * @return the pv id
	 */
	public long getPvId() {
		return pvId;
	}

	/**
	 * Sets the pv id.
	 * @param pvId the new pv id
	 */
	public void setPvId(long pvId) {
		this.pvId = pvId;
	}

	/**
	 * Gets the vs code.
	 * @return the vs code
	 */
	public long getVsCode() {
		return vsCode;
	}

	/**
	 * Sets the vs code.
	 * @param vsCode the new vs code
	 */
	public void setVsCode(long vsCode) {
		this.vsCode = vsCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
