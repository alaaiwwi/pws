package ma.iam.mobile.fidelio.dao;

public interface ByPassFidelioDao extends BaseDAOFidelio {
	public boolean modeByPass();
}
