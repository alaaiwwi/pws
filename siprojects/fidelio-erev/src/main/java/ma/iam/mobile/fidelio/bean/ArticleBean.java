package ma.iam.mobile.fidelio.bean;

public class ArticleBean {

	/** Id Article */
	private Integer articleId;

	/** Code Article */
	private String code;

	/** Libelle Article */
	private String libelle;

	/** Etat De Supenseion Dans Fidelio */
	private String enService;

	/** Libelle Article */
	private String libelleModelePoste;

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getEnService() {
		return enService;
	}

	public void setEnService(String enService) {
		this.enService = enService;
	}

	public String getLibelleModelePoste() {
		return libelleModelePoste;
	}

	public void setLibelleModelePoste(String libelleModelePoste) {
		this.libelleModelePoste = libelleModelePoste;
	}

}