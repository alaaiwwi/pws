package ma.iam.mobile.fidelio.interceptor;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public interface BaseDAO {
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate();
}
