/*
 * @author AtoS
 */
package ma.iam.mobile.fidelio.constants;

/**
 * The Enum ExceptionCodeTypeEnum.
 */
public enum ExceptionCodeTypeEnum {

	SYSTEM_ERROR_WS("ERR_PI_00_000", "Erreur systéme au niveau du webservice"),

	PROBLEM_INVALIDE_NUM_APPEL("ERR_PI_LOI_001", "Numéro d'appel invalide"),

	PROBLEM_TYPE_IDENTIFIANT("ERR_PI_LOI_002",
			"Le type d’identifiant est invalide.il doit une valeur entre 1 et 7"),

	PROBLEM_IDENTIFIANT_CUSTOMER(
			"ERR_PI_LOI_003",
			"Identifiant client non valide. L'identifiant client ne doit pas comporter des espaces ou des caractères spéciaux"),

	PROBLEM_ND_NOT_FOUND("ERR_PI_LOI_004", "Numéro d'appel inexistant"),

	PROBLEM_ND_NOT_ACTIF("ERR_PI_LOI_005", "Numéro d'appel est inactif"), PROBLEM_IDENTIFIANT_CUSTOMER_NOT_CORRECT(
			"ERR_PI_LOI_006",
			"Le numéro d'appel ne correspond pas à  l’identifiant fourni"),
	/** The PROBLE m_ criteri a_ ws. */
	PROBLEM_FID_FIXE("ERR_PI_LOI_007",
			" Identifiant de paiement FIXE est invalide"),
			
			PROBLEM_FID_FIXE_NOT_SAME("ERR_PI_LOI_008",
					" Le numéro d'appel ne correspond pas au code de paiement FIXE"),
					
			PROBLEM_FID_INTERNET(
			"ERR_PI_LOI_009", " Identifiant de paiement INTERNET est invalide"), 
			
			PROBLEM_INACTIF_CLIENT(
					"ERR_PI_LOI_010", " Client inactif "),
			
			PROBLEM_CRITERIA_CD_WS(
			"ERR_PI_LOI_011", "Probléme dans les critères de recherche"), PROBLEM_TYPE_IDENTIFIANT_NOT_CORRECT(
			"ERR_PI_LOI_012",
			"Le numéro d'appel ne correspond pas au type d’identifiant fourni"),
	PROBLEM_TICKET("CF0007", "Problème de ticket"),
	PROBLEM_PRG_CODE_NOT_CORRECT("ERR_PI_LOI_013",
			"la catégorie du client est non autorisé"), PROBLEM_INVOICES_NOT_FOUND(
			"ERR_PI_LOI_014", "Pas de factures ouvertes trouvées"),

			
			
			/*************************************/////
			
			

	POI_EMPTY_REQUEST("ERR_PI_POI_001", "L’objet de la recherche est vide"),
	POI_INVALIDE_INVOICE("ERR_PI_POI_002", "Une des références factures n'est pas valide "),
	POI_INVALIDE_INVOICE_HASH("ERR_PI_POI_003", "Une des références factures ne correspond pas au hash"),
	POI_INVALIDE_INVOICE_OPEN("ERR_PI_POI_004", "Pas de facture ouverte trouvée correspondant à une des référence fournie"),
	POI_INVALIDE_AMOUNT_INVOICE("ERR_PI_POI_005", "Le montant total du paiement ne correspond pas au montant total ouvert des factures à fermer."),
	POI_INVALIDE_TRANSACTION("ERR_PI_POI_006", "L’identifiant de transaction POWERCARD est vide ou dépasse 60 caractères"),
	POI_INVALIDE_AMOUNT("ERR_PI_POI_007", "le montant total doit être numérique"),
	POI_INVALIDE_TRANSACTION_DATE("ERR_PI_POI_008", "le champ date de transaction doit respecter le format dd/MM/yyyy HHmm:ss"),
	POI_INVALIDE_PARTNER("ERR_PI_POI_009", "le champ identifiant du partenaire n'est pas renseigner ou inexistant"),
	POI_INVALIDE_TRANSACTION_TYPE("ERR_PI_POI_010", "le type de transaction est vide ou sa taille dépasse un caractère"),
	POI_INVALIDE_INVOICES_LIST("ERR_PI_POI_011", "la liste des factures est vide"),
	POI_INVALIDE_HASH("ERR_PI_POI_012", "un des codes hash n'est pas valide"),
	POI_ERROR_CMD("ERR_FM_REE_01", "Erreur est survenue lors de la passation de la commande, veuillez contacter l'administrateur"),
	POI_INVALIDE_IMEI("ERR_FM_REE_01", "La valeur saisie pour le champ IMEI est incorrecte. "),
	POI_EXIST_IMEI("ERR_FM_REE_02", "La valeur saisie pour le champ IMEI est incorrecte. "),

	POI_CONTRACT_NON_ECHU("ERR_FM_REE_03", "Ce numéro d'appel n'a pas d'engagement échu")
	
	
	;

	/** The error code. */
	private final String errorCode;

	/** The error context. */
	private final String errorContext;

	/**
	 * Instantiates a new exception code type enum.
	 * 
	 * @param errorCode
	 *            the error code
	 * @param errorContext
	 *            the error context
	 */
	ExceptionCodeTypeEnum(final String errorCode, final String errorContext) {
		this.errorCode = errorCode;
		this.errorContext = errorContext;
	}

	/**
	 * Gets the error code.
	 * 
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error name.
	 * 
	 * @return the error name
	 */
	public String getErrorContext() {
		return errorContext;
	}

}
