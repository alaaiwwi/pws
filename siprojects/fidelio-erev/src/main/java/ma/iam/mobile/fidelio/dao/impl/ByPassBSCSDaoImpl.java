/*
 * @author AtoS
 */

package ma.iam.mobile.fidelio.dao.impl;

import ma.iam.mobile.fidelio.dao.ByPassBSCSDao;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ByPassBSCSDaoImpl extends BaseDAOMobileImpl implements ByPassBSCSDao {

	@Autowired
	private JDBCPing pingMobileBscs;

	public boolean modeByPass() {
		return pingMobileBscs.check();
	}

}
