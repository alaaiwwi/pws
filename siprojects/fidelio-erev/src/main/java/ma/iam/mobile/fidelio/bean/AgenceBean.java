package ma.iam.mobile.fidelio.bean;


public class AgenceBean {


	/** The id. */
	private String id;

	/** The libelle. */
	private String libelle;

	/** The code. */
	private String costCenter;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	

}
