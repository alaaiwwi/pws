/*
 * @author AtoS
 */

package ma.iam.mobile.fidelio.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.mobile.fidelio.bean.ArticleBean;
import ma.iam.mobile.fidelio.bean.EngagementContratBean;
import ma.iam.mobile.fidelio.bean.TarifBSCSBean;
import ma.iam.mobile.fidelio.constants.Constants;
import ma.iam.mobile.fidelio.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.fidelio.dao.FidelioDao;
import ma.iam.mobile.fidelio.dao.MobileBSCSDao;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.persist.ContractCustomer;
import ma.iam.mobile.fidelio.persist.ContractPersist;
import ma.iam.mobile.fidelio.persist.InvoicesPersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

@Repository
public class MobileBSCSDaoImpl extends BaseDAOMobileImpl implements MobileBSCSDao {
	private static final Logger LOG = LoggerFactory.getLogger(MobileBSCSDao.class);
	private static final String GET_CONTRACT_BY_DN_NUM = MobileBSCSDaoImpl.class.getName() + ".GET_CONTRACT_BY_DN_NUM";

	private static final String GET_CUSTOMER_WITH_SAME_IDENT = MobileBSCSDaoImpl.class.getName()
			+ ".GET_CUSTOMER_WITH_SAME_IDENT";

	private static final String LIST_INVOICES = MobileBSCSDaoImpl.class.getName() + ".LIST_INVOICES";

	private static final String GET_IDENT_BY_CUSTOMER_ID = MobileBSCSDaoImpl.class.getName()
			+ ".GET_IDENT_BY_CUSTOMER_ID";
	private static final String GET_CODE_FID = MobileBSCSDaoImpl.class.getName() + ".GET_CODE_FID";

	private static final String GET_CUSTOMER_CONTRACT = MobileBSCSDaoImpl.class.getName() + ".GET_CUSTOMER_CONTRACT";

	private static final String GET_PARTNER_DES = MobileBSCSDaoImpl.class.getName() + ".GET_PARTNER_DES";

	private static final String LIST_INVOICES_REF_INVOICE = MobileBSCSDaoImpl.class.getName()
			+ ".LIST_INVOICES_REF_INVOICE";

	private static final String GET_TARIF_BSCS = MobileBSCSDaoImpl.class.getName() + ".GET_TARIF_BSCS";

	private static final String GET_PRIX_PACK_X_MOIS_GPPM = MobileBSCSDaoImpl.class.getName()
			+ ".GET_PRIX_PACK_X_MOIS_GPPM";

	private static final String LISTE_NUMAPPEL_BY_CLIENT = MobileBSCSDaoImpl.class.getName()
			+ ".LISTE_NUMAPPEL_BY_CLIENT";

	private static final String GET_CUSTCODE_BY_ID = MobileBSCSDaoImpl.class.getName() + ".GET_CUSTCODE_BY_ID";
	
	private static final String GET_ENGAGEMENT_ENCOURS = MobileBSCSDaoImpl.class.getName()
			+ ".GET_ENGAGEMENT_ENCOURS";

	@Autowired
	FidelioDao fidelioDao;

	public ContractPersist getContractByDnNum(String nd) {

		LOG.debug("--> getContractByDnNum nd {}", nd);
		final List<ContractPersist> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CONTRACT_BY_DN_NUM);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("nd", nd);

			final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
					ContractPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getContractByDnNum");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getContractByDnNum is  null for nd = " + nd);
			return null;
		}
		return (list != null && !list.isEmpty()) ? list.get(0) : null;

	}

	public ContractPersist getIdentifiantByCustomerId(String customerId) {

		LOG.debug("--> getIdentifiantByCustomerId customerId {}", customerId);
		final List<ContractPersist> list;
		try {

			final String sql = CustomSQLUtil.get(GET_IDENT_BY_CUSTOMER_ID);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("customerId", customerId);

			final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
					ContractPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- customerId");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getIdentifiantByCustomerId is  null for nd = " + customerId);
			return null;
		}
		return (list != null && !list.isEmpty()) ? list.get(0) : null;

	}

	public List<ContractPersist> getCustomersWithSameIdent(ContractPersist contract) {

		LOG.debug("--> getCustomersWithSameIdent contract {}", contract);
		final List<ContractPersist> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CUSTOMER_WITH_SAME_IDENT);

			@SuppressWarnings("rawtypes")
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerParent", contract.getIdCustomerParent());
			namedParameters.put("ident", String.valueOf(contract.getCustomerIdentifiant()));

			final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
					ContractPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getContractByDnNum");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCustomersWithSameIdent is  null for contract = " + contract);
			return null;
		}
		return list;

	}

	public List<InvoicesPersist> getInvoices(List<Long> customerIds) {

		LOG.debug("--> getInvoices customerIds {}", customerIds);
		final List<InvoicesPersist> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_INVOICES);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("listCust", customerIds);

			final CustomBeanPropertyRowMapper<InvoicesPersist> custRowMapper = new CustomBeanPropertyRowMapper<InvoicesPersist>(
					InvoicesPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getInvoices");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getInvoices is  null for customerIds = " + customerIds);
			return null;
		}
		return list;

	}

	public String getCodeFid(String customerId) {

		LOG.debug("--> getCodeFid");

		try {

			final String sql = CustomSQLUtil.get(GET_CODE_FID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			String result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
			if ((result != null) && (!"".equals(result))) {
				return result;
			}
			return null;
		} catch (EmptyResultDataAccessException e) {

		}
		return null;

	}

	public List<ContractCustomer> getCustomersWithContract(String customerId) {

		LOG.debug("--> getCustomersWithContract customerId {}", customerId);
		final List<ContractCustomer> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CUSTOMER_CONTRACT);

			@SuppressWarnings("rawtypes")
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			final CustomBeanPropertyRowMapper<ContractCustomer> custRowMapper = new CustomBeanPropertyRowMapper<ContractCustomer>(
					ContractCustomer.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getCustomersWithContract");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCustomersWithContract is  null for customerId = " + customerId);
			return null;
		}
		return list;

	}

	public String getPartner(String partnerId) {

		LOG.debug("--> getPartner");

		try {

			final String sql = CustomSQLUtil.get(GET_PARTNER_DES);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("partnerId", partnerId);

			String result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
			if ((result != null) && (!"".equals(result))) {
				return result;
			}
			return null;
		} catch (EmptyResultDataAccessException e) {

		}
		return null;

	}

	public List<InvoicesPersist> getInvoicesByRefInvoice(List<String> invoices) {

		LOG.debug("--> getInvoicesByRefInvoice invoices {}", invoices);
		final List<InvoicesPersist> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_INVOICES_REF_INVOICE);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("listRef", invoices);

			final CustomBeanPropertyRowMapper<InvoicesPersist> custRowMapper = new CustomBeanPropertyRowMapper<InvoicesPersist>(
					InvoicesPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getInvoicesByRefInvoice");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getInvoices is  null for invoices = " + invoices);
			return null;
		}
		return list;

	}

	public Map<String, String> createPayment(String transactionId, Date transactionDate, String transactionType,
			String shdesPartner, String transactionRemarq, String ohxAct, String type) {

		return null;
	}

	public TarifBSCSBean getPrixPackXMoisSema(String planTarifaireId, String codeArticle, String nbreMois) {

		LOG.debug("--> getPrixPackXMoisSema");
		final List<TarifBSCSBean> list;
		String paramModeEngagementBSCS = "";

		if (null != nbreMois) {
			// Cas d'une recuperation d'articles pour un engagement de 12 Mois
			if (nbreMois.equals("12")) {
				paramModeEngagementBSCS = Constants.PREFIX_MODE_ENGAGEMENT_12_MOIS_SEMA;
			}
			// Cas d'une recuperation d'articles pour un engagement de 24 Mois
			if (nbreMois.equals("24")) {
				paramModeEngagementBSCS = Constants.PREFIX_MODE_ENGAGEMENT_24_MOIS_SEMA;
			}

		}
		try {
			final String sql = CustomSQLUtil.get(GET_PRIX_PACK_X_MOIS_GPPM);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("paramModeEngagementBSCS", paramModeEngagementBSCS);
			namedParameters.put("codeArticle", codeArticle);
			namedParameters.put("planTarifaireId", planTarifaireId);

			final CustomBeanPropertyRowMapper<TarifBSCSBean> custRowMapper = new CustomBeanPropertyRowMapper<TarifBSCSBean>(
					TarifBSCSBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getPrixPackXMoisSema");
		} catch (

		EmptyResultDataAccessException e) {

			LOG.error("getPrixPackXMoisSema is  null ");
			return null;
		}
		return list.get(0);
	}

	private TarifBSCSBean getPrixPackXMois(String planTarifaireId, String codeArticle, Integer nbreMois) {

		LOG.debug("--> getPrixPackXMois");
		final List<TarifBSCSBean> list;
		String paramModeEngagementBSCS = "";
		if (null != nbreMois) {

			// Cas d'une recuperation d'articles pour un engagement de 12 Mois
			if (nbreMois.intValue() == 12) {
				paramModeEngagementBSCS = Constants.PREFIX_MODE_ENGAGEMENT_12_MOIS;
			}

			// Cas d'une recuperation d'articles pour un engagement de 24 Mois
			if (nbreMois.intValue() == 24) {
				paramModeEngagementBSCS = Constants.PREFIX_MODE_ENGAGEMENT_24_MOIS;
			}
		}
		try {

			final String sql = CustomSQLUtil.get(GET_TARIF_BSCS);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("paramModeEngagementBSCS", paramModeEngagementBSCS);
			namedParameters.put("codeArticle", codeArticle);
			namedParameters.put("planTarifaireId", planTarifaireId);

			final CustomBeanPropertyRowMapper<TarifBSCSBean> custRowMapper = new CustomBeanPropertyRowMapper<TarifBSCSBean>(
					TarifBSCSBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getPrixPackXMois");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getPrixPackXMois is  null ");
			return null;
		}
		return list.get(0);

	}

	public Float getRemisePackPrivilege(String codeClient, String numAppel, Float prixPack, Float seuilPrix,
			Float remisePrivilege) {

		CallableStatement callableStatement = null;

		Map<String, String> map = new HashMap<String, String>();

		final String OUI = "O";
		final String NON = "N";

		String entreprise;
		String privilege;
		Float result = 0f;

		try {

			Connection connection = super.getDataSource().getConnection();
			callableStatement = connection
					.prepareCall("{ ? = call INTERFACE.SEMA_INT_BSCS_FIDELIO.RECUP_E_P(?,?,?,?,?) }");
			callableStatement.registerOutParameter(1, Types.DOUBLE);
			callableStatement.setString(2, codeClient);
			callableStatement.setString(3, numAppel);
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.registerOutParameter(6, Types.NUMERIC);

			callableStatement.executeQuery();
			entreprise = callableStatement.getString(4);
			privilege = callableStatement.getString(5);
			callableStatement.clearParameters();

			if (NON.equals(entreprise) && OUI.equals(privilege)) {

				if (prixPack > seuilPrix) {
					result = remisePrivilege;
				} else {
					result = prixPack;
				}

			}

			Float resultReturn = result > 0 ? result : 0f;

			return resultReturn;

		} catch (Exception e) {
			LOG.error("error in getRemisePackPrivilege : " + e.getMessage());

		} finally {

			if (callableStatement != null) {
				try {
					callableStatement.close();
				} catch (SQLException e) {
					LOG.error("error in getRemisePackPrivilege : " + e.getMessage());
					callableStatement = null;

				}
			}
		}
		return result;
	}

	public List<String> getAllNumAppels(String customerId) {

		LOG.debug("--> getAllNumAppels");
		final List<String> list;

		try {
			final String sql = CustomSQLUtil.get(LISTE_NUMAPPEL_BY_CLIENT);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			final CustomBeanPropertyRowMapper<String> custRowMapper = new CustomBeanPropertyRowMapper<String>(
					String.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getAllNumAppels");
		} catch (

		EmptyResultDataAccessException e) {

			LOG.error("getAllNumAppels is  null ");
			return null;
		}
		return list;
	}

	public String getCustCodeById(String customerId) {

		LOG.debug("--> getCustCodeById");
		final List<String> list;

		try {
			final String sql = CustomSQLUtil.get(GET_CUSTCODE_BY_ID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			final CustomBeanPropertyRowMapper<String> custRowMapper = new CustomBeanPropertyRowMapper<String>(
					String.class);
			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters, String.class);
			LOG.debug("<-- getCustCodeById");
		} catch (

		EmptyResultDataAccessException e) {

			LOG.error("getCustCodeById is  null ");
			return null;
		}
		return list.get(0);
	}

	public int affecterLivraison(String codeClient, Long coId, String numAppel, String codePoste, Integer engagementId,
			Date dateAffectation, String user, String imei, Long agenceId, Integer commandId, boolean affectComplement,
			Double montantComplement, String occComplementCode, boolean affectFraisReengagemnt,
			Double montantFraisReengagemnt, String occFraisReengagemntCode, boolean affectFraisLivraison) throws FunctionnalException {

		String debugString = "";
		debugString = "\n codeClient : " + codeClient + "\n numAppel : " + numAppel + "\n codePoste : " + codePoste
				+ "\n engagementId : " + engagementId + "\n dateAffectation : " + dateAffectation + "\n user : " + user
				+ "\n emei : " + imei + "\n agenceId : " + agenceId + "\n commandId : " + commandId
				+ "\n affectComplement : " + affectComplement + "\n montantComplement : " + montantComplement
				+ "\n occComplementCode: " + occComplementCode + "\n affectFraisReengagemnt	: " + affectFraisReengagemnt
				+ "\n montantFraisReengagemnt : " + montantFraisReengagemnt + "\n occFraisReengagemntCode : "
				+ occFraisReengagemntCode + "\n affectFraisLivraison	: " + affectFraisLivraison;

		try {
			Connection connection = super.getDataSource().getConnection();
			connection.setAutoCommit(false);

			if (affectComplement) {

				debugString = "Affectation des complement d'argent  ";
				LOG.debug(debugString);
				CallableStatement queryComplement = null;
				Integer result = 0;
				try {
					queryComplement = connection
							.prepareCall("{ ? =  call SEMA_INT_FIDELIO_BSCS.AFFECT_OCC(?,?,?,?,?,?) }");
					queryComplement.registerOutParameter(1, Types.INTEGER);
					queryComplement.setDouble(2, commandId.doubleValue());
					queryComplement.setString(3, codeClient);
					queryComplement.setString(4, numAppel);
					queryComplement.setDouble(5, montantComplement);// o.k
					queryComplement.setString(6, occComplementCode);
					queryComplement.setString(7, user);

					queryComplement.executeQuery();
					result = queryComplement.getInt(1);
					queryComplement.close();

					if (result != 0) {
						connection.rollback();
						LOG.error(
								"La procédure SEMA_INT_FIDELIO_BSCS.AFFECT_OCC est executée avec erreurs  code erreur  "
										+ result);
						removeIMEI(coId, imei);

					}
				} catch (SQLException e) {
					LOG.error("Impossible d'executer la procédure stockée SEMA_INT_FIDELIO_BSCS.AFFECT_OCC  "
							+ e.getMessage());
					removeIMEI(coId, imei);
					throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
				} finally {

					if (queryComplement != null) {
						try {
							queryComplement.close();
						} catch (SQLException e) {
							queryComplement = null;
						}
					}
				}
			}

			if (affectFraisReengagemnt) {

				debugString = "Affectation frais de réengagement  ";
				LOG.debug(debugString);
				CallableStatement queryFraisEngagament = null;
				Integer result = 0;
				try {
					queryFraisEngagament = connection
							.prepareCall("{ ? =  call SEMA_INT_FIDELIO_BSCS.AFFECT_OCC(?,?,?,?,?,?) }");
					queryFraisEngagament.registerOutParameter(1, Types.INTEGER);
					queryFraisEngagament.setDouble(2, commandId.doubleValue());
					queryFraisEngagament.setString(3, codeClient);
					queryFraisEngagament.setString(4, numAppel);
					queryFraisEngagament.setDouble(5, montantFraisReengagemnt);
					queryFraisEngagament.setString(6, occFraisReengagemntCode);
					queryFraisEngagament.setString(7, user);

					queryFraisEngagament.executeQuery();
					result = queryFraisEngagament.getInt(1);
					queryFraisEngagament.close();

					if (result != 0) {
						connection.rollback();
						LOG.error(
								"La procédure SEMA_INT_FIDELIO_BSCS.AFFECT_OCC est executée avec erreurs  code erreur  "
										+ result);
						removeIMEI(coId, imei);
						throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
					}
				} catch (SQLException e) {
					LOG.error("Impossible d'executer la procédure stockée SEMA_INT_FIDELIO_BSCS.AFFECT_OCC  "
							+ e.getMessage());
					removeIMEI(coId, imei);
					throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
				} finally {

					if (queryFraisEngagament != null) {
						try {
							queryFraisEngagament.close();
						} catch (SQLException e) {
							queryFraisEngagament = null;
						}
					}
				}
			}

			CallableStatement queryEngagement = null;
			Integer resultEngagement;
			try {
				queryEngagement = connection
						.prepareCall("{ ? =  call SEMA_INT_FIDELIO_BSCS.AFFECT_ENGAG(?,?,?,?,?,?,?,?,?) }");
				queryEngagement.registerOutParameter(1, Types.INTEGER);
				queryEngagement.setDouble(2, commandId.doubleValue());
				queryEngagement.setString(3, codeClient);
				queryEngagement.setString(4, numAppel);
				queryEngagement.setString(5, codePoste);
				queryEngagement.setInt(6, engagementId);
				queryEngagement.setDate(7, new java.sql.Date(dateAffectation.getTime()));
				queryEngagement.setString(8, user);
				queryEngagement.setString(9, imei);
				queryEngagement.setLong(10, agenceId);
				queryEngagement.executeQuery();
				resultEngagement = queryEngagement.getInt(1);
				queryEngagement.close();
				if (resultEngagement != 0) {

					LOG.error("La procédure SEMA_INT_FIDELIO_BSCS.AFFECT_ENGAG est execuée avec erreurs code erreur : "
							+ resultEngagement);
					connection.rollback();
					removeIMEI(coId, imei);
					throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);

				}

			} catch (SQLException e) {

				LOG.error("Impossible d'executer la procédure stockée SEMA_INT_FIDELIO_BSCS.AFFECT_ENGAG  : "
						+ e.getMessage());
				removeIMEI(coId, imei);

			} finally {
				if (queryEngagement != null) {
					try {
						queryEngagement.close();
					} catch (SQLException e) {
						queryEngagement = null;
					}
				}
			}
		} catch (Exception e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);

		}

		return 0;

	}

	/**
	 * Ajouter IMEI dans la table d'unicité
	 * 
	 * @param coId
	 *            the co id
	 * @param codePoste
	 *            the code poste
	 * @param engagementId
	 *            the engagement id
	 * @param user
	 *            the user
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public String addIMEI(Long coId, String pND, String pArticleDes, String codePoste, String engagement, String user,
			String pOldIMEI, String pAgence, String pNomClient, String pNewIMEI) throws FunctionnalException {

		String debugString = "";
		debugString += "\n coId :" + (coId);
		debugString += "\n ND :" + (pND);
		debugString += "\n codePoste :" + (codePoste);
		debugString += "\n Description poste :" + (pArticleDes);
		debugString += "\n Nom client :" + (pNomClient);
		debugString += "\n Agence:" + (pAgence);
		debugString += "\n engagementId :" + (engagement);
		debugString += "\n user :" + (user);
		debugString += "\n ancien IMEI : " + (pOldIMEI);
		debugString += "\n nouveau IMEI : " + (pNewIMEI);

		LOG.info(debugString);

		String result;
		Connection connection = null;
		CallableStatement query = null;
		try {

			connection = super.getDataSource().getConnection();

			query = connection.prepareCall("{ ? =  call  INTERFACE.SEMA_IMEI.insertIMEI(?,?,?,?,?,?,?,?,?,?) }");

			query.registerOutParameter(1, Types.VARCHAR);
			query.setLong(2, coId);
			query.setString(3, pND);
			query.setString(4, pNewIMEI);
			query.setString(5, codePoste);
			query.setString(6, pArticleDes);
			query.setString(7, "FIDEREVWS");
			query.setString(8, pNomClient);
			query.setString(9, pAgence);
			query.setString(10, engagement);
			query.setString(11, pOldIMEI);

			query.executeQuery();
			result = query.getString(1);

			query.clearParameters();

			query.close();
			LOG.info(result);

			return result;

		} catch (SQLException e) {
			try {

				connection.rollback();
				throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			} catch (SQLException e1) {
				LOG.debug(
						"Impossible d'executer la procédure stockée INTERFACE.SEMA_IMEI.insertIMEI " + e1.getMessage());

			}
			LOG.debug("Impossible d'executer la procédure stockée INTERFACE.SEMA_IMEI.insertIMEI " + e.getMessage());

		} finally {
			if (query != null) {
				try {
					query.close();
				} catch (SQLException e) {
					query = null;
				}
			}
		}
		return null;
	}

	/**
	 * Supprimer IMEI dans la table d'unicité
	 * 
	 * @param coId
	 *            the co id
	 * @param codePoste
	 *            the code poste
	 * @param engagementId
	 *            the engagement id
	 * @param user
	 *            the user
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 *             the technical exception
	 */
	public int removeIMEI(Long pCoId, String pIMEI) throws FunctionnalException {

		String debugString = "";
		debugString += "\n coId :" + (pCoId);
		debugString += "\n IMEI : " + (pIMEI);

		LOG.info(debugString);

		Connection connection = null;

		CallableStatement query = null;
		String result;

		try {
			connection = super.getDataSource().getConnection();

			query = connection.prepareCall("{ ? =  call  INTERFACE.SEMA_IMEI.deleteIMEI(?,?,?) }");

			query.registerOutParameter(1, Types.VARCHAR);
			query.setString(2, pIMEI);
			query.setLong(3, pCoId);
			query.setString(4, "BOL");

			query.executeQuery();
			result = query.getString(1);
			query.clearParameters();
			query.close();

			if (result != "OK") {
				return -1;
			}

		} catch (SQLException e) {
			try {
				connection.rollback();
				throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			} catch (SQLException e1) {

				LOG.debug(
						"Impossible d'executer la procédure stockée INTERFACE.SEMA_IMEI.deleteIMEI " + e1.getMessage());

			}
			LOG.debug("Impossible d'executer la procédure stockée INTERFACE.SEMA_IMEI.deleteIMEI " + e.getMessage());

		} finally {
			if (query != null) {
				try {
					query.close();
				} catch (SQLException e) {
					query = null;
				}
			}
		}
		return 0;
	}

	
	public EngagementContratBean getEngagementEnCours(Long coId) {

		LOG.debug("--> getEngagementEnCours");
		final List<EngagementContratBean> list;
	
		try {

			final String sql = CustomSQLUtil.get(GET_ENGAGEMENT_ENCOURS);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("coId", coId.toString());

			final CustomBeanPropertyRowMapper<EngagementContratBean> custRowMapper = new CustomBeanPropertyRowMapper<EngagementContratBean>(
					EngagementContratBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getEngagementEnCours");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getEngagementEnCours is  null ");
			return null;
		}
		return list.get(0);

	}

}
