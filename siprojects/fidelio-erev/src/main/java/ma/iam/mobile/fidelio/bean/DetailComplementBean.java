package ma.iam.mobile.fidelio.bean;

public class DetailComplementBean {

	/** Nombre De Point Acquis. */
	private Long pointsAcquis;

	/** Nombre De Point Encours. */
	private Long pointsEncours;

	/** Le complement d'argent. */
	private Double complement;

	private String duree;

	private Long prixPtEffectif;
	private Integer bonus;
	private Long prixEnPoints;

	public Long getPointsAcquis() {
		return pointsAcquis;
	}

	public void setPointsAcquis(Long pointsAcquis) {
		this.pointsAcquis = pointsAcquis;
	}

	public Long getPointsEncours() {
		return pointsEncours;
	}

	public void setPointsEncours(Long pointsEncours) {
		this.pointsEncours = pointsEncours;
	}

	public Double getComplement() {
		return complement;
	}

	public void setComplement(Double complement) {
		this.complement = complement;
	}

	public String getDuree() {
		return duree;
	}

	public void setDuree(String duree) {
		this.duree = duree;
	}

	public Long getPrixPtEffectif() {
		return prixPtEffectif;
	}

	public void setPrixPtEffectif(Long prixPtEffectif) {
		this.prixPtEffectif = prixPtEffectif;
	}

	public Integer getBonus() {
		return bonus;
	}

	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	public Long getPrixEnPoints() {
		return prixEnPoints;
	}

	public void setPrixEnPoints(Long prixEnPoints) {
		this.prixEnPoints = prixEnPoints;
	}

}