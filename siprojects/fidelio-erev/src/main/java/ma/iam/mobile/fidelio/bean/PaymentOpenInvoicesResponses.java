package ma.iam.mobile.fidelio.bean;

import java.util.ArrayList;
import java.util.List;

public class PaymentOpenInvoicesResponses {

	private String idTransaction;
	private List<PaymentResponse> payments = new ArrayList<PaymentResponse>();

	public String getIdTransaction() {
		return idTransaction;
	}

	public void setIdTransaction(String idTransaction) {
		this.idTransaction = idTransaction;
	}

	public List<PaymentResponse> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentResponse> payments) {
		this.payments = payments;
	}

}
