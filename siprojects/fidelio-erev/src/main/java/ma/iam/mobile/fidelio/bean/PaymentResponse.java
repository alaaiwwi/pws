package ma.iam.mobile.fidelio.bean;

import java.util.ArrayList;
import java.util.List;

public class PaymentResponse {

	private String paymentTransaction;
	private List<String> invoiceReference = new ArrayList<String>();
	private Double paymentAmount;

	public String getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(String paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}

	public List<String> getInvoiceReference() {
		return invoiceReference;
	}

	public void setInvoiceReference(List<String> invoiceReference) {
		this.invoiceReference = invoiceReference;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

}
