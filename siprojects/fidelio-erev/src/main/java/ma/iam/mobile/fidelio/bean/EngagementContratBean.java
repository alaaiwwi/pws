package ma.iam.mobile.fidelio.bean;

public class EngagementContratBean {

	private String engagementId;

	private String dateDebut;

	public String getEngagementId() {
		return engagementId;
	}

	public void setEngagementId(String engagementId) {
		this.engagementId = engagementId;
	}

	public String getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}

	public EngagementContratBean() {
		super();
	}

}
