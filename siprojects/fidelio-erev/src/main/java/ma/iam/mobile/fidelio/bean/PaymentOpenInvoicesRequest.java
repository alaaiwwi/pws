package ma.iam.mobile.fidelio.bean;

import java.util.List;

public class PaymentOpenInvoicesRequest {
	private String transactionId;
	private String amount;
	private List<InvoiceRequest> invoices;
	private String transactionDate;
	private String partnerShdes;
	private String transactionInfo;
	private String transactionType;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public List<InvoiceRequest> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<InvoiceRequest> invoices) {
		this.invoices = invoices;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	

	public String getPartnerShdes() {
		return partnerShdes;
	}

	public void setPartnerShdes(String partnerShdes) {
		this.partnerShdes = partnerShdes;
	}

	public String getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(String transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

}
