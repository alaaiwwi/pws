package ma.iam.mobile.fidelio.business.impl;

import ma.iam.mobile.fidelio.business.ByPassFidelioManagerBiz;
import ma.iam.mobile.fidelio.dao.ByPassBSCSDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc

@Component(value = "byPassFidelioManagerBiz")
public class ByPassFidelioManagerBizImpl implements ByPassFidelioManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory.getLogger(ByPassFidelioManagerBizImpl.class);
	@Autowired
	private ByPassBSCSDao byPassBSCSDao;

	public Integer modeByPass() {

		LOG.info("acces to fidelio database");
		return (byPassBSCSDao.modeByPass()) ? 1 : -1;

	}

}
