package ma.iam.mobile.fidelio.bean;

public class OpenInvoicesRequest {

	private String nd;
	private String idType;
	private String customerIdentity;
	private String codeFidFixe;
	private String codePayementInternet;

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getCustomerIdentity() {
		return customerIdentity;
	}

	public void setCustomerIdentity(String customerIdentity) {
		this.customerIdentity = customerIdentity;
	}

	

	public String getCodeFidFixe() {
		return codeFidFixe;
	}

	public void setCodeFidFixe(String codeFidFixe) {
		this.codeFidFixe = codeFidFixe;
	}

	public String getCodePayementInternet() {
		return codePayementInternet;
	}

	public void setCodePayementInternet(String codePayementInternet) {
		this.codePayementInternet = codePayementInternet;
	}

}
