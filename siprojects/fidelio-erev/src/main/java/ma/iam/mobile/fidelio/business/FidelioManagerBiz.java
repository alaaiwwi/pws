package ma.iam.mobile.fidelio.business;

import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.fidelio.bean.AgenceBean;
import ma.iam.mobile.fidelio.bean.CatalogueEngagementBean;
import ma.iam.mobile.fidelio.bean.CategorieArticleBean;
import ma.iam.mobile.fidelio.bean.CompteClientBean;
import ma.iam.mobile.fidelio.bean.DetailComplementBean;
import ma.iam.mobile.fidelio.bean.ModeEngagementBean;
import ma.iam.mobile.fidelio.bean.ModelePosteBean;
import ma.iam.mobile.fidelio.bean.OpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.OpenInvoicesResponse;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesResponses;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;

public interface FidelioManagerBiz {

	
	List<ModeEngagementBean> getEngagementDisponilbe(String nd) throws FunctionnalException;

	List<CategorieArticleBean> getCategorieProduit() throws FunctionnalException;

	List<ModelePosteBean> getFamillePoste() throws FunctionnalException;

	CompteClientBean getSoldePointClient(String nd) throws FunctionnalException;

	List<CatalogueEngagementBean> getListPack(String nd, String modeEngagementId, String categorieProduitId,
			String famillePosteId) throws FunctionnalException;

	DetailComplementBean getComplement(String nd, String articleId, String modeEngagementId)
			throws FunctionnalException;

	
	List<AgenceBean> getAgences() throws FunctionnalException;
	
	String doReengagement(String nd, String articleId, String modeEngagementId, Long pointsAcquis,
			Long pointsEncours, Double complement,String imei,String agenceId) throws FunctionnalException;
}
