package ma.iam.mobile.fidelio.bean;


public class EngagementEnCoursBean  {

	/** The id. */
	private Integer id;

	/** The libelle. */
	private String libelle;

	/** The date fin. */
	private String dateFin;

	/** The reliquat provisoire. */
	private Double reliquatProvisoire;

	/** The reliquat provisoire en point. */
	private Double reliquatProvisoireEnPoint;

	/** The occ index. */
	private int occIndex;

	/** The CO ID. */
	private String coId;

	/**
	 * Gets the date fin.
	 * 
	 * @return the date fin
	 */
	public String getDateFin() {
		return dateFin;
	}

	/**
	 * Sets the date fin.
	 * 
	 * @param dateFin
	 *            the new date fin
	 */
	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the libelle.
	 * 
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * 
	 * @param libelle
	 *            the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the reliquat provisoire.
	 * 
	 * @return the reliquat provisoire
	 */
	public Double getReliquatProvisoire() {
		return reliquatProvisoire;
	}

	/**
	 * Sets the reliquat provisoire.
	 * 
	 * @param reliquat
	 *            the new reliquat provisoire
	 */
	public void setReliquatProvisoire(Double reliquat) {
		this.reliquatProvisoire = reliquat;
	}

	/**
	 * Gets the occ index.
	 * 
	 * @return the occ index
	 */
	public int getOccIndex() {
		return occIndex;
	}

	/**
	 * Sets the occ index.
	 * 
	 * @param occIndex
	 *            the new occ index
	 */
	public void setOccIndex(int occIndex) {
		this.occIndex = occIndex;
	}

	/**
	 * Sets the reliquat provisoire en point.
	 * 
	 * @param reliquatProvisoireEnPoint
	 *            the reliquatProvisoireEnPoint to set
	 */
	public void setReliquatProvisoireEnPoint(Double reliquatProvisoireEnPoint) {
		this.reliquatProvisoireEnPoint = reliquatProvisoireEnPoint;
	}

	/**
	 * Gets the reliquat provisoire en point.
	 * 
	 * @return the reliquatProvisoireEnPoint
	 */
	public Double getReliquatProvisoireEnPoint() {
		return reliquatProvisoireEnPoint;
	}

	// FIXBUG - QC14059 ANP
	/**
	 * Get the contract ID.
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the contract ID.
	 * 
	 * @param coId
	 *            the contract ID
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	// .FIXBUG - QC14059 ANP
	/**
	 * Gets the Description of the object EngagementEnCoursBean.
	 * 
	 * @return the String Description of the object EngagementEnCoursBean.
	 */

	public String toString() {

		String strRetour = "\n";

		strRetour += " ********************************************* Description Objet EngagementEnCoursBean ******************************************* ] \n";
		strRetour += "  id =  " + id + "\n";
		strRetour += "  libelle =  " + libelle + "\n";
		strRetour += "  dateFin =  " + dateFin + "\n";
		strRetour += "  reliquatProvisoire =  " + reliquatProvisoire + "\n";
		strRetour += "  reliquatProvisoireEnPoint =  " + reliquatProvisoireEnPoint + "\n";
		strRetour += "  occIndex =  " + occIndex + "\n";
		strRetour += " ********************************************************************************************************************************* ] \n";

		return strRetour;

	}

}
