/*
 * @author AtoS
 */
package ma.iam.mobile.fidelio.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.fidelio.bean.AgenceBean;
import ma.iam.mobile.fidelio.bean.CatalogueEngagementBean;
import ma.iam.mobile.fidelio.bean.CategorieArticleBean;
import ma.iam.mobile.fidelio.bean.CompteClientBean;
import ma.iam.mobile.fidelio.bean.DetailComplementBean;
import ma.iam.mobile.fidelio.bean.ModeEngagementBean;
import ma.iam.mobile.fidelio.bean.ModelePosteBean;
import ma.iam.mobile.fidelio.bean.OpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.OpenInvoicesResponse;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesResponses;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;

/**
 * The Class FidelioManagerService.
 */
@WebService(serviceName = "fidelioManagerService")
public interface FidelioManagerService {

	@WebMethod(operationName = "fidelioModeByPass")
	@WebResult(name = "statut")
	public Integer fidelioModeByPass();

	@WebMethod(operationName = "bscsModeByPass")
	@WebResult(name = "statut")
	public Integer bscsModeByPass();

	@WebMethod(operationName = "getEngagementDisponilbe")
	@WebResult(name = "result")
	public List<ModeEngagementBean> getEngagementDisponilbe(@WebParam(name = "numAppel") String nd)
			throws FunctionnalException;

	@WebMethod(operationName = "getCategorieProduit")
	@WebResult(name = "result")
	public List<CategorieArticleBean> getCategorieProduit() throws FunctionnalException;

	@WebMethod(operationName = "getFamillePoste")
	@WebResult(name = "result")
	public List<ModelePosteBean> getFamillePoste() throws FunctionnalException;

	@WebMethod(operationName = "getSoldePointClient")
	@WebResult(name = "result")
	public CompteClientBean getSoldePointClient(@WebParam(name = "numAppel") String nd) throws FunctionnalException;

	@WebMethod(operationName = "getListPack")
	@WebResult(name = "result")
	public List<CatalogueEngagementBean> getListPack(@WebParam(name = "numAppel") String nd,
			@WebParam(name = "modeEngagementId") String modeEngagementId,
			@WebParam(name = "categorieProduitId") String categorieProduitId,
			@WebParam(name = "famillePosteId") String famillePosteId) throws FunctionnalException;

	@WebMethod(operationName = "getComplement")
	@WebResult(name = "result")
	public DetailComplementBean getComplement(@WebParam(name = "numAppel") String nd,
			@WebParam(name = "articleId") String articleId,
			@WebParam(name = "modeEngagementId") String modeEngagementId) throws FunctionnalException;

	@WebMethod(operationName = "doReengagement")
	@WebResult(name = "result")
	public String doReengagement(@WebParam(name = "numAppel") String nd,
			@WebParam(name = "articleId") String articleId,
			@WebParam(name = "modeEngagementId") String modeEngagementId,
			@WebParam(name = "pointsAcquis") Long pointsAcquis, @WebParam(name = "pointsEncours") Long pointsEncours,
			@WebParam(name = "complement") Double complement,
			@WebParam(name = "imei") String imei, @WebParam(name = "agenceId") String agenceId

	) throws FunctionnalException;

	@WebMethod(operationName = "getAgences")
	@WebResult(name = "result")
	public List<AgenceBean> getAgences() throws FunctionnalException;

}
