package ma.iam.mobile.fidelio.dao;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import ma.iam.mobile.fidelio.bean.AgenceBean;
import ma.iam.mobile.fidelio.bean.ArticleBean;
import ma.iam.mobile.fidelio.bean.CatalogueEngagementBean;
import ma.iam.mobile.fidelio.bean.CategorieArticleBean;
import ma.iam.mobile.fidelio.bean.CategorieClientBean;
import ma.iam.mobile.fidelio.bean.CommandeIdBean;
import ma.iam.mobile.fidelio.bean.CompteClientBean;
import ma.iam.mobile.fidelio.bean.ModeEngagementBean;
import ma.iam.mobile.fidelio.bean.ModelePosteBean;
import ma.iam.mobile.fidelio.bean.TarifPackBSCSBean;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.persist.ContractCustomer;
import ma.iam.mobile.fidelio.persist.ContractPersist;
import ma.iam.mobile.fidelio.persist.InvoicesPersist;

public interface FidelioDao extends BaseDAOFidelio {
	
	ContractPersist getContractByDnNum(String nd);

	List<ContractPersist> getCustomersWithSameIdent(ContractPersist contract);

	List<InvoicesPersist> getInvoices(List<Long> customerIds);

	ContractPersist getIdentifiantByCustomerId(String customerId);

	String getCodeFid(String customerId);

	List<ContractCustomer> getCustomersWithContract(String customerId);

	String getPartner(String partnerId);


	List<InvoicesPersist> getInvoicesByRefInvoice(List<String> invoices);

	List<ModeEngagementBean> getEngagementEnService();

	List<CategorieArticleBean> getCategorieProduit();

	List<ModelePosteBean> getModelePoste();

	CompteClientBean getCompteClient(String customerId);

	ModeEngagementBean getEngagementByCode(String codeEngagement);

	CategorieClientBean getCategorieClientByQualite(Integer qualiteId);

	List<CatalogueEngagementBean> getListPack(String categorieClientId, String qualiteId, String modeEngagementId,
			String categorieProduitId, String planTarifaireId, String familleProduitId, String planTarifaireDes);

	CatalogueEngagementBean getTarifPack(String categorieClientId, String qualiteId, String modeEngagementId,
			String categorieProduitId, String planTarifaireId, String familleProduitId, String planTarifaireDes,
			String articleId);

	Integer getBonusEngagement(String categorieClientId, String modeEngagementId);

	Integer getSeuilMinimalEngagement(String categorieClientId, String modeEngagementId);

	Integer getTauxConversion(String categorieClientId);

	Double getPrixRachatPointByModeEngagement(String modeEngagementId);

	ModeEngagementBean getEngagementById(String modeEngagementId);

	ArticleBean getArticleById(String articleId);

	TarifPackBSCSBean getTarifPackFidelio(String codeArticle, String modeEngagementId, String tmcode);

	Float getPrixPackMinimalPourRemise(String categorieClientId);

	CommandeIdBean createOrder(String customerId, String coId, Long pointsAcquis, Long pointsEncours, String imei, String nd,
			int agenceId, Double complement, String articleId);

	List<AgenceBean> getAgences();

	AgenceBean getAgenceById(String agenceId);

	int updateSoldeConvertis(Integer affaireId, Integer commandeId, Integer compteClientId, Long soldeAcquisUsed,
			Long soldeEnCoursUsed, Long withCmdTrace) throws FunctionnalException;
	
	ModeEngagementBean getModeEngagementByBscsId(String idBscs);

}
