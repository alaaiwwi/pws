package ma.iam.mobile.fidelio.persist;

public class FillCustInfoPersist {
	private Long customerId;
	private String cin;
	private String firstName;
	private String lastName;
	private String adrLine1;
	private String adrLine2;
	private String adrNum;
	private String adrStreet;
	private String adrZip;
	private String adrCity;
	private String adrCountry;
	private Long country;
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAdrLine1() {
		return adrLine1;
	}
	public void setAdrLine1(String adrLine1) {
		this.adrLine1 = adrLine1;
	}
	public String getAdrLine2() {
		return adrLine2;
	}
	public void setAdrLine2(String adrLine2) {
		this.adrLine2 = adrLine2;
	}
	public String getAdrNum() {
		return adrNum;
	}
	public void setAdrNum(String adrNum) {
		this.adrNum = adrNum;
	}
	public String getAdrStreet() {
		return adrStreet;
	}
	public void setAdrStreet(String adrStreet) {
		this.adrStreet = adrStreet;
	}
	public String getAdrZip() {
		return adrZip;
	}
	public void setAdrZip(String adrZip) {
		this.adrZip = adrZip;
	}
	public String getAdrCity() {
		return adrCity;
	}
	public void setAdrCity(String adrCity) {
		this.adrCity = adrCity;
	}
	public String getAdrCountry() {
		return adrCountry;
	}
	public void setAdrCountry(String adrCountry) {
		this.adrCountry = adrCountry;
	}
	public Long getCountry() {
		return country;
	}
	public void setCountry(Long country) {
		this.country = country;
	}
	
	
	
}
