package ma.iam.mobile.fidelio.bean;

/**
 * The Class CatalogueEngagementBean.
 * 
 * @version 1.0.0
 * @author AtoS
 */
public class CatalogueEngagementBean {

	/** The id. */
	private String id;
	/** The prix points. */
	private Long prixPoints;

	/** The article id. */
	private String articleId;

	/** The categorie code. */
	private String categorieCode;

	/** The reliquat ht. */
	private Double reliquatHT;

	/** The libelle. */
	private String libelle;

	/** The quantite. */
	private String quantite;

	/** The pts aquis. */
	private Integer ptsAquis;

	/** The pts en cours. */
	private Integer ptsEnCours;

	/** The is exclus. */
	private Boolean isExclus = false;

	/** The colour. */
	private String colour;

	/** The is dispo client. */
	private Boolean isDispoClient;

	/** The code produit. */
	private String codeProduit;

	/** The bonus. */
	private Integer bonus;

	public String getCodeProduit() {
		return codeProduit;
	}

	public void setCodeProduit(String codeProduit) {
		this.codeProduit = codeProduit;
	}

	public Integer getBonus() {
		return bonus;
	}

	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getPrixPoints() {
		return prixPoints;
	}

	public void setPrixPoints(Long prixPoints) {
		this.prixPoints = prixPoints;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getCategorieCode() {
		return categorieCode;
	}

	public void setCategorieCode(String categorieCode) {
		this.categorieCode = categorieCode;
	}

	public Double getReliquatHT() {
		return reliquatHT;
	}

	public void setReliquatHT(Double reliquatHT) {
		this.reliquatHT = reliquatHT;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

	public Integer getPtsAquis() {
		return ptsAquis;
	}

	public void setPtsAquis(Integer ptsAquis) {
		this.ptsAquis = ptsAquis;
	}

	public Integer getPtsEnCours() {
		return ptsEnCours;
	}

	public void setPtsEnCours(Integer ptsEnCours) {
		this.ptsEnCours = ptsEnCours;
	}

	public Boolean getIsExclus() {
		return isExclus;
	}

	public void setIsExclus(Boolean isExclus) {
		this.isExclus = isExclus;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public Boolean getIsDispoClient() {
		return isDispoClient;
	}

	public void setIsDispoClient(Boolean isDispoClient) {
		this.isDispoClient = isDispoClient;
	}

	public CatalogueEngagementBean() {
		super();
		// TODO Auto-generated constructor stub
	}

}
