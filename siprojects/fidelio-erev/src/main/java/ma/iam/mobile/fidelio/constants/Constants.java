package ma.iam.mobile.fidelio.constants;

public interface Constants {

	public static final String FORFAIT_GEOLOC_5_MO = "GELOC";

	public static final String FORFAIT_LIBERTE_ENGAGEMENT = "FOSPR";

	public static final String CODE_ARTICLE_CADEAUX = "C";

	public static final String CODE_ENGAGEMENT_FIDELIO_12M = "F12";

	public static final Integer MODE_ENGAGEMENT_FIDELIO_12M_DUREE = 12;

	public static final float REMISE_PRIVILEGE = 300f;

	public static final String MODEL_POST_SGE = "SGE";

	public static final String PREFIX_MODE_ENGAGEMENT_12_MOIS_SEMA = "PRIX_12M";

	public static final String PREFIX_MODE_ENGAGEMENT_24_MOIS_SEMA = "PRIX_24M";

	/** La constante MODE_ENGAGEMENT_12_MOIS. **/
	public static final String PREFIX_MODE_ENGAGEMENT_12_MOIS = "12MPA-";

	/** La constante MODE_ENGAGEMENT_24_MOIS. **/
	public static final String PREFIX_MODE_ENGAGEMENT_24_MOIS = "24MPA-";

}
