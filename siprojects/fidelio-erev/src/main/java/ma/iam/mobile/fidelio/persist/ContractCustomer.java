package ma.iam.mobile.fidelio.persist;

public class ContractCustomer {
	
	private String customerId;
	private String coId;
	private String status;
	private String shdes;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCoId() {
		return coId;
	}

	public void setCoId(String coId) {
		this.coId = coId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getShdes() {
		return shdes;
	}

	public void setShdes(String shdes) {
		this.shdes = shdes;
	}

}
