/*
 * @author AtoS
 */

package ma.iam.mobile.fidelio.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.mobile.fidelio.bean.AgenceBean;
import ma.iam.mobile.fidelio.bean.ArticleBean;
import ma.iam.mobile.fidelio.bean.CatalogueEngagementBean;
import ma.iam.mobile.fidelio.bean.CategorieArticleBean;
import ma.iam.mobile.fidelio.bean.CategorieClientBean;
import ma.iam.mobile.fidelio.bean.CommandeIdBean;
import ma.iam.mobile.fidelio.bean.CompteClientBean;
import ma.iam.mobile.fidelio.bean.ModeEngagementBean;
import ma.iam.mobile.fidelio.bean.ModelePosteBean;
import ma.iam.mobile.fidelio.bean.TarifPackBSCSBean;
import ma.iam.mobile.fidelio.constants.Constants;
import ma.iam.mobile.fidelio.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.fidelio.dao.FidelioDao;
import ma.iam.mobile.fidelio.dao.MobileBSCSDao;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.persist.ContractCustomer;
import ma.iam.mobile.fidelio.persist.ContractPersist;
import ma.iam.mobile.fidelio.persist.InvoicesPersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

@Repository
public class FidelioDaoImpl extends BaseDAOFidelioImpl implements FidelioDao {
	private static final Logger LOG = LoggerFactory.getLogger(MobileBSCSDao.class);
	private static final String GET_CONTRACT_BY_DN_NUM = MobileBSCSDaoImpl.class.getName() + ".GET_CONTRACT_BY_DN_NUM";

	private static final String GET_CUSTOMER_WITH_SAME_IDENT = MobileBSCSDaoImpl.class.getName()
			+ ".GET_CUSTOMER_WITH_SAME_IDENT";

	private static final String LIST_INVOICES = MobileBSCSDaoImpl.class.getName() + ".LIST_INVOICES";

	private static final String GET_IDENT_BY_CUSTOMER_ID = MobileBSCSDaoImpl.class.getName()
			+ ".GET_IDENT_BY_CUSTOMER_ID";
	private static final String GET_CODE_FID = MobileBSCSDaoImpl.class.getName() + ".GET_CODE_FID";

	private static final String GET_CUSTOMER_CONTRACT = MobileBSCSDaoImpl.class.getName() + ".GET_CUSTOMER_CONTRACT";

	private static final String GET_PARTNER_DES = MobileBSCSDaoImpl.class.getName() + ".GET_PARTNER_DES";

	private static final String LIST_INVOICES_REF_INVOICE = MobileBSCSDaoImpl.class.getName()
			+ ".LIST_INVOICES_REF_INVOICE";

	private static final String LIST_ENGAGEMENT_EN_SERVICE = FidelioDaoImpl.class.getName()
			+ ".LIST_ENGAGEMENT_EN_SERVICE";

	private static final String LIST_CATEGORIE_PRODUIT = FidelioDaoImpl.class.getName() + ".LIST_CATEGORIE_PRODUIT";

	private static final String LIST_MODELE_POSTE = FidelioDaoImpl.class.getName() + ".LIST_MODELE_POSTE";

	private static final String GET_COMPTE_CLIENT = FidelioDaoImpl.class.getName() + ".GET_COMPTE_CLIENT";

	private static final String GET_ENGAGEMENT_BY_CODE = FidelioDaoImpl.class.getName() + ".GET_ENGAGEMENT_BY_CODE";

	private static final String GET_CATEGORIE_CLIENT_BY_QUALITE = FidelioDaoImpl.class.getName()
			+ ".GET_CATEGORIE_CLIENT_BY_QUALITE";

	private static final String GET_LIST_PACK = FidelioDaoImpl.class.getName() + ".GET_LIST_PACK";

	private static final String GET_TARIF_PACK = FidelioDaoImpl.class.getName() + ".GET_TARIF_PACK";

	private static final String GET_BONUS_ENGAGEMENT = FidelioDaoImpl.class.getName() + ".GET_BONUS_ENGAGEMENT";

	private static final String GET_SEUIL_MIN_CONVERSION = FidelioDaoImpl.class.getName() + ".GET_SEUIL_MIN_CONVERSION";

	private static final String GET_TAUX_CONVERSION = FidelioDaoImpl.class.getName() + ".GET_TAUX_CONVERSION";

	private static final String GET_TARIF_ACHAT_POINT = FidelioDaoImpl.class.getName() + ".GET_TARIF_ACHAT_POINT";

	private static final String GET_ENGAGEMENT_BY_ID = FidelioDaoImpl.class.getName() + ".GET_ENGAGEMENT_BY_ID";

	private static final String GET_ARTICLE_BY_ID = FidelioDaoImpl.class.getName() + ".GET_ARTICLE_BY_ID";

	private static final String GET_TARIF_PACK_FIDELIO = FidelioDaoImpl.class.getName() + ".GET_TARIF_PACK_FIDELIO";

	private static final String GET_SEUIL_MIN_PACKS = FidelioDaoImpl.class.getName() + ".GET_SEUIL_MIN_PACKS";

	private static final String GET_AFFAIRE_SEQ = FidelioDaoImpl.class.getName() + ".GET_AFFAIRE_SEQ";

	private static final String GET_COMMANDE_SEQ = FidelioDaoImpl.class.getName() + ".GET_COMMANDE_SEQ";
	
	private static final String LIST_AGENCES = FidelioDaoImpl.class.getName() + ".LIST_AGENCES";
	
	private static final String GET_AGENCE_BY_ID = FidelioDaoImpl.class.getName() + ".GET_AGENCE_BY_ID";
	
	private static final String ENGAGEMENT_BY_ID_BSCS = FidelioDaoImpl.class.getName() + ".ENGAGEMENT_BY_ID_BSCS";

	public ContractPersist getContractByDnNum(String nd) {

		LOG.debug("--> getContractByDnNum nd {}", nd);
		final List<ContractPersist> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CONTRACT_BY_DN_NUM);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("nd", nd);

			final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
					ContractPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getContractByDnNum");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getContractByDnNum is  null for nd = " + nd);
			return null;
		}
		return (list != null && !list.isEmpty()) ? list.get(0) : null;

	}

	public ContractPersist getIdentifiantByCustomerId(String customerId) {

		LOG.debug("--> getIdentifiantByCustomerId customerId {}", customerId);
		final List<ContractPersist> list;
		try {

			final String sql = CustomSQLUtil.get(GET_IDENT_BY_CUSTOMER_ID);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("customerId", customerId);

			final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
					ContractPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- customerId");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getIdentifiantByCustomerId is  null for nd = " + customerId);
			return null;
		}
		return (list != null && !list.isEmpty()) ? list.get(0) : null;

	}

	public List<ContractPersist> getCustomersWithSameIdent(ContractPersist contract) {

		LOG.debug("--> getCustomersWithSameIdent contract {}", contract);
		final List<ContractPersist> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CUSTOMER_WITH_SAME_IDENT);

			@SuppressWarnings("rawtypes")
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerParent", contract.getIdCustomerParent());
			namedParameters.put("ident", String.valueOf(contract.getCustomerIdentifiant()));

			final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
					ContractPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getContractByDnNum");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCustomersWithSameIdent is  null for contract = " + contract);
			return null;
		}
		return list;

	}

	public List<InvoicesPersist> getInvoices(List<Long> customerIds) {

		LOG.debug("--> getInvoices customerIds {}", customerIds);
		final List<InvoicesPersist> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_INVOICES);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("listCust", customerIds);

			final CustomBeanPropertyRowMapper<InvoicesPersist> custRowMapper = new CustomBeanPropertyRowMapper<InvoicesPersist>(
					InvoicesPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getInvoices");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getInvoices is  null for customerIds = " + customerIds);
			return null;
		}
		return list;

	}

	public String getCodeFid(String customerId) {

		LOG.debug("--> getCodeFid");

		try {

			final String sql = CustomSQLUtil.get(GET_CODE_FID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			String result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
			if ((result != null) && (!"".equals(result))) {
				return result;
			}
			return null;
		} catch (EmptyResultDataAccessException e) {

		}
		return null;

	}

	public List<ContractCustomer> getCustomersWithContract(String customerId) {

		LOG.debug("--> getCustomersWithContract customerId {}", customerId);
		final List<ContractCustomer> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CUSTOMER_CONTRACT);

			@SuppressWarnings("rawtypes")
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			final CustomBeanPropertyRowMapper<ContractCustomer> custRowMapper = new CustomBeanPropertyRowMapper<ContractCustomer>(
					ContractCustomer.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getCustomersWithContract");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCustomersWithContract is  null for customerId = " + customerId);
			return null;
		}
		return list;

	}

	public String getPartner(String partnerId) {

		LOG.debug("--> getPartner");

		try {

			final String sql = CustomSQLUtil.get(GET_PARTNER_DES);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("partnerId", partnerId);

			String result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
			if ((result != null) && (!"".equals(result))) {
				return result;
			}
			return null;
		} catch (EmptyResultDataAccessException e) {

		}
		return null;

	}

	public List<InvoicesPersist> getInvoicesByRefInvoice(List<String> invoices) {

		LOG.debug("--> getInvoicesByRefInvoice invoices {}", invoices);
		final List<InvoicesPersist> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_INVOICES_REF_INVOICE);

			final SqlParameterSource namedParameters = new MapSqlParameterSource("listRef", invoices);

			final CustomBeanPropertyRowMapper<InvoicesPersist> custRowMapper = new CustomBeanPropertyRowMapper<InvoicesPersist>(
					InvoicesPersist.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getInvoicesByRefInvoice");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getInvoices is  null for invoices = " + invoices);
			return null;
		}
		return list;

	}

	public CommandeIdBean createOrder(String customerId, String coId, Long pointsAcquis, Long pointsEncours, String imei,
			String nd, int agenceId, Double complement,String articleId) {

		CallableStatement callableStatement = null;

		Map<String, String> map = new HashMap<String, String>();
		Long commandeId = 0L;
		CommandeIdBean commandeBean = new CommandeIdBean();
		Connection connection = null;
		try {

			Map<String, String> namedParameters = new HashMap();
			String sql = CustomSQLUtil.get(GET_AFFAIRE_SEQ);

			Long affaireId = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);
			namedParameters = new HashMap();

			sql = CustomSQLUtil.get(GET_COMMANDE_SEQ);

			 commandeId = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);
			 commandeBean.setAffaireId(affaireId);
			 commandeBean.setCommandeId(commandeId);
			 
			 connection = super.getDataSource().getConnection();
			 //connection.setAutoCommit(false);
			callableStatement = connection.prepareCall("{ ? = call FIDELIO.AFFECT_CMD_FIDELIO(?,?,?,?,?,?,?,?,?,?,?) }");
			callableStatement.setString(2, customerId);
			callableStatement.setString(3, coId);
			callableStatement.setLong(4, pointsAcquis);
			callableStatement.setLong(5, pointsEncours);
			callableStatement.setString(6, articleId);
			callableStatement.setString(7, imei);
			callableStatement.setString(8, nd);
			callableStatement.setInt(9, agenceId);
			callableStatement.setDouble(10, complement);
			callableStatement.setLong(11, commandeId);
			callableStatement.setLong(12, affaireId);

			callableStatement.registerOutParameter(1, java.sql.Types.NUMERIC);

			callableStatement.executeUpdate();
			int  result = callableStatement.getInt(1);


		} catch (Exception e) {
			try {
				connection.rollback();
				throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			} catch (Exception e2) {
			}
			
			LOG.error("error in createOrder : " + e.getMessage());
			map.put("MESSAGE", null);
			map.put("ERROR", e.getMessage());

		} finally {

			if (callableStatement != null) {
				try {
					callableStatement.close();
				} catch (SQLException e) {
					LOG.error("error in createOrder : " + e.getMessage());
					map.put("MESSAGE", null);
					map.put("ERROR", e.getMessage());

				}
			}
		}
		return commandeBean;
	}

	public List<ModeEngagementBean> getEngagementEnService() {

		LOG.debug("--> getEngagementEnService");
		final List<ModeEngagementBean> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_ENGAGEMENT_EN_SERVICE);

			// final SqlParameterSource namedParameters = = new HashMap();;
			Map<String, String> namedParameters = new HashMap();

			final CustomBeanPropertyRowMapper<ModeEngagementBean> custRowMapper = new CustomBeanPropertyRowMapper<ModeEngagementBean>(
					ModeEngagementBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getEngagementEnService");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getEngagementEnService is  null ");
			return null;
		}
		return list;
	}

	@Override
	public List<CategorieArticleBean> getCategorieProduit() {
		LOG.debug("--> getCategorieProduit");
		final List<CategorieArticleBean> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_CATEGORIE_PRODUIT);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("code", Constants.CODE_ARTICLE_CADEAUX);

			final CustomBeanPropertyRowMapper<CategorieArticleBean> custRowMapper = new CustomBeanPropertyRowMapper<CategorieArticleBean>(
					CategorieArticleBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getCategorieProduit");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCategorieProduit is  null ");
			return null;
		}
		return list;
	}

	@Override
	public List<ModelePosteBean> getModelePoste() {
		LOG.debug("--> getFamillePoste");
		final List<ModelePosteBean> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_MODELE_POSTE);

			Map<String, String> namedParameters = new HashMap();
			// namedParameters.put("code", Constants.CODE_ARTICLE_CADEAUX);

			final CustomBeanPropertyRowMapper<ModelePosteBean> custRowMapper = new CustomBeanPropertyRowMapper<ModelePosteBean>(
					ModelePosteBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getFamillePoste");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getFamillePoste is  null ");
			return null;
		}
		return list;
	}

	@Override
	public CompteClientBean getCompteClient(String customerId) {
		LOG.debug("--> getCompteClient");
		final List<CompteClientBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_COMPTE_CLIENT);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("customerId", customerId);

			final CustomBeanPropertyRowMapper<CompteClientBean> custRowMapper = new CustomBeanPropertyRowMapper<CompteClientBean>(
					CompteClientBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getCompteClient");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCompteClient is  null ");
			return null;
		}
		return list.get(0);
	}

	public ModeEngagementBean getEngagementByCode(String codeEngagement) {

		LOG.debug("--> getEngagementByCode");
		final List<ModeEngagementBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_ENGAGEMENT_BY_CODE);

			// final SqlParameterSource namedParameters = = new HashMap();;
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("codeEngagement", codeEngagement);

			final CustomBeanPropertyRowMapper<ModeEngagementBean> custRowMapper = new CustomBeanPropertyRowMapper<ModeEngagementBean>(
					ModeEngagementBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getEngagementByCode");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getEngagementByCode is  null ");
			return null;
		}
		return list.get(0);
	}

	public CategorieClientBean getCategorieClientByQualite(Integer qualiteId) {

		LOG.debug("--> getCategorieClientByQualite");
		final List<CategorieClientBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_CATEGORIE_CLIENT_BY_QUALITE);

			// final SqlParameterSource namedParameters = = new HashMap();;
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("qualiteId", qualiteId.toString());

			final CustomBeanPropertyRowMapper<CategorieClientBean> custRowMapper = new CustomBeanPropertyRowMapper<CategorieClientBean>(
					CategorieClientBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getCategorieClientByQualite");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getCategorieClientByQualite is  null ");
			return null;
		}
		return list.get(0);
	}

	public List<CatalogueEngagementBean> getListPack(String categorieClientId, String qualiteId,
			String modeEngagementId, String categorieProduitId, String planTarifaireId, String familleProduitId,
			String planTarifaireDes) {

		LOG.debug("--> getListPack");
		final List<CatalogueEngagementBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_LIST_PACK);

			// final SqlParameterSource namedParameters = = new HashMap();;
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("catClientId", categorieClientId);
			namedParameters.put("qualiteId", qualiteId);
			namedParameters.put("modeEngagementId", modeEngagementId);
			namedParameters.put("catId", categorieProduitId);
			namedParameters.put("planTarifaire", planTarifaireId);
			namedParameters.put("famille", familleProduitId);
			namedParameters.put("planTarifaireDes", planTarifaireDes != null ? "true" : "false");

			final CustomBeanPropertyRowMapper<CatalogueEngagementBean> custRowMapper = new CustomBeanPropertyRowMapper<CatalogueEngagementBean>(
					CatalogueEngagementBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getListPack");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getListPack is  null ");
			return null;
		}
		return list;
	}

	public CatalogueEngagementBean getTarifPack(String categorieClientId, String qualiteId, String modeEngagementId,
			String categorieProduitId, String planTarifaireId, String familleProduitId, String planTarifaireDes,
			String articleId) {

		LOG.debug("--> getListPack");
		final List<CatalogueEngagementBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_TARIF_PACK);

			// final SqlParameterSource namedParameters = = new HashMap();;
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("catClientId", categorieClientId);
			namedParameters.put("qualiteId", qualiteId != null ? qualiteId : "-1");
			namedParameters.put("modeEngagementId", modeEngagementId);
			namedParameters.put("catId", categorieProduitId != null ? categorieProduitId : "-1");
			namedParameters.put("planTarifaire", planTarifaireId != null ? planTarifaireId : "-1");
			namedParameters.put("famille", familleProduitId != null ? familleProduitId : "-1");
			namedParameters.put("planTarifaireDes", planTarifaireDes != null ? "true" : "false");
			namedParameters.put("articleId", articleId);

			final CustomBeanPropertyRowMapper<CatalogueEngagementBean> custRowMapper = new CustomBeanPropertyRowMapper<CatalogueEngagementBean>(
					CatalogueEngagementBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getListPack");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getListPack is  null ");
			return null;
		}
		return list.get(0);
	}

	@Override
	public Integer getBonusEngagement(String categorieClientId, String modeEngagementId) {

		LOG.debug("--> getBonusEngagement");
		final List<Integer> list;
		try {

			final String sql = CustomSQLUtil.get(GET_BONUS_ENGAGEMENT);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("catClientId", categorieClientId);
			namedParameters.put("modeEngagementId", modeEngagementId);

			// final CustomBeanPropertyRowMapper<Integer> custRowMapper = new
			// CustomBeanPropertyRowMapper<Integer>(
			// Integer.class);
			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters, Integer.class);
			LOG.debug("<-- getBonusEngagement");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getBonusEngagement is  null ");
			return null;
		}
		return list.get(0);
	}

	@Override
	public Integer getSeuilMinimalEngagement(String categorieClientId, String modeEngagementId) {

		LOG.debug("--> getSeuilMinimalEngagement");
		final List<Integer> list;
		try {

			final String sql = CustomSQLUtil.get(GET_SEUIL_MIN_CONVERSION);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("catClientId", categorieClientId);
			namedParameters.put("modeEngagementId", modeEngagementId);

			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters, Integer.class);
			LOG.debug("<-- getSeuilMinimalEngagement ");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getSeuilMinimalEngagement is  null ");
			return null;
		}
		return list.get(0);
	}

	@Override
	public Integer getTauxConversion(String categorieClientId) {

		LOG.debug("--> getTauxConversion");
		final List<Integer> list;
		try {

			final String sql = CustomSQLUtil.get(GET_TAUX_CONVERSION);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("catClientId", categorieClientId);

			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters, Integer.class);
			LOG.debug("<-- getTauxConversion");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getTauxConversion is  null ");
			return null;
		}
		return list.get(0);
	}

	@Override
	public Double getPrixRachatPointByModeEngagement(String modeEngagementId) {

		LOG.debug("--> getPrixRachatPointByModeEngagement");
		final List<Double> list;
		try {

			final String sql = CustomSQLUtil.get(GET_TARIF_ACHAT_POINT);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("modeEngagementId", modeEngagementId);

			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters, Double.class);
			LOG.debug("<-- getSeuilMinimalEngagement ");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getSeuilMinimalEngagement is  null ");
			return null;
		}
		return list.get(0);
	}

	public ModeEngagementBean getEngagementById(String modeEngagementId) {

		LOG.debug("--> getEngagementById");
		final List<ModeEngagementBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_ENGAGEMENT_BY_ID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("modeEngagementId", modeEngagementId);

			final CustomBeanPropertyRowMapper<ModeEngagementBean> custRowMapper = new CustomBeanPropertyRowMapper<ModeEngagementBean>(
					ModeEngagementBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getEngagementById");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getEngagementById is  null ");
			return null;
		}
		return list.get(0);
	}

	public ArticleBean getArticleById(String articleId) {

		LOG.debug("--> getArticleById");
		final List<ArticleBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_ARTICLE_BY_ID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("articleId", articleId);

			final CustomBeanPropertyRowMapper<ArticleBean> custRowMapper = new CustomBeanPropertyRowMapper<ArticleBean>(
					ArticleBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getArticleById");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getArticleById is  null ");
			return null;
		}
		return list.get(0);
	}

	@Override
	public TarifPackBSCSBean getTarifPackFidelio(String codeArticle, String modeEngagementId, String tmcode) {
		LOG.debug("--> getTarifPackFidelio");
		final List<TarifPackBSCSBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_TARIF_PACK_FIDELIO);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("codeArticle", codeArticle);
			namedParameters.put("modeEngagementId", modeEngagementId);
			namedParameters.put("tmcode", tmcode);

			final CustomBeanPropertyRowMapper<TarifPackBSCSBean> custRowMapper = new CustomBeanPropertyRowMapper<TarifPackBSCSBean>(
					TarifPackBSCSBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getTarifPackFidelio");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getTarifPackFidelio is  null ");
			return null;
		}
		return list.get(0);
	}

	@Override
	public Float getPrixPackMinimalPourRemise(String categorieClientId) {
		LOG.debug("--> getPrixPackMinimalPourRemise");
		final List<Float> list;
		try {

			final String sql = CustomSQLUtil.get(GET_SEUIL_MIN_PACKS);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("catClientId", categorieClientId);

			list = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters, Float.class);
			LOG.debug("<-- getTarifPackFidelio");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getTarifPackFidelio is  null ");
			return null;
		}
		return list.get(0);
	}
	public List<AgenceBean> getAgences() {

		LOG.debug("--> getAgences");
		final List<AgenceBean> list;
		try {

			final String sql = CustomSQLUtil.get(LIST_AGENCES);

			Map<String, String> namedParameters = new HashMap();

			final CustomBeanPropertyRowMapper<AgenceBean> custRowMapper = new CustomBeanPropertyRowMapper<AgenceBean>(
					AgenceBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getAgences");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getAgences is  null ");
			return null;
		}
		return list;
	}

	

	public int updateSoldeConvertis(Integer affaireId, Integer commandeId,
			Integer compteClientId, Long soldeAcquisUsed,
			Long soldeEnCoursUsed, Long withCmdTrace) throws FunctionnalException  {

		String debugString = "";
		debugString += "\n affaireId :" + (affaireId);
		debugString += "\n commandeId :" + (commandeId);
		debugString += "\n compteClientId :" + (compteClientId);
		debugString += "\n soldeAcquisUsed  :" + (soldeAcquisUsed);
		debugString += "\n soldeEnCoursUsed :" + (soldeEnCoursUsed);
		debugString += "\n withCmdTrace:" + (withCmdTrace);

		LOG.info( "updateSoldeConvertis",
				debugString);

		Connection dao = null;
		
		CallableStatement query = null;
		Integer result;

		try {
			dao = super.getDataSource().getConnection();
	
			query = dao
					.prepareCall("{ ? =  call  UPDATESOLDECONVERTIS(?,?,?,?,?,?) }");

			query.registerOutParameter(1, Types.INTEGER);
			query.setLong(2, affaireId);
			query.setLong(3, commandeId);
			query.setLong(4, compteClientId);
			query.setLong(5, soldeAcquisUsed);
			query.setLong(6, soldeEnCoursUsed);
			query.setLong(7, withCmdTrace);

			query.executeQuery();
			result = query.getInt(1);

			if (result == 0) {
				query.getConnection().commit();
				query.clearParameters();
				query.close();
			}

			if (result != 0) {
				dao.rollback();
				LOG.debug(
						"updateSoldeConvertis",
						"La procédure UPDATESOLDECONVERTIS est executée avec erreurs code erreur : "
								+ result);
				throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			}
		} catch (SQLException e) {
			try {
				dao.rollback();
				throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			} catch (SQLException e1) {
				LOG.debug(
						"updateSoldeConvertis",
						"Impossible d'executer la procédure stockée UPDATESOLDECONVERTIS "
								+ e1.getMessage());
				
			}
			LOG.debug("updateSoldeConvertis",
					"Impossible d'executer la procédure stockée SEMA_INT_FIDELIO_BSCS.UPDATE_IMEI "
							+ e.getMessage());
	
		} finally {
			if (query != null) {
				try {
					query.close();
				} catch (SQLException e) {
					query = null;
				}
			}
		}
		return 0;
	}
	
	public AgenceBean getAgenceById(String agenceId) {

		LOG.debug("--> getAgenceById");
		final List<AgenceBean> list;
		try {

			final String sql = CustomSQLUtil.get(GET_AGENCE_BY_ID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("agenceId", agenceId);

			final CustomBeanPropertyRowMapper<AgenceBean> custRowMapper = new CustomBeanPropertyRowMapper<AgenceBean>(
					AgenceBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getAgenceById");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getAgenceById is  null ");
			return null;
		}
		return list.get(0);
	}

	

	public ModeEngagementBean getModeEngagementByBscsId(String idBscs) {

		LOG.debug("--> getModeEngagementByBscsId");
		final List<ModeEngagementBean> list;
		try {

			final String sql = CustomSQLUtil.get(ENGAGEMENT_BY_ID_BSCS);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("idBscs", idBscs);

			final CustomBeanPropertyRowMapper<ModeEngagementBean> custRowMapper = new CustomBeanPropertyRowMapper<ModeEngagementBean>(
					ModeEngagementBean.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getModeEngagementByBscsId");
		} catch (EmptyResultDataAccessException e) {

			LOG.error("getModeEngagementByBscsId is  null ");
			return null;
		}
		return list.get(0);
	}
}
