package ma.iam.mobile.fidelio.business.impl;

import ma.iam.mobile.fidelio.business.ByPassBscsManagerBiz;
import ma.iam.mobile.fidelio.dao.ByPassBSCSDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc

@Component(value = "byPassBscsManagerBiz")
public class ByPassBscsManagerBizImpl implements ByPassBscsManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ByPassBscsManagerBizImpl.class);
	@Autowired
	private ByPassBSCSDao byPassBscsDao;

	public Integer modeByPass() {

		LOG.info("acces to BSCS database");
		return (byPassBscsDao.modeByPass()) ? 1 : -1;

	}

}
