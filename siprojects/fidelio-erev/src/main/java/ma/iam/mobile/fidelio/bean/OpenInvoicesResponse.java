package ma.iam.mobile.fidelio.bean;

public class OpenInvoicesResponse {

	private String invoiceReference;
	private Double invoicedAmount;
	private Double amountRemaining;
	private String month;
	private String type;
	private String hash;

	public String getInvoiceReference() {
		return invoiceReference;
	}

	public void setInvoiceReference(String invoiceReference) {
		this.invoiceReference = invoiceReference;
	}

	public Double getInvoicedAmount() {
		return invoicedAmount;
	}

	public void setInvoicedAmount(Double invoicedAmount) {
		this.invoicedAmount = invoicedAmount;
	}

	public Double getAmountRemaining() {
		return amountRemaining;
	}

	public void setAmountRemaining(Double amountRemaining) {
		this.amountRemaining = amountRemaining;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
