package ma.iam.mobile.fidelio.bean;

public class CategorieClientBean {

	/** The id. */
	private String id;

	/** The code. */
	private String code;

	/** The libelle. */
	private String libelle;

	/** The utilisateur. */
	private String utilisateur;

	/** The en service. */
	private String enService;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the libelle.
	 * 
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * 
	 * @param libelle
	 *            the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the utilisateur.
	 * 
	 * @return the utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Sets the utilisateur.
	 * 
	 * @param utilisateur
	 *            the new utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Gets the en service.
	 * 
	 * @return the en service
	 */
	public String getEnService() {
		return enService;
	}

	/**
	 * Sets the en service.
	 * 
	 * @param enService
	 *            the new en service
	 */
	public void setEnService(String enService) {
		this.enService = enService;
	}

}
