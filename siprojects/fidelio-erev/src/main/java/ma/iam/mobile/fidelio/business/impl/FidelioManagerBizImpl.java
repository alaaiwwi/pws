package ma.iam.mobile.fidelio.business.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.fidelio.bean.AgenceBean;
import ma.iam.mobile.fidelio.bean.ArticleBean;
import ma.iam.mobile.fidelio.bean.CatalogueEngagementBean;
import ma.iam.mobile.fidelio.bean.CategorieArticleBean;
import ma.iam.mobile.fidelio.bean.CategorieClientBean;
import ma.iam.mobile.fidelio.bean.CommandeIdBean;
import ma.iam.mobile.fidelio.bean.CompteClientBean;
import ma.iam.mobile.fidelio.bean.DetailComplementBean;
import ma.iam.mobile.fidelio.bean.EngagementContratBean;
import ma.iam.mobile.fidelio.bean.InvoiceRequest;
import ma.iam.mobile.fidelio.bean.ModeEngagementBean;
import ma.iam.mobile.fidelio.bean.ModelePosteBean;
import ma.iam.mobile.fidelio.bean.OpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.OpenInvoicesResponse;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesResponses;
import ma.iam.mobile.fidelio.bean.PaymentResponse;
import ma.iam.mobile.fidelio.bean.TarifBSCSBean;
import ma.iam.mobile.fidelio.bean.TarifPackBSCSBean;
import ma.iam.mobile.fidelio.business.FidelioManagerBiz;
import ma.iam.mobile.fidelio.constants.Constants;
import ma.iam.mobile.fidelio.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.fidelio.controller.FidelioController;
import ma.iam.mobile.fidelio.dao.FidelioDao;
import ma.iam.mobile.fidelio.dao.MobileBSCSDao;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.persist.ContractCustomer;
import ma.iam.mobile.fidelio.persist.ContractPersist;
import ma.iam.mobile.fidelio.persist.InvoicesPersist;
import ma.iam.mobile.fidelio.util.DateUtil;
import ma.iam.mobile.fidelio.util.HelperUtil;
import ma.iam.mobile.fidelio.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "fidelioManagerBiz")
public class FidelioManagerBizImpl implements FidelioManagerBiz {

	@Autowired
	FidelioController fidelioController;

	@Autowired
	private Properties config;

	@Autowired
	MobileBSCSDao mobileBSCSDao;

	@Autowired
	FidelioDao fidelioDao;

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory.getLogger(FidelioManagerBizImpl.class);


	private ContractPersist getContractPersist(String nd) throws FunctionnalException {
		ContractPersist contractPersist = mobileBSCSDao.getContractByDnNum(nd);

		if (contractPersist == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_ND_NOT_FOUND);
		}

		if (contractPersist.getStatus() == null || !contractPersist.getStatus().equalsIgnoreCase("a")) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_ND_NOT_ACTIF);
		}

		// check rateplan
		// if (contractPersist.getRatePlan() == null
		// || !contractPersist.getRatePlan().equals("JC")) {
		// throw new FunctionnalException(
		// ExceptionCodeTypeEnum.PROBLEM_PT_NON_ELEGIBLE);
		// }

		return contractPersist;
	}

	

	@Override
	public List<ModeEngagementBean> getEngagementDisponilbe(String nd) throws FunctionnalException {

		
		fidelioController.verifyND(nd);
		// Check contract
		ContractPersist contractPersist = getContractPersist(nd);

		List<ModeEngagementBean> listEngagement = fidelioDao.getEngagementEnService();
		for (int i = 0; i < listEngagement.size(); i++) {
			if (listEngagement.get(i).getCode().equals(Constants.CODE_ENGAGEMENT_FIDELIO_12M)
					&& (contractPersist.getRatePlan().equals(Constants.FORFAIT_GEOLOC_5_MO)
							|| contractPersist.getRatePlan().equals(Constants.FORFAIT_LIBERTE_ENGAGEMENT))) {
				listEngagement.remove(i);
			}

		}
		return listEngagement;
	}

	@Override
	public List<CategorieArticleBean> getCategorieProduit() throws FunctionnalException {

		return fidelioDao.getCategorieProduit();
	}

	@Override
	public List<ModelePosteBean> getFamillePoste() throws FunctionnalException {
		return fidelioDao.getModelePoste();
	}

	@Override
	public CompteClientBean getSoldePointClient(String nd) throws FunctionnalException {

		// Check ND syntactic
		fidelioController.verifyND(nd);
		// Check contract
		ContractPersist contractPersist = getContractPersist(nd);
		CompteClientBean compteClient = null;
		if (contractPersist.getIdCustomer() != null)
			compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomer());
		if (compteClient == null && contractPersist.getIdCustomer() != null)
			compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomerParent());

		// List<ModeEngagementBean> listEngagement =
		// fidelioDao.getEngagementEnService();

		return compteClient;
	}

	@Override
	public List<CatalogueEngagementBean> getListPack(String nd, String modeEngagementId, String categorieProduitId,
			String famillePosteId) throws FunctionnalException {

		// Check ND syntactic
		fidelioController.verifyND(nd);
		// Check contract
		ContractPersist contractPersist = getContractPersist(nd);
		CompteClientBean compteClient = null;
		if (contractPersist.getIdCustomer() != null)
			compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomer());
		if (compteClient == null && contractPersist.getIdCustomer() != null)
			compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomerParent());

		CategorieClientBean categorieClientBean = fidelioDao.getCategorieClientByQualite(compteClient.getQualiteId());
		// ModeEngagementBean engagement =
		// fidelioDao.getEngagementByCode(codeEngagement);

		List<CatalogueEngagementBean> catalogueEngagement = fidelioDao.getListPack(categorieClientBean.getId(),
				compteClient.getQualiteId().toString(), modeEngagementId, categorieProduitId,
				contractPersist.getTmcode(), famillePosteId,
				contractPersist.getRatePlanDes().equals(Constants.FORFAIT_GEOLOC_5_MO) ? Constants.FORFAIT_GEOLOC_5_MO
						: null);
		// List<ModeEngagementBean> listEngagement =
		// fidelioDao.getEngagementEnService();

		return catalogueEngagement;
	}

	@Override
	public DetailComplementBean getComplement(String nd, String modeEngagementId, String articleId)
			throws FunctionnalException {

		// Check ND syntactic
		fidelioController.verifyND(nd);
		// Check contract
		ContractPersist contractPersist = getContractPersist(nd);
		CompteClientBean compteClient = null;
		if (contractPersist.getIdCustomer() != null)
			compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomer());
		if (compteClient == null && contractPersist.getIdCustomer() != null)
			compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomerParent());

		CategorieClientBean categorieClientBean = fidelioDao.getCategorieClientByQualite(compteClient.getQualiteId());
		// ModeEngagementBean engagement =
		// fidelioDao.getEngagementByCode(codeEngagement);

		CatalogueEngagementBean catalogueEngagement = fidelioDao.getTarifPack(categorieClientBean.getId(),
				compteClient.getQualiteId().toString(), modeEngagementId, null, contractPersist.getTmcode(), null,
				contractPersist.getRatePlanDes().equals(Constants.FORFAIT_GEOLOC_5_MO) ? Constants.FORFAIT_GEOLOC_5_MO
						: null,
				articleId);
		Integer bonus = fidelioDao.getBonusEngagement(categorieClientBean.getId(), modeEngagementId);

		Long prixPtEffectif = catalogueEngagement.getPrixPoints();
		prixPtEffectif -= Math.min(catalogueEngagement.getPrixPoints(), bonus);

		double seuilMinEngagement = fidelioDao.getSeuilMinimalEngagement(categorieClientBean.getId(), modeEngagementId);

		if (compteClient.getSoldeAcquis() < 0L)
			compteClient.setSoldeAcquis(0L);
		if (compteClient.getSoldeEnCours() < 0L)
			compteClient.setSoldeAcquis(0L);

		if (compteClient.getSoldeAcquis() < (seuilMinEngagement * prixPtEffectif)) {
			// erreur
		}

		double taux = fidelioDao.getTauxConversion(categorieClientBean.getId()) / 100.0;

		ArticleBean article = fidelioDao.getArticleById(articleId);

		String custCode = mobileBSCSDao.getCustCodeById(compteClient.getCustomerId());

		ModeEngagementBean modeEnagagement = fidelioDao.getEngagementById(modeEngagementId);
		DetailComplementBean complementDto = null;
		try {
			Long pointsAcquisUtilise = 0L;
			Long pointsEnCoursUtilise = 0L;
			Double complement = this.calculComplement(contractPersist.getTmcode(), custCode,
					compteClient.getCustomerId(), nd, categorieClientBean.getId(), compteClient.getSoldeAcquis(),
					compteClient.getSoldeEnCours(), prixPtEffectif, taux, article, modeEnagagement);
			if (compteClient.getSoldeAcquis() < prixPtEffectif) {
				pointsAcquisUtilise = compteClient.getSoldeAcquis();
				pointsEnCoursUtilise = this.getEncoursUtilise(compteClient.getSoldeAcquis(),
						compteClient.getSoldeEnCours(), prixPtEffectif, taux);
			} else {
				pointsAcquisUtilise = prixPtEffectif;
			}
			complementDto = new DetailComplementBean();
			complementDto.setBonus(bonus);
			complementDto.setPrixEnPoints(catalogueEngagement.getPrixPoints());
			complementDto.setPrixPtEffectif(prixPtEffectif);
			complementDto.setDuree(modeEnagagement.getDureeEnMois());
			complementDto.setComplement(round(complement, 2));
			complementDto.setPointsAcquis(pointsAcquisUtilise);
			complementDto.setPointsEncours(pointsEnCoursUtilise);
		} catch (TechnicalException e) {
			e.printStackTrace();
		}

		return complementDto;
	}

	public static Double round(double d, int n) {
		double result = d;
		int p = (int) Math.pow(10, n);
		result *= p;
		result = Math.round(result);
		result /= p;
		return result;
	}

	private Long getEncoursUtilise(Long soldeAcquis, Long soldeEnCours, Long prixPtEffectif, double taux) {

		Long result = 0L;
		Long plafandEnCours = Double.valueOf(Math.floor(taux * prixPtEffectif)).longValue();

		if (soldeAcquis + soldeEnCours <= prixPtEffectif) {
			result = (soldeEnCours <= plafandEnCours) ? soldeEnCours
					: Math.min(plafandEnCours, (prixPtEffectif - soldeAcquis));
		} else {
			result = plafandEnCours + soldeAcquis <= prixPtEffectif ? plafandEnCours : prixPtEffectif - soldeAcquis;
		}

		return result;
	}

	private Double calculComplement(String planTarifaireId, String custCode, String customerId, String numAppel,
			String categorieClientId, Long soldeAquis, Long soldeEnCours, Long prixCatalogue, double taux,
			ArticleBean article, ModeEngagementBean modeEngagement) throws TechnicalException {

		if (soldeAquis >= prixCatalogue) {
			return 0.0;
		}

		// si Regle de 75 est respectÃ©e alors return 0
		if (((soldeAquis + soldeEnCours) >= prixCatalogue) && ((1 - taux) * prixCatalogue <= soldeAquis)) {
			return 0.0;
		}

		// CA = (PPC â€“ B â€“ SC) * PVP â€“ RCA
		// (Si SC > PPC â€“ B alors CA=0)
		// MRE : Montant de Rachat de lâ€™Engagement
		// PPC : Prix en Point du Cadeau
		// SC : Solde Convertible
		// CA : ComplÃ©ment dâ€™argent
		// PVP : Prix de Vente dâ€™un Point
		// RCA : Remise sur complÃ©ment dâ€™argent
		// B : Bonus rÃ©engagement

		Double soldeConvertible = getSoldeConvertible(soldeAquis, soldeEnCours, prixCatalogue, taux);
		Double chiffreAffaire = (prixCatalogue - soldeConvertible);

		// Convertir le prix en point au prix en DH
		Double prixRachatPoint = fidelioDao.getPrixRachatPointByModeEngagement(modeEngagement.getId());
		chiffreAffaire = chiffreAffaire * prixRachatPoint;

		// CA = prix conquete + frais de mise en service
		Double prixConquete = getPrixConquete(planTarifaireId, categorieClientId, custCode, customerId, numAppel,
				article, modeEngagement);

		return Math.min(chiffreAffaire, prixConquete);

	}

	/**
	 * @param soldeAquis
	 * @param soldeEncours
	 * @param prixTotal
	 * @param taux
	 * @return
	 */
	private Double getSoldeConvertible(Long soldeAquis, Long soldeEncours, Long prixTotal, Double taux) {
		Double result = 0d;
		if (prixTotal <= soldeAquis) {
			result = (prixTotal).doubleValue();
		}

		Double reste = (Double) (prixTotal * taux);
		if (soldeEncours >= reste) {
			result = (soldeAquis).doubleValue() + (reste).doubleValue();
		} else {
			result = (soldeAquis).doubleValue() + soldeEncours.doubleValue();
		}
		if (result > prixTotal) {
			return (prixTotal).doubleValue();
		}
		return result;
	}

	@SuppressWarnings("unlikely-arg-type")
	private double getPrixConquete(String planTarifaireId, String categorieClientId, String custCode, String customerId,
			String numAppel, ArticleBean article, ModeEngagementBean modeEngagement) throws TechnicalException {

		Float result = 0f;

		// Article produit = (Article)
		// rechercheParametreDao.getArticleByCode(engagement.getArticleCode());

		// si la famille du produit est "SGE"
		if (article.getLibelleModelePoste() != null
				&& article.getLibelleModelePoste().trim().equals(Constants.MODEL_POST_SGE)) {
			List<String> listNumAplClient = mobileBSCSDao.getAllNumAppels(customerId);

			if (listNumAplClient.size() > 10) {
				result = 0f;
			} else {

				result = getPrixPack(planTarifaireId, article, modeEngagement);
			}
		}
		// sinon
		else {

			Float prixPack = getPrixPack(planTarifaireId, article, modeEngagement);

			// La remise privilege n est pas accordee dans le cas d un engagement de 12 mois
			// Fidelio
			if (modeEngagement.getDureeEnMois().equals(Constants.MODE_ENGAGEMENT_FIDELIO_12M_DUREE)) {
				result = prixPack;
			} else {

				float seuilRemise = fidelioDao.getPrixPackMinimalPourRemise(categorieClientId);

				result = prixPack - mobileBSCSDao.getRemisePackPrivilege(custCode, numAppel, prixPack, seuilRemise,
						Constants.REMISE_PRIVILEGE);
			}
		}

		return result;
	}

	private Float getPrixPack(String planTarifaireId, ArticleBean article, ModeEngagementBean pModeEngagement)
			throws TechnicalException {
		TarifBSCSBean lTarifBSCS = mobileBSCSDao.getPrixPackXMoisSema(planTarifaireId, article.getCode(),
				pModeEngagement.getDureeEnMois());
		TarifPackBSCSBean lTarifFidelio = fidelioDao.getTarifPackFidelio(article.getCode(), pModeEngagement.getId(),
				planTarifaireId);

		float lPrixGSM = 0f;
		if (lTarifBSCS != null
				&& (lTarifFidelio == null || lTarifBSCS.getDate().compareTo(lTarifFidelio.getDateDebut()) >= 0)) {
			lPrixGSM = lTarifBSCS.getPrix() != null ? lTarifBSCS.getPrix().floatValue() : 0f;
		} else if (lTarifFidelio != null) {
			lPrixGSM = lTarifBSCS.getPrix() != null ? lTarifBSCS.getPrix().floatValue() : 0f;
		}
		return lPrixGSM;
	}

	@Override
	public String doReengagement(String nd, String articleId, String modeEngagementId, Long pointsAcquis,
			Long pointsEncours, Double complement, String imei, String agenceId) throws FunctionnalException {

		if (imei == null || imei.equals("")) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.POI_INVALIDE_IMEI);

			
		}

		if (imei.length() != 15 || (!imei.substring(0, 2).equals("35") && !imei.substring(0, 2).equals("86"))) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.POI_INVALIDE_IMEI);
		}
		try {
			// Check ND syntactic
			fidelioController.verifyND(nd);
			// Check contract
			ContractPersist contractPersist = getContractPersist(nd);

			verifierEngagementEchu(Long.valueOf(contractPersist.getCoId()));
			CompteClientBean compteClient = null;
			if (contractPersist.getIdCustomer() != null)
				compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomer());
			if (compteClient == null && contractPersist.getIdCustomer() != null)
				compteClient = fidelioDao.getCompteClient(contractPersist.getIdCustomerParent());
			AgenceBean agence = fidelioDao.getAgenceById(agenceId);
			ArticleBean article = fidelioDao.getArticleById(articleId);

		

		String custCode = mobileBSCSDao.getCustCodeById(compteClient.getCustomerId());

		String engagementCode = modeEngagementId.equals("3") ? "24MFIDELIO" : "12MFIDELIO";

		int engagementId = modeEngagementId.equals("3") ? 2 : 1;

			String lUniqueIMEI = mobileBSCSDao.addIMEI(Long.parseLong(contractPersist.getCoId()), nd,
					article.getLibelle(), article.getCode(), engagementCode, "FIDEREVWS", "", agence.getLibelle(), "",
					imei);

			if (lUniqueIMEI != null && !lUniqueIMEI.equals("OK")) {
				Object[] params = new Object[1];
				params[0] = lUniqueIMEI;
				throw new FunctionnalException(ExceptionCodeTypeEnum.POI_EXIST_IMEI);
			}
			CommandeIdBean commandeBean = fidelioDao.createOrder(compteClient.getCustomerId(),
					contractPersist.getCoId(), pointsAcquis, pointsEncours, imei, nd, Integer.parseInt(agenceId),
					complement, articleId);

			int result = mobileBSCSDao.affecterLivraison(custCode, Long.parseLong(contractPersist.getCoId()), nd,
					article.getCode(), engagementId, new Date(), "FIDEREVWS", imei,
					Long.parseLong(agence.getCostCenter()), commandeBean.getCommandeId().intValue(), complement > 0.0D,
					complement, "CMPLM", true, 100.0D, "REENM", false);
			int result2 = 1;
			if (result == 0) {
				result2 = fidelioDao.updateSoldeConvertis(commandeBean.getAffaireId().intValue(),
						commandeBean.getCommandeId().intValue(), Integer.parseInt(compteClient.getCustomerId()),
						pointsAcquis, pointsEncours, 1L);
			} else {
				throw new FunctionnalException(ExceptionCodeTypeEnum.POI_ERROR_CMD);
			}
			if (result2 != 0) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.POI_ERROR_CMD);
			}
			return commandeBean.getCommandeId().toString();
		} catch (Exception e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

	}

	@Override
	public List<AgenceBean> getAgences() throws FunctionnalException {
		return fidelioDao.getAgences();
	}

	/**
	 * VÃ©rifier si le contrat a un engagement Ã©chu
	 * 
	 * @param contratId
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	public void verifierEngagementEchu(Long contratId) throws FunctionnalException {

		Date dateDuJour = Calendar.getInstance().getTime();
		Date dateDebut = null;
		ModeEngagementBean modeEngagementBean = null;
		try {
			EngagementContratBean engagementContratBean = mobileBSCSDao.getEngagementEnCours(contratId.longValue());

			modeEngagementBean = fidelioDao.getModeEngagementByBscsId(engagementContratBean.getEngagementId());

			Calendar engCalendar = Calendar.getInstance();
			dateDebut = DateUtil.stringToDate(engagementContratBean.getDateDebut());
			dateDebut = dateDebut != null ? dateDebut : new Date();
			engCalendar.setTime(dateDebut);
			engCalendar.add(Calendar.MONTH, Integer.valueOf(modeEngagementBean.getDureeEnMois()));
			Date dateFin = new Date(engCalendar.getTimeInMillis());

			if (!dateDuJour.after(dateFin)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.POI_CONTRACT_NON_ECHU);
			}
		} catch (Exception e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.POI_CONTRACT_NON_ECHU);
		}

	}

}
