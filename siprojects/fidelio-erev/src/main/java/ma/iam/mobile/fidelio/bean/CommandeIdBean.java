/*
 * @author AtoS
 */
package ma.iam.mobile.fidelio.bean;

public class CommandeIdBean {

	private Long commandeId;

	private Long affaireId;

	public Long getCommandeId() {
		return commandeId;
	}

	public void setCommandeId(Long commandeId) {
		this.commandeId = commandeId;
	}

	public Long getAffaireId() {
		return affaireId;
	}

	public void setAffaireId(Long affaireId) {
		this.affaireId = affaireId;
	}

}
