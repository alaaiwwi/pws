/*
 * @author AtoS
 */

package ma.iam.mobile.fidelio.dao.impl;

import ma.iam.mobile.fidelio.dao.ByPassFidelioDao;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ByPassFidelioDaoImpl extends BaseDAOFidelioImpl implements ByPassFidelioDao {

	@Autowired
	private JDBCPing pingFidelio;

	public boolean modeByPass() {
		return pingFidelio.check();
	}

}
