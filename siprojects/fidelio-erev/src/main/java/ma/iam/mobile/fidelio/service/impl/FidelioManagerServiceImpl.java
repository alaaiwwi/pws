/*
 * @author AtoS
 */
package ma.iam.mobile.fidelio.service.impl;

import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.fidelio.bean.AgenceBean;
import ma.iam.mobile.fidelio.bean.CatalogueEngagementBean;
import ma.iam.mobile.fidelio.bean.CategorieArticleBean;
import ma.iam.mobile.fidelio.bean.CompteClientBean;
import ma.iam.mobile.fidelio.bean.DetailComplementBean;
import ma.iam.mobile.fidelio.bean.ModeEngagementBean;
import ma.iam.mobile.fidelio.bean.ModelePosteBean;
import ma.iam.mobile.fidelio.bean.OpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.OpenInvoicesResponse;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesRequest;
import ma.iam.mobile.fidelio.bean.PaymentOpenInvoicesResponses;
import ma.iam.mobile.fidelio.business.ByPassBscsManagerBiz;
import ma.iam.mobile.fidelio.business.ByPassFidelioManagerBiz;
import ma.iam.mobile.fidelio.business.FidelioManagerBiz;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.service.FidelioManagerService;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component(value = "fidelioManagerService")
public class FidelioManagerServiceImpl extends SpringBeanAutowiringSupport implements FidelioManagerService {

	/** The customer infos manager biz. */
	@Autowired
	private ByPassFidelioManagerBiz byPassFidelioManagerBiz;

	@Autowired
	private ByPassBscsManagerBiz byPassBscsManagerBiz;

	@Autowired
	private FidelioManagerBiz fidelioManagerBiz;

	@Autowired
	private JDBCPing pingFidelio;

	public Integer fidelioModeByPass() {
		return byPassFidelioManagerBiz.modeByPass();
	}

	@Autowired
	private JDBCPing pingMobileBscs;

	public Integer bscsModeByPass() {
		return byPassBscsManagerBiz.modeByPass();
	}

	

	@Override
	public List<ModeEngagementBean> getEngagementDisponilbe(String nd) throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getEngagementDisponilbe(nd);
	}

	@Override
	public List<CategorieArticleBean> getCategorieProduit() throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getCategorieProduit();
	}

	@Override
	public List<ModelePosteBean> getFamillePoste() throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getFamillePoste();
	}

	@Override
	public CompteClientBean getSoldePointClient(String nd) throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getSoldePointClient(nd);
	}

	@Override
	public List<CatalogueEngagementBean> getListPack(String nd, String modeEngagementId, String categorieProduitId,
			String famillePosteId) throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getListPack(nd, modeEngagementId, categorieProduitId, famillePosteId);
	}

	@Override
	public DetailComplementBean getComplement(String nd, String modeEngagementId, String articleId)
			throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getComplement(nd, articleId, modeEngagementId);
	}

	@Override
	public String doReengagement(String nd, String articleId, String modeEngagementId, Long pointsAcquis,
			Long pointsEncours, Double complement,String imei,String agenceId) throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.doReengagement(nd, articleId, modeEngagementId, pointsAcquis, pointsEncours, complement,imei,agenceId);
	}

	@Override
	public List<AgenceBean> getAgences() throws FunctionnalException {
		// TODO Auto-generated method stub
		return fidelioManagerBiz.getAgences();
	}

}
