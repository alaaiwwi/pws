package ma.iam.mobile.fidelio.util;

public class StringUtil {
	
	
    /**
     * test si une chaine de caract�re est vide
     * @param s
     * @return
     */
    public static boolean isEmpty(String s){
    		
    	return (s == null || "".equals(s.trim())) ? Boolean.TRUE
				: Boolean.FALSE;
  
    }
}
