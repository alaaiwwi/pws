package ma.iam.mobile.fidelio.bean;

import java.util.Date;

public class CompteClientBean {

	/** The id client. */
	private Integer idClient;

	/** The solde acquis. */
	private Long soldeAcquis;

	/** The solde en cours. */
	private Long soldeEnCours;

	/** The date maj solde. */
	private Date dateMAJSolde;

	/** The date creation. */
	private Date dateCreation;

	/** The code fidelio. */
	private Integer codeFidelio;

	/** The code fidelio. */
	private Integer qualiteId;
	
	private String customerId;

	/**
	 * Gets the id client.
	 * 
	 * @return the id client
	 */
	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer customerId) {
		this.idClient = customerId;
	}

	public Long getSoldeAcquis() {
		return soldeAcquis;
	}

	public void setSoldeAcquis(Long soldeAcquis) {
		this.soldeAcquis = soldeAcquis;
	}

	public Long getSoldeEnCours() {
		return soldeEnCours;
	}

	public void setSoldeEnCours(Long soldeEnCours) {
		this.soldeEnCours = soldeEnCours;
	}

	public Date getDateMAJSolde() {
		return dateMAJSolde;
	}

	public void setDateMAJSolde(Date dateMAJSolde) {
		this.dateMAJSolde = dateMAJSolde;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Integer getCodeFidelio() {
		return codeFidelio;
	}

	public void setCodeFidelio(Integer codeFidelio) {
		this.codeFidelio = codeFidelio;
	}

	public Integer getQualiteId() {
		return qualiteId;
	}

	public void setQualiteId(Integer qualiteId) {
		this.qualiteId = qualiteId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	

}