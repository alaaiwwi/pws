package ma.iam.wiam.ws.neto.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ParamGetter {

    private static Properties batchProperties = new Properties();

    public static Logger batchLoger = null;
    public static Logger batchLogerTrace = null;

    private static ParamGetter instance = null;

    public ParamGetter() {
	initBatchConfig();
    }

    private boolean initBatchConfig() {
	String propertiesfilename = "src/main/resources/Neto_Config.properties";
	try {
		//this.getClass().getResourceAsStream("Neto_Config.properties");
	    InputStream in = this.getClass().getResourceAsStream("/Neto_Config.properties");
	    batchProperties.load(in);
	    in.close();
	} catch (IOException ioex) {
	    ParamGetter.batchLoger.error(" Exception in initBatchConfig() : " + ioex.getClass() + " "
		    + ioex.getMessage());
	    return false;
	}
	return true;
    }

    /**
     * @return Returns the instance.
     */
    public static ParamGetter getInstance() {
	if (instance == null)
	    instance = new ParamGetter();
	return instance;
    }

    /**
     * @return Returns the batchProperties.
     */
    public Properties getBatchProperties() {
	return batchProperties;
    }
}