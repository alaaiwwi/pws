package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseParameter;



public class Parameter extends BaseParameter {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Parameter () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Parameter (long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Parameter (
		long id,
		java.lang.String paramCode,
		java.lang.String paramType) {

		super (
			id,
			paramCode,
			paramType);
	}

/*[CONSTRUCTOR MARKER END]*/


}