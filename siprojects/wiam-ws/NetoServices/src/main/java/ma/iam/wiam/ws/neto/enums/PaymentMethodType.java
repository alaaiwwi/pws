package ma.iam.wiam.ws.neto.enums;

public enum PaymentMethodType {
	LEVY(1), WICKET(2);
	
	private Integer index;
	
	PaymentMethodType(Integer index) {
		this.index = index;
	}
	
	public Integer getIndex() {
		return index;
	}
}
