package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the CONTACT_ADD_INFO table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="CONTACT_ADD_INFO"
 */

public abstract class BaseContactAddInfo  implements Serializable {

	public static String REF = "ContactAddInfo";
	public static String PROP_VALUE = "Value";
	public static String PROP_ID = "Id";


	// constructors
	public BaseContactAddInfo () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseContactAddInfo (ma.iam.wiam.ws.neto.model.ContactAddInfoPK id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private ma.iam.wiam.ws.neto.model.ContactAddInfoPK id;

	// many to one
	private ma.iam.wiam.ws.neto.model.ContactAddInfoValue value;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     */
	public ma.iam.wiam.ws.neto.model.ContactAddInfoPK getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (ma.iam.wiam.ws.neto.model.ContactAddInfoPK id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: VALUE_ID
	 */
	public ma.iam.wiam.ws.neto.model.ContactAddInfoValue getValue () {
		return value;
	}

	/**
	 * Set the value related to the column: VALUE_ID
	 * @param value the VALUE_ID value
	 */
	public void setValue (ma.iam.wiam.ws.neto.model.ContactAddInfoValue value) {
		this.value = value;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.ContactAddInfo)) return false;
		else {
			ma.iam.wiam.ws.neto.model.ContactAddInfo contactAddInfo = (ma.iam.wiam.ws.neto.model.ContactAddInfo) obj;
			if (null == this.getId() || null == contactAddInfo.getId()) return false;
			else return (this.getId().equals(contactAddInfo.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}