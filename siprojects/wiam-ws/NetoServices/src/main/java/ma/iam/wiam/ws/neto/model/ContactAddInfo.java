package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseContactAddInfo;



public class ContactAddInfo extends BaseContactAddInfo {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public ContactAddInfo () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public ContactAddInfo (ma.iam.wiam.ws.neto.model.ContactAddInfoPK id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}