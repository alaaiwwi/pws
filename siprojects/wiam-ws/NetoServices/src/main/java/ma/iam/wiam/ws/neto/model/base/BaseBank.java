package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * @author y.eddaalous
 */

public abstract class BaseBank  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String ID = "id";

	public static final String CODE = "code";

	public static final String NAME = "name";

	public static final String SHORT_NAME = "shortName";

	public static final String RIB_CONTROL_TYPE = "rIBControlType";

	// constructors
	public BaseBank () {
	}

	/**
	 * Constructor for required fields
	 */
	public BaseBank (String code) {
		this.code = code;
	}

	// fields
	protected Long id;

	protected String code;

	protected String name;

	protected String shortName;

	protected String rIBControlType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getrIBControlType() {
		return rIBControlType;
	}

	public void setrIBControlType(String rIBControlType) {
		this.rIBControlType = rIBControlType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
            return true;
        }
        if (!(o instanceof BaseBank)) {
            return false;
        }

        final BaseBank bank = (BaseBank) o;

        return !(code != null ? !code.equals(bank.getCode()) : bank.getCode() != null);
	}

	@Override
	public int hashCode() {
		return (code != null ? code.hashCode() : 0);
	}

}