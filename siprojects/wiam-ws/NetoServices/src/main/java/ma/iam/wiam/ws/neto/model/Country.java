package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseCountry;



public class Country extends BaseCountry {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Country () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Country (long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Country (
		long id,
		boolean countryDefault) {

		super (
			id,
			countryDefault);
	}

/*[CONSTRUCTOR MARKER END]*/


}