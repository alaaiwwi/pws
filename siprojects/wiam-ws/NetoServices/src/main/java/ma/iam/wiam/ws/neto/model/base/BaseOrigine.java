package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
public abstract class BaseOrigine implements Serializable{

	protected Long origine;
	protected String name;
	
	public long getOrigine() {
		return origine;
	}
	public void setOrigine(Long origine) {
		this.origine = origine;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BaseOrigine() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BaseOrigine(Long origine, String name) {
		super();
		this.origine = origine;
		this.name = name;
	}
	
	
	
	
}
