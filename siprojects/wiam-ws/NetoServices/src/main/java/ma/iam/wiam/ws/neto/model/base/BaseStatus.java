package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
public abstract class BaseStatus implements Serializable{

	
	
	protected Long status;
	protected String name;
	
	public long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BaseStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BaseStatus(Long status, String name) {
		super();
		this.status = status;
		this.name = name;
	}
	
	
	
	
	
}
