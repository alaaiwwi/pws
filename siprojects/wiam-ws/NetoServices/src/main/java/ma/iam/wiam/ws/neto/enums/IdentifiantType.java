package ma.iam.wiam.ws.neto.enums;

public enum IdentifiantType {

	
	CIN("CIN"),
	CARTE_DE_SEJOUR("Carte de séjour"),
	PASSEPORT("Passeport"),
	PERMIS("Permis"),
	NON_DEFINIT("Non défini");
	
	private String identifiant;

	public String getIdentifiant() {
		return identifiant;
	}

	private IdentifiantType(String identifiant) {
		this.identifiant = identifiant;
	}
	
	
}
