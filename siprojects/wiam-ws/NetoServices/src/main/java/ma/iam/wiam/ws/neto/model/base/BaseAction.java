package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
public abstract class BaseAction implements Serializable{

	
	protected Long action;
	protected String name;
	
	public long getAction() {
		return action;
	}
	public void setAction(Long action) {
		this.action = action;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BaseAction() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BaseAction(Long action, String name) {
		super();
		this.action = action;
		this.name = name;
	}
	
	
	
	
	
}
