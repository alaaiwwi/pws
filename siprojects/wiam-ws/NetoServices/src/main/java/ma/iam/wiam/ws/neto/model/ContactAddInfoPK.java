package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseContactAddInfoPK;

public class ContactAddInfoPK extends BaseContactAddInfoPK {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public ContactAddInfoPK () {}
	
	public ContactAddInfoPK (
		Parameter paramId,
		ma.iam.wiam.ws.neto.model.Contact contact) {

		super (
			paramId,
			contact);
	}
/*[CONSTRUCTOR MARKER END]*/


}