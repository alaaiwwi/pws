package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.PQuartierPK;
import ma.iam.wiam.ws.neto.model.PVoiePK;

/**
 * This is an object that contains data related to the P_VOIE table. Do not
 * modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class table="P_VOIE"
 */

public abstract class BasePVoie implements Serializable {

	public static String REF = "PVoie";
	public static String PROP_LVOIE = "Lvoie";
	public static String PROP_ID = "Id";

	// constructors
	public BasePVoie() {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BasePVoie (PVoiePK id) {
		this.setId(id);
		initialize();
	}

	protected void initialize() {
	}

	// fields
	private PVoiePK id;
	private java.lang.String lvoie;

	/**
	 * Return the value associated with the column: LVOIE
	 */
	public java.lang.String getLvoie() {
		return lvoie;
	}

	/**
	 * Set the value related to the column: LVOIE
	 * 
	 * @param lvoie
	 *            the LVOIE value
	 */
	public void setLvoie(java.lang.String lvoie) {
		this.lvoie = lvoie;
	}

	public PVoiePK getId() {
		return id;
	}

	public void setId(PVoiePK id) {
		this.id = id;
	}

	public String toString() {
		return super.toString();
	}

}