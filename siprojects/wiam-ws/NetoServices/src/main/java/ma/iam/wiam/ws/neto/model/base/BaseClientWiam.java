package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

public class BaseClientWiam implements Serializable {

	public static String REF = "ClientWiam";
	public static String PROP_CLIENT_CODE = "StringId";
	public static String PROP_CUSTOMER_ID = "CountryName";
	public static String PROP_AGENT = "CountryDefault";
	public static String PROP_AGENCE = "CountryCode";
	
	
	public BaseClientWiam() {
		super();
	}
	
	private String clientCode;
	private Long customerId;
	private String agent;
	private String agence;
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getAgence() {
		return agence;
	}
	public void setAgence(String agence) {
		this.agence = agence;
	}

}
