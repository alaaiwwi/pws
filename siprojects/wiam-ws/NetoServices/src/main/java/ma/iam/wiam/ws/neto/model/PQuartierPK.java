package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BasePQuartierPK;

public class PQuartierPK extends BasePQuartierPK {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public PQuartierPK () {}
	
	public PQuartierPK (
		java.lang.String ccom,
		java.lang.String cquartier) {

		super (
			ccom,
			cquartier);
	}
/*[CONSTRUCTOR MARKER END]*/


}