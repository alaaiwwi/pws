package ma.iam.wiam.ws.neto.enums;

public enum ActionType {
	CREATE_CUSTOMER, ADD_LEVEL, UPDATE_CUSTOMER_BILLING_ACCOUNT;
}
