package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.PQuartierPK;


public abstract class BasePQuartierPK implements Serializable {

	protected int hashCode = Integer.MIN_VALUE;

	public static final String PROP_CCOM="Ccom";
	public static final String PROP_CQUARTIER="Cquartier";
	
	private java.lang.String ccom;
	private java.lang.String cquartier;


	public BasePQuartierPK () {}
	
	public BasePQuartierPK (
		java.lang.String ccom,
		java.lang.String cquartier) {

		this.setCcom(ccom);
		this.setCquartier(cquartier);
	}


	/**
	 * Return the value associated with the column: CCOM
	 */
	public java.lang.String getCcom () {
		return ccom;
	}

	/**
	 * Set the value related to the column: CCOM
	 * @param ccom the CCOM value
	 */
	public void setCcom (java.lang.String ccom) {
		this.ccom = ccom;
	}



	/**
	 * Return the value associated with the column: CQUARTIER
	 */
	public java.lang.String getCquartier () {
		return cquartier;
	}

	/**
	 * Set the value related to the column: CQUARTIER
	 * @param cquartier the CQUARTIER value
	 */
	public void setCquartier (java.lang.String cquartier) {
		this.cquartier = cquartier;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof PQuartierPK)) return false;
		else {
			PQuartierPK mObj = (PQuartierPK) obj;
			if (null != this.getCcom() && null != mObj.getCcom()) {
				if (!this.getCcom().equals(mObj.getCcom())) {
					return false;
				}
			}
			else {
				return false;
			}
			if (null != this.getCquartier() && null != mObj.getCquartier()) {
				if (!this.getCquartier().equals(mObj.getCquartier())) {
					return false;
				}
			}
			else {
				return false;
			}
			return true;
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			StringBuilder sb = new StringBuilder();
			if (null != this.getCcom()) {
				sb.append(this.getCcom().hashCode());
				sb.append(":");
			}
			else {
				return super.hashCode();
			}
			if (null != this.getCquartier()) {
				sb.append(this.getCquartier().hashCode());
				sb.append(":");
			}
			else {
				return super.hashCode();
			}
			this.hashCode = sb.toString().hashCode();
		}
		return this.hashCode;
	}


}