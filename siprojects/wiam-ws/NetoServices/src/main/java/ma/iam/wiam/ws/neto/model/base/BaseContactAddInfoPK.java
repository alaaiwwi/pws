package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.Parameter;


public abstract class BaseContactAddInfoPK implements Serializable {

	protected int hashCode = Integer.MIN_VALUE;
	
	public static final String PROP_PARAM="ParamId";
	public static final String PROP_CONTACT="Contact";

	private Parameter paramId;
	private ma.iam.wiam.ws.neto.model.Contact contact;


	public BaseContactAddInfoPK () {}
	
	public BaseContactAddInfoPK (
		Parameter paramId,
		ma.iam.wiam.ws.neto.model.Contact contact) {

		this.setParamId(paramId);
		this.setContact(contact);
	}


	/**
	 * Return the value associated with the column: PARAM_ID
	 */
	public Parameter getParamId () {
		return paramId;
	}

	/**
	 * Set the value related to the column: PARAM_ID
	 * @param paramId the PARAM_ID value
	 */
	public void setParamId (Parameter paramId) {
		this.paramId = paramId;
	}



	/**
	 * Return the value associated with the column: CONTACT_ID
	 */
	public ma.iam.wiam.ws.neto.model.Contact getContact () {
		return contact;
	}

	/**
	 * Set the value related to the column: CONTACT_ID
	 * @param contact the CONTACT_ID value
	 */
	public void setContact (ma.iam.wiam.ws.neto.model.Contact contact) {
		this.contact = contact;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.ContactAddInfoPK)) return false;
		else {
			ma.iam.wiam.ws.neto.model.ContactAddInfoPK mObj = (ma.iam.wiam.ws.neto.model.ContactAddInfoPK) obj;
			if (this.getParamId() != mObj.getParamId()) {
				return false;
			}
			if (null != this.getContact() && null != mObj.getContact()) {
				if (!this.getContact().equals(mObj.getContact())) {
					return false;
				}
			}
			else {
				return false;
			}
			return true;
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			StringBuilder sb = new StringBuilder();
			if (null != this.getParamId()) {
				sb.append(this.getParamId().hashCode());
				sb.append(":");
			}
			if (null != this.getContact()) {
				sb.append(this.getContact().hashCode());
				sb.append(":");
			}
			else {
				return super.hashCode();
			}
			this.hashCode = sb.toString().hashCode();
		}
		return this.hashCode;
	}


}