package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the COUNTRY table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="COUNTRY"
 */

public abstract class BaseCountry  implements Serializable {

	public static String REF = "Country";
	public static String PROP_STRING_ID = "StringId";
	public static String PROP_COUNTRY_NAME = "CountryName";
	public static String PROP_COUNTRY_DEFAULT = "CountryDefault";
	public static String PROP_COUNTRY_CODE = "CountryCode";
	public static String PROP_ID = "Id";
	public static String PROP_COUNTRY_LEGACY_ID = "CountryLegacyId";


	// constructors
	public BaseCountry () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCountry (long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseCountry (
		long id,
		boolean countryDefault) {

		this.setId(id);
		this.setCountryDefault(countryDefault);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private long id;

	// fields
	private java.lang.String countryLegacyId;
	private java.lang.String countryName;
	private java.lang.Integer stringId;
	private java.lang.String countryCode;
	private Boolean countryDefault;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="sequence"
     *  column="COUNTRY_ID"
     */
	public long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: COUNTRY_LEGACY_ID
	 */
	public java.lang.String getCountryLegacyId () {
		return countryLegacyId;
	}

	/**
	 * Set the value related to the column: COUNTRY_LEGACY_ID
	 * @param countryLegacyId the COUNTRY_LEGACY_ID value
	 */
	public void setCountryLegacyId (java.lang.String countryLegacyId) {
		this.countryLegacyId = countryLegacyId;
	}



	/**
	 * Return the value associated with the column: COUNTRY_NAME
	 */
	public java.lang.String getCountryName () {
		return countryName;
	}

	/**
	 * Set the value related to the column: COUNTRY_NAME
	 * @param countryName the COUNTRY_NAME value
	 */
	public void setCountryName (java.lang.String countryName) {
		this.countryName = countryName;
	}



	/**
	 * Return the value associated with the column: STRING_ID
	 */
	public java.lang.Integer getStringId () {
		return stringId;
	}

	/**
	 * Set the value related to the column: STRING_ID
	 * @param stringId the STRING_ID value
	 */
	public void setStringId (java.lang.Integer stringId) {
		this.stringId = stringId;
	}



	/**
	 * Return the value associated with the column: COUNTRY_CODE
	 */
	public java.lang.String getCountryCode () {
		return countryCode;
	}

	/**
	 * Set the value related to the column: COUNTRY_CODE
	 * @param countryCode the COUNTRY_CODE value
	 */
	public void setCountryCode (java.lang.String countryCode) {
		this.countryCode = countryCode;
	}



	/**
	 * Return the value associated with the column: COUNTRY_DEFAULT
	 */
	public Boolean isCountryDefault () {
		return countryDefault;
	}

	/**
	 * Set the value related to the column: COUNTRY_DEFAULT
	 * @param countryDefault the COUNTRY_DEFAULT value
	 */
	public void setCountryDefault (Boolean countryDefault) {
		this.countryDefault = countryDefault;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.Country)) return false;
		else {
			ma.iam.wiam.ws.neto.model.Country country = (ma.iam.wiam.ws.neto.model.Country) obj;
			return (this.getId() == country.getId());
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			return (int) this.getId();
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}