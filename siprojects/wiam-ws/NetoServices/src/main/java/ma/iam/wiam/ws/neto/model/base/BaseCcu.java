package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
import java.util.Date;


/**
 * @author y.eddaalous
 */

public abstract class BaseCcu  implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String CODE = "code";

	public static final String BUSINESS_NAME = "businessName";

	public static final String CREATION_DATE = "creationDate";

	public static final String CATEGORY = "category";

	// constructors
	public BaseCcu () {
	}

	/**
	 * Constructor for required fields
	 */
	public BaseCcu (String code) {
		this.code = code;
	}

	// fields
	protected String code;

	protected String businessName;

	protected Date creationDate;

	protected Integer category;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
            return true;
        }
        if (!(o instanceof BaseCcu)) {
            return false;
        }

        final BaseCcu bank = (BaseCcu) o;

        return !(code != null ? !code.equals(bank.getCode()) : bank.getCode() != null);
	}

	@Override
	public int hashCode() {
		return (code != null ? code.hashCode() : 0);
	}

}