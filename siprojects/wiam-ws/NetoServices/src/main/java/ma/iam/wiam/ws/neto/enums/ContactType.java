package ma.iam.wiam.ws.neto.enums;

public enum ContactType {
	LEGAL, BILLING, TECHNICAL, COMMERCIAL;
}
