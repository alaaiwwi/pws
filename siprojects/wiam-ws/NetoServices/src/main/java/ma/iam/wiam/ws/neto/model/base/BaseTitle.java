package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * @author y.eddaalous
 */

public abstract class BaseTitle  implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String ID = "id";
	
	public static final String LEGACY_ID = "legacyId";

	public static final String NAME = "name";

	public static final String DEFAULT_TITLE = "defaultTitle";

	public static final String SEX = "sex";

	// constructors
	public BaseTitle () {
	}

	/**
	 * Constructor for required fields
	 */
	public BaseTitle (Long id) {
		this.id = id;
	}

	// fields
	protected Long id;
	
	protected String legacyId;

	protected String name;

	protected Integer defaultTitle;

	protected String sex;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLegacyId() {
		return legacyId;
	}

	public void setLegacyId(String legacyId) {
		this.legacyId = legacyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDefaultTitle() {
		return defaultTitle;
	}

	public void setDefaultTitle(Integer defaultTitle) {
		this.defaultTitle = defaultTitle;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
            return true;
        }
        if (!(o instanceof BaseTitle)) {
            return false;
        }

        final BaseTitle title = (BaseTitle) o;

        return !(id != null ? !id.equals(title.getId()) : title.getId() != null);
	}

	@Override
	public int hashCode() {
		return (id != null ? id.hashCode() : 0);
	}

}