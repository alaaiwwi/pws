package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.Contact;


/**
 * This is an object that contains data related to the CONTACT table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="CONTACT"
 */

public abstract class BaseContact  implements Serializable {

	public static String REF = "Contact";
	public static String PROP_ADD_ADDRESS2 = "AddAddress2";
	public static String PROP_SEX = "Sex";
	public static String PROP_STATE = "State";
	public static String PROP_COUNTRY_ID = "CountryId";
	public static String PROP_ZIP_CODE = "ZipCode";
	public static String PROP_ADD_ADDRESS1 = "AddAddress1";
	public static String PROP_CITY = "City";
	public static String PROP_IS_CORPORATE = "IsCorporate";
	public static String PROP_STREET_NUMBER = "StreetNumber";
	public static String PROP_FAX_NUMBER = "FaxNumber";
	public static String PROP_TEL_NUMBER2 = "TelNumber2";
	public static String PROP_EMAIL = "Email";
	public static String PROP_TITLE_ID = "TitleId";
	public static String PROP_FIRST_NAME = "FirstName";
	public static String PROP_TEL_NUMBER = "TelNumber";
	public static String PROP_BUSINESS_NAME = "BusinessName";
	public static String PROP_STREET_ADDRESS = "StreetAddress";
	public static String PROP_ADD_ADDRESS3 = "AddAddress3";
	public static String PROP_ID = "Id";
	public static String PROP_LAST_NAME = "LastName";


	// constructors
	public BaseContact () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseContact (long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseContact (
		long id,
		boolean isCorporate) {

		this.setId(id);
		this.setIsCorporate(isCorporate);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private long id;

	// fields
	private boolean isCorporate;
	private java.lang.String businessName;
	private java.lang.String sex;
	private Long titleId;
	private java.lang.String firstName;
	private java.lang.String lastName;
	private java.lang.String streetNumber;
	private java.lang.String streetAddress;
	private java.lang.String addAddress1;
	private java.lang.String addAddress2;
	private java.lang.String addAddress3;
	private java.lang.String zipCode;
	private java.lang.String city;
	private java.lang.String state;
	private Long countryId;
	private java.lang.String telNumber;
	private java.lang.String telNumber2;
	private java.lang.String faxNumber;
	private java.lang.String email;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="sequence"
     *  column="CONTACT_ID"
     */
	public long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: IS_CORPORATE
	 */
	public boolean isIsCorporate () {
		return isCorporate;
	}

	/**
	 * Set the value related to the column: IS_CORPORATE
	 * @param isCorporate the IS_CORPORATE value
	 */
	public void setIsCorporate (boolean isCorporate) {
		this.isCorporate = isCorporate;
	}



	/**
	 * Return the value associated with the column: BUSINESS_NAME
	 */
	public java.lang.String getBusinessName () {
		return businessName;
	}

	/**
	 * Set the value related to the column: BUSINESS_NAME
	 * @param businessName the BUSINESS_NAME value
	 */
	public void setBusinessName (java.lang.String businessName) {
		this.businessName = businessName;
	}



	/**
	 * Return the value associated with the column: SEX
	 */
	public java.lang.String getSex () {
		return sex;
	}

	/**
	 * Set the value related to the column: SEX
	 * @param sex the SEX value
	 */
	public void setSex (java.lang.String sex) {
		this.sex = sex;
	}



	/**
	 * Return the value associated with the column: TITLE_ID
	 */
	public Long getTitleId () {
		return titleId;
	}

	/**
	 * Set the value related to the column: TITLE_ID
	 * @param titleId the TITLE_ID value
	 */
	public void setTitleId (Long titleId) {
		this.titleId = titleId;
	}



	/**
	 * Return the value associated with the column: FIRST_NAME
	 */
	public java.lang.String getFirstName () {
		return firstName;
	}

	/**
	 * Set the value related to the column: FIRST_NAME
	 * @param firstName the FIRST_NAME value
	 */
	public void setFirstName (java.lang.String firstName) {
		this.firstName = firstName;
	}



	/**
	 * Return the value associated with the column: LAST_NAME
	 */
	public java.lang.String getLastName () {
		return lastName;
	}

	/**
	 * Set the value related to the column: LAST_NAME
	 * @param lastName the LAST_NAME value
	 */
	public void setLastName (java.lang.String lastName) {
		this.lastName = lastName;
	}



	/**
	 * Return the value associated with the column: STREET_NUMBER
	 */
	public java.lang.String getStreetNumber () {
		return streetNumber;
	}

	/**
	 * Set the value related to the column: STREET_NUMBER
	 * @param streetNumber the STREET_NUMBER value
	 */
	public void setStreetNumber (java.lang.String streetNumber) {
		this.streetNumber = streetNumber;
	}



	/**
	 * Return the value associated with the column: STREET_ADDRESS
	 */
	public java.lang.String getStreetAddress () {
		return streetAddress;
	}

	/**
	 * Set the value related to the column: STREET_ADDRESS
	 * @param streetAddress the STREET_ADDRESS value
	 */
	public void setStreetAddress (java.lang.String streetAddress) {
		this.streetAddress = streetAddress;
	}



	/**
	 * Return the value associated with the column: ADD_ADDRESS_1
	 */
	public java.lang.String getAddAddress1 () {
		return addAddress1;
	}

	/**
	 * Set the value related to the column: ADD_ADDRESS_1
	 * @param addAddress1 the ADD_ADDRESS_1 value
	 */
	public void setAddAddress1 (java.lang.String addAddress1) {
		this.addAddress1 = addAddress1;
	}



	/**
	 * Return the value associated with the column: ADD_ADDRESS_2
	 */
	public java.lang.String getAddAddress2 () {
		return addAddress2;
	}

	/**
	 * Set the value related to the column: ADD_ADDRESS_2
	 * @param addAddress2 the ADD_ADDRESS_2 value
	 */
	public void setAddAddress2 (java.lang.String addAddress2) {
		this.addAddress2 = addAddress2;
	}



	/**
	 * Return the value associated with the column: ADD_ADDRESS_3
	 */
	public java.lang.String getAddAddress3 () {
		return addAddress3;
	}

	/**
	 * Set the value related to the column: ADD_ADDRESS_3
	 * @param addAddress3 the ADD_ADDRESS_3 value
	 */
	public void setAddAddress3 (java.lang.String addAddress3) {
		this.addAddress3 = addAddress3;
	}



	/**
	 * Return the value associated with the column: ZIP_CODE
	 */
	public java.lang.String getZipCode () {
		return zipCode;
	}

	/**
	 * Set the value related to the column: ZIP_CODE
	 * @param zipCode the ZIP_CODE value
	 */
	public void setZipCode (java.lang.String zipCode) {
		this.zipCode = zipCode;
	}



	/**
	 * Return the value associated with the column: CITY
	 */
	public java.lang.String getCity () {
		return city;
	}

	/**
	 * Set the value related to the column: CITY
	 * @param city the CITY value
	 */
	public void setCity (java.lang.String city) {
		this.city = city;
	}



	/**
	 * Return the value associated with the column: STATE
	 */
	public java.lang.String getState () {
		return state;
	}

	/**
	 * Set the value related to the column: STATE
	 * @param state the STATE value
	 */
	public void setState (java.lang.String state) {
		this.state = state;
	}



	/**
	 * Return the value associated with the column: COUNTRY_ID
	 */
	public Long getCountryId () {
		return countryId;
	}

	/**
	 * Set the value related to the column: COUNTRY_ID
	 * @param countryId the COUNTRY_ID value
	 */
	public void setCountryId (Long countryId) {
		this.countryId = countryId;
	}



	/**
	 * Return the value associated with the column: TEL_NUMBER
	 */
	public java.lang.String getTelNumber () {
		return telNumber;
	}

	/**
	 * Set the value related to the column: TEL_NUMBER
	 * @param telNumber the TEL_NUMBER value
	 */
	public void setTelNumber (java.lang.String telNumber) {
		this.telNumber = telNumber;
	}



	/**
	 * Return the value associated with the column: TEL_NUMBER_2
	 */
	public java.lang.String getTelNumber2 () {
		return telNumber2;
	}

	/**
	 * Set the value related to the column: TEL_NUMBER_2
	 * @param telNumber2 the TEL_NUMBER_2 value
	 */
	public void setTelNumber2 (java.lang.String telNumber2) {
		this.telNumber2 = telNumber2;
	}



	/**
	 * Return the value associated with the column: FAX_NUMBER
	 */
	public java.lang.String getFaxNumber () {
		return faxNumber;
	}

	/**
	 * Set the value related to the column: FAX_NUMBER
	 * @param faxNumber the FAX_NUMBER value
	 */
	public void setFaxNumber (java.lang.String faxNumber) {
		this.faxNumber = faxNumber;
	}



	/**
	 * Return the value associated with the column: EMAIL
	 */
	public java.lang.String getEmail () {
		return email;
	}

	/**
	 * Set the value related to the column: EMAIL
	 * @param email the EMAIL value
	 */
	public void setEmail (java.lang.String email) {
		this.email = email;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof Contact)) return false;
		else {
			Contact contact = (Contact) obj;
			return (this.getId() == contact.getId());
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			return (int) this.getId();
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}