package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the  table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table=""
 */

public abstract class BaseClient  implements Serializable {

	public static String REF = "Client";
	public static String PROP_ADRESSE = "adresse";
	public static String PROP_NUM_CLIENT = "numClient";
	public static String PROP_BILLING_CONTACT = "billingContact";
	public static String PROP_NOM_CLIENT = "nomClient";
	public static String PROP_LEGAL_CONTACT = "legalContact";
	public static String PROP_ORG_TYPE_NAME = "orgTypeName";
	public static String PROP_ID = "id";


	// constructors
	public BaseClient () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseClient (java.lang.Integer id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Integer id;

	// fields
	private java.lang.Integer legalContact;
	private java.lang.Integer billingContact;
	private java.lang.String orgTypeName;
	private java.lang.String numClient;
	private java.lang.String adresse;
	private java.lang.String nomClient;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  column="id"
     */
	public java.lang.Integer getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Integer id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: legalContact
	 */
	public java.lang.Integer getLegalContact () {
		return legalContact;
	}

	/**
	 * Set the value related to the column: legalContact
	 * @param legalContact the legalContact value
	 */
	public void setLegalContact (java.lang.Integer legalContact) {
		this.legalContact = legalContact;
	}



	/**
	 * Return the value associated with the column: billingContact
	 */
	public java.lang.Integer getBillingContact () {
		return billingContact;
	}

	/**
	 * Set the value related to the column: billingContact
	 * @param billingContact the billingContact value
	 */
	public void setBillingContact (java.lang.Integer billingContact) {
		this.billingContact = billingContact;
	}



	/**
	 * Return the value associated with the column: orgTypeName
	 */
	public java.lang.String getOrgTypeName () {
		return orgTypeName;
	}

	/**
	 * Set the value related to the column: orgTypeName
	 * @param orgTypeName the orgTypeName value
	 */
	public void setOrgTypeName (java.lang.String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}



	/**
	 * Return the value associated with the column: numClient
	 */
	public java.lang.String getNumClient () {
		return numClient;
	}

	/**
	 * Set the value related to the column: numClient
	 * @param numClient the numClient value
	 */
	public void setNumClient (java.lang.String numClient) {
		this.numClient = numClient;
	}



	/**
	 * Return the value associated with the column: adresse
	 */
	public java.lang.String getAdresse () {
		return adresse;
	}

	/**
	 * Set the value related to the column: adresse
	 * @param adresse the adresse value
	 */
	public void setAdresse (java.lang.String adresse) {
		this.adresse = adresse;
	}



	/**
	 * Return the value associated with the column: nomClient
	 */
	public java.lang.String getNomClient () {
		return nomClient;
	}

	/**
	 * Set the value related to the column: nomClient
	 * @param nomClient the nomClient value
	 */
	public void setNomClient (java.lang.String nomClient) {
		this.nomClient = nomClient;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.Client)) return false;
		else {
			ma.iam.wiam.ws.neto.model.Client client = (ma.iam.wiam.ws.neto.model.Client) obj;
			if (null == this.getId() || null == client.getId()) return false;
			else return (this.getId().equals(client.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}