package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseCcu;

public class Ccu extends BaseCcu {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Ccu () {
		super();
	}

	/**
	 * Constructor for required fields
	 */
	public Ccu (String code) {
		super (code);
	}

/*[CONSTRUCTOR MARKER END]*/
}