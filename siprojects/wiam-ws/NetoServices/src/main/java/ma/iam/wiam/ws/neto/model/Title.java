package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseTitle;

public class Title extends BaseTitle {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Title () {
		super();
	}

	/**
	 * Constructor for required fields
	 */
	public Title (Long id) {
		super (id);
	}

/*[CONSTRUCTOR MARKER END]*/
}