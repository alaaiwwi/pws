package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.Bank;


/**
 * @author y.eddaalous
 */

public abstract class BaseBankAgency  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String ID = "id";

	public static final String CODE = "code";

	public static final String NAME = "name";
	
	public static final String BANK = "bank";

	// constructors
	public BaseBankAgency () {
	}

	/**
	 * Constructor for required fields
	 */
	public BaseBankAgency (String code) {
		this.code = code;
	}

	// fields
	private Long id;

    private String code;

    private String name;
    
    private Bank bank;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
            return true;
        }
        if (!(o instanceof BaseBankAgency)) {
            return false;
        }

        final BaseBankAgency agency = (BaseBankAgency) o;

        return !(code != null ? !code.equals(agency.getCode()) : agency.getCode() != null);
	}

	@Override
	public int hashCode() {
		return (code != null ? code.hashCode() : 0);
	}

}