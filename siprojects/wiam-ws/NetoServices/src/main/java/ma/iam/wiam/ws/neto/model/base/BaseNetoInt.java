package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ma.iam.wiam.ws.neto.model.Action;
import ma.iam.wiam.ws.neto.model.Origine;
import ma.iam.wiam.ws.neto.model.Status;
import oracle.sql.CLOB;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
public abstract class BaseNetoInt implements Serializable{
private static final long serialVersionUID = 1L;
	
	private int hashCode = Integer.MIN_VALUE;
	
	protected Long requestID;
	protected Origine origine; 
	protected Status status;
	protected Action action;
	protected Long priority;
	protected Date insertionDate ;
	protected Date statusUpdateDate;
	protected String inputData;
	protected String errorMessage;
	protected String outputData;
	protected Long parentRequestID;
	protected String additionalInfo;
	protected Long coID;
	protected Long customerID;
	protected Long destID;
	protected String userName;
	protected Date entryDate;
	protected String requestMode;
	
	public Long getRequestID() {
		return requestID;
	}
	public void setRequestID(Long requestID) {
		this.requestID = requestID;
		this.hashCode = Integer.MIN_VALUE;
	}
	public Origine getOrigine() {
		return origine;
	}
	public void setOrigine(Origine origine) {
		this.origine = origine;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Action getAction() {
		return action;
	}
	public void setAction(Action action) {
		this.action = action;
	}
	public Long getPriority() {
		return priority;
	}
	public void setPriority(Long priority) {
		this.priority = priority;
	}
	public Date getInsertionDate() {
		return insertionDate;
	}
	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = insertionDate;
	}
	public Date getStatusUpdateDate() {
		return statusUpdateDate;
	}
	public void setStatusUpdateDate(Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}
	public String getInputData() {
		return inputData;
	}
	public void setInputData(String inputData) {
		this.inputData = inputData;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getOutputData() {
		return outputData;
	}
	public void setOutputData(String outputData) {
		this.outputData = outputData;
	}
	public Long getParentRequestID() {
		return parentRequestID;
	}
	public void setParentRequestID(Long parentRequestID) {
		this.parentRequestID = parentRequestID;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public Long getCoID() {
		return coID;
	}
	public void setCoID(Long coID) {
		this.coID = coID;
	}
	public Long getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}
	public Long getDestID() {
		return destID;
	}
	public void setDestID(Long destID) {
		this.destID = destID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	public String getRequestMode() {
		return requestMode;
	}
	public void setRequestMode(String requestMode) {
		this.requestMode = requestMode;
	}
	public BaseNetoInt(Long requestID, Origine origine, Status status, Action action, Long priority, Date insertionDate,
			Date statusUpdateDate, String inputData, String errorMessage, String outputData, Long parentRequestID,
			String additionalInfo, Long coID, Long customerID, Long destID, String userName, Date entryDate,
			String requestMode) {
		super();
		this.requestID = requestID;
		this.origine = origine;
		this.status = status;
		this.action = action;
		this.priority = priority;
		this.insertionDate = insertionDate;
		this.statusUpdateDate = statusUpdateDate;
		this.inputData = inputData;
		this.errorMessage = errorMessage;
		this.outputData = outputData;
		this.parentRequestID = parentRequestID;
		this.additionalInfo = additionalInfo;
		this.coID = coID;
		this.customerID = customerID;
		this.destID = destID;
		this.userName = userName;
		this.entryDate = entryDate;
		this.requestMode = requestMode;
		initialize ();
	}
	
	protected void initialize () {}
	
	
	public BaseNetoInt() {
		super();
		initialize ();
	}
	
	public BaseNetoInt(java.lang.Long id) {
		this.setRequestID(id);
		initialize ();
	}
	
	
	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.NetoInt)) return false;
		else {
			ma.iam.wiam.ws.neto.model.NetoInt netoint = (ma.iam.wiam.ws.neto.model.NetoInt) obj;
			return (this.getRequestID() == netoint.getRequestID());
		}
	}
	
	
	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getRequestID())
				return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getRequestID().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}
	public BaseNetoInt(Long requestID, Origine origine, Status status, Action action, Long priority, Date insertionDate,
			Date statusUpdateDate, String inputData, Date entryDate) {
		super();
		this.requestID = requestID;
		this.origine = origine;
		this.status = status;
		this.action = action;
		this.priority = priority;
		this.insertionDate = insertionDate;
		this.statusUpdateDate = statusUpdateDate;
		this.inputData = inputData;
		this.entryDate = entryDate;
		initialize ();
	}
	
	
	
	
	
	
}
