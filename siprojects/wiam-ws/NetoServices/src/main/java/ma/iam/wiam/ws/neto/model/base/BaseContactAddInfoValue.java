package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.ContactAddInfoValue;


/**
 * This is an object that contains data related to the CONTACT_ADD_INFO_VALUE table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="CONTACT_ADD_INFO_VALUE"
 */

public abstract class BaseContactAddInfoValue  implements Serializable {

	public static String REF = "ContactAddInfoValue";
	public static String PROP_VALUE_DATETIME = "ValueDatetime";
	public static String PROP_PARAM_ID = "ParamId";
	public static String PROP_VALUE_INTEGER = "ValueInteger";
	public static String PROP_VALUE_TYPE = "ValueType";
	public static String PROP_CHOICE_ITEM_ID = "ChoiceItemId";
	public static String PROP_VALUE_STRING = "ValueString";
	public static String PROP_VALUE_BOOLEAN = "ValueBoolean";
	public static String PROP_VALUE_FLOAT = "ValueFloat";
	public static String PROP_ID = "Id";


	// constructors
	public BaseContactAddInfoValue () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseContactAddInfoValue (Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseContactAddInfoValue (
		Long id,
		java.lang.String valueType) {

		this.setId(id);
		this.setValueType(valueType);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private Long id;

	// fields
	private java.lang.String valueType;
	private java.lang.String valueString;
	private Long valueFloat;
	private Long valueInteger;
	private Long valueBoolean;
	private java.util.Date valueDatetime;
	private Long choiceItemId;
	private Long paramId;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="sequence"
     *  column="VALUE_ID"
     */
	public Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: VALUE_TYPE
	 */
	public java.lang.String getValueType () {
		return valueType;
	}

	/**
	 * Set the value related to the column: VALUE_TYPE
	 * @param valueType the VALUE_TYPE value
	 */
	public void setValueType (java.lang.String valueType) {
		this.valueType = valueType;
	}



	/**
	 * Return the value associated with the column: VALUE_STRING
	 */
	public java.lang.String getValueString () {
		return valueString;
	}

	/**
	 * Set the value related to the column: VALUE_STRING
	 * @param valueString the VALUE_STRING value
	 */
	public void setValueString (java.lang.String valueString) {
		this.valueString = valueString;
	}



	/**
	 * Return the value associated with the column: VALUE_FLOAT
	 */
	public Long getValueFloat () {
		return valueFloat;
	}

	/**
	 * Set the value related to the column: VALUE_FLOAT
	 * @param valueFloat the VALUE_FLOAT value
	 */
	public void setValueFloat (Long valueFloat) {
		this.valueFloat = valueFloat;
	}



	/**
	 * Return the value associated with the column: VALUE_INTEGER
	 */
	public Long getValueInteger () {
		return valueInteger;
	}

	/**
	 * Set the value related to the column: VALUE_INTEGER
	 * @param valueInteger the VALUE_INTEGER value
	 */
	public void setValueInteger (Long valueInteger) {
		this.valueInteger = valueInteger;
	}



	/**
	 * Return the value associated with the column: VALUE_BOOLEAN
	 */
	public Long getValueBoolean () {
		return valueBoolean;
	}

	/**
	 * Set the value related to the column: VALUE_BOOLEAN
	 * @param valueBoolean the VALUE_BOOLEAN value
	 */
	public void setValueBoolean (Long valueBoolean) {
		this.valueBoolean = valueBoolean;
	}



	/**
	 * Return the value associated with the column: VALUE_DATETIME
	 */
	public java.util.Date getValueDatetime () {
		return valueDatetime;
	}

	/**
	 * Set the value related to the column: VALUE_DATETIME
	 * @param valueDatetime the VALUE_DATETIME value
	 */
	public void setValueDatetime (java.util.Date valueDatetime) {
		this.valueDatetime = valueDatetime;
	}



	/**
	 * Return the value associated with the column: CHOICE_ITEM_ID
	 */
	public Long getChoiceItemId () {
		return choiceItemId;
	}

	/**
	 * Set the value related to the column: CHOICE_ITEM_ID
	 * @param choiceItemId the CHOICE_ITEM_ID value
	 */
	public void setChoiceItemId (Long choiceItemId) {
		this.choiceItemId = choiceItemId;
	}



	/**
	 * Return the value associated with the column: PARAM_ID
	 */
	public Long getParamId () {
		return paramId;
	}

	/**
	 * Set the value related to the column: PARAM_ID
	 * @param paramId the PARAM_ID value
	 */
	public void setParamId (Long paramId) {
		this.paramId = paramId;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ContactAddInfoValue)) return false;
		else {
			ContactAddInfoValue contactAddInfoValue = (ContactAddInfoValue) obj;
			return (this.getId() == contactAddInfoValue.getId());
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			return (int) this.getId().intValue();
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}