package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BasePVoiePK;

public class PVoiePK extends BasePVoiePK {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public PVoiePK() {
	}

	public PVoiePK(java.lang.String ccom, java.lang.String cquartier, String cvoie) {

		super(ccom, cquartier, cvoie);
	}
	/* [CONSTRUCTOR MARKER END] */

}