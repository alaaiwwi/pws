package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseContact;



public class Contact extends BaseContact {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Contact () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Contact (long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Contact (
		long id,
		boolean isCorporate) {

		super (
			id,
			isCorporate);
	}

/*[CONSTRUCTOR MARKER END]*/


}