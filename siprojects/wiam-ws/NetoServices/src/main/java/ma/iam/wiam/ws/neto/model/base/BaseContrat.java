package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**feature/Rechercher_Contrat_Client*/
public abstract class BaseContrat implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nd_login;
	private String rateplan;
	private String rateplanCode;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNd_login() {
		return nd_login;
	}
	public void setNd_login(String nd_login) {
		this.nd_login = nd_login;
	}
	public String getRateplan() {
		return rateplan;
	}
	public void setRateplan(String rateplan) {
		this.rateplan = rateplan;
	}
	public String getRateplanCode() {
		return rateplanCode;
	}
	public void setRateplanCode(String rateplanCode) {
		this.rateplanCode = rateplanCode;
	}
	public BaseContrat() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BaseContrat(Long id, String nd_login, String rateplan, String rateplanCode) {
		super();
		this.id = id;
		this.nd_login = nd_login;
		this.rateplan = rateplan;
		this.rateplanCode = rateplanCode;
	}
	
	
	
	

}
