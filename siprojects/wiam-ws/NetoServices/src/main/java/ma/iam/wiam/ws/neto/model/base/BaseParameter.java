package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

/**
 * This is an object that contains data related to the PARAMETER table. Do not
 * modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class table="PARAMETER"
 */

public abstract class BaseParameter implements Serializable {

	

	/**
	 * Constantes parameter
	 */

	/**
	 * @return the first name of the contact
	 */
	public static String FIRST_NAME_CONTACT = "CONTACT_NOM_1";

	/**
	 * @return the last name of the contact
	 */
	public static String SECOND_NAME_CONTACT = "CONTACT_NOM_2";

	/**
	 * @return the batiment of the contact
	 */
	public static String BATIMENT_CONTACT = "CONTACT_BATIMENT";

	/**
	 * @return the etage of the contact
	 */
	public static String ETAGE_CONTACT = "CONTACT_ETAGE";

	/**
	 * @return the porte of the contact
	 */
	public static String PORTE_CONTACT = "CONTACT_PORTE";

	/**
	 * @return the type of distribution of the contact
	 */
	public static String DIST_TYPE_CONTACT = "CONTACT_TYPE_DIST";

	/**
	 * @return the boite postale of the contact
	 */
	public static String BP_CONTACT = "CONTACT_BP";

	/**
	 * @return the escalier of the contact
	 */
	public static String ESCALIER_CONTACT = "CONTACT_ESCALIER";

	/**
	 * @return the adresseEtranger of the contact
	 */
	public static String ADR_ETRANGER_CONTACT = "CONTACT_ADR_ETRANGER";

	/**
	 * @return the complement adresse of the contact
	 */
	public static String COMPL_ADR_CONTACT = "CONTACT_COMPL_ADR";

	/**
	 * @return the identifier of the contact
	 */
	public static String IDENTIFIANT_CONTACT = "CONTACT_IDENTIFIANT";

	/**
	 * @return the identifier date obtention of the contact
	 */
	public static String IDENT_DATE_CONTACT = "CONTACT_IDENT_DATE";

	/**
	 * @return the identifier lieu obtention of the contact
	 */
	public static String IDENT_LIEU_CONTACT = "CONTACT_IDENT_LIEU";

	/**
	 * @return the identifier type for a grand publique customer of the contact
	 */
	public static String GP_TYPE_IDENT_CONTACT = "CONTACT_GP_TYPE_IDENT";

	/**
	 * @return the identifier type for a professionel customer of the contact
	 */
	public static String PRF_TYPE_IDENT_CONTACT = "CONTACT_PRF_TYPE_IDENT";

	/**
	 * @return the identifier type for a entreprise customer of the contact
	 */
	public static String ENT_TYPE_IDENT_CONTACT = "CONTACT_ENT_TYPE_IDENT";

	/**
	 * @return the numero patente of the contact
	 */
	public static String PATENTE_CONTACT = "CONTACT_PATENTE";
	/**
	 * @return the sigle of the contact
	 */
	public static String SIGLE_CONTACT = "CONTACT_SIGLE";

	/**
	 * @return the date anniversaire of the contact
	 */
	public static String ANNIVERSAIRE_CONTACT = "CONTACT_ANNIVERSAIRE";

	/**
	 * @return the profession of the contact
	 */
	public static String JOB_CONTACT = "CONTACT_JOB";


	/**
	 * @return the sexe of the contact
	 */
	public static String PRO_GP_SEX_CONTACT = "CONTACT_PRO_GP_SEX";

	/**
	 * @return the etat civil of the contact
	 */
	public static String PRO_GP_ETAT_CIVILE_CONTACT = "CONTACT_PRO_GP_ETAT_CIVL";

	/**
	 * @return the commune etrangere
	 */
	public static String ETRG_COMMUNE_CONTACT= "CONTACT_ETRG_COMMUNE";

	
	/**
	 * fin de constantes parameter
	 */



	public static String REF = "Parameter";
	public static String PROP_STRING_ID = "StringId";
	public static String PROP_PARAM_SHORT_DESC_STRING_ID = "ParamShortDescStringId";
	public static String PROP_PARAM_MIN_ITEMS = "ParamMinItems";
	public static String PROP_PARAM_NAME = "ParamName";
	public static String PROP_PARAM_DESCRIPTION = "ParamDescription";
	public static String PROP_PARAM_MAX_ITEMS = "ParamMaxItems";
	public static String PROP_PARAM_TYPE = "ParamType";
	public static String PROP_PARAM_CODE = "ParamCode";
	public static String PROP_PARAM_SHORT_DESCRIPTION = "ParamShortDescription";
	public static String PROP_PARAM_LEGACY_ID = "ParamLegacyId";
	public static String PROP_PARAM_MAX = "ParamMax";
	public static String PROP_PARAM_DESC_STRING_ID = "ParamDescStringId";
	public static String PROP_PARAM_PATTERN = "ParamPattern";
	public static String PROP_PARAM_MIN = "ParamMin";
	public static String PROP_ID = "Id";


	// constructors
	public BaseParameter () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseParameter (long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseParameter (
		long id,
		java.lang.String paramCode,
		java.lang.String paramType) {

		this.setId(id);
		this.setParamCode(paramCode);
		this.setParamType(paramType);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private long id;

	// fields
	private java.lang.String paramCode;
	private Long paramDescStringId;
	private java.lang.String paramDescription;
	private java.lang.String paramLegacyId;
	private java.lang.String paramMax;
	private Long paramMaxItems;
	private java.lang.String paramMin;
	private Long paramMinItems;
	private java.lang.String paramName;
	private java.lang.String paramPattern;
	private Long paramShortDescStringId;
	private java.lang.String paramShortDescription;
	private java.lang.String paramType;
	private Long stringId;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="sequence"
     *  column="PARAM_ID"
     */
	public long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: PARAM_CODE
	 */
	public java.lang.String getParamCode () {
		return paramCode;
	}

	/**
	 * Set the value related to the column: PARAM_CODE
	 * @param paramCode the PARAM_CODE value
	 */
	public void setParamCode (java.lang.String paramCode) {
		this.paramCode = paramCode;
	}



	/**
	 * Return the value associated with the column: PARAM_DESC_STRING_ID
	 */
	public Long getParamDescStringId () {
		return paramDescStringId;
	}

	/**
	 * Set the value related to the column: PARAM_DESC_STRING_ID
	 * @param paramDescStringId the PARAM_DESC_STRING_ID value
	 */
	public void setParamDescStringId (Long paramDescStringId) {
		this.paramDescStringId = paramDescStringId;
	}



	/**
	 * Return the value associated with the column: PARAM_DESCRIPTION
	 */
	public java.lang.String getParamDescription () {
		return paramDescription;
	}

	/**
	 * Set the value related to the column: PARAM_DESCRIPTION
	 * @param paramDescription the PARAM_DESCRIPTION value
	 */
	public void setParamDescription (java.lang.String paramDescription) {
		this.paramDescription = paramDescription;
	}



	/**
	 * Return the value associated with the column: PARAM_LEGACY_ID
	 */
	public java.lang.String getParamLegacyId () {
		return paramLegacyId;
	}

	/**
	 * Set the value related to the column: PARAM_LEGACY_ID
	 * @param paramLegacyId the PARAM_LEGACY_ID value
	 */
	public void setParamLegacyId (java.lang.String paramLegacyId) {
		this.paramLegacyId = paramLegacyId;
	}



	/**
	 * Return the value associated with the column: PARAM_MAX
	 */
	public java.lang.String getParamMax () {
		return paramMax;
	}

	/**
	 * Set the value related to the column: PARAM_MAX
	 * @param paramMax the PARAM_MAX value
	 */
	public void setParamMax (java.lang.String paramMax) {
		this.paramMax = paramMax;
	}



	/**
	 * Return the value associated with the column: PARAM_MAX_ITEMS
	 */
	public Long getParamMaxItems () {
		return paramMaxItems;
	}

	/**
	 * Set the value related to the column: PARAM_MAX_ITEMS
	 * @param paramMaxItems the PARAM_MAX_ITEMS value
	 */
	public void setParamMaxItems (Long paramMaxItems) {
		this.paramMaxItems = paramMaxItems;
	}



	/**
	 * Return the value associated with the column: PARAM_MIN
	 */
	public java.lang.String getParamMin () {
		return paramMin;
	}

	/**
	 * Set the value related to the column: PARAM_MIN
	 * @param paramMin the PARAM_MIN value
	 */
	public void setParamMin (java.lang.String paramMin) {
		this.paramMin = paramMin;
	}



	/**
	 * Return the value associated with the column: PARAM_MIN_ITEMS
	 */
	public Long getParamMinItems () {
		return paramMinItems;
	}

	/**
	 * Set the value related to the column: PARAM_MIN_ITEMS
	 * @param paramMinItems the PARAM_MIN_ITEMS value
	 */
	public void setParamMinItems (Long paramMinItems) {
		this.paramMinItems = paramMinItems;
	}



	/**
	 * Return the value associated with the column: PARAM_NAME
	 */
	public java.lang.String getParamName () {
		return paramName;
	}

	/**
	 * Set the value related to the column: PARAM_NAME
	 * @param paramName the PARAM_NAME value
	 */
	public void setParamName (java.lang.String paramName) {
		this.paramName = paramName;
	}



	/**
	 * Return the value associated with the column: PARAM_PATTERN
	 */
	public java.lang.String getParamPattern () {
		return paramPattern;
	}

	/**
	 * Set the value related to the column: PARAM_PATTERN
	 * @param paramPattern the PARAM_PATTERN value
	 */
	public void setParamPattern (java.lang.String paramPattern) {
		this.paramPattern = paramPattern;
	}



	/**
	 * Return the value associated with the column: PARAM_SHORT_DESC_STRING_ID
	 */
	public Long getParamShortDescStringId () {
		return paramShortDescStringId;
	}

	/**
	 * Set the value related to the column: PARAM_SHORT_DESC_STRING_ID
	 * @param paramShortDescStringId the PARAM_SHORT_DESC_STRING_ID value
	 */
	public void setParamShortDescStringId (Long paramShortDescStringId) {
		this.paramShortDescStringId = paramShortDescStringId;
	}



	/**
	 * Return the value associated with the column: PARAM_SHORT_DESCRIPTION
	 */
	public java.lang.String getParamShortDescription () {
		return paramShortDescription;
	}

	/**
	 * Set the value related to the column: PARAM_SHORT_DESCRIPTION
	 * @param paramShortDescription the PARAM_SHORT_DESCRIPTION value
	 */
	public void setParamShortDescription (java.lang.String paramShortDescription) {
		this.paramShortDescription = paramShortDescription;
	}



	/**
	 * Return the value associated with the column: PARAM_TYPE
	 */
	public java.lang.String getParamType () {
		return paramType;
	}

	/**
	 * Set the value related to the column: PARAM_TYPE
	 * @param paramType the PARAM_TYPE value
	 */
	public void setParamType (java.lang.String paramType) {
		this.paramType = paramType;
	}



	/**
	 * Return the value associated with the column: STRING_ID
	 */
	public Long getStringId () {
		return stringId;
	}

	/**
	 * Set the value related to the column: STRING_ID
	 * @param stringId the STRING_ID value
	 */
	public void setStringId (Long stringId) {
		this.stringId = stringId;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.Parameter)) return false;
		else {
			ma.iam.wiam.ws.neto.model.Parameter parameter = (ma.iam.wiam.ws.neto.model.Parameter) obj;
			return (this.getId() == parameter.getId());
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			return (int) this.getId();
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}



}