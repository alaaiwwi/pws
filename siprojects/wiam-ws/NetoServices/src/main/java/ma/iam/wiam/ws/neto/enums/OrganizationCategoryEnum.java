package ma.iam.wiam.ws.neto.enums;

public enum OrganizationCategoryEnum {
	GRAND_PUBLIC, PROFESSIONNEL, ENTREPRISE;
}
