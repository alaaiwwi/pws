package ma.iam.wiam.ws.neto.model;

import java.util.Date;
import java.util.List;

import ma.iam.wiam.ws.neto.model.base.BaseAction;
import ma.iam.wiam.ws.neto.model.base.BaseNetoInt;
import ma.iam.wiam.ws.neto.model.base.BaseOrigine;
import ma.iam.wiam.ws.neto.model.base.BaseStatus;
import oracle.sql.CLOB;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
public class NetoInt extends BaseNetoInt{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NetoInt() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NetoInt(Long requestID, Origine origine, Status status, Action action, Long priority, Date insertionDate,
			Date statusUpdateDate, String inputData, String errorMessage, String outputData, Long parentRequestID,
			String additionalInfo, Long coID, Long customerID, Long destID, String userName, Date entryDate,
			String requestMode) {
		super(requestID, origine, status, action, priority, insertionDate, statusUpdateDate, inputData, errorMessage,
				outputData, parentRequestID, additionalInfo, coID, customerID, destID, userName, entryDate, requestMode);
		// TODO Auto-generated constructor stub
	}

	public NetoInt(Long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public NetoInt(Long requestID, Origine origine, Status status, Action action, Long priority, Date insertionDate,
			Date statusUpdateDate, String inputData, Date entryDate) {
		super(requestID, origine, status, action, priority, insertionDate, statusUpdateDate, inputData, entryDate);
		// TODO Auto-generated constructor stub
	}
	
	
}
