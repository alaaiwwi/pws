package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
import java.util.Date;


/**
 * @author y.eddaalous
 */

public abstract class BaseUser  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String ID = "id";

	public static final String LOGIN = "login";

	public static final String LEGACY_ID = "legacyId";
	
	public static final String PASSWORD = "password";
	
	public static final String ENABLED = "enabled";
	
	public static final String CREATION_DATE = "creationDate";

	// constructors
	public BaseUser () {
	}

	// fields
	private Long id;

    private String login;
    
    private String legacyId;

    private String password;
    
    private Boolean enabled;
    
    private Date creationDate;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLegacyId() {
		return legacyId;
	}

	public void setLegacyId(String legacyId) {
		this.legacyId = legacyId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
            return true;
        }
        if (!(o instanceof BaseUser)) {
            return false;
        }

        final BaseUser user = (BaseUser) o;

        return !(login != null ? !login.equals(user.getLogin()) : user.getLogin() != null);
	}

	@Override
	public int hashCode() {
		return (login != null ? login.hashCode() : 0);
	}

}