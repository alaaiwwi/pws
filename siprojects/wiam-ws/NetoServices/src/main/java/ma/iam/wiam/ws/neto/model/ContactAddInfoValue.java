package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseContactAddInfoValue;



public class ContactAddInfoValue extends BaseContactAddInfoValue {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public ContactAddInfoValue () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public ContactAddInfoValue (long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public ContactAddInfoValue (
		long id,
		java.lang.String valueType) {

		super (
			id,
			valueType);
	}

/*[CONSTRUCTOR MARKER END]*/


}