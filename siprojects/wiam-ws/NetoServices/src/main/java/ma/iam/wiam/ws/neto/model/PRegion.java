package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BasePRegion;



public class PRegion extends BasePRegion {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public PRegion () {
		super();
	}

	/**
	 * Constructor for required fields
	 */
	public PRegion (
		java.lang.Integer cregion) {

		super (
			cregion);
	}

/*[CONSTRUCTOR MARKER END]*/


}