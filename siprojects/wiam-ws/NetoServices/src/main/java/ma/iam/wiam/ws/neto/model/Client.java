package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseClient;

public class Client {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public Client() {
		super();
	}

	/**
	 * Constructor for primary key
	 */

	private Long id;

	// fields
	private java.lang.Long legalContact;
	private java.lang.Long billingContact;
	private java.lang.String orgTypeName;
	private java.lang.String numClient;
	private java.lang.String adresse;
	private java.lang.String nomClient;
	/* [CONSTRUCTOR MARKER END] */

	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public java.lang.Long getLegalContact() {
		return legalContact;
	}

	public void setLegalContact(java.lang.Long legalContact) {
		this.legalContact = legalContact;
	}

	public java.lang.Long getBillingContact() {
		return billingContact;
	}

	public void setBillingContact(java.lang.Long billingContact) {
		this.billingContact = billingContact;
	}

	public java.lang.String getOrgTypeName() {
		return orgTypeName;
	}

	public void setOrgTypeName(java.lang.String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}

	public java.lang.String getNumClient() {
		return numClient;
	}

	public void setNumClient(java.lang.String numClient) {
		this.numClient = numClient;
	}

	public java.lang.String getAdresse() {
		return adresse;
	}

	public void setAdresse(java.lang.String adresse) {
		this.adresse = adresse;
	}

	public java.lang.String getNomClient() {
		return nomClient;
	}

	public void setNomClient(java.lang.String nomClient) {
		this.nomClient = nomClient;
	}

}