package ma.iam.wiam.ws.neto.enums;

public enum NiveauType {
	VOIX_DATA("T"), INTERNET("I"), NIVEAU("N");
	
	private String type;
	
	private NiveauType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
