package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseBank;



public class Bank extends BaseBank {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Bank () {
		super();
	}

	/**
	 * Constructor for required fields
	 */
	public Bank (String code) {
		super (code);
	}

/*[CONSTRUCTOR MARKER END]*/
}