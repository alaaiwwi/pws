package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the P_REGION table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="P_REGION"
 */

public abstract class BasePRegion  implements Serializable {

	public static String REF = "PRegion";
	public static String PROP_LREGION = "Lregion";
	public static String PROP_CREGION = "Cregion";


	// constructors
	public BasePRegion () {
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BasePRegion (
		java.lang.Integer cregion) {

		this.setCregion(cregion);
		initialize();
	}

	protected void initialize () {}



	// fields
	private java.lang.Integer cregion;
	private java.lang.String lregion;






	/**
	 * Return the value associated with the column: CREGION
	 */
	public java.lang.Integer getCregion () {
		return cregion;
	}

	/**
	 * Set the value related to the column: CREGION
	 * @param cregion the CREGION value
	 */
	public void setCregion (java.lang.Integer cregion) {
		this.cregion = cregion;
	}



	/**
	 * Return the value associated with the column: LREGION
	 */
	public java.lang.String getLregion () {
		return lregion;
	}

	/**
	 * Set the value related to the column: LREGION
	 * @param lregion the LREGION value
	 */
	public void setLregion (java.lang.String lregion) {
		this.lregion = lregion;
	}







	public String toString () {
		return super.toString();
	}


}