package ma.iam.wiam.ws.neto.session.test;

import static org.junit.Assert.fail;
import ma.iam.wiam.ws.neto.exception.ServiceException;
import ma.iam.wiam.ws.neto.session.NetoSessionFactory;

import org.junit.Assert;
import org.junit.Test;

import com.netonomy.blm.api.utils.SessionF;

public class NetoSessionFactoryTest {

    @Test
    public void testGetInstance() {
	SessionF session = null;
	try {
	    session = NetoSessionFactory.getInstance();
	    Assert.assertNotNull("Initialisation de la session est effectu�e avec succ�s", session);
	} catch (ServiceException e) {
	    fail("Can not initialize the session");
	    e.printStackTrace();
	}

    }

}
