package ma.iam.wiam.dao.impl;

import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.OrigineDao;
import ma.iam.wiam.ws.neto.model.Origine;

/**<!-- @todo feature/Ajouter_Neto_Int -->**/
@Repository
public class OrigineDaoImpl extends HibernateGenericDaoImpl<Origine, Long> implements OrigineDao{

	public OrigineDaoImpl() {
		super(Origine.class);
	}
}
