package ma.iam.wiam.netoapi.dao;

import java.util.List;

import com.netonomy.blm.interfaces.contact.TitleIF;

public interface TitlesDao extends BaseNetoDao{
	
	List<TitleIF> getAllTitles();

}
