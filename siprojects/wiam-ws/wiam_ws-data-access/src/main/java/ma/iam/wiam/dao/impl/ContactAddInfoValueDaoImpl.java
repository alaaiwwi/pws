package ma.iam.wiam.dao.impl;

import java.util.List;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.ContactAddInfoValueDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.ContactAddInfoValue;

@Repository
public class ContactAddInfoValueDaoImpl extends HibernateGenericDaoImpl<ContactAddInfoValue, Long>
		implements ContactAddInfoValueDao {

	public ContactAddInfoValueDaoImpl() {
		super(ContactAddInfoValue.class);
	}

	public ContactAddInfoValue getContactAddInfoValueById(Long id) throws TechnicalException {
		try {
			Criteria criteria = getPersistenceManager().createCriteria(ContactAddInfoValue.class);
			criteria.add(Restrictions.eq(ContactAddInfoValue.PROP_ID, id));
			List<ContactAddInfoValue> list = criteria.list();
			return (CollectionUtils.isEmpty(list)) ? null : list.get(0);
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

}
