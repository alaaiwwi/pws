package ma.iam.wiam.dao;

import ma.iam.wiam.ws.neto.model.User;

public interface UserDao extends HibernateGenericDao<User, Long> {
	
}
