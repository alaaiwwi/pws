package ma.iam.wiam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.BankDao;
import ma.iam.wiam.ws.neto.model.Bank;

@Repository
public class BankDaoImpl extends HibernateGenericDaoImpl<Bank, Long> implements BankDao {

	public BankDaoImpl() {
		super(Bank.class);
	}
	@SuppressWarnings("unchecked")
	public List<Bank> getBanksNotInCodesList(List<String> codes)  {
		try{
		Criteria criteria = super.getPersistenceManager().createCriteria (Bank.class);
			criteria
			.add(Restrictions
					.not(Restrictions.in(Bank.CODE, codes)))
			.addOrder(Order.asc(Bank.NAME));
		List<Bank> banks = criteria.list();
		return banks;
		}catch (Throwable e){
			return  null;

		}
	}
}
