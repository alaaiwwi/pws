package ma.iam.wiam.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.CcuDao;
import ma.iam.wiam.ws.neto.model.Ccu;

@Repository
public class CcuDaoImpl extends HibernateGenericDaoImpl<Ccu, String> implements CcuDao {
	public CcuDaoImpl() {
		super(Ccu.class);
	}
}
