package ma.iam.wiam.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.netonomy.blm.api.Hierarchy.LevelF;

import ma.iam.wiam.dao.LevelDao;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class LevelDaoImpl extends HibernateGenericDaoImpl<LevelF, Long> implements LevelDao {

	public LevelDaoImpl() {
		super(LevelF.class);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllMTAgenciesInRegie() {
		try{
		return super.getPersistenceManager().getNamedQuery("getAllMTAgenciesInRegie",null);
		}catch(Throwable e){
			return  null;
		}
	}

	public List<Object[]> getSmartFactOrganizations() {
		try {
			return (List<Object[]>) super.getPersistenceManager().getNamedQuery("getSmartFactOrganizations",null);
		} catch (Throwable e) {
			return null;
		}
	}
}
