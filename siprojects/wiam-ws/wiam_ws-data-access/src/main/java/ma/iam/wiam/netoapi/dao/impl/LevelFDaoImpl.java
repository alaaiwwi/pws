package ma.iam.wiam.netoapi.dao.impl;

import org.springframework.stereotype.Repository;

import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.util.ObjectId;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.LevelFDao;

@Repository
public class LevelFDaoImpl extends BaseNetoDAOImpl implements LevelFDao {

	public LevelF get(String id) throws TechnicalException {
		return new LevelF(this.getCurrentSession(), ObjectId.instantiate(id));
	}
	
}