package ma.iam.wiam.dao;

import java.util.List;

import com.netonomy.blm.api.Hierarchy.LevelF;

public interface LevelDao extends HibernateGenericDao<LevelF, Long> {
	
	List<Object[]> getAllMTAgenciesInRegie();
	
	List<Object[]> getSmartFactOrganizations();
	
}
