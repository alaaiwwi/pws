package ma.iam.wiam.dao;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.ContactAddInfoValue;

public interface ContactAddInfoValueDao extends HibernateGenericDao<ContactAddInfoValue, Long> {

	ContactAddInfoValue getContactAddInfoValueById(Long id) throws TechnicalException;

}
