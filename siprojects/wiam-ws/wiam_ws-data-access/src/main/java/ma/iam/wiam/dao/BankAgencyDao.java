package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.ws.neto.model.BankAgency;

public interface BankAgencyDao extends HibernateGenericDao<BankAgency, Long> {
	
	List<BankAgency> getAgenciesByCodeBank(String bankCode);
	
	List<BankAgency> getAgencyByCodeAndCodeBank(String code, String bankCode);
}
