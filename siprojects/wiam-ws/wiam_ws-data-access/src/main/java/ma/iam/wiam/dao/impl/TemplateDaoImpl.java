package ma.iam.wiam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ma.iam.wiam.dao.TemplateDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Template;

/**@todo : feature/Template**/
@Repository
@Transactional
public class TemplateDaoImpl extends HibernateGenericDaoImpl<Template, Long> implements TemplateDao{

	public TemplateDaoImpl() {
		super(ma.iam.wiam.ws.neto.model.Template.class);
	}

	
}
