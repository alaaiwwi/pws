package ma.iam.wiam.netoapi.dao;

import java.util.List;

import com.netonomy.blm.api.Contract.ContractF;
import com.netonomy.blm.util.BlmSecurityException;

import ma.iam.wiam.exceptions.TechnicalException;

public interface ContractDao extends BaseNetoDao {
	
	ContractF getContractById(Long Id) throws BlmSecurityException, TechnicalException;
}
