package ma.iam.wiam.dao;

import ma.iam.wiam.netoapi.dao.BaseNetoDao;

public interface ByPassDao extends BaseNetoDao {
	public boolean modeByPassBscsNeto();
}
