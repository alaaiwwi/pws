package ma.iam.wiam.dao.impl;

import java.util.List;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.CommuneDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PCommune;

@Repository
public class CommuneDaoImpl extends HibernateGenericDaoImpl<PCommune, String> implements CommuneDao {

	public CommuneDaoImpl() {
		super(PCommune.class);

	}

	public List<PCommune> getCommuneList() throws TechnicalException {
		try {
			Criteria criteria = getPersistenceManager().createCriteria(PCommune.class);
			criteria.addOrder(Order.desc(PCommune.PROP_CREGION));
			@SuppressWarnings("unchecked")
			List<PCommune> listCommunes = criteria.list();
			return listCommunes;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

	}

	@SuppressWarnings("unchecked")
	public PCommune getCommuneByCode(String code) throws TechnicalException {
		try{
			Criteria criteria = getPersistenceManager().createCriteria(PCommune.class);
			criteria
			.add(Restrictions.eq(PCommune.PROP_CCOM, code));
			List<PCommune> listCommunes =criteria.list();
			return CollectionUtils.isNotEmpty(listCommunes) ? listCommunes.get(0) : null;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

}
