package ma.iam.wiam.dao.impl;

import java.util.List;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.CountryDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Country;;

@Repository
public class CountryDaoImpl extends HibernateGenericDaoImpl<Country, Long> implements CountryDao {

	public CountryDaoImpl() {
		super(Country.class);

	}
	public Country getCountryById(Long id) throws TechnicalException {
		try{
			Criteria criteria = getPersistenceManager().createCriteria(Country.class);
			criteria.add(Restrictions.eq(Country.PROP_ID, id));
			List<Country> list = criteria.list();
			return (CollectionUtils.isNotEmpty(list)) ? list.get(0) : null;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

}
