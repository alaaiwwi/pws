package ma.iam.wiam.dao.impl;

import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.ActionDao;
import ma.iam.wiam.ws.neto.model.Action;

/**<!-- @todo feature/Ajouter_Neto_Int -->**/
@Repository
public class ActionDaoImpl extends HibernateGenericDaoImpl<Action, Long> implements ActionDao{

	public ActionDaoImpl() {
		super(Action.class);
	}
}
