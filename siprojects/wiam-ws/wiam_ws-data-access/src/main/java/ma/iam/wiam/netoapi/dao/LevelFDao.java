package ma.iam.wiam.netoapi.dao;

import com.netonomy.blm.api.Hierarchy.LevelF;

import ma.iam.wiam.exceptions.TechnicalException;

public interface LevelFDao extends BaseNetoDao{
	
	LevelF get(String id) throws TechnicalException;

}
