package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Contact;
import ma.iam.wiam.ws.neto.model.ContactAddInfo;
import ma.iam.wiam.ws.neto.model.ContactAddInfoPK;

public interface ContactAddInfoDao extends HibernateGenericDao<ContactAddInfo, ContactAddInfoPK> {

	List<ContactAddInfo> getContactAddInfoByContactAndCodeParams(Contact contact, List<String> params)
			throws TechnicalException;

}
