package ma.iam.wiam.dao.impl;

import java.util.List;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.ContactAddInfoDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Contact;
import ma.iam.wiam.ws.neto.model.ContactAddInfo;
import ma.iam.wiam.ws.neto.model.ContactAddInfoPK;
import ma.iam.wiam.ws.neto.model.Parameter;

@Repository
public class ContactAddInfoDaoImpl extends HibernateGenericDaoImpl<ContactAddInfo, ContactAddInfoPK>
		implements ContactAddInfoDao {

	public ContactAddInfoDaoImpl() {
		super(ContactAddInfo.class);
	}

	public List<ContactAddInfo> getContactAddInfoByContactAndCodeParams(Contact contact, List<String> params)
			throws TechnicalException {
	try{
		Criteria cParam = getPersistenceManager().createCriteria(Parameter.class);
		cParam.add(Restrictions.in(Parameter.PROP_PARAM_CODE, params));


		Criteria criteria =getPersistenceManager().createCriteria(ContactAddInfo.class);
		criteria.add(Restrictions.eq(ContactAddInfo.PROP_ID + "." + ContactAddInfoPK.PROP_CONTACT, contact));
		criteria.add(Restrictions.in(ContactAddInfo.PROP_ID + "." + ContactAddInfoPK.PROP_PARAM,
				cParam.list()));
		List<ContactAddInfo> list = criteria.list();
		return list;
	}catch(Throwable e){
		throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
	}
	}

}
