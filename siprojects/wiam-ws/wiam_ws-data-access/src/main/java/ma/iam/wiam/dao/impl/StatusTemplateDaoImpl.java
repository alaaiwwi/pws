package ma.iam.wiam.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ma.iam.wiam.dao.StatusTemplateDao;
import ma.iam.wiam.ws.neto.model.StatusTemplate;
/**@todo : feature/Template**/
@Repository
@Transactional
public class StatusTemplateDaoImpl extends HibernateGenericDaoImpl<StatusTemplate, Long> implements StatusTemplateDao{
	public StatusTemplateDaoImpl() {
		super(ma.iam.wiam.ws.neto.model.StatusTemplate.class);
	}

}
