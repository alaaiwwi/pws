package ma.iam.wiam.dao;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import java.io.Serializable;
import java.util.Collection;

public interface HibernateGenericDao <T, PK extends Serializable> {

    /**
     * R�cup�re un objet suivant un identifiant donn�.
     * 
     * @param id L'identifiant de l'objet recherch�
     * @return L'objet recherch�
     */
    T getById(PK id) throws TechnicalException;

    /**
     * R�cup�re tous les objets d'un type particulier.
     * 
     * @return Liste de objets
     */
    Collection<T> getAll()throws TechnicalException;

    /**
     * Cr�ation d'un objet.
     * 
     * @param object L'objet � sauvegarder
     * @return L'identifiant de l'objet sauv�
     */
    void save(T object) throws TechnicalException;

    /**
     * Cr�ation ou mise � jour d'un objet.
     * 
     * @param object L'objet cr�� ou modifi�.
     */
    void saveOrUpdate(T object) throws TechnicalException;

    /**
     * Supprime un objet.
     * 
     * @param object L'objet � supprimer.
     */
    void remove(T object) throws TechnicalException;

    /**
     * Supprime un objet.
     * 
     * @param id Identifiant de l'objet � supprimer
     */
    void remove(PK id) throws TechnicalException;

    /**
     * Retourne le type de l'entit� manipul�
     * 
     * @return le type de l'entit� manipul�
     */
    Class getType();
}
