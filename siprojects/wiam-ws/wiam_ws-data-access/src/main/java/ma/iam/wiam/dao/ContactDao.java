package ma.iam.wiam.dao;

import ma.iam.wiam.ws.neto.model.Contact;

public interface ContactDao extends HibernateGenericDao<Contact, Long> {

}
