package ma.iam.wiam.dao.impl;

import java.util.List;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.QuartierDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PQuartier;
import ma.iam.wiam.ws.neto.model.PQuartierPK;;

@Repository
public class QuartierDaoImpl extends HibernateGenericDaoImpl<PQuartier, PQuartierPK> implements QuartierDao {

	public QuartierDaoImpl() {
		super(PQuartier.class);

	}

	@SuppressWarnings("unchecked")
	public List<PQuartier> listQuartierByCodeCommune(String codeCommune) throws TechnicalException {
		try {
			Criteria criteria = super.getPersistenceManager().createCriteria(PQuartier.class);
			criteria.add(Restrictions.eq(PQuartier.PROP_ID + "." + PQuartierPK.PROP_CCOM, codeCommune));
			List<PQuartier> list = criteria.list();
			return list;
		}catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

	}

	@SuppressWarnings("unchecked")
	public PQuartier 	getQuartierByCodeCommuneAndCodeQuartier(String codeCommune, String codeQuartier)
			throws TechnicalException {
		try {
			Criteria criteria = super.getPersistenceManager().createCriteria(PQuartier.class);
			criteria.add(Restrictions.eq(PQuartier.PROP_ID + "." + PQuartierPK.PROP_CCOM, codeCommune))
			.add(Restrictions.eq(PQuartier.PROP_ID + "." + PQuartierPK.PROP_CQUARTIER, codeQuartier));
			List<PQuartier> pQuartiers = criteria.list();
			return CollectionUtils.isNotEmpty(pQuartiers) ? pQuartiers.get(0) : null;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

	}
}
