package ma.iam.wiam.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ma.iam.wiam.dao.NetoIntDao;
import ma.iam.wiam.ws.neto.model.NetoInt;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
@Repository
@Transactional
public class NetoIntDaoImpl extends HibernateGenericDaoImpl<NetoInt, Long> implements NetoIntDao{
	public NetoIntDaoImpl() {
		super(NetoInt.class);
	}
}
