package ma.iam.wiam.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.UserDao;
import ma.iam.wiam.ws.neto.model.User;

@Repository
public class UserDaoImpl extends HibernateGenericDaoImpl<User, Long> implements UserDao {

	public UserDaoImpl() {
		super(User.class);
	}

}
