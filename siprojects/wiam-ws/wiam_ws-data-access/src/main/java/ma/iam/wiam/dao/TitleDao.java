package ma.iam.wiam.dao;


import ma.iam.wiam.ws.neto.model.Title;

public interface TitleDao extends HibernateGenericDao<Title, Long> {
}
