package ma.iam.wiam.netoapi.dao;

import java.util.List;

import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

public interface ValueChoiceItemDao extends BaseNetoDao {
	
	List<ValueChoiceItemIF> getValueChoiceItems(String paramCode);
}
