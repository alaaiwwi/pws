package ma.iam.wiam.netoapi.dao.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.parameters.ValueChoiceIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.util.ParameterIF;

import ma.iam.wiam.netoapi.dao.ValueChoiceItemDao;

@Repository
public class ValueChoiceItemDaoImpl extends BaseNetoDAOImpl implements ValueChoiceItemDao {

	public List<ValueChoiceItemIF> getValueChoiceItems(String paramCode) {
		ParameterIF parameter = null;
		ValueChoiceItemIF[] valueChoiceItems = null;

		parameter = ObjectRefMgr.getParameterByCode(paramCode);
		if (parameter instanceof ValueChoiceIF) {
			valueChoiceItems = ((ValueChoiceIF) parameter).getItems();
		}
		return Arrays.asList(valueChoiceItems);
	}
}
