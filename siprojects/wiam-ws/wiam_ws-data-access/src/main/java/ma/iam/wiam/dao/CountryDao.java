package ma.iam.wiam.dao;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Country;

public interface CountryDao extends HibernateGenericDao<Country, Long> {

	Country getCountryById(Long id) throws TechnicalException;

}
