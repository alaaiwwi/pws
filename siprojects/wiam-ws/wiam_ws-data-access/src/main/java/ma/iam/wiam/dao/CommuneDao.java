package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PCommune;

public interface CommuneDao extends HibernateGenericDao<PCommune, String> {
	
	List<PCommune> getCommuneList() throws  TechnicalException;
	PCommune getCommuneByCode(String code) throws  TechnicalException;

}
