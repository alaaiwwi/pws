package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.ws.neto.model.Bank;

public interface BankDao extends HibernateGenericDao<Bank, Long> {
	
	List<Bank> getBanksNotInCodesList(List<String> codes);
}
