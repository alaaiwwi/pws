/*
 * @author AtoS
 */
package ma.iam.wiam.netoapi.dao.impl;

import javax.annotation.PostConstruct;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.WIAM;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.exception.ServiceException;
import ma.iam.wiam.ws.neto.session.NetoSessionFactory;

import org.springframework.stereotype.Repository;

import com.netonomy.blm.api.utils.SessionF;
import com.netonomy.util.SessionInterface;
import com.netonomy.util.SessionManager;

/**
 * The Class .
 */

@Repository("baseNetoDAOImpl")
public class BaseNetoDAOImpl implements BaseNetoDao {

	SessionF session;

	@PostConstruct
	private void init() throws TechnicalException {
		WIAM.InitConfigFile();
		WIAM.getAllProperties();
		try {
			session = NetoSessionFactory.getInstance();
			SessionManager.addCurrentSession(session);
		} catch (ServiceException e) {

			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	public SessionF getSession() {
		return session;
	}

	public void setSession(SessionF session) {
		this.session = session;
	}

	public Boolean sessionByPass() {
		try {
			return session != null;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	public SessionF getCurrentSession() throws TechnicalException {
		if (session != null) {
			SessionInterface it = SessionManager.tryToGetCurrentSession();
			if (it == null)
				SessionManager.addCurrentSession(session);
			return session;
		} else {
			try {
				session = NetoSessionFactory.getInstance();
				return session;
			} catch (ServiceException e) {

				throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			}
		}
	}

}
