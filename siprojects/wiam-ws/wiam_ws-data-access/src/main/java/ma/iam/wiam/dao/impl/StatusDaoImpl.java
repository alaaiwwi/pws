package ma.iam.wiam.dao.impl;

import org.springframework.stereotype.Repository;
import ma.iam.wiam.dao.StatusDao;

/**<!-- @todo feature/Ajouter_Neto_Int -->**/
@Repository
public class StatusDaoImpl extends HibernateGenericDaoImpl<ma.iam.wiam.ws.neto.model.Status, Long> implements StatusDao{
	public StatusDaoImpl() {
		super(ma.iam.wiam.ws.neto.model.Status.class);
	}
}
