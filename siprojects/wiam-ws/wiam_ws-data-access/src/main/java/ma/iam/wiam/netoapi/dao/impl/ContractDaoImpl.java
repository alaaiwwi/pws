package ma.iam.wiam.netoapi.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.netonomy.blm.api.Contract.ContractF;
import com.netonomy.blm.util.BlmSecurityException;
import com.netonomy.util.LongId;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.ContractDao;

@Repository
public class ContractDaoImpl extends BaseNetoDAOImpl implements ContractDao {

	public ContractF getContractById(Long Id) throws BlmSecurityException, TechnicalException {
		
		return new ContractF(getCurrentSession(), new LongId(Id));
	}
}
