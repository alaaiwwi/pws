/*
 * @author AtoS
 */
package ma.iam.wiam.netoapi.dao;

import com.netonomy.blm.api.utils.SessionF;

import ma.iam.wiam.exceptions.TechnicalException;

/**
 * The Class BaseDAO.
 */

public interface BaseNetoDao {
	
	public Boolean sessionByPass();
	public SessionF getCurrentSession() throws TechnicalException;
	
}
