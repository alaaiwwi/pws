package ma.iam.wiam.dao.impl;

import java.io.Serializable;
import java.util.Collection;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.dao.PersistenceManager;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;

import ma.iam.wiam.dao.HibernateGenericDao;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public abstract class HibernateGenericDaoImpl<T, PK extends Serializable>  implements HibernateGenericDao<T, PK> {

	/**
	 * Type de l'entit� persist�e.
	 */

	/**
	 * Requ�te g�n�r�e pour r�cup�rer toutes les entit�s du m�me type.
	 */
	protected Class<T> persistentClass;

    @Autowired
    private PersistenceManager persistenceManager = null;

	/**
	 * Constructeur, permet de r�cup�rer le type de l'entit�.
	 * 
	 * @param type
	 *            Type de l'entit� � persister
	 */
	public HibernateGenericDaoImpl(Class<T> persistentClass) {

		this.persistentClass = persistentClass;
	}
	
	

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
    @Transactional
	public Collection<T> getAll() throws TechnicalException {
        return persistenceManager.findAll(this.persistentClass,true);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public T getById(PK id) throws TechnicalException {
		return (T) persistenceManager.findById(this.persistentClass,id,false);
	}

	/**
	 * {@inheritDoc}
	 */
	public void remove(T object) throws TechnicalException  {
		persistenceManager.delete(object);
	}

	/**
	 * {@inheritDoc}
	 */
	public void save(T object) throws TechnicalException {
		 persistenceManager.save(object);
	}

	/**
	 * {@inheritDoc}
	 */
	public void saveOrUpdate(T object) throws TechnicalException {
        persistenceManager.save(object);
	}

	/**
	 * {@inheritDoc}
	 */
	public void remove(PK id) throws TechnicalException  {
        persistenceManager.delete(this.persistentClass ,id);
	}

	public Class getType() {
		return persistentClass.getClass();
	}

	public HibernateGenericDaoImpl() {
		super();
	}

    public PersistenceManager getPersistenceManager() {
        return persistenceManager;
    }

    public void setPersistenceManager(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

}
