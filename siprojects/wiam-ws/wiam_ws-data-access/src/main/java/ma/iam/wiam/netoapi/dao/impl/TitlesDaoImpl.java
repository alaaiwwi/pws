package ma.iam.wiam.netoapi.dao.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.contact.TitleIF;

import ma.iam.wiam.netoapi.dao.TitlesDao;

@Repository
public class TitlesDaoImpl extends BaseNetoDAOImpl implements TitlesDao {

	public List<TitleIF> getAllTitles() {
		TitleIF[] tiles = null;
		tiles = ObjectRefMgr.getAllTitles();

		return (tiles != null) ? Arrays.asList(tiles) : null;
	}

}
