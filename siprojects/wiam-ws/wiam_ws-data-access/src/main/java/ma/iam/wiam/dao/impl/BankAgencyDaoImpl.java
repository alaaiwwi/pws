package ma.iam.wiam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.BankAgencyDao;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;

@Repository
public class BankAgencyDaoImpl extends HibernateGenericDaoImpl<BankAgency, Long> implements BankAgencyDao {

	public BankAgencyDaoImpl() {
		super(BankAgency.class);
	}

	@SuppressWarnings("unchecked")
	public List<BankAgency> getAgenciesByCodeBank(String bankCode) {
		try {
			Criteria criteria = getPersistenceManager().createCriteria(BankAgency.class);
			criteria
				.addOrder(Order.asc(BankAgency.NAME))
				.createCriteria(BankAgency.BANK)
				.add(Restrictions.eq(Bank.CODE, bankCode));
			List<BankAgency> bankAgencies = criteria.list();
			return bankAgencies;
		}catch (Throwable e){
			return  null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<BankAgency> getAgencyByCodeAndCodeBank(String code, String bankCode) {
		try {
			Criteria criteria = getPersistenceManager().createCriteria(BankAgency.class);
			criteria
					.addOrder(Order.asc(BankAgency.NAME))
					.add(Restrictions.eq(BankAgency.CODE, code))
					.createCriteria(BankAgency.BANK)
					.add(Restrictions.eq(Bank.CODE, bankCode));
			List<BankAgency> bankAgencies = criteria.list();
			return bankAgencies;
		} catch (Throwable e) {
			return null;
		}
	}
}
