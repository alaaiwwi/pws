package ma.iam.wiam.business.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.netonomy.blm.api.Contract.ContractF;
import com.netonomy.blm.util.BlmSecurityException;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.ContractDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/ApplicationContext.xml" })
public class ContractManagerBizImplTest {

	@Autowired
	ContractDao contractDao;

	@SuppressWarnings("unused")
	@Test
	public void testGetContractById() throws BlmSecurityException, NumberFormatException, TechnicalException {
		String str = null;
		Long Id = 33306L;
		if (Id != null) {
			ContractF contractF = contractDao.getContractById(Id);
			str = contractF.toString();
		}
		Assert.assertNotNull("str not null", str);
	}

}
