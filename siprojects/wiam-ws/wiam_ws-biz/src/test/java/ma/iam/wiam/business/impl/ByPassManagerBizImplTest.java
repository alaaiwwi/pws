package ma.iam.wiam.business.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.netonomy.blm.util.BlmSecurityException;

import ma.iam.wiam.dao.ByPassDao;
import ma.iam.wiam.exceptions.TechnicalException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/ApplicationContext.xml" })
public class ByPassManagerBizImplTest {

	@Autowired
	private ByPassDao byPassDao;

	@Test
	public void testsessionByPass() throws BlmSecurityException, NumberFormatException, TechnicalException {

		Assert.assertTrue("Initialisation de la session est effectuee avec succes", byPassDao.sessionByPass());

	}

}
