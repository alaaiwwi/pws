package ma.iam.wiam.business;

import ma.iam.wiam.exceptions.TechnicalException;

public interface ReferentialManagerBiz {

	Object getItemFromReferential(String code);
	
	void setItemInReferential(String code, Object value) throws TechnicalException;
}
