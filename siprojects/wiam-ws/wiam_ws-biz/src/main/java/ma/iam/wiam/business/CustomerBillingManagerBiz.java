package ma.iam.wiam.business;

import java.util.List;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.CustomerBillingInfoBean;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;

public interface CustomerBillingManagerBiz {

	/**
	 * search and get customer billing account by number of client
	 * 
	 * @param criteria
	 * @return List that contains the billing account of the given client
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 */
	List<CustomerBillingInfoBean> getBillingInfos(
			ClientSearchCriteria criteria) throws FunctionnalException,
			TechnicalException;

	/**
	 * Modify the customer billing account
	 * 
	 * @param mapVO2BusinessBean
	 * @return NCLI in success
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 */
	String modifyBillingAccountInfo(
			ma.iam.wiam.neto.bean.CustomerBillingInfoBean mapVO2BusinessBean)
			throws FunctionnalException, TechnicalException;

}
