package ma.iam.wiam.param.criteria;

public class ClientSearchCriteria {
	private String ncli;

	private String ndOrLogin;

	private String identifiant;

	private String socialReason;

	public String getNcli() {
		return ncli;
	}

	public void setNcli(String ncli) {
		this.ncli = ncli;
	}

	public String getNdOrLogin() {
		return ndOrLogin;
	}

	public void setNdOrLogin(String ndOrLogin) {
		this.ndOrLogin = ndOrLogin;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public String getSocialReason() {
		return socialReason;
	}

	public void setSocialReason(String socialReason) {
		this.socialReason = socialReason;
	}

}
