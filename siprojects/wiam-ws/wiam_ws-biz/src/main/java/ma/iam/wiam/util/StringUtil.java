package ma.iam.wiam.util;

public class StringUtil {
	public static final boolean isNullOrEmpty(String value) {
		return (value == null || "".equals(value)) ? Boolean.TRUE : Boolean.FALSE;
	}

	public static final String nonNullOrEmpty(String value) {
		return (isNullOrEmpty(value)) ? "" : value;
	}
}
