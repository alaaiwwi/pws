/*
 * @author AtoS
 */

package ma.iam.wiam.business.constants;

/**
 * The Interface ProjectProperties.
 */
public interface ProjectProperties {

    /** La constante TIMEOUT. */
    int TIMEOUT = 120;
    
    String FULL_FORMATTER = "dd/MM/yyyy HH:mm:ss";
    
	String SHORT_FORMATTER = "dd/MM/yyyy";
}
