package ma.iam.wiam.neto.bean;

public class ContactLegalBean extends ContactBean {
	
	private String adresseEtrangere;
	private String sigle;
	private String dateDeNaissance;
	private ReferentialBean typeIdentifiant;
	private String numeroIdentifiant;
	private String numeroDePatente;
	private String locationObtention;
	private String dateObtention;
	private ReferentialBean profession;
	private ReferentialBean etatCivil;
	private ReferentialBean sexe;
	private AdresseBean adresse;
	public String getAdresseEtrangere() {
		return adresseEtrangere;
	}
	public void setAdresseEtrangere(String adresseEtrangere) {
		this.adresseEtrangere = adresseEtrangere;
	}
	public String getSigle() {
		return sigle;
	}
	public void setSigle(String sigle) {
		this.sigle = sigle;
	}
	public String getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(String dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	public ReferentialBean getTypeIdentifiant() {
		return typeIdentifiant;
	}
	public void setTypeIdentifiant(ReferentialBean typeIdentifiant) {
		this.typeIdentifiant = typeIdentifiant;
	}
	public String getNumeroIdentifiant() {
		return numeroIdentifiant;
	}
	public void setNumeroIdentifiant(String numeroIdentifiant) {
		this.numeroIdentifiant = numeroIdentifiant;
	}
	public String getNumeroDePatente() {
		return numeroDePatente;
	}
	public void setNumeroDePatente(String numeroDePatente) {
		this.numeroDePatente = numeroDePatente;
	}
	public String getLocationObtention() {
		return locationObtention;
	}
	public void setLocationObtention(String locationObtention) {
		this.locationObtention = locationObtention;
	}
	public String getDateObtention() {
		return dateObtention;
	}
	public void setDateObtention(String dateObtention) {
		this.dateObtention = dateObtention;
	}
	public ReferentialBean getProfession() {
		return profession;
	}
	public void setProfession(ReferentialBean profession) {
		this.profession = profession;
	}
	public ReferentialBean getEtatCivil() {
		return etatCivil;
	}
	public void setEtatCivil(ReferentialBean etatCivil) {
		this.etatCivil = etatCivil;
	}
	public ReferentialBean getSexe() {
		return sexe;
	}
	public void setSexe(ReferentialBean sexe) {
		this.sexe = sexe;
	}
	public AdresseBean getAdresse() {
		return adresse;
	}
	public void setAdresse(AdresseBean adresse) {
		this.adresse = adresse;
	}
}