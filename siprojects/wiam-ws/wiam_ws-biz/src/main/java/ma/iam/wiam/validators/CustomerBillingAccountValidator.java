package ma.iam.wiam.validators;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.neto.bean.CustomerBillingInfoBean;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author s.hroumti
 *
 */
public class CustomerBillingAccountValidator {

	public static void validateBillingAccount(
			CustomerBillingInfoBean customerBillingInfoBean,
			ActionType actionType) throws FunctionnalException {
		if (customerBillingInfoBean == null) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.REQUIRED_FIELD_WS,
					"Les informations de facturations sont obligatoires");
		}
		if (ActionType.UPDATE_CUSTOMER_BILLING_ACCOUNT.equals(actionType)) {
			if (StringUtils.isEmpty(customerBillingInfoBean.getNcli())) {
				throw new FunctionnalException(
						ExceptionCodeTypeEnum.REQUIRED_FIELD_WS,
						"Informations de facturation: le champs ncli est obligatoire");
			}
		}

	}

}
