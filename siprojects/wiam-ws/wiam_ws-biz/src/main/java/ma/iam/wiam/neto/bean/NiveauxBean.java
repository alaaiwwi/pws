/*
 * @author AtoS
 */
package ma.iam.wiam.neto.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author y.eddaalous
 */
public class NiveauxBean implements Serializable {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -8616113284323002751L;
    
    private List<NiveauBean> niveau;

	public List<NiveauBean> getNiveau() {
		return niveau;
	}

	public void setNiveau(List<NiveauBean> niveau) {
		this.niveau = niveau;
	}
}
