package ma.iam.wiam.business;

import java.util.List;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ServiceList;
import ma.iam.wiam.neto.bean.ServiceRulesRequestBean;
import ma.iam.wiam.services.regle.bean.ServiceRule;

public interface ServiceManagerBiz {

	/**
	 * Cherche et retourne les services souscrits par contrat
	 * 
	 * @param ncli
	 *            numClient
	 * @param nd
	 *            ND
	 * @return Liste des services souscrits au contrat
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 */
	ServiceList getServicesSouscrits(String ncli, String nd)
			throws FunctionnalException, TechnicalException;

	/**
	 * Cherche et et retourne la liste des services pouvant être ajoutés au
	 * contrat
	 * 
	 * @param ncli
	 *            numClient
	 * @param nd
	 *            ND
	 * @return la liste des services pouvant être ajoutés au contrat
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 */
	ServiceList getAddableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException;

	/**
	 * Cherche et retourne la liste des services pouvant être supprimés du
	 * contrat
	 * 
	 * @param ncli
	 *            numClient
	 * @param nd
	 *            ND
	 * @return
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 */
	ServiceList getRemovableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException;

	/**
	 * Cherche et retourne la liste des services pouvant être modifiés dans un
	 * contrat
	 * 
	 * @param ncli
	 *            numClient
	 * @param nd
	 *            ND
	 * @return
	 */
	ServiceList getEditableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException;

	ServiceList getServiceParamsModify(String ncli, String nd,
			List<String> serviceIds) throws FunctionnalException,
			TechnicalException;

	List<ServiceRule> getServiceRules(
			ServiceRulesRequestBean serviceRulesRequestBean)
			throws FunctionnalException, TechnicalException;

	ServiceList getServiceParamsAdd(String ncli, String nd,
			List<String> serviceIds) throws FunctionnalException,
			TechnicalException;
	
	
	
}
