package ma.iam.wiam.business;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Template;
/**@todo : feature/Template**/
public interface TemplateManagerBiz {

	Template getTemplateById(Long id)throws FunctionnalException, TechnicalException;
	java.util.List<Template> getAllTemplate()throws FunctionnalException, TechnicalException;
	
}
