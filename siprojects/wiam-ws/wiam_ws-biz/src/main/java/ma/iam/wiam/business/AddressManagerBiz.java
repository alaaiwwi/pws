package ma.iam.wiam.business;

import com.netonomy.blm.interfaces.util.MailingAddressIF;
import com.netonomy.blm.interfaces.util.ParameterIF;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.AdresseBean;

public interface AddressManagerBiz {
	
	void createAddress(final AdresseBean adresseBean, final MailingAddressIF mailingAddress, final String  organizationTypeCategoryCode) throws TechnicalException, FunctionnalException;
	
	ParameterIF[] setAdditionalInfosForAddress(final AdresseBean adresseBean) throws FunctionnalException;
}
