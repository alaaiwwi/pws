/*
 * @author AtoS
 */
package ma.iam.wiam.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The Class IntervalWSValidator.
 */
public final class WSValidator {

	/**
	 * Instantiates a new interval ws validator.
	 */
	private WSValidator() {

	}

	/** The FORMAT. */
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	
	/**
	 * Validate date.
	 * 
	 * @param date
	 *            the date
	 * @return the boolean
	 */
	public static Boolean validateDate(final String date) {
		return validateDate(date, DATE_FORMAT);
	}

	/**
	 * Validate date.
	 * 
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return the boolean
	 */
	public static Boolean validateDate(final String date, final String format) {
		Boolean result = Boolean.TRUE;
		try {
			stringToDate(date, format);
		} catch (final ParseException e) {
			result = Boolean.FALSE;
		} catch (final IllegalArgumentException e) {
			result = Boolean.FALSE;
		}
		return result;
	}
	
	public static int isDateBetween(final String stringDate1, final String stringDate2, final String stringDate3, final String format) {
		int result;
		try {
			Date date1 = stringToDate(stringDate1, format);
			Date date2 = stringToDate(stringDate2, format);
			Date date3 = stringToDate(stringDate3, format);
			
			result = date1.after(date2) && date1.before(date3) ? 1 : 0;
		} catch (final ParseException e) {
			result = -2;
		} catch (final IllegalArgumentException e) {
			result = -2;
		}
		return result;
	}
	
	public static Boolean isDateBetween(final String stringDate1, final String stringDate2, final String stringDate3) {
		Boolean result = Boolean.TRUE;
		try {
			Date targetDate = stringToDate(stringDate1, DATE_FORMAT);
			Date minDate = stringToDate(stringDate2, DATE_FORMAT);
			Date maxDate = stringToDate(stringDate3, DATE_FORMAT);
			
			result = targetDate.after(minDate) && targetDate.before(maxDate) ? Boolean.TRUE : Boolean.FALSE;
		} catch (final ParseException e) {
			result = Boolean.FALSE;
		} catch (final IllegalArgumentException e) {
			result = Boolean.FALSE;
		}
		return result;
	}

	/**
	 * String to date.
	 * 
	 * @param strDate
	 *            the str date
	 * @param format
	 *            the format
	 * @return the date
	 * @throws ParseException
	 *             the parse exception
	 */
	public static Date stringToDate(final String strDate, final String format)
			throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(strDate);
	}

	/**
	 * Validate integer.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateNumber(final String value) {
		Boolean result = Boolean.TRUE;
		if (StringUtil.isNullOrEmpty(value)) {
			result = Boolean.FALSE;
		} else {
			try {
				Long.parseLong(value);
			} catch (final NumberFormatException e) {
				result = Boolean.FALSE;
			}
		}
		return result;
	}

	/**
	 * Validate Mobile Number.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateMobileNumber(final String value) {

		Pattern pattern = Pattern.compile("(0|00212|\\+212|212)6[0-9]{8}");
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	/**
	 * Validate Fix Number.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean
	 */
	public static Boolean validateFixNumber(final String value) {

		Pattern pattern = Pattern.compile("(0|00212|\\+212|212)5[0-9]{8}");
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();

	}
	
	public static Boolean validateNonNumeric(final String value) {

		Pattern pattern = Pattern.compile("^[A-Za-z \'-.]{0,40}$");
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	public static Boolean validateMail(final String value) {

		Pattern pattern = Pattern.compile("([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})");
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	public static Boolean validateNumberLength(final String value, final int length) {
		return value.length() == length;
	}
	
	public static Boolean isLengthLessThan(final String value, final int length) {
		return value.length() <= length;
	}
}
