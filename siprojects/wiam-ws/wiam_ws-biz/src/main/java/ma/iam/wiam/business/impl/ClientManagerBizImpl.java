package ma.iam.wiam.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.netonomy.blm.api.Hierarchy.LevelF;



import com.netonomy.util.ObjectId;

import ma.iam.wiam.business.AdresseManagerBiz;
import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.constants.Constants;
import ma.iam.wiam.customer.ClientMgr;
import ma.iam.wiam.dao.AppConfigSousCategorieDao;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.dao.ContactDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.hbm.bean.ClientBean;
import ma.iam.wiam.helpers.ClientHelper;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.userevent.WIAMHistory;
import ma.iam.wiam.util.StringUtil;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.OrganizationTypeEnum;
import ma.iam.wiam.ws.neto.model.Client;
import ma.iam.wiam.ws.neto.model.Contact;


@Component("clientManagerBiz")
public class ClientManagerBizImpl implements ClientManagerBiz {

	@Autowired
	private ClientDao clientDao;
	@Autowired
	private LevelFDao levelFDao;
	
	@Autowired
	private AdresseManagerBiz adresseMangerBiz;
	@Autowired
	private ContactDao contactDao;

	@Value("#{config['CONTRACT_STATUS_DOUTEUX']}")
	private String statusDouteu;
	
	/**Begin feature/Changement_Categorie_Client**/
	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;
	
	@Autowired
	private AppConfigSousCategorieDao appConfigSousCategorieDao;
	/**End feature/Changement_Categorie_Client**/

	public Boolean verifySolavabilteClientByNcli(ClientSearchCriteria criteria)
			throws FunctionnalException, TechnicalException {

		String[] statusDouteusArray = statusDouteu.split(Constants.SEPARATOR);
		if (criteria == null || StringUtil.isNullOrEmpty(criteria.getNcli())) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_SEARCH_WS,
					"Les criteres de recherches sont vides");
		}

		List<Client> list = clientDao.findClientByClientNum(criteria.getNcli());

		if (list == null || list.isEmpty()) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_RESULT_WS,
					"Aucun client trouvé");

		}

		Integer integer = clientDao.getCountContractDouteuxByNcli(
				criteria.getNcli(), statusDouteusArray);

		return (integer != null && integer.intValue() > 0);
	}

	private List<ClientBean> findClient(List<Client> list)
			throws FunctionnalException, TechnicalException {
try{
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}

		List<ClientBean> listBean = new ArrayList<ClientBean>();

		for (Client client : list) {
			Contact contact = null;
			ClientBean clientBean = new ClientBean();
			BeanUtils.copyProperties(client, clientBean);
			if (client.getLegalContact() != null) {

				contact = contactDao.getById(client.getLegalContact());

				if (contact != null) {

					/**
					 * contact.isIsCorporate() &&
					 * !StringUtil.isNullOrEmpty(contact.getBusinessName())
					 * changed by
					 * !StringUtil.isNullOrEmpty(contact.getBusinessName())
					 * because there is some GP clients who have isCorporate
					 * true
					 */
					if (StringUtils.isNotEmpty(contact.getBusinessName())) {
						clientBean.setNomClient(StringUtil
								.nonNullOrEmpty(contact.getBusinessName()));
					} else {
						clientBean.setNomClient(StringUtil
								.nonNullOrEmpty(contact.getFirstName())
								+ " "
								+ StringUtil.nonNullOrEmpty(contact
										.getLastName()));
					}

					clientBean.setAdresse(adresseMangerBiz
							.getAdresseAsString(contact));
				}
			} else {
				if (client.getBillingContact() != null) {
					contact = contactDao.getById(client.getLegalContact());

					if (contact != null) {

						if (StringUtils.isNotEmpty(contact.getBusinessName())) {
							clientBean.setNomClient(StringUtil
									.nonNullOrEmpty(contact.getBusinessName()));
						} else {
							clientBean.setNomClient(StringUtil
									.nonNullOrEmpty(contact.getFirstName())
									+ " "
									+ StringUtil.nonNullOrEmpty(contact
											.getLastName()));
						}

						clientBean.setAdresse(adresseMangerBiz
								.getAdresseAsString(contact));
					}
				}
			}
			listBean.add(clientBean);
		}

		return listBean;
	}catch(FunctionnalException e){
		throw e;
	}
	catch(Throwable e){
		throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
	}
	}

	public List<ClientBean> findClientByCriteria(ClientSearchCriteria criteria)
			throws FunctionnalException, TechnicalException {
		try {
			if (criteria == null
					|| (StringUtil.isNullOrEmpty(criteria.getNcli())
					&& StringUtil.isNullOrEmpty(criteria.getNdOrLogin())
					&& StringUtil.isNullOrEmpty(criteria.getIdentifiant()) && StringUtil
					.isNullOrEmpty(criteria.getSocialReason()))) {
				throw new FunctionnalException(
						ExceptionCodeTypeEnum.EMPTY_SEARCH_WS,
						"Les criteres de recherches sont vides");
			}

			if (!StringUtil.isNullOrEmpty(criteria.getNdOrLogin())) {
				List<Client> list = clientDao.findClientByNdOrLogin(criteria
						.getNdOrLogin());
				return findClient(list);
			}

			if (!StringUtil.isNullOrEmpty(criteria.getNcli())) {
				List<Client> list = clientDao.findClientByClientNum(criteria
						.getNcli());
				return findClient(list);
			}
			if (!StringUtil.isNullOrEmpty(criteria.getIdentifiant())) {
				List<Client> list = clientDao.findClientByIdent(criteria
						.getIdentifiant());
				return findClient(list);
			}
			if (!StringUtil.isNullOrEmpty(criteria.getSocialReason())) {
				List<Client> list = clientDao.findClientBySocialReason(criteria
						.getSocialReason());
				return findClient(list);
			}
			return null;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	public Client getClient(String ncli) throws TechnicalException,
			FunctionnalException {

		List<Client> list = clientDao.findClientByClientNum(ncli);
		if (list == null || list.isEmpty()) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_RESULT_WS,
					"aucun client trouvé");
		}
		if (list.size() > 1) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.MANY_RESULT_WS,
					"Plusieurs résultats trouvés");

		}

		return list.get(0);
	}

	/**Begin feature/Changement_Categorie_Client*/
	@Override
	public Boolean changeCategoryClient(String ncli, String codeSousCategorieDest)
			throws TechnicalException, FunctionnalException {
		try {
			String orgType=appConfigSousCategorieDao.getOrganizationTypeIdByCode(codeSousCategorieDest);////////////////////Charger par DAO
			Client client = getClient(ncli);
			LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId()).toString());
			String orgID = orgF.getIdentifier().getString();
			if (ArrayUtils.contains(new String[] { OrganizationTypeEnum.RESIDENTIEL.toString() },orgF.getType().getCode().toString()) || ArrayUtils.contains(new String[] { OrganizationTypeEnum.PROFESSIONNEL.toString() },orgF.getType().getCode().toString())) {
				if (ArrayUtils.contains(new String[] { OrganizationTypeEnum.RESIDENTIEL.toString() },codeSousCategorieDest)|| ArrayUtils.contains(new String[] { OrganizationTypeEnum.PROFESSIONNEL.toString() },codeSousCategorieDest)) {
					//Comportement Neto
					if (ClientHelper.getInstance().isEcartedCustomer(Long.parseLong(orgF.getLegacyIdentifier().getString()))) {
						
						throw new FunctionnalException(ExceptionCodeTypeEnum.RESTRICTION_ERROR,
								"Vous essayez d'effectuer un changement de categorie d'un client ecarte vers une autre categorie, "
										+ "cette operation n'est pas permise actuellement, veuillez essayer ulterieurement apres la fin de facturation.");
					}
					else {
						if (!ClientHelper.getInstance().isPossibleChangingCategorie(orgF.getLegacyIdentifier().getString(), orgType)) {
							throw new FunctionnalException(ExceptionCodeTypeEnum.RESTRICTION_ERROR,
									"Vous essayez d'effectuer un changement de categorie d'un client ayant un billcycle "
											+ "non facture vers une autre categorie avec un billcycle facture, cette opration n'est "
											+ "pas permise actuellement, veuillez essayer ulterieurement apres la fin de facturation.");
						}else {
							ClientMgr.updateClientCategory(orgID, orgType);
							WIAMHistory.addModifCustomInfoEvent(orgF, new Date(),baseNetoDao.getCurrentSession().getUserF().getLoginId(),orgF.getType().getName(), null, null, null, null,null);
							return true;
						}
					}
				}
				else {
					throw new FunctionnalException(ExceptionCodeTypeEnum.RESTRICTION_ERROR,
							"La Sous-Catégorie destination du Client doit être soit RESIDENTIEL soit PROFESSIONNEL");
				}
			}
			else {
				throw new FunctionnalException(ExceptionCodeTypeEnum.RESTRICTION_ERROR,
						"La Sous-Catégorie du Client doit être soit RESIDENTIEL soit PROFESSIONNEL");
			}
		}
		catch(Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
		
	}
	/**End feature/Changement_Categorie_Client*/

}
