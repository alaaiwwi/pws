package ma.iam.wiam.business.constants;

public class Constants {

	public static final String SEPARATOR = ",";
	public static final long MOROCCO_ID = 133;
	public static final String XXXX = "XXXX";
	public static final String NONE = "NONE";
	public static final String ADRESSE_ETAGE = ", Etage ";
	public static final String ADRESSE_ESC = ", Esc ";
	public static final String ADRESSE_BATIMENT = ", Bat ";
	public static final String ADRESSE_PORTE = ", Porte ";
	public static final String ADRESSE_BP = ", BP ";

	public static final String EMPTY_STRING = "";
	public static final String MODE_PAIEMENT_PRELEVEMENT_CODE = "1";

	public static final String PROFESSIONNEL = "PROFESSIONNEL";
	public static final String ENTREPRISE = "ENTREPRISE";
	public static final String TELEBOUTIQUE = "TELEBOUTIQUE";
	public static final String GRANDS_COMPTE = "GRANDS_COMPTE";

	public static final String SERVICE_INFO_CODE_SVC_NOT_HIDDEN_MS = "SVC_NOT_HIDDEN_MS";

	public static final String SERVICE_INFO_CODE_SVC_MINCOM_FLAG = "SVC_MINCOM_FLAG";

	public static final String SERVICE_INFO_CODE_SVC_ALLOW_MODIF = "SVC_ALLOW_MODIF";

	public static final String SERVICE_INFO_CODE_SVC_PACK_MODIF = "SVC_PACK_MODIF";

	public static final int COMMENT_MAX_SIZE = 60;
}
