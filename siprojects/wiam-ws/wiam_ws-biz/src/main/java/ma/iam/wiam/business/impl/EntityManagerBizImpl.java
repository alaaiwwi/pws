package ma.iam.wiam.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.business.EntityManagerBiz;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.dao.AppConfigSousCategorieDao;
import ma.iam.wiam.dao.BankAgencyDao;
import ma.iam.wiam.dao.BankDao;
import ma.iam.wiam.dao.LevelDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.ValueChoiceItemDao;
import ma.iam.wiam.param.criteria.BankAgencyCriteria;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.AppConfigSousCategorie;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;

@Component
public class EntityManagerBizImpl implements EntityManagerBiz {

	@Value("#{config['ma.iam.wiam.bank.restrictedList']}")
	private String restrictedBanks;

	@Autowired
	private BankDao bankDao;

	@Autowired
	private BankAgencyDao bankAgencyDao;

	@Autowired
	private ValueChoiceItemDao valueChoiceItemDao;

	@Autowired
	private LevelDao levelDao;
	
	//@todo feature/24: Autowiring avec la sous categorie dao
	@Autowired
	private AppConfigSousCategorieDao appConfigSousCategorieDao;

	public List<ValueChoiceItemIF> getSegments1() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_SEGMENT);
	}

	public List<ValueChoiceItemIF> getSegments2() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_SEGMENT2);
	}

	public List<Object[]> getAllMTAgenciesInRegie() throws FunctionnalException, TechnicalException {
		return levelDao.getAllMTAgenciesInRegie();
	}

	public List<ValueChoiceItemIF> getProspectionInfos() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_LIB_PROSPECT);
	}

	public List<Bank> getBanks() throws FunctionnalException, TechnicalException {
		String separator = ",";
		String[] restrictedBanksArray = restrictedBanks.split(separator);

		Arrays.asList(restrictedBanksArray);

		List<Bank> notRestrictedBanks = bankDao.getBanksNotInCodesList(Arrays.asList(restrictedBanksArray));

		return notRestrictedBanks;
	}

	public List<BankAgency> getAgenciesByCriteria(BankAgencyCriteria bankAgencyCriteria)
			throws FunctionnalException, TechnicalException {
		if (StringUtils.isEmpty(bankAgencyCriteria.getBankCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_SEARCH_WS);
		}
		List<BankAgency> bankAgencies = new ArrayList<BankAgency>(bankAgencyDao.getAgenciesByCodeBank(bankAgencyCriteria.getBankCode()));
		return bankAgencies;
	}

	public List<ValueChoiceItemIF> getPayerQualities() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_QUALITE_PAYEUR);
	}

	public List<ValueChoiceItemIF> getBillingCycles() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_CYCLE_FACTURATION);
	}

	public List<ValueChoiceItemIF> getPaymentTerms() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_TERME_PAIEMENT);
	}

	public List<ValueChoiceItemIF> getBillingSupports() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_SUPPORT_FACTURE);
	}

	public List<ValueChoiceItemIF> getBillingLanguages() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_LANGUE_FACTURE);
	}
	
	//@todo feature/24: ajouter la methode de sous categorie qui sera appeler
	public String[] getSousCategory() throws FunctionnalException, TechnicalException {
		return appConfigSousCategorieDao.getSousCategorie(Constants.SOUS_CATEGORY_EREV);
	}
	
}
