package ma.iam.wiam.util;

import org.springframework.util.StringUtils;

import com.netonomy.util.ObjectId;

/**
 * 
 * @author S.hroumti
 *
 */
public class ObjectIdUtil {

	/**
	 * Convert string that represent ObjectId with type LongId to Long<br>
	 * example : convert L4:1539 to Long(1539)
	 * 
	 * @param objectId
	 * @return
	 */
	public static Long convertLongIdToLong(String objectId) {
		if (StringUtils.isEmpty(objectId)) {
			return null;
		}
		return ObjectId.convertToLong(ObjectId.instantiate(objectId));
	}

	/**
	 * Convert string that represent ObjectId with type LongId to String <br>
	 * example : convert L4:1539 to String("1539")
	 * 
	 * @param objectId
	 * @return
	 */
	public static String convertLongIdToLongAsString(String objectId) {
		Long id = convertLongIdToLong(objectId);
		if (id != null) {
			return id.toString();
		}
		return null;
	}
}
