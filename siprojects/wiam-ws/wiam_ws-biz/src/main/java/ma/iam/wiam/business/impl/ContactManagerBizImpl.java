package ma.iam.wiam.business.impl;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.contact.ContactIF;
import com.netonomy.blm.interfaces.contact.TitleIF;
import com.netonomy.blm.interfaces.contract.ContractIF;
import com.netonomy.blm.interfaces.organization.LevelIF;
import com.netonomy.blm.interfaces.organization.NodeItemIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.EmailIF;
import com.netonomy.blm.interfaces.util.MailingAddressIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.blm.interfaces.util.PhoneIF;
import com.netonomy.util.LongId;
import com.netonomy.util.ObjectId;

import ma.iam.wiam.business.AddressManagerBiz;
import ma.iam.wiam.business.ContactManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.business.constants.ProjectProperties;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.customer.BillingAccountWIAM;
import ma.iam.wiam.customer.ContactWIAM;
import ma.iam.wiam.customer.PaymentInfoWIAM;
import ma.iam.wiam.dao.BankAgencyDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContactBean;
import ma.iam.wiam.neto.bean.ContactLegalBean;
import ma.iam.wiam.neto.bean.ContactDeFacturationBean;
import ma.iam.wiam.neto.bean.MethodeDePaiementBean;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.offre.CommercialOfferWIAM;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.util.WSValidator;
import ma.iam.wiam.validators.OrganizationValidator;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.OrganizationCategoryEnum;
import ma.iam.wiam.ws.neto.enums.PaymentMethodType;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;
import ma.iam.wiam.ws.neto.model.Title;

/**
 * This service
 * 
 * @author y.eddaalous
 *
 */
@Service
@Repository
public class ContactManagerBizImpl implements ContactManagerBiz {

	private static final SimpleDateFormat fullFormatter = new SimpleDateFormat(ProjectProperties.FULL_FORMATTER);

	@Autowired
	private BankAgencyDao bankAgencyDao;

	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;

	@Autowired
	private ReferentialManagerBiz referentialManagerBiz;

	@Autowired
	private AddressManagerBiz addressManagerBiz;

	public void processContact(final ActionMgrIF actionMgr, final LevelIF levelIF, final ContactBean contactBean,
			final String organizationTypeCategoryCode) throws TechnicalException, FunctionnalException {
		ContactIF contactIF = createContact(contactBean, organizationTypeCategoryCode);
		MailingAddressIF mailingAddress = contactIF.getAddress();

		if ((contactBean instanceof ContactLegalBean)) {
			ContactLegalBean contactLegalBean = (ContactLegalBean) contactBean;

			addressManagerBiz.createAddress(contactLegalBean.getAdresse(), mailingAddress,
					organizationTypeCategoryCode);
			contactIF.setAddress(mailingAddress);

			ParameterIF[] legalContactParameters = setAdditionalInfosForLegalContact(contactLegalBean,
					organizationTypeCategoryCode);
			ParameterIF[] addressParameters = addressManagerBiz
					.setAdditionalInfosForAddress(contactLegalBean.getAdresse());
			ParameterIF[] allParameters = (ParameterIF[]) ArrayUtils.addAll(legalContactParameters, addressParameters);

			levelIF.createLegalContact(actionMgr, contactIF, allParameters);
		} else if ((contactBean instanceof ContactDeFacturationBean)) {
			ContactDeFacturationBean billingContactBean = (ContactDeFacturationBean) contactBean;

			createFakeContract(actionMgr);

			addressManagerBiz.createAddress(billingContactBean.getAdresse(), mailingAddress,
					organizationTypeCategoryCode);
			contactIF.setAddress(mailingAddress);

			ParameterIF[] billingAccountParameterIFs = setAdditionnalInfosForBillingContact(
					(ContactDeFacturationBean) contactBean, organizationTypeCategoryCode);
			ParameterIF[] paymentMethodParameterIFs = setPaymentMethodInfosForBillingContact(
					billingContactBean.getMethodeDePaiement());

			ParameterIF[] addressParameterIFs = addressManagerBiz
					.setAdditionalInfosForAddress(billingContactBean.getAdresse());

			levelIF.createBillingContact(actionMgr, contactIF, addressParameterIFs);
			levelIF.createBillingAccount(actionMgr,
					ObjectRefMgr.getPaymentMethod(
							new LongId(new Long(billingContactBean.getMethodeDePaiement().getCode()))),
					paymentMethodParameterIFs, billingAccountParameterIFs);
		}
	}

	private ContactIF createContact(final ContactBean contactBean, final String organizationTypeCategoryCode)
			throws TechnicalException, FunctionnalException {

		OrganizationValidator.validateContact(contactBean, organizationTypeCategoryCode);

		ContactIF contactIF = ObjectMgr.createContact();

		updateContact(contactBean, organizationTypeCategoryCode, contactIF);

		return contactIF;
	}

	@SuppressWarnings("unchecked")
	public void updateContact(final ContactBean contactBean,
			final String organizationTypeCategoryCode, ContactIF contactIF)
			throws FunctionnalException {
		if (contactBean.getCodePersonne() != null && StringUtils.isNotEmpty(contactBean.getCodePersonne().getCode())) {

			List<Title> titles = (List<Title>) referentialManagerBiz.getItemFromReferential(Constants.TITLES);
			Title nonKernelTitle = getTitleFromList(
					ObjectId.convertToLong(ObjectId.instantiate(contactBean.getCodePersonne().getCode())), titles);

			try {
				TitleIF title = ObjectRefMgr.getTitle(ObjectId.instantiate(contactBean.getCodePersonne().getCode()));

				if (title == null || nonKernelTitle == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							contactBean.getClass().getSimpleName() + ": codePersonne "
									+ contactBean.getCodePersonne().getCode() + "  non trouve dans le referentiel");
				}
				contactIF.setTitle(title);
				contactIF.setSex(nonKernelTitle.getSex().charAt(0));
			} catch (Exception e) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						contactBean.getClass().getSimpleName() + ": codePersonne "
								+ contactBean.getCodePersonne().getCode() + "  non trouve dans le referentiel");
			}
		}

		contactIF.setFirstName(contactBean.getPrenom());
		contactIF.setLastName(contactBean.getNom());

		PhoneIF phoneNumber1 = contactIF.getPhoneNumber();
		phoneNumber1.setNumber(contactBean.getTelephoneFix());
		contactIF.setPhoneNumber(phoneNumber1);

		PhoneIF phoneNumber2 = contactIF.getPhoneNumber2();
		phoneNumber2.setNumber(contactBean.getTelephoneMobile());
		contactIF.setPhoneNumber2(phoneNumber2);

		PhoneIF faxNumber = contactIF.getFaxNumber();
		faxNumber.setNumber(contactBean.getFax());
		contactIF.setFaxNumber(faxNumber);

		EmailIF email = contactIF.getEmail();
		email.setEmail(contactBean.getEmail());
		contactIF.setEmail(email);

		if (ArrayUtils.contains(new String[] { OrganizationCategoryEnum.PROFESSIONNEL.toString(),
				OrganizationCategoryEnum.ENTREPRISE.toString() }, organizationTypeCategoryCode)) {
			contactIF.setBusinessName(contactBean.getRaisonSocial());
		} else {
			contactIF.setBusinessName("");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ParameterIF[] setAdditionalInfosForLegalContact(final ContactLegalBean contactLegalBean,
			final String organizationTypeCategoryCode) throws FunctionnalException, TechnicalException {

		OrganizationValidator.validateAdditionnalInfosForLegalContact(contactLegalBean, organizationTypeCategoryCode);

		Vector legalContactParameters = new Vector();

		if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {

			if (contactLegalBean.getEtatCivil() != null
					&& StringUtils.isNotEmpty(contactLegalBean.getEtatCivil().getCode())) {

				List<ValueChoiceItemIF> maritalStatuss = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.MARITAL_STATUS);
				ValueChoiceItemIF maritalStatus = getValueChoiceItemFromList(contactLegalBean.getEtatCivil().getCode(),
						maritalStatuss);

				if (maritalStatus == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contactLegal: etatCivil " + contactLegalBean.getEtatCivil().getCode()
									+ "  non trouve dans le referentiel");
				}
			}
			ContactWIAM.setEtatCivilId(legalContactParameters,
					(contactLegalBean.getEtatCivil() != null) ? contactLegalBean.getEtatCivil().getCode() : null);

			List<ValueChoiceItemIF> genders = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.GENDERS);
			ValueChoiceItemIF gender = getValueChoiceItemFromList(contactLegalBean.getSexe().getCode(), genders);

			if (gender == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "contactLegal: sexe "
						+ contactLegalBean.getSexe().getCode() + "  non trouve dans le referentiel");
			}
			ContactWIAM.setSexeId(legalContactParameters,
					contactLegalBean.getSexe() != null ? contactLegalBean.getSexe().getCode() : null);

			if (StringUtils.isNotEmpty(contactLegalBean.getDateDeNaissance())) {
				try {
					Date birthDay = WSValidator.stringToDate(contactLegalBean.getDateDeNaissance(),
							ProjectProperties.SHORT_FORMATTER);
					ContactWIAM.setDateAnniversaire(legalContactParameters, fullFormatter.format(birthDay));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (contactLegalBean.getProfession() != null
					&& StringUtils.isNotEmpty(contactLegalBean.getProfession().getCode())) {

				List<ValueChoiceItemIF> professions = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.PROFESSIONS);
				ValueChoiceItemIF profession = getValueChoiceItemFromList(contactLegalBean.getProfession().getCode(),
						professions);

				if (profession == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contactLegal: profession " + contactLegalBean.getProfession().getCode()
									+ "  non trouve dans le referentiel");
				}

				ContactWIAM.setProfession(legalContactParameters, contactLegalBean.getProfession().getCode());
			}

			List<ValueChoiceItemIF> identifierTypes = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.GP_IDENTITY_TYPES);
			ValueChoiceItemIF identifierType = getValueChoiceItemFromList(
					contactLegalBean.getTypeIdentifiant().getCode(), identifierTypes);

			if (identifierType == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"contactLegal: typeIdentifiant " + contactLegalBean.getTypeIdentifiant().getCode()
								+ "  non trouve dans le referentiel");
			}

			ContactWIAM.setTypeIdentifiantGP(legalContactParameters, contactLegalBean.getTypeIdentifiant().getCode());
			ContactWIAM.setIdentifiant(legalContactParameters, contactLegalBean.getNumeroIdentifiant());
			ContactWIAM.setLieuIdentifiant(legalContactParameters, contactLegalBean.getLocationObtention());

			if (StringUtils.isNotEmpty(contactLegalBean.getDateObtention())) {
				try {
					Date obtainmentDate = WSValidator.stringToDate(contactLegalBean.getDateObtention(),
							ProjectProperties.SHORT_FORMATTER);
					ContactWIAM.setDateIdentifiant(legalContactParameters, fullFormatter.format(obtainmentDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (StringUtils.isNotEmpty(contactLegalBean.getAdresseEtrangere())) {
				String tempForeignAddress = contactLegalBean.getAdresseEtrangere();
				tempForeignAddress = tempForeignAddress.replaceAll("\r\n", " ");
				tempForeignAddress = tempForeignAddress.replaceAll("\r", " ");
				tempForeignAddress = tempForeignAddress.replaceAll("\n", " ");
				ContactWIAM.setAdresseEtranger(legalContactParameters, tempForeignAddress);
			}

		} else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {

			ContactWIAM.setSigle(legalContactParameters, contactLegalBean.getSigle());

			if (contactLegalBean.getEtatCivil() != null
					&& StringUtils.isNotEmpty(contactLegalBean.getEtatCivil().getCode())) {

				List<ValueChoiceItemIF> maritalStatuss = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.MARITAL_STATUS);
				ValueChoiceItemIF maritalStatus = getValueChoiceItemFromList(contactLegalBean.getEtatCivil().getCode(),
						maritalStatuss);

				if (maritalStatus == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contactLegal: etatCivil " + contactLegalBean.getEtatCivil().getCode()
									+ "  non trouve dans le referentiel");
				}
			}
			ContactWIAM.setEtatCivilId(legalContactParameters, contactLegalBean.getEtatCivil().getCode());

			List<ValueChoiceItemIF> genders = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.GENDERS);
			ValueChoiceItemIF gender = getValueChoiceItemFromList(contactLegalBean.getSexe().getCode(), genders);

			if (gender == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "contactLegal: sexe "
						+ contactLegalBean.getSexe().getCode() + "  non trouve dans le referentiel");
			}
			ContactWIAM.setSexeId(legalContactParameters,
					contactLegalBean.getSexe() != null ? contactLegalBean.getSexe().getCode() : null);

			if (StringUtils.isNotEmpty(contactLegalBean.getDateDeNaissance())) {
				try {
					Date birthDay = WSValidator.stringToDate(contactLegalBean.getDateDeNaissance(),
							ProjectProperties.SHORT_FORMATTER);
					ContactWIAM.setDateAnniversaire(legalContactParameters, fullFormatter.format(birthDay));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (contactLegalBean.getProfession() != null
					&& StringUtils.isNotEmpty(contactLegalBean.getProfession().getCode())) {

				List<ValueChoiceItemIF> professions = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.PROFESSIONS);
				ValueChoiceItemIF profession = getValueChoiceItemFromList(contactLegalBean.getProfession().getCode(),
						professions);

				if (profession == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contactLegal: profession " + contactLegalBean.getProfession().getCode()
									+ "  non trouve dans le referentiel");
				}

				ContactWIAM.setProfession(legalContactParameters, contactLegalBean.getProfession().getCode());
			}

			List<ValueChoiceItemIF> identifierTypes = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.PRO_IDENTITY_TYPES);
			ValueChoiceItemIF identifierType = getValueChoiceItemFromList(
					contactLegalBean.getTypeIdentifiant().getCode(), identifierTypes);

			if (identifierType == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"contactLegal: typeIdentifiant " + contactLegalBean.getTypeIdentifiant().getCode()
								+ "  non trouve dans le referentiel");
			}

			ContactWIAM.setTypeIdentifiantPRF(legalContactParameters, contactLegalBean.getTypeIdentifiant().getCode());
			ContactWIAM.setIdentifiant(legalContactParameters, contactLegalBean.getNumeroIdentifiant());
			ContactWIAM.setLieuIdentifiant(legalContactParameters, contactLegalBean.getLocationObtention());

			try {
				Date obtainmentDate = WSValidator.stringToDate(contactLegalBean.getDateObtention(),
						ProjectProperties.SHORT_FORMATTER);
				ContactWIAM.setDateIdentifiant(legalContactParameters, fullFormatter.format(obtainmentDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ContactWIAM.setNumeroPatente(legalContactParameters, contactLegalBean.getNumeroDePatente());

			if (StringUtils.isNotEmpty(contactLegalBean.getAdresseEtrangere())) {
				String tempForeignAddress = contactLegalBean.getAdresseEtrangere();
				tempForeignAddress = tempForeignAddress.replaceAll("\r\n", " ");
				tempForeignAddress = tempForeignAddress.replaceAll("\r", " ");
				tempForeignAddress = tempForeignAddress.replaceAll("\n", " ");
				ContactWIAM.setAdresseEtranger(legalContactParameters, tempForeignAddress);
			}
		} else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {
			ContactWIAM.setSigle(legalContactParameters, contactLegalBean.getSigle());

			List<ValueChoiceItemIF> identifierTypes = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.ENT_IDENTITY_TYPES);
			ValueChoiceItemIF identifierType = getValueChoiceItemFromList(
					contactLegalBean.getTypeIdentifiant().getCode(), identifierTypes);

			if (identifierType == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"contactLegal: typeIdentifiant " + contactLegalBean.getTypeIdentifiant().getCode()
								+ "  non trouve dans le referentiel");
			}

			ContactWIAM.setTypeIdentifiantENT(legalContactParameters, contactLegalBean.getTypeIdentifiant().getCode());
			ContactWIAM.setIdentifiant(legalContactParameters, contactLegalBean.getNumeroIdentifiant());
			ContactWIAM.setLieuIdentifiant(legalContactParameters, contactLegalBean.getLocationObtention());

			try {
				Date obtainmentDate = WSValidator.stringToDate(contactLegalBean.getDateObtention(),
						ProjectProperties.SHORT_FORMATTER);
				ContactWIAM.setDateIdentifiant(legalContactParameters, fullFormatter.format(obtainmentDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ContactWIAM.setNumeroPatente(legalContactParameters, contactLegalBean.getNumeroDePatente());

			if (StringUtils.isNotEmpty(contactLegalBean.getAdresseEtrangere())) {
				String tempForeignAddress = contactLegalBean.getAdresseEtrangere();
				tempForeignAddress = tempForeignAddress.replaceAll("\r\n", " ");
				tempForeignAddress = tempForeignAddress.replaceAll("\r", " ");
				tempForeignAddress = tempForeignAddress.replaceAll("\n", " ");
				ContactWIAM.setAdresseEtranger(legalContactParameters, tempForeignAddress);
			}
		}

		ParameterIF[] legalContactParameterIFs = WIAMHelper.vectorToParameter(legalContactParameters);
		return legalContactParameterIFs;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ParameterIF[] setAdditionnalInfosForBillingContact(final ContactDeFacturationBean contacteDeFacturationBean,
			final String organizationTypeCategoryCode) throws FunctionnalException {

		OrganizationValidator.validateAdditionnalInfosForBillingContact(contacteDeFacturationBean,
				organizationTypeCategoryCode);

		Vector billingContactParameters = new Vector();

		if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {
			BillingAccountWIAM.setExemptionTaxe(billingContactParameters,
					contacteDeFacturationBean.getExempte().toString());

			List<ValueChoiceItemIF> billingSupports = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.BILLING_SUPPORTS);

			ValueChoiceItemIF billingSupport = getValueChoiceItemFromList(
					contacteDeFacturationBean.getSupportDeFacturation().getCode(), billingSupports);

			if (billingSupport == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"contacteDeFacturation: supportDeFacturation "
								+ contacteDeFacturationBean.getSupportDeFacturation().getCode()
								+ "  non trouve dans le referentiel");
			}

			BillingAccountWIAM.setSupportFacture(billingContactParameters,
					contacteDeFacturationBean.getSupportDeFacturation().getCode());

			if (ArrayUtils.contains(new String[] { "L4:1345", "L4:1346" },
					contacteDeFacturationBean.getSupportDeFacturation().getCode())) {

				List<Object[]> organizations = (List<Object[]>) referentialManagerBiz
						.getItemFromReferential(Constants.SMART_FACT_ORGANIZATIONS);
				Object[] organization = getSmartFactOrgnaizationFromList(
						contacteDeFacturationBean.getOrganisation().getCode(), organizations);

				if (organization == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: organisation "
									+ contacteDeFacturationBean.getOrganisation().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setSmartFact(billingContactParameters,
						contacteDeFacturationBean.getOrganisation().getCode());
			} else {
				BillingAccountWIAM.setSmartFact(billingContactParameters, null);
			}

			if (contacteDeFacturationBean.getLangue() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getLangue().getCode())) {

				List<ValueChoiceItemIF> billingLanguages = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.BILLING_LANGUAGES);
				ValueChoiceItemIF billingLanguage = getValueChoiceItemFromList(
						contacteDeFacturationBean.getLangue().getCode(), billingLanguages);

				if (billingLanguage == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: langue " + contacteDeFacturationBean.getLangue().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setLangue(billingContactParameters, contacteDeFacturationBean.getLangue().getCode());
			}

			List<ValueChoiceItemIF> billingCycles = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.BILLING_CYCLES);

			if (contacteDeFacturationBean.getCycleDeFacturation() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getCycleDeFacturation().getCode())) {

				ValueChoiceItemIF billingCycle = getValueChoiceItemFromList(
						contacteDeFacturationBean.getCycleDeFacturation().getCode(), billingCycles);

				if (billingCycle == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: cycleDeFacturation "
									+ contacteDeFacturationBean.getCycleDeFacturation().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setCycleFacturation(billingContactParameters,
						contacteDeFacturationBean.getCycleDeFacturation().getCode());
			} else {
				if (CollectionUtils.isNotEmpty(billingCycles)) {
					BillingAccountWIAM.setCycleFacturation(billingContactParameters,
							billingCycles.get(0).getChoiceItem().getIdentifier().toString());
				}
			}

			List<ValueChoiceItemIF> paymentTerms = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.PAYMENT_TERMS);

			if (contacteDeFacturationBean.getTermeDePaiment() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getTermeDePaiment().getCode())) {

				ValueChoiceItemIF paymentMethod = getValueChoiceItemFromList(
						contacteDeFacturationBean.getTermeDePaiment().getCode(), paymentTerms);

				if (paymentMethod == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: termeDePaiment "
									+ contacteDeFacturationBean.getTermeDePaiment().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setTermePaiement(billingContactParameters,
						contacteDeFacturationBean.getTermeDePaiment().getCode());
			} else {
				if (CollectionUtils.isNotEmpty(paymentTerms)) {
					BillingAccountWIAM.setTermePaiement(billingContactParameters,
							paymentTerms.get(0).getChoiceItem().getIdentifier().toString());
				}
			}
		} else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {
			BillingAccountWIAM.setExemptionTaxe(billingContactParameters,
					contacteDeFacturationBean.getExempte().toString());

			List<ValueChoiceItemIF> billingSupports = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.BILLING_SUPPORTS);

			ValueChoiceItemIF billingSupport = getValueChoiceItemFromList(
					contacteDeFacturationBean.getSupportDeFacturation().getCode(), billingSupports);

			if (billingSupport == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"contacteDeFacturation: supportDeFacturation "
								+ contacteDeFacturationBean.getSupportDeFacturation().getCode()
								+ "  non trouve dans le referentiel");
			}

			BillingAccountWIAM.setSupportFacture(billingContactParameters,
					contacteDeFacturationBean.getSupportDeFacturation().getCode());

			if (ArrayUtils.contains(new String[] { "L4:1345", "L4:1346" },
					contacteDeFacturationBean.getSupportDeFacturation().getCode())) {

				List<Object[]> organizations = (List<Object[]>) referentialManagerBiz
						.getItemFromReferential(Constants.SMART_FACT_ORGANIZATIONS);
				Object[] organization = getSmartFactOrgnaizationFromList(
						contacteDeFacturationBean.getOrganisation().getCode(), organizations);

				if (organization == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: organisation "
									+ contacteDeFacturationBean.getOrganisation().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setSmartFact(billingContactParameters,
						contacteDeFacturationBean.getOrganisation().getCode());
			} else {
				BillingAccountWIAM.setSmartFact(billingContactParameters, null);
			}

			if (contacteDeFacturationBean.getLangue() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getLangue().getCode())) {

				List<ValueChoiceItemIF> billingLanguages = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.BILLING_LANGUAGES);
				ValueChoiceItemIF billingLanguage = getValueChoiceItemFromList(
						contacteDeFacturationBean.getLangue().getCode(), billingLanguages);

				if (billingLanguage == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: langue " + contacteDeFacturationBean.getLangue().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setLangue(billingContactParameters, contacteDeFacturationBean.getLangue().getCode());
			}

			List<ValueChoiceItemIF> billingCycles = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.BILLING_CYCLES);

			if (contacteDeFacturationBean.getCycleDeFacturation() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getCycleDeFacturation().getCode())) {

				ValueChoiceItemIF billingCycle = getValueChoiceItemFromList(
						contacteDeFacturationBean.getCycleDeFacturation().getCode(), billingCycles);

				if (billingCycle == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: cycleDeFacturation "
									+ contacteDeFacturationBean.getCycleDeFacturation().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setCycleFacturation(billingContactParameters,
						contacteDeFacturationBean.getCycleDeFacturation().getCode());
			} else {
				if (CollectionUtils.isNotEmpty(billingCycles)) {
					BillingAccountWIAM.setCycleFacturation(billingContactParameters,
							billingCycles.get(0).getChoiceItem().getIdentifier().toString());
				}
			}

			List<ValueChoiceItemIF> paymentTerms = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.PAYMENT_TERMS);

			if (contacteDeFacturationBean.getTermeDePaiment() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getTermeDePaiment().getCode())) {

				ValueChoiceItemIF paymentMethod = getValueChoiceItemFromList(
						contacteDeFacturationBean.getTermeDePaiment().getCode(), paymentTerms);

				if (paymentMethod == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: termeDePaiment "
									+ contacteDeFacturationBean.getTermeDePaiment().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setTermePaiement(billingContactParameters,
						contacteDeFacturationBean.getTermeDePaiment().getCode());
			} else {
				if (CollectionUtils.isNotEmpty(paymentTerms)) {
					BillingAccountWIAM.setTermePaiement(billingContactParameters,
							paymentTerms.get(0).getChoiceItem().getIdentifier().toString());
				}
			}
		} else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {

			BillingAccountWIAM.setGroupemetFact(billingContactParameters,
					contacteDeFacturationBean.getGrouperFacture().toString());

			BillingAccountWIAM.setExemptionTaxe(billingContactParameters,
					contacteDeFacturationBean.getExempte().toString());

			List<ValueChoiceItemIF> billingSupports = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.BILLING_SUPPORTS);

			ValueChoiceItemIF billingSupport = getValueChoiceItemFromList(
					contacteDeFacturationBean.getSupportDeFacturation().getCode(), billingSupports);

			if (billingSupport == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"contacteDeFacturation: supportDeFacturation "
								+ contacteDeFacturationBean.getSupportDeFacturation().getCode()
								+ "  non trouve dans le referentiel");
			}

			BillingAccountWIAM.setSupportFacture(billingContactParameters,
					contacteDeFacturationBean.getSupportDeFacturation().getCode());

			if (ArrayUtils.contains(new String[] { "L4:1345", "L4:1346" },
					contacteDeFacturationBean.getSupportDeFacturation().getCode())) {

				List<Object[]> organizations = (List<Object[]>) referentialManagerBiz
						.getItemFromReferential(Constants.SMART_FACT_ORGANIZATIONS);
				Object[] organization = getSmartFactOrgnaizationFromList(
						contacteDeFacturationBean.getOrganisation().getCode(), organizations);

				if (organization == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: organisation "
									+ contacteDeFacturationBean.getOrganisation().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setSmartFact(billingContactParameters,
						contacteDeFacturationBean.getOrganisation().getCode());
			} else {
				BillingAccountWIAM.setSmartFact(billingContactParameters, null);
			}

			if (contacteDeFacturationBean.getLangue() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getLangue().getCode())) {

				List<ValueChoiceItemIF> billingLanguages = (List<ValueChoiceItemIF>) referentialManagerBiz
						.getItemFromReferential(Constants.BILLING_LANGUAGES);
				ValueChoiceItemIF billingLanguage = getValueChoiceItemFromList(
						contacteDeFacturationBean.getLangue().getCode(), billingLanguages);

				if (billingLanguage == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: langue " + contacteDeFacturationBean.getLangue().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setLangue(billingContactParameters, contacteDeFacturationBean.getLangue().getCode());
			}

			List<ValueChoiceItemIF> billingCycles = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.BILLING_CYCLES);

			if (contacteDeFacturationBean.getCycleDeFacturation() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getCycleDeFacturation().getCode())) {

				ValueChoiceItemIF billingCycle = getValueChoiceItemFromList(
						contacteDeFacturationBean.getCycleDeFacturation().getCode(), billingCycles);

				if (billingCycle == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: cycleDeFacturation "
									+ contacteDeFacturationBean.getCycleDeFacturation().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setCycleFacturation(billingContactParameters,
						contacteDeFacturationBean.getCycleDeFacturation().getCode());
			} else {
				if (CollectionUtils.isNotEmpty(billingCycles)) {
					BillingAccountWIAM.setCycleFacturation(billingContactParameters,
							billingCycles.get(0).getChoiceItem().getIdentifier().toString());
				}
			}

			List<ValueChoiceItemIF> paymentTerms = (List<ValueChoiceItemIF>) referentialManagerBiz
					.getItemFromReferential(Constants.PAYMENT_TERMS);

			if (contacteDeFacturationBean.getTermeDePaiment() != null
					&& StringUtils.isNotEmpty(contacteDeFacturationBean.getTermeDePaiment().getCode())) {

				ValueChoiceItemIF paymentMethod = getValueChoiceItemFromList(
						contacteDeFacturationBean.getTermeDePaiment().getCode(), paymentTerms);

				if (paymentMethod == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
							"contacteDeFacturation: termeDePaiment "
									+ contacteDeFacturationBean.getTermeDePaiment().getCode()
									+ "  non trouve dans le referentiel");
				}

				BillingAccountWIAM.setTermePaiement(billingContactParameters,
						contacteDeFacturationBean.getTermeDePaiment().getCode());
			} else {
				if (CollectionUtils.isNotEmpty(paymentTerms)) {
					BillingAccountWIAM.setTermePaiement(billingContactParameters,
							paymentTerms.get(0).getChoiceItem().getIdentifier().toString());
				}
			}
		}

		ParameterIF[] billingContactParameterIFs = WIAMHelper.vectorToParameter(billingContactParameters);
		return billingContactParameterIFs;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ParameterIF[] setPaymentMethodInfosForBillingContact(final MethodeDePaiementBean methodeDePaiementBean)
			throws FunctionnalException {
		OrganizationValidator.validatePaymentMethodForBillingContact(methodeDePaiementBean);

		Vector paymentMethodParameters = new Vector();

		PaymentInfoWIAM.setCodeConfidentiel(paymentMethodParameters,
				methodeDePaiementBean.getCodeConfidentiel().toString());

		if (!ArrayUtils.contains(new String[] { PaymentMethodType.WICKET.getIndex().toString(),
				PaymentMethodType.LEVY.getIndex().toString() }, methodeDePaiementBean.getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS,
					"methodeDePaiement: code " + methodeDePaiementBean.getCode() + " non trouve dans le referentiel");
		}

		if (methodeDePaiementBean.getCode().equals(PaymentMethodType.LEVY.getIndex().toString())) {

			List<Bank> banks = (List<Bank>) referentialManagerBiz.getItemFromReferential(Constants.BANKS);

			Bank bank = getBankByCode(methodeDePaiementBean.getBanque().getCode(), banks);

			if (bank == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"methodeDePaiement: banque " + methodeDePaiementBean.getBanque().getCode()
								+ "  non trouve dans le referentiel");
			}

			PaymentInfoWIAM.setCodeBanque(paymentMethodParameters, methodeDePaiementBean.getBanque().getCode());

			List<BankAgency> bankAgencies = bankAgencyDao.getAgencyByCodeAndCodeBank(
					methodeDePaiementBean.getAgenceBancaire().getCode(), methodeDePaiementBean.getBanque().getCode());

			if (CollectionUtils.isEmpty(bankAgencies)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"methodeDePaiement: agenceBancaire " + methodeDePaiementBean.getAgenceBancaire().getCode()
								+ "  non trouve dans le referentiel pour la banque "
								+ methodeDePaiementBean.getBanque().getCode());
			}

			PaymentInfoWIAM.setCodeAgence(paymentMethodParameters, methodeDePaiementBean.getAgenceBancaire().getCode());
			PaymentInfoWIAM.setNumeroCompte(paymentMethodParameters,
					methodeDePaiementBean.getNumeroDeCompte().toString());
			PaymentInfoWIAM.setCleBancaire(paymentMethodParameters, methodeDePaiementBean.getRib().toString());
			PaymentInfoWIAM.setNomTitulaireCompte(paymentMethodParameters,
					methodeDePaiementBean.getTitulaireDeCompte());
			PaymentInfoWIAM.setNumeroAutorisation(paymentMethodParameters,
					methodeDePaiementBean.getNumeroAutorisation().toString());
		}

		ParameterIF[] paymentMethodParameterIFs = WIAMHelper.vectorToParameter(paymentMethodParameters);
		return paymentMethodParameterIFs;
	}

	private void createFakeContract(final ActionMgrIF actionMgr) throws TechnicalException {
		LevelIF level = null;

		level = (LevelIF) (NodeItemIF) actionMgr.getObject();

		ActionMgrIF child = actionMgr.createChild();

		String ratePlanCode = null;
		try {
			ratePlanCode = CommercialOfferWIAM.getOrgTypeInfo(level.getType().getCode(), "CUST_GLOBAL_RTP");
		} catch (SQLException e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

		if (ratePlanCode == null) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

		ContractIF contract = level.createContract(child, null, ObjectRefMgr.getRatePlanByCode(ratePlanCode),
				ObjectRefMgr.getContractType(new LongId(0)), null, "", 'I', null);

		if (contract == null) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	private ValueChoiceItemIF getValueChoiceItemFromList(String code, List<ValueChoiceItemIF> valueChoiceItemIFs) {
		if (code != null && CollectionUtils.isNotEmpty(valueChoiceItemIFs)) {
			for (ValueChoiceItemIF valueChoiceItemIF : valueChoiceItemIFs) {
				if (code.equals(valueChoiceItemIF.getChoiceItem().getIdentifier().toString())) {
					return valueChoiceItemIF;
				}
			}
		}

		return null;
	}

	private Title getTitleFromList(Long id, List<Title> titles) {
		if (id != null && CollectionUtils.isNotEmpty(titles)) {
			for (Title title : titles) {
				if (id.equals(title.getId())) {
					return title;
				}
			}
		}

		return null;
	}

	private Bank getBankByCode(String code, List<Bank> banks) {
		if (code != null && CollectionUtils.isNotEmpty(banks)) {
			for (Bank bank : banks) {
				if (code.equals(bank.getCode())) {
					return bank;
				}
			}
		}

		return null;
	}

	private Object[] getSmartFactOrgnaizationFromList(String code, List<Object[]> organizations) {
		if (code != null && CollectionUtils.isNotEmpty(organizations)) {
			for (Object[] organization : organizations) {
				if (code.equals(organization[0])) {
					return organization;
				}
			}
		}

		return null;
	}
}
