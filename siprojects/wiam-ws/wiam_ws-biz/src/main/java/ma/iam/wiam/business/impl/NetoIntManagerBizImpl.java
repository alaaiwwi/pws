package ma.iam.wiam.business.impl;

import java.util.Collection;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContractBean;
import ma.iam.wiam.neto.bean.ServiceList;
import ma.iam.mutualisation.fwk.dao.PersistenceManager;
import ma.iam.mutualisation.fwk.dao.impl.PersistenceManagerImpl;
import ma.iam.wiam.business.NetoIntManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.dao.ContratDao;
import ma.iam.wiam.dao.NetoIntDao;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Contrat;
import ma.iam.wiam.ws.neto.model.NetoInt;

/**<!-- @todo feature/Ajouter_Neto_Int -->**/
@Service
public class NetoIntManagerBizImpl implements NetoIntManagerBiz{

	@Autowired
	private NetoIntDao netoIntDao;
	

	@Override
	public void createNetoInt(NetoInt netoint) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		try {
				netoIntDao.save(netoint);
			}catch(Throwable e) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
			}
		
	}

	@Override
	public void updateNetoInt(NetoInt netoint) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		try {
			netoIntDao.saveOrUpdate(netoint);
		}catch(Throwable e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
	}

	@Override
	public NetoInt getNetoIntByID(Long id) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		try {
			return netoIntDao.getById(id);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
	}

	@Override
	public Collection<NetoInt> getAllNetoInt() throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		try {
			return netoIntDao.getAll();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
	}
	
	
}
