package ma.iam.wiam.neto.bean;

public class InfosCommandeBean {

	private String dateDepot;
	private String dateEffet;
	private String commentaire;
	private String motif;

	public String getDateDepot() {
		return dateDepot;
	}

	public void setDateDepot(String dateDepot) {
		this.dateDepot = dateDepot;
	}

	public String getDateEffet() {
		return dateEffet;
	}

	public void setDateEffet(String dateEffet) {
		this.dateEffet = dateEffet;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getMotif() {
		return motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

}
