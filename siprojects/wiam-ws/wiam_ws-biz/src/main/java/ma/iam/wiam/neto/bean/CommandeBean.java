package ma.iam.wiam.neto.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "commande")
@XmlAccessorType(XmlAccessType.FIELD)
public class CommandeBean {

	private String ncli;
	private String nd;
	private String numCommande;
	@XmlElement(name = "addServices")
	private ServiceList servicesToAdd;
	@XmlElement(name = "updateServices")
	private ServiceList servicesToModify;
	@XmlElement(name = "removeServices")
	private ServiceList servicesToRemove;
	private InfosCommandeBean infosCommande;

	public String getNcli() {
		return ncli;
	}

	public void setNcli(String ncli) {
		this.ncli = ncli;
	}

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getNumCommande() {
		return numCommande;
	}

	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}

	public ServiceList getServicesToAdd() {
		return servicesToAdd;
	}

	public void setServicesToAdd(ServiceList servicesToAdd) {
		this.servicesToAdd = servicesToAdd;
	}

	public ServiceList getServicesToModify() {
		return servicesToModify;
	}

	public void setServicesToModify(ServiceList servicesToModify) {
		this.servicesToModify = servicesToModify;
	}

	public ServiceList getServicesToRemove() {
		return servicesToRemove;
	}

	public void setServicesToRemove(ServiceList servicesToRemove) {
		this.servicesToRemove = servicesToRemove;
	}

	public InfosCommandeBean getInfosCommande() {
		return infosCommande;
	}

	public void setInfosCommande(InfosCommandeBean infosCommande) {
		this.infosCommande = infosCommande;
	}
}
