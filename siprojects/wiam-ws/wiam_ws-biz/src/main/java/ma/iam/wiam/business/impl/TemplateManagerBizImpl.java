package ma.iam.wiam.business.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.iam.wiam.business.TemplateManagerBiz;
import ma.iam.wiam.dao.TemplateDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Template;

/**@todo : feature/Template**/
@Service
public class TemplateManagerBizImpl implements TemplateManagerBiz{

	@Autowired
	private TemplateDao templateDao;
	
	/**@todo :Begin feature/Template_CreateContractOrder**/
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ContractManagerBizImpl.class);
	private static  JAXBContext JAXB_CONTEXT = null;
	
	private static XMLInputFactory factory= null;
	static {
		try {
			JAXB_CONTEXT = JAXBContext.newInstance(CreateContractOrder.class);
			factory = XMLInputFactory.newInstance();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	/**@todo :End feature/Template_CreateContractOrder**/
	
	@Override
	public Template getTemplateById(Long id) throws FunctionnalException, TechnicalException {
		try {
			Template template  = templateDao.getById(id);
			/**@todo :Begin feature/Template_CreateContractOrder***/
			String input = template.getInputData();
			Unmarshaller unmarshaller = JAXB_CONTEXT.createUnmarshaller();
			StringReader reader = new StringReader(input);
			XMLStreamReader xmlReader = factory.createXMLStreamReader(reader); 
			JAXBElement<CreateContractOrder> jaxbElement = (JAXBElement<CreateContractOrder>) unmarshaller.unmarshal(xmlReader, CreateContractOrder.class);
			template.setCreateContractOrder(jaxbElement.getValue());
			if(template == null)
				throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
			/**@todo :End feature/Template_CreateContractOrder**/
			return template;
		} catch (ma.iam.mutualisation.fwk.common.exception.TechnicalException e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}catch (JAXBException  |XMLStreamException e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE);
		}
	}

	@Override
	public List<Template> getAllTemplate() throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		try {
			List<Template> lst = (List<Template>) templateDao.getAll();
			/**@todo :Begin feature/Template_CreateContractOrder**/
			Unmarshaller unmarshaller = JAXB_CONTEXT.createUnmarshaller();
			String input = null;
			StringReader reader = null;
			XMLStreamReader xmlReader = null;
			JAXBElement<CreateContractOrder> jaxbElement = null;
			for (Template template : lst) {
				input = template.getInputData();
				reader = new StringReader(input);
				xmlReader = factory.createXMLStreamReader(reader);
				jaxbElement = (JAXBElement<CreateContractOrder>) unmarshaller.unmarshal(xmlReader, CreateContractOrder.class);
				template.setCreateContractOrder(jaxbElement.getValue());
			}
			/**@todo :End feature/Template_CreateContractOrder**/
			if(lst==null || lst.size()<1)
				throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
			return  lst;
		}catch(ma.iam.mutualisation.fwk.common.exception.TechnicalException e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		} catch (JAXBException  |XMLStreamException e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE);
		}
	}

		

}
