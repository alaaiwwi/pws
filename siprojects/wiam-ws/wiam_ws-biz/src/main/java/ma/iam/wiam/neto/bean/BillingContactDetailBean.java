package ma.iam.wiam.neto.bean;

public class BillingContactDetailBean {

	private Integer paymentMethodCode;
	private MethodeDePaiementBean methodeDePaiementBean;
	private ContactDeFacturationBean contactDeFacturationBean;

	public Integer getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(Integer paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public MethodeDePaiementBean getMethodeDePaiementBean() {
		return methodeDePaiementBean;
	}

	public void setMethodeDePaiementBean(
			MethodeDePaiementBean methodeDePaiementBean) {
		this.methodeDePaiementBean = methodeDePaiementBean;
	}

	public ContactDeFacturationBean getContactDeFacturationBean() {
		return contactDeFacturationBean;
	}

	public void setContactDeFacturationBean(
			ContactDeFacturationBean contactDeFacturationBean) {
		this.contactDeFacturationBean = contactDeFacturationBean;
	}

}
