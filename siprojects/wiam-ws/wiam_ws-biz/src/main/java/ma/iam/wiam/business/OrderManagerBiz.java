package ma.iam.wiam.business;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.CheckOrderResponseBean;
import ma.iam.wiam.neto.bean.CommandeBean;

public interface OrderManagerBiz {

	/**
	 * Verifier la coherences de la commande avant soumission
	 * 
	 * @param commandeBean
	 * @return
	 * @throws FunctionnalException
	 * @throws TechnicalException
	 */
	CheckOrderResponseBean checkOrder(CommandeBean commandeBean)
			throws FunctionnalException, TechnicalException;

	CheckOrderResponseBean createOrder(CommandeBean commandeBean, String login)
			throws FunctionnalException, TechnicalException;

	CheckOrderResponseBean cancelOrder(CommandeBean commandeRequestBean, String login)
			throws FunctionnalException, TechnicalException;

}
