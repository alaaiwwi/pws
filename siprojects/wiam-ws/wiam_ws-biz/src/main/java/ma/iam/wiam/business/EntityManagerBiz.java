package ma.iam.wiam.business;

import java.util.List;

import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.param.criteria.BankAgencyCriteria;
import ma.iam.wiam.ws.neto.model.AppConfigSousCategorie;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;

public interface EntityManagerBiz {
	
	List<ValueChoiceItemIF> getSegments1() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getSegments2() throws FunctionnalException, TechnicalException;

	List<Object[]> getAllMTAgenciesInRegie() throws FunctionnalException, TechnicalException;

	List<ValueChoiceItemIF> getProspectionInfos() throws FunctionnalException, TechnicalException;

	List<Bank> getBanks() throws FunctionnalException, TechnicalException;

	List<BankAgency> getAgenciesByCriteria(BankAgencyCriteria bankAgencyCriteria) throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getPayerQualities() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getBillingCycles() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getPaymentTerms() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getBillingSupports() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getBillingLanguages() throws FunctionnalException, TechnicalException;
	
	//@todo feature/24:Ajouter la methode ici
	String[] getSousCategory() throws FunctionnalException, TechnicalException;
}
