package ma.iam.wiam.validators;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import ma.iam.wiam.business.constants.ProjectProperties;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.neto.bean.AdresseBean;
import ma.iam.wiam.neto.bean.ContactBean;
import ma.iam.wiam.neto.bean.ContactCommercialBean;
import ma.iam.wiam.neto.bean.ContactDeFacturationBean;
import ma.iam.wiam.neto.bean.ContactLegalBean;
import ma.iam.wiam.neto.bean.ContactTechniqueBean;
import ma.iam.wiam.neto.bean.MethodeDePaiementBean;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.util.WSValidator;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.NiveauType;
import ma.iam.wiam.ws.neto.enums.OrganizationCategoryEnum;
import ma.iam.wiam.ws.neto.enums.PaymentMethodType;

public class OrganizationValidator {
	
	private static final int IDENTIFIER_NUMBER_LENGTH = 40;
	
	private static final int CONFIDENTIAL_CODE_LENGTH = 4;
	
	private static final int BUILDING_LENGTH = 5;
	
	private static final int STAIRCASE_LENGTH = 3;
	
	private static final int FLOWER_LENGTH = 2;
	
	private static final int DOOR_LENGTH = 5;
	
	private static final int ACCOUNT_NUMBER_LENGTH = 16;
	
	private static final int RIB_LENGTH = 2;
	
	private static final String MIN_DATE = "01/01/1900";
	
	private static final SimpleDateFormat shortFormatter = new SimpleDateFormat(ProjectProperties.SHORT_FORMATTER);
	
	public static void validateOrganization(NiveauBean niveauBean, final ActionType actionType) throws FunctionnalException {
		if(niveauBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Niveau est obligatoire");
		}
		if(actionType.equals(ActionType.CREATE_CUSTOMER)) {
			if(niveauBean.getType() == null || StringUtils.isEmpty(niveauBean.getType().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs type est obligatoire");
			}
		} else if (actionType.equals(ActionType.ADD_LEVEL)) {
			if(StringUtils.isEmpty(niveauBean.getId())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Niveau: le champs id est obligatoire");
			}
			if(niveauBean.getNiveaux() == null || CollectionUtils.isEmpty(niveauBean.getNiveaux().getNiveau())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Niveau: sous-niveaux manquants");
			}
			if(niveauBean.getNiveaux().getNiveau().size() != 1) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.RESTRICTION_ERROR, "Niveau: un seul sous-niveau qui est autorise pour creation");
			}
		}
	}
	
	public static void validateSubLevel(NiveauBean niveauBean, final String organisationTypeCategoryCode, final ActionType actionType) throws FunctionnalException {
		if(niveauBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Niveau est obligatoire");
		}
		if(niveauBean.getNiveauType() == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Niveau: le champs niveauType est obligatoire");
		}
		if (ArrayUtils.contains(new String[] {OrganizationCategoryEnum.GRAND_PUBLIC.toString(), OrganizationCategoryEnum.PROFESSIONNEL.toString()}, organisationTypeCategoryCode)) {
			if(actionType.equals(ActionType.CREATE_CUSTOMER)) {
				if (!ArrayUtils.contains(new NiveauType[] {NiveauType.VOIX_DATA}, niveauBean.getNiveauType())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE, "Niveau: seuls les types de niveau: " + NiveauType.VOIX_DATA + " qui sont autorises lors de la creation du client");
				}
			} else if(actionType.equals(ActionType.ADD_LEVEL)) {
				if (!ArrayUtils.contains(new NiveauType[] {NiveauType.INTERNET, NiveauType.VOIX_DATA}, niveauBean.getNiveauType())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE, "Niveau: seuls les types de niveau: " + NiveauType.INTERNET + ", " + NiveauType.VOIX_DATA + " qui sont autorises lors de la creation du client");
				}
			}
		} else if (ArrayUtils.contains(new String[] {OrganizationCategoryEnum.ENTREPRISE.toString()}, organisationTypeCategoryCode)) {
			if (!ArrayUtils.contains(new NiveauType[] {NiveauType.INTERNET, NiveauType.VOIX_DATA, NiveauType.NIVEAU}, niveauBean.getNiveauType())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE, "Niveau: seuls les types de niveau: " + NiveauType.INTERNET + ", " + NiveauType.VOIX_DATA + ", " + NiveauType.NIVEAU +" qui sont autorises");
			}
		}
	}
	
	public static void validateAdditionalInfosForLevel(NiveauBean organisationBean, final String organisationTypeCategoryCode, final ActionType actionType) throws FunctionnalException {
		
		if(organisationBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation est null");
		}
		
		if (organisationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {
			if ("RETRAITE".equals(organisationBean.getType().getCode())) {
				if(StringUtils.isEmpty(organisationBean.getDateDeRetraite())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs dateDeRetraite est obligatoire");
				}
				if(!WSValidator.validateDate(organisationBean.getDateDeRetraite(), ProjectProperties.SHORT_FORMATTER)) {
					
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: la dateDeRetraite est invalide - format: " + ProjectProperties.SHORT_FORMATTER);
				} 
				if(!WSValidator.isDateBetween(organisationBean.getDateDeRetraite(), MIN_DATE, shortFormatter.format(new Date()))) {
					
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateDeRetraite doit avoir une date entre " + MIN_DATE + " et " + shortFormatter.format(new Date()));
				}
			}
			if(organisationBean.getEntiteGestionnaire() == null || StringUtils.isEmpty(organisationBean.getEntiteGestionnaire().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs entiteGestionnaire est obligatoire");
			}
			if(organisationBean.getInfoDeProspection() == null || StringUtils.isEmpty(organisationBean.getInfoDeProspection().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs InfoDeProspection est obligatoire");
			}
			if(organisationBean.getContactLegal() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal is obligatoire");
			}
			if(organisationBean.getContactDeFacturation() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactDeFacturation is obligatoire");
			}
			if(organisationBean.getNiveaux() == null || CollectionUtils.isEmpty(organisationBean.getNiveaux().getNiveau())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: il faut au moins un site voix-data pour creer le client");
			}
			if(organisationBean.getNiveaux().getNiveau().size() != 1) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: un seul site de type voix-data est autorise pour creer le client");
			}
		} else if (organisationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {
			if(organisationBean.getEntiteGestionnaire() == null || StringUtils.isEmpty(organisationBean.getEntiteGestionnaire().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs entiteGestionnaire est obligatoire");
			}
			if(organisationBean.getInfoDeProspection() == null || StringUtils.isEmpty(organisationBean.getInfoDeProspection().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs infoDeProspection est obligatoire");
			}
			if(organisationBean.getContactLegal() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal is obligatoire");
			}
			if(organisationBean.getContactDeFacturation() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactDeFacturation is obligatoire");
			}
			if(organisationBean.getNiveaux() == null || CollectionUtils.isEmpty(organisationBean.getNiveaux().getNiveau())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: il faut au moins un site voix-data pour creer le client");
			}
			if(organisationBean.getNiveaux().getNiveau().size() != 1) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: un seul site de type voix-data est autorise pour creer le client");
			}
		} else if (organisationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {
			if(StringUtils.isEmpty(organisationBean.getCcu())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs CCU est obligatoire");
			}
			if(organisationBean.getEntiteGestionnaire() == null || StringUtils.isEmpty(organisationBean.getEntiteGestionnaire().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs entiteGestionnaire est obligatoire");
			}
			if(organisationBean.getInfoDeProspection() == null || StringUtils.isEmpty(organisationBean.getInfoDeProspection().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs infoDeProspection est obligatoire");
			}
			if(organisationBean.getContactLegal() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal is obligatoire");
			}
			if(organisationBean.getContactDeFacturation() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactDeFacturation is obligatoire");
			}
			if(actionType.equals(ActionType.CREATE_CUSTOMER)) {
				if(StringUtils.isEmpty(organisationBean.getActivite())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs activite est obligatoire");
				}
				if(organisationBean.getProssesusDecisionnelCentralise() == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs prossesusDecisionnelCentralise est obligatoire");
				}
				if(StringUtils.isNotEmpty(organisationBean.getEffectif()) && !WSValidator.validateNumber(organisationBean.getEffectif())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs effectif doit etre numerique");
				}
				if(organisationBean.getMultinationale() == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs multinationale est obligatoire");
				}
				if(organisationBean.getSecteurConcurrentiel() == null) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs secteurConcurrentiel est obligatoire");
				}
			}
			if(organisationBean.getAcIcgc() == null || organisationBean.getAcIcgc().equals(0)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: le champs acIcgc est obligatoire");
			}
			if(organisationBean.getContactTechnique() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactTechnique is obligatoire");
			}
			if(organisationBean.getContactCommercial() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactCommercial is obligatoire");
			}
			if(organisationBean.getNiveaux() != null && CollectionUtils.isNotEmpty(organisationBean.getNiveaux().getNiveau())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Organisation: un client " + organisationTypeCategoryCode + " ne peut pas avoir des sous-niveaux durant sa creation");
			}
		}
	}
	
	public static void validateAdditionalInfosForSite(NiveauBean niveauBean, final String OrganisationTypeCategoryCode) throws FunctionnalException {
		
		if(niveauBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Site est null");
		}
	}
	
	public static void validateContact(final ContactBean contactBean, final String  OrganisationTypeCategoryCode) throws FunctionnalException {
		
		if(contactBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "Contact ne peut pas etre null");
		}
		
		if (OrganisationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {
			if (contactBean.getCodePersonne() == null || StringUtils.isEmpty(contactBean.getCodePersonne().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs codePersonne est obligatoire");
			}
			if ((contactBean instanceof ContactLegalBean) && StringUtils.isEmpty(contactBean.getPrenom())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs prenom est obligatoire");
			}
			if (StringUtils.isEmpty(contactBean.getNom())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs nom est obligatoire");
			}
			if(StringUtils.isNotEmpty(contactBean.getEmail()) && !WSValidator.validateMail(contactBean.getEmail())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": email non valide format: xxxx@xxxx.xxx");
			}
			if (StringUtils.isEmpty(contactBean.getTelephoneMobile()) && StringUtils.isEmpty(contactBean.getTelephoneFix())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": il faut au moins mentionner soit le telephoneFix ou telephoneMobile");
			}
			if(StringUtils.isNotEmpty(contactBean.getTelephoneMobile()) && !WSValidator.validateMobileNumber(contactBean.getTelephoneMobile())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneMobile non valide format: (0\\00212\\+212\\212) 6XX XX XX XX");
			}
			if(StringUtils.isNotEmpty(contactBean.getTelephoneFix()) && !WSValidator.validateFixNumber(contactBean.getTelephoneFix())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneFix non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
			}
			if(StringUtils.isNotEmpty(contactBean.getFax()) && !WSValidator.validateFixNumber(contactBean.getFax())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": fax non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
			}
		}
		
		if (OrganisationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {
			if (StringUtils.isEmpty(contactBean.getRaisonSocial())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs raisonSocial est obligatoire");
			}
			if (contactBean.getCodePersonne() == null || StringUtils.isEmpty(contactBean.getCodePersonne().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs codePersonne est obligatoire");
			}
			if ((contactBean instanceof ContactLegalBean) && StringUtils.isEmpty(contactBean.getPrenom())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs prenom est obligatoire");
			}
			if (StringUtils.isEmpty(contactBean.getNom())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs nom est obligatoire");
			}
			if(StringUtils.isNotEmpty(contactBean.getEmail()) && !WSValidator.validateMail(contactBean.getEmail())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": email non valide format: xxxx@xxxx.xxx");
			}
			if (StringUtils.isEmpty(contactBean.getTelephoneMobile()) && StringUtils.isEmpty(contactBean.getTelephoneFix())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": il faut au moins mentionner soit le telephoneFix ou telephoneMobile");
			}
			if(StringUtils.isNotEmpty(contactBean.getTelephoneMobile()) && !WSValidator.validateMobileNumber(contactBean.getTelephoneMobile())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneMobile non valide format: (0\\00212\\+212\\212) 6XX XX XX XX");
			}
			if(StringUtils.isNotEmpty(contactBean.getTelephoneFix()) && !WSValidator.validateFixNumber(contactBean.getTelephoneFix())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneFix non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
			}
			if(StringUtils.isNotEmpty(contactBean.getFax()) && !WSValidator.validateFixNumber(contactBean.getFax())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": fax non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
			}
		}
		
		if (OrganisationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {
			if((contactBean instanceof ContactLegalBean) || (contactBean instanceof ContactDeFacturationBean)) {
				if (StringUtils.isEmpty(contactBean.getRaisonSocial())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs raisonSocial est obligatoire");
				}
				if (StringUtils.isEmpty(contactBean.getNom())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs nom est obligatoire");
				}
				if(StringUtils.isNotEmpty(contactBean.getEmail()) && !WSValidator.validateMail(contactBean.getEmail())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": email non valide format: xxxx@xxxx.xxx");
				}
				if (StringUtils.isEmpty(contactBean.getTelephoneMobile()) && StringUtils.isEmpty(contactBean.getTelephoneFix())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": il faut au moins mentionner soit le telephoneFix ou telephoneMobile");
				}
				if(StringUtils.isNotEmpty(contactBean.getTelephoneMobile()) && !WSValidator.validateMobileNumber(contactBean.getTelephoneMobile())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneMobile non valide format: (0\\00212\\+212\\212) 6XX XX XX XX");
				}
				if(StringUtils.isNotEmpty(contactBean.getTelephoneFix()) && !WSValidator.validateFixNumber(contactBean.getTelephoneFix())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneFix non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
				}
				if(StringUtils.isNotEmpty(contactBean.getFax()) && !WSValidator.validateFixNumber(contactBean.getFax())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": fax non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
				}
			} else if((contactBean instanceof ContactTechniqueBean) || (contactBean instanceof ContactCommercialBean)) {
				if (StringUtils.isEmpty(contactBean.getPrenom())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs prenom est obligatoire");
				}
				if (StringUtils.isEmpty(contactBean.getNom())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": le champs nom est obligatoire");
				}
				if(StringUtils.isNotEmpty(contactBean.getEmail()) && !WSValidator.validateMail(contactBean.getEmail())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": email non valide format: xxxx@xxxx.xxx");
				}
				if (StringUtils.isEmpty(contactBean.getTelephoneMobile()) && StringUtils.isEmpty(contactBean.getTelephoneFix())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": il faut au moins mentionner soit le telephoneFix ou telephoneMobile");
				}
				if(StringUtils.isNotEmpty(contactBean.getTelephoneMobile()) && !WSValidator.validateMobileNumber(contactBean.getTelephoneMobile())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneMobile non valide format: (0\\00212\\+212\\212) 6XX XX XX XX");
				}
				if(StringUtils.isNotEmpty(contactBean.getTelephoneFix()) && !WSValidator.validateFixNumber(contactBean.getTelephoneFix())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": telephoneFix non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
				}
				if(StringUtils.isNotEmpty(contactBean.getFax()) && !WSValidator.validateFixNumber(contactBean.getFax())) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, contactBean.getClass().getSimpleName() + ": fax non valide format: (0\\00212\\+212\\212) 5XX XX XX XX");
				}
			}
		}
	}
	
	public static void validateAdditionnalInfosForLegalContact(final ContactLegalBean contactLegalBean, final String  OrganisationTypeCategoryCode) throws FunctionnalException {
		if(contactLegalBean == null ) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal est obligatoire");
		}
		if (OrganisationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {
			if(contactLegalBean.getSexe() == null || StringUtils.isEmpty(contactLegalBean.getSexe().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs sexe est obligatoire");
			}
			if(contactLegalBean.getTypeIdentifiant() == null || StringUtils.isEmpty(contactLegalBean.getTypeIdentifiant().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs typeIdentifiant est obligatoire");
			}
			if(!ArrayUtils.contains(new String[] {"L4:1092"}, contactLegalBean.getTypeIdentifiant().getCode()) 
					&& StringUtils.isEmpty(contactLegalBean.getNumeroIdentifiant())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant est obligatoire");
			}
			if(!ArrayUtils.contains(new String[] {"L4:1092"}, contactLegalBean.getTypeIdentifiant().getCode()) 
					&& !WSValidator.isLengthLessThan(contactLegalBean.getNumeroIdentifiant(), IDENTIFIER_NUMBER_LENGTH)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant doit contenir moins de " + IDENTIFIER_NUMBER_LENGTH + " caracteres");
			}
			if(StringUtils.isNotEmpty(contactLegalBean.getDateDeNaissance()) 
					&& !WSValidator.validateDate(contactLegalBean.getDateDeNaissance(), ProjectProperties.SHORT_FORMATTER)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: la dateDeNaissance est invalide - format: " + ProjectProperties.SHORT_FORMATTER);
			} 
			if(StringUtils.isNotEmpty(contactLegalBean.getDateDeNaissance()) 
					&& !WSValidator.isDateBetween(contactLegalBean.getDateDeNaissance(), MIN_DATE, shortFormatter.format(new Date()))) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateDeNaissance doit avoir une date entre " + MIN_DATE + " et " + shortFormatter.format(new Date()));
			}
			if(StringUtils.isNotEmpty(contactLegalBean.getDateObtention()) 
					&& !WSValidator.validateDate(contactLegalBean.getDateObtention(), ProjectProperties.SHORT_FORMATTER)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: la dateObtention est invalide - format: " + ProjectProperties.SHORT_FORMATTER);
			} 
			if(StringUtils.isNotEmpty(contactLegalBean.getDateObtention()) 
					&& !WSValidator.isDateBetween(contactLegalBean.getDateObtention(), MIN_DATE, shortFormatter.format(new Date()))) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateObtention doit avoir une date entre " + MIN_DATE + " et " + shortFormatter.format(new Date()));
			}
		} else if (OrganisationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {
			if(contactLegalBean.getSexe() == null || StringUtils.isEmpty(contactLegalBean.getSexe().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs sexe est obligatoire");
			}
			if(contactLegalBean.getTypeIdentifiant() == null || StringUtils.isEmpty(contactLegalBean.getTypeIdentifiant().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs typeIdentifiant est obligatoire");
			}
			if(!ArrayUtils.contains(new String[] {"L4:1159"}, contactLegalBean.getTypeIdentifiant().getCode()) 
					&& StringUtils.isEmpty(contactLegalBean.getNumeroIdentifiant())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant est obligatoire");
			}
			if(!ArrayUtils.contains(new String[] {"L4:1159"}, contactLegalBean.getTypeIdentifiant().getCode()) 
					&& !WSValidator.isLengthLessThan(contactLegalBean.getNumeroIdentifiant(), IDENTIFIER_NUMBER_LENGTH)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant doit contenir moins de " + IDENTIFIER_NUMBER_LENGTH + " caracteres");
			}
			if(StringUtils.isNotEmpty(contactLegalBean.getDateDeNaissance())
					&& !WSValidator.validateDate(contactLegalBean.getDateDeNaissance(), ProjectProperties.SHORT_FORMATTER)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: la dateDeNaissance " + contactLegalBean.getDateDeNaissance() + " est invalide - format: " + ProjectProperties.SHORT_FORMATTER);
			} 
			if(StringUtils.isNotEmpty(contactLegalBean.getDateDeNaissance())
					&& !WSValidator.isDateBetween(contactLegalBean.getDateDeNaissance(), MIN_DATE, shortFormatter.format(new Date()))) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateDeNaissance doit avoir une date entre " + MIN_DATE + " et " + shortFormatter.format(new Date()));
			}
			if(StringUtils.isEmpty(contactLegalBean.getDateObtention())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateObtention est obligatoire");
			}
			if(!WSValidator.validateDate(contactLegalBean.getDateObtention(), ProjectProperties.SHORT_FORMATTER)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: la dateObtention est invalide - format: " + ProjectProperties.SHORT_FORMATTER);
			} 
			if(!WSValidator.isDateBetween(contactLegalBean.getDateObtention(), MIN_DATE, shortFormatter.format(new Date()))) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateObtention doit avoir une date entre " + MIN_DATE + " et " + shortFormatter.format(new Date()));
			}
		} else if (OrganisationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {
			if(contactLegalBean.getTypeIdentifiant() == null || StringUtils.isEmpty(contactLegalBean.getTypeIdentifiant().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs typeIdentifiant est obligatoire");
			}
			if(!ArrayUtils.contains(new String[] {"L4:1219"}, contactLegalBean.getTypeIdentifiant().getCode()) 
					&& StringUtils.isEmpty(contactLegalBean.getNumeroIdentifiant())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant est obligatoire");
			}
			if(!ArrayUtils.contains(new String[] {"L4:1219"}, contactLegalBean.getTypeIdentifiant().getCode()) 
					&& !WSValidator.isLengthLessThan(contactLegalBean.getNumeroIdentifiant(), IDENTIFIER_NUMBER_LENGTH)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant doit contenir moins de " + IDENTIFIER_NUMBER_LENGTH + " caracteres");
			}
			if(!WSValidator.isLengthLessThan(contactLegalBean.getNumeroIdentifiant(), IDENTIFIER_NUMBER_LENGTH)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs numeroIdentifiant doit contenir moins de " + IDENTIFIER_NUMBER_LENGTH + " caracteres");
			}
			if(StringUtils.isEmpty(contactLegalBean.getDateObtention())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateObtention est obligatoire");
			}
			if(!WSValidator.validateDate(contactLegalBean.getDateObtention(), ProjectProperties.SHORT_FORMATTER)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: la dateObtention est invalide - format: " + ProjectProperties.SHORT_FORMATTER);
			} 
			if(!WSValidator.isDateBetween(contactLegalBean.getDateObtention(), MIN_DATE, shortFormatter.format(new Date()))) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contactLegal: le champs dateObtention doit avoir une date entre " + MIN_DATE + " et " + shortFormatter.format(new Date()));
			}
		}
	}
	
	public static void validateAddress(final AdresseBean adresseBean, final String  OrganisationTypeCategoryCode) throws FunctionnalException {
		if(adresseBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs cannot be null");
		}
		if(adresseBean.getPays() == null || StringUtils.isEmpty(adresseBean.getPays().getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs pays est obligatoire");
		}
		if(adresseBean.getCommune() == null || StringUtils.isEmpty(adresseBean.getCommune().getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs commune est obligatoire");
		}
		if(adresseBean.getQuartier() == null || StringUtils.isEmpty(adresseBean.getQuartier().getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs quartier est obligatoire");
		}
		if(adresseBean.getVoie() == null || StringUtils.isEmpty(adresseBean.getVoie().getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs voie est obligatoire");
		}
		if(StringUtils.isEmpty(adresseBean.getCodePostale())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs codePostale est obligatoire");
		}
	}
	
	public static void validateAdditionnalInfosFroAddress(final AdresseBean adresseBean) throws FunctionnalException {
		if(adresseBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs cannot be null");
		}
		if(StringUtils.isNotEmpty(adresseBean.getBatiment()) && !WSValidator.isLengthLessThan(adresseBean.getBatiment(), BUILDING_LENGTH)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs batiment doit contenir au maximum " + BUILDING_LENGTH + " chiffres");
		}
		if(StringUtils.isNotEmpty(adresseBean.getEscalier()) && !WSValidator.isLengthLessThan(adresseBean.getEscalier(), STAIRCASE_LENGTH)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs escalier doit contenir au maximum " + STAIRCASE_LENGTH + " chiffres");
		}
		if(StringUtils.isNotEmpty(adresseBean.getEtage()) && !WSValidator.isLengthLessThan(adresseBean.getEtage(), FLOWER_LENGTH)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs etage doit contenir au maximum " + FLOWER_LENGTH + " chiffres");
		}
		if(StringUtils.isNotEmpty(adresseBean.getPorte()) && !WSValidator.isLengthLessThan(adresseBean.getPorte(), DOOR_LENGTH)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "address: le champs porte doit contenir au maximum " + DOOR_LENGTH + " chiffres");
		}
	}
	
	public static void validateAdditionnalInfosForBillingContact(final ContactDeFacturationBean contacteDeFacturationBean, final String organizationTypeCategoryCode) throws FunctionnalException {
		
		if(contacteDeFacturationBean == null ) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation est obligatoire");
		}
		if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {
			if (contacteDeFacturationBean.getExempte() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs exempte est obligatoire");
			}
			if (contacteDeFacturationBean.getSupportDeFacturation() == null || StringUtils.isEmpty(contacteDeFacturationBean.getSupportDeFacturation().getCode())) {
				
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs supportDeFacturation est obligatoire");
			}
			if((contacteDeFacturationBean.getOrganisation() == null || StringUtils.isEmpty(contacteDeFacturationBean.getOrganisation().getCode())) 
					&& ArrayUtils.contains(new String[] {"L4:1345", "L4:1346"},  contacteDeFacturationBean.getSupportDeFacturation().getCode())) {
				
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs organisation est obligatoire pour le support de facturation " + contacteDeFacturationBean.getSupportDeFacturation().getCode());
			}
		} else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {
			if (contacteDeFacturationBean.getExempte() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs exempte est obligatoire");
			}
			if (contacteDeFacturationBean.getSupportDeFacturation() == null || StringUtils.isEmpty(contacteDeFacturationBean.getSupportDeFacturation().getCode())) {
				
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs supportDeFacturation est obligatoire");
			}
			if((contacteDeFacturationBean.getOrganisation() == null || StringUtils.isEmpty(contacteDeFacturationBean.getOrganisation().getCode())) 
					&& ArrayUtils.contains(new String[] {"L4:1345", "L4:1346"},  contacteDeFacturationBean.getSupportDeFacturation().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs organisation est obligatoire pour le support de facturation " + contacteDeFacturationBean.getSupportDeFacturation().getCode());
			}
		} else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {
			if (contacteDeFacturationBean.getGrouperFacture() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs grouperFacture est obligatoire");
			}
			if (contacteDeFacturationBean.getExempte() == null) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs exempte est obligatoire");
			}
			if (contacteDeFacturationBean.getSupportDeFacturation() == null || StringUtils.isEmpty(contacteDeFacturationBean.getSupportDeFacturation().getCode())) {
				
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs supportDeFacturation est obligatoire");
			}
			if((contacteDeFacturationBean.getOrganisation() == null || StringUtils.isEmpty(contacteDeFacturationBean.getOrganisation().getCode())) 
					&& ArrayUtils.contains(new String[] {"L4:1345", "L4:1346"},  contacteDeFacturationBean.getSupportDeFacturation().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs organisation est obligatoire pour le support de facturation " + contacteDeFacturationBean.getSupportDeFacturation().getCode());
			}
		}
		
	}
	
	public static void validatePaymentMethodForBillingContact(final MethodeDePaiementBean methodeDePaiementBean) throws FunctionnalException {
		
		if(methodeDePaiementBean == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement est obligatoire");
		}
		if (StringUtils.isEmpty(methodeDePaiementBean.getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs code est obligatoire");
		}
		if (StringUtils.isEmpty(methodeDePaiementBean.getCodeConfidentiel())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs codeConfidentiel est obligatoire");
		}
		if (!WSValidator.validateNumber(methodeDePaiementBean.getCodeConfidentiel())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs codeConfidentiel doit etre numerique");
		}
		if (!WSValidator.validateNumberLength(methodeDePaiementBean.getCodeConfidentiel(), CONFIDENTIAL_CODE_LENGTH)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs codeConfidentiel doit contenir " + CONFIDENTIAL_CODE_LENGTH + " chiffres");
		}
		
		if (methodeDePaiementBean.getCode().equals(PaymentMethodType.LEVY.getIndex().toString())) {
			if (methodeDePaiementBean.getBanque() == null || StringUtils.isEmpty(methodeDePaiementBean.getBanque().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs banque est obligatoire");
			}
			if (methodeDePaiementBean.getAgenceBancaire() == null || StringUtils.isEmpty(methodeDePaiementBean.getAgenceBancaire().getCode())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs agenceBancaire est obligatoire");
			}
			if (StringUtils.isEmpty(methodeDePaiementBean.getNumeroDeCompte())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "contacteDeFacturation: le champs numeroDeCompte est obligatoire");
			}
			if (!WSValidator.validateNumber(methodeDePaiementBean.getNumeroDeCompte())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs numeroDeCompte doit etre numerique");
			}
			if (!WSValidator.isLengthLessThan(methodeDePaiementBean.getNumeroDeCompte(), ACCOUNT_NUMBER_LENGTH)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs numeroDeCompte doit contenir moins de " + ACCOUNT_NUMBER_LENGTH + " chiffres");
			}
			if (StringUtils.isEmpty(methodeDePaiementBean.getRib())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs rib est obligatoire");
			}
			if (!WSValidator.validateNumber(methodeDePaiementBean.getRib())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs rib doit etre numerique");
			}
			if (!WSValidator.isLengthLessThan(methodeDePaiementBean.getRib(), RIB_LENGTH)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs rib doit contenir moins de " + RIB_LENGTH + " chiffres");
			}
			if (StringUtils.isEmpty(methodeDePaiementBean.getTitulaireDeCompte())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs titulaireDeCompte est obligatoire");
			}
			if (!WSValidator.validateNonNumeric(methodeDePaiementBean.getTitulaireDeCompte())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs titulaireDeCompte doit contenir des characteres non numeriques");
			}
			if (StringUtils.isEmpty(methodeDePaiementBean.getNumeroAutorisation())) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.REQUIRED_FIELD_WS, "methodeDePaiement: le champs numeroAutorisation est obligatoire");
			}
		}
	}
}
