package ma.iam.wiam.neto.bean;

import java.io.Serializable;

public class CheckOrderResponseBean implements Serializable {

	private static final long serialVersionUID = -2321440492576928252L;

	private String orderId;
	private Boolean isValid;
	private ErrorList errorList;
	private String orderStatus;

	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public ErrorList getErrorList() {
		return errorList;
	}

	public void setErrorList(ErrorList errorList) {
		this.errorList = errorList;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

}
