package ma.iam.wiam.util;

import java.util.Map;
import java.util.StringTokenizer;

import ma.iam.wiam.business.impl.OrderManagerBizImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

	/**
	 * Batch Util Class extracted from wiam JSPs
	 * 
	 * @param map
	 * @param path
	 * @return
	 */
	public static Object getObjectFromMap(Map map, String path) {
		Map newMap = map;
		StringTokenizer tokenizer = new StringTokenizer(path, ":");
		int n = tokenizer.countTokens();
		for (int i = 1; i < n; i++) {
			newMap = (Map) newMap.get(tokenizer.nextToken());
			if (newMap == null) {
				// Can't find target in Map
				LOGGER.info("Can't find target in Map");
				return null;
			}
		}
		return newMap.get(tokenizer.nextToken());
	}
}
