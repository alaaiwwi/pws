package ma.iam.wiam.neto.bean;

import java.io.Serializable;
import java.util.List;

public class ErrorList implements Serializable {

	private static final long serialVersionUID = 8344340321277307635L;

	private List<String> errorMessages;

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

}
