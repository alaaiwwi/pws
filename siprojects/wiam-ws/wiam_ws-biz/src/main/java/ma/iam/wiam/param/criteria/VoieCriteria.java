package ma.iam.wiam.param.criteria;

public class VoieCriteria {
	private String codeCommune;
	private String codeQuartier;
	public String getCodeCommune() {
		return codeCommune;
	}
	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}
	public String getCodeQuartier() {
		return codeQuartier;
	}
	public void setCodeQuartier(String codeQuartier) {
		this.codeQuartier = codeQuartier;
	}
	
}
