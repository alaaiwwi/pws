package ma.iam.wiam.neto.bean;

public class MethodeDePaiementBean {

	private String code;
	private String codeConfidentiel;
	private ReferentialBean banque;
	private ReferentialBean agenceBancaire;
	private String numeroDeCompte;
	private String rib;
	private String titulaireDeCompte;
	private String numeroAutorisation;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeConfidentiel() {
		return codeConfidentiel;
	}

	public void setCodeConfidentiel(String codeConfidentiel) {
		this.codeConfidentiel = codeConfidentiel;
	}

	public ReferentialBean getBanque() {
		return banque;
	}

	public void setBanque(ReferentialBean banque) {
		this.banque = banque;
	}

	public ReferentialBean getAgenceBancaire() {
		return agenceBancaire;
	}

	public void setAgenceBancaire(ReferentialBean agenceBancaire) {
		this.agenceBancaire = agenceBancaire;
	}

	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}

	public void setNumeroDeCompte(String numeroDeCompte) {
		this.numeroDeCompte = numeroDeCompte;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public String getTitulaireDeCompte() {
		return titulaireDeCompte;
	}

	public void setTitulaireDeCompte(String titulaireDeCompte) {
		this.titulaireDeCompte = titulaireDeCompte;
	}

	public String getNumeroAutorisation() {
		return numeroAutorisation;
	}

	public void setNumeroAutorisation(String numeroAutorisation) {
		this.numeroAutorisation = numeroAutorisation;
	}
}