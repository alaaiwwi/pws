package ma.iam.wiam.interceptor;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.common.injection.NoJSR250Annotations;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;

@NoJSR250Annotations
public class ExceptionInterceptor extends AbstractSoapInterceptor {

	private final static int HTTP_200 = 200;

	public ExceptionInterceptor() {
		super(Phase.PRE_LOGICAL);
	}

	// @Override
	public void handleMessage(SoapMessage message) throws Fault {
		Fault fault = (Fault) message.getContent(Exception.class);
		Throwable ex = fault.getCause();
		if (ex instanceof FunctionnalException) {
			FunctionnalException e = (FunctionnalException) ex;
			generateSoapFault(fault, e);
		}

		else {
			TechnicalException e = new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS, ex);
			generateSoapFault(fault, e);
		}
	}

	private void generateSoapFault(Fault fault, FunctionnalException e) {
		fault.setFaultCode(createQName(e.getFaultInfo().getCode() != null ? e.getFaultInfo().getCode() : "WS-ERR"));
		fault.setMessage(e.getFaultInfo().getContext());
		fault.setStatusCode(HTTP_200);
	}

	private void generateSoapFault(Fault fault, TechnicalException e) {
		fault.setFaultCode(createQName(e.getFaultInfo().getCode() != null ? e.getFaultInfo().getCode() : "WS-ERR"));
		fault.setMessage(e.getFaultInfo().getContext());
		Element detail = fault.getOrCreateDetail();
		Document ownerDocument = detail.getOwnerDocument();
		Element technicalException = ownerDocument.createElementNS("ws.iam.ma", "TechnicalException");
		Element codeException = technicalException.getOwnerDocument().createElementNS("ws.iam.ma", "code");
		codeException.appendChild(ownerDocument.createTextNode("" + e.getFaultInfo().getCode()));
		Element contextException = technicalException.getOwnerDocument().createElementNS("ws.iam.ma", "context");
		contextException.appendChild(ownerDocument.createTextNode("" + e.getFaultInfo().getContext()));
		Element msgException = technicalException.getOwnerDocument().createElementNS("ws.iam.ma", "message");

		msgException.appendChild(ownerDocument.createTextNode("" + e.getMessage()));// (e.getMessage());
		technicalException.appendChild(codeException);
		technicalException.appendChild(contextException);
		technicalException.appendChild(msgException);
		detail.appendChild(technicalException);

	}

	private static QName createQName(String errorCode) {
		return new QName("ws.iam.ma", String.valueOf(errorCode));
	}

	private void generateSoapFault(Fault fault, Throwable e) {
		fault.setFaultCode(createQName("WS-ERR"));
		fault.setMessage(e.getMessage());
	}
}
