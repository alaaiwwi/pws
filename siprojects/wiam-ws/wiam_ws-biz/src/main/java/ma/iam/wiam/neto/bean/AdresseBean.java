package ma.iam.wiam.neto.bean;

public class AdresseBean {
	
	private ReferentialBean pays;
	private ReferentialBean commune;
	private String codePostale;
	private ReferentialBean quartier;
	private String quartierNonCodifie;
	private ReferentialBean voie;
	private String voieNonCodifie;
	private String numeroVoie;
	
	//Additionnal Infos
	private String communeEtrangere;
	private String batiment;
	private String escalier;
	private String etage;
	private String porte;
	private ReferentialBean typeDestribution;
	private String boitePostale;
	private String complementAdresse;
	
	public ReferentialBean getPays() {
		return pays;
	}
	public void setPays(ReferentialBean pays) {
		this.pays = pays;
	}
	public ReferentialBean getCommune() {
		return commune;
	}
	public void setCommune(ReferentialBean commune) {
		this.commune = commune;
	}
	public String getCodePostale() {
		return codePostale;
	}
	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}
	public ReferentialBean getQuartier() {
		return quartier;
	}
	public void setQuartier(ReferentialBean quartier) {
		this.quartier = quartier;
	}
	public String getQuartierNonCodifie() {
		return quartierNonCodifie;
	}
	public void setQuartierNonCodifie(String quartierNonCodifie) {
		this.quartierNonCodifie = quartierNonCodifie;
	}
	public ReferentialBean getVoie() {
		return voie;
	}
	public void setVoie(ReferentialBean voie) {
		this.voie = voie;
	}
	public String getVoieNonCodifie() {
		return voieNonCodifie;
	}
	public void setVoieNonCodifie(String voieNonCodifie) {
		this.voieNonCodifie = voieNonCodifie;
	}
	public String getNumeroVoie() {
		return numeroVoie;
	}
	public void setNumeroVoie(String numeroVoie) {
		this.numeroVoie = numeroVoie;
	}
	public String getCommuneEtrangere() {
		return communeEtrangere;
	}
	public void setCommuneEtrangere(String communeEtrangere) {
		this.communeEtrangere = communeEtrangere;
	}
	public String getBatiment() {
		return batiment;
	}
	public void setBatiment(String batiment) {
		this.batiment = batiment;
	}
	public String getEscalier() {
		return escalier;
	}
	public void setEscalier(String escalier) {
		this.escalier = escalier;
	}
	public String getEtage() {
		return etage;
	}
	public void setEtage(String etage) {
		this.etage = etage;
	}
	public String getPorte() {
		return porte;
	}
	public void setPorte(String porte) {
		this.porte = porte;
	}
	public ReferentialBean getTypeDestribution() {
		return typeDestribution;
	}
	public void setTypeDestribution(ReferentialBean typeDestribution) {
		this.typeDestribution = typeDestribution;
	}
	public String getBoitePostale() {
		return boitePostale;
	}
	public void setBoitePostale(String boitePostale) {
		this.boitePostale = boitePostale;
	}
	public String getComplementAdresse() {
		return complementAdresse;
	}
	public void setComplementAdresse(String complementAdresse) {
		this.complementAdresse = complementAdresse;
	}
}
