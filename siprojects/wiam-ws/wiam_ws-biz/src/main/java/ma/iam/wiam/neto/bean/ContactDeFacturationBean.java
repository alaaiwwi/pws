package ma.iam.wiam.neto.bean;

public class ContactDeFacturationBean extends ContactBean {

	private Boolean grouperFacture;
	private Boolean exempte;
	private ReferentialBean supportDeFacturation;
	private ReferentialBean organisation;
	private ReferentialBean langue;
	private ReferentialBean cycleDeFacturation;
	private ReferentialBean termeDePaiment;
	private String codeConfidentiel;
	private MethodeDePaiementBean methodeDePaiement;
	private AdresseBean adresse;

	public Boolean getGrouperFacture() {
		return grouperFacture;
	}

	public void setGrouperFacture(Boolean grouperFacture) {
		this.grouperFacture = grouperFacture;
	}

	public Boolean getExempte() {
		return exempte;
	}

	public void setExempte(Boolean exempte) {
		this.exempte = exempte;
	}

	public ReferentialBean getSupportDeFacturation() {
		return supportDeFacturation;
	}

	public void setSupportDeFacturation(ReferentialBean supportDeFacturation) {
		this.supportDeFacturation = supportDeFacturation;
	}

	public ReferentialBean getOrganisation() {
		return organisation;
	}

	public void setOrganisation(ReferentialBean organisation) {
		this.organisation = organisation;
	}

	public ReferentialBean getLangue() {
		return langue;
	}

	public void setLangue(ReferentialBean langue) {
		this.langue = langue;
	}

	public ReferentialBean getCycleDeFacturation() {
		return cycleDeFacturation;
	}

	public void setCycleDeFacturation(ReferentialBean cycleDeFacturation) {
		this.cycleDeFacturation = cycleDeFacturation;
	}

	public ReferentialBean getTermeDePaiment() {
		return termeDePaiment;
	}

	public void setTermeDePaiment(ReferentialBean termeDePaiment) {
		this.termeDePaiment = termeDePaiment;
	}

	public String getCodeConfidentiel() {
		return codeConfidentiel;
	}

	public void setCodeConfidentiel(String codeConfidentiel) {
		this.codeConfidentiel = codeConfidentiel;
	}

	public MethodeDePaiementBean getMethodeDePaiement() {
		return methodeDePaiement;
	}

	public void setMethodeDePaiement(MethodeDePaiementBean methodeDePaiement) {
		this.methodeDePaiement = methodeDePaiement;
	}

	public AdresseBean getAdresse() {
		return adresse;
	}

	public void setAdresse(AdresseBean adresse) {
		this.adresse = adresse;
	}
}