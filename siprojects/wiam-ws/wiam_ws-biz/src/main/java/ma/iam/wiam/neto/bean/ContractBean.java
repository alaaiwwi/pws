package ma.iam.wiam.neto.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**Begin feature/Recherche_Contrat_Client*/
@XmlRootElement(name="contract")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContractBean {

	private Long id;
	private String nd_login;
	private String rateplan;
	private String rateplanCode;
	@XmlElement(name = "services")
	private ServiceList	serviceList;
	private AdresseBean adresse;
	
	public String getRateplanCode() {
		return rateplanCode;
	}
	
	public void setRateplanCode(String rateplanCode) {
		this.rateplanCode = rateplanCode;
	}
	public AdresseBean getAdresse() {
		return adresse;
	}
	public void setAdresse(AdresseBean adresse) {
		this.adresse = adresse;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNd_login() {
		return nd_login;
	}
	public void setNd_login(String nd_login) {
		this.nd_login = nd_login;
	}
	public String getRateplan() {
		return rateplan;
	}
	public void setRateplan(String rateplan) {
		this.rateplan = rateplan;
	}
	public ServiceList getServiceList() {
		return serviceList;
	}
	public void setServiceList(ServiceList serviceList) {
		this.serviceList = serviceList;
	}
	public ContractBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ContractBean(Long id, String nd_login, String rateplan ,String rateplanCode, ServiceList serviceList ,AdresseBean adresse) {
		super();
		this.id = id;
		this.nd_login = nd_login;
		this.rateplan = rateplan;
		this.serviceList = serviceList;
		this.rateplanCode = rateplanCode;
		this.adresse = adresse;
	}
		
		
		
	/**End feature/Recherche_Contrat_Client*/
}
