package ma.iam.wiam.neto.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ItemList implements Serializable {

	private static final long serialVersionUID = 1651205243706772184L;

	@XmlElement(name = "item")
	private List<ReferentialBean> items;

	public List<ReferentialBean> getItems() {
		if (items == null) {
			items = new ArrayList<ReferentialBean>();
		}
		return items;
	}

	public void setItems(List<ReferentialBean> items) {
		this.items = items;
	}

}
