package ma.iam.wiam.business;

import java.util.List;

import com.netonomy.blm.interfaces.contact.TitleIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;

public interface IdentityManagerBiz {
	
	List<TitleIF> getCodesPersonnes() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getEtatsCiviles() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getIdentifiantsResidentiels() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getIdentifiantsProf() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getIdentifiantsEntreprise() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getProfessions() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getGenders() throws FunctionnalException, TechnicalException;
}