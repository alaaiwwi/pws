package ma.iam.wiam.validators;

import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.neto.bean.CustomerBillingInfoBean;
import ma.iam.wiam.neto.bean.ServiceRulesRequestBean;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.util.StringUtil;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.OrganizationTypeEnum;
import ma.iam.wiam.ws.neto.model.Client;

import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.util.ObjectId;

@Component(value = "commonValidator")
public class CommonValidator {

	@Autowired
	private ClientManagerBiz clientManagerBiz;

	@Autowired
	private LevelFDao levelFDao;

	private void validateCategory(String category) throws FunctionnalException {
		if (!OrganizationTypeEnum.categoriesElligibleForModification
				.contains(category)) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.INVALID_TYPE,
					"Catgéorie : Seuls les clients des sous-catégories « Résidentiel, Professionnel et Entreprise » sont éligibles à la modification");
		}
	}

	// @Before("execution(* ma.iam.wiam.business.*ManagerBiz.*(..))")
	public void validateCategory(JoinPoint proceedingJoinPoint)
			throws FunctionnalException {

		Object[] args = proceedingJoinPoint.getArgs();
		if (args != null && args.length > 0) {
			String ncli = null;
			Object arg0 = args[0];
			if (arg0 != null) {
				if (arg0 instanceof CustomerBillingInfoBean) {
					ncli = ((CustomerBillingInfoBean) arg0).getNcli();
				} else if (arg0 instanceof ClientSearchCriteria) {
					ncli = ((ClientSearchCriteria) arg0).getNcli();
				} else if (arg0 instanceof ServiceRulesRequestBean) {
					ncli = ((ServiceRulesRequestBean) arg0).getNcli();
				} else if (arg0 instanceof String
						&& proceedingJoinPoint.getTarget() instanceof ServiceManagerBiz) {
					ncli = (String) arg0;
				}
			}
			if (!StringUtil.isNullOrEmpty(ncli)) {

				LevelF orgF = null;
				try {
					Client client = clientManagerBiz.getClient(ncli);
					orgF = levelFDao.get(ObjectId.newLongId(client.getId())
							.toString());
					// return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (orgF != null) {
					validateCategory(orgF.getType().getCategory().getCode());
				}
			}
		}

		try {
			// proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}
	}
}
