package ma.iam.wiam.neto.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "services")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceList {

	@XmlElement(name = "service")
	private List<ServiceBean> services;

	public ServiceList() {
		super();
	}

	public ServiceList(List<ServiceBean> services) {
		this.services = services;
	}

	public List<ServiceBean> getServices() {
		return services;
	}

	public void setServices(List<ServiceBean> service) {
		this.services = service;
	}

}
