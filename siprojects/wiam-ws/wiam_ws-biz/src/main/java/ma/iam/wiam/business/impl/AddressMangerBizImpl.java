package ma.iam.wiam.business.impl;

import java.util.Vector;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.util.CountryIF;
import com.netonomy.blm.interfaces.util.MailingAddressIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.util.LongId;

import ma.iam.wiam.adresse.AdresseMgr;
import ma.iam.wiam.business.AddressManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.customer.ContactWIAM;
import ma.iam.wiam.dao.CommuneDao;
import ma.iam.wiam.dao.QuartierDao;
import ma.iam.wiam.dao.VoieDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.AdresseBean;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.validators.OrganizationValidator;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.PCommune;
import ma.iam.wiam.ws.neto.model.PQuartier;
import ma.iam.wiam.ws.neto.model.PVoie;

/**
 * This service
 * 
 * @author y.eddaalous
 *
 */
@Service
public class AddressMangerBizImpl implements AddressManagerBiz {

	@Autowired
	private CommuneDao communeDao;

	@Autowired
	private QuartierDao quartierDao;

	@Autowired
	private VoieDao voieDao;

	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;

	@Autowired
	private ReferentialManagerBiz referentialManagerBiz;

	public void createAddress(final AdresseBean adresseBean,
			final MailingAddressIF mailingAddress,
			final String organizationTypeCategoryCode)
			throws TechnicalException, FunctionnalException {

		OrganizationValidator.validateAddress(adresseBean,
				organizationTypeCategoryCode);

		CountryIF[] countries = (CountryIF[]) referentialManagerBiz
				.getItemFromReferential(Constants.ALL_COUNTRIES);

		CountryIF countryIF = getCountryFromList(adresseBean.getPays()
				.getCode(), countries);

		if (countryIF == null) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
					"adresse: pays " + adresseBean.getPays().getCode()
							+ "  non trouve dans le referentiel");
		}

		mailingAddress.setCountry(ObjectRefMgr.getCountry(new LongId(new Long(
				adresseBean.getPays().getCode()))));

		PCommune pCommune = communeDao.getCommuneByCode(adresseBean
				.getCommune().getCode());

		if (pCommune == null) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
					"adresse: commune " + adresseBean.getCommune().getCode()
							+ "  non trouve dans le referentiel");
		}
		AdresseMgr.setCommune(mailingAddress, pCommune.getCcom(),
				pCommune.getLcom());

		AdresseMgr.setCodePostale(mailingAddress, adresseBean.getCodePostale());

		if (adresseBean.getQuartier().getCode().equals("Autre")) {
			AdresseMgr.setAutreQuartier(mailingAddress);
		} else {
			PQuartier pQuartier = quartierDao
					.getQuartierByCodeCommuneAndCodeQuartier(adresseBean
							.getCommune().getCode(), adresseBean.getQuartier()
							.getCode());
			if (pQuartier == null) {
				throw new FunctionnalException(
						ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
						"adresse: quartier "
								+ adresseBean.getQuartier().getCode()
								+ "  non trouve dans le referentiel pour la commune "
								+ adresseBean.getCommune().getCode());
			}
			AdresseMgr.setCodifiedQuartier(mailingAddress, pQuartier.getId()
					.getCquartier(), pQuartier.getLquartier());
		}
		if (StringUtils.isNotEmpty(adresseBean.getQuartierNonCodifie())) {
			AdresseMgr.setNonCodifiedQuartier(mailingAddress,
					adresseBean.getQuartierNonCodifie());
		}

		PVoie pVoie = voieDao.getVoie(adresseBean.getCommune().getCode(),
				adresseBean.getQuartier().getCode(), adresseBean.getVoie()
						.getCode());

		if (pVoie == null) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS,
					"adresse: voie "
							+ adresseBean.getVoie().getCode()
							+ "  non trouve dans le referentiel pour le quatier "
							+ adresseBean.getVoie().getCode());
		}
		AdresseMgr.setCodifiedVoie(mailingAddress, pVoie.getId().getCvoie(),
				pVoie.getLvoie());

		if (StringUtils.isNotEmpty(adresseBean.getVoieNonCodifie())) {
			AdresseMgr.setNonCodifiedVoie(mailingAddress,
					adresseBean.getVoieNonCodifie());
		}

		AdresseMgr.setVoieNumber(mailingAddress, adresseBean.getNumeroVoie());
	}

	@SuppressWarnings("rawtypes")
	public ParameterIF[] setAdditionalInfosForAddress(
			final AdresseBean adresseBean) throws FunctionnalException {

		OrganizationValidator.validateAdditionnalInfosFroAddress(adresseBean);

		Vector addressParameters = new Vector();

		ContactWIAM.setCommuneEtranger(addressParameters,
				adresseBean.getCommuneEtrangere());
		ContactWIAM.setBatiment(addressParameters, adresseBean.getBatiment());
		ContactWIAM.setEscalier(addressParameters, adresseBean.getEscalier());
		ContactWIAM.setEtage(addressParameters, adresseBean.getEtage());
		ContactWIAM.setPorte(addressParameters, adresseBean.getPorte());
		ContactWIAM.setTypeDistribution(addressParameters, adresseBean
				.getTypeDestribution().getCode());

		ContactWIAM
				.setBoitePostale(
						addressParameters,
						adresseBean.getBoitePostale() != null ? adresseBean
								.getBoitePostale() : "");

		if (StringUtils.isNotEmpty(adresseBean.getComplementAdresse())) {
			String addressComplement = adresseBean.getComplementAdresse();
			addressComplement = addressComplement.replaceAll("\r\n", " ");
			addressComplement = addressComplement.replaceAll("\r", " ");
			addressComplement = addressComplement.replaceAll("\n", " ");
			addressComplement.trim();
			ContactWIAM.setComplementAdresse(addressParameters,
					addressComplement);
		} else {
			ContactWIAM.setComplementAdresse(addressParameters,
					adresseBean.getComplementAdresse());
		}

		ParameterIF[] addressParameterIFs = WIAMHelper
				.vectorToParameter(addressParameters);
		return addressParameterIFs;
	}

	private CountryIF getCountryFromList(String code, CountryIF[] countries) {
		if (code != null && ArrayUtils.isNotEmpty(countries)) {
			for (CountryIF country : countries) {
				if (code.equals(country.getIdentifier().getString())) {
					return country;
				}
			}
		}

		return null;
	}
}
