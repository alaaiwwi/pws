package ma.iam.wiam.business;

import java.util.List;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.hbm.bean.ClientBean;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.ws.neto.model.Client;

public interface ClientManagerBiz {

	Boolean verifySolavabilteClientByNcli(ClientSearchCriteria criteria)
			throws FunctionnalException, TechnicalException;

	List<ClientBean> findClientByCriteria(ClientSearchCriteria criteria)
			throws FunctionnalException, TechnicalException;

	Client getClient(String ncli) throws TechnicalException,
			FunctionnalException;
	/**feature/Changement_Categorie_Client*/
	Boolean changeCategoryClient(String ncli,String codeSousCategorieDest)throws TechnicalException,
	FunctionnalException;
}
