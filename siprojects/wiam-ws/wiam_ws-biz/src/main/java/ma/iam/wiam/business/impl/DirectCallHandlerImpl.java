package ma.iam.wiam.business.impl;

import java.util.concurrent.TimeoutException;

import ma.iam.wiam.business.DirectCallHandler;
import ma.iam.wiam.util.WIAM_logMgr;
import ma.iam.wiam.util.directCall.ConnectionError;
import ma.iam.wiam.util.directCall.DirectCallContext;
import ma.iam.wiam.util.directCall.DirectCallManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class DirectCallHandlerImpl implements DirectCallHandler {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OrderManagerBizImpl.class);

	@Value("#{config['ma.iam.wiam.directCallReply.period']}")
	private long directCallReplyPeriod;
	@Value("#{config['ma.iam.wiam.service.timeout']}")
	private long timeout;

	public String handle(DirectCallContext ctx)
			throws TimeoutException {
		long startTime = System.currentTimeMillis();

		String directCallReply = DIRECTCALL_NO_REPLY;

		while (directCallReply.equalsIgnoreCase(DIRECTCALL_NO_REPLY)) {

			if (System.currentTimeMillis() - startTime >= timeout) {
				LOGGER.debug("directCallHandle Time out ");
				throw new TimeoutException(DIRECTCALL_NO_REPLY);
			}

			try {
				directCallReply = logicDirectCallHandle(ctx);
				LOGGER.debug("directCallReply = {} : attempts = {}",
						directCallReply, ctx.getAttempts());
			} catch (Throwable ee) {
				LOGGER.error("Throwable error : logic_directCallHandle : {}",
						ee);
			}

			// WAITING PERIOD
			if (directCallReply.equalsIgnoreCase(DIRECTCALL_NO_REPLY)) {
				try {
					LOGGER.debug("Waiting For directCallReply {} ms.",
							directCallReplyPeriod);
					Thread.sleep(directCallReplyPeriod);
				} catch (InterruptedException e) {
					LOGGER.warn("WARN CANNOT SLEEP {}", e);
				}
			}

		}

		return directCallReply;
	}

	/**
	 * Check for the direct call reply
	 * 
	 * @param ctx
	 * @return
	 * @throws Throwable
	 */
	private String logicDirectCallHandle(DirectCallContext ctx)
			throws Throwable {

		LOGGER.debug("DIRECT CALL handle : {}", ctx);

		try {
			if (ctx != null) {
				ctx = DirectCallManager.getInstance().handle(ctx);
			}
		} catch (ConnectionError err) {
			LOGGER.error("Err logic_directCallHandle : {}", err);
			ctx.setHandled();
		}

		// if ended, redir to funkstep specified in
		// the context and store the reply in httpRequest
		if (ctx.isDone()) {

			// the call has succeeded
			if (ctx.getStatus() == DirectCallContext.ENDED_SUCCESS) {
				if (ctx.getReply() != null)
					WIAM_logMgr.getNoneLogger().logInfo(
							"DirectCallContext.ENDED_SUCCESS : ctx.getReply() = "
									+ ctx.getReply(), null, "3");
				return DIRECTCALL_REPLY_OK;

				// the call has failed
			} else {
				LOGGER.error("DIRECTCALL_ERROR : {}" + ctx.getErr());
				LOGGER.error("DIRECTCALL_ATTEMPTS : {} ", ctx.getAttempts());
				LOGGER.error("MESSAGE = Incapable de se relier au service Direct Call.");
				return DIRECTCALL_REPLY_ERROR;
			}

		} else {
			// loggerInfo.warn("NO REPLY");
			return DIRECTCALL_NO_REPLY;
		}
	}

}
