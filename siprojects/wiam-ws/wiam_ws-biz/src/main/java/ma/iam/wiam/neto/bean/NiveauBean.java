package ma.iam.wiam.neto.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import ma.iam.wiam.ws.neto.enums.NiveauType;

@XmlAccessorType(XmlAccessType.FIELD)
public class NiveauBean {
	
	private String id;
	private ReferentialBean type;
	private NiveauType niveauType;
	private String dateDeRetraite;
	private String ccu;
	private ReferentialBean entiteGestionnaire;
	private ReferentialBean segment1;
	private String commentaire;
	private Boolean notificationSms;
	private Boolean notificaionMail;
	private ReferentialBean infoDeProspection;
	private String activite;
	private String porteFeuille;
	private String attacheCommercial;
	private String nombreDeSites;
	private String actionnariat;
	private Boolean prossesusDecisionnelCentralise;
	private String produit;
	private String effectif;
	private Double chiffreAffaire;
	private Double croissance;
	private Boolean multinationale;
	private Boolean secteurConcurrentiel;
	private Long acIcgc;
	private ContactLegalBean contactLegal;
	private ContactDeFacturationBean contactDeFacturation;
	private ContactTechniqueBean contactTechnique;
	private ContactCommercialBean contactCommercial;
	private NiveauxBean niveaux;
	
	public NiveauBean() {}
	
	public NiveauBean(String id) {
		this.id  = id;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ReferentialBean getType() {
		return type;
	}
	public void setType(ReferentialBean type) {
		this.type = type;
	}
	public NiveauType getNiveauType() {
		return niveauType;
	}
	public void setNiveauType(NiveauType niveauType) {
		this.niveauType = niveauType;
	}
	public String getDateDeRetraite() {
		return dateDeRetraite;
	}
	public void setDateDeRetraite(String dateDeRetraite) {
		this.dateDeRetraite = dateDeRetraite;
	}
	public String getCcu() {
		return ccu;
	}
	public void setCcu(String ccu) {
		this.ccu = ccu;
	}
	public ReferentialBean getEntiteGestionnaire() {
		return entiteGestionnaire;
	}
	public void setEntiteGestionnaire(ReferentialBean entiteGestionnaire) {
		this.entiteGestionnaire = entiteGestionnaire;
	}
	public ReferentialBean getSegment1() {
		return segment1;
	}
	public void setSegment1(ReferentialBean segment1) {
		this.segment1 = segment1;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public Boolean getNotificationSms() {
		return notificationSms;
	}
	public void setNotificationSms(Boolean notificationSms) {
		this.notificationSms = notificationSms;
	}
	public Boolean getNotificaionMail() {
		return notificaionMail;
	}
	public void setNotificaionMail(Boolean notificaionMail) {
		this.notificaionMail = notificaionMail;
	}
	public ReferentialBean getInfoDeProspection() {
		return infoDeProspection;
	}
	public void setInfoDeProspection(ReferentialBean infoDeProspection) {
		this.infoDeProspection = infoDeProspection;
	}
	public String getActivite() {
		return activite;
	}
	public void setActivite(String activite) {
		this.activite = activite;
	}
	public String getPorteFeuille() {
		return porteFeuille;
	}
	public void setPorteFeuille(String porteFeuille) {
		this.porteFeuille = porteFeuille;
	}
	public String getAttacheCommercial() {
		return attacheCommercial;
	}
	public void setAttacheCommercial(String attacheCommercial) {
		this.attacheCommercial = attacheCommercial;
	}
	public String getNombreDeSites() {
		return nombreDeSites;
	}
	public void setNombreDeSites(String nombreDeSites) {
		this.nombreDeSites = nombreDeSites;
	}
	public String getActionnariat() {
		return actionnariat;
	}
	public void setActionnariat(String actionnariat) {
		this.actionnariat = actionnariat;
	}
	public Boolean getProssesusDecisionnelCentralise() {
		return prossesusDecisionnelCentralise;
	}
	public void setProssesusDecisionnelCentralise(Boolean prossesusDecisionnelCentralise) {
		this.prossesusDecisionnelCentralise = prossesusDecisionnelCentralise;
	}
	public String getProduit() {
		return produit;
	}
	public void setProduit(String produit) {
		this.produit = produit;
	}
	public String getEffectif() {
		return effectif;
	}
	public void setEffectif(String effectif) {
		this.effectif = effectif;
	}
	public Double getChiffreAffaire() {
		return chiffreAffaire;
	}
	public void setChiffreAffaire(Double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}
	public Double getCroissance() {
		return croissance;
	}
	public void setCroissance(Double croissance) {
		this.croissance = croissance;
	}
	public Boolean getMultinationale() {
		return multinationale;
	}
	public void setMultinationale(Boolean multinationale) {
		this.multinationale = multinationale;
	}
	public Boolean getSecteurConcurrentiel() {
		return secteurConcurrentiel;
	}
	public void setSecteurConcurrentiel(Boolean secteurConcurrentiel) {
		this.secteurConcurrentiel = secteurConcurrentiel;
	}
	public Long getAcIcgc() {
		return acIcgc;
	}
	public void setAcIcgc(Long acIcgc) {
		this.acIcgc = acIcgc;
	}
	public ContactLegalBean getContactLegal() {
		return contactLegal;
	}
	public void setContactLegal(ContactLegalBean contactLegal) {
		this.contactLegal = contactLegal;
	}
	public ContactDeFacturationBean getContactDeFacturation() {
		return contactDeFacturation;
	}
	public void setContactDeFacturation(ContactDeFacturationBean contacteDeFacturation) {
		this.contactDeFacturation = contacteDeFacturation;
	}
	public ContactTechniqueBean getContactTechnique() {
		return contactTechnique;
	}
	public void setContactTechnique(ContactTechniqueBean contactTechnique) {
		this.contactTechnique = contactTechnique;
	}
	public ContactCommercialBean getContactCommercial() {
		return contactCommercial;
	}
	public void setContactCommercial(ContactCommercialBean contactCommercial) {
		this.contactCommercial = contactCommercial;
	}
	public NiveauxBean getNiveaux() {
		return niveaux;
	}
	public void setNiveaux(NiveauxBean niveaux) {
		this.niveaux = niveaux;
	}
}