package ma.iam.wiam.business.impl;

import ma.iam.wiam.business.ByPassManagerBiz;
import ma.iam.wiam.business.constants.ProjectProperties;
import ma.iam.wiam.dao.ByPassDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component(value = "byPassManagerBiz")
public class ByPassManagerBizImpl implements ByPassManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory.getLogger(ByPassManagerBizImpl.class);

	@Autowired
	private ByPassDao byPassDao;

	@Transactional(readOnly = true, timeout = ProjectProperties.TIMEOUT)
	public String bdModeByPasse() {
		LOG.info("acces to fct");
		return (byPassDao.modeByPassBscsNeto()) ? "1" : "-1";
	}

	@Transactional(timeout = ProjectProperties.TIMEOUT)
	public String sessionModeByPasse() {
		LOG.info("acces to fct");
		return (byPassDao.sessionByPass()) ? "1" : "-1";
	}

	@Transactional(readOnly = true, timeout = ProjectProperties.TIMEOUT)
	public String modeByPasse() {
		LOG.info("acces to fct");
		return (byPassDao.modeByPassBscsNeto() && byPassDao.sessionByPass()) ? "1" : "-1";
	}
}
