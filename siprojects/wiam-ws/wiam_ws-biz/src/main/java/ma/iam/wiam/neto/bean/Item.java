//package ma.iam.wiam.neto.bean;
//
//import java.io.Serializable;
//
//public class Item implements Serializable {
//
//	private static final long serialVersionUID = 4034520978714172844L;
//
//	private String code;
//	private String libelle;
//
//	public Item() {
//		super();
//	}
//
//	public Item(String code, String libelle) {
//		this.code = code;
//		this.libelle = libelle;
//	}
//
//	public String getCode() {
//		return code;
//	}
//
//	public void setCode(String code) {
//		this.code = code;
//	}
//
//	public String getLibelle() {
//		return libelle;
//	}
//
//	public void setLibelle(String libelle) {
//		this.libelle = libelle;
//	}
//
//	@Override
//	public String toString() {
//		return "Item [code=" + code + ", libelle=" + libelle + "]";
//	}
//
//}
