package ma.iam.wiam.neto.bean;

public abstract class ContactBean {
	
	protected ReferentialBean codePersonne;
	protected String prenom;
	protected String nom;
	protected String telephoneMobile;
	protected String telephoneFix;
	protected String email;
	protected String fax;
	protected String raisonSocial;
	
	public ReferentialBean getCodePersonne() {
		return codePersonne;
	}
	public void setCodePersonne(ReferentialBean codePersonne) {
		this.codePersonne = codePersonne;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getTelephoneMobile() {
		return telephoneMobile;
	}
	public void setTelephoneMobile(String telephoneMobile) {
		this.telephoneMobile = telephoneMobile;
	}
	public String getTelephoneFix() {
		return telephoneFix;
	}
	public void setTelephoneFix(String telephoneFix) {
		this.telephoneFix = telephoneFix;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getRaisonSocial() {
		return raisonSocial;
	}
	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}	
}