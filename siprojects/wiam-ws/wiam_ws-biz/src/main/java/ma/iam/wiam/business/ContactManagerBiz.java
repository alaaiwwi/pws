package ma.iam.wiam.business;

import com.netonomy.blm.interfaces.contact.ContactIF;
import com.netonomy.blm.interfaces.organization.LevelIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.ParameterIF;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContactBean;
import ma.iam.wiam.neto.bean.ContactDeFacturationBean;
import ma.iam.wiam.neto.bean.MethodeDePaiementBean;

public interface ContactManagerBiz {

	void processContact(final ActionMgrIF actionMgr, final LevelIF levelIF,
			final ContactBean contactBean,
			final String organizationTypeCategoryCode)
			throws TechnicalException, FunctionnalException;

	public ParameterIF[] setAdditionnalInfosForBillingContact(
			final ContactDeFacturationBean contacteDeFacturationBean,
			final String organizationTypeCategoryCode)
			throws FunctionnalException;

	public ParameterIF[] setPaymentMethodInfosForBillingContact(
			final MethodeDePaiementBean methodeDePaiementBean)
			throws FunctionnalException;

	public void updateContact(final ContactBean contactBean,
			final String organizationTypeCategoryCode, ContactIF contactIF)
			throws FunctionnalException;
}
