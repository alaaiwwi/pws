package ma.iam.wiam.vo;

public class CommuneBean {
	private String codeCommune;
	private String labelCommune;
	private String codePostal;
	public String getCodeCommune() {
		return codeCommune;
	}
	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}
	public String getLabelCommune() {
		return labelCommune;
	}
	public void setLabelCommune(String labelCommune) {
		this.labelCommune = labelCommune;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}



}
