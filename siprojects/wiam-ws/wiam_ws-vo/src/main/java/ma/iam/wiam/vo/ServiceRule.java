package ma.iam.wiam.vo;

public class ServiceRule {

	/**
	 * L'ID du service Cible
	 */
	private String serviceID1;

	/**
	 * Le message de la règle
	 */
	private String messageRule;

	/**
	 * Le service a tester avec le service cible
	 */
	private String serviceID2;

	/**
	 * Le nom du servcie cible
	 */
	private String serviceName1;

	/**
	 * Le nom du service a tester avec le service cible
	 */
	private String serviceName2;

	/**
	 * Le code du servcie cible
	 */
	private String serviceCode1;

	/**
	 * Le code du service a tester avec le service cible
	 */
	private String serviceCode2;

	/**
	 * La description de la règle
	 */
	private String ruleDescription;

	/**
	 * Constructeur par defaut
	 */
	public ServiceRule() {
		super();
	}

	/**
	 * @return the serviceID1
	 */
	public String getServiceID1() {
		return serviceID1;
	}

	/**
	 * @param serviceID1
	 *            the serviceID1 to set
	 */
	public void setServiceID1(String serviceID1) {
		this.serviceID1 = serviceID1;
	}

	/**
	 * @return the messageRule
	 */
	public String getMessageRule() {
		return messageRule;
	}

	/**
	 * @param messageRule
	 *            the messageRule to set
	 */
	public void setMessageRule(String messageRule) {
		this.messageRule = messageRule;
	}

	/**
	 * @return the serviceID2
	 */
	public String getServiceID2() {
		return serviceID2;
	}

	/**
	 * @param serviceID2
	 *            the serviceID2 to set
	 */
	public void setServiceID2(String serviceID2) {
		this.serviceID2 = serviceID2;
	}

	/**
	 * @return the serviceName1
	 */
	public String getServiceName1() {
		return serviceName1;
	}

	/**
	 * @param serviceName1
	 *            the serviceName1 to set
	 */
	public void setServiceName1(String serviceName1) {
		this.serviceName1 = serviceName1;
	}

	/**
	 * @return the serviceName2
	 */
	public String getServiceName2() {
		return serviceName2;
	}

	/**
	 * @param serviceName2
	 *            the serviceName2 to set
	 */
	public void setServiceName2(String serviceName2) {
		this.serviceName2 = serviceName2;
	}

	/**
	 * @return the serviceCode1
	 */
	public String getServiceCode1() {
		return serviceCode1;
	}

	/**
	 * @param serviceCode1
	 *            the serviceCode1 to set
	 */
	public void setServiceCode1(String serviceCode1) {
		this.serviceCode1 = serviceCode1;
	}

	/**
	 * @return the serviceCode2
	 */
	public String getServiceCode2() {
		return serviceCode2;
	}

	/**
	 * @param serviceCode2
	 *            the serviceCode2 to set
	 */
	public void setServiceCode2(String serviceCode2) {
		this.serviceCode2 = serviceCode2;
	}

	/**
	 * @return the ruleDescription
	 */
	public String getRuleDescription() {
		return ruleDescription;
	}

	/**
	 * @param ruleDescription
	 *            the ruleDescription to set
	 */
	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

}
