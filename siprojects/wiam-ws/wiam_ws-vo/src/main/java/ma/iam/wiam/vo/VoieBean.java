package ma.iam.wiam.vo;

public class VoieBean {

	// fields
	private java.lang.String codeCommune;
	private java.lang.String codeQuartier;
	private java.lang.String codeVoie;
	private java.lang.String labelVoie;

	public java.lang.String getCodeCommune() {
		return codeCommune;
	}

	public void setCodeCommune(java.lang.String codeCommune) {
		this.codeCommune = codeCommune;
	}

	public java.lang.String getCodeQuartier() {
		return codeQuartier;
	}

	public void setCodeQuartier(java.lang.String codeQuartier) {
		this.codeQuartier = codeQuartier;
	}

	public java.lang.String getCodeVoie() {
		return codeVoie;
	}

	public void setCodeVoie(java.lang.String codeVoie) {
		this.codeVoie = codeVoie;
	}

	public java.lang.String getLabelVoie() {
		return labelVoie;
	}

	public void setLabelVoie(java.lang.String labelVoie) {
		this.labelVoie = labelVoie;
	}

}
