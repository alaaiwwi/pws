package ma.iam.wiam.vo;

public class QuartierBean {

	private java.lang.String labelQuartier;
	private java.lang.String fCodifVoie;
	private java.lang.String codeCommune;
	private java.lang.String codeQuartier;

	public java.lang.String getLabelQuartier() {
		return labelQuartier;
	}

	public void setLabelQuartier(java.lang.String labelQuartier) {
		this.labelQuartier = labelQuartier;
	}

	public java.lang.String getfCodifVoie() {
		return fCodifVoie;
	}

	public void setfCodifVoie(java.lang.String fCodifVoie) {
		this.fCodifVoie = fCodifVoie;
	}

	public java.lang.String getCodeCommune() {
		return codeCommune;
	}

	public void setCodeCommune(java.lang.String codeCommune) {
		this.codeCommune = codeCommune;
	}

	public java.lang.String getCodeQuartier() {
		return codeQuartier;
	}

	public void setCodeQuartier(java.lang.String codeQuartier) {
		this.codeQuartier = codeQuartier;
	}

}
