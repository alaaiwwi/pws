package ma.iam.wiam.params;

public class AgenceBancaireParams {

	private String codeBanque;

	public String getCodeBanque() {
		return codeBanque;
	}

	public void setCodeBanque(String codeBanque) {
		this.codeBanque = codeBanque;
	}
}
