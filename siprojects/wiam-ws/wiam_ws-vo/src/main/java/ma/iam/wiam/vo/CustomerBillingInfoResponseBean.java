package ma.iam.wiam.vo;

public class CustomerBillingInfoResponseBean extends AbstractCustomerBillingInfoBean{

	private String categorieSs;

	public String getCategorieSs() {
		return categorieSs;
	}

	public void setCategorieSs(String categorieSs) {
		this.categorieSs = categorieSs;
	}
}
