package ma.iam.wiam.vo;

public class ClientSearchBean {

	private String numClient;
	private String clientName;
	private String sousCategorie;
	private String adresse;
	private String idClient;
		
	
	public String getIdClient() {
		return idClient;
	}
	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	public String getNumClient() {
		return numClient;
	}
	public void setNumClient(String numClient) {
		this.numClient = numClient;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getSousCategorie() {
		return sousCategorie;
	}
	public void setSousCategorie(String sousCategorie) {
		this.sousCategorie = sousCategorie;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
}
