package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;

public class ServiceMapper {

//	public static List<ma.iam.wiam.vo.ServiceBean> mapBusinessBeanToVO(
//			List<ServiceBean> services) {
//		List<ma.iam.wiam.vo.ServiceBean> list = null;
//		if (!CollectionUtils.isEmpty(services)) {
//			list = new ArrayList<ma.iam.wiam.vo.ServiceBean>();
//			for (ma.iam.wiam.neto.bean.ServiceBean service : services) {
//				list.add(mapBusinessBean2VO(service));
//			}
//		}
//
//		return list;
//	}
//
//	private static ma.iam.wiam.vo.ServiceBean mapBusinessBean2VO(
//			ServiceBean service) {
//		ma.iam.wiam.vo.ServiceBean result = new ma.iam.wiam.vo.ServiceBean();
//		BeanUtils.copyProperties(service, result, "parameters");
//
//		List<ParameterDescriptor> businessParameters = service.getParameters();
//		result.setParameters(ServiceMapper
//				.mapParameterBusinessBeanToVO(businessParameters));
//		return result;
//	}
//
//	public static ArrayList<ma.iam.wiam.vo.ParameterDescriptor> mapParameterBusinessBeanToVO(
//			List<ParameterDescriptor> businessParameters) {
//		ArrayList<ma.iam.wiam.vo.ParameterDescriptor> parameters = new ArrayList<ma.iam.wiam.vo.ParameterDescriptor>();
//		if (!CollectionUtils.isEmpty(businessParameters)) {
//			for (ParameterDescriptor parameterDescriptor : businessParameters) {
//				ma.iam.wiam.vo.ParameterDescriptor parameterDescriptorVO = new ma.iam.wiam.vo.ParameterDescriptor();
//				BeanUtils.copyProperties(parameterDescriptor,
//						parameterDescriptorVO, "items");
//				if (!CollectionUtils.isEmpty(parameterDescriptor
//						.getPossibleValues())) {
//					mapItemsBusinessBeanToVO(
//							parameterDescriptor.getPossibleValues(),
//							parameterDescriptorVO);
//
//				}
//				parameters.add(parameterDescriptorVO);
//			}
//		}
//		return parameters;
//	}
//
//	private static void mapItemsBusinessBeanToVO(
//			List<ReferentialBean> possibleValues,
//			ma.iam.wiam.vo.ParameterDescriptor parameterDescriptorVO) {
//		parameterDescriptorVO.setItems(new ItemList());
//		for (ReferentialBean referentialBean : possibleValues) {
//
//			parameterDescriptorVO
//					.getItems()
//					.getItems()
//					.add(new Item(referentialBean.getCode(), referentialBean
//							.getLibelle()));
//		}
//	}

	public static List<ma.iam.wiam.vo.ServiceRule> mapServiceRuleBusinessBeanToVO(
			List<ma.iam.wiam.services.regle.bean.ServiceRule> serviceRules) {
		List<ma.iam.wiam.vo.ServiceRule> result = new ArrayList<ma.iam.wiam.vo.ServiceRule>();
		if (!CollectionUtils.isEmpty(serviceRules)) {
			for (ma.iam.wiam.services.regle.bean.ServiceRule service : serviceRules) {
				ma.iam.wiam.vo.ServiceRule serviceRuleVO = new ma.iam.wiam.vo.ServiceRule();
				BeanUtils.copyProperties(service, serviceRuleVO);
				result.add(serviceRuleVO);
			}
		}

		return result;
	}

	// public static List<ServiceParamsResponseBean>
	// mapServiceParamsBusinessBeanToVO(
	// List<ServiceBean> serviceParams) {
	// List<ServiceParamsResponseBean> result = new
	// ArrayList<ServiceParamsResponseBean>();
	// if (!CollectionUtils.isEmpty(serviceParams)) {
	// for (ServiceBean serviceResponseBean : serviceParams) {
	// ServiceParamsResponseBean serviceParamsResponseBean = new
	// ServiceParamsResponseBean();
	// serviceParamsResponseBean.setServiceId(serviceResponseBean
	// .getServiceId().getCode());
	// serviceParamsResponseBean.setParameters(ServiceMapper
	// .mapParameterBusinessBeanToVO(serviceResponseBean
	// .getParameters()));
	// result.add(serviceParamsResponseBean);
	// }
	// }
	//
	// return result;
	// }
}
