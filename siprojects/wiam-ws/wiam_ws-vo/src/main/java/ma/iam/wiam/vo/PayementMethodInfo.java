package ma.iam.wiam.vo;

public class PayementMethodInfo {

	private Integer paymentMethodCode;
	private String bankCode;
	private String bankAgencyCode;
	private String accountNumber;
	private String rib;
	private String accountHolder;
	private Integer autorizationNumber;

	public Integer getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(Integer paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankAgencyCode() {
		return bankAgencyCode;
	}

	public void setBankAgencyCode(String bankAgencyCode) {
		this.bankAgencyCode = bankAgencyCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public Integer getAutorizationNumber() {
		return autorizationNumber;
	}

	public void setAutorizationNumber(Integer autorizationNumber) {
		this.autorizationNumber = autorizationNumber;
	}

	@Override
	public String toString() {
		return "PayementMethodInfoBean [paymentMethodCode=" + paymentMethodCode
				+ ", bankCode=" + bankCode + ", bankAgencyCode="
				+ bankAgencyCode + ", accountNumber=" + accountNumber
				+ ", rib=" + rib + ", accountHolder=" + accountHolder
				+ ", autorizationNumber=" + autorizationNumber + "]";
	}

}
