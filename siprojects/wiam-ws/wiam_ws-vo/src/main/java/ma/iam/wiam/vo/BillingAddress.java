package ma.iam.wiam.vo;

public class BillingAddress {

	private String pays;
	private String communeEtranger;
	private String commune;
	private String codePostal;
	private String quartier;
	private String quartierNonCodifiee;
	private String voie;
	private String numVoie;
	private String voieNonCodifie;
	private String batiment;
	private String escalier;
	private String etage;
	private String porte;
	private String typeDestribution;
	private String boitePostale;
	private String complement;

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getCommuneEtranger() {
		return communeEtranger;
	}

	public void setCommuneEtranger(String communeEtranger) {
		this.communeEtranger = communeEtranger;
	}

	public String getCommune() {
		return commune;
	}

	public void setCommune(String commune) {
		this.commune = commune;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getQuartier() {
		return quartier;
	}

	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}

	public String getQuartierNonCodifiee() {
		return quartierNonCodifiee;
	}

	public void setQuartierNonCodifiee(String quartierNonCodifiee) {
		this.quartierNonCodifiee = quartierNonCodifiee;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public String getNumVoie() {
		return numVoie;
	}

	public void setNumVoie(String numVoie) {
		this.numVoie = numVoie;
	}

	public String getVoieNonCodifie() {
		return voieNonCodifie;
	}

	public void setVoieNonCodifie(String voieNonCodifie) {
		this.voieNonCodifie = voieNonCodifie;
	}

	public String getBatiment() {
		return batiment;
	}

	public void setBatiment(String batiment) {
		this.batiment = batiment;
	}

	public String getEscalier() {
		return escalier;
	}

	public void setEscalier(String escalier) {
		this.escalier = escalier;
	}

	public String getEtage() {
		return etage;
	}

	public void setEtage(String etage) {
		this.etage = etage;
	}

	public String getPorte() {
		return porte;
	}

	public void setPorte(String porte) {
		this.porte = porte;
	}

	public String getTypeDestribution() {
		return typeDestribution;
	}

	public void setTypeDestribution(String typeDestribution) {
		this.typeDestribution = typeDestribution;
	}

	public String getBoitePostale() {
		return boitePostale;
	}

	public void setBoitePostale(String boitePostale) {
		this.boitePostale = boitePostale;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	@Override
	public String toString() {
		return "BilligAddress [pays=" + pays + ", communeEtranger="
				+ communeEtranger + ", commune=" + commune + ", codePostal="
				+ codePostal + ", quartier=" + quartier
				+ ", quartierNonCodifiee=" + quartierNonCodifiee + ", voie="
				+ voie + ", numVoie=" + numVoie + ", voieNonCodifie="
				+ voieNonCodifie + ", batiment=" + batiment + ", escalier="
				+ escalier + ", etage=" + etage + ", porte=" + porte
				+ ", typeDestribution=" + typeDestribution + ", boitePostale="
				+ boitePostale + ", complement=" + complement + "]";
	}

}
