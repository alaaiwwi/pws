//package ma.iam.wiam.vo;
//
//import java.util.List;
//
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlRootElement;
//
//@XmlRootElement(name="service")
//@XmlAccessorType(XmlAccessType.FIELD)
//public class ServiceBean {
//
//	private String serviceId;
//	private String serviceCode;
//	private String serviceName;
//	private Double accessFee;
//	private Double subscriptionFee;
//	private List<ParameterDescriptor> parameters;
//
//	public String getServiceId() {
//		return serviceId;
//	}
//
//	public void setServiceId(String serviceId) {
//		this.serviceId = serviceId;
//	}
//	public String getServiceCode() {
//		return serviceCode;
//	}
//
//	public void setServiceCode(String serviceCode) {
//		this.serviceCode = serviceCode;
//	}
//
//	public String getServiceName() {
//		return serviceName;
//	}
//
//	public void setServiceName(String serviceName) {
//		this.serviceName = serviceName;
//	}
//
//	public Double getAccessFee() {
//		return accessFee;
//	}
//
//	public void setAccessFee(Double accessFee) {
//		this.accessFee = accessFee;
//	}
//
//	public Double getSubscriptionFee() {
//		return subscriptionFee;
//	}
//
//	public void setSubscriptionFee(Double subscriptionFee) {
//		this.subscriptionFee = subscriptionFee;
//	}
//
//	public List<ParameterDescriptor> getParameters() {
//		return parameters;
//	}
//
//	public void setParameters(List<ParameterDescriptor> parameters) {
//		this.parameters = parameters;
//	}
//
//}
