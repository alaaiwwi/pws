package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

public abstract class GenericMapper<BM, BB> {

	public final List<BB> mapListBusinesModelsToListBusinesBeans(List<BM> businesModels) {
		List<BB> businesBeans = new ArrayList<BB>();

		if (CollectionUtils.isNotEmpty(businesModels)) {
			for (BM businesModel : businesModels) {
				businesBeans.add(mapBusinesModelToBusinesBean(businesModel));
			}
		}

		return businesBeans;
	}
	
	public abstract BB mapBusinesModelToBusinesBean(BM businesModel);
	
	public abstract BM mapBusinesBeanToBusinesModel(BB businesModel);
}
