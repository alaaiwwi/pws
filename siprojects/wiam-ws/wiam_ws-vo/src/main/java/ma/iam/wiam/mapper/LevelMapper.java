package ma.iam.wiam.mapper;

import ma.iam.wiam.neto.bean.ReferentialBean;

public class LevelMapper extends GenericMapper<Object[], ReferentialBean>{
	
	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(Object[] level) {
		ReferentialBean referentialBean = null;

		if (level == null)
			return referentialBean;

		referentialBean = new ReferentialBean();
		referentialBean.setCode((String) level[0]);
		referentialBean.setLibelle((String) level[1]);

		return referentialBean;
	}

	@Override
	public Object[] mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}
}
