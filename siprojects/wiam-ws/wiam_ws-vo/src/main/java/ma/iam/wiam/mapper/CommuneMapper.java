package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.vo.CommuneBean;
import ma.iam.wiam.ws.neto.model.PCommune;

public class CommuneMapper {

	public final static CommuneBean mapPersist2Vo(PCommune persit) {
		CommuneBean bean = null;
		if (persit != null) {
			bean = new CommuneBean();
			bean.setCodeCommune(persit.getCcom());
			bean.setCodePostal(persit.getCpostal());
			bean.setLabelCommune(persit.getLcom());
		}
		return bean;
	}

	public final static List<CommuneBean> mapPersit2VoList(List<PCommune> list) {
		List<CommuneBean> listVos = new ArrayList<CommuneBean>();
		if (CollectionUtils.isNotEmpty(list)) {
			for (PCommune persit : list) {
				listVos.add(mapPersist2Vo(persit));
			}
		}

		return listVos;
	}

}
