package ma.iam.wiam.vo;

public class BillingInfoDiverse {

	private Boolean grouperFacture;
	private Boolean exempted;
	private String billingSupportCode;
	private String organisationCode;
	private String languageCode;
	private String billingCycleCode;
	private String paymentTermCode;
	private String confidentialCode;

	public Boolean getGrouperFacture() {
		return grouperFacture;
	}

	public void setGrouperFacture(Boolean grouperFacture) {
		this.grouperFacture = grouperFacture;
	}

	public Boolean getExempted() {
		return exempted;
	}

	public void setExempted(Boolean exempted) {
		this.exempted = exempted;
	}

	public String getBillingSupportCode() {
		return billingSupportCode;
	}

	public void setBillingSupportCode(String billingSupportCode) {
		this.billingSupportCode = billingSupportCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getBillingCycleCode() {
		return billingCycleCode;
	}

	public void setBillingCycleCode(String billingCycleCode) {
		this.billingCycleCode = billingCycleCode;
	}

	public String getPaymentTermCode() {
		return paymentTermCode;
	}

	public void setPaymentTermCode(String paymentTermCode) {
		this.paymentTermCode = paymentTermCode;
	}

	public String getConfidentialCode() {
		return confidentialCode;
	}

	public void setConfidentialCode(String confidentialCode) {
		this.confidentialCode = confidentialCode;
	}

	public String getOrganisationCode() {
		return organisationCode;
	}

	public void setOrganisationCode(String organisationCode) {
		this.organisationCode = organisationCode;
	}

}
