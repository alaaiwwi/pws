package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.hbm.bean.ClientBean;
import ma.iam.wiam.vo.ClientSearchBean;
import ma.iam.wiam.ws.neto.model.Client;

public class ClientMapper {


	public static final ClientSearchBean persistBean2Bean(ClientBean client) {
		ClientSearchBean bean = null;

		if (client != null) {
			bean = new ClientSearchBean();
			bean.setAdresse(client.getAdresse());
			bean.setSousCategorie(client.getOrgTypeName());
			bean.setNumClient(client.getNumClient());
			bean.setClientName(client.getNomClient());
			bean.setIdClient("L"+String.valueOf(client.getId()).length()+":"+client.getId());

		}
		return bean;
	}

	public static final ClientSearchBean persist2Bean(Client client) {
		ClientSearchBean bean = null;

		if (client != null) {
			bean = new ClientSearchBean();
			bean.setAdresse(client.getAdresse());
			bean.setSousCategorie(client.getOrgTypeName());
			bean.setNumClient(client.getNumClient());
			bean.setClientName(client.getNomClient());
			bean.setIdClient("L"+String.valueOf(client.getId()).length()+":"+client.getId());

		}
		return bean;
	}

	public static final List<ClientSearchBean> mapPersist2BeanCollection(List<Client> listClient) {
		List<ClientSearchBean> list = null;
		if (CollectionUtils.isNotEmpty(listClient)) {
			list = new ArrayList<ClientSearchBean>();
			for (Client client : listClient) {
				list.add(persist2Bean(client));
			}
		}

		return list;
	}

	public static final List<ClientSearchBean> mapPersistBean2BeanCollection(List<ClientBean> listClient) {
		List<ClientSearchBean> list = null;
		if (CollectionUtils.isNotEmpty(listClient)) {
			list = new ArrayList<ClientSearchBean>();
			for (ClientBean client : listClient) {
				list.add(persistBean2Bean(client));
			}
		}

		return list;
	}
}
