package ma.iam.wiam.mapper;

import com.netonomy.blm.interfaces.contact.TitleIF;

import ma.iam.wiam.neto.bean.ReferentialBean;

public class TiltesMapper extends GenericMapper<TitleIF, ReferentialBean> {

	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(TitleIF persist) {
		ReferentialBean bean = null;
		if (persist != null) {
			bean = new ReferentialBean();
			bean.setCode(String.valueOf(persist.getIdentifier()));
			bean.setLibelle(persist.getName());
		}
		return bean;
	}

	@Override
	public TitleIF mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}

}
