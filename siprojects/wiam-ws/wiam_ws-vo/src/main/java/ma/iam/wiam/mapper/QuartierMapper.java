package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.param.criteria.QuartierCriteria;
import ma.iam.wiam.params.QuartierParams;
import ma.iam.wiam.vo.QuartierBean;
import ma.iam.wiam.ws.neto.model.PQuartier;

public class QuartierMapper extends GenericMapper<PQuartier, ReferentialBean> {

	public static final QuartierCriteria mapParams2Criteria(QuartierParams param) {
		QuartierCriteria criteria = null;
		if (param != null) {
			criteria = new QuartierCriteria();
			criteria.setCodeCommune(param.getCodeCommune());
		}
		return criteria;
	}

	public static final QuartierBean mapPersist2Vo(PQuartier persist) {
		QuartierBean bean = null;
		if (persist != null) {
			bean = new QuartierBean();

			bean.setLabelQuartier(persist.getLquartier());
			bean.setCodeCommune(persist.getId().getCcom());
			bean.setCodeQuartier(persist.getId().getCquartier());
			bean.setfCodifVoie(persist.getFCodifVoie());
		}
		return bean;
	}

	public static final List<QuartierBean> mapPersist2VoList(List<PQuartier> list) {
		List<QuartierBean> listVos = new ArrayList<QuartierBean>();

		if (CollectionUtils.isNotEmpty(list)) {
			for (PQuartier persit : list) {
				listVos.add(mapPersist2Vo(persit));
			}
		}

		return listVos;
	}

	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(PQuartier persist) {
		ReferentialBean bean = null;
		if (persist != null) {
			bean = new ReferentialBean();
			bean.setCode(persist.getId().getCquartier());
			bean.setLibelle(persist.getLquartier());

		}
		return bean;
	}

	@Override
	public PQuartier mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}
}
