package ma.iam.wiam.vo;

public abstract class AbstractCustomerBillingInfoBean {

	private String ncli;
	private BillingInfoDiverse billingInfoDiverse;
	private PayementMethodInfo payementMethodInfo;
	private BillingContact billingContact;
	private BillingAddress billingAddress;

	public String getNcli() {
		return ncli;
	}

	public void setNcli(String ncli) {
		this.ncli = ncli;
	}

	public BillingInfoDiverse getBillingInfoDiverse() {
		return billingInfoDiverse;
	}

	public void setBillingInfoDiverse(BillingInfoDiverse billingInfoDiverseBean) {
		this.billingInfoDiverse = billingInfoDiverseBean;
	}

	public PayementMethodInfo getPayementMethodInfo() {
		return payementMethodInfo;
	}

	public void setPayementMethodInfo(PayementMethodInfo payementMethodInfoBean) {
		this.payementMethodInfo = payementMethodInfoBean;
	}

	public BillingContact getBillingContact() {
		return billingContact;
	}

	public void setBillingContact(BillingContact billingContact) {
		this.billingContact = billingContact;
	}

	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	@Override
	public String toString() {
		return "AbstractCustomerBillingInfoBean [ncli=" + ncli
				+ ", billingInfoDiverse=" + billingInfoDiverse
				+ ", payementMethodInfo=" + payementMethodInfo
				+ ", billingContact=" + billingContact + ", billingAddress="
				+ billingAddress + "]";
	}

}
