package ma.iam.wiam.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.vo.CustomerBillingInfoResponseBean;
import ma.iam.wiam.vo.UpdateCustomerBillingInfoRequestBean;

@WebService(serviceName = "CustomerBillingAccountService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface CustomerBillingAccountService extends BasicService{

	@WebMethod(operationName = "getBillingInfoByClientNum")
	@WebResult(name = "result")
	public List<CustomerBillingInfoResponseBean> getBillingInfoByClientNum(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String param)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "modifyBillingAccountInfo")
	@WebResult(name = "result")
	public String modifyBillingAccountInfo(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") UpdateCustomerBillingInfoRequestBean customerBillingInfoBean)
			throws FunctionnalException, TechnicalException;

}
