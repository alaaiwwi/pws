package ma.iam.wiam.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.neto.bean.RequestBean;
import ma.iam.wiam.neto.bean.TicketObject;

@WebService(serviceName = "CustomerService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface CustomerService extends BasicService {

	@WebMethod(operationName = "creationClientGrandPublic")
	@WebResult(name = "resultat")
	public String createGPCustomer(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") NiveauBean organisationBean,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "creationClientProfessionnel")
	@WebResult(name = "resultat")
	public String createProfessionnelCustomer(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") NiveauBean organisationBean,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "creationClientEntreprise")
	@WebResult(name = "resultat")
	public String createEntrepriseCustomer(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") NiveauBean organisationBean,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "creationNiveau")
	@WebResult(name = "resultat")
	public String createLevel(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") NiveauBean organisationBean,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "attacherNiveauCcu")
	@WebResult(name = "client")
	public NiveauBean linkLevelWithCcu(
			@WebParam(name = "request") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") RequestBean requestBean,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject)
			throws FunctionnalException, TechnicalException;

	// @WebMethod(operationName = "byPass")
	// @WebResult(name = "statut")
	// public String modeByPass();
	/**Begin feature/Changement_Categorie_Client**/
	@WebMethod(operationName = "changeCategoryCustomer")
	@WebResult(name = "resultat")
	public String changeCategoryCustomer(
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject,
			@WebParam(name = "numeroClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "sousCategorieDestination") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String codeSousCategorieDest
			)
			throws FunctionnalException, TechnicalException;
	/**End feature/Changement_Categorie_Client**/
}
