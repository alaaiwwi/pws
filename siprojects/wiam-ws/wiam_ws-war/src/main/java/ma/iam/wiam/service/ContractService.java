package ma.iam.wiam.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContractBean;
import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.ws.neto.model.Contrat;
import ma.iam.wiam.ws.neto.model.Template;

@WebService(serviceName = "ContractService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface ContractService extends BasicService {

	@WebMethod(operationName = "getStatus")
	@WebResult(name = "status")
	public ReferentialBean getStatus( 
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd)
			throws FunctionnalException, TechnicalException;
	
	/**<!--Begin feature/Ajouter_Neto_Int -->**/
	@WebMethod(operationName = "createContract")
	@WebResult(name = "resultat")
	public String createContract(
			@WebParam(name = "createContractOrder") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") CreateContractOrder createContractOrder,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject)
					throws FunctionnalException, TechnicalException;
	
	/**<!--End feature/Ajouter_Neto_Int -->**/
	
	/**Begin : feature/Template**/
	@WebMethod(operationName = "getTemplateById")
	@WebResult(name = "resultat")
	public Template getTemplateById(
			@WebParam(name = "templateID") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") java.lang.Long templateID,
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject
			)throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "getAllTemplate")
	@WebResult(name = "resultat")
	public java.util.List<Template> getAllTemplate() throws FunctionnalException, TechnicalException;
	
	/**End : feature/Template**/
	
	/**Begin : feature/Recherche_Contrat_Client */
	@WebMethod(operationName = "getContractClient")
	@WebResult(name = "resultat")
	public List<Contrat> getContractClient(
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject,
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String numCli,
			@WebParam(name = "typeIdentifiant") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String typeIdentifiant,
			@WebParam(name = "identifiant") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String identifiant
			)throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "getContractWithServices")
	@WebResult(name = "resultat")
	public ContractBean getContractWithServices(
			@WebParam(name = "ticketObject") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") TicketObject ticketObject,
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String numCli,
			@WebParam(name = "Nd_Login") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd_login,
			@WebParam(name = "typeIdentifiant") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String typeIdentifiant,
			@WebParam(name = "identifiant") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String identifiant
			)throws FunctionnalException, TechnicalException;
	
	
	/**End : feature/Recherche_Contrat_Client */
	
	
	
}
