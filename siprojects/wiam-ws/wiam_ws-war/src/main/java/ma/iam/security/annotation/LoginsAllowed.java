package ma.iam.security.annotation;

import java.lang.annotation.*;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;


@Documented
@Retention (RUNTIME)
@Target({TYPE, METHOD})
public @interface LoginsAllowed {
    String[] value();
}
