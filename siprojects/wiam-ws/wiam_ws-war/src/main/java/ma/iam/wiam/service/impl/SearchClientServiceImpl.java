package ma.iam.wiam.service.impl;

import java.util.List;

import ma.iam.security.SecuredObject;
import ma.iam.wiam.business.ByPassManagerBiz;
import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.CustomerBillingManagerBiz;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.mapper.ClientMapper;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.service.SearchClientService;
import ma.iam.wiam.vo.ClientSearchBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SecuredObject
@Component(value = "searchClientService")
public class SearchClientServiceImpl extends BasicServiceImpl implements
		SearchClientService {

	@Autowired
	private ClientManagerBiz clientManagerBiz;
	@Autowired
	private ByPassManagerBiz byPassManagerBiz;
	@Autowired
	private CustomerBillingManagerBiz customerBillingManagerBiz;

	public Boolean verifySolavabilteClientByNcli(String ncli)
			throws FunctionnalException, TechnicalException {
		ClientSearchCriteria clientCriteria = new ClientSearchCriteria();
		clientCriteria.setNcli(ncli);
		return clientManagerBiz.verifySolavabilteClientByNcli(clientCriteria);
	}

	public List<ClientSearchBean> findClientByNdOrLogin(String param)
			throws FunctionnalException, TechnicalException {
		ClientSearchCriteria clientCriteria = new ClientSearchCriteria();
		clientCriteria.setNdOrLogin(param);
		return ClientMapper.mapPersistBean2BeanCollection(clientManagerBiz
				.findClientByCriteria(clientCriteria));
	}

	public List<ClientSearchBean> getClientByClientNum(String param)
			throws FunctionnalException, TechnicalException {
		ClientSearchCriteria clientCriteria = new ClientSearchCriteria();
		clientCriteria.setNcli(param);
		return ClientMapper.mapPersistBean2BeanCollection(clientManagerBiz
				.findClientByCriteria(clientCriteria));
	}

	public List<ClientSearchBean> getClientByIdentifiant(String param)
			throws FunctionnalException, TechnicalException {
		ClientSearchCriteria clientCriteria = new ClientSearchCriteria();
		clientCriteria.setIdentifiant(param);
		return ClientMapper.mapPersistBean2BeanCollection(clientManagerBiz
				.findClientByCriteria(clientCriteria));
	}

	public List<ClientSearchBean> getClientByReasonSocial(String param)
			throws FunctionnalException, TechnicalException {
		ClientSearchCriteria clientCriteria = new ClientSearchCriteria();
		clientCriteria.setSocialReason(param);
		return ClientMapper.mapPersistBean2BeanCollection(clientManagerBiz
				.findClientByCriteria(clientCriteria));
	}

	// public String modeByPass() {
	// return byPassManagerBiz.modeByPasse();
	// }
}
