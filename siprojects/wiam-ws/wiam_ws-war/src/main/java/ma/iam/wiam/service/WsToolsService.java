package ma.iam.wiam.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.neto.bean.ReferentialBeanList;
import ma.iam.wiam.params.AgenceBancaireParams;
import ma.iam.wiam.params.QuartierParams;
import ma.iam.wiam.params.VoieParams;
import ma.iam.wiam.vo.CommuneBean;

@WebService(serviceName = "wsToolsService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface WsToolsService extends BasicService {

	@WebMethod(operationName = "listerCommunes")
	@WebResult(name = "commune")
	public List<CommuneBean> getCommunes() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerQuartierByCommune")
	@WebResult(name = "quartier")
	public List<ReferentialBean> getQuartiersByCommune(
			@WebParam(name = "quartierParams") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") QuartierParams commune)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerVoieByQuartier")
	@WebResult(name = "voie")
	public List<ReferentialBean> getVoieByQuartier(
			@WebParam(name = "quartier") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") VoieParams voieParam)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerCodesPersonnes")
	@WebResult(name = "codePersonne")
	public List<ReferentialBean> getCodesPersonnes()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerEtatsCiviles")
	@WebResult(name = "etatCivile")
	public List<ReferentialBean> getEtatsCiviles() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerIdentifiantsResidentiels")
	@WebResult(name = "identifiantResidentiel")
	public List<ReferentialBean> getIdentifiantsResidentiels()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerIdentifiantsProfessionels")
	@WebResult(name = "identifiantProfessionel")
	public List<ReferentialBean> getIdentifiantsProf()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerIdentifiantsEntreprises")
	@WebResult(name = "identifiantEntreprise")
	public List<ReferentialBean> getIdentifiantsEntreprise()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerProfessions")
	@WebResult(name = "profession")
	public List<ReferentialBean> getProfessions() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerEntitesGestionnaires")
	@WebResult(name = "entitesGestionnaire")
	public List<ReferentialBean> getEntityManagers()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listSegments1")
	@WebResult(name = "segment1")
	public List<ReferentialBean> getSegments1() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listSegments2")
	@WebResult(name = "segment2")
	public List<ReferentialBean> getSegments2() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerInfosProspections")
	@WebResult(name = "infoProspection")
	public List<ReferentialBean> getProspectionInfos()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerBanques")
	@WebResult(name = "banque")
	public List<ReferentialBean> getBanks() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerAgencesBancaires")
	@WebResult(name = "agenceBancaire")
	public List<ReferentialBean> getAgenciesByParams(
			@WebParam(name = "agenceBancaireParams") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") AgenceBancaireParams agenceBancaireParams)
			throws FunctionnalException, TechnicalException;

	// @WebMethod(operationName = "byPass")
	// @WebResult(name = "statut")
	// public String modeByPass();
	@WebMethod(operationName = "getConfigAll")
	@WebResult(name = "referentialBeanList")
	public ReferentialBeanList getConfigAll()throws FunctionnalException, TechnicalException;
}
