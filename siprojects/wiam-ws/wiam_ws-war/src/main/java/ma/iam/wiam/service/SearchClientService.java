package ma.iam.wiam.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.vo.ClientSearchBean;

@WebService(serviceName = "searchClientService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface SearchClientService extends BasicService{

	@WebMethod(operationName = "verifierClientDouteux")
	@WebResult(name = "result")
	public Boolean verifySolavabilteClientByNcli(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli)
					throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getClientByNdOrLogin")
	@WebResult(name = "result")
	public List<ClientSearchBean> findClientByNdOrLogin(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String param)
					throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getClientByClientNum")
	@WebResult(name = "result")
	public List<ClientSearchBean> getClientByClientNum(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String param)
					throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getClientByIdentifiant")
	@WebResult(name = "result")
	public List<ClientSearchBean> getClientByIdentifiant(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String param)
					throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getClientByRaisonSocial")
	@WebResult(name = "result")
	public List<ClientSearchBean> getClientByReasonSocial(
			@WebParam(name = "client") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String param)
					throws FunctionnalException, TechnicalException;
	
//	@WebMethod(operationName = "byPass")
//	@WebResult(name = "statut")
//	public String modeByPass();

}
