package ma.iam.wiam.service.impl;

import java.util.List;

import ma.iam.security.SecuredObject;
import ma.iam.wiam.business.CustomerBillingManagerBiz;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.mapper.CustomerBillingMapper;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.service.CustomerBillingAccountService;
import ma.iam.wiam.vo.CustomerBillingInfoResponseBean;
import ma.iam.wiam.vo.UpdateCustomerBillingInfoRequestBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author s.hroumti
 *
 */
@SecuredObject
@Component(value = "customerBillingAccountServiceImpl")
public class CustomerBillingAccountServiceImpl extends BasicServiceImpl
		implements CustomerBillingAccountService {
	@Autowired
	private CustomerBillingManagerBiz customerBillingManagerBiz;

	@SuppressWarnings("unchecked")
	public List<CustomerBillingInfoResponseBean> getBillingInfoByClientNum(
			String ncli) throws FunctionnalException, TechnicalException {
		ClientSearchCriteria clientCriteria = new ClientSearchCriteria();
		clientCriteria.setNcli(ncli);
		return (List<CustomerBillingInfoResponseBean>) CustomerBillingMapper
				.mapBusinessBean2VOCollection(customerBillingManagerBiz
						.getBillingInfos(clientCriteria));
	}

	public String modifyBillingAccountInfo(
			UpdateCustomerBillingInfoRequestBean customerBillingInfoBean)
			throws FunctionnalException, TechnicalException {

		return customerBillingManagerBiz
				.modifyBillingAccountInfo(CustomerBillingMapper
						.mapVO2BusinessBean(customerBillingInfoBean));
	}
}
