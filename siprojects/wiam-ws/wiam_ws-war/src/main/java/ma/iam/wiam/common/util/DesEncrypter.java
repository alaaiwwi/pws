package ma.iam.wiam.common.util;

import java.io.UnsupportedEncodingException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class DesEncrypter {

	/** Instance du LOGGER */
	/** Le logger de la classe. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DesEncrypter.class);

    Cipher ecipher;



    Cipher dcipher;



    // 8-byte Salt

    byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,

                (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03 };



    // Iteration count

    int iterationCount = 19;



    String passPhrase = "INAT2OSTE0RNIA0MET7";



    SecretKeySpec key;



    /**

     * @throws Exception

     */

    public DesEncrypter() throws Exception {

          try {

                // Create the key

                /*

                 * Security.addProvider(new com.ibm.crypto.provider.IBMJCE());

                 * Security.addProvider(new com.sun.crypto.provider.SunJCE());

                 */

                Class providerClass = null;

                try {

                      providerClass = Class.forName("com.sun.crypto.provider.SunJCE");

                } catch (ClassNotFoundException cnfe) {

                      providerClass = Class.forName("com.ibm.crypto.provider.IBMJCE");
                      LOGGER.info("DesEncrypter()", " the provider class is com.ibm.crypto.provider.IBMJCE " );
                }

                Security.addProvider((Provider) providerClass.newInstance());


                byte[] keyBytes= new byte[16];

                byte[] b= passPhrase.getBytes();

                int len= b.length;

                if (len > keyBytes.length) len = keyBytes.length;

                System.arraycopy(b, 0, keyBytes, 0, len);



                key = new SecretKeySpec(keyBytes, "AES");

                ecipher = Cipher.getInstance(key.getAlgorithm());

                dcipher = Cipher.getInstance(key.getAlgorithm());

               

                ecipher.init(Cipher.ENCRYPT_MODE, key);

                dcipher.init(Cipher.DECRYPT_MODE, key);



          }catch (javax.crypto.NoSuchPaddingException e) {

  			LOGGER.error("DesEncrypter()", e.getMessage(), e);
  			LOGGER.info("DesEncrypter()", " the exception was catched " );

          } catch (java.security.NoSuchAlgorithmException e) {

        	  LOGGER.error("DesEncrypter()", e.getMessage(), e);
        	  LOGGER.info("DesEncrypter()", " the exception was catched " );

          } catch (java.security.InvalidKeyException e) {

        	  LOGGER.error("DesEncrypter()", e.getMessage(), e);
        	  LOGGER.info("DesEncrypter()", " the exception was catched " );

          }

    }

    /**

     * cripte la chaine pass�e en parametre

     * @param str

     * @return

     */

    public String encrypt(String str) {

          try {

                // Encode the string into bytes using utf-8

                byte[] utf8 = str.getBytes("UTF8");



                // Encrypt

                byte[] enc = ecipher.doFinal(utf8);



                // Encode bytes to base64 to get a string

                return new sun.misc.BASE64Encoder().encode(enc);

          } catch (javax.crypto.BadPaddingException e) {

        	  LOGGER.error("encrypt", "encrypter le " + str);
        	  LOGGER.error("encrypt", e.getMessage(), e);
        	  LOGGER.info("encrypt", " the exception was catched " );

          } catch (IllegalBlockSizeException e) {
        	  LOGGER.error("encrypt", "encrypter le " + str);
        	  LOGGER.error("encrypt", e.getMessage(), e);
        	  LOGGER.info("encrypt", " the exception was catched " );

          } catch (UnsupportedEncodingException e) {
        	  LOGGER.error("encrypt", "encrypter le " + str);
        	  LOGGER.error("encrypt", e.getMessage(), e);
        	  LOGGER.info("encrypt", " the exception was catched " );

          } catch (java.io.IOException e) {
        	  LOGGER.error("encrypt", "encrypter le " + str);
        	  LOGGER.error("encrypt", e.getMessage(), e);
        	  LOGGER.info("encrypt", " the exception was catched " );

          }

          return null;

    }



    /**

     * decripte la chaine pass�e en parametre

     *

     * @param str

     * @return

     */

    public String decrypt(String str) {

          try {

                // Decode base64 to get bytes

                byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);



                // Decrypt

                byte[] utf8 = dcipher.doFinal(dec);



                // Decode using utf-8

                return new String(utf8, "UTF8");

          } catch (javax.crypto.BadPaddingException e) {

        	  LOGGER.error("decrypt", "encrypter le " + str);
        	  LOGGER.error("decrypt", e.getMessage(), e);
        	  LOGGER.info("decrypt", " the exception was catched " );

          } catch (IllegalBlockSizeException e) {

        	  LOGGER.error("decrypt", "encrypter le " + str);
        	  LOGGER.error("decrypt", e.getMessage(), e);
        	  LOGGER.info("decrypt", " the exception was catched " );

          } catch (UnsupportedEncodingException e) {

        	  LOGGER.error("decrypt", "encrypter le " + str);
        	  LOGGER.error("decrypt", e.getMessage(), e);
        	  LOGGER.info("decrypt", " the exception was catched " );

          } catch (java.io.IOException e) {

        	  LOGGER.error("decrypt", "encrypter le " + str);
        	  LOGGER.error("decrypt", e.getMessage(), e);
        	  LOGGER.info("decrypt", " the exception was catched " );

          }

          return null;

    }



   
}
