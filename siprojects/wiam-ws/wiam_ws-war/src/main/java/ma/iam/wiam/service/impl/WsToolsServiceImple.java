package ma.iam.wiam.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;

import ma.iam.security.SecuredObject;
import ma.iam.wiam.neto.bean.ReferentialBeanList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netonomy.blm.interfaces.contact.TitleIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.business.AdresseManagerBiz;
import ma.iam.wiam.business.ByPassManagerBiz;
import ma.iam.wiam.business.EntityManagerBiz;
import ma.iam.wiam.business.IdentityManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.mapper.BankAgencyMapper;
import ma.iam.wiam.mapper.BankMapper;
import ma.iam.wiam.mapper.CommuneMapper;
import ma.iam.wiam.mapper.LevelMapper;
import ma.iam.wiam.mapper.QuartierMapper;
import ma.iam.wiam.mapper.TiltesMapper;
import ma.iam.wiam.mapper.ValueChoiceItemMapper;
import ma.iam.wiam.mapper.VoieMapper;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.params.AgenceBancaireParams;
import ma.iam.wiam.params.QuartierParams;
import ma.iam.wiam.params.VoieParams;
import ma.iam.wiam.service.WsToolsService;
import ma.iam.wiam.vo.CommuneBean;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;
import ma.iam.wiam.ws.neto.model.PCommune;

@SecuredObject
@Component(value = "wsToolsService")
public class WsToolsServiceImple extends BasicServiceImpl implements
		WsToolsService {

	@Autowired
	AdresseManagerBiz adresseMangerBiz;

	@Autowired
	IdentityManagerBiz identityManagerBiz;

	@Autowired
	EntityManagerBiz entityManagerBiz;

	@Autowired
	ReferentialManagerBiz referentialManagerBiz;

	@Autowired
	ByPassManagerBiz byPassManagerBiz;

	@Resource
	protected WebServiceContext context;

	public List<CommuneBean> getCommunes() throws FunctionnalException,
			TechnicalException {

		List<PCommune> communes = adresseMangerBiz.getListCommune();

		referentialManagerBiz
				.setItemInReferential(Constants.COMMUNES, communes);

		return CommuneMapper.mapPersit2VoList(communes);
	}

	public List<ReferentialBean> getQuartiersByCommune(QuartierParams commune)
			throws FunctionnalException, TechnicalException {
		return new QuartierMapper()
				.mapListBusinesModelsToListBusinesBeans(adresseMangerBiz
						.getQuartiersByCriteria(QuartierMapper
								.mapParams2Criteria(commune)));
	}

	public List<ReferentialBean> getVoieByQuartier(VoieParams voieParam)
			throws FunctionnalException, TechnicalException {
		return new VoieMapper()
				.mapListBusinesModelsToListBusinesBeans(adresseMangerBiz
						.getVoieByByCriteria(VoieMapper
								.mapParams2Criteria(voieParam)));
	}

	public List<ReferentialBean> getCodesPersonnes()
			throws FunctionnalException, TechnicalException {
		List<TitleIF> personCodes = identityManagerBiz.getCodesPersonnes();

		referentialManagerBiz.setItemInReferential(Constants.PERSON_CODES,
				personCodes);

		return new TiltesMapper()
				.mapListBusinesModelsToListBusinesBeans(personCodes);
	}

	public List<ReferentialBean> getEtatsCiviles() throws FunctionnalException,
			TechnicalException {
		List<ValueChoiceItemIF> maritalStatus = identityManagerBiz
				.getEtatsCiviles();

		referentialManagerBiz.setItemInReferential(Constants.MARITAL_STATUS,
				maritalStatus);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(maritalStatus);
	}

	public List<ReferentialBean> getIdentifiantsResidentiels()
			throws FunctionnalException, TechnicalException {
		List<ValueChoiceItemIF> identifiers = identityManagerBiz
				.getIdentifiantsResidentiels();

		referentialManagerBiz.setItemInReferential(Constants.GP_IDENTITY_TYPES,
				identifiers);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(identifiers);
	}

	public List<ReferentialBean> getIdentifiantsProf()
			throws FunctionnalException, TechnicalException {
		List<ValueChoiceItemIF> identifiers = identityManagerBiz
				.getIdentifiantsProf();

		referentialManagerBiz.setItemInReferential(
				Constants.PRO_IDENTITY_TYPES, identifiers);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(identifiers);
	}

	public List<ReferentialBean> getIdentifiantsEntreprise()
			throws FunctionnalException, TechnicalException {
		List<ValueChoiceItemIF> identifiers = identityManagerBiz
				.getIdentifiantsEntreprise();

		referentialManagerBiz.setItemInReferential(
				Constants.ENT_IDENTITY_TYPES, identifiers);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(identifiers);
	}

	public List<ReferentialBean> getProfessions() throws FunctionnalException,
			TechnicalException {
		List<ValueChoiceItemIF> professions = identityManagerBiz
				.getProfessions();

		referentialManagerBiz.setItemInReferential(Constants.PROFESSIONS,
				professions);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(professions);
	}

	public List<ReferentialBean> getEntityManagers()
			throws FunctionnalException, TechnicalException {
		List<Object[]> mtAgenciesInRegie = entityManagerBiz
				.getAllMTAgenciesInRegie();

		referentialManagerBiz.setItemInReferential(
				Constants.MT_AGENCIES_IN_REGIE, mtAgenciesInRegie);

		return new LevelMapper()
				.mapListBusinesModelsToListBusinesBeans(mtAgenciesInRegie);
	}

	public List<ReferentialBean> getSegments1() throws FunctionnalException,
			TechnicalException {
		List<ValueChoiceItemIF> segments1 = entityManagerBiz.getSegments1();

		referentialManagerBiz.setItemInReferential(Constants.SEGMENTS1,
				segments1);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(segments1);
	}

	public List<ReferentialBean> getSegments2() throws FunctionnalException,
			TechnicalException {
		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(entityManagerBiz
						.getSegments2());
	}

	public List<ReferentialBean> getProspectionInfos()
			throws FunctionnalException, TechnicalException {
		List<ValueChoiceItemIF> prospectionInfos = entityManagerBiz
				.getProspectionInfos();

		referentialManagerBiz.setItemInReferential(Constants.PROSPECTION_INFOS,
				prospectionInfos);

		return new ValueChoiceItemMapper()
				.mapListBusinesModelsToListBusinesBeans(prospectionInfos);
	}

	public List<ReferentialBean> getBanks() throws FunctionnalException,
			TechnicalException {
		List<Bank> banks = entityManagerBiz.getBanks();

		referentialManagerBiz.setItemInReferential(Constants.BANKS, banks);

		return new BankMapper().mapListBusinesModelsToListBusinesBeans(banks);
	}

	public List<ReferentialBean> getAgenciesByParams(
			AgenceBancaireParams agenceBancaireParams)
			throws FunctionnalException, TechnicalException {
		List<BankAgency> bankAgencies = entityManagerBiz
				.getAgenciesByCriteria(BankAgencyMapper
						.mapParamsToCriteria(agenceBancaireParams));
		return new BankAgencyMapper()
				.mapListBusinesModelsToListBusinesBeans(bankAgencies);
	}

	// public String modeByPass() {
	// return byPassManagerBiz.modeByPasse();
	// }

	@Override
	public ReferentialBeanList getConfigAll() throws FunctionnalException, TechnicalException {
		ReferentialBeanList referentialBeanList = new ReferentialBeanList();
		referentialBeanList.setCodesPersonnes(getCodesPersonnes());
		referentialBeanList.setEtatsCiviles (getEtatsCiviles ());
		referentialBeanList.setIdentifiantsResidentiels(getIdentifiantsResidentiels());
		referentialBeanList.setIdentifiantsProf(getIdentifiantsProf());
		referentialBeanList.setIdentifiantsEntreprise(getIdentifiantsEntreprise());
		referentialBeanList.setProfessions (getProfessions ());
		referentialBeanList.setSegments1 (getSegments1 ());
		referentialBeanList.setSegments2 (getSegments2 ());
		referentialBeanList.setProspectionInfos(getProspectionInfos());
		referentialBeanList.setBanks (getBanks() );
		return referentialBeanList;
	}
}
