package ma.iam.wiam.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.CheckOrderResponseBean;
import ma.iam.wiam.neto.bean.CommandeBean;
import ma.iam.wiam.neto.bean.ServiceList;
import ma.iam.wiam.neto.bean.ServiceRulesRequestBean;
import ma.iam.wiam.vo.ServiceRule;

@WebService(serviceName = "ServiceOperationsService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface ServiceOperationsService extends BasicService {

	@WebMethod(operationName = "getServicesSouscrits")
	public ServiceList getServicesSouscrits(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getAddableServices")
	public ServiceList getAddableServices(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getRemovableServices")
	public ServiceList getRemovableServices(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getEditableServices")
	public ServiceList getEditableServices(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getServiceParamsModify")
	@WebResult(name = "serviceParams")
	public ServiceList getServiceParamsModify(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd,
			@WebParam(name = "serviceId") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") List<String> serviceId)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getServiceParamsAdd")
	@WebResult(name = "serviceParams")
	public ServiceList getServiceParamsAdd(
			@WebParam(name = "numClient") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ncli,
			@WebParam(name = "nd") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String nd,
			@WebParam(name = "serviceId") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") List<String> serviceIds)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "getDependanceAndIncompatibleServiceRules")
	@WebResult(name = "rule")
	public List<ServiceRule> getDependanceAndIncompatibleServiceRules(
			@WebParam(name = "request") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") ServiceRulesRequestBean serviceRulesRequestBean)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "checkOrder")
	@WebResult(name = "result")
	public CheckOrderResponseBean checkOrder(
			@WebParam(name = "commande") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "createOrder")
	@WebResult(name = "result")
	public CheckOrderResponseBean createOrder(
			@WebParam(name = "commande") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "cancelOrder")
	@WebResult(name = "result")
	public CheckOrderResponseBean cancelOrder(
			@WebParam(name = "commande") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException;

}
